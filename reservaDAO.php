<?php
require_once'db.php';
require_once'character.php';

date_default_timezone_set('America/Lima');
class reservaDAO{

	public function lista(){
		try {
			$database = new ConexionBD();

			/*$sql = "SELECT r.*,c.cliente_id, CONCAT(c.cliente_nombre, ' ', c.cliente_apellido) As NombreCliente,
					co.conductor_id,CONCAT(co.conductor_nombre, ' ', co.conductor_apellido) As NombreConductor,
					t.tiposervicio_nombre FROM tb_reserva r 
					INNER JOIN tb_cliente c ON c.cliente_id = r.cliente_id
					LEFT JOIN tb_conductor co ON co.conductor_id = r.conductor_id
					INNER JOIN tb_tiposervicio t ON t.tiposervicio_id = r.tiposervicio_id
					WHERE r.estado = 1;
					";*/
			$sql = "CALL sp_listar_reservas()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaConfigreserva(){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_config_reserva ";
			$sql = "CALL sp_listar_config_reserva(0)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaConfigreservaActivo(){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_config_reserva where configreserva_status = 1";
			$sql = "CALL sp_listar_config_reserva(1)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}


	public function listaCli($data){
		try {
			$database = new ConexionBD();

			/*$sql = " SELECT r.*, CONCAT(c.cliente_nombre, ' ', c.cliente_apellido) As NombreCliente, 
						DATE_FORMAT(r.reserva_fecha, '%Y-%m-%d') as fecha , 
						DATE_FORMAT(r.reserva_fecha, '%T') AS hora,
						CONCAT(co.conductor_nombre, ' ', co.conductor_apellido) As NombreConductor
					FROM tb_reserva r 
					INNER JOIN tb_cliente c ON r.cliente_id = c.cliente_id
					INNER JOIN tb_conductor co ON r.conductor_id = co.conductor_id
					WHERE r.cliente_id = :cliente_id AND reserva_estado not in (3)";*/
			$sql = "CALL sp_listar_reservas_por_cliente(:cliente_id)";
			$database->query($sql);
			$database->bind(':cliente_id', $data['cliente_id']);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }

        
	}

	public function listaCond($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT r.*, CONCAT(c.cliente_nombre, ' ', c.cliente_apellido) As NombreCliente, DATE_FORMAT(r.reserva_fecha, '%Y-%m-%d') as fecha , DATE_FORMAT(r.reserva_fecha, '%T') AS hora FROM tb_reserva r INNER JOIN tb_cliente c ON r.cliente_id = c.cliente_id WHERE conductor_id = :conductor_id AND reserva_estado not in (3)";
			$sql = "CALL sp_listar_reservas_por_conductor(:conductor_id)";
			$database->query($sql);
			$database->bind(':conductor_id', $data['conductor_id']);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }   
	}

	public function insertConfigreserva($data){
		//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		try {
			$database = new ConexionBD();

			/*$sql="INSERT INTO tb_config_reserva (configreserva_horas,configreserva_alerta,configreserva_panel,configreserva_bloqueo) VALUES 
			(:configreserva_horas,:configreserva_alerta,:configreserva_panel,:configreserva_bloqueo)";*/
			$sql = "CALL sp_configreserva_mantenimiento(@a_message, 'nuevo', '', :configreserva_horas, :configreserva_alerta, :configreserva_panel, :configreserva_bloqueo)";

			$database->query($sql);
			$database->bind(':configreserva_horas', $data['configreserva_horas']);
			$database->bind(':configreserva_alerta',$data['configreserva_alerta']);
			$database->bind(':configreserva_panel', $data['configreserva_panel']);
			$database->bind(':configreserva_bloqueo', $data['configreserva_bloqueo']);
		    $database->execute();
		    return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function UpdateConfigreservaData($data){

		$configreserva_bloqueo = explode("string:", $data['configreserva_bloqueo']);

		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_config_reserva SET configreserva_horas = :configreserva_horas, configreserva_alerta = :configreserva_alerta, configreserva_panel = :configreserva_panel, configreserva_bloqueo = :configreserva_bloqueo WHERE configreserva_id = :configreserva_id";
			$sql = "CALL sp_configreserva_mantenimiento(@a_message, 'update', :configreserva_id, :configreserva_horas, :configreserva_alerta, :configreserva_panel, :configreserva_bloqueo)";
			$database->query($sql);
			$database->bind(':configreserva_horas', $data['configreserva_horas']);
			$database->bind(':configreserva_alerta', $data['configreserva_alerta']); 
			$database->bind(':configreserva_panel', $data['configreserva_panel']); 
			$database->bind(':configreserva_id', $data['configreserva_id']);
			$database->bind(':configreserva_bloqueo', $configreserva_bloqueo[1]);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function UpdateConfigreserva($data){
		$where = '';
		if($data['configreserva_id'] != 0){
			$where = ' WHERE configreserva_id = :configreserva_id';
		}

		try {
			$database = new ConexionBD();
			$sql = "UPDATE tb_config_reserva SET configreserva_status = :configreserva_status ".$where;
			$database->query($sql);
			$database->bind(':configreserva_status', $data['configreserva_status']);
			if($data['configreserva_id'] != 0){
				$database->bind(':configreserva_id', $data['configreserva_id']);
			}
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function CountConfigreserva($configreserva_id){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*) as total FROM tb_config_reserva WHERE configreserva_status = 1 AND configreserva_id NOT IN (:configreserva_id)";
			$sql = "CALL sp_count_config_reserva(:configreserva_id)";
			$database->query($sql);
			$database->bind(':configreserva_id', $configreserva_id);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertar($data){
		try {
			//echo '<pre>'; print_r($data); echo '</pre>'; exit;
			$database = new ConexionBD();

			/*$sql="INSERT INTO tb_reserva (cliente_id, reserva_fecha, origen_id, origen, lat_origen, lon_origen, dir_origen, referencia_origen, destino_id, destino, lat_destino, lon_destino, dir_destino, referencia_destino, tiposervicio_id, tipo_pago, tarifa, estado ) VALUES 
			(:cliente_id, :reserva_fecha, :origen_id, :origen, :lat_origen, :lon_origen, :dir_origen, :ref_origen, :destino_id, :destino, :lat_destino, :lon_destino, :dir_destino, :ref_destino, :tiposervicio_id, :tipo_pago, :tarifa, :estado )";*/
			$sql = "CALL sp_reserva_mantenimiento(@a_message,'nuevo',NULL,:cliente_id,:reserva_fecha,:origen_id,:origen,:lat_origen,:lon_origen,:dir_origen,:ref_origen,:destino_id,:destino,:lat_destino,:lon_destino,:dir_destino,:ref_destino,:tiposervicio_id,:tipo_pago,:tarifa,:estado, :reserva_estado)";

			$database->query($sql);
			$database->bind(':cliente_id', $data['cliente_id']);
			//$database->bind(':reserva_fecha', date('Y-m-d H:i:s',$data['fecha']));
			$database->bind(':reserva_fecha', date_format( new DateTime($data['fecha']), 'Y-m-d H:i:s' ));
			$database->bind(':origen_id', $data['id_origen']);
			$database->bind(':origen', SanitizeCharacter($data['origen']));
			$database->bind(':lat_origen', $data['origen_latitud']);
			$database->bind(':lon_origen', $data['origen_longitud']);
			$database->bind(':dir_origen', SanitizeCharacter($data['direccion_origen']));
			$database->bind(':ref_origen', SanitizeCharacter($data['ref_origen']));
			$database->bind(':destino_id', $data['id_destino']);
			$database->bind(':destino', SanitizeCharacter($data['destino']));
			$database->bind(':lat_destino', $data['destino_latitud']);
			$database->bind(':lon_destino', $data['destino_longitud']);
			$database->bind(':dir_destino', SanitizeCharacter($data['direccion_destino']));
			$database->bind(':ref_destino', SanitizeCharacter($data['ref_destino']));
			$database->bind(':tiposervicio_id', $data['tiposervicio_id']);
			$database->bind(':tipo_pago', $data['tipo_pago']);
			$database->bind(':tarifa', $data['tarifa']);
			$database->bind(':estado', '1');
			$database->bind(':reserva_estado', $data['reserva_estado']);
		    $database->execute();
		    return $database->resultSet();
		    //$id = $database->lastInsertId();
	        //return array('status' => true, 'lastid' => $id);

	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function deleteReserva($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_reserva SET estado = :estado WHERE reserva_id = :reserva_id";
			$sql = "CALL sp_reserva_mantenimiento(@a_message,'delete',:reserva_id,'','','','','','','','','','','','','','','','','',:estado)";
			$database->query($sql);
			$database->bind(':estado', 0);
	        $database->bind(':reserva_id', $data['reserva_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertReserva($data){

		try {
			if(!empty($data->conductor_id)){
				if(!empty($data->flag)){
					$estado = 5;
				}else{
					$estado = 1;
				}
			}else{
				$estado = 0;
			}

			//echo '<pre>'; print_r($estado); echo '</pre>'; exit;
			$database = new ConexionBD();

			$sql="INSERT INTO tb_reserva (cliente_id, reserva_fecha, origen_id, lat_origen, lon_origen, dir_origen, destino_id, lat_destino, lon_destino, dir_destino, tiposervicio_id, tipo_pago, tarifa, reserva_estado , conductor_id) VALUES 
			(:cliente_id, :reserva_fecha, :origen_id, :lat_origen, :lon_origen, :dir_origen, :destino_id, :lat_destino, :lon_destino, :dir_destino, :tiposervicio_id, :tipo_pago, :tarifa, :reserva_estado, :conductor_id)";

			$database->query($sql);
			$database->bind(':cliente_id', $data->cliente_id);
			$database->bind(':reserva_fecha', date_format( new DateTime($data->cliente_fecha), 'Y-m-d H:i:s' ));
			$database->bind(':origen_id', $data->origen_id);
			$database->bind(':lat_origen', $data->origen_lat);
			$database->bind(':lon_origen', $data->origen_lng);
			$database->bind(':dir_origen', SanitizeCharacter($data->origen_direccion));
			$database->bind(':destino_id', $data->destino_id);
			$database->bind(':lat_destino', $data->destino_lat );
			$database->bind(':lon_destino', $data->destino_lng );
			$database->bind(':dir_destino', SanitizeCharacter($data->destino_direccion));
			$database->bind(':tiposervicio_id', $data->tiposervicio);
			$database->bind(':tipo_pago', $data->tipopago);
			$database->bind(':tarifa', $data->cliente_costo);
			$database->bind(':reserva_estado', $estado);
			if(!empty($data->conductor_id)){
				$database->bind(':conductor_id', $data->conductor_id);
			}else{
				$database->bind(':conductor_id', NULL);
			}
		    $database->execute();
		    $id = $database->lastInsertId();

	        return array('status' => true, 'lastid' => $id);


	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function updateReserva($conductor_id, $reserva_id, $tiposervicio_id){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_reserva SET conductor_id = :conductor_id, tiposervicio_id = :tiposervicio_id, reserva_estado = :reserva_estado WHERE reserva_id = :reserva_id";
			$sql = "CALL sp_reserva_mantenimiento(@a_message,'delete',:reserva_id,conductor_id,'','','','','','','','','','','','','',:tiposervicio_id,'','',:reserva_estado)";
			$database->query($sql);
			$database->bind(':reserva_estado', 1);
			$database->bind(':conductor_id', $conductor_id);
			$database->bind(':tiposervicio_id', $tiposervicio_id);
	        $database->bind(':reserva_id', $reserva_id);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}


	public function listaReservaId($id){
		try {

			$database = new ConexionBD();

			/*$sql = "SELECT r.*,cli.cliente_id,CONCAT(cli.cliente_nombre,' ',cli.cliente_apellido) AS cliente_nombre,cli.cliente_correo,
					con.conductor_id,CONCAT(con.conductor_nombre,' ',con.conductor_apellido) as conductor_nombre,con.conductor_dni,con.conductor_foto,con.conductor_calificacion,con.conductor_celular,
					uni.unidad_placa,uni.unidad_placa,uni.unidad_marca,uni.unidad_modelo,uni.unidad_color,uni.unidad_foto
				    FROM movil.tb_reserva r
					LEFT JOIN tb_cliente cli ON r.cliente_id = cli.cliente_id 
				    LEFT JOIN tb_conductor con ON r.conductor_id = con.conductor_id
				    INNER JOIN tb_unidad uni ON con.unidad_id = uni.unidad_id
				    WHERE r.reserva_id = :reserva_id";*/


			$database->query($sql);
			$database->bind(':reserva_id',$id);
			$database->execute();
			return $database->resultSet();

			
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function cancelarReserva($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_reserva SET reserva_estado = :estado WHERE reserva_id = :reserva_id";
			$sql = "CALL sp_reserva_mantenimiento(@a_message,'cancelar',:reserva_id,'','','','','','','','','','','','','','','','','','',:reserva_estado)";
			$database->query($sql);
			$database->bind(':reserva_estado', $data['estado']);
	        $database->bind(':reserva_id', $data['reserva_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function updateReservaConductor($reserva_id){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_reserva SET conductor_id = null WHERE reserva_id = :reserva_id";
			$sql = "CALL sp_reserva_mantenimiento(@a_message,'update_conductor',:reserva_id,'','','','','','','','','','','','','','','','','','','')";
			$database->query($sql);
	        $database->bind(':reserva_id', $reserva_id);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

}