<?php

require 'DAL/generalDAO.php';
require 'DAL/servicioDAO.php';
require 'DAL/constantes.php';

$generalDAO = new generalDAO();
$servicioDAO = new servicioDAO();

$cadena = $_REQUEST['servicio'];
$key = 'saul';
$res = $generalDAO->decrypt($cadena,$key);

$result = $servicioDAO->listaServicio($res);

$origenlat=floatval($result[0]['origen_lat']);
$origenlon=floatval($result[0]['origen_lon']);
$destinolat=floatval($result[0]['destino_lat']);
$destinolon=floatval($result[0]['destino_lon']);

$conductorId = $result[0]['conductor_id'];

?>
<style>
  #mapMov{
    height:750px;
    width:100%;
  }
  #mapMov img{
    max-width:none!important;
    background:none!important;
  }

</style>
<div class="row">
  <input type="hidden" name="servicio" id="servicio" value="<?=$conductorId;?>">
	<div class="col-md-12">
	        <div class="box-body">
	          	<div style='overflow:hidden;height:100%;width:100%;'>
            		<div id='mapMov'></div>
          	  	</div>
	        </div><!-- /.box-body -->
	</div>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>   
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=<?=KEY_PANEL_GOOGLE?>"></script>
<script type="text/javascript" src="dist/js/constantes.js"></script>
<script src="<?=IP_SOCKET?>socket.io/socket.io.js"></script>
<script>

var map;
var idsConductor = [];
var marker = [];
var markers = new Array();
var datos = [];
var infowindow = [];
var socket = null;
var conductors;
var logoestado = '';

var servicioDireccion;
var directionsDisplay;
var conductor_id = $("#servicio").val();

$(function() {

  init_mapa();

    socket = io.connect(URL_CONTROLLER_SOCKET);
    socket.emit('dataMapConductorsWeball',{estado: 'all'});
    socket.on('listaConductorsall',function(result){
    conductors = result;
    console.log(conductors);
     for (var i = 0; i < result.length; i++) {
        if(result[i].conductor_id == conductor_id){
            datos = {
                Nombre:result[i].conductor_nombre, 
                Apellido:result[i].conductor_apellido, 
                Foto:result[i].conductor_foto, 
                Estado:result[i].conductor_estado,
                Celular:result[i].conductor_celular,
                Placa:result[i].unidad_placa,
                Marca:result[i].unidad_marca,
                Servicio:result[i].tiposervicio_nombre,
                Socket:result[i].conductor_socket
                };
            idsConductor.push(result[i].conductor_id);
            addMarker(result[i].conductor_id,result[i].conductor_lat,result[i].conductor_lng,map,datos);  
        } 
     }
    });

    socket.on('refreshCoordinates', function(data){

      if(data.idConductor == conductor_id){
        if(marker[data.idConductor]){
          console.log('Conductor existente :'+ data.idConductor);
          if(data.socket_conductor != null){
            switch(data.stateConductor) {
                case 1:
                  //marker[data.idConductor].setIcon('includes/img/carrito-verde.png');
                   marker[data.idConductor].setIcon('includes/img/ic_gaima.png');
                    break;
                case 2:
                  //marker[data.idConductor].setIcon('includes/img/carrito-naranja.png');
                   marker[data.idConductor].setIcon('includes/img/ic_gaima.png');
                    break;
            }
          }else{
            
            marker[data.idConductor].setIcon('includes/img/ic_gaima.png');
            //marker[data.idConductor].setIcon('includes/img/carrito-negro.png');

          }
            var newLatLng = new google.maps.LatLng(data.latitud, data.longitud);
            marker[data.idConductor].setPosition(newLatLng);
        }
        else
        { 

          datos = {
                  Nombre:data.nombre, 
                  Apellido:data.apellido, 
                  Foto:data.foto, 
                  Estado:data.stateConductor,
                  Celular:data.celular,
                  Placa:data.placa,
                  Marca:data.marca,
                  Servicio:data.servicio,
                  Socket:data.conductor_socket
                  };

              idsConductor.push(data.idConductor);
         
        }
      }
      
    });

});

function init_mapa() {

    directionsDisplay = new google.maps.DirectionsRenderer();
    servicioDireccion = new google.maps.DirectionsService();

    var latorigen = <?=$origenlat;?>;
    var lngorigen = <?=$origenlon?>;
    var latdestino = <?=$destinolat?>;
    var lngdestino = <?=$destinolon?>;

    var bangalore = {lat: <?=$origenlat;?>, lng: <?=$origenlon?>};
    map = new google.maps.Map(document.getElementById('mapMov'), {
      zoom: 14,
      center: bangalore,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var logoOrigen = 'includes/img/ic_cliente_origen.png';
    var imageOrigen = new google.maps.MarkerImage(logoOrigen,
        new google.maps.Size(42, 65),
        new google.maps.Point(0, 0),
        new google.maps.Point(17, 34),
        new google.maps.Size(42, 65)
        );
    var myLatLngOrigen = {lat: latorigen, lng: lngorigen};
    var markerOrigen = new google.maps.Marker({
      icon : imageOrigen,
      position: myLatLngOrigen,
      map: map
    });

    var logoDestino = 'includes/img/ic_cliente_destino.png';
    var imageDestino = new google.maps.MarkerImage(logoDestino,
        new google.maps.Size(42, 65),
        new google.maps.Point(0, 0),
        new google.maps.Point(17, 34),
        new google.maps.Size(42, 65));
    var myLatLngDestino = {lat: latdestino, lng: lngdestino};
    var markerDestino = new google.maps.Marker({
      icon : imageDestino,
      position: myLatLngDestino,
      map: map
    });
    /*var inicio = new google.maps.LatLng(parseFloat(latorigen),parseFloat(lngorigen));
    var fin = new google.maps.LatLng(parseFloat(latdestino),parseFloat(lngdestino));

    var peticion = {
	    origin:inicio,
	    destination:fin,
	    travelMode: google.maps.DirectionsTravelMode.DRIVING,
	  };

	 servicioDireccion.route(peticion, function(response, status) {

		if (status === google.maps.DirectionsStatus.OK) {
		  directionsDisplay.setDirections(response);
		} else {
		  window.alert('Directions request failed due to ' + status);
		}

	});

	directionsDisplay.setMap(map);*/
}

function addMarker(idConductor, lat, lng, map, datos) {

      var latitud = parseFloat(lat);
        var longitud = parseFloat(lng);
        if(datos.Socket != null)
        {
          switch(datos.Estado) {
          case 1:
              //logoestado = 'includes/img/carrito-verde.png';
              logoestado = 'includes/img/ic_gaima.png';
              break;
          case 2:
              //logoestado = 'includes/img/carrito-naranja.png';
              logoestado = 'includes/img/ic_gaima.png';
              break;
      }
        } 
        else
        {
          //logoestado = 'includes/img/carrito-negro.png';
          logoestado = 'includes/img/ic_gaima.png';
        }

      image = new google.maps.MarkerImage(logoestado,
        new google.maps.Size(50, 50),
        new google.maps.Point(1,1),
        new google.maps.Point(1, 1)
    );

        marker[idConductor] = new google.maps.Marker({
        icon : image,
        position: {lat: latitud, lng: longitud},
        map: map
      });

      markers.push(marker[idConductor]);
        marker[idConductor].setMap(map);

         var contentString = '<div id="content">'+
                  '<div id="siteNotice">'+
                  '</div>'+
                  '<p class="firstHeading"><b>'+datos.Nombre+' '+datos.Apellido+'</b></p>'+
                  '<div id="bodyContent">'+
                  '<div style="width:100%;display: inline-block;">'+
                  '<div style="float:left;margin-right: 15px;">'+
                  '<img src="BL/'+datos.Foto+'" class="img-thumbnail" width="100" height="100">'+
                  '</div>'+
                  '<div style="float:right">'+
                  '<lalel><b>Nro.Placa : </b>'+datos.Placa+'</label><br>'+
                  '<lalel><b>Marca : </b>'+datos.Marca+'</label><br>'+
                  '<lalel><b>Telefono : </b>'+datos.Celular+'</label><br>'+
                  '<lalel><b>Servicio : </b>'+decodeURIComponent(escape(datos.Servicio))+'</label>'+
                  '</div>'+ 
                  '</div>'+
                  '</div>'+
                  '</div>';
    infowindow[idConductor] = new google.maps.InfoWindow({
        content: contentString
    });

        marker[idConductor].addListener('click', function() {
        infowindow[idConductor].open(map, marker[idConductor]);
      });
}
</script>

