<?php

//var_dump(date("Y-m-d H:i:s"));exit;

require 'lib/PHPMailer/PHPMailerAutoload.php';
require 'DAL/reservaDAO.php';
require 'DAL/constantes.php';

$reserva_id = $_REQUEST['reserva_id'];
$reservaDAO = new reservaDAO();

$resulSet = $reservaDAO->listaReservaId($reserva_id);

if(count($resulSet)>0){
	$resulSet = $resulSet[0];

	$Address = $resulSet['cliente_correo'];

	$Nombre= $resulSet['cliente_nombre'];

	$body = "<center><table border='0', cellpadding='0', cellspacing='0', align='center'>
					  <tr>
					    <td colspan='7' style='text-align:center;'>				    	
							<img src='".URL_CORREO."includes/img/logo_hammer.png' class='img-responsive' alt=''/>
							<p  style='display: inline-block;text-align: justify;'><b>GRACIAS POR UTILIZAR Y CONFIAR EN TAXI HUB</b><br>
							<b>APLICACION TOTALMENTE PERUANA. SIGUE UTILIZANDO LA</b><br>
							<b>APLICACION, SE VIENEN GRANDES SORPRESAS.</b> </p>
					    </td>
					  </tr>
					  <tr>
					  <td colspan='7' style='text-align:center;'>				    	
							<p>ESTIMADO PASAJERO SU RESERVA HA SIDO RECEPCIONADA.
						LOS DATOS DE SU RESERVA SON LOS SIGUIENTES:</p>
					    </td>
 					  <tr>
					    <td colspan='5' width='290', height='57'>Desde : <b>".NAME_EMPRESA."</b> <br>Asunto : Nueva Reserva</td>
					  </tr>
					  <tr>
					    <td colspan='5' width='290' height='40'>El pasajero <b>".utf8_decode($resulSet['cliente_nombre'])."</b> acaba de iniciar una reserva en <b>".NAME_EMPRESA."</b>.</td>
					  </tr>
					  <tr>
					    <td width='181' height='38'> Tarifa : <b> S/.".intval($resulSet['tarifa'])." </b> </td>
					  </tr>

					  <tr>
					    <td width='181' height='38'> Origen : <b>".utf8_decode($resulSet['dir_origen'])." </b><br> Destino : <b>".utf8_decode($resulSet['dir_destino'])."</b> </td>
					  </tr>
					  <tr>
					  	<td width='181' height='34'>Dia : <b>".$resulSet['reserva_fecha']."</b> </td>
					  </tr>
					  <tr>
					  	<td width='181'><h3>Datos del Conductor</h3></td>
					  </tr>
					  
					  <tr>
					    <td width='181' height='49'>Conductor : <b>".utf8_decode($resulSet['conductor_nombre'])."</b></td>
					    <td width='181' height='49'>Telefono : <b>".utf8_decode($resulSet['conductor_celular'])."</b></td>
					  </tr>
					  <tr>
					  	<td>
					  	 <center>
					  		<img src='".URL_CORREO."BL/".$resulSet['conductor_foto']."' class='img-responsive' alt='' width='100'/>
					  		</center>
					  	</td>
					  </tr>

					  <tr>
					  	<td width='181'><h3>".utf8_decode("Datos del vehículo")."</h3></td>
					  </tr>
					   <tr>
					     <td colspan='5' width='290', height='57'>Placa : <b>".utf8_decode($resulSet['unidad_placa'])."</b> <br>Marca :  <b>".utf8_decode($resulSet['unidad_marca'])."</b></td>
					     <td colspan='5' width='290', height='57'>Modelo : <b>".utf8_decode($resulSet['unidad_modelo'])."</b> <br>Color : <b>".utf8_decode($resulSet['unidad_color'])."</b></td>
					  </tr>
					  <tr>
					  	<td>
					  	<center>
					  		<img src='".URL_CORREO."BL/".$resulSet['unidad_foto']."' class='img-responsive' alt='' width='100'/>
					  		</center>
					  	</td>
					  </tr>

					  
					</table></center>";




}else{
	echo 'no';
	exit;
}


$mail = new PHPMailer;
//$mail->SMTPDebug = 2;                               // Enable verbose debug output
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = HOST;//'smtp.live.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = USERNAME;                 // SMTP username
$mail->Password = PASSWORD;
$mail->SMTPSecure = SMTPSECURE; 
$mail->Port = PORT;  
$mail->setFrom(FROM, NAME_EMPRESA);
//$mail->addAddress('shuanay@appslovers.com', 'Saul'); 
$mail->addAddress($Address, $Nombre);
$mail->isHTML(true);  
                               // Set email format to HTML
$mail->Subject = utf8_decode('Informacion de Reserva: '.$resulSet['cliente_nombre']);
//$mail->Subject = 'Informaci&oacute;n de Servicio:';
$mail->Body    = $body;
$mail->AltBody = URL_CORREO;




if(!$mail->send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
   echo 'Message has been sent';
}
