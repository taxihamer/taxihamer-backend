<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once'../DAL/apisDAO.php';

class apisController{

	public function insertGrupo($request){

		$data['api_google_web'] = $request[0]->api_google_web;
		$data['api_google_android'] = $request[0]->api_google_android;
		$data['api_google_ios'] = $request[0]->api_google_ios;
		$data['api_foursquare_key'] = $request[0]->api_foursquare_key;
		$data['api_foursquare_secret'] = $request[0]->api_foursquare_secret;
		$data['api_here_key'] = $request[0]->api_here_key;
		$data['api_here_secret'] = $request[0]->api_here_secret;
		$data['api_fecha_create'] = $request[0]->api_fecha_create;
		$data['api_fecha_fin'] = $request[0]->api_fecha_end;


		$apisDAO = new apisDAO();
		$save = $apisDAO->insertarApis($data);

		if($save['status'] == 1){
			$save = true;
		}
		return $save;
	}

	public function CountPage($pagenum, $pagesize, $offset){

		$apisDAO = new apisDAO();
		$count = $apisDAO->CountApis();

		return $count;

	}

}

session_start();
$controller = new apisController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->saveApis)){

	$data = $request->send;
	$insert = $controller->insertGrupo($data);
	if($insert != true){
		$json['status'] = false;
		$json['mensaje'] = 'Error al Guardar';
		$json['type'] = 2;
	}else{
		$json['status'] = true;
		$json['mensaje'] = 'Registro con exito';
		$json['type'] = 1;
	}

	header('Content-Type: application/json');
	echo json_encode($json);
}

if(isset($request->lista_apis)){

	$pagenum = $request->page;
	$pagesize = $request->size;
	$offset = ($pagenum - 1) * $pagesize;
	$search = $request->search;

	$count = $controller->CountPage($pagenum, $pagesize, $offset);
	$count_new = $count[0]['total'];

	$apisDAO = new apisDAO();
	$Res = $apisDAO->listaPaginacion($offset, $pagesize);

	$myData = array('Request' => $Res, 'totalCount' => $count_new);
	header('Content-Type: application/json');
	echo json_encode($myData);
}

if(isset($request->estado)){
	
	$data['api_id'] = $request->api;
	$data['api_estado'] = (!empty($request->switch))?$request->switch:0;
	$apisDAO = new apisDAO();

	if($data['api_estado'] == 1){
		$updateAll = $apisDAO->updateAll(0);
		$updateFind = $apisDAO->updateFind($data);
		$json['status'] = true;
		$json['msg'] = 'Actualizacion con exito';
	}else{
		$json['status'] = false;
		$json['msg'] = 'No puede actualizar al menos 1 fila tiene que ser activado';
	}

	header('Content-Type: application/json');
	echo json_encode($json);
}