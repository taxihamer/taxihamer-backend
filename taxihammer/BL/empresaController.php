<?php
require_once'../DAL/empresaDAO.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class empresaController{

	public function listarempresa(){

		$empresaDAO = new empresaDAO();
		$res = $empresaDAO->listempresa();
		return $res;
	}

	public function ingresarEmpresa($codigo,$razonsocial,$ruc,$telefono,$correo,$contacto,$direccion){
		$data['empresa_codigo'] = $codigo;
		$data['empresa_razonsocial'] = $razonsocial;
		$data['empresa_ruc'] = $ruc;
		$data['empresa_telefono'] = $telefono;
		$data['empresa_correo'] = $correo;
		$data['empresa_contacto'] = $contacto;
		$data['empresa_direccion'] = $direccion;

		$empresaDAO = new empresaDAO();
		$result = $empresaDAO->insertarEmpresa($data);
		return $result;
	}

	public function actualizarEmpresa($id,$codigo,$razonsocial,$ruc,$telefono,$correo,$contacto,$direccion){
		$data['empresa_codigo'] = $codigo;
		$data['empresa_razonsocial'] = $razonsocial;
		$data['empresa_ruc'] = $ruc;
		$data['empresa_telefono'] = $telefono;
		$data['empresa_correo'] = $correo;
		$data['empresa_contacto'] = $contacto;
		$data['empresa_id'] = $id;
		$data['empresa_direccion'] = $direccion;

		$empresaDAO = new empresaDAO();
		$result = $empresaDAO->updateEmpresa($data);
		return $result;
	}

	public function eliminarEmpresas($id){
		$data['empresa_id'] = $id;
		$empresaDAO = new empresaDAO();
		$result = $empresaDAO->deleteEmpresa($data);
		return $result;
	}

	public function ListarData($pagenum, $pagesize, $offset, $text){
		
		$empresaDAO = new empresaDAO();
		$count = $empresaDAO->CountEmpresas($text);
		return $count;
	}

}

session_start();
$controller = new empresaController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->list) && $request->list == 'lista_empresas'){

	$pagenum = $request->variables->page;
	$pagesize = $request->variables->size;
	$offset = ($pagenum - 1) * $pagesize;
	$searchText = $request->variables->search;

	//$tipoUsuario = (isset($request->variables->select))?(int)$request->variables->select:0;

	$count = $controller->ListarData($pagenum, $pagesize, $offset, $searchText);
	$count_new = $count[0]['total'];

	$empresaDAO = new empresaDAO();
	$Res = $empresaDAO->listaPaginacion($offset, $pagesize, $searchText);

	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);
}

if(isset($_REQUEST['delete_general'])){

	$res = $controller->eliminarEmpresas($_REQUEST['Gid']);
	$json['status'] = true;
	header('Content-Type: application/json');
   	echo json_encode($json);
}

if(isset($request->list) && $request->list == 'editado_empresa'){

	$empresa_id = (int)$request->variables->id;
	$empresaDAO = new empresaDAO();
	$Res = $empresaDAO->listempresaBy($empresa_id);
	header('Content-Type: application/json');
	echo json_encode($Res[0]);
}

if(isset($request->list) && $request->list == 'insert_empresas'){

     $codigo = $request->variables->data->empre->codigo;
     $razon_social = $request->variables->data->empre->rzonsocial;
     $ruc = (int)$request->variables->data->empre->ruc;
     $direccion = $request->variables->data->empre->direccion;
     $telefono = $request->variables->data->empre->telefono;
     $email = $request->variables->data->empre->email;
     $contacto = $request->variables->data->empre->contacto;

     if(!empty($ruc) && strlen($ruc) == '11'){
     	$empresaDAO = new empresaDAO();
		$Res = $empresaDAO->validarRuc($ruc);
		if($Res[0]['total'] == 0){
			$res = $controller->ingresarEmpresa($codigo,$razon_social,$ruc,$telefono,$email,$contacto,$direccion);
			$json['status'] = true;
			$json['msg'] = 'Su registro fue guardado exitosamente.';
		}else{
			$json['status'] = false;
			$json['msg'] = 'El numero de Ruc ya existe.';
		}
     }else{
     	$json['status'] = false;
     	$json['msg'] = 'Ingresar el numero de Ruc correctamente.';
     }

    header('Content-Type: application/json');
   	echo json_encode($json);
}

if(isset($_REQUEST['hidden_tipo_select'])){

	if(!empty($_REQUEST['id'])){
		$res = $controller->listarempresa();
		if($res == true){
			header('Content-Type: application/json');
            echo json_encode($res);
		}else{
			$res = false;
			header('Content-Type: application/json');
            echo json_encode($res);
		}
	}else{
		header("location:../index.php?seccion=clientes&status=error");
	}
}

if(isset($_REQUEST['hidden_empresa_update'])){
	if(!empty($_REQUEST['razonsocialedit']) && !empty($_REQUEST['codigoedit']) && !empty($_REQUEST['rucedit']) && !empty($_REQUEST['empid'])){
		$res = $controller->actualizarEmpresa($_REQUEST['empid'],$_REQUEST['codigoedit'],$_REQUEST['razonsocialedit'],$_REQUEST['rucedit'],$_REQUEST['telefonoedit'],$_REQUEST['correoedit'],$_REQUEST['contactoedit'],$_REQUEST['direccionedit']);
		if($res == true){
			header("location:../index.php?seccion=empresas&status=true");
		}else{
			header("location:../index.php?seccion=empresas&status=error");
		}
	}else{
		header("location:../index.php?seccion=empresas&status=error");
	}
}

