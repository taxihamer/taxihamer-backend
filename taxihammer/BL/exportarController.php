<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once'../DAL/vehiculoDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/servicioDAO.php';
require_once'../DAL/tarifarioDAO.php';
require_once'../DAL/lugaresDAO.php';
require_once'../DAL/clientesDAO.php';
require_once'../DAL/empresaDAO.php';
require_once'../DAL/estadocuentaDAO.php';


require_once'../lib/PHPExcel/PHPExcel.php';


$lugaresDAO = new lugaresDAO();
$result_lugares = $lugaresDAO->lista();


$empresaDAO = new empresaDAO();
$result_empresa = $empresaDAO->listempresa();

class exportarController{

}

date_default_timezone_set("America/Lima");

if (isset($_REQUEST['hidden_exportar'])) {

	$objPHPExcel = new PHPExcel();
    PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

	$flag = '';
	$title = '';
	if($_REQUEST['exportar'] == 1){
		$flag = 1;
		$vehiculoDAO = new vehiculoDAO();
		$lista = $vehiculoDAO->lista();
		$title = 'Vehiculos';
	}elseif ($_REQUEST['exportar'] == 2) {
		$flag = 2;
		$conductorDAO = new conductorDAO();
		$lista = $conductorDAO->lista();
		$title = 'Conductores';
	}elseif ($_REQUEST['exportar'] == 3) {
		$flag = 3;
		$servicioDAO = new servicioDAO();
		$lista = $servicioDAO->listMovimientos();
		$title = 'Servicios';
	}elseif ($_REQUEST['exportar'] == 4){
        $flag = 4;
        $tarifarioDAO = new tarifarioDAO();
        $lista = $tarifarioDAO->listaPaginacionExportar();
        $title = 'Tarifas';
    }elseif ($_REQUEST['exportar'] == 7){
        $flag = 7;
        $estadocuentaDAO =  new estadocuentaDAO();
        $search = $_REQUEST['hsearch'];
        $title = 'Estado de Cuenta';
        $lista = $estadocuentaDAO->exportarEstadoCuenta($search);
    }else{
        $flag = 5;
        $clientesDAO = new clientesDAO();
        $lista = $clientesDAO->lista();
        $title = 'Clientes';
    }

	if(PHP_SAPI == '')
		die('Este archivo solo se puede ver desde un navegador web');

	// Se asignan las propiedades del libro
		$objPHPExcel->getProperties()->setCreator("SaulHuanay") //Autor
							 ->setLastModifiedBy("SaulHuanay") //Ultimo usuario que lo modificó
							 ->setTitle("Reporte Excel con PHP y MySQL")
							 ->setSubject("Reporte Excel con PHP y MySQL")
							 ->setDescription("Reporte de ".$title)
							 ->setKeywords("reporte ".$title)
							 ->setCategory("Reporte excel");

		$tituloReporte = "Relación de ".$title;
		if($flag == 1){
			$titulosColumnas = array('Codigo Unidad', 'Servicio', 'Alias', 'Nro Placa', 'Marca' , 'Modelo' , 'Color', 'Año');	
		}else if ($flag == 2) {
			$titulosColumnas = array('Codigo Conductor', 'Unidad Alias', 'Nombre', 'Apellido', 'Celular' , 'Dni' , 'Correo', 'Password', 'Fecha Registro');
		}else if ($flag == 3){
			$titulosColumnas = array('Codigo Servicio', 'Nombre Conductor', 'Nombre Cliente', 'Tipo Cliente', 'Origen' , 'Destino' , 'Tarifa', 'Adicionales', 'Tarifa Final', 'Tipo Pago', 'Fecha Servicio', 'Hora Inicio', 'Hora de Fin');
		}else if ($flag == 4){
            $titulosColumnas = array('Codigo de Tarifa ', 'Tipo Cliente', 'Origen', 'Destino', 'Tipo de Servicio', 'Monto', 'Estado');
        }else if ($flag == 7){
            $titulosColumnas = array('Conductor', 'Unidad', 'Dni', 'Correo', 'Celular', 'Saldo', 'Tipo Movimiento', 'Fecha', 'Tipo Pago','Monto');
        }else{
            $titulosColumnas = array('Codigo Cliente', 'Nombre', 'Apellido', 'Correo', 'Celular' , 'Tipo de Cliente' , 'Calificacion', 'Empresa');
        }

		
		

		$objPHPExcel->setActiveSheetIndex(0)
        		    ->mergeCells('A1:J1');

		// Se agregan los titulos del reporte 
        if($flag != 3){
            if($flag != 4){
                if($flag != 2){
                    if($flag != 7){
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1',$tituloReporte)
                        ->setCellValue('A3',  $titulosColumnas[0])
                        ->setCellValue('B3',  $titulosColumnas[1])
                        ->setCellValue('C3',  $titulosColumnas[2])
                        ->setCellValue('D3',  $titulosColumnas[3])
                        ->setCellValue('E3',  $titulosColumnas[4])
                        ->setCellValue('F3',  $titulosColumnas[5])
                        ->setCellValue('G3',  $titulosColumnas[6])
                        ->setCellValue('H3',  $titulosColumnas[7]); 
                    }else{
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1',$tituloReporte)
                        ->setCellValue('A3',  $titulosColumnas[0])
                        ->setCellValue('B3',  $titulosColumnas[1])
                        ->setCellValue('C3',  $titulosColumnas[2])
                        ->setCellValue('D3',  $titulosColumnas[3])
                        ->setCellValue('E3',  $titulosColumnas[4])
                        ->setCellValue('F3',  $titulosColumnas[5])
                        ->setCellValue('G3',  $titulosColumnas[6])
                        ->setCellValue('H3',  $titulosColumnas[7])
                        ->setCellValue('I3',  $titulosColumnas[8])
                        ->setCellValue('J3',  $titulosColumnas[9]);
                    }
                }else{
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1',$tituloReporte)
                    ->setCellValue('A3',  $titulosColumnas[0])
                    ->setCellValue('B3',  $titulosColumnas[1])
                    ->setCellValue('C3',  $titulosColumnas[2])
                    ->setCellValue('D3',  $titulosColumnas[3])
                    ->setCellValue('E3',  $titulosColumnas[4])
                    ->setCellValue('F3',  $titulosColumnas[5])
                    ->setCellValue('G3',  $titulosColumnas[6])
                    ->setCellValue('H3',  $titulosColumnas[7])
                    ->setCellValue('I3',  $titulosColumnas[8]);
                }
            }else{
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1',$tituloReporte)
                    ->setCellValue('A3',  $titulosColumnas[0])
                    ->setCellValue('B3',  $titulosColumnas[1])
                    ->setCellValue('C3',  $titulosColumnas[2])
                    ->setCellValue('D3',  $titulosColumnas[3])
                    ->setCellValue('E3',  $titulosColumnas[4])
                    ->setCellValue('F3',  $titulosColumnas[5])
                    ->setCellValue('G3',  $titulosColumnas[6]);
            }
        }else{
           $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1',$tituloReporte)
                    ->setCellValue('A3',  $titulosColumnas[0])
                    ->setCellValue('B3',  $titulosColumnas[1])
                    ->setCellValue('C3',  $titulosColumnas[2])
                    ->setCellValue('D3',  $titulosColumnas[3])
                    ->setCellValue('E3',  $titulosColumnas[4])
                    ->setCellValue('F3',  $titulosColumnas[5])
                    ->setCellValue('G3',  $titulosColumnas[6])
                    ->setCellValue('H3',  $titulosColumnas[7])
                    ->setCellValue('I3',  $titulosColumnas[8])   
                    ->setCellValue('J3',  $titulosColumnas[9])   
                    ->setCellValue('K3',  $titulosColumnas[10])
                    ->setCellValue('L3',  $titulosColumnas[11])
                    ->setCellValue('M3',  $titulosColumnas[12]);       
        }
		
		$i = 4;
        // var_dump($lista);exit;
		foreach ($lista as $key => $rowlista) {

			if($flag == 1){

				$objPHPExcel->setActiveSheetIndex(0)
        		    ->setCellValue('A'.$i,  $rowlista['unidad_id'])
		            ->setCellValue('B'.$i,  $rowlista['tiposervicio_nombre'])
        		    ->setCellValue('C'.$i,  $rowlista['unidad_alias'])
            		->setCellValue('D'.$i, $rowlista['unidad_placa'])
            		->setCellValue('E'.$i, $rowlista['unidad_marca'])
            		->setCellValue('F'.$i, $rowlista['unidad_modelo'])
            		->setCellValue('G'.$i, $rowlista['unidad_color'])
            		->setCellValue('H'.$i, $rowlista['unidad_fabricacion']);

			}else if ($flag == 2) {

				$alias = (!empty($rowlista['unidad_alias']))?$rowlista['unidad_alias']:'No tiene unidad';
                $fecha = (!empty($rowlista['conductor_fecha']))?$rowlista['conductor_fecha']:'0000-00-00';

				$objPHPExcel->setActiveSheetIndex(0)
        		    ->setCellValue('A'.$i, $rowlista['conductor_id'])
        		   	->setCellValue('B'.$i, $alias)
            		->setCellValue('C'.$i, $rowlista['conductor_nombre'])
            		->setCellValue('D'.$i, $rowlista['conductor_apellido'])
            		->setCellValue('E'.$i, $rowlista['conductor_celular'])
            		->setCellValue('F'.$i, $rowlista['conductor_dni'])
            		->setCellValue('G'.$i, $rowlista['conductor_correo'])
            		->setCellValue('H'.$i, $rowlista['conductor_password'])
                    ->setCellValue('I'.$i, $fecha);
			
			}else if($flag == 3){
                //var_dump($rowlista);exit;
				$TipoPago = ($rowlista['tipo_pago']==1)?'Efectivo':'Vale';
                $adicionales = $rowlista['peaje']+$rowlista['parqueo']+$rowlista['espera']+$rowlista['parada'];
				$objPHPExcel->setActiveSheetIndex(0)
        		    ->setCellValue('A'.$i, $rowlista['servicio_id'])
        		   	->setCellValue('B'.$i, $rowlista['NombreConductor'])
            		->setCellValue('C'.$i, $rowlista['NombreCliente'])
            		->setCellValue('D'.$i, $rowlista['tipo_descripcion'])
            		->setCellValue('E'.$i, ($rowlista['origen_direccion']))
            		->setCellValue('F'.$i, ($rowlista['destino_nombre']))
            		->setCellValue('G'.$i, 'S/. '.number_format($rowlista['tarifa'], 2, '.', ''))
            		->setCellValue('H'.$i, 'S/. '.number_format($adicionales, 2, '.', ''))
                    ->setCellValue('I'.$i, 'S/. '. (number_format($adicionales, 2, '.', '') + number_format($rowlista['tarifa'], 2, '.', '')))
                    ->setCellValue('J'.$i, ($TipoPago.' '.$rowlista['servicio_vale']))
                    ->setCellValue('K'.$i, $rowlista['fecha_seleccion'])
                    ->setCellValue('L'.$i, $rowlista['Inicio'])
                    ->setCellValue('M'.$i, $rowlista['Fin']);

			}else if($flag == 4){

                foreach($result_lugares as $resu){
                    if($rowlista['origen_id'] == $resu['lugares_id']){
                           $org = $resu['lugar_descripcion'];
                    }
                }
                foreach($result_lugares as $resu){
                    if($rowlista['destino_id'] == $resu['lugares_id']){
                         $des = $resu['lugar_descripcion'];
                    }
                }
                $Estado = ($rowlista['tarifario_status']==1)?'Activo':'Inactivo';
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $rowlista['tarifario_id'])
                    ->setCellValue('B'.$i, $rowlista['empresa_razonsocial'])
                    ->setCellValue('C'.$i, $org)
                    ->setCellValue('D'.$i, $des)
                    ->setCellValue('E'.$i, $rowlista['tiposervicio_nombre'])
                    ->setCellValue('F'.$i, 'S/. '.number_format($rowlista['tarifario_monto'], 2, '.', ','))
                    ->setCellValue('G'.$i, $Estado);

            }else if($flag == 7){
                //var_dump($rowlista);exit;
                
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $rowlista['conductor'])
                    ->setCellValue('B'.$i, $rowlista['unidad'])
                    ->setCellValue('C'.$i, $rowlista['dni'])
                    ->setCellValue('D'.$i, $rowlista['correo'])
                    ->setCellValue('E'.$i, $rowlista['celular'])
                    ->setCellValue('F'.$i, 'S/. '.number_format($rowlista['saldo'], 2, '.', ','))
                    ->setCellValue('G'.$i, $rowlista['movimiento'])
                    ->setCellValue('H'.$i, $rowlista['fecha'])
                    ->setCellValue('I'.$i, $rowlista['tipo_pago'])
                    ->setCellValue('J'.$i, $rowlista['monto']);

            }else{
                $empresa = '';
                foreach($result_empresa as $resu){
                    if(!empty($rowlista['empresa_id'])){
                        if($rowlista['empresa_id'] == $resu['empresa_id']){
                           $empresa = $resu['empresa_razonsocial'];
                        }
                    }
                }
                 $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $rowlista['cliente_id'])
                    ->setCellValue('B'.$i, $rowlista['cliente_nombre'])
                    ->setCellValue('C'.$i, $rowlista['cliente_apellido'])
                    ->setCellValue('D'.$i, $rowlista['cliente_correo'])
                    ->setCellValue('E'.$i, $rowlista['cliente_celular'])
                    ->setCellValue('F'.$i, $rowlista['tipocliente_descripcion'])
                    ->setCellValue('G'.$i, intval($rowlista['cliente_calificacion']))
                    ->setCellValue('H'.$i, $empresa);
            }
			$i++;
		}

		$estiloTituloReporte = array(
        	'font' => array(
	        	'name'      => 'Verdana',
    	        'bold'      => true,
        	    'italic'    => false,
                'strike'    => false,
               	'size' =>16,
	            	'color'     => array(
    	            	'rgb' => 'FFFFFF'
        	       	)
            ),
	        'fill' => array(
				'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
				'color'	=> array('argb' => 'FF220835')
			),
            'borders' => array(
               	'allborders' => array(
                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
               	)
            ), 
            'alignment' =>  array(
        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        			'rotation'   => 0,
        			'wrap'          => TRUE
    		)
        );

		$estiloTituloColumnas = array(
            'font' => array(
                'name'      => 'Arial',
                'bold'      => true,                          
                'color'     => array(
                    'rgb' => 'FFFFFF'
                )
            ),
            'fill' 	=> array(
				'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
				'rotation'   => 90,
        		'startcolor' => array(
            		'rgb' => 'c47cf2'
        		),
        		'endcolor'   => array(
            		'argb' => 'FF431a5d'
        		)
			),
            'borders' => array(
            	'top'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '143860'
                    )
                ),
                'bottom'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '143860'
                    )
                )
            ),
			'alignment' =>  array(
        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        			'wrap'          => TRUE
    		));
			
		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray(
			array(
           		'font' => array(
               	'name'      => 'Arial',               
               	'color'     => array(
                   	'rgb' => '000000'
               	)
           	),
           	'fill' 	=> array(
				'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
				'color'		=> array('argb' => 'FFd9b7f4')
			),
           	'borders' => array(
               	'left'     => array(
                   	'style' => PHPExcel_Style_Border::BORDER_THIN ,
	                'color' => array(
    	            	'rgb' => '3a2a47'
                   	)
               	)             
           	)
        ));

		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($estiloTituloReporte);
        if($flag != 3){
            if($flag != 4){
                if($flag != 2){
                    if($flag != 7){
                        $objPHPExcel->getActiveSheet()->getStyle('A3:H3')->applyFromArray($estiloTituloColumnas);
                    }else{
                        $objPHPExcel->getActiveSheet()->getStyle('A3:J3')->applyFromArray($estiloTituloColumnas);
                    }
                }else{
                     $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->applyFromArray($estiloTituloColumnas);
                }
            }else{
                $objPHPExcel->getActiveSheet()->getStyle('A3:G3')->applyFromArray($estiloTituloColumnas);   
            }
        }else{
            $objPHPExcel->getActiveSheet()->getStyle('A3:M3')->applyFromArray($estiloTituloColumnas);   
        }
			
		$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:H".($i-1));

		// Se asigna el nombre a la hoja
		$objPHPExcel->getActiveSheet()->setTitle($title);

		// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
		$objPHPExcel->setActiveSheetIndex(0);
		// Inmovilizar paneles 
		//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
		$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,8);

		// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reportede"'.$title.'".xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;

}