<?php
require_once'../DAL/clientesDAO.php';
require_once'../DAL/alertaDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/vehiculoDAO.php';
require_once'../DAL/servicioDAO.php';

class alertaController{

	public function UpdateCheck($data){

		$alertaDAO = new alertaDAO();
		$result = $alertaDAO->updaChek($data);
		return $result;
		
	}
}

session_start();
$controller = new alertaController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->listar_alerta)){
	
	$alertaDAO = new alertaDAO();
	$clientesDAO = new clientesDAO(); 
	$conductorDAO = new conductorDAO();
	$vehiculoDAO = new vehiculoDAO();
	$servicioDAO = new servicioDAO();
	$Res = $alertaDAO->listaAlert();

	
	foreach ($Res as $key => $alertarow) {

		$Res[$key]['detalle'] = ($alertarow['alerta_ckeck'] == 1)?'Revisado':'Pendiente';
		if($alertarow['alerta_tipo'] == 1){
			$cliente = $clientesDAO->listaClienteId($alertarow['alerta_personaid']);
			$Res[$key]['alertatipoid'] = 'Cliente';
			$Res[$key]['alerta_personaid'] = $cliente[0]['cliente_nombre'].' '.$cliente[0]['cliente_apellido'];
			$Res[$key]['alerta_unidadid'] = 'Ninguna Unidad';
		}else if($alertarow['alerta_tipo'] == 2) {
			$conductor = $conductorDAO->listaConductorId($alertarow['alerta_personaid']);
			$vehiculo = $vehiculoDAO->listaBy($alertarow['alerta_unidadid']);
			$Res[$key]['alertatipoid'] = 'Conductor';
			$Res[$key]['alerta_personaid'] = $conductor[0]['conductor_nombre'].' '.$conductor[0]['conductor_apellido'];
			$Res[$key]['alerta_unidadid'] = $vehiculo[0]['unidad_marca'].' '.$vehiculo[0]['unidad_modelo'].' '.$vehiculo[0]['unidad_placa'];
		}else{

			$vehiculo = $vehiculoDAO->listaBy($alertarow['alerta_unidadid']);
			$servicio = $servicioDAO->listaServicio($alertarow['alerta_personaid']);
			$flag = '';
			if($alertarow['alerta_flag'] == 1){
				$cliente = $clientesDAO->listaClienteId($servicio[0]['cliente_id']);
				$Res[$key]['alerta_personaid'] = $cliente[0]['cliente_nombre'].' '.$cliente[0]['cliente_apellido'];
				$flag = 'Cliente';
			}else{
				$conductor = $conductorDAO->listaConductorId($servicio[0]['conductor_id']);
				$Res[$key]['alerta_personaid'] = $conductor[0]['conductor_nombre'].' '.$conductor[0]['conductor_apellido'];
				$flag = 'Conductor';
			}
			$Res[$key]['alertatipoid'] = $flag.'(S)';
			$Res[$key]['alerta_unidadid'] = $vehiculo[0]['unidad_marca'].' '.$vehiculo[0]['unidad_modelo'].' '.$vehiculo[0]['unidad_placa'];

		}
	}

	header('Content-Type: application/json');
	echo json_encode($Res);
}

if(isset($request->hidden_toggle_alert)){
	
	$data['alerta_id']=$request->AlertID;
	$data['alerta_ckeck']=$request->status;
	$Res = $controller->UpdateCheck($data);
	header('Content-Type: application/json');
	echo json_encode($Res);
}

if(isset($request->hidden_toggle_modal)){
	$alerta_id = $request->AlertID;
	$alerta_tipo = $request->AlertTy;
	header('Content-Type: application/json');
	if(!empty($alerta_id) && !empty($alerta_tipo)){

		$alertaDAO = new alertaDAO();
		$clientesDAO = new clientesDAO(); 
		$conductorDAO = new conductorDAO();
		$vehiculoDAO = new vehiculoDAO();
		$servicioDAO = new servicioDAO();

		$alert = $alertaDAO->listaAlertId($alerta_id);

		if ($alerta_tipo == 1) {//Cliente
			$Res = $clientesDAO->listaClienteId($alert[0]['alerta_personaid']);
			$Res[0]['T'] = 1;
		} elseif ($alerta_tipo == 2) {//Conductor
			$Res = $conductorDAO->listaConductorId($alert[0]['alerta_personaid']);
			$Res[0]['T'] = 2;
		} else {//Servicio
			
			$servicio = $servicioDAO->listaServicio($alert[0]['alerta_personaid']);
			$cliente = $clientesDAO->listaClienteId($servicio[0]['cliente_id']);
			$conductor = $conductorDAO->listaConductorId($servicio[0]['conductor_id']);

			$Res[0]['cliente_nombre'] = $cliente[0]['cliente_nombre'];
			$Res[0]['conductor_nombre'] = $conductor[0]['conductor_nombre']; 
			$Res[0]['cliente_apellido'] = $cliente[0]['cliente_apellido']; 
			$Res[0]['conductor_apellido'] = $conductor[0]['conductor_apellido']; 
			$Res[0]['cliente_celular'] = $cliente[0]['cliente_celular']; 
			$Res[0]['conductor_celular'] = $conductor[0]['conductor_celular'];  
			$Res[0]['cliente_correo'] = $cliente[0]['cliente_correo']; 
			$Res[0]['conductor_correo'] = $conductor[0]['conductor_correo']; 
			$Res[0]['T'] = 3;
		}

		//echo '<pre>'; print_r($Res); echo '</pre>'; exit;
		$Res['status'] = true; 
		echo json_encode($Res[0]);

	}else{
		$Res['status'] = false; 
		echo json_encode($Res);
	}
}

if(isset($_REQUEST['hidden_data_verif'])){

	$alerta_id = $_REQUEST['codigo'];
	$tipo = $_REQUEST['tipo'];
	//echo '<pre>'; print_r($alerta_id); echo '</pre>'; exit;
	$alertaDAO = new alertaDAO();

	if($tipo == 1){
		$resul = $alertaDAO->listaByVerifi($alerta_id);
		if(!empty($resul)){
			$rest['status'] = $resul[0]['alerta_click'];
		}
	}else{
		
		$resul = $alertaDAO->actualizarClick($alerta_id);
		if($resul == true){
			$rest['status'] = 1;
		}else{
			$rest['status'] = 0;
		}
	}

	header('Content-Type: application/json');
	echo json_encode($rest);
}