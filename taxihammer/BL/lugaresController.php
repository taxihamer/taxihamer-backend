<?php
require_once '../DAL/lugaresDAO.php';

class lugaresController{

	public function agregarLugar($data){

		$dato['lugar_coordpolygono'] = array_filter($data['coord_polygono']);
		$dato['lugar_lat'] = $data['centro_polygono'][0];
		$dato['lugar_lon'] = $data['centro_polygono'][1];
		$dato['lugar_descripcion'] = $data['lugar_descripcion'];

		$lugaresDAO = new lugaresDAO();
		$result = $lugaresDAO->insertarLugar($dato);
		return $result;
	}

	public function eliminarLugar($id){
		$data['lugares_id'] = $id;
		$lugaresDAO = new lugaresDAO();
		$res = $lugaresDAO->deleteLugar($data);
		
		if($res == true){

			$mod = $lugaresDAO->maxlugar();
			$cantidad = $mod[0]['lugar_mod'] + 1;

			$datos['lugares_id'] = $data['lugares_id'];
			$datos['lugar_mod'] = $cantidad;

			$result = $lugaresDAO->updateLugarmax($datos);
		}

		return $result;
	}

	public function actualizarLugar($data){

		$dato['lugar_coordpolygono'] = array_filter($data['coord_polygono']);
		$dato['lugares_id'] = $data['lugares_id'];

		$lugaresDAO = new lugaresDAO();
		$res = $lugaresDAO->updateLugar($dato);

		if($res == true){

			$mod = $lugaresDAO->maxlugar();
			$cantidad = $mod[0]['lugar_mod'] + 1;

			$datos['lugares_id'] = $data['lugares_id'];
			$datos['lugar_mod'] = $cantidad;

			$result = $lugaresDAO->updateLugarmax($datos);

		}

		return $result;
	}
}

session_start();
$controller = new lugaresController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
/*-----------------ACTUALIZAR NOMBRE LUGAR-------------*/
if(isset($_REQUEST['update_nombre_zona'])){
	$lugaresDAO = new lugaresDAO();
	$data['lugares_id'] = trim($_REQUEST['idlugar']);
	$data['lugar_descripcion'] = trim($_REQUEST['descrip']);
	$res = $lugaresDAO->actualizarLugarnombre($data);
		if($res == true){
			header('Content-Type: application/json');
	        echo json_encode($res);
		}else{
			$res = false;
			header('Content-Type: application/json');
	        echo json_encode($res);
		}
}

/*-----------------INSERTAR POR AJAX-------------------*/
if(isset($_REQUEST['zona'])){

	$data['lugar_descripcion'] = $_REQUEST['nombre'];
	$data['coord_polygono'] = $_REQUEST['coordenadas'];
	$data['centro_polygono'] = $_REQUEST['centercoord'];
	$res = $controller->agregarLugar($data);
		if($res == true){
			header('Content-Type: application/json');
	        echo json_encode($res);
		}else{
			$res = false;
			header('Content-Type: application/json');
	        echo json_encode($res);
		}
}
/*----------------ACTUALIZAR LUGAR--------------------*/
if(isset($_REQUEST['edit'])){

	$data['lugares_id'] = $_REQUEST['idlugar'];
	$data['coord_polygono'] = $_REQUEST['coordenadas'];
	$res = $controller->actualizarLugar($data);
		if($res == true){
			header('Content-Type: application/json');
	        echo json_encode($res);
		}else{
			$res = false;
			header('Content-Type: application/json');
	        echo json_encode($res);
		}
}

/*----------------ELIMINAR LUGAR----------------------*/
if(isset($_REQUEST['hidden_delete_lugar'])){
	$res = $controller->eliminarLugar($_REQUEST['id']);
	if($res == true){
		header("location:../index.php?seccion=listazona&status=true");
	}else{
		header("location:../index.php?seccion=listazona&status=false");
	}
}
/*---------------- BUSQUEDA COORDENADAS POR ID QUE EXISTE -----------------------------*/
if(isset($_REQUEST['listar_in'])){

	$lugaresDAO = new lugaresDAO();
	$res = $lugaresDAO->listabyin($_REQUEST['idlugar']);

	foreach ($res as $key => $row) {

		$res[$key]['lugar_coordpolygono'] = json_decode($row['lugar_coordpolygono']);
		$a = str_replace("(", "", json_decode($row['lugar_coordpolygono']));
		$c = str_replace(")", "", $a);

		$res[$key]['lugar_coordpolygono'] = $c;
		foreach ($c as $k => $exp) {
			$explode = explode(',', $exp);
			$res[$key]['new'][] = $explode;
		}

		$i = 0;
		foreach ($res[$key]['new'] as $ky => $indice) {
				$res[$key]['new'][$i]['lat'] = $res[$key]['new'][$i][0];
				$res[$key]['new'][$i]['lng'] = $res[$key]['new'][$i][1];
				unset($res[$key]['new'][$i][0]);
				unset($res[$key]['new'][$i][1]);
			$i++;
		}
	}

	header('Content-Type: application/json');
    echo json_encode($res);
}

/*------------------ BSQUEDA COORDENADAS POR ID QUE NO EXISTE Y SIN ID ----------------*/
if(isset($_REQUEST['listar'])){

	$lugaresDAO = new lugaresDAO();
	if($_REQUEST['idlugar'] == '-1'){
		$res = $lugaresDAO->lista();
	}else{
		$res = $lugaresDAO->listabyon($_REQUEST['idlugar']);
	}
	

	foreach ($res as $key => $row) {

		$res[$key]['lugar_coordpolygono'] = json_decode($row['lugar_coordpolygono']);
		$a = str_replace("(", "", json_decode($row['lugar_coordpolygono']));
		$c = str_replace(")", "", $a);

		$res[$key]['lugar_coordpolygono'] = $c;
		foreach ($c as $k => $exp) {
			$explode = explode(',', $exp);
			$res[$key]['new'][] = $explode;
		}

		$i = 0;
		foreach ($res[$key]['new'] as $ky => $indice) {
				$res[$key]['new'][$i]['lat'] = $res[$key]['new'][$i][0];
				$res[$key]['new'][$i]['lng'] = $res[$key]['new'][$i][1];
				unset($res[$key]['new'][$i][0]);
				unset($res[$key]['new'][$i][1]);
			$i++;
		}
	}

	header('Content-Type: application/json');
    echo json_encode($res);
}

/*-------------------- BUSQUEDA TODAS LAS ZONA CREADAS -----------------------*/
if(isset($_REQUEST['hidden_lugar_map']) || isset($request->hidden_lugar_map)){
	$lugaresDAO = new lugaresDAO();
	$res = $lugaresDAO->lista();

	foreach ($res as $key => $row) {

		$res[$key]['lugar_coordpolygono'] = json_decode($row['lugar_coordpolygono']);
		$a = str_replace("(", "", json_decode($row['lugar_coordpolygono']));
		$c = str_replace(")", "", $a);

		$res[$key]['lugar_coordpolygono'] = $c;
		foreach ($c as $k => $exp) {
			$explode = explode(',', $exp);
			$res[$key]['new'][] = $explode;
		}

		$i = 0;
		foreach ($res[$key]['new'] as $ky => $indice) {
				$res[$key]['new'][$i]['lat'] = $res[$key]['new'][$i][0];
				$res[$key]['new'][$i]['lng'] = $res[$key]['new'][$i][1];
				unset($res[$key]['new'][$i][0]);
				unset($res[$key]['new'][$i][1]);
			$i++;
		}
	}
	header('Content-Type: application/json');
	echo json_encode($res);
}