<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once'../DAL/servicioDAO.php';
require_once'../DAL/lugaresDAO.php';
require_once'../DAL/clientesDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/vehiculoDAO.php';
require_once '../DAL/calificacionDAO.php';
require_once '../DAL/constantes.php';

class movimientosController{
}


function utf8array(&$value,$key){

	$a = ['Ã¡','Ã©','Ã*','Ã³','Ãº','Ã','Ã‰','Ã','Ã“','Ãš','Ã±','Ã§','Ã‘','Ã‡','Â©','Â®','â„¢','Ã˜','Âª','Ã¤','Ã«','Ã¯','Ã¶','Ã¼','Ã„','Ã‹','Ã ','Ã–','Ãœ','Ã­','ÃÂº','Ã©','Ã'];
	$b = ['á','é','í','ó','ú','Á','É','Í','Ó','Ú','ñ','ç','Ñ','Ç','©','®','™','Ø','ª','ä','ë','ï','ö','ü','Ä','Ë','Ï','Ö','Ü','í','ú','é'];

    $value = (str_replace($a,$b,utf8_encode($value)));
}

session_start();
$movimientosController = new movimientosController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
if(isset($request->listar_movimiento)){

	$servicioDAO = new servicioDAO();
	$lugaresDAO = new lugaresDAO();
	$movi = $servicioDAO->listMovimientos();

	$newMovi = array();

	foreach ($movi as $key => $value) {
		array_walk($value, 'utf8array');
		//$movi[$key]['adicionales'] = number_format($value['peaje']+$value['espera']+$value['parqueo']+$value['parada'], 2, ',', ' ');
        $newMovi[$key]['adicionales'] = $value['peaje']+$value['espera']+$value['parqueo']+$value['parada'];
        $newMovi[$key]['origen_nombre'] = DelCharacter($value['origen_direccion']);
        $newMovi[$key]['destino_nombre'] = DelCharacter($value['destino_nombre']);
        $newMovi[$key]['servicio_vale'] = DelCharacter($value['servicio_vale']);

        //ADICIONALES JSOLIS

        $newMovi[$key]['NombreConductor'] = DelCharacter($value['NombreConductor']);
        $newMovi[$key]['tipocliente_descripcion'] = DelCharacter($value['tipocliente_descripcion']);
        $newMovi[$key]['servicio_id'] = DelCharacter($value['servicio_id']);
        $newMovi[$key]['tipo_descripcion'] = DelCharacter($value['tipo_descripcion']);
        $newMovi[$key]['servicio_vale'] = DelCharacter($value['servicio_vale']);
        $newMovi[$key]['fecha_seleccion'] = DelCharacter($value['fecha_seleccion']);
        $newMovi[$key]['Inicio'] = DelCharacter($value['Inicio']);
        $newMovi[$key]['Fin'] = DelCharacter($value['Fin']);
        $newMovi[$key]['tarifa'] = DelCharacter($value['tarifa']);
        $newMovi[$key]['conductor_id'] = DelCharacter($value['conductor_id']);
        $newMovi[$key]['cliente_id'] = DelCharacter($value['cliente_id']);
        $newMovi[$key]['tipo_pago'] = DelCharacter($value['tipo_pago']);
        $newMovi[$key]['NombreCliente'] = DelCharacter($value['NombreCliente']);
        $newMovi[$key]['origen_id'] = DelCharacter($value['origen_id']);
        $newMovi[$key]['destino_id'] = DelCharacter($value['destino_id']);
        $newMovi[$key]['origen_id'] = DelCharacter($value['origen_id']);
        $newMovi[$key]['origen_lat'] = DelCharacter($value['origen_lat']);
        $newMovi[$key]['origen_lon'] = DelCharacter($value['origen_lon']);
        $newMovi[$key]['destino_lat'] = DelCharacter($value['destino_lat']);
        $newMovi[$key]['destino_lon'] = DelCharacter($value['destino_lon']);
        $newMovi[$key]['ff'] = DelCharacter($value['ff']);
        $newMovi[$key]['estadoservice'] = DelCharacter($value['estadoservice']);
        $newMovi[$key]['estado'] = DelCharacter($value['estado']);
        $newMovi[$key]['peaje'] = DelCharacter($value['peaje']);
        $newMovi[$key]['parqueo'] = DelCharacter($value['parqueo']);
        $newMovi[$key]['espera'] = DelCharacter($value['espera']);
        $newMovi[$key]['parada'] = DelCharacter($value['parada']);
        $newMovi[$key]['origen_direccion'] = DelCharacter($value['origen_direccion']);
        $newMovi[$key]['calificacion_conductor'] = DelCharacter($value['calificacion_conductor']);
        $newMovi[$key]['calificacion_cliente'] = DelCharacter($value['calificacion_cliente']);

    }

	 //echo '<pre>'; print_r($movi); echo '</pre>'; exit;
	header('Content-Type: application/json, utf-8');
	echo json_encode($newMovi);
}

if(isset($request->details_movimiento)){

	$servicioDAO = new servicioDAO();
	$lugaresDAO = new lugaresDAO();
	$clientesDAO = new clientesDAO();
	$conductorDAO = new conductorDAO();
	$vehiculoDAO = new vehiculoDAO();
	$calificacionDAO = new calificacionDAO();

   	$details = $servicioDAO->listaServicio($request->servicio_id);

   	/*LOGICA PARA LA PARTE DE CALIFICACION */


	$calificaciones = $calificacionDAO->obtenerCalficiacionPorServicio($request->servicio_id);
	


	$dataCalificaciones= [];
	foreach($calificaciones as $calificacion){
		 
		 $calificacion['calificacion_tipoid'] == 1 
		 ? $dataCalificaciones['calificacion_cliente'] = $calificacion['calificacion_value']
		 : $dataCalificaciones['calificacion_conductor'] = $calificacion['calificacion_value']  ;

		 $calificacion['calificacion_tipoid'] == 1 
		 ? $dataCalificaciones['comentario_cliente'] = $calificacion['comentario']
		 : $dataCalificaciones['comentario_conductor'] = $calificacion['comentario'];
	}




	/*fin de la lOGICA*/

	foreach ($details as $key => $value) {

		//$lugarorigen = $lugaresDAO->listabyin($value['origen_id']);
		//$lugardestino = $lugaresDAO->listabyin($value['destino_id']);
		$cliente = $clientesDAO->listaClienteId($value['cliente_id']);
		$conductor = $conductorDAO->listaConductorId($value['conductor_id']);
		$unidad = $vehiculoDAO->listaBy($conductor[0]['unidad_id']);
		$details[$key]['Unidad'] = (!empty($unidad))?$unidad[0]['unidad_alias']:'No tiene Unidad Asignada';
		$details[$key]['tipo_pago'] = ($value['tipo_pago'] == 1)?'Efectivo':(($value['tipo_pago'] == 2)?'Vale':'Tarjeta');
		$details[$key]['NombreCliente'] = $cliente[0]['cliente_nombre'].' '.$cliente[0]['cliente_apellido'];
		$details[$key]['NombreConductor'] = $conductor[0]['conductor_nombre'].' '.$conductor[0]['conductor_apellido'];
		



		//CALIFICACIONES
		$details[$key]['calificacion_conductor'] = isset($dataCalificaciones['calificacion_conductor']) ? $dataCalificaciones['calificacion_conductor'] : 0;
		$details[$key]['calificacion_cliente'] = isset($dataCalificaciones['calificacion_cliente']) ? $dataCalificaciones['calificacion_cliente'] : 0;
		$details[$key]['comentario_conductor'] = isset($dataCalificaciones['comentario_conductor']) ? $dataCalificaciones['comentario_conductor'] : 'No hay';
		$details[$key]['comentario_cliente'] = isset($dataCalificaciones['comentario_cliente']) ? $dataCalificaciones['comentario_cliente'] : 'No hay';
		//FIN DE CALIFIACION

		//foreach ($lugarorigen as $row) {
			$details[$key]['origen_nombre'] = $value['origen_direccion'];
		//}

		//foreach ($lugardestino as $row) {
			$details[$key]['destino_nombre'] = $value['destino_direccion'];
		//}

	}

	unset($details[0]['origen_direccion'],$details[0]['destino_direccion']);
	header('Content-Type: application/json, utf-8');
	echo json_encode($details);
}

