<?php
require_once'../DAL/clientesDAO.php';
require_once'../DAL/alertaDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/vehiculoDAO.php';
require_once'../DAL/servicioDAO.php';
require_once'../DAL/constantes.php';

class tarjetaController{

	public function ListarData($pagenum, $pagesize, $offset, $search){

		if ($search != "") {
		    $where = " AND concat(a.conductor_nombre,'',a.conductor_apellido) LIKE '%" . $search . "%'";
		} else {
		    $where = "";
		}

		$conductorDAO = new conductorDAO();
		$count = $conductorDAO->CountConductores($where);

		return $count;	

	}

}

session_start();
$controller = new tarjetaController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->listar_new)){

	$pagenum = $request->page;
	$pagesize = $request->size;
	$offset = ($pagenum - 1) * $pagesize;
	$search = $request->search;
	
	$count = $controller->ListarData($pagenum, $pagesize, $offset , $search);
	$count_new = $count[0]['total'];

	$conductorDAO = new conductorDAO();
	$Res = $conductorDAO->listaPaginacion($offset, $pagesize, $search);
	foreach ($Res as $key => $value) {
		$imagen = '../BL/'.$value['conductor_foto'];
		$exists = is_file($imagen);
		if($exists == true){
			$Res[$key]['conductor_foto'] = $value['conductor_foto'];
		}else{
			$Res[$key]['conductor_foto'] = IMG_DEFAULT1;
		}
	}

	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);
}

if(isset($request->listar_modal)){
	$conductor_id = $request->id;
	$conductorDAO = new conductorDAO();
	$Res = $conductorDAO->listaPaginacionModal($conductor_id);
	foreach ($Res as $key => $value) {
		$imagen = '../BL/'.$value['conductor_foto'];
		$exists = is_file($imagen);
		if($exists == true){
			$Res[$key]['conductor_foto'] = $value['conductor_foto'];
		}else{
			$Res[$key]['conductor_foto'] = IMG_DEFAULT1;
		}
	}

	header('Content-Type: application/json');
	echo json_encode($Res);
}



