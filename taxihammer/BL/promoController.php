<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once'../DAL/promocionDAO.php';
require_once'../DAL/clientesDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/constantes.php';

class PromoController{
	
	public function insertar($nombre, $puntos, $imagen, $fechaFinal, $fechaInicio, $cantidad, $tipo){
		$promocionDAO = new promocionDAO();
		$data['promo_nombre'] = $nombre;
		$data['promo_puntos'] = $puntos;
		$data['promo_imagen'] = $imagen;
		$data['promo_fvigencia'] = $fechaFinal;
		$data['promo_finicio'] = $fechaInicio;
		$data['promo_cantidad'] = $cantidad;
		$data['promo_tipo'] = $tipo;
		$res = $promocionDAO->insert($data);
		return $res;
	}

	public function update($data,$indice){
		$promocionDAO = new promocionDAO();
		if($indice == 1){
			$stock = $promocionDAO->obtenerPuntosPromocion($data['promo_id']);
			$data['promo_stock'] = $stock[0]['promo_stock'];
		}else{
			$data['promo_stock'] = $data['promo_cantidad'];
		}
		$res = $promocionDAO->updatePromocion($data);
		return $res;
	}

	public function ListarData($pagenum, $pagesize, $offset, $select, $text, $tipo){
		
		$promocionDAO = new promocionDAO();
		if($tipo == 'promo'){
			$count = $promocionDAO->CountPromociones($select, $text);
		}else{
			$count = $promocionDAO->CountCanjes($select, $text);
		}
		return $count;
	}
}

//Limpiar Espacios en blancos de una cadena
function ClearSpace($cadena){
  $cadena = str_replace(' ', '', $cadena);
  return $cadena;
}

/*---------------------------------------------------------------------------*/
$controller = new PromoController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->list) && $request->list == 'lista_promociones'){

	$pagenum = $request->variables->page;
	$pagesize = $request->variables->size;
	$offset = ($pagenum - 1) * $pagesize;
	$searchText = $request->variables->search;

	$tipoUsuario = (isset($request->variables->select))?(int)$request->variables->select:0;

	$count = $controller->ListarData($pagenum, $pagesize, $offset, $tipoUsuario, $searchText,'promo');
	$count_new = $count[0]['total'];

	$promocionDAO = new promocionDAO();
	$Res = $promocionDAO->listaPaginacion($offset, $pagesize, $tipoUsuario, $searchText);

	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);

}

if(isset($request->list) && $request->list == 'estado_promocion'){

	$data['promo_id'] = (int)$request->variables->id;
	$data['promo_flag'] = (int)$request->variables->estado;
	$promocionDAO = new promocionDAO();
	$res = $promocionDAO->actualizarEstado($data);
	header('Content-Type: application/json');
	echo json_encode($res);
}

if(isset($_REQUEST['delete_general'])){

   	$promocionDAO = new promocionDAO();
	$flag = $promocionDAO->obtenerPuntosPromocion($_REQUEST['Gid']);
	if(!empty($flag)){
		if($flag[0]['promo_flag'] != 1){
			$res = $promocionDAO->updatePromocionDelete($_REQUEST['Gid']);
			$json['status'] = $res;
		}else{
			$json['status'] = false;
		}
	}else{
		$json['status'] = false;
	}

	header('Content-Type: application/json');
   	echo json_encode($json);
}

if(isset($request->list) && $request->list == 'editado_promo'){

	$promo_id = (int)$request->variables->id;
	$promocionDAO = new promocionDAO();
	$Res = $promocionDAO->listaPromocionesBy($promo_id);
	header('Content-Type: application/json');
	echo json_encode($Res[0]);
}

if(isset($request->list) && $request->list == 'lista_canjes'){

	$pagenum = $request->variables->page;
	$pagesize = $request->variables->size;
	$offset = ($pagenum - 1) * $pagesize;
	$searchText = $request->variables->search;

	$tipoUsuario = (isset($request->variables->select))?(int)$request->variables->select:0;

	$count = $controller->ListarData($pagenum, $pagesize, $offset, $tipoUsuario, $searchText,'canjes');
	$count_new = $count[0]['total'];

	$promocionDAO = new promocionDAO();
	$Res = $promocionDAO->listaPaginacionCanjes($offset, $pagesize, $tipoUsuario, $searchText);

	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);

}

/*-----------------INSERTAR CONDUCTOR-------------------*/
if(isset($_REQUEST['hidden_insert_promocion'])){

	$tamano = $_FILES['imagen']['size'];
	$tipo = $_FILES['imagen']['type'];
	$archivo = $_FILES['imagen']['name'];
	$prefijo = substr(md5(uniqid(rand())),0,6);
	//$timFinal = strtotime($_REQUEST['fvigencia']);
	//$timInicial = strtotime($_REQUEST['finicio']);

	$timFinal = $_REQUEST['fvigencia'];
	$timInicial = $_REQUEST['finicio'];
	//echo $timestamp;
	if ($archivo != '') {
		// guardamos el archivo a la carpeta files
		$archivo = ClearSpace(DelCharacter($archivo));
		
		$destino = 'files/'.$prefijo.'_'.$archivo;
		if (copy($_FILES['imagen']['tmp_name'],$destino)) {
			$res = $controller->insertar($_REQUEST['nombrepromo'], $_REQUEST['puntos'], $destino, $timFinal,$timInicial,$_REQUEST['cantidad'], $_REQUEST['tipo']);
			if($res == true){
				header("location:../index.php?seccion=promo&status=true");
			}else{
				header("location:../index.php?seccion=promo&status=false");
			}
		} else {
			header("location:../index.php?seccion=promo&status=false");
		}
	} else {
		header("location:../index.php?seccion=promo&status=false");
	}
}

if(isset($_REQUEST['hidden_update_promocion'])){

	//echo '<pre>';print_r($_FILES['imagen1']['name']); echo '</pre>'; exit('foto');
	$promocionDAO = new promocionDAO();
	$tamano = $_FILES['imagen1']['size'];
	$tipo = $_FILES['imagen1']['type'];
	$archivo = $_FILES['imagen1']['name'];
	$prefijo = substr(md5(uniqid(rand())),0,6);

	$data['promo_nombre'] = $_REQUEST['nombrepromo1'];
    $data['promo_id'] = $_REQUEST['idpromocion1'];
    $data['promo_tipo'] = $_REQUEST['tipo1'];
    $data['promo_finicio'] = $_REQUEST['finicio1']; 
    $data['promo_fvigencia'] = $_REQUEST['fvigencia1'];
    $data['promo_puntos'] = $_REQUEST['puntos1'];
    $data['promo_cantidad'] = $_REQUEST['cantidad1'];

    if($archivo != ''){

    	$destino = 'files/'.$prefijo.'_'.$archivo;
    	$data['promo_imagen'] = $destino;
    	if (copy($_FILES['imagen1']['tmp_name'],$destino)) {
    		$p = $promocionDAO->obtenerPuntosPromocion($_REQUEST['idpromocion1']);
	    	if($p[0]['promo_stock'] != 0 && $p[0]['promo_stock'] < $p[0]['promo_cantidad']){
	    		$dato['promo_flag'] = 1;
	    		$dato['promo_id'] = $_REQUEST['idpromocion1'];
	    		$a = $promocionDAO->actualizarEstado($dato);
	    		$d = 1;
	    	}else{
	    		$d = 2;
	    	}
    		$res = $controller->update($data,$d);
    	}else {
			header("location:../index.php?seccion=promo&status=false");
		}

    }else{
    	$p = $promocionDAO->obtenerPuntosPromocion($_REQUEST['idpromocion1']);
    	if($p[0]['promo_stock'] != 0 && $p[0]['promo_stock'] < $p[0]['promo_cantidad']){
    		$dato['promo_flag'] = 1;
    		$dato['promo_id'] = $_REQUEST['idpromocion1'];
    		$a = $promocionDAO->actualizarEstado($dato);
    		$d = 1;
    	}else{
    		$d = 2;
    	}
    	$data['promo_imagen'] = '';
    	$res = $controller->update($data,$d);
    }

    if($res == true){
		header("location:../index.php?seccion=promo&status=true");
	}else{
		header("location:../index.php?seccion=promo&status=false");
	}
}



