<?php
require_once'../DAL/clientesDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/puntosDAO.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class puntosController{

	public function insertarPuntos($data){
		$puntosDAO = new puntosDAO();
		$result = $puntosDAO->insertPuntos($data);
		return $result;
	}

	public function updatePuntos($data){
		$puntosDAO = new puntosDAO();
		$result = $puntosDAO->updaPuntos($data);
		return $result;
	}

	public function ListarData($pagenum, $pagesize, $offset, $search, $select){
		
		if($select == 1){//cliente
			$where = "";
			if($search != ""){
				$var = str_replace(" ","%",$search);
			    $where = " AND concat(a.cliente_nombre,'',a.cliente_apellido) LIKE '%" . $var . "%'";
			}
		}else{//conductor
			$where = "";
			if($search != ""){
				$var = str_replace(" ","%",$search);
			    $where = " AND concat(a.conductor_nombre,'',a.conductor_apellido) LIKE '%" . $var . "%'";
			}
		}
			
		$puntosDAO = new puntosDAO();
		$count = $puntosDAO->CountReportePuntos($where, $select);

		return $count;

	}

}

session_start();
$controller = new puntosController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->lista_ptos)){
	$puntosDAO = new puntosDAO();
	$Res = $puntosDAO->listaptos();
	header('Content-Type: application/json');
	echo json_encode($Res);
}

if(isset($request->lista_add_ptos)){
	$data['puntos_cliente']=$request->cliente;
	$data['puntos_conductor']=$request->conductor;
	$data['puntos_factor_cliente']=$request->puntos1;
	$data['puntos_factor_conductor']=$request->puntos2;
	$Res = $controller->insertarPuntos($data);
	header('Content-Type: application/json');
	echo json_encode($Res);
}

if(isset($request->hidden_toggle)){
	$puntosDAO = new puntosDAO();
	$data['puntos_id']=$request->taskID;
	$data['puntos_estado']=$request->status;
	//echo '<pre>'; print_r($data); echo '</pre>';exit;
	$valid = $puntosDAO->validPuntos();
	//echo '<pre>'; print_r($valid); echo '</pre>';exit;
	if($valid[0]['total'] != 0){
		$actualizar = $puntosDAO->updatestados();
	}
	$Res = $controller->updatePuntos($data);
	header('Content-Type: application/json');
	echo json_encode($Res);
}

/*if(isset($request->hidden_delete)){
	$data['puntos_id']=$request->taskID;
	$data['puntos_delete']=0;
	$puntosDAO = new puntosDAO();
	$Res = $puntosDAO->deletPuntos($data);
	header('Content-Type: application/json');
	echo json_encode($Res);
}*/
if(isset($_REQUEST['delete_general'])){

	$data['puntos_id']=(int)$_REQUEST['Gid'];
	$data['puntos_delete']=0;
	$puntosDAO = new puntosDAO();
	$res = $puntosDAO->deletPuntos($data);
	$json['status'] = true;
	header('Content-Type: application/json');
   	echo json_encode($json);
}

if(isset($request->listar_reporte_puntos)){

	$pagenum = $request->page;
	$pagesize = $request->size;
	$offset = ($pagenum - 1) * $pagesize;
	$search = $request->search;
	$select = $request->select;

	$count = $controller->ListarData($pagenum, $pagesize, $offset, $search, $select);
	$count_new = $count[0]['total'];

	if($select != 1){
		$conductorDAO = new conductorDAO();
		$Res = $conductorDAO->listaPaginacionPuntos($offset, $pagesize, $search);
	}else{
		$clientesDAO = new clientesDAO();
		$Res = $clientesDAO->listaPaginacionPuntos($offset, $pagesize, $search);
	}
	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);
	
}

