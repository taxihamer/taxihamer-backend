<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once'../DAL/recargasDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/operacionesDAO.php';

class recargasController{

	public function autocompleteLista($data){

		$conductorDAO = new conductorDAO();
		$respta = $conductorDAO->filtrarConductorPorCelular($data);

		$json = array();
		for ($x = 0; $x < count($respta); $x++) {
		    array_push($json, array(
		        "label" => $respta[$x]['conductor_celular'] . ' / ' .
		            utf8_encode($respta[$x]['conductor_nombre'] . ' ' . $respta[$x]['conductor_apellido']),
		        "value" => $respta[$x]['conductor_celular']. ' - '.  utf8_encode($respta[$x]['conductor_nombre'] . ' ' . $respta[$x]['conductor_apellido']),
		        "idConductor" => $respta[$x]['conductor_id'],
		        "cliente" => utf8_encode($respta[$x]['conductor_nombre'] . ' ' . $respta[$x]['conductor_apellido']),
		        "correo" => $respta[$x]['conductor_correo'],
		        "celular" => $respta[$x]['conductor_celular'],
		        "user" => $respta[$x]['conductor_correo'],
		        "pass" => $respta[$x]['conductor_password']
		    ));
		}
		return $json;
	}

	public function actualizarMontoConductor($data){

		$conductorDAO = new conductorDAO();
		$datosConductor = $conductorDAO->listaConductorId($data['conductor_id']);

		if(!empty($datosConductor)){

			$montoanterior = $datosConductor[0]['conductor_recarga'];
			$saldoActual = $data['cta_monto'] + $montoanterior;
			$send2['conductor_id'] = $data['conductor_id'];
			$send2['conductor_recarga'] = $saldoActual;
			$montoactual = $conductorDAO->actualizarMontoRecarga($send2);
			$rspta = $montoactual;

		}else{
			$rspta = false;
		}

		return $rspta;
		
	}
}

session_start();
$recargasController = new recargasController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->lista_recargas)){

	$recargasDAO = new recargasDAO();
	$Res = $recargasDAO->lista();
	header('Content-Type: application/json');
	echo json_encode($Res);
}

if(isset($_REQUEST['listar_recarga'])){

	$data['conductor_celular'] = $_REQUEST['q'];
	$res = $recargasController->autocompleteLista($data);
	header('Content-Type: application/json');
    echo json_encode($res);
}

//********Switch de activar Recarga ********//
if(isset($request->estado)){
	
	$recargasDAO = new recargasDAO();
	$operacionesDAO = new operacionesDAO();
	$conductorDAO = new conductorDAO();

	$data['recarga_id'] = $request->recarga;
	$data['recarga_check'] = (!empty($request->switch))?$request->switch:0;

	$validar = $recargasDAO->validarClickRecarga((int)$request->recarga);
	
	if($validar[0]['total'] != 0){

		$res = $recargasDAO->actualizarEstado($data);
		if($res == true){
			$updateCta = $operacionesDAO->updateCta($data);
			if($updateCta == true){
				$datosRecarga = $recargasDAO->listaBy($request->recarga);
				$datosConductor = $conductorDAO->listaConductorId($datosRecarga[0]['conductor_id']);
				if(!empty($datosRecarga)){

					$valorRecarga = $datosRecarga[0]['recarga_monto'];
					$valorConductor = $datosConductor[0]['conductor_recarga'];

					if($data['recarga_check'] == 1){
						$saldoActual = $valorConductor + $valorRecarga;
					}elseif ($data['recarga_check'] == 99) {
						$saldoActual = $valorConductor;
					}else{
						$saldoActual = $valorConductor - $valorRecarga;
					}

					$tipo =  '';
					switch ($datosRecarga[0]['recarga_check']) {
						case '0': $tipo = 'Rechazado'; break;
						case '1': $tipo = 'Aprobado'; break;
						case '2': $tipo = 'Pendiente'; break;
						case '99': $tipo = 'Rechazado'; break;
					};
					
					$send2['conductor_id'] = $datosRecarga[0]['conductor_id'];
					$send2['conductor_recarga'] = $saldoActual;
					
					$montoactual = $conductorDAO->actualizarMontoRecarga($send2);

					$json['status'] = true;
					$json['mensaje'] = $data['recarga_check'];
					$json['tipo_recarga'] = $tipo;
					header('Content-Type: application/json');
	    			echo json_encode($json);

				}else{
					$res = false;
					header('Content-Type: application/json');
	        		echo json_encode($res);
				}
			}else{
				$res = false;
				header('Content-Type: application/json');
	        	echo json_encode($res);
			}
			
		}else{
			$res = false;
			header('Content-Type: application/json');
	        echo json_encode($res);
		}
	}else{

		$json['status'] = true;
		$json['mensaje2'] = " La Recarga ya fue actualizado por otro Operador ";
		header('Content-Type: application/json');
    	echo json_encode($json);
	}	
}

if(isset($request->insert_recarga)){


	$monto = $request->data->monto;
	$operacion = $request->data->noperacion;
	$fecha = $request->fecha;
	$conductor_id = $request->id;
	$tipo = $request->select;	

	if(!empty($monto) && !empty($operacion) && !empty($conductor_id)){

		$data['conductor_id'] = $conductor_id;
		$data['recarga_monto'] = $monto;
		$data['recarga_tipo'] = $tipo;
		$data['recarga_fecha'] = $fecha;
		$data['recarga_num_oper'] = $operacion;
		$data['recarga_check'] = 1;
		$data['recarga_send'] = 1;

		$recargasDAO = new recargasDAO();
		$rest = $recargasDAO->insertRecarga($data);

		header('Content-Type: application/json');
		if($rest[0]['p_return_code'] != '-1' || $rest[0]['p_return_code'] != '-2'){

		//if($rest['status'] == true){
				$operacionesDAO = new operacionesDAO();

				$send['recarga_id'] = $rest[0]['p_return_code'];
				$send['cta_tipo'] = 1;
				$send['cta_fecha'] = $fecha;
				$send['cta_monto'] = $monto;
				$send['conductor_id'] = $conductor_id;
				$send['cta_check'] = 1;

				$monto = $recargasController->actualizarMontoConductor($send);
				$operacion = $operacionesDAO->insertCta($send);

				if($operacion == true){
					echo json_encode($operacion);
				}else{
					$operacion = false;
					echo json_encode($operacion);
				}
        	
		}else{
			$rest = false;
	        echo json_encode($rest);
		}
	}else{
		header('Content-Type: application/json');
			$rest = false;
	        echo json_encode($rest);
	}
}

if(isset($_REQUEST['hidden_data_verif'])){

	$recarga_id = $_REQUEST['codigo'];
	$tipo = $_REQUEST['tipo'];

	$recargasDAO = new recargasDAO();

	if($tipo == 1){
		$resul = $recargasDAO->listaByVerifi($recarga_id);
		if(!empty($resul)){
			$rest['status'] = $resul[0]['recarga_click'];
		}
	}else{
		
		$resul = $recargasDAO->actualizarClick($recarga_id);
		if($resul == true){
			$rest['status'] = 1;
		}else{
			$rest['status'] = 0;
		}
	}

	header('Content-Type: application/json');
	echo json_encode($rest);
}

/************ Liquidaciones *************/

if(isset($request->lista_liquidacion)){

	$recargasDAO = new recargasDAO();
	$Res = $recargasDAO->listaLiquidacion();
	foreach ($Res as $key => $rowfecha) {
		$date = explode(" ", $rowfecha['liquidacion_fecha']);
		$Res[$key]['liquidacion_fecha'] = $date[0];
		$Res[$key]['liquidacion_hora'] = $date[1];
	}
	header('Content-Type: application/json');
	echo json_encode($Res);
}

if(isset($request->insert_liquidacion)){

	$monto = floatval($request->data->monto);
	$observacion = $request->data->msg;
	$conductor_id = (int)$request->id;

	if(!empty($monto) && !empty($observacion) && !empty($conductor_id)){

		$data['conductor_id'] = $conductor_id;
		$data['liquidacion_monto'] = $monto;
		$data['liquidacion_fecha'] = date("Y-m-d H:i:s");
		$data['liquidacion_observacion'] = $observacion;

		$recargasDAO = new recargasDAO();

			$rest = $recargasDAO->insertLiquidacion($data);
			header('Content-Type: application/json');
			//if($rest[0]['p_return_code'] != '-1' || $rest[0]['p_return_code'] != '-2'){
			if($rest['status'] &&  isset($rest['lastid']) ){
					$operacionesDAO = new operacionesDAO();

					$send['recarga_id'] = $rest['lastid'];
					$send['cta_tipo'] = 3;
					$send['cta_fecha'] = $data['liquidacion_fecha'];
					$send['cta_monto'] = $monto*-1;
					$send['conductor_id'] = $conductor_id;
					$send['cta_check'] = 1;

					$monto = $recargasController->actualizarMontoConductor($send);
					$operacion = $operacionesDAO->insertCta($send);

					if($operacion == true){
						echo json_encode($operacion);
					}else{
						$operacion = false;
						echo json_encode($operacion);
					}
	        	
			}else{
				$rest = false;
		        echo json_encode($rest);
			}
	}else{
		header('Content-Type: application/json');
			$rest = false;
	        echo json_encode($rest);
	}
}
