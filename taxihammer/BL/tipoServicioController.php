<?php
require_once'../DAL/tipoServicioDAO.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class tipoServicioController{

	public function insertarTipoServicio($nombre, $factor, $aeropuerto, $reserva){
		$tipoServicioDAO = new tipoServicioDAO();
		$data['tiposervicio_nombre'] = $nombre;
		$data['tiposervicio_factor'] = $factor;
		$data['tiposervicio_aeropuerto'] = $aeropuerto;
		$data['tiposervicio_reserva'] = $reserva;
		$res = $tipoServicioDAO->insertarTipoServicio($data);
		return $res;
	}
	public function actualizarTipoServicio($nombre, $id, $factor, $aeropuerto, $reserva){
		$tipoServicioDAO = new tipoServicioDAO();
		$data['tiposervicio_nombre'] = $nombre;
		$data['tiposervicio_factor'] = $factor;
		$data['tiposervicio_aeropuerto'] = $aeropuerto;
		$data['tiposervicio_reserva'] = $reserva;
		$data['tiposervicio_id'] = $id;
		$res = $tipoServicioDAO->updateTipoServicio($data);
		return $res;
	}
	public function eliminarTipoServicio($id){
		$tipoServicioDAO = new tipoServicioDAO();
		$data['tiposervicio_id'] = $id;
		$res = $tipoServicioDAO->deleteTipoServicio($data);
		return $res;
	}

	public function ListarData($pagenum, $pagesize, $offset, $text){
		
		$tipoServicioDAO = new tipoServicioDAO();
		$count = $tipoServicioDAO->CountTipoServicio($text);
		return $count;
	}
}

$controller = new tipoServicioController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
/*-----------------INSERTAR TIPO SERVICIO-------------------*/
if(isset($_REQUEST['hidden_insert_tiposervicio'])){
	$res = $controller->insertarTipoServicio($_REQUEST['nombre'], $_REQUEST['factor'], (int)$_REQUEST['aeropuerto'], (int)$_REQUEST['reserva']);
	if($res == true){
		header("location:../index.php?seccion=servicios&status=true");
	}else{
		header("location:../index.php?seccion=servicios&status=false");
	}
}
/*-----------------ACTUALIZAR TIPO SERVICIO-------------------*/
if(isset($_REQUEST['hidden_update_tiposervicio'])){
	$res = $controller->actualizarTipoServicio($_REQUEST['servicio'], $_REQUEST['id'], $_REQUEST['factorEdit'], (int)$_REQUEST['aeropuertoEdit'], (int)$_REQUEST['reservaEdit']);
	if($res == true){
		header("location:../index.php?seccion=servicios&status=true");
	}else{
		header("location:../index.php?seccion=servicios&status=false");
	}
}
/*-----------------DELETE TIPO SERVICIO-------------------*/
if(isset($_REQUEST['delete_general'])){

	$res = $controller->eliminarTipoServicio($_REQUEST['Gid']);
	$json['status'] = true;
	header('Content-Type: application/json');
   	echo json_encode($json);

}

if(isset($request->list) && $request->list == 'lista_tipo_servicio'){

	$pagenum = $request->variables->page;
	$pagesize = $request->variables->size;
	$offset = ($pagenum - 1) * $pagesize;
	$searchText = $request->variables->search;

	$count = $controller->ListarData($pagenum, $pagesize, $offset, $searchText);
	$count_new = $count[0]['total'];

	$tipoServicioDAO = new tipoServicioDAO();
	$Res = $tipoServicioDAO->listaPaginacion($offset, $pagesize, $searchText);

	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);
}

if(isset($request->list) && $request->list == 'editado_tipo_servicio'){

	$tiposervicio_id = (int)$request->variables->id;
	$tipoServicioDAO = new tipoServicioDAO();
	$data = $tipoServicioDAO->listaTipoServiciosId($tiposervicio_id);
	header('Content-Type: application/json');
	echo json_encode($data[0]);

}