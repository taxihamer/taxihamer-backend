<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once'../DAL/usuarioDAO.php';
require_once'../DAL/menuDAO.php';
require_once'../DAL/rolDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/menuDAO.php';

class usuarioController{

	public function verificarLogin($mail, $pass){
		$data['mail'] = $mail;
		$data['pass'] = $pass;

		$usuarioDAO = new usuarioDAO();
		$resultset = $usuarioDAO->logIn($data);
		$return['status'] = true;
		if($resultset != NULL){
			$return['user'] = $resultset[0]['usuario_nombre'];
			$return['rol'] = $resultset[0]['rol_nombre'];
			$return['mail'] = $resultset[0]['usuario_mail'];
			$return['rol_id'] = $resultset[0]['rol_id'];

			//obtenemos el menu y submenu de bd
			$menuDAO = new menuDAO();
			$menulist = $menuDAO->listmenu();

			foreach ($menulist as $key => $menu) {
				$menuparent[] = $menu['menu_id'];
				$menusublist = array_filter($menuDAO->listsubmenuId($menu['menu_id']));
				foreach ($menusublist as $j => $submenu) {
					$menuchildren[] = $submenu['submenu_id'];
				}
			}

			if(empty($resultset[0]['menurol_id'])){
				if($resultset[0]['rol_id'] == 1){
					$this->menuAdminautomatico(1,$resultset[0]['rol_id'],$menuparent,$menuchildren);
				}
			}

			$rolDAO = new rolDAO();
			$result = $rolDAO->listmenurolid($resultset[0]['rol_id']);
			if(!empty($result)){
				$menuDAO = new menuDAO();
				$data['menurol_id'] = $result[0]['menurol_id'];
				$listmenu = $menuDAO->listaoperadoresById($data);
				$parent = json_decode($listmenu[0]['menurol_jsonparent']);
				if(!empty($parent)){ 
					if (in_array('11', $parent, true)) {
						$return['trans'] = 1;
					}else{
						$return['trans'] = 2;
					}
				}
			}
			/*else{
				if($resultset[0]['rol_id'] == 1){
					$this->menuAdminautomatico(2,$resultset[0]['rol_id'],$menuparent,$menuchildren);
				}
			}*/

		}else{

			//$result = $usuarioDAO->listEstado($data);
			//if($result[0]['usuario_status'] == 0){
				//$return['status'] = 5;
			//}else{
				$return['status'] = false;	
			//}
		}

		return $return;
	}

	public function verificarLoginConductor($mail, $pass){

		$data['mail'] = $mail;
		$data['pass'] = $pass;

		$conductorDAO = new conductorDAO();
		$resultset = $conductorDAO->logIn($data);
		$return['status'] = true;
		if($resultset != NULL){
			$return['user'] = $resultset[0]['conductor_nombre'];
			$return['rol'] = 'Conductor';
			$return['mail'] = $resultset[0]['conductor_correo'];
			$return['conductor_id'] = $resultset[0]['conductor_id'];
			$return['conductor_foto'] = $resultset[0]['conductor_foto'];

		}else{
				$return['status'] = false;	
		}

		return $return;
	}

	function menuAdminautomatico($type,$rol_id,$parent,$children){

		$data['rol_id'] = $rol_id;
		$data['menurol_jsonparent'] = $parent;
		$data['menurol_jsonchildren'] = $children;

		$menuDAO = new menuDAO();

		if($type == 1){

			$result = $menuDAO->insertarmenurol($data);

			if($result['status'] == 1){

				$roldata['rol_id'] = $rol_id;
				$roldata['menurol_id'] = $result['lastid'];

				$rolDAO = new rolDAO();
				$resultado = $rolDAO->updaterol($roldata); 
			}

		}
		/*else{
			$result = $menuDAO->actualizarmenurol($data);
		}*/
	}

	public function recuperarPass($mail){
		$data['mail'] = $mail;

		$usuarioDAO = new usuarioDAO();
		$resultset = $usuarioDAO->listByMail($data);
		if($resultset != NULL){
			return $resultset[0]['usuario_password'];
		}else{
			return "false";
		}
	}

	public function cambiarPass($mail, $pass){
		$data['mail'] = $mail;
		$data['pass'] = $pass;

		$usuarioDAO = new usuarioDAO();
		$res = $usuarioDAO->updatePassword($data);
		return $res;
	}

	public function lista(){
		$usuarioDAO = new usuarioDAO();
		$res = $usuarioDAO->lista();
		return $res;
	}

	public function eliminarUsuario($id){
		$data['usuario_id'] = $id;
		$usuarioDAO = new usuarioDAO();
		$result = $usuarioDAO->eliminarUsuario($data);
		return $result;
	}

	public function insertarUsuario($nombre,$correo,$password,$lat,$lon,$status,$permisos){
		$data['usuario_nombre'] = $nombre;
		$data['usuario_mail'] = $correo;
		$data['rol_id'] = $permisos;
		$data['region_id'] = 1;
		$data['usuario_latitud'] = $lat;
		$data['usuario_longitud'] = $lon;
		$data['usuario_status'] = $status;
		$data['usuario_password'] = $password;

		$usuarioDAO = new usuarioDAO();
		$result = $usuarioDAO->insertarUsuario($data);
		return $result;
	}

	public function actualizarUsuario($id,$nombre,$correo,$password,$lat,$lon,$status,$permisos){
		$data['usuario_nombre'] = $nombre;
		$data['usuario_mail'] = $correo;
		$data['rol_id'] = $permisos;
		$data['region_id'] = 1;
		$data['usuario_latitud'] = $lat;
		$data['usuario_longitud'] = $lon;
		$data['usuario_status'] = $status;
		$data['usuario_password'] = $password;
		$data['usuario_id'] = $id;

		$usuarioDAO = new usuarioDAO();
		$result = $usuarioDAO->updateUsuario($data);
		return $result;
	}

	public function ListarData($pagenum, $pagesize, $offset, $text){
		
		$usuarioDAO = new usuarioDAO();
		$count = $usuarioDAO->CountUsuarios($text);
		return $count;
	}
}
session_start();
$controller = new usuarioController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

/*-----------------LOGIN CONDUCTOR-------------------*/
if(isset($_REQUEST['hidden_login_conductor'])){

	$res = $controller->verificarLoginConductor($_REQUEST['mail'], $_REQUEST['pass']);
	if($res['status'] == true){
		session_start();
		$_SESSION['user'] = $res['user'];
		$_SESSION['rol'] = $res['rol'];
		$_SESSION['mail'] = $res['mail'];
		$_SESSION['id'] = $res['conductor_id'];
		$_SESSION['foto'] = $res['conductor_foto'];
		header("location:../indexConductor.php");
	}else{
		header("location:../conductor.php?status=false");
	}
}
/*-----------------LOGIN-------------------*/
if(isset($_REQUEST['hidden_login'])){
	$res = $controller->verificarLogin($_REQUEST['mail'], $_REQUEST['pass']);

	if($res['status'] == true){
		session_start();
		$_SESSION['user'] = $res['user'];
		$_SESSION['rol'] = $res['rol'];
		$_SESSION['mail'] = $res['mail'];
		$_SESSION['rol_id'] = $res['rol_id'];
		$_SESSION['trans'] = $res['trans'];
		header("location:../index.php");
	}else{
		header("location:../login.php?status=false");
	}
}
/*-----------------RECUPERAR PASSWORD-------------------*/
if(isset($_REQUEST['hidden_recoverpass'])){
	$res = $controller->recuperarPass($_REQUEST['mail']);
	if($res != "false"){
		$para      = $_REQUEST['mail'];
		$titulo    = 'Correo de Recuperacion de Password';
		$mensaje   = 'Hola, te enviamos tu password a pedido desde la web,  Su password es: '.$res;

		if(mail($para, $titulo, $mensaje)){
			header("location:../recuperar.php?status=true");
		}else{
			header("location:../recuperar.php?status=error");
		}
	}else{
		header("location:../recuperar.php?status=false");
	}
}
/*-----------------CAMBIAR PASSWORD-------------------*/
if(isset($_REQUEST['hidden_cambiarpass'])){
	if($_REQUEST['pass1'] == $_REQUEST['pass2']){
		$res = $controller->cambiarPass($_REQUEST['mail'], $_REQUEST['pass1']);
		if($res == true){
			header("location:../cambiar.php?status=true");
		}else{
			header("location:../cambiar.php?status=false");
		}
	}else{
		header("location:../cambiar.php?status=error");
	}
}
/*-----------------INSERTAR USUARIO-------------------*/
if(isset($_REQUEST['hidden_usuario_insert'])){
	if($_REQUEST['pass1'] == $_REQUEST['pass2']){
		$res = $controller->insertarUsuario($_REQUEST['nombre'],$_REQUEST['correo'],$_REQUEST['pass1'],$_REQUEST['latitud'],$_REQUEST['longitud'],$_REQUEST['status'],$_REQUEST['permisos']);
		if($res == true){
			header("location:../index.php?seccion=configuracion&status=true");
		}else{
			header("location:../index.php?seccion=configuracion&status=error");
		}
	}else{
		header("location:../index.php?seccion=configuracion&status=error");
	}
}
/*-----------------ACTUALIZAR USUARIO-------------------*/
if(isset($_REQUEST['hidden_usuario_update'])){
	if($_REQUEST['pass11'] == $_REQUEST['pass22']){
		$res = $controller->actualizarUsuario($_REQUEST['id'], $_REQUEST['nombre1'],$_REQUEST['correo1'],$_REQUEST['pass11'],$_REQUEST['latitud1'],$_REQUEST['longitud1'],$_REQUEST['status1'],$_REQUEST['permisos1']);
		if($res == true){
			header("location:../index.php?seccion=configuracion&status=true");
		}else{
			header("location:../index.php?seccion=configuracion&status=error");
		}
	}else{
		header("location:../index.php?seccion=configuracion&status=error");
	}
}

if(isset($request->list) && $request->list == 'lista_usuario'){

	$pagenum = $request->variables->page;
	$pagesize = $request->variables->size;
	$offset = ($pagenum - 1) * $pagesize;
	$searchText = $request->variables->search;

	$count = $controller->ListarData($pagenum, $pagesize, $offset, $searchText);
	$count_new = $count[0]['total'];

	$usuarioDAO = new usuarioDAO();
	$Res = $usuarioDAO->listaPaginacion($offset, $pagesize, $searchText);

	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);
}

if(isset($request->list) && $request->list == 'editado_usuario'){

	$usuario_id = (int)$request->variables->id;
	$usuarioDAO = new usuarioDAO();
	$data['usuario_id'] = $usuario_id;
	$Res = $usuarioDAO->listByIdRolUsuario($data);

	header('Content-Type: application/json');
	echo json_encode($Res[0]);
}

if(isset($request->list) && $request->list == 'estado_usuario'){

	$data['usuario_id'] = (int)$request->variables->id;
	$data['usuario_status'] = (int)$request->variables->estado;

	$usuarioDAO = new usuarioDAO();
	$Res = $usuarioDAO->actualizarEstado($data);

	header('Content-Type: application/json');
	echo json_encode($Res);
}

if(isset($_REQUEST['delete_general'])){

	$res = $controller->eliminarUsuario($_REQUEST['Gid']);
	$json['status'] = true;
	$json['data'] = $centros;
	header('Content-Type: application/json');
   	echo json_encode($json);
}
