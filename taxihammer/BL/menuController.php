<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once'../DAL/menuDAO.php';
require_once'../DAL/rolDAO.php';

class menuController{

	public function insertarMenu($rol,$parent,$chil){

		$data['rol_id'] = $rol;
		$data['menurol_jsonparent'] = $parent;
		$data['menurol_jsonchildren'] = $chil;

		$menuDAO = new menuDAO();
		$result = $menuDAO->insertarmenurol($data);
		if($result['status'] == 1){

			$roldata['rol_id'] = $rol;
			$roldata['menurol_id'] = $result['lastid'];

			$rolDAO = new rolDAO();
			$resultado = $rolDAO->updaterol($roldata); 
		}

		return $resultado;
	}

	public function updateMenu($rol,$parent,$chil){

		$data['rol_id'] = $rol;
		$data['menurol_jsonparent'] = $parent;
		$data['menurol_jsonchildren'] = $chil;

		$menuDAO = new menuDAO();
		$rolDAO = new rolDAO();
		$result = $menuDAO->actualizarmenurol($data);

		if($rol == 3){
			//Beneficios => 12 , Transacciones => 11 , Tarjetas => 4 , Despacho => 7 --Hijos (Reservas => 10)
			$result = $rolDAO->listmenurolid($rol);
			$data['menurol_id'] = $result[0]['menurol_id'];
			$listmenu = $menuDAO->listaoperadoresById($data);
			$menu_parent = (!empty($listmenu[0]['menurol_jsonparent']))?json_decode($listmenu[0]['menurol_jsonparent']):'';
			$menu_children = (!empty($listmenu[0]['menurol_jsonchildren']))?json_decode($listmenu[0]['menurol_jsonchildren']):'';

			$menu_movil  = array();

			if (in_array(4, $menu_parent)){array_push($menu_movil, "4");}
			if (in_array(7, $menu_parent)){array_push($menu_movil, "7");}
			if (in_array(10, $menu_children)){array_push($menu_movil, "10");}
			if (in_array(11, $menu_parent)){array_push($menu_movil, "11");}
			if (in_array(12, $menu_parent)){array_push($menu_movil, "12");}

				if(count($menu_movil) > 0){
					$send['menurol_json_movil'] = $menu_movil;
					$send['rol_id'] = $rol;
					$resultMovil = $menuDAO->actualizarmenurolMovil($send);
				}
		}

		return $result;
	}

	public function rolbyid($id){

		$data['menurol_id'] = $id;
		$menuDAO = new menuDAO();
		$res = $menuDAO->listaoperadoresById($data);
		return $res;
	}
}

session_start();
$controller = new menuController();
/*-----------------INSERTAR MENU-------------------*/
if(isset($_REQUEST['hidden_menu_insert'])){
	
	$menuDAO = new menuDAO();
	$validar = $menuDAO->validarRol($_REQUEST['operadores']);
	
	if($validar == 1){
		header("location:../index.php?seccion=modulo&status=valid");
	}else{
		if($_REQUEST['operadores'] != 0 && !empty($_REQUEST['parent'])){
			$res = $controller->insertarMenu($_REQUEST['operadores'],$_REQUEST['parent'],$_REQUEST['chil']);
			if($res == true){
				header("location:../index.php?seccion=modulo&status=true");
			}else{
				header("location:../index.php?seccion=modulo&status=error");
			}
		}else{
			header("location:../index.php?seccion=modulo&status=error");
		}
	}
}
/*-----------------ACTUALIZAR MENU-------------------*/
if(isset($_REQUEST['hidden_menu_update'])){

	if($_REQUEST['operadorup'] != 0 && !empty($_REQUEST['parent'])){
		$res = $controller->updateMenu($_REQUEST['operadorup'],$_REQUEST['parent'],$_REQUEST['chil']);
		if($res == true){
			header("location:../index.php?seccion=modulo&status=true");
		}else{
			header("location:../index.php?seccion=modulo&status=error");
		}
	}else{
		header("location:../index.php?seccion=modulo&status=error");
	}
}
/*-----------------ID MENU Y SUBMENU-------------------*/
if(isset($_REQUEST['hidden_menu_select'])){
	
	if(!empty($_REQUEST['id'])){
		$res = $controller->rolbyid($_REQUEST['id']);
		if($res == true){
			header('Content-Type: application/json');
            echo json_encode($res);
		}else{
			$res = false;
			header('Content-Type: application/json');
            echo json_encode($res);
		}
	}else{
		header("location:../index.php?seccion=modulo&status=error");
	}
}