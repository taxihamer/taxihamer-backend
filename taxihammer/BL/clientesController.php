<?php
require_once'../DAL/clientesDAO.php';
require_once'../DAL/alertaDAO.php';

class clientesController{

	public function agregarCliente($tipo,$idempresa,$nombre,$apellidos,$costo,$supervisor,$celular,$correo,$password,$radio){

		$data['cliente_nombre'] = $nombre;
		$data['cliente_apellido'] = $apellidos;
		$data['cliente_correo'] = $correo;
		$data['cliente_celular'] = $celular;
		$data['cliente_costos'] = $costo;
		$data['cliente_supervisor'] = $supervisor;
		$data['cliente_password'] = $password;
		$data['tipocliente_id'] = $tipo;
		$data['empresa_id'] = $idempresa;
		$data['cliente_status'] = 1;
		$data['cliente_estados'] = $radio;

		$clientesDAO = new clientesDAO();
		$result = $clientesDAO->insertarCliente($data);

		if($result['status'] == 1){
			$result = true;
		}
		return $result;
	}

	public function eliminarCliente($id){
		$data['cliente_id'] = $id;
		$clientesDAO = new clientesDAO();
		$result = $clientesDAO->deleteCliente($data);
		return $result;
	}

	public function actualizarCliente($id,$tipo,$idempresa,$nombre,$apellidos,$costo,$supervisor,$celular,$correo,$password,$radio){

		if($tipo == 1){
			$costo = '';
			$supervisor = '';
			$idempresa = 0;
			$data['cliente_estados'] = intval($radio);
		}else{
			$data['cliente_estados'] = intval($radio);
		}

		$data['cliente_id'] = $id;
		$data['cliente_nombre'] = $nombre;
		$data['cliente_apellido'] = $apellidos;
		$data['cliente_correo'] = $correo;
		$data['cliente_celular'] = $celular;
		$data['cliente_costos'] = $costo;
		$data['cliente_supervisor'] = $supervisor;
		$data['cliente_password'] = $password;
		$data['tipocliente_id'] = $tipo;
		$data['empresa_id'] = $idempresa;
		//$data['cliente_status'] = 1;

		$clientesDAO = new clientesDAO();
		$result = $clientesDAO->updateCliente($data);
		return $result;
	}

	public function filtrar($start, $per_page, $filter){

		$clientesDAO = new clientesDAO();
		$data['start'] = $start;
		$data['per_page'] = $per_page;
		$data['cliente_nombre'] = $filter;
		$res = $clientesDAO->listaPaginacionby($data);
		return $res;
	}

	public function ListarData($pagenum, $pagesize, $offset, $select, $text){
		
		$clientesDAO = new clientesDAO();
		$count = $clientesDAO->CountClientes($select, $text);
		return $count;
	}
}

session_start();
$controller = new clientesController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->list) && $request->list == "lista_clientes"){

	$pagenum = $request->variables->page;
	$pagesize = $request->variables->size;
	$offset = ($pagenum - 1) * $pagesize;
	$searchText = $request->variables->search;

	$tipoCliente = (isset($request->variables->select))?(int)$request->variables->select:0;

	$count = $controller->ListarData($pagenum, $pagesize, $offset, $tipoCliente, $searchText);
	$count_new = $count[0]['total'];

	$clientesDAO = new clientesDAO();
	$Res = $clientesDAO->listaPaginacion($offset, $pagesize, $tipoCliente, $searchText);

	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);

}

if(isset($request->list) && $request->list == 'editado_cliente'){

	$cliente_id = (int)$request->variables->id;
	$clientesDAO = new clientesDAO();
	$Res = $clientesDAO->listaClienteId($cliente_id);
	header('Content-Type: application/json');
	echo json_encode($Res[0]);
}

if(isset($request->list) && $request->list == 'estado_cliente'){

	$data['cliente_id'] = (int)$request->variables->id;
	$data['cliente_estados'] = (int)$request->variables->estado;
	$clientesDAO = new clientesDAO();
	$res = $clientesDAO->actualizarEstado($data);
	header('Content-Type: application/json');
	echo json_encode($res);
}

if(isset($_REQUEST['delete_general'])){

	$res = $controller->eliminarCliente($_REQUEST['Gid']);
	$json['status'] = true;
	header('Content-Type: application/json');
   	echo json_encode($json);
}

/*-----------------INSERTAR MENU-------------------*/
if(isset($_REQUEST['hidden_cliente_insert'])){

	if($_REQUEST['tipo'] != '-1' && !empty($_REQUEST['nombres']) && !empty($_REQUEST['password'])){
		$res = $controller->agregarCliente(
									$_REQUEST['tipo'],
								    $_REQUEST['empresa'],
								    $_REQUEST['nombres'],
								    $_REQUEST['apellidos'], 
								    $_REQUEST['costo'],
								    $_REQUEST['supervisor'], 
								    $_REQUEST['celular'], 
								    $_REQUEST['correo'], 
								    $_REQUEST['password'],
								    $_REQUEST['radio'] 
											);
		if($res == true){
			header("location:../index.php?seccion=clientes&status=true");
		}else{
			header("location:../index.php?seccion=clientes&status=error");
		}
	}else{
		header("location:../index.php?seccion=clientes&status=error");
	}
}
/*-----------------ACTUALIZAR CLIENTE-------------------*/
if(isset($_REQUEST['hidden_update_cliente'])){

	if($_REQUEST['tipo'] != '-1' && !empty($_REQUEST['nombres']) && !empty($_REQUEST['password'])){
		 $empresa = (isset( $_REQUEST['empresa']))? $_REQUEST['empresa']:'';
		$res = $controller->actualizarCliente(
									$_REQUEST['cliente_id'],
									$_REQUEST['tipo'],
								    $empresa,
								    $_REQUEST['nombres'],
								    $_REQUEST['apellidos'], 
								    $_REQUEST['costo'],
								    $_REQUEST['supervisor'], 
								    $_REQUEST['celular'], 
								    $_REQUEST['correo'], 
								    $_REQUEST['password'],
								    $_REQUEST['radioedit']
												);
		if($res == true){
			header("location:../index.php?seccion=clientes&status=true");
		}else{
			header("location:../index.php?seccion=clientes&status=error");
		}
	}else{
		header("location:../index.php?seccion=clientes&status=error");
	}
}

if(isset($_REQUEST['orders'])){

	$data['page'] = (isset($_REQUEST['page']))?1:2;
	$data['per_page_inic'] = 10;
	$data['start'] = (isset($_REQUEST['page']))?($_REQUEST['page']-1)*$data['per_page_inic']:0;
	$clientesDAO = new clientesDAO();
	$data['order'] = ($_REQUEST['order'] == "asc")?2:1;

	$Res = $clientesDAO->listOrderBy($data);

	$count_new = count($Res);
	$paginacion = ceil(count($Res)/$data['per_page_inic']);

	$Res = ($count_new > 10)?array_slice($Res, 0, 10):$Res;
	$myData = array('Request' => $Res, 'TotalCount' => $count_new, 'Paginacion' => $paginacion);

	header('Content-Type: application/json');
	echo json_encode($myData);
}

/*----------- SEARCH AUTOCOMPLETE DESPACHO CELULARES---------------*/
if(isset($_REQUEST['autocom']) || isset($request->autocom)){

	if($_REQUEST['autocom']){
		$data['cliente_celular'] = $_REQUEST['q'];
	}else{
		$data['cliente_celular'] = $request->q;
	}

	$clientesDAO = new clientesDAO();
	$data['cliente_celular'] = $_REQUEST['q'];
	$res = $clientesDAO->filtrarClientesPorCelular($data); 
	$json = array();
	for ($x = 0; $x < count($res); $x++) {
	    array_push($json, array(
	        "label" => $res[$x]['cliente_celular'] . ' / ' .
	            utf8_encode($res[$x]['cliente_nombre'] . ' ' . $res[$x]['cliente_apellido']),
	        "value" => $res[$x]['cliente_celular'],
	        "idCliente" => $res[$x]['cliente_id'],
	        "cliente" => utf8_encode($res[$x]['cliente_nombre']),
	        "correo" => $res[$x]['cliente_correo'],
	        "celular" => $res[$x]['cliente_celular'],
	        "user" => $res[$x]['cliente_correo'],
	        "pass" => $res[$x]['cliente_password'],
	        "emp" => $res[$x]['empresa_id'],
	        "ape" => utf8_encode($res[$x]['cliente_apellido'])
	    ));
	}
	header('Content-Type: application/json');
    echo json_encode($json);
}
	
if(isset($_REQUEST['hidden_data'])){
	$clientesDAO = new clientesDAO();
	$alertaDAO = new alertaDAO();

	$res = $clientesDAO->listaClienteId($_REQUEST['codigo']);

	$data['alerta_tipo'] = $_REQUEST['tipo'];
	$data['alerta_latitud'] = $_REQUEST['lat'];
	$data['alerta_longitud'] = $_REQUEST['lng'];
	$data['alerta_personaid'] = $_REQUEST['codigo'];
	$data['alerta_flag'] = 0;

	$insertAlert = $alertaDAO->insertAlerta($data);

	$jsondata = array();
	foreach ($res as $key => $value) {
		$jsondata['clienteNombre'] = $value['cliente_nombre'].' '.$value['cliente_apellido'];
		$jsondata['clienteCorreo'] = $value['cliente_correo'];
		$jsondata['clienteCelular'] = $value['cliente_celular'];
	}
	
	header('Content-Type: application/json');
    echo json_encode($jsondata);
}

