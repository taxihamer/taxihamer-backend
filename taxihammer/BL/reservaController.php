<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once'../DAL/reservaDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/tipoServicioDAO.php';
require_once'../DAL/clientesDAO.php';
require_once'../DAL/lugaresDAO.php';

class reservaController{

	public function eliminarReserva($id){
		$data['reserva_id'] = $id;
		$reservaDAO = new reservaDAO();
		$result = $reservaDAO->deleteReserva($data);
		return $result;
	}

	public function insertarReserva($data){

		$reservaDAO = new reservaDAO();
		if($data[0]->cliente_id != 0 ){
			$data = $reservaDAO->insertReserva($data[0]);
			if($data['status'] = 1){
				$result = true;
				$id = $data['lastid'];
			}else{
				$result = false;
				$id = '';
			}
		}else{

			$clientesDAO = new clientesDAO();
			$data['cliente_nombre'] = $data[0]->cliente_nombre;
			$data['cliente_apellido'] = $data[0]->cliente_nombre;
			$data['cliente_correo'] = $data[0]->cliente_correo;
			$data['cliente_celular'] = $data[0]->cliente_celular;
			$data['cliente_costos'] = null;
			$data['cliente_supervisor'] = null;
			$data['cliente_password'] = 'App12345';
			$data['tipocliente_id'] = 1;
			$data['empresa_id'] = 0;
			$data['cliente_status'] = 1;
			$data['cliente_estados'] = 1;

			$cliente = $clientesDAO->insertarCliente($data);

			if($cliente['status'] == 1){
				$data[0]->cliente_id = $cliente['lastid'];
				$respta = $reservaDAO->insertReserva($data[0]);	
				if($respta['status'] = 1){
					$result = true;
					$id = $respta['lastid'];
				}else{
					$result = false;
					$id = '';
				}
			}
		}

		return array('status' => $result, 'lastid' => $id);
	}

	public function updateReserva($conductor_id, $reserva_id, $tiposervicio_id){
		$reservaDAO = new reservaDAO();
		$result = $reservaDAO->updateReserva($conductor_id, $reserva_id, $tiposervicio_id);
		return $result;
	}

	public function insertarConfigReserva($data){

		$reservaDAO = new reservaDAO();
		$data['configreserva_horas'] = $data[0]->configreserva_horas;
		$data['configreserva_alerta'] = $data[0]->configreserva_alerta;
		$data['configreserva_panel'] = $data[0]->configreserva_panel;
		$data['configreserva_bloqueo'] = $data[0]->configreserva_bloqueo;
		$res = $reservaDAO->insertConfigreserva($data);
		return $res;
	}
}

session_start();
$reservaController = new reservaController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

/*----------------- Listado de Reservas -------------------*/
if(isset($request->configuracion)){
	$flag = true;
	$reservaDAO = new reservaDAO();
	$send['configreserva_id'] = $request->config;
	$sw = (!empty($request->switch))?$request->switch:0;
	if($sw != 0){
		$data['configreserva_id'] = 0;
		$data['configreserva_status'] = 0;
		$res = $reservaDAO->UpdateConfigreserva($data);
		$send['configreserva_status'] = $sw;
	}else{
		$respta = $reservaDAO->CountConfigreserva($request->config);
		if($respta[0]['total'] > 0){
			$send['configreserva_status'] = 0;
		}else{
			$flag = false;
			$json['status'] = false;
			$json['msg'] = 'No puede desactivar este registro';
			header('Content-Type: application/json');
			echo json_encode($json);
		}
	}

	if($flag == true){
		$res = $reservaDAO->UpdateConfigreserva($send);
		if($res == true){
			$json['status'] = true;
			$json['msg'] = 'Su registro se actualizo correctamente';
			header('Content-Type: application/json');
			echo json_encode($json);
		}else{
			$json['status'] = false;
			$json['msg'] = '!Ups hubo un error al actulizar el registro';
			header('Content-Type: application/json');
			echo json_encode($json);
		}

	}
}

if(isset($request->lista_config_reserva)){

	$reservaDAO = new reservaDAO();
	$Res = $reservaDAO->listaConfigreserva();
	header('Content-Type: application/json');
	echo json_encode($Res);
}

if(isset($request->hidden_updateconfig_reserva)){

	$data['configreserva_id'] = $request->data[0]->configreserva_id;
	$data['configreserva_horas'] = $request->data[0]->configreserva_horas;
	$data['configreserva_alerta'] = $request->data[0]->configreserva_alerta;
	$data['configreserva_panel'] = $request->data[0]->configreserva_panel;
	$data['configreserva_bloqueo'] = $request->data[0]->configreserva_bloqueo;

	$reservaDAO = new reservaDAO();
	$resu = $reservaDAO->UpdateConfigreservaData($data);
	header('Content-Type: application/json');
	if($resu == true){
		$rpta['status'] = true;
	}else{
		$rpta['status'] = false;
	}
	echo json_encode($rpta);

}

if(isset($request->hidden_insertconfig_reserva)){

	$data['configreserva_id'] = 0;
	$data['configreserva_status'] = 0;

	$reservaDAO = new reservaDAO();
	$resu = $reservaDAO->UpdateConfigreserva($data);
	if($resu == true){
		$res = $reservaController->insertarConfigReserva($request->data);
		header('Content-Type: application/json');
		if($res == true){
			$rpta['status'] = true;
		}else{
			$rpta['status'] = false;
		}	
	}else{
		$rpta['status'] = false;
	}
	echo json_encode($rpta);

}

if(isset($request->listar_reserva)){

	$reservaDAO = new reservaDAO();
	$Res = $reservaDAO->lista();
	//echo '<pre>'; print_r($Res); echo '<pre>'; exit('reserva');
	header('Content-Type: application/json');
	echo json_encode($Res);
}

if(isset($request->delete_reserva)){
	$res = $reservaController->eliminarReserva($request->reserva_id);
	header('Content-Type: application/json');
	if($res == true){
		$rpta = true;
	}else{
		$rpta = false;
	}
	echo json_encode($rpta);
}

if(isset($request->droplistipo)){
	$tipoServicioDAO = new tipoServicioDAO();
	$result_tipo = $tipoServicioDAO->lista();
	header('Content-Type: application/json');
	echo json_encode($result_tipo);
}

if(isset($request->droplisConductor)){
	$conductorDAO = new conductorDAO();
	$data['tiposervicio_id'] = $request->selecteid;
	$resul = $conductorDAO->findAllConductorSelect($data);
	header('Content-Type: application/json');
	echo json_encode($resul);
}

if(isset($request->search)){
	$clientesDAO = new clientesDAO();
	$res = $clientesDAO->filtrarClientes();
	header('Content-Type: application/json');
	echo json_encode($res);
}

if(isset($request->hidden_lugar_tarifa)){
	$lugaresDAO = new lugaresDAO();
	$origen_id = $request->origen_id;
	$destino_id = $request->destino_id;
	$tiposervicio= $request->tiposervicio;
    $empresa_id = $request->empresa_id;
	$origen = $lugaresDAO->validarLugarId($origen_id);
	if($origen[0]['total'] == 1){
		$destino = $lugaresDAO->validarLugarId($destino_id);
		if($destino[0]['total'] == 1){
			$data['origen_id'] = $origen_id;
			$data['destino_id'] = $destino_id;
			$data['tiposervicio_id'] = $tiposervicio;
			$data['cliente_id'] = $empresa_id;
			$tarifa = $lugaresDAO->SolicitarTarifa($data);
			header('Content-Type: application/json');
			echo json_encode($tarifa);
		}
	}
}

if(isset($request->hidden_insert_reserva)){

	//echo '<pre>'; print_r($request); echo '</pre>'; exit;
	$res = $reservaController->insertarReserva($request->data);
	
	header('Content-Type: application/json');
	if($res['status'] == true){
		$rpta['status'] = true;
		$rpta['id'] = $res['lastid'];
	}else{
		$rpta['status'] = false;
	}
	echo json_encode($rpta);
}

if(isset($request->update_reserva)){

	$res = $reservaController->updateReserva($request->conductor_id, $request->reserva_id, $request->tiposervicio_id);
	header('Content-Type: application/json');

	if($res == true){
		$rpta = true;
		// //Envia
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"http://gaimaapp.tutaxidispatch.com/mailer_reserva.php");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,"reserva_id=".$request->reserva_id);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);

		curl_close($ch);
	}else{
		$rpta = false;
	}
	echo json_encode($rpta);
}

if(isset($request->update_estado_reserva)){

	$data['reserva_id'] = $request->data->reserva_id;
	$data['estado'] = $request->data->estado;

	$reservaDAO = new reservaDAO();
	$Res = $reservaDAO->cancelarReserva($data);
	if($Res == true){
		$data = $reservaDAO->updateReservaConductor($data['reserva_id']);
	}else{
		$data = false;
	}
	
	header('Content-Type: application/json');
	echo json_encode($data);

}
