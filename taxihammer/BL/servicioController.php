<?php

require_once'../DAL/servicioDAO.php';
require_once'../DAL/clientesDAO.php';
require_once'../DAL/lugaresDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/vehiculoDAO.php';
require_once'../DAL/servicioDAO.php';
require_once'../DAL/lugaresDAO.php';
require_once'../DAL/alertaDAO.php';
require_once'../DAL/generalDAO.php';

class servicioController{

	public function agregarServicio($cliente,$conductor,$tiposervicio,$origenid,$origename,$origendirc,$origenlat,$origenlng,$destinoid,$destinoname,$destinodirc,$destinolat,$destinolng,$tipopago,$tarifa,$estado,$seleccion,$horainicio){

		$data['cliente_id'] = $cliente;
		$data['conductor_id'] = $conductor;
		$data['tiposervicio_id'] = $tiposervicio;
		$data['origen_id'] = $origenid;
		$data['origen_nombre'] = ($origename)?$origename:'';
		$data['origen_direccion'] = $origendirc;
		$data['origen_lat'] = $origenlat;
		$data['origen_lon'] = $origenlng;
		$data['destino_id'] = $destinoid;
		$data['destino_nombre'] = ($destinoname)?$destinoname:'';
		$data['destino_direccion'] = $destinodirc;
		$data['destino_lat'] = $destinolat;
		$data['destino_lon'] = $destinolng;
		$data['tipo_pago'] = $tipopago;
		$data['tarifa'] = str_replace(',','.',number_format($tarifa, 2, ',', ' '));
		$data['estado'] = $estado;
		$data['fecha_seleccion'] = $seleccion;
		$data['hora_inicio'] = $horainicio;
		$data['flag_reserva'] = 0;
		$data['origen_referencia'] = '';
		$data['destino_referencia']= '';
		$data['imagen'] = '';
   		$data['comentario'] = '';
		//var_dump($data);exit;
		$servicioDAO = new servicioDAO();
		$result = $servicioDAO->insertarServicio($data);

		if($result[0]['p_return_code'] != '-1' || $result[0]['p_return_code'] != '-2'){
			
			return array('status' => true, 'lastid' => $result[0]['p_return_code']);
		}
	}
	
}

session_start();
$controller = new servicioController();
/*-----------------INSERTAR MENU-------------------*/
if(isset($_POST['hidden_servicio'])){
	//echo '<pre>'; print_r($_REQUEST); echo '</pre>'; exit('saul');
	$flag = $_POST['flag'];

	$clientesDAO = new clientesDAO();
	$conductorDAO = new conductorDAO();
	$generalDAO = new generalDAO();
	$config = $generalDAO->lista();
	$KiloZona = $config[0]['tipo_tarifa'];
	$socketconduc = $conductorDAO->listaConductorId($_REQUEST['idco']);

	if(empty($_REQUEST['idcl']) && empty($_REQUEST['empid'])){

			$data['cliente_nombre'] = $_REQUEST['cliente'];
			$data['cliente_apellido'] = $_REQUEST['apecliente'];
			$data['cliente_correo'] = $_REQUEST['correo'];
			$data['cliente_celular'] = $_REQUEST['celular'];
			$data['cliente_costos'] = null;
			$data['cliente_supervisor'] = null;
			$data['cliente_password'] = 'App12345';
			$data['tipocliente_id'] = 1;
			$data['empresa_id'] = 0;
			$data['cliente_status'] = 1;
			$data['cliente_estados'] = 1;
			$data['cliente_prueba'] = 0;

			$result = $clientesDAO->insertarCliente($data);
			if($result['status'] == 1){
				$lugaresDAO = new lugaresDAO();

				if($KiloZona != 2){
					$origen = $lugaresDAO->listabyin($_REQUEST['origenlugar']);
					$destino = $lugaresDAO->listabyin($_REQUEST['destinolugar']);
					$lugarorigen_descripcion = $origen[0]['lugar_descripcion'];
					$lugardestin_descripcion = $destino[0]['lugar_descripcion'];
					$origen_id = $_REQUEST['origenlugar'];
					$destino_id = $_REQUEST['destinolugar'];
				}else{
					$lugarorigen_descripcion = $_REQUEST['reforigen'];
					$lugardestin_descripcion = $_REQUEST['refdestinno'];
					$origen_id = 0;
					$destino_id = 0;
				}

				$seleccion = 2;
				$horainicio = 1;
				$res = $controller->agregarServicio(
									$result['lastid'],//cliente id
								    $_REQUEST['idco'],//conductor id
								    $_REQUEST['tiposervicio_id'],// tipo de servicio
								    $origen_id,//origen id 
								    $lugarorigen_descripcion,//origen nombre
								   	$_REQUEST['reforigen'],//origen direccion
								    $_REQUEST['reforilat'],//latitud origen
								    $_REQUEST['reforilng'],//longitud origen
								    $destino_id,// destino id
								    $lugardestin_descripcion,// destino nombre
								    $_REQUEST['refdestinno'],//destino direccion
								    $_REQUEST['refdeslat'],//latitud destino
								    $_REQUEST['refdeslng'],//longitud destino
								    $_REQUEST['tipopago'],// tipo de pago
								    $_REQUEST['costo'], // tarifa
								    0,//estado
								    $seleccion,
								    $horainicio);//fecha de seleccion

				
				if($res['status'] == true){
					$jsondata = array();
					header('Content-type: application/json; charset=utf-8');
					$rpta = $clientesDAO->listaClienteId($result['lastid']);

					$send['IdCliente']=$result['lastid']; 
					$send['latOrig']=$_REQUEST['reforilat']; 
					$send['latDest']=$_REQUEST['refdeslat']; 
					$send['lngOrig']=$_REQUEST['reforilng'];  
					$send['lngDest']=$_REQUEST['refdeslng']; 
					$send['Origen']=$origen_id; 
					$send['Destino']=$destino_id; 
					$send['dirOrigen']=$lugarorigen_descripcion; 
	                $send['dirDestino']=$lugardestin_descripcion; 
	                $send['refOrigen']=$_REQUEST['reforigen']; 
	                $send['refDestino']= $_REQUEST['refdestinno']; 
	                $send['tipoServicio']=$_REQUEST['tiposervicio_id']; 
	                $send['tarifa']=$_REQUEST['costo'];  
	                $send['socketCliente']=$rpta[0]['cliente_socket']; 
	                $send['socketConductor']=$socketconduc[0]['conductor_socket'];
	                $send['tipo_pago'] =$_REQUEST['tipopago']; 

	                $jsondata['status'] = true;
					$jsondata['servicioId'] = $res['lastid'];
					$jsondata['send'] = $send;
			        echo json_encode($jsondata);
				}else{
					$res['status'] = false;
					header('Content-Type: application/json');
			        echo json_encode($res);
				}
			}

	}else{

		$_REQUEST['origenlugar'] = ($KiloZona == 2)?'99999':$_REQUEST['origenlugar'];
		$_REQUEST['destinolugar'] = ($KiloZona == 2)?'99999':$_REQUEST['destinolugar'];
		//Cliente que existe
		if($_REQUEST['tiposervicio_id'] != '0' && !empty($_REQUEST['idco']) && !empty($_REQUEST['origenlugar']) && !empty($_REQUEST['destinolugar'])){	
				$lugaresDAO = new lugaresDAO();

				if($KiloZona != 2){
					$origen = $lugaresDAO->listabyin($_REQUEST['origenlugar']);
					$destino = $lugaresDAO->listabyin($_REQUEST['destinolugar']);
					$lugarorigen_descripcion = $origen[0]['lugar_descripcion'];
					$lugardestin_descripcion = $destino[0]['lugar_descripcion'];
					$origen_id = $_REQUEST['origenlugar'];
					$destino_id = $_REQUEST['destinolugar'];
				}else{
					$lugarorigen_descripcion = $_REQUEST['reforigen'];
					$lugardestin_descripcion = $_REQUEST['refdestinno'];
					$origen_id = 0;
					$destino_id = 0;
				}


				$seleccion = 2;
				$horainicio = 1;
				$res = $controller->agregarServicio(
									$_REQUEST['idcl'],//cliente id
								    $_REQUEST['idco'],//conductor id
								    $_REQUEST['tiposervicio_id'],// tipo de servicio
								    $origen_id,//origen id 
								    $lugarorigen_descripcion,//origen nombre
								   	$_REQUEST['reforigen'],//origen direccion
								    $_REQUEST['reforilat'],//latitud origen
								    $_REQUEST['reforilng'],//longitud origen
								    $destino_id,// destino id
								    $lugardestin_descripcion,// destino nombre
								    $_REQUEST['refdestinno'],//destino direccion
								    $_REQUEST['refdeslat'],//latitud destino
								    $_REQUEST['refdeslng'],//longitud destino
								    $_REQUEST['tipopago'],// tipo de pago
								    $_REQUEST['costo'], // tarifa
								    0,//estado
								    $seleccion,
								    $horainicio);//fecha de seleccion

				if($res['status'] == true){

					header('Content-Type: application/json');

					$jsondata = array();
					$rpta = $clientesDAO->listaClienteId($_REQUEST['idcl']);

					$send['IdCliente']=$_REQUEST['idcl']; 
					$send['latOrig']=$_REQUEST['reforilat']; 
					$send['latDest']=$_REQUEST['refdeslat']; 
					$send['lngOrig']=$_REQUEST['reforilng'];  
					$send['lngDest']=$_REQUEST['refdeslng']; 
					$send['Origen']=$origen_id; 
					$send['Destino']=$destino_id; 
					$send['dirOrigen']=$lugarorigen_descripcion; 
	                $send['dirDestino']=$lugardestin_descripcion; 
	                $send['refOrigen']=$_REQUEST['reforigen']; 
	                $send['refDestino']= $_REQUEST['refdestinno']; 
	                $send['tipoServicio']=$_REQUEST['tiposervicio_id']; 
	                $send['tarifa']=$_REQUEST['costo'];  
	                $send['socketCliente']=$rpta[0]['cliente_socket']; 
	                $send['socketConductor']=$socketconduc[0]['conductor_socket']; 
	                $send['tipo_pago'] =$_REQUEST['tipopago'];

	                $jsondata['status'] = true;
					$jsondata['servicioId'] = $res['lastid'];
					$jsondata['send'] = $send;
			        echo json_encode($jsondata);

				}else{

					$res = false;
					header('Content-Type: application/json');
			        echo json_encode($res);
				}
			}
	}
}

if(isset($_REQUEST['hidden_data'])){

	$servicioDAO = new servicioDAO();
	$clientesDAO = new clientesDAO();
	$conductorDAO = new conductorDAO();
	$vehiculoDAO = new vehiculoDAO();
	$lugaresDAO = new lugaresDAO();
	$alertaDAO = new alertaDAO();

	$service = $servicioDAO->listaServicio($_REQUEST['codigo']);
	$cliente = $clientesDAO->listaClienteId($service[0]['cliente_id']);
	$conduct = $conductorDAO->listaConductorId($service[0]['conductor_id']);
	$unidad = $vehiculoDAO->listaBy($conduct[0]['unidad_id']);


	$data['alerta_tipo'] = $_REQUEST['tipo'];
	$data['alerta_latitud'] = $_REQUEST['lat'];
	$data['alerta_longitud'] = $_REQUEST['lng'];
	$data['alerta_personaid'] = $_REQUEST['codigo'];
	$data['alerta_unidadid'] = $unidad[0]['unidad_id'];
	$data['alerta_flag'] = $_REQUEST['flag'];

	$insertAlert = $alertaDAO->insertAlerta($data);

	$jsondata = array();
	foreach ($service as $key => $value) {

		$jsondata['servicio_id'] = $value['servicio_id'];
		$lugarorigen = $lugaresDAO->listabyin($value['origen_id']);
		$lugardestino = $lugaresDAO->listabyin($value['destino_id']);
		foreach ($lugarorigen as $row) {
			$jsondata['origen_nombre'] = $row['lugar_descripcion'];
		}
		foreach ($lugardestino as $row) {
			$jsondata['destino_nombre'] = $row['lugar_descripcion'];
		}
		$jsondata['clienteNombre'] = $cliente[0]['cliente_nombre'].' '.$cliente[0]['cliente_apellido'];
		$jsondata['clienteCorreo'] = $cliente[0]['cliente_correo'];
		$jsondata['clienteCelular'] = $cliente[0]['cliente_celular'];
		$jsondata['conductorNombre'] = $conduct[0]['conductor_nombre'].' '.$conduct[0]['conductor_apellido'];
		$jsondata['conductorCorreo'] = $conduct[0]['conductor_correo'];
		$jsondata['conductorDni'] = $conduct[0]['conductor_dni'];
		$jsondata['unidadtiposervi'] = $unidad[0]['tiposervicio_nombre'];
		$jsondata['unidadAlias'] = $unidad[0]['unidad_alias'];
		$jsondata['unidadPlaca'] = $unidad[0]['unidad_placa'];
		$jsondata['unidadModelo'] = $unidad[0]['unidad_modelo'];
		$jsondata['unidadMarca'] = $unidad[0]['unidad_marca'];
		$jsondata['unidadColor'] = $unidad[0]['unidad_color'];
		$jsondata['unidadFabrica'] = $unidad[0]['unidad_fabricacion'];
		$jsondata['unidadFoto'] = $unidad[0]['unidad_foto'];
	}

	header('Content-Type: application/json');
    echo json_encode($jsondata);
}
