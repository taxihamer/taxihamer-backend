<?php
require_once'../DAL/tarifarioDAO.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class configuracionController{

	public function lista(){
		$usuarioDAO = new usuarioDAO();
		$res = $usuarioDAO->lista();
		return $res;
	}
	
	public function insertarConfigHoras($data){
		$tarifarioDAO = new tarifarioDAO();
		$res = $tarifarioDAO->insertarConfigHoras($data);
		return $res;
	}

	public function UpdateConfigHoras($data){
		$tarifarioDAO = new tarifarioDAO();
		$res = $tarifarioDAO->UpdateConfigHoras($data);
		return $res;
	}
}

$controller = new configuracionController();


if(isset($_REQUEST['hidden_read'])){

	$tarifarioDAO = new tarifarioDAO();
	$res = $tarifarioDAO->listaConfigrango();
	if(!empty($res)){
		foreach ($res as $key => $rowConfig) {
			$res[$key]['rango_estado_new'] = ($rowConfig['rango_estado'] == 1)?'Habilitado':'Desabilitado';
		}
	}

	header('Content-Type: application/json');
    echo json_encode($res);
}

if (isset($_REQUEST['hidden_ll_config'])){
	$rangohora_id =  (int)$_REQUEST['ID'];
	$tarifarioDAO = new tarifarioDAO();
	$res = $tarifarioDAO->listaConfigrangoId($rangohora_id);
	header('Content-Type: application/json');
    echo json_encode($res);
}

if(isset($_REQUEST['config_insert'])){

	if(!empty($_REQUEST['result'])){
		$array = explode(",", $_REQUEST['result']);

		$data['rango_hinicio'] = $_REQUEST['horainicio'];
		$data['rango_finicio'] = $_REQUEST['horafinal'];

		$tarifarioDAO = new tarifarioDAO();
		$validHora = $tarifarioDAO->ValidarHoraRango($data['rango_hinicio'],$data['rango_finicio']);
		if($validHora[0]['total'] == 0){

			$data['rango_lun'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_1']))?$_REQUEST['currency_1']:0);
			$data['rango_mar'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_2']))?$_REQUEST['currency_2']:0);
			$data['rango_mie'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_3']))?$_REQUEST['currency_3']:0);
			$data['rango_jue'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_4']))?$_REQUEST['currency_4']:0);
			$data['rango_vie'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_5']))?$_REQUEST['currency_5']:0);
			$data['rango_sab'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_6']))?$_REQUEST['currency_6']:0);
			$data['rango_dom'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_7']))?$_REQUEST['currency_7']:0);
			$data['rango_check'] = json_encode($array);
			
			$res = $controller->insertarConfigHoras($data);

			if($res == true){
				header("location:../index.php?seccion=rangohoras&status=true");
			}else{
				header("location:../index.php?seccion=rangohoras&status=error");
			}
		}else{
			header("location:../index.php?seccion=rangohoras&status=validar");
		}
	}else{
		header("location:../index.php?seccion=rangohoras&status=bool");
	}
}

if(isset($_REQUEST['config_update'])){
	
	if(!empty($_REQUEST['chil'])){
		//$array = implode(",", $_REQUEST['chil']);
		$data['rango_estado'] = (isset($_REQUEST['my-checkbox']))?1:0;
		$data['rango_hinicio'] = $_REQUEST['horainicioEdit'];
		$data['rango_finicio'] = $_REQUEST['horafinalEdit'];
		$data['rango_lun'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_1']))?$_REQUEST['currency_1']:0);
		$data['rango_mar'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_2']))?$_REQUEST['currency_2']:0);
		$data['rango_mie'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_3']))?$_REQUEST['currency_3']:0);
		$data['rango_jue'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_4']))?$_REQUEST['currency_4']:0);
		$data['rango_vie'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_5']))?$_REQUEST['currency_5']:0);
		$data['rango_sab'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_6']))?$_REQUEST['currency_6']:0);
		$data['rango_dom'] = (isset($_REQUEST['currency_99']))?$_REQUEST['currency_99']:((isset($_REQUEST['currency_7']))?$_REQUEST['currency_7']:0);
		$data['rango_check'] = json_encode($_REQUEST['chil']);
		$data['rangohora_id'] = $_REQUEST['rango_id'];
			
		$res = $controller->UpdateConfigHoras($data);

		if($res == true){
			header("location:../index.php?seccion=rangohoras&status=true");
		}else{
			header("location:../index.php?seccion=rangohoras&status=error");
		}

	}else{
		header("location:../index.php?seccion=rangohoras&status=bool");
	}
}

if(isset($_REQUEST['delete_general'])){
	$rango_id = (int)$_REQUEST['Gid'];
	$tarifarioDAO = new tarifarioDAO();
	$resp = $tarifarioDAO->deleteRango($rango_id);

	if($resp == true){
		$json['status'] = true;
	}else{
		$json['status'] = false;
	}

	header('Content-Type: application/json');
	echo json_encode($json);
}	