<?php
require_once'../DAL/generalDAO.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class generalController{

	public function agregarGeneral($data){
		$general = new generalDAO();
		$res = $general->insertar($data);

		return $res;
	}

	public function updateGeneral($data){
		$general = new generalDAO();
		$res = $general->update($data);

		return $res;
	}

	public function ListarData($pagenum, $pagesize, $offset, $text){
		
		$generalDAO = new generalDAO();
		$count = $generalDAO->CountGeneral($text);
		return $count;
	}
}

$controller = new generalController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->lista_correos_modal)){

	$config_id = (int)$request->correo;
	$generalDAO = new generalDAO();
	$list = $generalDAO->listaBy($config_id);
	header('Content-Type: application/json');
	echo json_encode($list);
}

if(isset($request->lista_add_config)){

	$data['config_server_smtp'] = $request->servidor;
	$data['config_puerto_smtp'] = $request->puerto;
    $data['config_usuaro_smtp'] = $request->usuario;
    $data['config_password_smtp'] = $request->password;
    $data['config_id'] = 1;

    $generalDAO = new generalDAO();
    $result = $generalDAO->updateCorreo($data);
    if($result == true){
    	$json['status'] = true;
    	$json['mensaje'] = 'Registro Correcto';
    }else{
    	$json['status'] = false;
    	$json['mensaje'] = 'Error de BD';
    }

    header('Content-Type: application/json');
	echo json_encode($json);
}

if(isset($request->lista_correo)){
	$generalDAO = new generalDAO();
	$result = $generalDAO->listaBy(1);

    header('Content-Type: application/json');
	echo json_encode($result);
}

if(isset($_REQUEST['insert_config'])){

	$optRadio = $_REQUEST['optradio'];

		$data['email'] = $_REQUEST['correo'];
		$data['telefono'] = $_REQUEST['telefono'];
		$data['web'] = $_REQUEST['web'];
		$data['config_costo_co'] = $_REQUEST['costocancel'];
		$data['tipo_tarifa'] = $optRadio;

		$data['tarifa_minima'] = ($optRadio == 1)?0:$_REQUEST['tarifamin'];
		$data['factor_tarifa_km'] = ($optRadio == 1)?0:$_REQUEST['factkm'];
		$data['factor_tarifa_tiempo'] = ($optRadio == 1)?0:$_REQUEST['factime'];
		$data['tarifa_minima_m'] = ($optRadio == 1)?0:$_REQUEST['tarifaminima'];

		$data['factor_particular'] = ($optRadio == 1)?0:$_REQUEST['factparticular'];
		$data['factor_empresarial'] = ($optRadio == 1)?0:$_REQUEST['factempresarial'];
			
	$res = $controller->agregarGeneral($data);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
	if($res == true){
		header("location:../index.php?seccion=general&status=true");
	}else{
		header("location:../index.php?seccion=general&status=false");
	}
}

if(isset($_REQUEST['hidden_config_update'])){
	
	$checkboxFactorParticular = isset($_REQUEST['checkboxFactorEditParticular']) ? 1 : null;
	$checkboxFactorEmpresarial = isset($_REQUEST['checkboxFactorEditEmpresarial']) ? 1 : null;

	$data['config_comision_tipo'] = (int)$_REQUEST['comisionid'];
	$data['config_comision_total'] = (int)$_REQUEST['montoconductor'];

	$optRadio = $_REQUEST['optradioedit'];
	$data['email'] = $_REQUEST['correoedit'];
	$data['telefono'] = $_REQUEST['telefonoedit'];
	$data['web'] = $_REQUEST['webedit'];
	$data['config_costo_co'] = $_REQUEST['costocanceledit'];
	$data['aeropuertoO'] = (int)$_REQUEST['incraeroO'];
	$data['aeropuertoD'] = (int)$_REQUEST['incraeroD'];
	
	$data['tipo_tarifa'] = $optRadio;
	$data['config_id'] = $_REQUEST['id'];
	$data['tarifa_minima_m'] = ($optRadio == 1)?0:$_REQUEST['tarifaminimaedit'];
	

	$data['tarifa_minima'] = is_null($checkboxFactorParticular) ? 0 : $_REQUEST['tarifamineditParticular'];
	$data['factor_tarifa_km'] = is_null($checkboxFactorParticular) ? 0 :$_REQUEST['factkmeditParticular'];
	$data['factor_tarifa_tiempo'] = is_null($checkboxFactorParticular) ? 0 :$_REQUEST['factimeditParticular'];

	$data['factor_particular'] = is_null($checkboxFactorParticular)? 0 : 1;

	$data['factor_empresarial'] = is_null($checkboxFactorEmpresarial)? 0 :1;

	$data['tarifa_minima_empresarial'] = is_null($checkboxFactorEmpresarial) ? 0 :  $_REQUEST['tarifamineditEmpresarial'];
	$data['factor_tarifa_km_empresarial'] = is_null($checkboxFactorEmpresarial) ? 0 :  $_REQUEST['factkmeditEmpresarial'];
	$data['factor_tarifa_tiempo_empresarial'] = is_null($checkboxFactorEmpresarial) ? 0 :  $_REQUEST['factimeditEmpresarial'];



	$res = $controller->updateGeneral($data);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
	if($res == true){
		header("location:../index.php?seccion=general&status=true");
	}else{
		header("location:../index.php?seccion=general&status=false");
	}
}

if(isset($_REQUEST['radio'])){

	$valor = $_REQUEST['valor'];

	$general = new generalDAO();
	$res = $general->updateradio($valor);                                                                                                                                                                                              
	header('Content-Type: application/json');
    echo json_encode($res);
}

if(isset($request->list) && $request->list == 'lista_general'){

	$pagenum = $request->variables->page;
	$pagesize = $request->variables->size;
	$offset = ($pagenum - 1) * $pagesize;
	$searchText = $request->variables->search;

	$count = $controller->ListarData($pagenum, $pagesize, $offset, $searchText);
	$count_new = $count[0]['total'];

	$generalDAO = new generalDAO();
	$Res = $generalDAO->listaPaginacion($offset, $pagesize, $searchText);

	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);
}

if(isset($request->list) && $request->list == 'editado_general'){

	$config_id = (int)$request->variables->id;
	$generalDAO = new generalDAO();
	$data = $generalDAO->listaBy($config_id);
	header('Content-Type: application/json');
	echo json_encode($data[0]);

}