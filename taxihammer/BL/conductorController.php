<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once'../DAL/conductorDAO.php';
require_once'../DAL/vehiculoDAO.php';
require_once'../DAL/alertaDAO.php';
require_once'../DAL/constantes.php';

class conductorController{

	public function eliminarConductor($id){

		$conductorDAO = new conductorDAO();
		$data['conductor_id'] = $id;
		$unidad = $conductorDAO->validUnidadConductor($id);
		if(!empty($unidad[0]['unidad_id'])){
			// unidad_id = null 
			$unidad2 = $conductorDAO->updateUnidadConductor($id);
			$vehiculoDAO = new vehiculoDAO();
			$dato['unidad_asignada'] = 0;
			$dato['unidad_id'] = $unidad[0]['unidad_id'];
			$estado = $vehiculoDAO->updateUnidadAsignada($dato);
		}

		$res = $conductorDAO->deleteConductor($data);
		return $res;
	}

	public function insertarConductor($nombre, $apellido, $correo, $password, $dni, $celular, $ciudad, $distrito, $licencia, $catlicencia, $fecha, $foto, $unidadid, $soat, $carnet, $polarizada, $cci, $banco, $fotolicencia, $fotosoat,$prueba){
		$conductorDAO = new conductorDAO();
		$generalDAO = new generalDAO();
		$comision = $generalDAO->listaBy(1);
		$tipocomision = $comision[0]['config_comision_tipo'];
		$montocomision = $comision[0]['config_comision_total'];
		//$tipocomision = (!empty($tipocomision))?$tipocomision:0;
		//$montocomision = (!empty($montoComision))?$montoComision:0;

		$unidadid = (empty($unidadid))?0:(($unidadid != "-1")?$unidadid:0);

		$data['conductor_nombre'] = $nombre;
		$data['conductor_apellido'] = $apellido;
		$data['conductor_correo'] = $correo;
		$data['conductor_password'] = $password;
		$data['conductor_dni'] = $dni;
		$data['conductor_celular'] = $celular;
		$data['conductor_ciudad'] = $ciudad;
		$data['conductor_distrito'] = $distrito;
		$data['conductor_licencia'] = $licencia;
		$data['conductor_catlicencia'] = $catlicencia;
		$data['conductor_fechabrevete'] = $fecha;
		$data['conductor_foto'] = $foto;
		$data['conductor_comision'] = $tipocomision;
		$data['conductor_comision_total'] = $montocomision;
		$data['unidad_id'] = $unidadid;
		$data['conductor_foto_licencia'] = $fotolicencia;
    	$data['conductor_fotosoat'] = $fotosoat;
    	// $data['conductor_fotocarnetvial'] = $fotocarnet;
    	$data['conductor_fechasoat'] = $soat;
		$data['conductor_fechacarnetvial'] = $carnet;
		$data['conductor_polarizadas'] = $polarizada;
		$data['conductor_cci'] = $cci;
		$data['banco_id'] = $banco;
		$data['conductor_prueba'] = (int)$prueba;
		//echo '<pre>'; print_r($data); echo '</pre>'; exit('saul');

		$validar = $conductorDAO->validarCorreoCelular($correo,$celular);
		if($validar[0]['total'] > 0){
			$res = false;
		}else{
			
			if(!empty($unidadid)){
				$updateVehiculoActual = $this->actualizarUnidadConductorAsignada(1,(int)$unidadid);
			}

			$res = $conductorDAO->insertarConductor($data);
			
		}

		return $res;
	}
	
	public function actualizarConductor($foto,$foto1,$foto2,$listaUpdate,$send){

		$conductorDAO = new conductorDAO();
		$status = '';

		if($send != 1){

			foreach($listaUpdate as $rowlist)
		    {	
		    	$data['conductor_id'] = $rowlist->conductor_id;
		    	$data['conductor_fechabrevete'] = date('Y-m-d',strtotime($rowlist->conductor_fechabrevete));
		    	$data['conductor_nombre'] = $rowlist->conductor_nombre;
		    	$data['conductor_apellido'] = $rowlist->conductor_apellido;
		    	$data['conductor_correo'] = $rowlist->conductor_correo;
		    	$data['conductor_password'] = $rowlist->conductor_password;
		    	$data['conductor_dni'] = $rowlist->conductor_dni;
		    	$data['conductor_celular'] = $rowlist->conductor_celular;
		    	$data['conductor_ciudad'] = $rowlist->conductor_ciudad;
		    	$data['conductor_distrito'] = $rowlist->conductor_distrito;
		    	$data['conductor_licencia'] = $rowlist->conductor_licencia;
		    	$data['conductor_catlicencia'] = $rowlist->conductor_categoria;
		    	$data['conductor_comision'] = (isset($rowlist->conductor_comision))?$rowlist->conductor_comision:0;
		    	$data['conductor_comision_total'] = (isset($rowlist->conductor_comision_total))?$rowlist->conductor_comision_total:0;
		    	$unidadid = (isset($rowlist->unidad_id))?$rowlist->unidad_id:0;
		    	$data['conductor_foto'] = $foto;
		    	// $data['conductor_fotosoat'] = $foto1;
		    	// $data['conductor_fotocarnetvial'] = $foto2;
		    	$data['conductor_foto_licencia'] = $foto1;
		    	$data['conductor_fotosoat'] = $foto2;
		    	$data['conductor_fechasoat'] = date('Y-m-d',strtotime($rowlist->conductor_fechasoat));
        		$data['conductor_fechacarnetvial'] =  date('Y-m-d',strtotime($rowlist->conductor_fechacarnetvial));
        		$data['conductor_polarizadas'] = $rowlist->conductor_polarizadas;
        		$data['conductor_cci'] = $rowlist->conductor_cci;
        		$data['banco_id'] =$rowlist->banco_id;
		    }

			if($unidadid == '-1' || $unidadid == 0 ){

				$listConductor = $conductorDAO->listaConductorId($data['conductor_id']);
				if(!empty($listConductor)){
					$unidadIdAntes = $listConductor[0]['unidad_id'];
					if(!empty($unidadIdAntes)){
						$updateVehiculoAntes = $this->actualizarUnidadConductorAsignada(0,$unidadIdAntes);
					}
				}
				$status = 0;
				$unidadid = NUll;

			}else{

				$listConductor = $conductorDAO->listaConductorId($data['conductor_id']);
				if(!empty($listConductor)){
					$unidadIdAntes = $listConductor[0]['unidad_id'];

					if(!empty($unidadIdAntes)){
						if($unidadid != $unidadIdAntes){
							$updateVehiculoAntes = $this->actualizarUnidadConductorAsignada(0,$unidadIdAntes);
							$updateVehiculoActual = $this->actualizarUnidadConductorAsignada(1,$unidadid);
						}
					}else{
						$updateVehiculoActual = $this->actualizarUnidadConductorAsignada(1,$unidadid);
					}
				}

				$status = 1;

			}

			$data['conductor_status'] = $status;
			$data['unidad_id'] = $unidadid;
			$res = $conductorDAO->updateConductor($data);

			if($res == true){
				$list = $conductorDAO->listaBy($data['conductor_id']);
				$rpta = array(
					'status' =>$res, 
					'nombre' =>$list[0]['conductor_nombre'],
					'apellido' =>$list[0]['conductor_apellido'],
					'correo' =>$list[0]['conductor_correo'],
					'telefono' =>$list[0]['conductor_celular'],
					'alias' =>$list[0]['unidad_alias'],
					'foto' =>$list[0]['conductor_foto'],
					'calificacion' =>$list[0]['conductor_calificacion'],
					'estado' =>$list[0]['conductor_status'],
					'servicio' =>$list[0]['tiposervicio_nombre']
					);
			}else{
				$rpta = array('status' =>false); 
			}

		}else{

			foreach($listaUpdate as $rowlist)
		    {	
		    	$data['conductor_id'] = $rowlist->conductor_id;
		    	$data['conductor_fechabrevete'] = date('Y-m-d',strtotime($rowlist->conductor_fechabrevete));
		    	$data['conductor_nombre'] = $rowlist->conductor_nombre;
		    	$data['conductor_apellido'] = $rowlist->conductor_apellido;
		    	$data['conductor_correo'] = $rowlist->conductor_correo;
		    	$data['conductor_password'] = $rowlist->conductor_password;
		    	$data['conductor_dni'] = $rowlist->conductor_dni;
		    	$data['conductor_celular'] = $rowlist->conductor_celular;
		    	$data['conductor_ciudad'] = $rowlist->conductor_ciudad;
		    	$data['conductor_distrito'] = $rowlist->conductor_distrito;
		    	$data['conductor_licencia'] = $rowlist->conductor_licencia;
		    	$data['conductor_catlicencia'] = $rowlist->conductor_categoria;
		    	$data['conductor_foto'] = $foto;
		    	$data['conductor_fotosoat'] = $foto1;
		    	$data['conductor_fotocarnetvial'] = $foto2;
		    	$data['conductor_fechasoat'] = date('Y-m-d',strtotime($rowlist->conductor_fechasoat));
        		$data['conductor_fechacarnetvial'] =  date('Y-m-d',strtotime($rowlist->conductor_fechacarnetvial));
        		$data['conductor_polarizadas'] = $rowlist->conductor_polarizadas;
        		$data['conductor_cci'] = $rowlist->conductor_cci;
        		$data['banco_id'] =$rowlist->banco_id;
		    }

			$res = $conductorDAO->updateConductorEdit($data);
			$rpta = array('status' =>$res); 
				    
		}

		return $rpta;

	}
	public function actualizarUnidadConductorAsignada($status,$unidadid){
			
			$vehiculoDAO = new vehiculoDAO();
			$dato['unidad_asignada'] = (int)$status;
			$dato['unidad_id'] = (int)$unidadid;
			$estado = $vehiculoDAO->updateUnidadAsignada($dato);
				
		return array('unidadId'=>$unidadid,'status'=>$status);
	}

	public function actualizarConductorSinFoto($listaUpdate, $send){

		$conductorDAO = new conductorDAO();
		$status = '';

		if($send != 1){

			foreach($listaUpdate as $rowlist)
		    {	
		    	$data['conductor_id'] = $rowlist->conductor_id;
		    	//$data['conductor_fechabrevete'] = date('Y-m-d',strtotime($rowlist->conductor_fechabrevete));
		    	$data['conductor_fechabrevete'] = $rowlist->conductor_fechabrevete;
		    	$data['conductor_nombre'] = $rowlist->conductor_nombre;
		    	$data['conductor_apellido'] = $rowlist->conductor_apellido;
		    	$data['conductor_correo'] = $rowlist->conductor_correo;
		    	$data['conductor_password'] = $rowlist->conductor_password;
		    	$data['conductor_dni'] = $rowlist->conductor_dni;
		    	$data['conductor_celular'] = $rowlist->conductor_celular;
		    	$data['conductor_ciudad'] = $rowlist->conductor_ciudad;
		    	$data['conductor_distrito'] = $rowlist->conductor_distrito;
		    	$data['conductor_licencia'] = $rowlist->conductor_licencia;
		    	$data['conductor_catlicencia'] = $rowlist->conductor_categoria;
		    	$unidadid = (isset($rowlist->unidad_id))?$rowlist->unidad_id:null;
		    	$data['conductor_comision'] = (isset($rowlist->conductor_comision))?$rowlist->conductor_comision:0;
		    	$data['conductor_comision_total'] = (isset($rowlist->conductor_comision_total))?$rowlist->conductor_comision_total:0;
		    	//$data['conductor_fechasoat'] = date('Y-m-d',strtotime($rowlist->conductor_fechasoat));
		    	$data['conductor_fechasoat'] = $rowlist->conductor_fechasoat;
        		$data['conductor_fechacarnetvial'] =  $rowlist->conductor_fechacarnetvial;
        		//$data['conductor_fechacarnetvial'] =  date('Y-m-d',strtotime($rowlist->conductor_fechacarnetvial));
        		$data['conductor_polarizadas'] = $rowlist->conductor_polarizadas;
        		$data['conductor_cci'] = $rowlist->conductor_cci;
        		$data['banco_id'] =$rowlist->banco_id;

		    }
		    	
		    	$explode = explode('string:', $data['banco_id']);
		    	$data['banco_id'] = $explode[1];

			if(empty($unidadid)){

				$listConductor = $conductorDAO->listaConductorId($data['conductor_id']);
				if(!empty($listConductor)){
					$unidadIdAntes = $listConductor[0]['unidad_id'];
					if(!empty($unidadIdAntes)){
						$updateVehiculoAntes = $this->actualizarUnidadConductorAsignada(0,$unidadIdAntes);
					}
				}
				$status = 0;
				$unidadid = NUll;

			}else{

				$listConductor = $conductorDAO->listaConductorId($data['conductor_id']);
				if(!empty($listConductor)){
					$unidadIdAntes = $listConductor[0]['unidad_id'];

					if(!empty($unidadIdAntes)){
						if($unidadid != $unidadIdAntes){
							$updateVehiculoAntes = $this->actualizarUnidadConductorAsignada(0,$unidadIdAntes);
							$updateVehiculoActual = $this->actualizarUnidadConductorAsignada(1,$unidadid);
						}
					}else{
						$updateVehiculoActual = $this->actualizarUnidadConductorAsignada(1,$unidadid);
					}
				}

				$status = 1;

			}

			$data['conductor_status'] = $status;
			$data['unidad_id'] = $unidadid;

			$res = $conductorDAO->updateConductorSinFoto($data);

			if($res == true){
				$list = $conductorDAO->listaBy($data['conductor_id']);
				$rpta = array(
					'status' =>$res, 
					'nombre' =>$list[0]['conductor_nombre'],
					'apellido' =>$list[0]['conductor_apellido'],
					'correo' =>$list[0]['conductor_correo'],
					'telefono' =>$list[0]['conductor_celular'],
					'alias' =>$list[0]['unidad_alias'],
					'foto' =>$list[0]['conductor_foto'],
					'calificacion' =>$list[0]['conductor_calificacion'],
					'estado' =>$list[0]['conductor_status'],
					'servicio' =>$list[0]['tiposervicio_nombre']
					);
			}else{
				$rpta = array('status' =>false); 
			}
		}else{

			foreach($listaUpdate as $rowlist)
		    {	
		    	$data['conductor_id'] = $rowlist->conductor_id;
		    	$data['conductor_fechabrevete'] = date('Y-m-d',strtotime($rowlist->conductor_fechabrevete));
		    	$data['conductor_nombre'] = $rowlist->conductor_nombre;
		    	$data['conductor_apellido'] = $rowlist->conductor_apellido;
		    	$data['conductor_correo'] = $rowlist->conductor_correo;
		    	$data['conductor_password'] = $rowlist->conductor_password;
		    	$data['conductor_dni'] = $rowlist->conductor_dni;
		    	$data['conductor_celular'] = $rowlist->conductor_celular;
		    	$data['conductor_ciudad'] = $rowlist->conductor_ciudad;
		    	$data['conductor_distrito'] = $rowlist->conductor_distrito;
		    	$data['conductor_licencia'] = $rowlist->conductor_licencia;
		    	$data['conductor_catlicencia'] = $rowlist->conductor_categoria;
		    	$data['conductor_fechasoat'] = date('Y-m-d',strtotime($rowlist->conductor_fechasoat));
        		$data['conductor_fechacarnetvial'] =  date('Y-m-d',strtotime($rowlist->conductor_fechacarnetvial));
        		$data['conductor_polarizadas'] = $rowlist->conductor_polarizadas;
        		$data['conductor_cci'] = $rowlist->conductor_cci;
        		$data['banco_id'] =$rowlist->banco_id;

		    }

		    $res = $conductorDAO->updateConductorSinFotoEdit($data);
		    $rpta = array('status' =>$res);
		}
		return $rpta;
	}

	public function ListarData($pagenum, $pagesize, $offset, $search){
		
		if($search != ""){
		    $where = " AND concat(a.conductor_nombre,'',a.conductor_apellido) LIKE '%" . $search . "%'";
		} else {
		    $where = "";
		}

		$conductorDAO = new conductorDAO();
		$count = $conductorDAO->CountConductores($where);

		return $count;

	}
}

//Limpiar Espacios en blancos de una cadena
function ClearSpace($cadena){
  $cadena = str_replace(' ', '', $cadena);
  return $cadena;
}

session_start();
$controller = new conductorController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

//********Listar Conductor  IFRAME********//
if(isset($_REQUEST['iframe'])){

	$conductor_id = $_REQUEST['idConductor'];
	$conductorDAO = new conductorDAO();
	$Res = $conductorDAO->listaBy($conductor_id);
	header('Content-Type: application/json');
	echo json_encode($Res);
}
//********Listar Conductor con Angular********//
if(isset($request->lista_conductor)){

    try{
        $pagenum = $request->page;
        $pagesize = $request->size;
        $offset = ($pagenum - 1) * $pagesize;
        $search = $request->search;

        $count = $controller->ListarData($pagenum, $pagesize, $offset, $search);
        $count_new = $count[0]['total'];

        $conductorDAO = new conductorDAO();
        $Res = $conductorDAO->listaPaginacion($offset, $pagesize, $search);
        foreach ($Res as $key => $value) {
            $imagen = '../BL/'.$value['conductor_foto'];
            $exists = is_file($imagen);
            if($exists == true){
                $Res[$key]['conductor_foto'] = $value['conductor_foto'];
            }else{
                $Res[$key]['conductor_foto'] = IMG_DEFAULT1;
            }
        }

        $myData = array('Request' => $Res, 'totalCount' => $count_new);

        header('Content-Type: application/json');
        echo json_encode($myData);
    } catch (Exception $e) {
        echo $e->getMessage();
        throw $e;
    }

}

if(isset($request->listar_modal)){
	$conductor_id = $request->id;
	$conductorDAO = new conductorDAO();
	$Res = $conductorDAO->listaPaginacionModal($conductor_id);
	foreach ($Res as $key => $value) {
		$imagen = '../BL/'.$value['conductor_foto'];
		$exists = is_file($imagen);
		if($exists == true){
			$Res[$key]['conductor_foto'] = $value['conductor_foto'];
		}else{
			$Res[$key]['conductor_foto'] = IMG_DEFAULT1;
		}
	}

	header('Content-Type: application/json');
	echo json_encode($Res);
}

//********Eliminar Conductor *****************//
if(isset($_REQUEST['delete_general'])){

	$res = $controller->eliminarConductor((int)$_REQUEST['Gid']);
	$json['status'] = true;
	header('Content-Type: application/json');
   	echo json_encode($json);
}
//********Switch de activar Conductor ********//
if(isset($request->estado)){
	
	$conductorDAO = new conductorDAO();
	$unidad = $conductorDAO->validUnidadConductor($request->conductor);
	//echo '<pre>'; print_r($request); echo '</pre>';exit;
	$data['conductor_id'] = $request->conductor;
	$data['conductor_status'] = (!empty($request->switch))?$request->switch:0;
	//echo '<pre>'; print_r($data); echo '</pre>'; 
	if(!empty($unidad[0]['unidad_id'])){
		
		$res = $conductorDAO->actualizarEstado($data);
		$vehiculoDAO = new vehiculoDAO();
		$dato['unidad_asignada'] = 1;
		$dato['unidad_id'] = $unidad[0]['unidad_id'];
		$estado = $vehiculoDAO->updateUnidadAsignada($dato);
		if($res == true){
			$json['status'] = true;
			$json['mensaje'] = $data['conductor_status'];
			header('Content-Type: application/json');
        	echo json_encode($json);
		}else{
			$res = false;
			header('Content-Type: application/json');
	        echo json_encode($res);
		}

	}else{
		$res = false;
		header('Content-Type: application/json');
	    echo json_encode($res);
	}
}
/*-----------------INSERTAR CONDUCTOR-------------------*/
if(isset($_REQUEST['hidden_insert_conductor'])){

	$tamano = $_FILES['foto']['size'];
	$tipo = $_FILES['foto']['type'];
	$archivo = $_FILES['foto']['name'];
	$prefijo = substr(md5(uniqid(rand())),0,6);

		$archivo = '';$archivo1 = ''; $archivo2 = '';

	if(!empty($_FILES)){
		if(!empty($_FILES['foto']) && !empty($_FILES['foto6']) && !empty($_FILES['foto_7'])){

			$tamano = $_FILES['foto']['size'];
			$tipo = $_FILES['foto']['type'];
			$archivo = $_FILES['foto']['name'];
			$prefijo = substr(md5(uniqid(rand())),0,6);

			$tamano1 = $_FILES['foto6']['size'];
			$tipo1 = $_FILES['foto6']['type'];
			$archivo1 = $_FILES['foto6']['name'];
			$prefijo1 = substr(md5(uniqid(rand())),0,6);

			$tamano2 = $_FILES['foto_7']['size'];
			$tipo2 = $_FILES['foto_7']['type'];
			$archivo2 = $_FILES['foto_7']['name'];
			$prefijo2 = substr(md5(uniqid(rand())),0,6);

		}elseif (empty($_FILES['foto']) && !empty($_FILES['foto6']) && !empty($_FILES['foto_7'])) {

			$tamano1 = $_FILES['foto6']['size'];
			$tipo1 = $_FILES['foto6']['type'];
			$archivo1 = $_FILES['foto6']['name'];
			$prefijo1 = substr(md5(uniqid(rand())),0,6);

			$tamano2 = $_FILES['foto_7']['size'];
			$tipo2 = $_FILES['foto_7']['type'];
			$archivo2 = $_FILES['foto_7']['name'];
			$prefijo2 = substr(md5(uniqid(rand())),0,6);

		}elseif (!empty($_FILES['foto']) && empty($_FILES['foto6']) && !empty($_FILES['foto_7'])) {
			
			$tamano = $_FILES['foto']['size'];
			$tipo = $_FILES['foto']['type'];
			$archivo = $_FILES['foto']['name'];
			$prefijo = substr(md5(uniqid(rand())),0,6);	

			$tamano2 = $_FILES['foto_7']['size'];
			$tipo2 = $_FILES['foto_7']['type'];
			$archivo2 = $_FILES['foto_7']['name'];
			$prefijo2 = substr(md5(uniqid(rand())),0,6);

		}elseif (!empty($_FILES['foto']) && !empty($_FILES['foto6']) && empty($_FILES['foto_7'])) {
			
			$tamano = $_FILES['foto']['size'];
			$tipo = $_FILES['foto']['type'];
			$archivo = $_FILES['foto']['name'];
			$prefijo = substr(md5(uniqid(rand())),0,6);	

			$tamano1 = $_FILES['foto6']['size'];
			$tipo1 = $_FILES['foto6']['type'];
			$archivo1 = $_FILES['foto6']['name'];
			$prefijo1 = substr(md5(uniqid(rand())),0,6);

		}elseif (!empty($_FILES['foto']) && empty($_FILES['foto6']) && empty($_FILES['foto_7'])) {
			
			$tamano = $_FILES['foto']['size'];
			$tipo = $_FILES['foto']['type'];
			$archivo = $_FILES['foto']['name'];
			$prefijo = substr(md5(uniqid(rand())),0,6);		

		}elseif (empty($_FILES['foto']) && !empty($_FILES['foto6']) && empty($_FILES['foto_7'])) {
			
			$tamano1 = $_FILES['foto6']['size'];
			$tipo1 = $_FILES['foto6']['type'];
			$archivo1 = $_FILES['foto6']['name'];
			$prefijo1 = substr(md5(uniqid(rand())),0,6);

		}elseif (empty($_FILES['foto']) && empty($_FILES['foto6']) && !empty($_FILES['foto_7'])) {
			
			$tamano2 = $_FILES['foto_7']['size'];
			$tipo2 = $_FILES['foto_7']['type'];
			$archivo2 = $_FILES['foto_7']['name'];
			$prefijo2 = substr(md5(uniqid(rand())),0,6);

		}
	}

	/*$comision = (isset($_REQUEST['comisionid']))?1:2;
	if($comision == '1'){
		if($comision == true){
			$montoComision = $_REQUEST['montoconductor'];
		}else{
			$montoComision = 0;
		}
	}else{
		$_REQUEST['comisionid'] = 0;
		$montoComision = 0;
	}*/

	$condPrueba = (isset($_REQUEST['check_test']))?1:0;

	if ($archivo != '' || $archivo1 != '' || $archivo2 != '') {

		$archivo = ClearSpace(DelCharacter($archivo));
		$archivo1 = ClearSpace(DelCharacter($archivo1));
		$archivo2 = ClearSpace(DelCharacter($archivo2));
		// guardamos el archivo a la carpeta files
		$destino = (!empty($archivo))?'files/'.$prefijo.'_'.$archivo:null;
		$destino1 = (!empty($archivo1))?'soat/'.$prefijo1.'_'.$archivo1:null;
		$destino2 = (!empty($archivo2))?'vial/'.$prefijo2.'_'.$archivo2:null;

		$resdestino = (!empty($destino))?copy($_FILES['foto']['tmp_name'],$destino):false;
		$resdestino1 = (!empty($destino1))?copy($_FILES['foto6']['tmp_name'],$destino1):false;
		$resdestino2 = (!empty($destino2))?copy($_FILES['foto_7']['tmp_name'],$destino2):false;


		$res = $controller->insertarConductor($_REQUEST['nombre'], $_REQUEST['apellido'], $_REQUEST['correo'], $_REQUEST['password'], $_REQUEST['dni'], $_REQUEST['celular'], $_REQUEST['ciudad'], $_REQUEST['distrito'], $_REQUEST['licencia'], $_REQUEST['catlicencia'], $_REQUEST['fecha'], $destino, $_REQUEST['unidadid'], $_REQUEST['soatAdd'],''/*$_REQUEST['carnetvialAdd']*/,$_REQUEST['polarizadasAdd'],$_REQUEST['cii_bancoAdd'],$_REQUEST['select_defaultAdd'],$destino1,$destino2,$condPrueba);

// insertarConductor($nombre, $apellido, $correo, $password, $dni, $celular, $ciudad, $distrito, $licencia, $catlicencia, $fecha, $foto, $unidadid, $soat, $carnet, $polarizada, $cci, $banco, $fotolicencia, $fotosoat,$prueba)
		

			if($res == true){
				header("location:../index.php?seccion=conductores&status=true");
			}else{
				header("location:../index.php?seccion=conductores&status=false");
			}
		/*if (copy($_FILES['foto']['tmp_name'],$destino)) {
			$res = $controller->insertarConductor($_REQUEST['nombre'], $_REQUEST['apellido'], $_REQUEST['correo'], $_REQUEST['password'], $_REQUEST['dni'], $_REQUEST['celular'], $_REQUEST['ciudad'], $_REQUEST['distrito'], $_REQUEST['licencia'], $_REQUEST['catlicencia'], $_REQUEST['fecha'], $destino, $_REQUEST['unidadid'], $_REQUEST['comisionid'], $montoComision);
			if($res == true){
				header("location:../index.php?seccion=conductores&status=true");
			}else{
				header("location:../index.php?seccion=conductores&status=false");
			}
		}else {
			header("location:../index.php?seccion=conductores&status=false");
		}*/
	} else {
		header("location:../index.php?seccion=conductores&status=false");
	}
}
/*-----------------ACTUALIZAR CONDUCTOR-------------------*/
if(isset($_REQUEST['hidden_update_conductor'])){

	$send = (isset($_REQUEST['hidden_editiframe']))?1:2;
	$archivo = null; $archivo1 = null; $archivo2 = null;

	if(!empty($_FILES)){
		if(!empty($_FILES['file']) && !empty($_FILES['file1']) && !empty($_FILES['file2'])){

			$tamano = $_FILES['file']['size'];
			$tipo = $_FILES['file']['type'];
			$archivo = $_FILES['file']['name'];
			$prefijo = substr(md5(uniqid(rand())),0,6);	

			$tamano1 = $_FILES['file1']['size'];
			$tipo1 = $_FILES['file1']['type'];
			$archivo1 = $_FILES['file1']['name'];
			$prefijo1 = substr(md5(uniqid(rand())),0,6);

			$tamano2 = $_FILES['file2']['size'];
			$tipo2 = $_FILES['file2']['type'];
			$archivo2 = $_FILES['file2']['name'];
			$prefijo2 = substr(md5(uniqid(rand())),0,6);

		}elseif (empty($_FILES['file']) && !empty($_FILES['file1']) && !empty($_FILES['file2'])) {

			$tamano1 = $_FILES['file1']['size'];
			$tipo1 = $_FILES['file1']['type'];
			$archivo1 = $_FILES['file1']['name'];
			$prefijo1 = substr(md5(uniqid(rand())),0,6);

			$tamano2 = $_FILES['file2']['size'];
			$tipo2 = $_FILES['file2']['type'];
			$archivo2 = $_FILES['file2']['name'];
			$prefijo2 = substr(md5(uniqid(rand())),0,6);

		}elseif (!empty($_FILES['file']) && empty($_FILES['file1']) && !empty($_FILES['file2'])) {
			
			$tamano = $_FILES['file']['size'];
			$tipo = $_FILES['file']['type'];
			$archivo = $_FILES['file']['name'];
			$prefijo = substr(md5(uniqid(rand())),0,6);	

			$tamano2 = $_FILES['file2']['size'];
			$tipo2 = $_FILES['file2']['type'];
			$archivo2 = $_FILES['file2']['name'];
			$prefijo2 = substr(md5(uniqid(rand())),0,6);

		}elseif (!empty($_FILES['file']) && !empty($_FILES['file1']) && empty($_FILES['file2'])) {
			
			$tamano = $_FILES['file']['size'];
			$tipo = $_FILES['file']['type'];
			$archivo = $_FILES['file']['name'];
			$prefijo = substr(md5(uniqid(rand())),0,6);	

			$tamano1 = $_FILES['file1']['size'];
			$tipo1 = $_FILES['file1']['type'];
			$archivo1 = $_FILES['file1']['name'];
			$prefijo1 = substr(md5(uniqid(rand())),0,6);

		}elseif (!empty($_FILES['file']) && empty($_FILES['file1']) && empty($_FILES['file2'])) {
			
			$tamano = $_FILES['file']['size'];
			$tipo = $_FILES['file']['type'];
			$archivo = $_FILES['file']['name'];
			$prefijo = substr(md5(uniqid(rand())),0,6);		

		}elseif (empty($_FILES['file']) && !empty($_FILES['file1']) && empty($_FILES['file2'])) {
			
			$tamano1 = $_FILES['file1']['size'];
			$tipo1 = $_FILES['file1']['type'];
			$archivo1 = $_FILES['file1']['name'];
			$prefijo1 = substr(md5(uniqid(rand())),0,6);

		}elseif (empty($_FILES['file']) && empty($_FILES['file1']) && !empty($_FILES['file2'])) {
			
			$tamano2 = $_FILES['file2']['size'];
			$tipo2 = $_FILES['file2']['type'];
			$archivo2 = $_FILES['file2']['name'];
			$prefijo2 = substr(md5(uniqid(rand())),0,6);

		}
	}
	
	$listupdate = json_decode($_REQUEST['data']);

	if (!empty($archivo) || !empty($archivo1) || !empty($archivo2)) {
		// guardamos el archivo a la carpeta files

		$archivo = ClearSpace(DelCharacter($archivo));
		$archivo1 = ClearSpace(DelCharacter($archivo1));
		$archivo2 = ClearSpace(DelCharacter($archivo2));

		$destino = (!empty($archivo))?'files/'.$prefijo.'_'.$archivo:null;
		$destino1 = (!empty($archivo1))?'soat/'.$prefijo1.'_'.$archivo1:null;
		$destino2 = (!empty($archivo2))?'vial/'.$prefijo2.'_'.$archivo2:null;

		$resdestino = (!empty($destino))?copy($_FILES['file']['tmp_name'],$destino):false;
		$resdestino1 = (!empty($destino1))?copy($_FILES['file1']['tmp_name'],$destino1):false;
		$resdestino2 = (!empty($destino2))?copy($_FILES['file2']['tmp_name'],$destino2):false;

		$res = $controller->actualizarConductor($destino,$destino1,$destino2,$listupdate,$send);
			if($res['status'] == true){
				header('Content-Type: application/json');
        		echo json_encode($res);
			}else{
				header('Content-Type: application/json');
        		echo json_encode($res);
			}

		/*if (copy($_FILES['file']['tmp_name'],$destino)) {
			$res = $controller->actualizarConductor($destino,$destino1,$destino2,$listupdate,$send);
			if($res['status'] == true){
				header('Content-Type: application/json');
        		echo json_encode($res);
			}else{
				header('Content-Type: application/json');
        		echo json_encode($res);
			}
		} else {
			$res['status'] = false;
			header('Content-Type: application/json');
        	echo json_encode($res);
		}*/
	} else {
	
		$res = $controller->actualizarConductorSinFoto($listupdate,$send);
		if($res['status'] == true){
			header('Content-Type: application/json');
        	echo json_encode($res);
		}else{
			header('Content-Type: application/json');
        	echo json_encode($res);
		}
	}
}
/*-----------------DELETE CONDUCTOR-------------------*/
if(isset($_REQUEST['hidden_delete_conductor'])){
	$res = $controller->eliminarConductor($_REQUEST['id']);
	if($res == true){
		header('Content-Type: application/json');
        echo json_encode($res);
	}else{
		$res = false;
		header('Content-Type: application/json');
        echo json_encode($res);
	}
	/*if($res == true){
		header("location:../index.php?seccion=conductores&status=true");
	}else{
		header("location:../index.php?seccion=conductores&status=false");
	}*/
}
/*-----------------BUSQUEDA AJAX-------------------*/
if(isset($_REQUEST['buscar'])){
	$conductorDAO = new conductorDAO();
	$data['tiposervicio_id'] = $_REQUEST['tipo'];
	$data['conductor_nombre'] = $_REQUEST['nombre'];
	$Res = $conductorDAO->listBy($data);

	foreach($Res as $res){
          
  	?>
    <tr>
      <td><img src="BL/<?php echo $res['conductor_foto']; ?>" width="50px" class="img-circle" alt="taxi"></td>
      <td><?php echo $res['conductor_nombre']." ".$res['conductor_apellido']; ?></td>
      <td><?php echo $res['conductor_correo']; ?></td>
      <td><?php echo $res['conductor_celular']; ?></td>
      <td><?php echo $res['unidad_alias']; ?></td>
      <td><?php echo $res['tiposervicio_nombre']; ?></td>
      <td><?php echo $res['conductor_calificacion']; ?></td>
      <td><button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="llenar('<?php echo $res['conductor_id']; ?>','<?php echo $res['conductor_nombre']; ?>','<?php echo $res['conductor_apellido']; ?>','<?php echo $res['unidad_id']; ?>', '<?php echo $res['conductor_correo']; ?>', '<?php echo $res['conductor_password']; ?>', '<?php echo $res['conductor_dni']; ?>', '<?php echo $res['conductor_celular']; ?>', '<?php echo $res['conductor_ciudad']; ?>', '<?php echo $res['conductor_distrito']; ?>', '<?php echo $res['conductor_licencia']; ?>', '<?php echo $res['conductor_catlicencia']; ?>', '<?php echo $res['conductor_fechabrevete']; ?>', '<?php echo $res['conductor_foto']; ?>')"><i class="glyphicon glyphicon-edit"></i></button>
          <a class="btn btn-danger" href="BL/conductorController.php?hidden_delete_conductor=1&id=<?php echo $res['conductor_id']; ?>"><i class="glyphicon glyphicon-remove"></i></a>

      </td>
    </tr>
  <?php
  }
}

if(isset($_REQUEST['estado'])){
	$conductorDAO = new conductorDAO();
	$data['conductor_status'] = $_REQUEST['tipo'];
	$data['conductor_id'] = $_REQUEST['conductor'];

	$unidad = $conductorDAO->validUnidadConductor($_REQUEST['conductor']);

	if(!empty($unidad[0]['unidad_id'])){
		
		$res = $conductorDAO->actualizarEstado($data);
		$vehiculoDAO = new vehiculoDAO();
		$dato['unidad_asignada'] = 1;
		$dato['unidad_id'] = $unidad[0]['unidad_id'];
		$estado = $vehiculoDAO->updateUnidadAsignada($dato);
		if($res == true){
			header('Content-Type: application/json');
        	echo json_encode($res);
		}else{
			$res = false;
			header('Content-Type: application/json');
	        echo json_encode($res);
		}

	}else{
		$res = false;
		header('Content-Type: application/json');
	    echo json_encode($res);
	}
}

if(isset($_REQUEST['hidden_data'])){
	$conductorDAO = new conductorDAO();
	$vehiculoDAO = new vehiculoDAO();
	$alertaDAO = new alertaDAO();

	$res = $conductorDAO->listaConductorId($_REQUEST['codigo']);
	$unidad = $vehiculoDAO->listaBy($res[0]['unidad_id']);


	$data['alerta_tipo'] = $_REQUEST['tipo'];
	$data['alerta_latitud'] = $_REQUEST['lat'];
	$data['alerta_longitud'] = $_REQUEST['lng'];
	$data['alerta_personaid'] = $_REQUEST['codigo'];
	$data['alerta_unidadid'] = $unidad[0]['unidad_id'];
	$data['alerta_flag'] = 0;

	$insertAlert = $alertaDAO->insertAlerta($data);

	$jsondata = array();
	foreach ($res as $key => $value) {

		$jsondata['conductorNombre'] = $value['conductor_nombre'].' '.$value['conductor_apellido'];
		$jsondata['conductorCorreo'] = $value['conductor_correo'];
		$jsondata['conductorDni'] = $value['conductor_dni'];
		$jsondata['conductorFoto'] = $value['conductor_foto'];
		$jsondata['unidadtiposervi'] = $unidad[0]['tiposervicio_nombre'];
		$jsondata['unidadAlias'] = $unidad[0]['unidad_alias'];
		$jsondata['unidadPlaca'] = $unidad[0]['unidad_placa'];
		$jsondata['unidadModelo'] = $unidad[0]['unidad_modelo'];
		$jsondata['unidadMarca'] = $unidad[0]['unidad_marca'];
		$jsondata['unidadColor'] = $unidad[0]['unidad_color'];
		$jsondata['unidadFabrica'] = $unidad[0]['unidad_fabricacion'];
		$jsondata['unidadFoto'] = $unidad[0]['unidad_foto'];
	}

	header('Content-Type: application/json');
    echo json_encode($jsondata);
}



