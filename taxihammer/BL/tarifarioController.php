<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once'../DAL/tarifarioDAO.php';
require_once'../DAL/lugaresDAO.php';
$lugaresDAO = new lugaresDAO();
$result_lugares = $lugaresDAO->lista();
class tarifarioController{

	public function eliminarTarifario($id){
		$tarifarioDAO = new tarifarioDAO();
		$data['tarifario_id'] = $id;
		$res = $tarifarioDAO->deleteTarifario($data);
		return $res;
	}
	public function insertarTarifario($clienteid, $tiposervicioid, $origenid, $destinoid, $monto){
		
		$tarifarioDAO = new tarifarioDAO();

		$data['cliente_id'] = $clienteid;
		$data['tiposervicio_id'] = $tiposervicioid;
		$data['origen_id'] = $origenid;
		$data['destino_id'] = $destinoid;
		$data['tarifario_monto'] = $monto;

		$valid = $tarifarioDAO->validarTarifa($data);
		
		if($valid[0]['Total'] > 0){
			$res = false;
		}else{
			$res = $tarifarioDAO->insertarTarifario($data);
			
		}
	
		return $res;
	}

	public function actualizarTarifario($clienteid, $tiposervicioid, $origenid, $destinoid, $monto, $id){
		$tarifarioDAO = new tarifarioDAO();
		$data['cliente_id'] = $clienteid;
		$data['tiposervicio_id'] = $tiposervicioid;
		$data['origen_id'] = $origenid;
		$data['destino_id'] = $destinoid;
		$data['tarifario_monto'] = $monto;
		$data['tarifario_id'] = $id;
		
		$res = $tarifarioDAO->updateTarifario($data);
		return $res;
	}
	public function filtrar($origen, $destino, $empresa, $tiposervicio, $start, $per_page){
		$tarifarioDAO = new tarifarioDAO();
		$data['cliente'] = $empresa;
		$data['tiposervicio'] = $tiposervicio;
		$data['origen'] = $origen;
		$data['destino'] = $destino;
		$data['start'] = $start;
		$data['per_page'] = $per_page;
		$res = $tarifarioDAO->listBy($data);
		return $res;
	}

	public function filtrarPaginacion($origen, $destino, $empresa, $tiposervicio){
		$tarifarioDAO = new tarifarioDAO();
		$data['cliente'] = $empresa;
		$data['tiposervicio'] = $tiposervicio;
		$data['origen'] = $origen;
		$data['destino'] = $destino;
		$res = $tarifarioDAO->listByFiltro($data);
		return $res;
	}
}
$controller = new tarifarioController();
//echo '<pre>'; print_r($_REQUEST); echo '</pre>'; exit('kendo');
/*-----------------ELIMINAR TARIFARIO-------------------*/
if(isset($_REQUEST['hidden_delete_tarifario'])){
	$res = $controller->eliminarTarifario($_REQUEST['id']);
	if($res == true){
		header("location:../index.php?seccion=tarifario&status=true");
	}else{
		header("location:../index.php?seccion=tarifario&status=false");
	}
}
/*-----------------INSERTAR TARIFARIO-------------------*/
if(isset($_REQUEST['hidden_tarifario_insert'])){
	
	$res = $controller->insertarTarifario($_REQUEST['cliente_id'],$_REQUEST['tiposervicio_id'],$_REQUEST['origen_id'],$_REQUEST['destino_id'],$_REQUEST['monto']);
	if($res == true){
		header("location:../index.php?seccion=tarifario&status=true");
	}else{
		header("location:../index.php?seccion=tarifario&status=error");
	}
	
}
/*-----------------ACTUALIZAR TARIFARIO-------------------*/
if(isset($_REQUEST['hidden_tarifario_update'])){
	
	$res = $controller->actualizarTarifario($_REQUEST['cliente_id1'],$_REQUEST['tiposervicio_id1'],$_REQUEST['origen_id1'],$_REQUEST['destino_id1'],$_REQUEST['monto1'], $_REQUEST['tarifario_id']);
	if($res == true){
		header("location:../index.php?seccion=tarifario&status=true");
	}else{
		header("location:../index.php?seccion=tarifario&status=error");
	}
	
}
/*-----------------FILTRO TARIFARIO-------------------*/
if(isset($_REQUEST['hidden_tarifario_buscar'])){
	$start = 0;
	$per_page = 400;
	$result_tarifario = $controller->filtrar($_REQUEST['origen'], $_REQUEST['destino'], $_REQUEST['cliente'], $_REQUEST['servicio'], $start, $per_page);
	foreach($result_tarifario as $res){
          
      	?>
        <tr>
          <td></td>
          <td><?php echo $res['empresa_razonsocial']; ?></td>
          <td>
          <?php 
          foreach($result_lugares as $resu){
            if($res['origen_id'] == $resu['lugares_id']){
              echo $resu['lugar_descripcion'];
            }
          }
          ?></td>
          <td>
          <?php 
          foreach($result_lugares as $resu){
            if($res['destino_id'] == $resu['lugares_id']){
              echo $resu['lugar_descripcion'];
            }
          }
          ?>
          </td>
          <td><?php echo $res['tiposervicio_nombre']; ?></td>
          <td><?php echo $res['tarifario_monto']; ?></td>
          
          <td><button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="llenar('<?php echo $res['tarifario_id']; ?>', '<?php echo $res['cliente_id']; ?>', '<?php echo $res['origen_id']; ?>','<?php echo $res['destino_id']; ?>','<?php echo $res['tiposervicio_id']; ?>','<?php echo $res['tarifario_monto']; ?>')"><i class="glyphicon glyphicon-edit"></i></button>
              <a href="../BL/tarifarioController.php?hidden_delete_tarifario=1&id=<?php echo $res['tarifario_id']; ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>

          </td>
        </tr>
      <?php
      }
}


/*-------------------Paginacion------------------------------*/
if(isset($_REQUEST['hidden_tarifario_paginacion'])){

	$page = $_REQUEST['page'];
	$per_page = 400;
	$start = ($page-1)*$per_page;
	$tarifarioDAO = new tarifarioDAO();
	if($_REQUEST['origen'] == 'todos' && $_REQUEST['destino'] == 'todos' && $_REQUEST['servicio'] == 'todos' && $_REQUEST['cliente'] == 'todos'){
		$result_tarifario = $tarifarioDAO->listaPaginacion($start,$per_page);	
	}else{
		$result_tarifario = $controller->filtrar($_REQUEST['origen'], $_REQUEST['destino'], $_REQUEST['cliente'], $_REQUEST['servicio'],$start,$per_page);
	}


	foreach($result_tarifario as $res){ ?>
        <tr>
          <td></td>
          <td><?php echo $res['empresa_razonsocial']; ?></td>
          <td>
          <?php 
          foreach($result_lugares as $resu){
            if($res['origen_id'] == $resu['lugares_id']){
              echo $resu['lugar_descripcion'];
            }
          }
          ?></td>
          <td>
          <?php 
          foreach($result_lugares as $resu){
            if($res['destino_id'] == $resu['lugares_id']){
              echo $resu['lugar_descripcion'];
            }
          }
          ?>
          </td>
          <td><?php echo $res['tiposervicio_nombre']; ?></td>
          <td><?php echo $res['tarifario_monto']; ?></td>
          
          <td><button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="llenar('<?php echo $res['tarifario_id']; ?>', '<?php echo $res['cliente_id']; ?>', '<?php echo $res['origen_id']; ?>','<?php echo $res['destino_id']; ?>','<?php echo $res['tiposervicio_id']; ?>','<?php echo $res['tarifario_monto']; ?>')"><i class="glyphicon glyphicon-edit"></i></button>
              <a href="../BL/tarifarioController.php?hidden_delete_tarifario=1&id=<?php echo $res['tarifario_id']; ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>

          </td>
        </tr>
      <?php }
}

if(isset($_REQUEST['hidden_tarifario_buscar_paginacion'])){
	$start = 0;
	$per_page = 400;
	if($_REQUEST['origen'] == 'todos' && $_REQUEST['destino'] == 'todos' && $_REQUEST['servicio'] == 'todos' && $_REQUEST['cliente'] == 'todos'){
		$tarifarioDAO = new tarifarioDAO();
		$result_tarifario = $tarifarioDAO->lista();
	}else{
		$result_tarifario = $controller->filtrarPaginacion($_REQUEST['origen'], $_REQUEST['destino'], $_REQUEST['cliente'], $_REQUEST['servicio']);
	}

	$paginacion = ceil(count($result_tarifario)/$per_page);
	header('Content-Type: application/json');
    echo json_encode($paginacion);
}



