<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once'../DAL/estadocuentaDAO.php';
require_once'../DAL/conductorDAO.php';
require_once'../DAL/vehiculoDAO.php';
require_once'../DAL/servicioDAO.php';
require_once'../DAL/constantes.php';

class stactaController{

}

session_start();
$controller = new stactaController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($_REQUEST['filter']['filters'][0]['field'])){
 	if($_REQUEST['filter']['filters'][0]['field'] == $_REQUEST['filter']['filters'][0]['value']){
 		$conductorDAO = new conductorDAO();
 		$vehiculoDAO = new vehiculoDAO();

 		$searchText = isset($_REQUEST['filter']['filters'][1]['value'])?$_REQUEST['filter']['filters'][1]['value']:'';
 		$searchText = trim($searchText);
 		
 		$idConductores = $conductorDAO->listardistincConductor($searchText);
 		if(!empty($idConductores)){
 			foreach ($idConductores as $key => $rowId) {
 				$datosConductor = $conductorDAO->listaConductorId($rowId['conductor_id']);
 				foreach ($datosConductor as $rowdatos) {
 					$datosUnidad = $vehiculoDAO->listaBy($rowdatos['unidad_id']);
 					if(empty($datosUnidad)){
 						$idConductores[$key]['unidad'] = 'Falta unidad Asignar';
 					}else{
 						$idConductores[$key]['unidad'] = $datosUnidad[0]['unidad_alias'].' '.$datosUnidad[0]['unidad_placa'];
 					}

 					$idConductores[$key]['conductor_nombre'] = DelCharacter($rowdatos['conductor_nombre']).' '.DelCharacter($rowdatos['conductor_apellido']);
 					$idConductores[$key]['conductor_correo'] = $rowdatos['conductor_correo'];
 					$idConductores[$key]['conductor_dni'] = $rowdatos['conductor_dni'];
 					$idConductores[$key]['conductor_celular'] = $rowdatos['conductor_celular'];
 					$idConductores[$key]['conductor_recarga'] = 'S/. '.round($rowdatos['conductor_recarga'],2);

 				}
 			}

 			//echo '<pre>'; print_r($idConductores); echo '</pre>'; exit;
 			header('Content-Type: application/json');
			echo json_encode($idConductores);

 		}
 	}elseif ($_REQUEST['filter']['filters'][0]['field'] == 'conductor_id') {

 		$conductor_id = $_REQUEST['filter']['filters'][0]['value'];
 		$estadocuentaDAO = new estadocuentaDAO();
 		$servicioDAO = new servicioDAO();
 		$estados = $estadocuentaDAO->listaEstadoCuenta($conductor_id);

 		if(!empty($estados)){
 			foreach ($estados as $key => $rowEstados) {

 				$tipo = ($rowEstados['cta_tipo'] == 1)?'Recarga':(($rowEstados['cta_tipo'] != 2)?'Liquidacion':'Servicio');

 				if($rowEstados['cta_tipo'] == 1 || $rowEstados['cta_tipo'] == 3){

 					$monto = round($rowEstados['cta_monto'],2);
 					$tipopago = 'Otros';
 					$estados[$key]['id_new'] = (int)$rowEstados['recarga_id'];

 				}else{

 					$estados[$key]['id_new'] = (int)$rowEstados['servicio_id'];
 					$serv = $servicioDAO->listaServicio($rowEstados['servicio_id']);
 					$signo = ($serv[0]['tipo_pago'] == 1)?'-':'+';
 					$monto = $signo.round($rowEstados['cta_monto'],2);
 					$tipopago = ($serv[0]['tipo_pago'] == 1)?'Efectivo':(($serv[0]['tipo_pago'] == 2)?'Vale':'Tarjeta');

 				}

 				$estados[$key]['tipo_pago'] = $tipopago;
 				$estados[$key]['cta_tipo'] = $tipo;
 				$estados[$key]['cta_monto'] = 'S/. '.$monto;
 			}
 		}

 		//echo '<pre>'; print_r($estados); echo '</pre>'; exit;
 		header('Content-Type: application/json');
		echo json_encode($estados);
 	}
	
}
