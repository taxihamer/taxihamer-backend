<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once'../DAL/vehiculoDAO.php';
require_once'../DAL/constantes.php';

class vehiculoController{
	public function eliminarVehiculo($id){
		$vehiculoDAO = new VehiculoDAO();
		$data['unidad_id'] = $id;
		$res = $vehiculoDAO->deleteVehiculo($data);
		return $res;
	}

	public function insertarVehiculo($alias, $tipo_servicio, $placa, $marca, $modelo, $color, $fabricacion, $fecha_soat, $fecha_revtec, $foto, $caracteristicas){
		if(!empty($caracteristicas)){
			$cadena_equipo = implode(",", $caracteristicas);
			$data['unidad_caracteristicas'] = $cadena_equipo;
		}else{
			$data['unidad_caracteristicas'] = null;
		}

		$status = 1;
		$vehiculoDAO = new VehiculoDAO();
		$data['tiposervicio_id'] = $tipo_servicio;
		$data['unidad_alias'] = $alias;
		$data['unidad_status'] = $status;
		$data['unidad_placa'] = $placa;
		$data['unidad_marca'] = $marca;
		$data['unidad_modelo'] = $modelo;
		$data['unidad_color'] = $color;
		$data['unidad_fabricacion'] = $fabricacion;
		$data['unidad_fechasoat'] = $fecha_soat;
		$data['unidad_fecharevtec'] = $fecha_revtec;
		$data['unidad_foto'] = $foto;
		$res = $vehiculoDAO->insertarVehiculo($data);

		//echo '<pre>'; print_r($res); echo '</pre>'; exit('holaaa');
		return $res;

	}

	public function actualizarVehiculo($id, $alias, $tipo_servicio, $placa, $marca, $modelo, $color, $fabricacion, $fecha_soat, $fecha_revtec, $foto, $caracteristicas){
		if(!empty($caracteristicas)){
			$cadena_equipo = implode(",", $caracteristicas);
			$data['unidad_caracteristicas'] = $cadena_equipo;
		}else{
			$data['unidad_caracteristicas'] = null;
		}

		$status = 1;
		$vehiculoDAO = new VehiculoDAO();
		$data['unidad_id'] = $id;
		$data['tiposervicio_id'] = $tipo_servicio;
		$data['unidad_alias'] = $alias;
		$data['unidad_status'] = $status;
		$data['unidad_placa'] = $placa;
		$data['unidad_marca'] = $marca;
		$data['unidad_modelo'] = $modelo;
		$data['unidad_color'] = $color;
		$data['unidad_fabricacion'] = $fabricacion;
		$data['unidad_fechasoat'] = $fecha_soat;
		$data['unidad_fecharevtec'] = $fecha_revtec;
		$data['unidad_foto'] = $foto;
		$res = $vehiculoDAO->updateVehiculo($data);
		return $res;

	}

	public function actualizarVehiculoSinFoto($id, $alias, $tipo_servicio, $placa, $marca, $modelo, $color, $fabricacion, $fecha_soat, $fecha_revtec, $caracteristicas){
		if(!empty($caracteristicas)){
			$cadena_equipo = implode(",", $caracteristicas);
			$data['unidad_caracteristicas'] = $cadena_equipo;
		}else{
			$data['unidad_caracteristicas'] = null;
		}
		$status = 1;
		$vehiculoDAO = new VehiculoDAO();
		$data['unidad_id'] = $id;
		$data['tiposervicio_id'] = $tipo_servicio;
		$data['unidad_alias'] = $alias;
		$data['unidad_status'] = $status;
		$data['unidad_placa'] = $placa;
		$data['unidad_marca'] = $marca;
		$data['unidad_modelo'] = $modelo;
		$data['unidad_color'] = $color;
		$data['unidad_fabricacion'] = $fabricacion;
		$data['unidad_fechasoat'] = $fecha_soat;
		$data['unidad_fecharevtec'] = $fecha_revtec;
		
		$res = $vehiculoDAO->updateVehiculoSinFoto($data);
		return $res;

	}

	public function ListarData($pagenum, $pagesize, $offset, $select, $text){
		
		$vehiculoDAO = new vehiculoDAO();
		$count = $vehiculoDAO->CountVehiculos($select, $text);
		return $count;
	}
}
$controller = new vehiculoController();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if(isset($request->list) && $request->list == 'lista_vehiculo'){

	$pagenum = $request->variables->page;
	$pagesize = $request->variables->size;
	$offset = ($pagenum - 1) * $pagesize;
	$searchText = $request->variables->search;

	$tipoServicio = (isset($request->variables->select))?(int)$request->variables->select:0;

	$count = $controller->ListarData($pagenum, $pagesize, $offset, $tipoServicio, $searchText);
	$count_new = $count[0]['total'];

	$vehiculoDAO = new vehiculoDAO();
	$Res = $vehiculoDAO->listaPaginacion($offset, $pagesize, $tipoServicio, $searchText);
	
	foreach ($Res as $key => $value) {
		if(!empty($value['unidad_foto'])){
			$imagen = '../BL/'.$value['unidad_foto'];
			$exists = is_file($imagen);

			if($exists == true){
				$Res[$key]['unidad_foto'] = $value['unidad_foto'];
			}else{
				$Res[$key]['unidad_foto'] = IMG_DEFAULT1;
			}
		}else{
			$Res[$key]['unidad_foto'] = IMG_DEFAULT1;
		}
	}

	//echo '<pre>'; print_r($Res); echo '</pre>'; exit;
	$myData = array('Request' => $Res, 'totalCount' => $count_new);

	header('Content-Type: application/json');
	echo json_encode($myData);

}	

if(isset($request->list) && $request->list == 'editado_vehiculo'){

	$unidad_id = (int)$request->variables->id;
	$vehiculoDAO = new vehiculoDAO();
	$Res = $vehiculoDAO->listaBy($unidad_id);

	header('Content-Type: application/json');
	echo json_encode($Res[0]);
}
//********Listar Vehiculos con Angular********//
if(isset($request->listar_vehiculo)){
	$VehiculoDAO = new VehiculoDAO();
	$Res = $VehiculoDAO->listaAsignada();
	header('Content-Type: application/json');
	echo json_encode($Res);
}

/*-----------------INSERTAR VEHICULO-------------------*/
if(isset($_REQUEST['hidden_insert_vehiculo'])){

	$tamano = $_FILES['foto']['size'];
	$tipo = $_FILES['foto']['type'];
	$archivo = $_FILES['foto']['name'];
	$prefijo = substr(md5(uniqid(rand())),0,6);
	$caracteristica = $_REQUEST['caracteristicas'];
	$vehiculoDAO = new vehiculoDAO();
	$validPlaca = $vehiculoDAO->validarPlaca($_REQUEST['placa']);
	
	if($validPlaca[0]['total'] >= 1){
		header("location:../index.php?seccion=taxis&status=valid");
	}else{
		if ($archivo != '') {
		// guardamos el archivo a la carpeta files
			$destino = 'files/'.$prefijo.'_'.$archivo;
			if (copy($_FILES['foto']['tmp_name'],$destino)) {
				$res = $controller->insertarVehiculo($_REQUEST['alias'], intval($_REQUEST['tiposervicio']), $_REQUEST['placa'], $_REQUEST['marca'], $_REQUEST['modelo'], $_REQUEST['color'], intval($_REQUEST['fabricacion']), $_REQUEST['fechasoat'], $_REQUEST['fecharevtec'], $destino, $caracteristica);
				if($res == true){
					header("location:../index.php?seccion=taxis&status=true");
				}else{
					header("location:../index.php?seccion=taxis&status=false");
				}
			} else {
				header("location:../index.php?seccion=taxis&status=false");
			}
		} else {
			header("location:../index.php?seccion=taxis&status=false");
		}

	}
}
/*-----------------ACTUALIZAR VEHICULO-------------------*/
if(isset($_REQUEST['hidden_update_vehiculo'])){
	//echo '<pre>'; print_r(); echo '</pre>'; exit('saul');
	$tamano = $_FILES['foto1']['size'];
	$tipo = $_FILES['foto1']['type'];
	$archivo = $_FILES['foto1']['name'];
	$prefijo = substr(md5(uniqid(rand())),0,6);
	$caracteristica = $_REQUEST['caracteristicasedit'];

	

	if ($archivo != '') {
		// guardamos el archivo a la carpeta files
		$destino = 'files/'.$prefijo.'_'.$archivo;
		if (copy($_FILES['foto1']['tmp_name'],$destino)) {
			$res = $controller->actualizarVehiculo($_REQUEST['unidad_id'], $_REQUEST['alias1'], intval($_REQUEST['tiposervicio1']), $_REQUEST['placa1'], $_REQUEST['marca1'], $_REQUEST['modelo1'], $_REQUEST['color1'], intval($_REQUEST['fabricacion1']), $_REQUEST['fechasoat1'], $_REQUEST['fecharevtec1'], $destino, $caracteristica);
			if($res == true){
				header("location:../index.php?seccion=taxis&status=true");
			}else{
				header("location:../index.php?seccion=taxis&status=false");
			}
		} else {
			header("location:../index.php?seccion=taxis&status=false");
		}
	} else {
		$res = $controller->actualizarVehiculoSinFoto($_REQUEST['unidad_id'], $_REQUEST['alias1'], intval($_REQUEST['tiposervicio1']), $_REQUEST['placa1'], $_REQUEST['marca1'], $_REQUEST['modelo1'], $_REQUEST['color1'], intval($_REQUEST['fabricacion1']), $_REQUEST['fechasoat1'], $_REQUEST['fecharevtec1'], $caracteristica);
		if($res == true){
				header("location:../index.php?seccion=taxis&status=true");
		}else{
			header("location:../index.php?seccion=taxis&status=false");
		}
	}
}
/*-----------------DELETE CONDUCTOR-------------------*/
if(isset($_REQUEST['delete_general'])){
	$res = $controller->eliminarVehiculo($_REQUEST['Gid']);
	$json['status'] = true;
	header('Content-Type: application/json');
   	echo json_encode($json);
}
