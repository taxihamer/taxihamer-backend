const unitMobileModel = require('../models/unitmobile');
const conductorModel = require('../models/conductors');
const _ = require('lodash');
const fs = require('fs')

const conductor = {
  store(req, res){
    return new Promise( ( resolve, reject )=>{
      // newName = null;
      // upload( req, res, (err)=>{
      //   if ( err ) return reject( err );
        
        let foto_perfil = '';
        let foto_auto = '';
        let foto_licencia = '';
        let foto_soat = '';
        // let foto_tarjeta_propiedad = '';
        
        const nombres = req.body.nombres;
        const apellidos = req.body.apellidos;
        const correo = req.body.correo;
        const celular = req.body.celular;
        const password = req.body.password;
        const dni = req.body.dni;

        const tipo_servicio = req.body.tipo_servicio;
        const placa = req.body.placa;
        const opciones_servicio = req.body.opciones_servicio;


        const banco = req.body.banco;
        const cci = req.body.cci;
        const unidad_marca = req.body.marca;
        const unidad_modelo = req.body.modelo;
        const unidad_color = req.body.color;

        unitMobileModel.validatePlacaDNI( placa, dni , (err2)=>{
          if( err2 ) return reject(err2);

          if( req.body.perfil ){
            foto_perfil = 'files/'+Date.now()+'1.jpg';
            fs.writeFileSync(`/var/www/html/taxihammer/BL/${foto_perfil}`, req.body.perfil, 'base64');
          }
          if( req.body.auto ){
            foto_auto = 'files/'+Date.now()+'2.jpg';
            fs.writeFileSync(`/var/www/html/taxihammer/BL/${foto_auto}`, req.body.auto, 'base64');
          }
          if( req.body.licencial ){
            foto_licencia = 'files/'+Date.now()+'3.jpg';
            fs.writeFileSync(`/var/www/html/taxihammer/BL/${foto_licencia}`, req.body.licencial, 'base64'); 
          }
          if( req.body.soat ){
            foto_soat = 'soat/'+Date.now()+'4.jpg';
            fs.writeFileSync(`/var/www/html/taxihammer/BL/${foto_soat}`, req.body.soat, 'base64');  
          }
          // if( req.body.tarjeta_propiedad ){
          //   foto_tarjeta_propiedad = 'files/'+Date.now()+'.jpg';
          //   fs.writeFileSync(`/var/www/html/taxihammer/BL/${foto_tarjeta_propiedad}`, req.body.tarjeta_propiedad, 'base64'); 
          // }


          unitMobileModel.createUnidad( tipo_servicio, placa, opciones_servicio, foto_auto, unidad_marca, unidad_modelo, unidad_color, ( unidad_id)=>{

            conductorModel.store(unidad_id, nombres, apellidos, correo, celular, password, cci, dni, banco, foto_perfil, foto_licencia, foto_soat, (err)=>{
              if( err ) return reject(err)
                return resolve({ success: true});
            });
          });
        })
        
      

        // return resolve({ nameFile: newName });
      // })
    });
  }
}

module.exports = conductor;
