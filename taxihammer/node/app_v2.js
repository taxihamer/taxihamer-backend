// requiriendo e inicializando libreriasnuevaSolicitudConductor
//var najax = $ = require('najax');
var express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session')({
        secret: "Appsl0v3rsnode",
        resave: true,
        saveUninitialized: true
    }),
    sharedsession = require("express-socket.io-session"),
    app = express(),
    server = require('http').createServer(app),
    io  = require('socket.io').listen(server);


    mysql = require('mysql'),
    moment = require('moment-timezone'),
    request = require('request'),
    mailer = require('express-mailer');
    email = require('mailer');
    nodemailer = require("nodemailer");
    smtpTransport = require("nodemailer-smtp-transport");
    utf8 = require('utf8');
    strftime = require('strftime');
    Entities = require('html-entities').XmlEntities;


    entities = new Entities();
//Constantes Node 
global.CONFIG = {};
var constant = require('define-constant')(global.CONFIG);
//constant('URL_PROJECT', 'http://208.68.39.4/');
constant('URL_PROJECT', 'http://192.168.10.10/');

var smtpTransport = nodemailer.createTransport(smtpTransport({
    host : "tsx.websitewelcome.com",
    secureConnection : false,
    port: 587,
    auth : {
        user : "puntos@peruentaxi.com",
        pass : "12345Puntos$"
    }
}));
//Realizamos la conexion a mysql que es el que contiene informacion de los datos que aparecen en la web

var Conductors = require('./models/conductors');
var Clients = require('./models/clients');
var UnitMobile = require('./models/unitmobile');
var Lugar = require('./models/lugar'); 
var Services = require('./models/servicio');
var Reservas = require('./models/reserva');

//  Archivos para el manejo de rutas
const serviciosRouter = require('./routes/servicios');
const conductorRouter = require('./routes/conductor');


//Ponemos a escuchar las notificaciones del servidor
server.listen(process.env.PORT || 3000);

//Sessions and io.socket
app.use(session)

//Parseamos en un formato necesario para la interpretacion de los parametros que se reciban
app.use(bodyParser.json({limit: "5mb"}));
app.use(bodyParser.urlencoded({extended : true }));
app.use(bodyParser.json());
console.log('inicio');


// Routers
app.use('/servicios', serviciosRouter );
app.use('/conductor', conductorRouter );

//Config send Mailer
mailer.extend(app, {
    from : 'info.prueba.appslovers@gmail.com',
    host : 'tsx.websitewelcome.com',
    transportMethod: 'SMTP',
    secureConnection : true,
    port: 465,
    auth:{
        user: 'puntos@peruentaxi.com',
        pass: '12345Puntos$'//''
    }
});

//Setear las vistas .jade 
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
//app.set('view engine', 'html');


/*function teststuart(idArg, nombreArg) {

    Conductors.searchServiceIdConductor(idArg,nombreArg, function(err, result){
            var servicio = [];
            for(var i in result){

            }

    });
}*/
function deg2rad (angle) {
    // http://jsphp.co/jsphp/fn/view/deg2rad
    // + original by: Enrique Gonzalez
    // * example 1: deg2rad(45);
    // * returns 1: 0.7853981633974483
    return (angle / 180) * Math.PI;
}

function rad2deg (angle) {
    // http://jsphp.co/jsphp/fn/view/rad2deg
    // + original by: Enrique Gonzalez
    // + improved by: Brett Zamir (http://brett-zamir.me)
    // * example 1: rad2deg(3.141592653589793);
    // * returns 1: 180
    return angle * 57.29577951308232; // angle / Math.PI * 180
}

function getBoundaries(lat, lng, distance){
    
    var earthRadius = 6367.41;
    var rarray = new Array();
    var rretur = [];
    // Los angulos para cada dirección
    var cardinalCoords = [];
    cardinalCoords['north'] = '0';
    cardinalCoords['south'] = '180';
    cardinalCoords['east'] = '90';
    cardinalCoords['west'] = '270';

    var rLat = deg2rad(lat);
    var rLng = deg2rad(lng);
    var rAngDist = distance/earthRadius;
    
    for (var name in cardinalCoords) {
        var rAngle = deg2rad(cardinalCoords[name]);
        var rLatB = Math.asin(Math.sin(rLat) * Math.cos(rAngDist) + Math.cos(rLat) * Math.sin(rAngDist) * Math.cos(rAngle));
        var rLonB = rLng + Math.atan2(Math.sin(rAngle) * Math.sin(rAngDist) * Math.cos(rLat), Math.cos(rAngDist) - Math.sin(rLat) * Math.sin(rLatB));
        rarray[name] = new Array(parseFloat(rad2deg(rLatB)),parseFloat(rad2deg(rLonB)));
    }

    rretur['min_lat'] = rarray['south'][0];
    rretur['max_lat'] = rarray['north'][0];
    rretur['min_lng'] = rarray['west'][1];
    rretur['max_lng'] = rarray['east'][1];
    
    return rretur;
}

function fechaActual(){

    var f = new Date();
    var fechactual = f.getFullYear()+'-'+(f.getMonth()+1)+"-"+f.getDate();
    var horaactual = (f.getHours())+":"+f.getMinutes()+":"+f.getSeconds();
    var fecha = [];

    fecha['actual'] = fechactual+' '+horaactual;

    return fecha;
}



var conductoresId = [];

app.all('*', function(req, res, next) {

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'accept, content-type, x-parse-application-id, x-parse-rest-api-key, x-parse-session-token');
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }

});

app.post('/w_save_vale_general', function(req, res) {

    var servicio_id = req.body.idservicio;
    var servicio_vale = req.body.vale;

    Services.validarVale(servicio_vale,function(err,resul){
        if(!err){
            var totalVale = resul[0].TotalVale;
            if(totalVale != 0){
                console.log('ERROR Ya hacido usado el vale w_save_vale_general', err);
                res.send(false); 
            }else{
                Services.updateConductorVale(servicio_vale,servicio_id,function(error){
                    if(!error){
                        console.log('OK socket.on(updateConductorVale) w_save_vale_general', error);
                        res.send(true); 
                    }else{
                        console.log('ERROR socket.on(updateConductorVale) w_save_vale_general', error);
                        res.send(false); 
                    }
                });
            }
        }else{
            console.log('ERROR socket.on(validarVale) w_save_vale_general', err);

        }
    });
}); 

app.post('/w_reserva_asignado_creado',function(req, res){

    // console.log('[------------SERVER-----] RESERVA ASIGNADO CREADO',  req.body.id);
    var idReserva = req.body.id;
    Reservas.listaReservaId(idReserva,function(err,resultreserva){

        // console.log(resultreserva);

        if(!err){
            Conductors.validarConductor(resultreserva[0].conductor_id,function(err,resul){
                if(!err){
                    var fechainicio = new Date(resultreserva[0].reserva_fecha);
                    var fechafinal = new Date(resultreserva[0].reserva_fecha);
                    fechafinal.setHours(fechainicio.getHours()-5);
                    resultreserva[0].reserva_fecha = fechafinal.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    // console.log(resul[0]);
                    // console.log('tttttt');
                    io.to(resul[0].conductor_socket).emit('NotificacionReservaAceptada',{Reserva:resultreserva});
                    res.send(true); 
                }
            }); 
        }else{
            console.log('ERROR socket.on(listaReservaId) w_reserva_asignado_creado', err); 
        }
    });
});

app.post('/w_reserva_asignado', function(req, res) { 

    // console.log('[------------SERVER-----] RESERVA ASIGNADO');
     var flag = req.body.flag;
    if(flag == 1){ //Si bviene de panel

        var clfec = req.body.cliente_fecha.split(" ");
        var format = clfec[0].split("/");
        req.body.cliente_fecha = format[2]+'-'+format[0]+'-'+format[1]+' '+clfec[1]+':00';
        req.body.tipo = 1;

        Conductors.validarConductor(req.body.conductor_id,function(err,resul){
            // console.log('[SERVER] -> flag = 1 => data ->',resul,'Body',req.body);
            if(!err){
                io.to(resul[0].conductor_socket).emit('NotificacionReserva',{Reserva:req.body});
                res.send(true); 
            }
        });
    }else if(flag == 2){

        Reservas.listaReservaId(req.body.idReserva,function(err,resultreserva){
            if(!err){
                Conductors.validarConductor(req.body.idConductor,function(err,resul){
                    if(!err){
                        var fechainicio = new Date(resultreserva[0].reserva_fecha);
                        var fechafinal = new Date(resultreserva[0].reserva_fecha);
                        fechafinal.setHours(fechainicio.getHours()-5);
                        resultreserva[0].reserva_fecha = fechafinal.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                        resultreserva[0].tarifa = parseInt(resultreserva[0].tarifa);

                        resultreserva[0].dir_origen = (resultreserva[0].dir_origen == null)?'':utf8.decode(resultreserva[0].dir_origen.replace('Ã“','O'));
                        resultreserva[0].referencia_origen = (resultreserva[0].referencia_origen == null ) ? '' : utf8.decode(resultreserva[0].referencia_origen.replace('Ã“','O'));
                        resultreserva[0].destino =  (resultreserva[0].destino == null) ? '' : utf8.decode(resultreserva[0].destino.replace('Ã“','O'))  ;
                        resultreserva[0].dir_destino = (resultreserva[0].dir_destino == null) ? '' : utf8.decode(resultreserva[0].dir_destino.replace('Ã“','O'));
                        resultreserva[0].referencia_destino = (resultreserva[0].referencia_destino == null) ? '' : utf8.decode(resultreserva[0].referencia_destino.replace('Ã“','O'))
                        
                        resultreserva[0].conductor_id = parseInt(req.body.idConductor);
                        // console.log('[SERVER] Log fLAG = 2 Dtaros resultreserva FINAL->',resultreserva[0]);
                        resultreserva[0].tipo = 2;
                        io.to(resul[0].conductor_socket).emit('NotificacionReserva',{Reserva:resultreserva[0]});
                        res.send(true); 
                    }
                }); 
            }
        });

    }else{
        // console.info('Nueva Implementacion de Reserva flag', flag);

        Reservas.listaReservaId(req.body.idReserva,function(err,resultreserva){
            if(!err){

                Conductors.validarConductor(req.body.idConductor,function(err,resul){
                    if(!err){

                        console.log(resultreserva);
                        var fechainicio = new Date(resultreserva[0].reserva_fecha);
                        var fechafinal = new Date(resultreserva[0].reserva_fecha);
                        fechafinal.setHours(fechainicio.getHours()-5);
                        resultreserva[0].reserva_fecha = fechafinal.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                        resultreserva[0].tarifa = parseInt(resultreserva[0].tarifa);

                        resultreserva[0].dir_origen = (resultreserva[0].dir_origen == null)?'':utf8.decode(resultreserva[0].dir_origen.replace('Ã“','O'));
                        resultreserva[0].referencia_origen = (resultreserva[0].referencia_origen == null ) ? '' : utf8.decode(resultreserva[0].referencia_origen.replace('Ã“','O'));
                        resultreserva[0].destino =  (resultreserva[0].destino == null) ? '' : utf8.decode(resultreserva[0].destino.replace('Ã“','O'))  ;
                        resultreserva[0].dir_destino = (resultreserva[0].dir_destino == null) ? '' : utf8.decode(resultreserva[0].dir_destino.replace('Ã“','O'));
                        resultreserva[0].referencia_destino = (resultreserva[0].referencia_destino == null) ? '' : utf8.decode(resultreserva[0].referencia_destino.replace('Ã“','O'))
                        resultreserva[0].tipo = 3;
                        io.to(resul[0].conductor_socket).emit('NotificacionReserva',{Reserva:resultreserva[0]});
                        res.send(true); 

                    }
                });

            }
        });

    }
    
});

app.get('/w_recarga_panel',function(req, res){

    var recarga_id = req.query.recarga_id;
    var conductor_id = req.query.conductor_id;

    Conductors.validarConductor(conductor_id,function(error,resultado){
        if(!error){
            var conductor_nombre = resultado[0].conductor_nombre+' '+resultado[0].conductor_apellido;
            Lugar.RecargaFindquery(recarga_id,conductor_id,function(err,result){
                if(!err){
                    console.log('enviar');
                    io.sockets.emit('alertaRecargaPanel', {
                            recarga_id:recarga_id,
                            conductor_id:conductor_id,
                            conductor_nombre:utf8.decode(conductor_nombre.replace('Ã“','O')),
                            recarga_monto:result[0].recarga_monto,
                            recarga_operacion:result[0].recarga_num_oper,
                            recarga_fecha:result[0].recarga_fecha
                        });
                    res.json('Ok');
                    console.log('correcto')
                }
            });
        }else{
            console.log('ERROR socket.on(validarConductor) w_recarga_panel', err);
        }
    });
});


app.get('/w_agregar_reserva',function(req, res){
    
    console.log('[------------SERVER-----] SUPUESTAMENTE AGREGA LA RESERVA Y DEBE ENVIAR CORREO'+req.query);
    var reserva_id = req.query.reserva_id;
    var cliente_id = req.query.cliente_id;

    Clients.findOneQueryCliente(cliente_id,function(err,result){
        if(!err){
            var cliente_nombre = result[0].cliente_nombre+' '+result[0].cliente_apellido;
            io.sockets.emit('alertaReservaPanel', {reserva_id:reserva_id,cliente_id:cliente_id,cliente_nombre:cliente_nombre});
            res.json('Ok');
        }else{
            console.log('ERROR socket.on(findOneQueryCliente) w_agregar_reserva', err);
        }
    });                                        
});

app.post('/w_token_android',function(req, res){
 console.log(req.body);
    var token = req.body.token;
    var cliente_id = req.body.cliente_id;
    console.log(token);
    console.log(cliente_id);
    Clients.updateTokenCliente(cliente_id, token,function(err,result){
        if(!err){
            console.log('ERROR socket.on(updateTokenCliente) w_token_android', err);
            res.json('Ok');
        }else{
            console.log('ERROR socket.on(updateTokenCliente) w_token_android', err);
        }
    }); 

});

app.post('/w_recuperar_estado', function (req, res){
    console.log('               [SERVER]        -> w_recuperar_estado');
    console.log(req.body);
    var id = req.body.codigo;
    var tipoflag = req.body.flag; //conductor = 1 //cliente = 2
    var dataServicio = [];
    // 1 => 
    // 2 => 
    if(id != 0){

        Lugar.ListarApiKey(1,function(errorApi,resulApi){
//821,1,1
            Services.recuperarEstado(id,tipoflag,1, function(err, resp){
                console.log('               [SERVER]        -> resp',resp);
               if(resp.length != 0){

                    var fechainicio = new Date(resp[0].fecha_recojo);
                    var fechafinal = new Date(resp[0].fecha_recojo);
                    fechafinal.setHours(fechainicio.getHours()-5);

                    var horainicio = new Date(resp[0].hora_inicio);
                    var horafinal = new Date(resp[0].hora_inicio);
                    horafinal.setHours(horainicio.getHours()-5);

                    if(tipoflag == 1){
                        console.log('flag '+tipoflag);//400
                        Clients.findOneQueryCliente(resp[0].cliente_id,function(err, resultado){
                            if(!err){
                                Conductors.validarConductor(id,function(err,result){
                                        var config_id = 1; 
                                        Lugar.ListarConfig(config_id,function(err,resultadotarifa){
                                            console.log(resultadotarifa[0]);
                                            console.log("Servicio");
                                            console.log(resp[0]);
                                       
                                            Servicio = {
                                                IdServicio:resp[0].servicio_id,
                                                IdCliente:resp[0].cliente_id, 
                                                IdConductor:resp[0].conductor_id, 
                                                OrigenId:resp[0].origen_id,  
                                                OrigenNombre:  (resp[0].origen_nombre == null)  ? '':  utf8.decode(resp[0].origen_nombre), 
                                                OrigenDireccion: (resp[0].origen_direccion == null)  ? '':   utf8.decode(resp[0].origen_direccion), 
                                                OrigenReferencia:(resp[0].origen_referencia == null)?'':utf8.decode(resp[0].origen_referencia),
                                                OrigenLat:resp[0].origen_lat, 
                                                OrigenLng:resp[0].origen_lon,
                                                DestinoId:resp[0].destino_id,
                                                DestinoNombre: (resp[0].destino_nombre == null ) ? ''  : utf8.decode(resp[0].destino_nombre),
                                                DestinoDireccion: (resp[0].destino_direccion == null ) ? ''  : utf8.decode(resp[0].destino_direccion),
                                                DestinoReferencia:(resp[0].destino_referencia == null) ?'':utf8.decode(resp[0].destino_referencia),
                                                DestinoLat:resp[0].destino_lat,
                                                DestinoLng:resp[0].destino_lon,
                                                TipoPago:resp[0].tipo_pago,
                                                Tarifa:resp[0].tarifa,
                                                Estado:resp[0].estado,
                                                HoraInicio :horafinal.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                                                FechaRecojo:fechafinal.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                                                TipoServicio:resp[0].tiposervicio_id,
                                                Reserva:resp[0].flag_reserva,
                                                vale:resp[0].servicio_vale,
                                                imagen: (resp[0].imagen)? ('http://208.68.39.4/BL/'+resp[0].imagen) : null,
                                                comentario: resp[0].comentario
                                            }


                                            console.log('TESTTEST',resultado);



                                             Lugar.ListarConfigReserva(1,function(error,resultadoAlerta){
                                                console.log(resultadoAlerta);
                                                if(!error){
                                                    var Alerta = (resultadoAlerta.length > 0 )?resultadoAlerta[0].configreserva_alerta:0;
                                                    dataServicio = {
                                                        Servicio:Servicio,
                                                        Cliente:resultado[0],
                                                        CalificacionConductor:result[0].conductor_calificacion_flag,
                                                        TipoTarifa:resultadotarifa[0].tipo_tarifa,
                                                        Alerta:Alerta,
                                                        foursquare_key:resulApi[0].api_foursquare_key,
                                                        foursquare_secret:resulApi[0].api_foursquare_secret,
                                                        here_key:resulApi[0].api_here_key,
                                                        here_secret:resulApi[0].api_here_secret,
                                                        google_ios:resulApi[0].api_google_ios,
                                                        google_android:resulApi[0].api_google_android
                                                    }

                                                    res.json(dataServicio);
                                                }

                                            });

                                            /*dataServicio = {
                                                Servicio:Servicio,
                                                Cliente:resultado[0],
                                                CalificacionConductor:result[0].conductor_calificacion_flag,
                                                TipoTarifa:resultadotarifa[0].tipo_tarifa
                                            }*/

                                            
                                        });
                                });
                            }
                        });
                    }else{

                        Conductors.validarConductor(resp[0].conductor_id,function(err,resul){
                            if(!err){
                                UnitMobile.findOneQueryUnidad(resul[0].unidad_id,function(err,resultunidad){
                                    Clients.findOneQueryCliente(id,function(err, resultado){
                                        var config_id = 1; 
                                        Lugar.ListarConfig(config_id,function(err,resultadotarifa){
                                            console.log(resultadotarifa[0]);
                                            Servicio = {
                                                    IdServicio:resp[0].servicio_id,
                                                    IdCliente:resp[0].cliente_id, 
                                                    IdConductor:resp[0].conductor_id, 
                                                    OrigenId:resp[0].origen_id,  
                                                    OrigenNombre:utf8.decode(resp[0].origen_nombre.replace('Ã“','O')), 
                                                    OrigenDireccion:(resp[0].origen_direccion == null)?'':utf8.decode(resp[0].origen_direccion.replace('Ã“','O')), 
                                                    OrigenReferencia:(resp[0].origen_referencia == null)?'':utf8.decode(resp[0].origen_referencia.replace('Ã“','O')),
                                                    OrigenLat:resp[0].origen_lat, 
                                                    OrigenLng:resp[0].origen_lon,
                                                    DestinoId:resp[0].destino_id,
                                                    DestinoNombre:utf8.decode(resp[0].destino_nombre.replace('Ã“','O')),
                                                    DestinoDireccion:(resp[0].destino_direccion == null)?'' : utf8.decode(resp[0].destino_direccion.replace('Ã“','O')),
                                                    DestinoReferencia:(resp[0].destino_referencia == null)?'':utf8.decode(resp[0].destino_referencia.replace('Ã“','O')),
                                                    DestinoLat:resp[0].destino_lat,
                                                    DestinoLng:resp[0].destino_lon,
                                                    TipoPago:resp[0].tipo_pago,
                                                    Tarifa:resp[0].tarifa,
                                                    Estado:resp[0].estado,
                                                    HoraInicio:horafinal.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                                                    FechaRecojo:fechafinal.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                                                    TipoServicio:resp[0].tiposervicio_id,
                                                    Paradas:parseFloat(resp[0].parada),
                                                    Parqueos:parseFloat(resp[0].parqueo),
                                                    Peajes:parseFloat(resp[0].peaje),
                                                    Espera:parseFloat(resp[0].espera),
                                                    Reserva:resp[0].flag_reserva
                                                }


                                                //Cambiamos algunos datos del conductor para que no salgan caracteres etraños

                                                resul[0].conductor_nombre = utf8.decode(resul[0].conductor_nombre.replace('Ã“','O'));
                                                resul[0].conductor_apellido = utf8.decode(resul[0].conductor_apellido.replace('Ã“','O'));


                                            dataServicio = {
                                                Servicio:Servicio,
                                                Conductor:resul[0],
                                                Unidad:resultunidad[0],
                                                CalificacionCliente:resultado[0].cliente_calificacion_flag,
                                                TipoTarifa:resultadotarifa[0].tipo_tarifa,
                                                foursquare_key:resulApi[0].api_foursquare_key,
                                                foursquare_secret:resulApi[0].api_foursquare_secret,
                                                here_key:resulApi[0].api_here_key,
                                                here_secret:resulApi[0].api_here_secret,
                                                google_ios:resulApi[0].api_google_ios,
                                                google_android:resulApi[0].api_google_android
                                            }

                                            res.json(dataServicio);

                                        });
                                    });

                                });
                            }
                        });
                    }
                    
               }else{
                
                    if(tipoflag == 1){
                        Conductors.validarConductor(id,function(err,result){
                            var config_id = 1; 
                            Lugar.ListarConfig(config_id,function(err,resultadotarifa){

                                Lugar.ListarConfigReserva(1,function(error,resultadoAlerta){
                                                
                                    if(!error){
                                        var Alerta = (resultadoAlerta.length > 0 )?resultadoAlerta[0].configreserva_alerta:0;
                                        dataServicio = {
                                            Servicio:null,
                                            Cliente:null,
                                            CalificacionConductor:result[0].conductor_calificacion_flag,
                                            TipoTarifa:resultadotarifa[0].tipo_tarifa,
                                            Alerta:Alerta,
                                            foursquare_key:resulApi[0].api_foursquare_key,
                                            foursquare_secret:resulApi[0].api_foursquare_secret,
                                            here_key:resulApi[0].api_here_key,
                                            here_secret:resulApi[0].api_here_secret,
                                            google_ios:resulApi[0].api_google_ios,
                                            google_android:resulApi[0].api_google_android
                                        }

                                        res.json(dataServicio);  
                                    }

                                });

                                /*dataServicio = {
                                                Servicio:null,
                                                Cliente:null,
                                                CalificacionConductor:result[0].conductor_calificacion_flag,
                                                TipoTarifa:resultadotarifa[0].tipo_tarifa
                                                }
                                */
                                         
                            }); 
                        });
                    }else{
                        Clients.findOneQueryCliente(id,function(err, resultado){
                            Services.recuperarEstado(id,tipoflag,2, function(error, resp){
                                // console.log('cliente info');
                                // console.log(resultado[0]);
                                // console.log(resp);
                                if(resp.length == 0){
                                    var config_id = 1; 
                                    Lugar.ListarConfig(config_id,function(err,resultadotarifa){
                                        dataServicio = {
                                                    Servicio:null,
                                                    Conductor:null,
                                                    Unidad:null,
                                                    CalificacionCliente:resultado[0].cliente_calificacion_flag,
                                                    TipoTarifa:resultadotarifa[0].tipo_tarifa,
                                                    foursquare_key:resulApi[0].api_foursquare_key,
                                                    foursquare_secret:resulApi[0].api_foursquare_secret,
                                                    here_key:resulApi[0].api_here_key,
                                                    here_secret:resulApi[0].api_here_secret,
                                                    google_ios:resulApi[0].api_google_ios,
                                                    google_android:resulApi[0].api_google_android
                                                    }

                                        res.json(dataServicio);
                                    });
                                
                                }else{

                                Conductors.validarConductor(resp[0].conductor_id,function(err,resul){
                                    if(!err){
                                        UnitMobile.findOneQueryUnidad(resul[0].unidad_id,function(err,resultunidad){
                                            var config_id = 1; 
                                            Lugar.ListarConfig(config_id,function(err,resultadotarifa){ 

                                                if(resultado[0].cliente_calificacion_flag == 1 ){

                                                    var fechainicio = new Date(resp[0].fecha_recojo);
                                                    var fechafinal = new Date(resp[0].fecha_recojo);
                                                    fechafinal.setHours(fechainicio.getHours()-5);

                                                    var horainicio = new Date(resp[0].hora_inicio);
                                                    var horafinal = new Date(resp[0].hora_inicio);
                                                    horafinal.setHours(horainicio.getHours()-5);


                                                    serviciocliente = {
                                                                        IdServicio:resp[0].servicio_id,
                                                                        IdCliente:resp[0].cliente_id, 
                                                                        IdConductor:resp[0].conductor_id, 
                                                                        OrigenId:resp[0].origen_id,  
                                                                        OrigenNombre:utf8.decode(resp[0].origen_nombre.replace('Ã“','O')), 
                                                                        OrigenDireccion:(resp[0].origen_direccion == null)?'':utf8.decode(resp[0].origen_direccion.replace('Ã“','O')), 
                                                                        OrigenReferencia:(resp[0].origen_referencia == null)?'':utf8.decode(resp[0].origen_referencia.replace('Ã“','O')),
                                                                        OrigenLat:resp[0].origen_lat, 
                                                                        OrigenLng:resp[0].origen_lon,
                                                                        DestinoId:resp[0].destino_id,
                                                                        DestinoNombre:utf8.decode(resp[0].destino_nombre.replace('Ã“','O')),
                                                                        DestinoDireccion:(resp[0].destino_direccion == null)?'':utf8.decode(resp[0].destino_direccion.replace('Ã“','O')),
                                                                        DestinoReferencia:(resp[0].destino_referencia == null)?'':utf8.decode(resp[0].destino_referencia.replace('Ã“','O')),
                                                                        DestinoLat:resp[0].destino_lat,
                                                                        DestinoLng:resp[0].destino_lon,
                                                                        TipoPago:resp[0].tipo_pago,
                                                                        Tarifa:resp[0].tarifa,
                                                                        Estado:resp[0].estado,
                                                                        HoraInicio:horafinal.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                                                                        FechaRecojo:fechafinal.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                                                                        TipoServicio:resp[0].tiposervicio_id,
                                                                        Paradas:parseFloat(resp[0].parada),
                                                                        Parqueos:parseFloat(resp[0].parqueo),
                                                                        Peajes:parseFloat(resp[0].peaje),
                                                                        Espera:parseFloat(resp[0].espera),
                                                                        Reserva:resp[0].flag_reserva,
                                                                        imagen: (resp[0].imagen)? ('http://208.68.39.4/BL/'+resp[0].imagen) : '',
                                                                        comentario: resp[0].comentario
                                                                       }

                                                    dataServicio = {
                                                                Servicio:serviciocliente,
                                                                Conductor:resul[0],
                                                                Unidad:resultunidad[0],
                                                                CalificacionCliente:resultado[0].cliente_calificacion_flag,
                                                                TipoTarifa:resultadotarifa[0].tipo_tarifa,
                                                                foursquare_key:resulApi[0].api_foursquare_key,
                                                                foursquare_secret:resulApi[0].api_foursquare_secret,
                                                                here_key:resulApi[0].api_here_key,
                                                                here_secret:resulApi[0].api_here_secret,
                                                                google_ios:resulApi[0].api_google_ios,
                                                                google_android:resulApi[0].api_google_android
                                                                }

                                                }else{

                                                    dataServicio = {
                                                                Servicio:null,
                                                                Conductor:null,
                                                                Unidad:null,
                                                                CalificacionCliente:resultado[0].cliente_calificacion_flag,
                                                                TipoTarifa:resultadotarifa[0].tipo_tarifa,
                                                                foursquare_key:resulApi[0].api_foursquare_key,
                                                                foursquare_secret:resulApi[0].api_foursquare_secret,
                                                                here_key:resulApi[0].api_here_key,
                                                                here_secret:resulApi[0].api_here_secret,
                                                                google_ios:resulApi[0].api_google_ios,
                                                                google_android:resulApi[0].api_google_android
                                                                }
                                                }

                                                res.json(dataServicio);

                                            });

                                        });
                                    }
                                });
                                }
                            });
                        }); 
                    }  

               }
            });

        });
    }else{
        console.log('ERROR socket.on(w_recuperar_estado) EL CODIGO ES' , id);
    }
});


//Compartir sesión con socket io
io.use(sharedsession(session));
//Conectantando al socket del servidor en nodeJS
io.sockets.on('connection', function(socket){
    //En caso un conductor se desconecte se actualiza el socket necesario para tenerlo en cuenta.
    socket.on('disconnect', function(){
        Conductors.findOneQuery(socket.id, function(err, result){
            if(result != null){
                Conductors.updateQuerySocket(socket.id, function(err){
                    if(!err){ 
                        // console.log('user disconnected', socket.id);
                        // console.log(result.length);
                        if(result.length != 0){
                            io.sockets.emit('refreshCoordinates', {idConductor: result[0].conductor_id, latitud: result[0].conductor_lat, longitud: result[0].conductor_lng, socketMiss:'99', stateConductor: result[0].conductor_estado, tlf: result[0].conductor_celular, socket_conductor:null, campo_new:result[0].conductor_status});
                        }else{
                            console.log('user clients disconnected', socket.id);
                        }
                    }; 
                });
            }
        });
    });

    socket.on('SocketWeb', function(docs){
        socket.handshake.session.websocket = docs.socketweb;
        socket.handshake.session.save();
    });
    //EDITADO 
    //Lista de conductores segun latitud y longitud en orden de mayor cercania
    //Consume el cliente
    socket.on('getListConductorGeo', function (docs,callback){

        docs = JSON.parse(docs);
            
        var idCliente = docs.idCliente;
        var tiposervicio = docs.tiposervicio;
        var tipopago = docs.tipopago;
        var socketCliente = socket.id;

        var latitudest = docs.latdes;
        var longitudest = docs.lngdes;
        var latitudorig = docs.latorig;
        var longitudorig = docs.lngorig;

        var origen = docs.origen;
        var destino = parseInt(docs.destino);
        var tarifa_monto = docs.tarifa;
        var direcorig = docs.direcorigen;
        var direcdest = docs.direcdestino;
        var kilometraje = docs.kilometraje;

        var reforigen = docs.referenciaorigen;
        var refdestin = docs.referenciadestino;

        var orignombre = docs.origenombre;
        var destnombre = docs.destinonombre;

        const imagen = (docs.imagen)? docs.imagen: '' ;
        const comentario = (docs.comentario)? docs.comentario: '';
        
        var tarifa_id;
        var config_id = 1; 

        Clients.findOneQueryClienteActivo(idCliente, function(errActivo, resultActivo){
            if(!errActivo){
                if(resultActivo.length > 0){
                    Lugar.ListarConfig(config_id,function(err,resultConfig){
                        if(!err){

                            var radio = parseInt(resultConfig[0].radio);
                            var box = new getBoundaries(latitudorig, longitudorig, radio);
                            var min_lat = box['min_lat'];
                            var max_lat = box['max_lat'];
                            var min_lng = box['min_lng'];
                            var max_lng = box['max_lng'];

                            var TarifaMinima = resultConfig[0].tarifa_minima_m;

                            if(kilometraje == 1){
                                destino = 0;
                                if( typeof tarifa_monto === 'undefined'){
                                    tarifa_monto = TarifaMinima;//este 8 sacar de la tarifa minima
                                }else{
                                    tarifa_monto = tarifa_monto;
                                }
                            }

                            Conductors.MaximoTipodeServicio(1, function(error, resultipo){
                                if(!error){
                                    var mtiposervicio_id = resultipo[0].maximo;
                                    // console.log('TIPO DE SERVICIO GETLISTCONDUCTORGEO', tiposervicio);
                                    //             console.log('TIPOSSSS DE SERVICIO GETLISTCONDUCTORGEO', mtiposervicio_id);
                                    //              console.log('searchConductradio');
                                    // console.log([latitudorig, longitudorig, min_lat, max_lat, min_lng, max_lng, radio, tiposervicio,mtiposervicio_id]);
                                    // busca conductores que esten en el radio de la latitud, longitud y el tipo de servicio
                                    Conductors.searchConductradio(latitudorig, longitudorig, min_lat, max_lat, min_lng, max_lng, radio, tiposervicio,mtiposervicio_id, function (err, docsc){
                                        if(err){
                                            console.log('ERROR socket.on(getListConductorGeo) part 2', err);
                                        }else{
                                            if(docsc){
                                                
                                                var OrigeRefere = (docs.referenciaorigen!=null)?docs.referenciaorigen:'';
                                                var DestiRefere = (docs.referenciadestino!=null)?docs.referenciadestino:'';
                                                var OrigeNombre = (docs.origenombre!=null)?docs.origenombre:'';
                                                var DestiNombre = (docs.destinonombre!=null)?docs.destinonombre:'';

                                                    var dataInsertServicio = {idCliente:docs.idCliente,TipoServicion:docs.tiposervicio,Origen:docs.origen,Destino:destino,OrigeNombre:OrigeNombre,DestiNombre:DestiNombre,OrigeDirecc:docs.direcorigen,DestiDirecc:docs.direcdestino,OrigeRefere:OrigeRefere,DestiRefere:DestiRefere,OrigenLat:docs.latorig,OrigenLng:docs.lngorig,DestinoLat:docs.latdes,DestinoLng:docs.lngdes,Tarifa:tarifa_monto,TipoPago:docs.tipopago,Estado:0,
                                                        imagen: imagen, 
                                                        comentario: comentario
                                                    };
                                                            //console.log(dataInsertServicio);
                                                            // console.log('GET LIST CONDUCTOR')
                                                            request.post({
                                                              url:global.CONFIG.URL_PROJECT+'ws/cliente/ws_agregar_servicio.php',
                                                              form:{data:dataInsertServicio}
                                                            }, function(error, response, body){
                                                                // console.log(body);
                                                                    var obj = JSON.parse(body);
                                                                    if(obj.status == true){
                                                                        Clients.findOneQueryCliente(idCliente,function(err, resultado){
                                                                            servicioId = obj.id;
                                                                            conductoresId = [];
                                                                            // console.log('resultado ->'+ resultado[0]);

                                                                            docsc.forEach(function (docsRows) {
                                                                                conductoresId.push(docsRows.conductor_id);
                                                                                io.to(docsRows.conductor_socket).emit('nuevaSolicitudConductor',{
                                                                                    IdCliente:idCliente, 
                                                                                    latOrig:latitudorig,
                                                                                    latDest:latitudest,
                                                                                    lngOrig:longitudorig, 
                                                                                    lngDest:longitudest,
                                                                                    Origen:origen,
                                                                                    Destino:destino,
                                                                                    dirOrigen:direcorig,
                                                                                    dirDestino:direcdest,
                                                                                    referenciaorigen:reforigen,
                                                                                    referenciadestino:refdestin,
                                                                                    tipoServicio:tiposervicio,
                                                                                    tarifa:tarifa_monto, 
                                                                                    socketCliente:socketCliente,
                                                                                    ServicioId:servicioId,
                                                                                    OrigenNombre:orignombre,
                                                                                    DestinoNombre:destnombre,
                                                                                    Envio:0,
                                                                                    Viajes:resultado[0].cliente_viajes,
                                                                                    Calificacion:resultado[0].cliente_calificacion,
                                                                                    tipopago:docs.tipopago
                                                                                });
                                                                            });
                                                                                // console.log(conductoresId);
                                                                                send = {Servicio :servicioId,idsCond: conductoresId};
                                                                                callback(send);
                                                                                console.log('OK socket.on(insertServicio) parte 1', err);
                                                                        });
                                                                    }
                                                            });
                                            }
                                        }
                                    });

                                }else{
                                    console.log('ERROR socket.on(MaximoTipodeServicio) getListConductorGeo', error);
                                }
                            });

                        }
                    });
                }else{
                    console.log('CLIENTE', idCliente, 'su estado es 0');
                }
            }else{
                console.log('ERROR SOCKET.ON(FINONQUERYCLIENTEACTIVO) PART 0 GETLISCONDUCTORGEO');
            }
        });
    });
    
    socket.on('AceptarSolicitudConductor',function(docs){

        docs = JSON.parse(docs);
        var servicioId = docs.servicio_id;
        var idConductor = docs.conductor_id;
        var socketCliente = docs.socketCli;

        Conductors.ConductoralertaTaxi(idConductor,function(err,result){
            if(err){
                console.log('ERROR socket.on(ConductoralertaTaxi) part 0 ');
            }else{  
                    
                    var nombreConductor = utf8.decode(result[0].conductor_nombre.replace('Ã“','O'));
                    var apelliConductor = utf8.decode(result[0].conductor_apellido.replace('Ã“','O'));
                    var conductorCelular = result[0].conductor_celular;
                    var placaunidad = result[0].unidad_placa;
                    var colorunidad = result[0].unidad_color;
                    var marcaunidad = utf8.decode(result[0].unidad_marca.replace('Ã“','O'));
                    var modelounidad = utf8.decode(result[0].unidad_modelo.replace('Ã“','O'));
                    var latconductor = result[0].conductor_lat;
                    var lngconductor = result[0].conductor_lng;
                    var fotoconductor = result[0].conductor_foto;
                    var fotounidad = result[0].unidad_foto;
                    var calificacion = result[0].conductor_calificacion;
                    var viaje = result[0].conductor_viajes;
                    var caracteristicas = result[0].unidad_caracteristicas;
                    console.log('caracteristicas ServicioId', caracteristicas);
                    Clients.CustomServicio(servicioId,function (error, dac){
                        if(error){
                             console.log('ERROR socket.on(CustomServicio) part 1 ');
                        }else{
                            console.log(dac);
                            var socketid = '';
                            if(socketCliente != null){ // Biene de panel Web
                                socketid = socketCliente;
                            }else{// Desde Movil
                                socketid = dac[0].cliente_socket;
                            }
                            console.log('socketid ->'+ socketid + 'Conductor'+idConductor );
//                  
                            Reservas.ReservasAproximadas(idConductor,function(eror,resultado){
                                if(!eror){
                                   if(resultado.length > 0){
                                        var minutosDiferencia = parseInt(resultado[0].diferencia) * -1;
                                        var estado = 1;
                                        Reservas.ConfigReservaId(estado,function(er,resuEstado){
                                            if(!er){
                                                var minuto = '';
                                                switch(resuEstado[0].configreserva_bloqueo) {
                                                    case 1: minuto = parseInt(30); break;
                                                    case 2: minuto = parseInt(40); break;
                                                    case 3: minuto = parseInt(60); break;
                                                    case 4: minuto = parseInt(120); break;
                                                }
                                                
                                                if(minutosDiferencia <= parseInt(minuto)){
                                                     io.to(result[0].conductor_socket).emit('ReservaPendienteConductor',{ServicioId:servicioId});
    //
                                                }else{

                                                    if(socketCliente != null){
                                                        console.log('envio por el panel');
                                                          io.sockets.emit('SolicitudAceptar',{
                                                                    IdConductor:idConductor,
                                                                    NombreConductor:nombreConductor, 
                                                                    ApelliConductor:apelliConductor, 
                                                                    Placa:placaunidad,  
                                                                    Color:colorunidad, 
                                                                    Marca:marcaunidad, 
                                                                    Modelo:modelounidad,
                                                                    Viaje:viaje, 
                                                                    Latitud:latconductor,
                                                                    Longitud:lngconductor,
                                                                    Foto:fotoconductor,
                                                                    Calificacion:calificacion,
                                                                    ServicioId:servicioId,
                                                                    Caracteristicas:caracteristicas,
                                                                    FotoTaxi:fotounidad
                                                                                            });
                                                    }else{
                                                        console.log('envio para el cliente');
                                                         io.to(socketid).emit('SolicitudAceptar',{
                                                                    IdConductor:idConductor,
                                                                    NombreConductor:nombreConductor, 
                                                                    ApelliConductor:apelliConductor,
                                                                    Celular:conductorCelular,
                                                                    Placa:placaunidad,  
                                                                    Color:colorunidad, 
                                                                    Marca:marcaunidad, 
                                                                    Modelo:modelounidad,
                                                                    Viaje:viaje, 
                                                                    Latitud:latconductor,
                                                                    Longitud:lngconductor,
                                                                    Foto:fotoconductor,
                                                                    Calificacion:calificacion,
                                                                    ServicioId:servicioId,
                                                                    Caracteristicas:caracteristicas,
                                                                    FotoTaxi:fotounidad
                                                                                            });
                                                    }

                                                }
                                            }
                                        });
                                    }else{
                                        if(socketCliente != null){
                                                    console.log('envio por el panel');
                                                      io.sockets.emit('SolicitudAceptar',{
                                                                IdConductor:idConductor,
                                                                NombreConductor:nombreConductor, 
                                                                ApelliConductor:apelliConductor, 
                                                                Placa:placaunidad,  
                                                                Color:colorunidad, 
                                                                Marca:marcaunidad, 
                                                                Modelo:modelounidad,
                                                                Viaje:viaje, 
                                                                Latitud:latconductor,
                                                                Longitud:lngconductor,
                                                                Foto:fotoconductor,
                                                                Calificacion:calificacion,
                                                                ServicioId:servicioId,
                                                                Caracteristicas:caracteristicas
                                                                                        });
                                                }else{
                                                    console.log('envio para el cliente');
                                                     io.to(socketid).emit('SolicitudAceptar',{
                                                                IdConductor:idConductor,
                                                                NombreConductor:nombreConductor, 
                                                                ApelliConductor:apelliConductor,
                                                                Celular:conductorCelular,
                                                                Placa:placaunidad,  
                                                                Color:colorunidad, 
                                                                Marca:marcaunidad, 
                                                                Modelo:modelounidad,
                                                                Viaje:viaje, 
                                                                Latitud:latconductor,
                                                                Longitud:lngconductor,
                                                                Foto:fotoconductor,
                                                                Calificacion:calificacion,
                                                                ServicioId:servicioId,
                                                                Caracteristicas:caracteristicas
                                                                                        });
                                                }
                                    }
                                }
                            });
                        }
                    });              
            }
        });
    });
    ///});

    //Actualizar y Validar tb_servicio lo consume Cliente
    //validar qe no exista idconductor con la tabla servicio  diferente de 0 y 6 disponible si no ocupado
    //actualizar el servicio = 1 + idconductor agregar la fecha_seleccion 
    socket.on('SeleccionConductor',function(docs,callback){
        docs = JSON.parse(docs);
        const Idservicio = docs.idservicio;
        const idconductores = docs.idarray;
        const idconductorse = docs.idconductor;
        const flagunidad = docs.tipo;
        

        Conductors.updateConductorEstadoOcupado(2,idconductorse,function(err){
            if(!err){
                console.log('OK socket.on(updateConductorEstadoOcupado) parte 0',err);
            }else{
                console.log('ERROR socket.of(updateConductorEstadoOcupado) parte 1',err);
            }   
        });

        Conductors.validarServicioConductor(idconductorse,function(er,result){
            if(result){
                if(result[0].total == 0){
                    //Actualizar a Estado 1 => En Camino
                    var EstadoServicio = 1;
                    var fs = new fechaActual();
                    var fechaseleccion = fs['actual'];

                    console.log('Fecha y hora actual : '+ fechaseleccion);

                    Conductors.updateServicioEstado(Idservicio,idconductorse,EstadoServicio,fechaseleccion,function(err){
                        if(!err){
                            Conductors.validarConductor(idconductorse,function(err,resultConductor){
                                if(resultConductor){
                                    Conductors.findOneQueryServicio(Idservicio,function(error,reservicio){
                                        if(reservicio){

                                            console.log('socket del conductor : '+ resultConductor[0].conductor_socket);
                                            console.log('Servicio : '+ reservicio[0].servicio_id);
                                            
                                            Clients.findOneQueryCliente(reservicio[0].cliente_id,function(err, resultado){

                                                Seleccionado = {
                                                            IdServicio:reservicio[0].servicio_id,
                                                            IdCliente:reservicio[0].cliente_id, 
                                                            IdConductor:reservicio[0].conductor_id, 
                                                            OrigenId:reservicio[0].origen_id,  
                                                            OrigenNombre: (reservicio[0].origen_nombre  == null) ? '' : utf8.decode(reservicio[0].origen_nombre.replace('Ã“','O') ) , 
                                                            OrigenDireccion: (reservicio[0].origen_direccion == null) ? '': utf8.decode(reservicio[0].origen_direccion.replace('Ã“','O')), 
                                                            OrigenReferencia: (reservicio[0].origen_referencia == null) ? '' : utf8.decode(reservicio[0].origen_referencia.replace('Ã“','O') ),
                                                            OrigenLat:reservicio[0].origen_lat,  
                                                            OrigenLng:reservicio[0].origen_lon,
                                                            DestinoId:reservicio[0].destino_id,
                                                            DestinoNombre: (reservicio[0].destino_nombre  == null )? '':utf8.decode(reservicio[0].destino_nombre.replace('Ã“','O')),
                                                            DestinoDireccion: (reservicio[0].destino_direccion == null)? '' : utf8.decode(reservicio[0].destino_direccion.replace('Ã“','O')),
                                                            DestinoReferencia: ( reservicio[0].destino_referencia == null) ? '' : utf8.decode(reservicio[0].destino_referencia.replace('Ã“','O')), 
                                                            DestinoLat:reservicio[0].destino_lat,
                                                            DestinoLng:reservicio[0].destino_lon,
                                                            TipoPago:reservicio[0].tipo_pago,
                                                            Tarifa:reservicio[0].tarifa,
                                                            Estado:reservicio[0].estado,
                                                            FechaRecojo:reservicio[0].fecha_recojo,
                                                            imagen: (reservicio[0].imagen)? ('http://208.68.39.4/BL/'+reservicio[0].imagen) : '',
                                                            comentario: reservicio[0].comentario
                                                           }


                                                io.to(resultConductor[0].conductor_socket).emit('ConductorSeleccionado',{
                                                         Servicio:Seleccionado,
                                                         Cliente:resultado[0]
                                                })

                                                console.log('OK socket.on(findOneQueryServicio) parte 0',error);
                                                if(flagunidad == 1){
                                                    idconductores.forEach(function (docsRows) {
                                                        console.log('Conductores Id : '+ docsRows);
                                                        Conductors.validarConductor(docsRows,function(err,resultsocketAll){
                                                            console.log('socket : '+ resultsocketAll[0].conductor_socket);
                                                            if(resultsocketAll){
                                                                io.to(resultsocketAll[0].conductor_socket).emit('ServicioCaducado',{IDServicio:Idservicio
                                                                });
                                                                console.log('OK socket.on(validarConductorAll) parte 1',err);
                                                            }
                                                        });
                                                    }); 
                                                }

                                                if(idconductores != null){
                                                    callback('OK');
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }else{
                            console.log('ERROR socket.on(updateServicioEstado) part 2',err);
                        }
                    });
                }else{
                    callback('Error');
                }
            }else{
                console.log('ERROR socket.on(validarServicioConductor) part 3', er);
            }
        });
    });
    
    //idconductor,idservicio
    //LLegadaConductor
    //actualizar => estado , fecha_recojo
    //callback(ok);
    //emit->cliente => fecha de recojo; 
    socket.on('LLegadaConductor',function(docs,callback){

        docs = JSON.parse(docs);

        var idConductor = docs.IdConductor;
        var idServicio = docs.IdServicio;
        var EstadoServicio = 2;
        
        var f = new fechaActual();
        var fecha_recojo = f['actual'];
        console.log(idServicio);
        console.log(fecha_recojo);
        Conductors.updateServicioEstadoAll(idServicio,idConductor,EstadoServicio,fecha_recojo,function(err){
            if(!err){
                Clients.CustomServicio(idServicio,function(error,result){
                    if(result){
                         io.to(result[0].cliente_socket).emit('ConductorLLego',{
                                    FechaRecojo:fecha_recojo
                                });

                                request(global.CONFIG.URL_PROJECT+'ws/cliente/ws_notifiConductor_llego.php?cliente_id='+result[0].cliente_id+'&tipo=0',function(error,response,body){
                                    if(error || response.statusCode != 200){
                                        // console.log('request LLegadaConductor');
                                        console.log('Error Send Request : ', error);
                                    }
                                });

                         callback('Ok');
                          console.log('OK socket.on(LLegadaConductor) parte 0',err);
                    }else{
                       console.log('ERROR socket.on(CustomServicio) part 1', err); 
                    }
                });
            }else{
                console.log('ERROR socket.on(updateServicioEstadoAll) part 2', err);
            }
        });

    });


    socket.on('IniciarServicio',function(docs,callback){

        docs = JSON.parse(docs);

        var idConductor = docs.IdConductor;
        var idServicio = docs.IdServicio;
        var EstadoServicio = 3;
        
        var f = new fechaActual();
        var fechaInicio = f['actual'];

        /*Conductors.findOneQueryServicio(idServicio,function(error,resultado){
            if(resultado){
                var idCliente = resultado[0].cliente_id;
                var validtarifa = resultado[0].tarifa;
                var tipoPago = resultado[0].tipo_pago;
                if(validtarifa != 0){
                        Services.validarEstadoCuentaId(idServicio,function(error,resultado){
                            if(!error){
                                if(resultado[0].total == 0){
                                    console.log('id CONDUCTOR :'+ idConductor);
                                    Conductors.validarConductor(idConductor,function(err,respta){
                                        console.log(respta);
                                        if(!err){
                                            var tipoComision = respta[0].conductor_comision;
                                            var montoComision = respta[0].conductor_comision_total;
                                            var montoRecarga = respta[0].conductor_recarga;
                                            if(montoComision != 0){
                                                if(tipoComision == 1){// X porcentaje
                                                    if(tipoPago != 1){
                                                        var cta_monto = parseFloat(validtarifa) - parseFloat(parseFloat(validtarifa * montoComision)/100);
                                                    }else{
                                                        var cta_monto = parseFloat((validtarifa * montoComision)/100);
                                                    }
                                                    
                                                    var f = new fechaActual();
                                                    var fechaCta = f['actual'];
                                                    var dataCta = {idConductor:idConductor,idServicio:idServicio,Ctatipo:2,Ctafecha:fechaCta,Ctamonto:cta_monto,CtaCheck:1};

                                                    Services.insertCta(dataCta, function(err, resultCta){
                                                        if(err){
                                                            console.log('ERROR socket.on(insertCta) parte 0 tipo porcentaje ',err);
                                                        }else{
                                                            if(tipoPago != 1){
                                                                var saldoActual = montoRecarga + cta_monto;
                                                            }else{
                                                                var saldoActual = montoRecarga - cta_monto;
                                                            }
                                                            
                                                            Conductors.updateRecargaMonto(idConductor,saldoActual,function(er){
                                                                if(er){
                                                                    console.log('ERROR socket.on(updateRecargaMonto) parte 0 porcentaje',er);
                                                                }else{
                                                                    console.log('OK socket.on(updateRecargaMonto) parte 0 porcentaje',er);
                                                                }   
                                                            });
                                                        }
                                                    });        

                                                }else if(tipoComision == 2){// X monto

                                                    if(tipoPago != 1){
                                                        montoComision = parseFloat(validtarifa - montoComision);
                                                    }

                                                    var dataCta = {idConductor:idConductor,idServicio:idServicio,Ctatipo:2,Ctafecha:fechaCta,Ctamonto:montoComision,CtaCheck:1};
                                                    Services.insertCta(dataCta, function(err, resultCta){
                                                        if(err){
                                                            console.log('ERROR socket.on(insertCta) parte 0 tipo monto ',err);
                                                        }else{
                                                            if(tipoPago != 1){
                                                                var saldoActual = montoRecarga + montoComision;
                                                            }else{
                                                                var saldoActual = montoRecarga - montoComision;
                                                            }
                                                            
                                                            Conductors.updateRecargaMonto(idConductor,saldoActual,function(er){
                                                                if(er){
                                                                    console.log('ERROR socket.on(updateRecargaMonto) parte 0 monto',er);
                                                                }else{
                                                                    console.log('OK socket.on(updateRecargaMonto) parte 0 monto',er);
                                                                }   
                                                            });
                                                        }
                                                    });
                                                }

                                            }else{
                                                 console.log('ERROR socket.on(validarConductor) comision 0 ',error);
                                            }
                                            console.log('CLIENTE ID DESDE INICIAR SERVICIO'+ idCliente);
                                            Services.seguridadCliente(idCliente,function(error,resulseguridad){
                                                    if(!error){
                                                        console.log('RESULTADO DE SEGURIDAD'+resulseguridad.length);
                                                        if(resulseguridad.length > 0){
                                                            //send correo de seguridad
                                                                request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+docs.IdServicio+'&tipo=1', function (error, response, body) {
                                                                    console.log(body);
                                                                    console.log('Se ejecuto el correo de seguridad'+response);
                                                                    if (!error && response.statusCode == 200) {
                                                                         // Show the HTML for the Google homepage. 
                                                                        //res.send('Mensaje enviado');
                                                                        console.log('Mensaje enviado');
                                                                    }else {
                                                                        console.log("Error "+response.statusCode)
                                                                        console.log('Mensaje NO enviado');
                                                                        //res.send('Mensaje NO enviado');
                                                                    }
                                                                });
                                                            //end send de seguridad

                                                        }else{
                                                            console.log('ERROR socket.on(seguridadCliente) no existe Cliente ',error);
                                                        }
                                                    }else{
                                                         console.log('ERROR socket.on(seguridadCliente) parte 1 ',error);
                                                    }

                                            });

                                        }
                                    });
                                }else{
                                    console.log('ERROR socket.on(validarServicioId) parte 0 existe',error);
                                }
                            }
                        });
                }else{
                    console.log('CLIENTE ID DESDE INICIAR SERVICIO EXPRESS'+ idCliente);
                    Services.seguridadCliente(idCliente,function(error,resulseguridad){
                            if(!error){
                                console.log('RESULTADO DE SEGURIDAD EXPRESS'+resulseguridad.length);
                                if(resulseguridad.length > 0){
                                    //send correo de seguridad
                                        request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+docs.IdServicio+'&tipo=1', function (error, response, body) {
                                            console.log(body);
                                            console.log('Se ejecuto el correo de seguridad'+response);
                                            if (!error && response.statusCode == 200) {
                                                 // Show the HTML for the Google homepage. 
                                                //res.send('Mensaje enviado');
                                                console.log('Mensaje enviado');
                                            }else {
                                                console.log("Error "+response.statusCode)
                                                console.log('Mensaje NO enviado');
                                                //res.send('Mensaje NO enviado');
                                            }
                                        });
                                    //end send de seguridad

                                }else{
                                    console.log('ERROR socket.on(seguridadCliente) no existe Cliente ',error);
                                }
                            }else{
                                 console.log('ERROR socket.on(seguridadCliente) parte 1 ',error);
                            }

                    });
                    console.log('ERROR socket.on(findOneQueryServicio) part 1 servicio Express', error); 
                }
            }else{
                console.log('ERROR socket.on(findOneQueryServicio) part 1', error); 
            }
        });*/
        Conductors.findOneQueryServicio(idServicio,function(error,resultado){
            if(resultado){
                var idCliente = resultado[0].cliente_id;

                Services.seguridadCliente(idCliente,function(error,resulseguridad){
                        if(!error){
                            // console.log('RESULTADO DE SEGURIDAD EXPRESS'+resulseguridad.length);
                            if(resulseguridad.length > 0){
                                //send correo de seguridad
                                    request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+docs.IdServicio+'&tipo=1', function (error, response, body) {
                                        // console.log(body);
                                        // console.log('Se ejecuto el correo de seguridad'+response);
                                        if (!error && response.statusCode == 200) {
                                             // Show the HTML for the Google homepage. 
                                            //res.send('Mensaje enviado');
                                            console.log('Mensaje enviado');
                                        }else {
                                            console.log("Error "+response.statusCode)
                                            console.log('Mensaje NO enviado');
                                            //res.send('Mensaje NO enviado');
                                        }
                                    });
                                //end send de seguridad

                            }else{
                                console.log('ERROR socket.on(seguridadCliente) no existe Cliente ',error);
                            }
                        }else{
                             console.log('ERROR socket.on(seguridadCliente) parte 1 ',error);
                        }
                });
            }else{
                console.log("%cERROR socket.on(seguridadCliente) no existe Cliente", "color: red", error);
            }
        });  

        Conductors.updateServicioEstadoAll(idServicio,idConductor,EstadoServicio,fechaInicio,function(err){
            if(!err){
                Clients.CustomServicio(idServicio,function(error,result){
                    if(result){
                         io.to(result[0].cliente_socket).emit('InicioServicio',{
                                    FechaInicio:fechaInicio
                                });

                         request(global.CONFIG.URL_PROJECT+'ws/cliente/ws_notifiConductor_llego.php?cliente_id='+result[0].cliente_id+'&tipo=1',function(error,response,body){
                                    if(error || response.statusCode != 200){
                                        // console.log('request IniciarServicio');
                                        console.log('Error Send Request : ', error);
                                    }
                                });

                        callback('Ok');
                        console.log('OK socket.on(IniciarServicio) parte 0',err);
                    }else{
                       console.log('ERROR socket.on(CustomServicio) part 1', err); 
                    }
                });
            }else{
                console.log('ERROR socket.on(updateServicioEstadoAll) part 2', err);
            }
        });

    });

    //Actualizar la tb.conductores => conductor_vista = 5
    //Actualizar la tb_cliente => cliente_vista = 4
    //Actualizar la tb_servicio = estado = 6 , hora_fin = fecha
    socket.on('FinalizarServicio',function(docs,callback){

        // console.log('Hora Actual..'+strftime('%H:%M %p'));
        // console.log('Dia Actual..'+strftime('%A'));
        console.log('::: Finaliza servicio :::');
        console.log(docs);
        docs = JSON.parse(docs);
        var distancia = parseFloat(docs.distancia);
        var idConductor = docs.IdConductor;
        var idServicio = docs.IdServicio;
        var flagexpreso  = docs.flagexpreso; // 0 no es , 1 si es
        var DestinoId =  docs.DestinoId;
        var empresa_id = docs.empresa_id;
        var DestinoLat = docs.DestinoLat;
        var DestinoLng = docs.DestinoLng;
        var DestinoDireccion='';
        var Paradas = parseFloat(docs.Paradas);
        var Parqueos = parseFloat(docs.Parqueos);
        var Peajes= parseFloat(docs.Peajes);
        var Espera = parseFloat(docs.Espera);
        var Activo = parseInt(docs.activo);
        var reserva = parseInt(docs.reserva);
        var EstadoServicio = 6;
        var ClienteVista = 4;
        var ConductorVista = 5;
        var Tarifa = parseFloat(docs.tarifa);
        var envio = docs.envio;
        console.log(`${Peajes}, ${Parqueos}, ${Paradas}`)
        
        // console.log('expreso-->'+flagexpreso);
        // console.log('aeropuerto-> '+Activo);
        // console.log('tarifa real'+ Tarifa);
        //if(Activo == 1){
            //Tarifa = parseFloat(docs.tarifa)+parseFloat(aeropuerto);
        //}
        // console.log('tarifa nueva = '+Tarifa);
        var f = new fechaActual();
        var fechaFin = f['actual'];

        //if(flagexpreso == 1 ){
            //if(Activo == 1){
             //Tarifa = parseFloat(docs.tarifa)+parseFloat(aeropuerto);
            //}
        //}

        ///if(flagexpreso != 1 ){
            //DestinoDireccion = null;
        //}else{
            DestinoDireccion = utf8.encode(docs.DestinoDireccion);
        //}    

        Conductors.updateServicioExpress(idServicio,idConductor,flagexpreso,DestinoId,DestinoDireccion,DestinoLat,DestinoLng,Paradas,Parqueos,Peajes,Espera,Tarifa,function(err){
            if(!err){
                // console.log('1');
                Conductors.updateServicioEstadoAll(idServicio,idConductor,EstadoServicio,fechaFin,function(err){
                    if(!err){
                        // console.log('2');
                        Clients.CustomServicio(idServicio,function(error,result){
                            console.log('servicio'+idServicio);
                            if(result){
                                // console.log('3');
                                Clients.updateClienteVista(result[0].cliente_id,ClienteVista,function(err){
                                    if(!err){
                                        // console.log('4');
                                        Conductors.validarConductor(idConductor,function(err,resulcond){
                                            if(resulcond){
                                                var contador = resulcond[0].conductor_viajes + 1;
                                                Conductors.updateConductorVista(idConductor,ConductorVista,contador,function(error){
                                                    // console.log('5');
                                                    if(!error){
                                                        console.log('OK socket.on(updateConductorVista) parte 0',err);
                                                        Clients.updateClienteCalificacion(result[0].cliente_id,function(err){
                                                            if(!err){
                                                                //Actualizacion de Cliente Viajes
                                                                Clients.findOneQueryCliente(result[0].cliente_id,function(error,datacliente){
                                                                    if(!error){
                                                                        var contadorCliente = datacliente[0].cliente_viajes + 1;
                                                                        // console.log('contador'+contadorCliente);
                                                                        // console.log('cliente'+result[0].cliente_id);
                                                                        Clients.updateClienteViajes(result[0].cliente_id,contadorCliente,function(err){
                                                                            if(!err){
                                                                                 console.log('OK socket.on(updateClienteViajes) parte 0',err);   
                                                                            }else{
                                                                                console.log('Error socket.on(updateClienteViajes) parte 0',err);
                                                                            }
                                                                        })
                                                                    }
                                                                });
                                                                // End Actualizacion de Viajes
                                                                
                                                                Conductors.updateConductorEstadoOcupado(1,idConductor,function(err){
                                                                    if(!err){
                                                                        console.log('OK socket.on(updateConductorEstadoOcupado) parte 0',err);
                                                                    }else{
                                                                        console.log('ERROR socket.of(updateConductorEstadoOcupado) parte 1',err);
                                                                    }   
                                                                });

    request(global.CONFIG.URL_PROJECT+'ws/cliente/ws_notifiConductor_llego.php?cliente_id='+result[0].cliente_id+'&tipo=2',function(error,response,body){
        if(error || response.statusCode != 200){
            // console.log('request FinalizarServicio');
            console.log('Error Send Request : ', error);
        }
    });

    //Validar IdServico en la tabla Estado de Cuentas
    /*Conductors.findOneQueryServicio(idServicio,function(err,data){
        
        var Clienteid = data[0].cliente_id;
        var validtarifa = data[0].tarifa;
        var tipoPago = data[0].tipo_pago;

        Services.validarEstadoCuentaId(idServicio,function(error,resultado){
            if(!error){
                if(resultado[0].total == 0){
                    console.log('id CONDUCTOR :'+ idConductor);
                    Conductors.validarConductor(idConductor,function(err,respta){
                        console.log(respta);
                        if(!err){
                            var tipoComision = respta[0].conductor_comision;
                            var montoComision = respta[0].conductor_comision_total;
                            var montoRecarga = respta[0].conductor_recarga;
                            if(montoComision != 0){

                                if(tipoComision == 1){// X porcentaje
                                    if(tipoPago != 1){
                                        var cta_monto = parseFloat(validtarifa) - parseFloat(parseFloat(validtarifa * montoComision)/100);
                                    }else{
                                        var cta_monto = parseFloat((validtarifa * montoComision)/100);
                                    }
                                    //var cta_monto = parseFloat((Tarifa * montoComision)/100);
                                    var f = new fechaActual();
                                    var fechaCta = f['actual'];
                                    var dataCta = {idConductor:idConductor,idServicio:idServicio,Ctatipo:2,Ctafecha:fechaCta,Ctamonto:cta_monto,CtaCheck:1};

                                    Services.insertCta(dataCta, function(err, resultCta){
                                        if(err){
                                            console.log('ERROR socket.on(insertCta) parte 0 tipo porcentaje ',err);
                                        }else{

                                            if(tipoPago != 1){
                                                var saldoActual = montoRecarga + cta_monto;
                                            }else{
                                                var saldoActual = montoRecarga - cta_monto;
                                            }

                                            Conductors.updateRecargaMonto(idConductor,saldoActual,function(er){
                                                if(er){
                                                    console.log('ERROR socket.on(updateRecargaMonto) parte 0 porcentaje',er);
                                                }else{
                                                    console.log('OK socket.on(updateRecargaMonto) parte 0 porcentaje',er);
                                                }   
                                            });
                                        }
                                    });        

                                }else if(tipoComision == 2){// X monto


                                    if(tipoPago != 1){
                                        montoComision = parseFloat(validtarifa - montoComision);
                                    }

                                    var dataCta = {idConductor:idConductor,idServicio:idServicio,Ctatipo:2,Ctafecha:fechaCta,Ctamonto:montoComision,CtaCheck:1};
                                    Services.insertCta(dataCta, function(err, resultCta){
                                        if(err){
                                            console.log('ERROR socket.on(insertCta) parte 0 tipo monto ',err);
                                        }else{
                                            if(tipoPago != 1){
                                                var saldoActual = montoRecarga + montoComision;
                                            }else{
                                                var saldoActual = montoRecarga - montoComision;
                                            }

                                            Conductors.updateRecargaMonto(idConductor,saldoActual,function(er){
                                                if(er){
                                                    console.log('ERROR socket.on(updateRecargaMonto) parte 0 monto',er);
                                                }else{
                                                    console.log('OK socket.on(updateRecargaMonto) parte 0 monto',er);
                                                }   
                                            });
                                        }
                                    });
                                }
                            }else{
                                 console.log('ERROR socket.on(validarConductor) comision 0 ',error);
                            }
                        }
                    });
                }else{
                    console.log('ERROR socket.on(validarServicioId) parte 0 existe',error);
                }
            }
        });
    });*/

    //Actualizar los campos de cliente = cliente_puntos and conductor = conductor_puntos
    Conductors.findOneQueryServicio(idServicio,function(err,data){
        if(!err){
            Services.configPtos(1,function(error,dataptos){
                // console.log(dataptos);
                if(!error){
                    if(dataptos.length != 0){
                        var idClientePtos = data[0].cliente_id;
                        var idConductorPtos = docs.IdConductor;
                        var Tarifanew = parseInt(docs.tarifa);
                        var ptosCliente = parseInt(dataptos[0].puntos_cliente);
                        var ptosConductor = parseInt(dataptos[0].puntos_conductor);
                        var factorCliente = parseInt(dataptos[0].puntos_factor_cliente);
                        var factorConductor = parseInt(dataptos[0].puntos_factor_conductor);

                        var totalPtosCliente = parseInt(eval(Tarifanew/factorCliente))*ptosCliente;
                        var totalPtosConductor = parseInt(eval(Tarifanew/factorConductor))*ptosConductor;
                        
                        var NewtotalPtosCliente = isNaN(parseInt(totalPtosCliente));
                        var NewtotalPtosConductor = isNaN(parseInt(totalPtosConductor));

                        if(NewtotalPtosConductor == true){ totalPtosConductor = parseInt(0);}
                        if(NewtotalPtosCliente == true){ totalPtosCliente = parseInt(0);}
                                
                        // console.log(totalPtosCliente);
                        // console.log(totalPtosConductor);

                        Conductors.validarConductor(idConductorPtos,function(err,datacond){
                            if(!err){
                                var ptosActualConductor = parseInt(eval(datacond[0].conductor_puntos+totalPtosConductor));
                                Conductors.updateConductorPtos(idConductorPtos,ptosActualConductor,function(err){
                                    if(!err){
                                    console.log('OK socket.on(updateConductorPtos) parte 0',err);
                                        Clients.findOneQueryCliente(idClientePtos,function(errcli,dataClie){
                                            if(!err){
                                                 var ptosActualCliente = parseInt(eval(dataClie[0].cliente_puntos+totalPtosCliente));
                                                Clients.updateClientePtos(idClientePtos,ptosActualCliente,function(err){
                                                    if(!err){
                                                         console.log('OK socket.on(updateClientePtos) parte 0',err);
                                                    }
                                                });
                                            }
                                        });  
                                    }
                                });
                            } 
                        });
                    }else{
                        console.log('ERROR socket.on(configPtos) data 0',err);
                    }
                }
            });
        }else{
            console.log('ERROR socket.on(findOneQueryServicio) parte 1',err);
        }
    });

//Tarifa por Kilometros
    Services.MinutoServicios(idServicio,function(error,resultado){
        if(!error){
            //   console.log('********[SERVER] no hay ERROR');
            var Tiempo = resultado[0].minutos;
            var config_id = 1; 

            Lugar.ListarConfig(config_id,function(err,resultConfig){

                if(!err){
                    // console.log('tipo de tarifa config general '+resultConfig[0].tipo_tarifa);
                    if(resultConfig[0].tipo_tarifa != 1){

                            request(global.CONFIG.URL_PROJECT+'ws/conductor/ws_horas_puntas.php', function (error, response, body) {
                                // console.log(body);
                                var obj  = JSON.parse(body);
                                // console.log(obj);
                                if(obj.status == true){

                                    // console.log('distancia '+distancia);
                                    // console.log('tiempor en Minutos'+ Tiempo);
                                    var HorasPuntas = parseFloat(1+(obj.valor/100));
                                    var Pbase = resultConfig[0].tarifa_minima;
                                    var CostoKilometro = resultConfig[0].factor_tarifa_km;
                                    var Distancia = distancia;
                                    var CostoTiempo = resultConfig[0].factor_tarifa_tiempo;
                                    var TarifaMinima = resultConfig[0].tarifa_minima_m;

                                    var f1 = parseFloat(Pbase+(Tiempo*CostoTiempo)+(Distancia*CostoKilometro));
                                    // console.log('f1', f1);
                                    Conductors.ConductoralertaTaxi(idConductor,function(errorC,resultConductor){
                                        if(!errorC){


                                        // console.log('NO HAY ERROR EN ERRORC');

                                            var fservicio = parseFloat(1+parseFloat(resultConductor[0].tiposervicio_factor/100));
                                            // console.log('f1 inicio', fservicio);
                                            var f1fservicio = parseFloat(f1*fservicio);
                                            // console.log('f1 inicio 2', f1fservicio);
                                            if(f1fservicio < TarifaMinima){ f1fservicio = TarifaMinima;}

                                            var aeropuerto = 0;
                                            if(Activo == 1){
                                                aeropuerto = parseFloat(resultConductor[0].tiposervicio_aeropuerto);
                                                if(envio == 'origen'){
                                                    aeropuerto = aeropuerto + parseFloat(resultConfig[0].aeropuertoD);
                                                }else{
                                                    aeropuerto = aeropuerto + parseFloat(resultConfig[0].aeropuertoO);
                                                }
                                            }

                                            var MontoReserva = 0;
                                            if(reserva == 1){//Es una reserva
                                                MontoReserva = parseFloat(resultConductor[0].tiposervicio_reserva);
                                            }

                                            var TarifaInicial = parseFloat(f1fservicio+aeropuerto+MontoReserva);
                                            // console.log('tarif', TarifaInicial);
                                            var TarifaFinal = parseFloat(TarifaInicial*HorasPuntas);
                                            // console.log('tarifhorashhh', HorasPuntas);
                                            // console.log('tarifhoras', TarifaFinal);
                                            // console.log('Tarifa Final es ',TarifaFinal);
                                            // console.log('Tarifa Es ',Tarifa);

                                            // console.log('Antes del If', TarifaFinal);
                                            
                                            if(Tarifa != TarifaMinima){

                                                if(TarifaFinal > TarifaMinima){

                                                    //if(TarifaFinal < Tarifa){
                                                        // console.log('********[SERVER] Entra');
                                                        TarifaFinal = Tarifa;
                                                    //} 

                                                }
                                            }

                                            // console.log('Despues del If', Math.round(TarifaFinal,2));

                                            var TarifaFinal = TarifaFinal*2;
                                            var TarifaFinal = Math.ceil(TarifaFinal)/2;
                                            
                                            Services.updateTarifaNewKilometraje(idServicio,TarifaFinal,function(er){

                                                if(!er){
                                                    // INICIO CALCULAR LA COMICION
                                                            Conductors.findOneQueryServicio(idServicio,function(err,data){
                                                                
                                                                // console.log(data);
                                                                var Clienteid = data[0].cliente_id;
                                                                var validtarifa = data[0].tarifa;
                                                                var tipoPago = data[0].tipo_pago;
                                                                // console.log('Espera', Espera);
                                                                validtarifa = parseFloat(validtarifa)+parseFloat(Espera)+parseFloat(Paradas);
                                                                // console.log('NUEVA TARIFA SUMADA.....'+validtarifa+'.....servicio id'+idServicio);
                                                                Clients.findOneQueryCliente(Clienteid,function(errorCliente,resultadoCliente){
                                                                    if(!errorCliente){
                                                                        var tipoCliente = resultadoCliente[0].tipocliente_id;
                                                                        // console.log('id CLIENTE :'+ Clienteid + ' TIPO CLIENTE :'+ tipoCliente);
                                                                            Services.validarEstadoCuentaId(idServicio,function(error,resultado){
                                                                                if(!error){
                                                                                    // console.log(resultado[0].total);
                                                                                    if(resultado[0].total == 0){

                                                                                        // console.log('id CONDUCTOR :'+ idConductor);

                                                                                        Conductors.validarConductor(idConductor,function(err,respta){
                                                                                            // console.log(respta);
                                                                                            if(!err){

                                                                                                var tipoComision = 0;
                                                                                                var montoComision = 0;

                                                                                                //if(tipoCliente != 1){
                                                                                                    //Cliente Coorporativo
                                                                                                    tipoComision = respta[0].conductor_comision;
                                                                                                    montoComision = respta[0].conductor_comision_total;
                                                                                                //}else{
                                                                                                    //Cliente Particular no existe estos campos en la tabla conductor
                                                                                                    //tipoComision = respta[0].conductor_comision_par;
                                                                                                    //montoComision = respta[0].conductor_comision_total_par;
                                                                                                //}
                                                                                                

                                                                                                var montoRecarga = respta[0].conductor_recarga;

                                                                                                if(montoComision != 0){

                                                                                                    if(tipoComision == 1){// X porcentaje
                                                                                                        if(tipoPago != 1){
                                                                                                            var cta_monto1 = parseFloat(validtarifa) - parseFloat(parseFloat(validtarifa * montoComision)/100);
                                                                                                            var cta_monto = parseInt(cta_monto1)+parseInt(Peajes)+parseInt(Parqueos);
                                                                                                        }else{
                                                                                                            var cta_monto = parseFloat((validtarifa * montoComision)/100);
                                                                                                        }
                                                                                                       
                                                                                                    //    console.log('tarifa guardar en estado de cuenta....'+cta_monto);

                                                                                                        var f = new fechaActual();
                                                                                                        var fechaCta = f['actual'];
                                                                                                        var dataCta = {idConductor:idConductor,idServicio:idServicio,Ctatipo:2,Ctafecha:fechaCta,Ctamonto:cta_monto,CtaCheck:1};
                                                                                                        // console.log('dataCta');
                                                                                                        // console.log(dataCta);
                                                                                                        Services.insertCta(dataCta, function(err, resultCta){
                                                                                                            if(err){
                                                                                                                console.log('ERROR socket.on(insertCta) parte 0 tipo porcentaje ',err);
                                                                                                            }else{

                                                                                                                if(tipoPago != 1){
                                                                                                                    var saldoActual = montoRecarga + cta_monto;
                                                                                                                }else{
                                                                                                                    var saldoActual = montoRecarga - cta_monto;
                                                                                                                }

                                                                                                                Conductors.updateRecargaMonto(idConductor,saldoActual,function(er){
                                                                                                                    if(er){
                                                                                                                        console.log('ERROR socket.on(updateRecargaMonto) parte 0 porcentaje',er);
                                                                                                                    }else{
                                                                                                                        console.log('OK socket.on(updateRecargaMonto) parte 0 porcentaje',er);

                                                                                                                        //Envio de Correo a Cliente 
                                                    request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+idServicio+'&tipo=2', function (error, response, body) {
                                                        console.log('Se ejecuto el correo de detalle'+response);
                                                        if (!error && response.statusCode == 200) {
                                                            // Show the HTML for the Google homepage. 
                                                            //res.send('Mensaje enviado');
                                                            console.log('Mensaje enviado detalle');
                                                        }else {
                                                            console.log("Error "+response.statusCode)
                                                            console.log('Mensaje NO enviado detalle');
                                                            //res.send('Mensaje NO enviado');
                                                        }
                                                    });
                                                    //End 


                                                    console.log(result[0].cliente_socket, '111111111');
                                                    io.to(result[0].cliente_socket).emit('FinalizarServicioCliente',{
                                                        FechaFin:fechaFin,
                                                        Paradas:parseFloat(docs.Paradas),
                                                        Parqueos:parseFloat(docs.Parqueos),
                                                        Peajes:parseFloat(docs.Peajes),
                                                        Espera:parseFloat(docs.Espera),
                                                        Tarifa:TarifaFinal,
                                                        TipoPago:tipoPago
                                                    });

                                                    send = {Servicio :idServicio,tarifa: TarifaFinal};
                                                    callback(send);
                                                    console.log('OK socket.on(updateTarifaNewKilometraje) parte 0 tipo tarifa X Kilometraje 111',er);


                                                                                                                    }   
                                                                                                                });
                                                                                                            }
                                                                                                        });        

                                                                                                    }else if(tipoComision == 2){// X monto


                                                                                                        if(tipoPago != 1){
                                                                                                            montoComision = parseFloat(validtarifa - montoComision)+parseInt(Peajes)+parseInt(Parqueos);
                                                                                                        }

                                                                                                        var dataCta = {idConductor:idConductor,idServicio:idServicio,Ctatipo:2,Ctafecha:fechaCta,Ctamonto:montoComision,CtaCheck:1};
                                                                                                        Services.insertCta(dataCta, function(err, resultCta){
                                                                                                            if(err){
                                                                                                                console.log('ERROR socket.on(insertCta) parte 0 tipo monto ',err);
                                                                                                            }else{
                                                                                                                if(tipoPago != 1){
                                                                                                                    var saldoActual = montoRecarga + montoComision;
                                                                                                                }else{
                                                                                                                    var saldoActual = montoRecarga - montoComision;
                                                                                                                }

                                                                                                                Conductors.updateRecargaMonto(idConductor,saldoActual,function(er){
                                                                                                                    if(er){
                                                                                                                        console.log('ERROR socket.on(updateRecargaMonto) parte 0 monto',er);
                                                                                                                    }else{
                                                                                                                        console.log('OK socket.on(updateRecargaMonto) parte 0 monto',er);

                                                                                                                        //Envio de Correo a Cliente 
                                                    request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+idServicio+'&tipo=2', function (error, response, body) {
                                                        // console.log('Se ejecuto el correo de detalle'+response);
                                                        if (!error && response.statusCode == 200) {
                                                            // Show the HTML for the Google homepage. 
                                                            //res.send('Mensaje enviado');
                                                            console.log('Mensaje enviado detalle');
                                                        }else {
                                                            console.log("Error "+response.statusCode)
                                                            console.log('Mensaje NO enviado detalle');
                                                            //res.send('Mensaje NO enviado');
                                                        }
                                                    });
                                                    //End 


                                                    // console.log(result[0].cliente_socket, '222222');
                                                    io.to(result[0].cliente_socket).emit('FinalizarServicioCliente',{
                                                        FechaFin:fechaFin,
                                                        Paradas:parseFloat(docs.Paradas),
                                                        Parqueos:parseFloat(docs.Parqueos),
                                                        Peajes:parseFloat(docs.Peajes),
                                                        Espera:parseFloat(docs.Espera),
                                                        Tarifa:TarifaFinal,
                                                        TipoPago:tipoPago
                                                    });

                                                    send = {Servicio :idServicio,tarifa: TarifaFinal};
                                                    callback(send);
                                                    console.log('OK socket.on(updateTarifaNewKilometraje) parte 0 tipo tarifa X Kilometraje 222',er);

                                                                                                                    }   
                                                                                                                });
                                                                                                            }
                                                                                                        });
                                                                                                    }
                                                                                                }else{
                                                                                                     console.log('ERROR socket.on(validarConductor 1) comision 0 ',error);

                                                                                                    request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+idServicio+'&tipo=2', function (error, response, body) {
                                                                                                        console.log('Se ejecuto el correo de detalle'+response);
                                                                                                        if (!error && response.statusCode == 200) {
                                                                                                            // Show the HTML for the Google homepage. 
                                                                                                            //res.send('Mensaje enviado');
                                                                                                            console.log('Mensaje enviado detalle');
                                                                                                        }else {
                                                                                                            console.log("Error "+response.statusCode)
                                                                                                            console.log('Mensaje NO enviado detalle');
                                                                                                            //res.send('Mensaje NO enviado');
                                                                                                        }
                                                                                                    });
                                                                                                    //End 


                                                                                                    // console.log(result[0].cliente_socket, 'comision 0');
                                                                                                    io.to(result[0].cliente_socket).emit('FinalizarServicioCliente',{
                                                                                                        FechaFin:fechaFin,
                                                                                                        Paradas:parseFloat(docs.Paradas),
                                                                                                        Parqueos:parseFloat(docs.Parqueos),
                                                                                                        Peajes:parseFloat(docs.Peajes),
                                                                                                        Espera:parseFloat(docs.Espera),
                                                                                                        Tarifa:TarifaFinal,
                                                                                                        TipoPago:tipoPago
                                                                                                    });

                                                                                                    send = {Servicio :idServicio,tarifa: TarifaFinal};
                                                                                                    callback(send);

                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    }else{
                                                                                        Services.findQueryEstadoCuentaId(idServicio,function(errorEs,resultEstad){

                                                                                                var cta_id = resultEstad[0].cta_id;
                                                                                                // console.log('nueva cta_id', cta_id);                                                                                                
                                                                                                Conductors.validarConductor(resultEstad[0].conductor_id,function(err,respta){
                                                                                    
                                                                                                    if(!err){
                                                                                                    // console.log(resultEstad[0].conductor_id);
                                                                                                        // console.log(respta[0]);
                                                                                                        var tipoComision = 0;
                                                                                                        var montoComision = 0;
                                                                                                        // console.log('tipoCliente', tipoCliente);
                                                                                                        //if(tipoCliente != 1){
                                                                                                            //Cliente Coorporativo
                                                                                                            tipoComision = respta[0].conductor_comision;
                                                                                                            montoComision = respta[0].conductor_comision_total;
                                                                                                        //}else{
                                                                                                            //Cliente Particular no existe estos campos en la tabla conductor
                                                                                                            //tipoComision = respta[0].conductor_comision_par;
                                                                                                            //montoComision = respta[0].conductor_comision_total_par;
                                                                                                        //}

                                                                                                        
                                                                                                        var montoRecarga = respta[0].conductor_recarga;

                                                                                                        // console.log('monto Comision existe cta_id', montoComision);
                                                                                                        // console.log('tipo Comision existe cta_id', tipoComision);
                                                                                                        // console.log('tipo pago existe cta_id', tipoPago);
                                                                                        
                                                                                                        if(montoComision != 0){

                                                                                                            if(tipoComision == 1){// X porcentaje

                                                                                                                if(tipoPago != 1){
                                                                                                                    var cta_monto1 = parseFloat(validtarifa) - parseFloat(parseFloat(validtarifa * montoComision)/100);
                                                                                                                    var cta_monto = parseInt(cta_monto1)+parseInt(Peajes)+parseInt(Parqueos);
                                                                                                                }else{
                                                                                                                    var cta_monto = parseFloat((validtarifa * montoComision)/100);
                                                                                                                }
                                                                                                            
                                                                                                                // console.log('tarifa guardar en estado de cuenta....'+cta_monto);

                                                                                                                //var f = new fechaActual();
                                                                                                                //var fechaCta = f['actual'];
                                                                                                                //var dataCta = {idConductor:idConductor,idServicio:idServicio,Ctatipo:2,Ctafecha:fechaCta,Ctamonto:cta_monto,CtaCheck:1};
                                                                                                                // console.log('111111 ---> ',cta_monto);
                                                                                                                Services.updateEstadoCuentaMonto(cta_monto,cta_id, function(err, resultCta){
                                                                                                                    if(err){
                                                                                                                        console.log('ERROR socket.on(updateEstadoCuentaMonto) parte 0 tipo porcentaje ',err);
                                                                                                                    }else{

                                                                                                                        if(tipoPago != 1){
                                                                                                                            var saldoActual = montoRecarga + cta_monto;
                                                                                                                        }else{
                                                                                                                            var saldoActual = montoRecarga - cta_monto;
                                                                                                                        }

                                                                                                                        Conductors.updateRecargaMonto(idConductor,saldoActual,function(er){
                                                                                                                            if(er){
                                                                                                                                console.log('ERROR socket.on(updateRecargaMonto) parte 0 porcentaje',er);
                                                                                                                            }else{
                                                                                                                                console.log('OK socket.on(updateRecargaMonto) parte 0 porcentaje',er);

                                                                                                                                //Envio de Correo a Cliente 
                                                            request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+idServicio+'&tipo=2', function (error, response, body) {
                                                                console.log('Se ejecuto el correo de detalle'+response);
                                                                if (!error && response.statusCode == 200) {
                                                                    // Show the HTML for the Google homepage. 
                                                                    //res.send('Mensaje enviado');
                                                                    console.log('Mensaje enviado detalle');
                                                                }else {
                                                                    console.log("Error "+response.statusCode)
                                                                    console.log('Mensaje NO enviado detalle');
                                                                    //res.send('Mensaje NO enviado');
                                                                }
                                                            });
                                                            //End 


                                                            // console.log(result[0].cliente_socket, '33333333');
                                                            io.to(result[0].cliente_socket).emit('FinalizarServicioCliente',{
                                                                FechaFin:fechaFin,
                                                                Paradas:parseFloat(docs.Paradas),
                                                                Parqueos:parseFloat(docs.Parqueos),
                                                                Peajes:parseFloat(docs.Peajes),
                                                                Espera:parseFloat(docs.Espera),
                                                                Tarifa:TarifaFinal,
                                                                TipoPago:tipoPago
                                                            });

                                                            send = {Servicio :idServicio,tarifa: TarifaFinal};
                                                            callback(send);
                                                            console.log('OK socket.on(updateTarifaNewKilometraje) parte 0 tipo tarifa X Kilometraje 333',er);

                                                                                                                            }   
                                                                                                                        });
                                                                                                                    }
                                                                                                                });        

                                                                                                            }else if(tipoComision == 2){// X monto

                                                                                                                if(tipoPago != 1){
                                                                                                                    montoComision = parseFloat(validtarifa - montoComision)+parseInt(Peajes)+parseInt(Parqueos);
                                                                                                                }

                                                                                                                //var dataCta = {idConductor:idConductor,idServicio:idServicio,Ctatipo:2,Ctafecha:fechaCta,Ctamonto:montoComision,CtaCheck:1};
                                                                                                                Services.updateEstadoCuentaMonto(montoComision,cta_id, function(err, resultCta){
                                                                                                                    if(err){
                                                                                                                        console.log('ERROR socket.on(insertCta) parte 0 tipo monto ',err);
                                                                                                                    }else{
                                                                                                                        if(tipoPago != 1){
                                                                                                                            var saldoActual = montoRecarga + montoComision;
                                                                                                                        }else{
                                                                                                                            var saldoActual = montoRecarga - montoComision;
                                                                                                                        }

                                                                                                                        Conductors.updateRecargaMonto(idConductor,saldoActual,function(er){
                                                                                                                            if(er){
                                                                                                                                console.log('ERROR socket.on(updateRecargaMonto) parte 0 monto',er);
                                                                                                                            }else{
                                                                                                                                console.log('OK socket.on(updateRecargaMonto) parte 0 monto',er);

                                                                                                                                //Envio de Correo a Cliente 
                                                            request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+idServicio+'&tipo=2', function (error, response, body) {
                                                                console.log('Se ejecuto el correo de detalle'+response);
                                                                if (!error && response.statusCode == 200) {
                                                                    // Show the HTML for the Google homepage. 
                                                                    //res.send('Mensaje enviado');
                                                                    console.log('Mensaje enviado detalle');
                                                                }else {
                                                                    console.log("Error "+response.statusCode)
                                                                    console.log('Mensaje NO enviado detalle');
                                                                    //res.send('Mensaje NO enviado');
                                                                }
                                                            });
                                                            //End 


                                                            // console.log(result[0].cliente_socket, '4444444');
                                                            io.to(result[0].cliente_socket).emit('FinalizarServicioCliente',{
                                                                FechaFin:fechaFin,
                                                                Paradas:parseFloat(docs.Paradas),
                                                                Parqueos:parseFloat(docs.Parqueos),
                                                                Peajes:parseFloat(docs.Peajes),
                                                                Espera:parseFloat(docs.Espera),
                                                                Tarifa:TarifaFinal,
                                                                TipoPago:tipoPago
                                                            });

                                                            send = {Servicio :idServicio,tarifa: TarifaFinal};
                                                            callback(send);
                                                            console.log('OK socket.on(updateTarifaNewKilometraje) parte 0 tipo tarifa X Kilometraje 444',er);
                                                                                                                            }   
                                                                                                                        });
                                                                                                                    }
                                                                                                                });  
                                                                                                            }
                                                                                                        }else{
                                                                                                             console.log('ERROR socket.on(validarConductor 2) comision 0 ',error);

                                                                                                                request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+idServicio+'&tipo=2', function (error, response, body) {
                                                                                                                    console.log('Se ejecuto el correo de detalle'+response);
                                                                                                                    if (!error && response.statusCode == 200) {
                                                                                                                        // Show the HTML for the Google homepage. 
                                                                                                                        //res.send('Mensaje enviado');
                                                                                                                        // console.log('Mensaje enviado detalle');
                                                                                                                    }else {
                                                                                                                        console.log("Error "+response.statusCode)
                                                                                                                        // console.log('Mensaje NO enviado detalle');
                                                                                                                        //res.send('Mensaje NO enviado');
                                                                                                                    }
                                                                                                                });
                                                                                                                //End 


                                                                                                                // console.log(result[0].cliente_socket, 'comision 0');
                                                                                                                io.to(result[0].cliente_socket).emit('FinalizarServicioCliente',{
                                                                                                                    FechaFin:fechaFin,
                                                                                                                    Paradas:parseFloat(docs.Paradas),
                                                                                                                    Parqueos:parseFloat(docs.Parqueos),
                                                                                                                    Peajes:parseFloat(docs.Peajes),
                                                                                                                    Espera:parseFloat(docs.Espera),
                                                                                                                    Tarifa:TarifaFinal,
                                                                                                                    TipoPago:tipoPago
                                                                                                                });

                                                                                                                send = {Servicio :idServicio,tarifa: TarifaFinal};
                                                                                                                callback(send);

                                                                                                        }
                                                                                                    }
                                                                                                });
                                                                                        });
                                                                                        
                                                                                        
                                                                                        console.log('ERROR socket.on(validarServicioId) parte 0 existe',error);
                                                                                    }
                                                                                }
                                                                            });
                                                                    }else{
                                                                        console.log('ERROR socket.on(findOneQueryCliente) FinalizarServicio',errorCliente);
                                                                    }
                                                                });
                                                            }); 
                                                    // FINAL DE COMICION
                                                    //Envio de Correo a Cliente 
                                                    /*request(global.CONFIG.URL_PROJECT+'mailer.php?cliente_id='+idServicio+'&tipo=2', function (error, response, body) {
                                                        console.log('Se ejecuto el correo de detalle'+response);
                                                        if (!error && response.statusCode == 200) {
                                                            // Show the HTML for the Google homepage. 
                                                            //res.send('Mensaje enviado');
                                                            console.log('Mensaje enviado detalle');
                                                        }else {
                                                            console.log("Error "+response.statusCode)
                                                            console.log('Mensaje NO enviado detalle');
                                                            //res.send('Mensaje NO enviado');
                                                        }
                                                    });
                                                    //End 


                                                    console.log(result[0].cliente_socket, '55555555');
                                                    io.to(result[0].cliente_socket).emit('FinalizarServicioCliente',{
                                                        FechaFin:fechaFin,
                                                        Paradas:parseFloat(docs.Paradas),
                                                        Parqueos:parseFloat(docs.Parqueos),
                                                        Peajes:parseFloat(docs.Peajes),
                                                        Espera:parseFloat(docs.Espera),
                                                        Tarifa:TarifaFinal,
                                                        TipoPago:tipoPago
                                                    });

                                                    send = {Servicio :idServicio,tarifa: TarifaFinal};
                                                    callback(send);
                                                    console.log('OK socket.on(updateTarifaNewKilometraje) parte 0 tipo tarifa X Kilometraje',er);*/

                                                }else{
                                                    console.log('ERROR socket.on(updateTarifaNewKilometraje) parte 0 tipo tarifa X Kilometraje',er);
                                                }

                                            });
                                        }else{

                                            console.log('si HAY ERROR EN ERRORC');
                                            console.log('ERROR socket.on(ConductoralertaTaxi) parte 0',err);
                                        }
                                    });
                                }
                            });
                        
                    }else{

                        //   console.log('********[SERVER] si hay ERROR');


                        console.log('OK socket.on(ListarConfig) parte 0 tipo tarifa X Zonas',err);
                        Conductors.ConductoralertaTaxi(idConductor,function(errorC,resultConductor){
                            if(!errorC){

                                var tarifatipo_servicio = parseFloat(resultConductor[0].tiposervicio_factor/100);
                                var tarifanuevaUltima = parseFloat(1+tarifatipo_servicio)*Tarifa;

                                 io.to(result[0].cliente_socket).emit('FinalizarServicioCliente',{
                                    FechaFin:fechaFin,
                                    Paradas:parseFloat(docs.Paradas).toFixed(2),
                                    Parqueos:parseFloat(docs.Parqueos).toFixed(2),
                                    Peajes:parseFloat(docs.Peajes).toFixed(2),
                                    Espera:parseFloat(docs.Espera).toFixed(2),
                                    Tarifa:parseFloat(tarifanuevaUltima).toFixed(2)
                                });
                                var send2 = {tarifa :Tarifa};
                                callback(send2);
                            }
                        });
                    }
                }else{

                    //   console.log('********[SERVER] si sisisi hay ERROR');
                    console.log('ERROR socket.on(ListarConfig) parte 0',err);
                }
            });
        }
    });

                                                                 console.log('OK socket.on(updateClienteCalificacion) parte 0',err);
                                                            }
                                                        });
                                                       
                                                    }else{
                                                        console.log('ERROR socket.on(updateConductorVista) parte 1',err);
                                                    }
                                                });
                                            }
                                        });
                                    }else{
                                        console.log('ERROR socket.on(updateClienteVista) parte 2',err);
                                    }
                                });
                            }else{
                               console.log('ERROR socket.on(CustomServicio) part 3', err); 
                            }
                        });
                    }else{
                        console.log('ERROR socket.on(updateServicioEstadoAll) part 4', err);
                    }
                });
            }
        });
    });

    //LISTA LAS UNIDADES Y CONDUCTORES QUE TENGAN EL ESTADO all , filter con tipo de servicio 
    socket.on('dataMapConductorsWeb', function(docs){

        if(docs.estado == 'all'){
            Conductors.findOneQueryMapConductors(docs.estado , function(err, result){
                if(result){
                        io.sockets.emit('listaConductors',result);
                }
            });
        }else{
            Conductors.findOneQueryMapConductorsTipoServicio(docs.estado , function(err, result){
                if(result){
                        io.sockets.emit('listaConductors',result);
                }
            });
        }
        
    });

    //LISTA TODAS LAS UNIDADES Y SUS CONDUCTORES
    socket.on('dataMapConductorsWeball', function(docs){
        if(docs.estado == 'all'){
            // console.log('Estado todos :'+docs.estado);
            Conductors.findOneQueryMapConductorsAll(docs.estado , function(err, result){
                if(result){
                   // console.log(result);
                        socket.emit('listaConductorsall',result);
                }
            });
        }
        else
        {   
            // console.log('Estado filtro :'+docs.estado);
            Conductors.findOneQueryMapConductorsEstados(docs.estado , function(err, result){
                if(result){
                 //   console.log(result);
                        socket.emit('listaConductorsall',result);
                }
            });
        }
        
    });

    //CONSULTAR TARIFA DESDE WEB
    socket.on('setServicioTarifaCliente', function(docs){

        var origen_id = docs.origen_id;
        var destino_id = docs.destino_id;
        var tiposervicio_id = docs.tiposervice;
        var empresa_id = docs.empresaid;

        Lugar.validarLugarId(origen_id,function(err,resultorigen){
            if(resultorigen[0].total == 1){
                Lugar.validarLugarId(destino_id,function(err,resultdestino){
                    if(resultdestino[0].total == 1){
                        Services.validarTipoServicioId(tiposervicio_id,function(err,resulservicio){
                            if(resulservicio[0].total == 1){
                                Lugar.SolicitarTarifa(empresa_id,origen_id,destino_id,tiposervicio_id,function(error,restdata){
                                    if(!error){
                                        io.sockets.emit('Tarifacalculada',restdata);
                                    }
                                });
                            }else{
                                console.log('ERROR socket.on(validarTipoServicioId) part 0', err);
                            }
                        });
                    }else{
                        console.log('ERROR socket.on(validarLugarId) part 1', err);
                    }
                });
            }else{
                console.log('ERROR socket.on(validarLugarId) part 2', err);
            }
        });

    });

    //Actualizacion socket de conductores al momento que inicia la aplicacion
    socket.on('setUpdateSocketConductor', function(docs){

        docs = JSON.parse(docs); 
        var conductor_id = docs.idConductor;
        var fechaActual = new Date();
        //var socketConductor = docs.socketConductor;
        var socketConductor = socket.id;
        // console.log('conductor_id',conductor_id);
        Conductors.validarConductor(conductor_id,function(error, resultado){
            if(!error){
                if(resultado[0].conductor_status == 1){
                        Conductors.updateQuerySocketConductor(conductor_id, socketConductor, function(err){
                            if(!err){
                                Conductors.validarConductor(conductor_id, function (err, da){
                                    if(da[0].conductor_status != 0){
                                        io.to(socketConductor).emit('respUpdateSocketConductor', 'true');
                                        Lugar.ListarApiKey(1,function(errorApi,resulApi){
                                            if(!errorApi){
                                                io.to(socketConductor).emit('SolicitudApis',{
                                                    google_android:resulApi[0].api_google_android,
                                                    google_ios:resulApi[0].api_google_ios,
                                                    foursquare_key:resulApi[0].api_foursquare_key,
                                                    foursquare_secret:resulApi[0].api_foursquare_secret,
                                                    here_key:resulApi[0].api_here_key,
                                                    here_secret:resulApi[0].api_here_secret
                                                });
                                                // console.log('Conexion correcta Conductor ListarApiKey ');
                                            }else{
                                                 console.log("UPS ERROR HERE ListarApiKey");
                                            }
                                        });
                                        console.log('Conexion correcta conductor '+conductor_id);
                                    }else{
                                        io.to(socketConductor).emit('respUpdateSocketConductor', 'false');
                                        console.log('Error de conexion del conductor '+conductor_id);
                                    }
                                }); 
                            }
                        });

                        Reservas.listaReservaConductorId(conductor_id,function(err){
                            if(!err){
                                var fechafinal = new Date();
                                fechafinal.setHours(fechaActual.getHours()-6);
                                var fi = fechafinal.toISOString().replace(/T/, ' ').replace(/\..+/, '')

                                var fecha = new Date();
                                fecha.setHours(fechaActual.getHours()-5);
                                var ff = fecha.toISOString().replace(/T/, ' ').replace(/\..+/, '')

                                Reservas.listaReservasProximas(fi,ff,conductor_id,function(error,data){
                                    if(!error){
                                        if(data.length > 0){
                                            var estado = 2;
                                            Reservas.UpdateReservas(estado,data[0].reserva_id,function(err){
                                                if(!err){
                                                    data.forEach(function (docsRows) {
                                                        var fechainicio = new Date(docsRows.reserva_fecha);
                                                        var fechafinal = new Date(docsRows.reserva_fecha);
                                                        fechafinal.setHours(fechainicio.getHours()-5);
                                                         data[0].reserva_fecha = fechafinal.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                                    });

                                                    io.to(socketConductor).emit('ReservaProxima',{
                                                        Reserva:data[0],
                                                        Tiempo:1
                                                    });
                                                }
                                            });
                                        }else{
                                           console.log('Ninguna Reserva 1'); 
                                        }
                                    }else{
                                        console.log('Ninguna Reserva 2');
                                    }
                                });
                            }
                        });
                }else{
                      io.to(socketConductor).emit('ConductorDesabilitado', 'true');
                }
            }else{
                console.log('Error al validar');
            }
        });
    });

    socket.on('ReservaIniciada',function(docs){


        // console.log('reserva Iniciada');
        //console.log(docs);
        docs = JSON.parse(docs);
        // console.log(docs);
        var reserva_id = docs.reserva_id;

        Reservas.listaReservaId(reserva_id,function(err,result){
            if(!err){
                // console.log(result[0]);
                var fs = new fechaActual();
                var fechaseleccion = fs['actual'];

                var OrigeRefere = (result[0].referencia_origen!=null)?result[0].referencia_origen:'';
                var DestiRefere = (result[0].referencia_destino!=null)?result[0].referencia_destino:'';
                var OrigeNombre = (result[0].origen!=null)?result[0].origen:'';
                var DestiNombre = (result[0].destino!=null)?result[0].destino:'';

                var dataInsertServicio = {
                        idCliente:result[0].cliente_id,
                        conductor_id:docs.conductor_id,
                        TipoServicion:result[0].tiposervicio_id,
                        Origen:result[0].origen_id,
                        Destino:result[0].destino_id,
                        OrigeNombre:OrigeNombre,
                        DestiNombre:DestiNombre,
                        OrigeDirecc:result[0].dir_origen,
                        DestiDirecc:result[0].dir_destino,
                        OrigeRefere:OrigeRefere,
                        DestiRefere:DestiRefere,
                        OrigenLat:result[0].lat_origen,
                        OrigenLng:result[0].lon_origen,
                        DestinoLat:result[0].lat_destino,
                        DestinoLng:result[0].lon_destino,
                        Tarifa:result[0].tarifa,
                        TipoPago:result[0].tipo_pago,
                        Estado:1,
                        fecha_seleccion:fechaseleccion,
                        flag_reserva:1
                    };

            //    console.log('[][]',dataInsertServicio);

                request.post({
                  url:     global.CONFIG.URL_PROJECT +'ws/cliente/ws_agregar_servicio.php',
                  form:    {data:dataInsertServicio}
                }, function(error, response, body){
                        // console.log(body);
                        var obj  = JSON.parse(body);
                        if(obj.status == true){
                            Clients.findOneQueryCliente(result[0].cliente_id,function(er,dataresult){
                                if(!er){
                                    // console.log('[****************SERVER]->',dataresult);
                                    //if(dataresult[0].cliente_token_android != null){
                                            Conductors.updateConductorEstadoOcupado(2,docs.conductor_id,function(err){
                                                if(!err){
                                                    console.log('OK socket.on(updateConductorEstadoOcupado) parte 0',err);
                                                }else{
                                                    console.log('ERROR socket.of(updateConductorEstadoOcupado) parte 1',err);
                                                }   
                                            });

                                        //if(dataresult[0].cliente_token_android.length > 0 ){
                                            //TOKEN ANDROID
                                            var estadoreserva = 3;
                                            Reservas.UpdateReservas(estadoreserva,docs.reserva_id,function(err){
                                                // console.log(dataresult[0].cliente_socket+" -> Socket cliente");

                                                if(err)
                                                    console.log("Error321", err)

                                                io.to(dataresult[0].cliente_socket).emit('InicioServicio', 'reserva');                                                     
                                                // console.log('se Envio emit cliente');

                                                request(global.CONFIG.URL_PROJECT+'ws/conductor/ws_notificacion_android.php?cliente_id='+result[0].cliente_id, function (error, response, body) {
                                                    if (!error && response.statusCode == 200) {
                                                        console.log('Notificacion enviado');
                                                    }else {
                                                        console.log("Error "+response.statusCode)
                                                        console.log('Notificacion NO enviado');
                                                    }
                                                });
                                                    //console.log('ERROR socket.on(UpdateReservas) parte 1 exito', err);

                                                    /*io.to(result[0].cliente_socket).emit('InicioServicio',{
                                                        FechaInicio:fechaseleccion
                                                    });*/

                                            });
                                        //}
                                        /*else{
                                            //TOKEN ANDROID
                                            var estadoreserva = 3;
                                            Reservas.UpdateReservas(estadoreserva,docs.reserva_id,function(err){
                                                if(!err){
                                                    request(global.CONFIG.URL_PROJECT+'ws/cliente/ws_notifiConductor_llego.php?cliente_id='+result[0].cliente_id+'&tipo=3',function(error,response,body){
                                                        if(error || response.statusCode != 200){
                                                            console.log('request reserva Iniciada');
                                                            console.log('Error Send Request : ', error);
                                                        }
                                                    });
                                                    console.log('ERROR socket.on(UpdateReservas) parte 1 exito', err);
                                                }else{
                                                     console.log('ERROR socket.on(UpdateReservas) parte 1', err);   
                                                }
                                            });
                                        }*/

                                    //}
                           
                                }else{
                                     console.log('ERROR socket.on(ReservaIniciada) parte 1', err); 
                                }
                            });
                        }
                });
                /*Conductors.insertServicioReserva(dataInsertServicio, function(err, resultServicio){
                    if(err){
                        console.log('ERROR socket.on(insertServicioReserva) parte 1', err);
                    }else{
                        var estadoreserva = 3;
                        Reservas.UpdateReservas(estadoreserva,docs.reserva_id,function(err){
                            if(!err){
                                 request(global.CONFIG.URL_PROJECT+'ws/conductor/ws_notificacion_android.php?cliente_id='+result[0].cliente_id, function (error, response, body) {
                                    if (!error && response.statusCode == 200) {
                                        console.log('Notificacion enviado');
                                    }else {
                                        console.log("Error "+response.statusCode)
                                        console.log('Notificacion NO enviado');
                                    }
                                });

                                console.log('ERROR socket.on(UpdateReservas) parte 1 exito', err);
                            }else{
                                 console.log('ERROR socket.on(UpdateReservas) parte 1', err);   
                            }
                        });
                    }
                    
                });*/ 
            }else{
                console.log('Error de datos de reserva '+reserva_id);
            }
        });
    });
    //Actualizacion del socket de clientes al momento que incia la aplicacion
    socket.on('setUpdateSocketCliente', function(docs){
               
        docs = JSON.parse(docs);
        
        var cliente_id = docs.idCliente;
        var socketCliente = docs.socketCliente;

        Clients.updateQuerySocketCliente(cliente_id,socketCliente, function(err){
            if(!err){
                io.to(socketCliente).emit('updateQuerySocketCliente', 'true');
                // console.log('Conexion correcta cliente '+cliente_id);
                Lugar.ListarApiKey(1,function(errorApi,resulApi){
                    if(!errorApi){
                        io.to(socketCliente).emit('SolicitudApis',{
                            google_android:resulApi[0].api_google_android,
                            google_ios:resulApi[0].api_google_ios,
                            foursquare_key:resulApi[0].api_foursquare_key,
                            foursquare_secret:resulApi[0].api_foursquare_secret,
                            here_key:resulApi[0].api_here_key,
                            here_secret:resulApi[0].api_here_secret
                        });
                        console.log('Conexion correcta Cliente ListarApiKey ');
                    }else{
                         console.log("UPS ERROR HERE ListarApiKey");
                    }
                });
            }else{
                 io.to(socketCliente).emit('updateQuerySocketCliente', 'false');
                 console.log('Error de conexion del cliente '+cliente_id);
            }
        });
    });

    //Actualiza la geolocalizacion de conductor para visualizar en el mapa la posicion del conductor.
    socket.on('setUpdateGeoConductor', function(docs){
            
            docs = JSON.parse(docs);   

        var conductor_id = parseFloat(docs.idConductor);
        var conductor_lat = parseFloat(docs.latitud);
        var conductor_lng = parseFloat(docs.longitud);

        Conductors.updateQueryCoordenations(conductor_id, conductor_lat, conductor_lng, function(err){
            Conductors.ConductoralertaTaxi(conductor_id, function(err, result){
                result.forEach( function (docsRows) {
                    io.sockets.emit('refreshCoordinates', {
                        idConductor: docsRows.conductor_id, 
                        latitud: docsRows.conductor_lat, 
                        longitud: docsRows.conductor_lng, 
                        socket_conductor:docsRows.conductor_socket, 
                        stateConductor: docsRows.conductor_estado,
                        nombre:docsRows.conductor_nombre,
                        apellido:docsRows.conductor_apellido,
                        foto:docsRows.conductor_foto,
                        celular:docsRows.conductor_celular,
                        placa:docsRows.unidad_placa,
                        marca:docsRows.unidad_marca,
                        servicio:docsRows.tiposervicio_nombre,
                        campo_new:docsRows.conductor_status
                    });
                });

                Conductors.ServicioConductor(conductor_id,function(error,resultado){
                    if(resultado.length != 0){
                        var cliente_id = resultado[0].cliente_id;
                        Clients.findOneQueryCliente(cliente_id,function(err,resul){
                            if(resul){
                                if(resul[0].cliente_socket != null){
                                    io.to(resul[0].cliente_socket).emit('RefreshCliente',{
                                                LatCondutor:conductor_lat,
                                                LngConductor:conductor_lng
                                          });
                                    // console.log('Envio RefreshCliente');
                                }else{
                                    console.log('Socket del cliente es null ', err);
                                }
                            }else{
                                console.log('ERROR socket.on(findOneQueryCliente) parte 0',err);
                            }
                        });
                    }else{
                       // console.log('ERROR socket.on(ServicioConductor) parte 1',err);
                    }
                });
            }); 
        });
    });

    socket.on('nuevaSolicitudConductorWeb', function(docs){
        // console.log('Recepcionar desde panel los servicios')
      //  console.log(docs);
       io.to(docs.socketConductor).emit('nuevaSolicitudConductor',{
                            IdCliente:docs.IdCliente, 
                            latOrig:docs.latOrig,
                            latDest:docs.latDest,
                            lngOrig:docs.lngOrig, 
                            lngDest:docs.lngDest,
                            Origen:docs.Origen,
                            Destino:docs.Destino,
                            dirOrigen:docs.refOrigen,
                            dirDestino:docs.refDestino,
                            refOrigen:null,
                            refDestino:null,
                            tipoServicio:docs.tipoServicio,
                            tarifa:docs.tarifa, 
                            socketCliente:docs.socketCliente,
                            ServicioId:docs.ServicioId,
                            OrigenNombre:docs.dirOrigen,
                            DestinoNombre:docs.dirDestino,
                            Envio:1,
                            imagen: '',
                            comentario: ''
                       });
    //    console.log('Envio de servicio al Conductor');
    });

    socket.on('nuevaAlertaWeb', function(docs){
        docs = JSON.parse(docs);
        // console.log(docs);
        request.post({
            url: global.CONFIG.URL_PROJECT+'ws/panel/ws_alerta_panel.php',
            form: {tipo:docs.tipo,codigo:docs.codigo,latitud:docs.latitud,longitud:docs.longitud,flag:docs.flag}
        }, function(error, response, body){
            // console.log(body);
            var obj  = JSON.parse(body);
            if(obj.status == true){
                io.sockets.emit('alertaEspera', {tipo:docs.tipo,send:obj});
            }else{
                 console.log('ERROR socket.on(nuevaAlertaWeb) /WS/PANEL/WS_ALERTA_PANEL');
            }
        });
        //io.sockets.emit('alertaEspera', {tipo:docs.tipo,codigo:docs.codigo,latitud:docs.latitud,longitud:docs.longitud,flag:docs.flag});
    });
    
    socket.on('SolicitudAceptarReservaMovil',function(docs){

        // console.log('reserva responde');
        docs = JSON.parse(docs);
        // console.log(docs);
        
        var flag = (docs.aceptado == 1)?1:0;

        if(docs.tipo == 3){

            Reservas.UpdateReservas(flag,docs.reserva_id,function(errActReserva){
                if(!errActReserva){
                    console.log('Ok socket.on(UpdateReservas) SolicitudAceptarReservaMovil');
                }else{
                     console.log('ERROR socket.on(UpdateReservas) SolicitudAceptarReservaMovil');
                }
            });

            Clients.findOneQueryCliente(docs.cliente_id,function(error,repta){
                if(!error){
                    console.log('socket cliente reserva ',repta[0].cliente_socket);
                    io.to(repta[0].cliente_socket).emit('ConductorAsignadoaReserva','true'); 
                    //io.to(repta[0].cliente_socket).emit('ConductorAsignadoaReserva','true'); 
                }
            });

            io.sockets.emit('SolicitudAceptarReserva',{tipo:'movil'});

        }else{
            if(flag == 1){

                Clients.findOneQueryCliente(docs.cliente_id,function(error,repta){
                    if(!error){
                        console.log('socket cliente reserva ',repta[0].cliente_socket);
                        io.to(repta[0].cliente_socket).emit('ConductorAsignadoaReserva','true'); 
                        //io.to(repta[0].cliente_socket).emit('ConductorAsignadoaReserva','true'); 
                    }
                });

                io.sockets.emit('SolicitudAceptarReserva',{tipo:'movil'});

            }else{
                console.log('Ok socket.on(SolicitudAceptarReservaMovil) tipo ', docs.aceptado ); 
            }
        }

        console.log('respondio Javier Quiroz Reserva111');
    });

    socket.on('CancelarService', function(docs){
        // console.log('cancelar');
        docs = JSON.parse(docs);
        //console.log(docs)
        var servicio_id = docs.servicio_id;
        var conductor_id = docs.conductor_id;
        var EstadoServicio = 10;
        var usuario = docs.usuario; // 1 = Ios , 2 = Android
        var tipo = docs.tipo; // 1 Cliente , 2 = Conductor
        var f = new fechaActual();
        var fecha_cancelar = f['actual'];

        Clients.CustomServicio(servicio_id,function(err,resultCliente){
            if(!err){
                var tipocliente = resultCliente[0].tipocliente_id;
                if(tipocliente != 1){
                    Services.montoCancelacion(1,function(err,resultConfig){
                        if(!err){
                            var montoCancelacion = parseFloat(resultConfig[0].config_costo_co);
                            // console.log('monto'+ montoCancelacion);
                            Services.updateMontoCancelacionTarifa(servicio_id,montoCancelacion,function(error){
                                if(!error){
                                    console.log('OK socket.on(updateMontoCancelacionTarifa) parte 0',err);
                                }else{
                                    console.log('ERROR socket.of(updateMontoCancelacionTarifa) parte 1',err);
                                }
                            });
                        }
                    });
                }else{
                    console.log('ERROR socket.of(CustomServicio) Cliente Particular',err);
                }
            }else{
                console.log('ERROR socket.of(CustomServicio) CancelarService',err);
            }
        });

        Conductors.updateServicioEstadoAll(servicio_id,conductor_id,EstadoServicio,fecha_cancelar,function(err){
            if(!err){
                Conductors.validarConductor(conductor_id,function(error,result){
                    if(!error){
                        // console.log(result[0].conductor_socket);

                        Clients.CustomServicio(servicio_id,function(errCliente,resultCliente){
                            
                            if(!errCliente){
                                // console.log('cancelar servicio conductor', result[0].conductor_socket);
                                io.to(result[0].conductor_socket).emit('ServicioCancelado',{
                                    ServicioId:servicio_id
                                });
                                // console.log('cancelar servicio cliente', resultCliente[0].cliente_socket);
                                io.to(resultCliente[0].cliente_socket).emit('ServicioCancelado',{
                                    ServicioId:servicio_id
                                });

                                if(usuario == 2){
                                    //Notificaciones para el conductor android
                                    if(tipo == 2){

                                        // console.log(resultCliente[0].cliente_id, 'cliente id');
                                        // console.log(resultCliente[0].cliente_token_android, 'android');
                                        // console.log(resultCliente[0].cliente_token_ios,'ios')

                                        if(resultCliente[0].cliente_token_android != null){ 
                                            // console.log('android 1');                              
                                            request(global.CONFIG.URL_PROJECT+'ws/conductor/ws_notificacion_android.php?cliente_id='+resultCliente[0].cliente_id, function (error, response, body) {
                                                    // console.log(response.statusCode);
                                                if (!error && response.statusCode == 200) {
                                                    console.log('Notificacion Cancelar servicio enviado');
                                                }else {
                                                    console.log("Error "+response.statusCode)
                                                    console.log('Notificacion Cancelar servicio no enviado');
                                                }
                                            });
                                        }else{
                                            console.log('ios 1'); 
                                            request(global.CONFIG.URL_PROJECT+'ws/cliente/ws_notifiConductor_llego.php?cliente_id='+resultCliente[0].cliente_id+'&tipo=10',function(error,response,body){
                                                if(error || response.statusCode != 200){
                                                    console.log('request LLegadaConductor');
                                                    console.log('Error Send Request : ', error);
                                                }
                                            });  
                                        }
                                    }
                                }
                            }

                        });
                        /*io.to(result[0].conductor_socket).emit('ServicioCancelado',{
                            ServicioId:servicio_id
                        });*/

                        Conductors.updateConductorEstadoOcupado(1,conductor_id,function(err){
                            if(!err){
                                console.log('OK socket.on(updateConductorEstadoOcupado) parte 0',err);
                            }else{
                                console.log('ERROR socket.of(updateConductorEstadoOcupado) parte 1',err);
                            }   
                        });

                        Conductors.updateCancelarTipoUsuario(tipo,servicio_id,function(err){
                             if(!err){
                                console.log('OK socket.on(updateCancelarTipoUsuario) parte 0',err);
                            }else{
                                console.log('ERROR socket.of(updateCancelarTipoUsuario) parte 1',err);
                            }   
                        })

                    }
                });
            }
        });
    });

});


