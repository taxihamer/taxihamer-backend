
//var mongoose = require('mongoose');

/*
var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'T4x1H4mm3r',
    database : 'taxihammer',
    port : 3306
});
*/

var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'homestead',
    password : 'secret',
    database : 'taxihammer',
    port : 3306
});


conexMysql.connect(function(err){
    if(err){ console.log('Error ->', err); }
});

module.exports.store = (unidad_id, nombres, apellidos, correo, celular, password, cci, dni, banco, foto_perfil, foto_licencia, foto_soat, callback )=>{
conexMysql.query('INSERT INTO tb_conductor (unidad_id, conductor_nombre, conductor_apellido, conductor_correo, conductor_password, conductor_dni, conductor_celular, conductor_status, conductor_calificacion, conductor_cci, banco_id , conductor_foto, conductor_fotosoat, conductor_foto_licencia) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ?)',[ unidad_id, nombres, apellidos, correo, password, dni, celular, 0, 5, cci, 2, foto_perfil , foto_soat, foto_licencia], callback);
}

module.exports.updateConductorEstadoOcupado = function(conductor_estado,idconductor, callback){

    conexMysql.query('UPDATE tb_conductor SET conductor_estado = ? WHERE conductor_id = ? ', [conductor_estado,idconductor], callback);
}

module.exports.updateConductorPtos = function(idconductor,ptosConductor, callback){

    conexMysql.query('UPDATE tb_conductor SET conductor_puntos = ? WHERE conductor_id = ? ', [ptosConductor,idconductor], callback);
}

module.exports.updateServicioExpress = function(idServicio,idConductor,flagexpreso,DestinoId,DestinoDireccion,DestinoLat,DestinoLng,Paradas,Parqueos,Peajes,Espera,Tarifa,callback){

    if(flagexpreso == 1){
        datactualizar = ' destino_id = ? , destino_direccion = ? , destino_lat = ? , destino_lon = ? , peaje = ? , parqueo = ? , espera = ? , parada = ? , tarifa = ? ';
        conexMysql.query('UPDATE tb_servicio SET '+datactualizar+' WHERE servicio_id = ? AND conductor_id = ? ', [DestinoId,DestinoDireccion,DestinoLat,DestinoLng,Peajes,Parqueos,Espera,Paradas,Tarifa,idServicio,idConductor], callback);

    }else{
        datactualizar = ' peaje = ? , parqueo = ? , espera = ? , parada = ? , tarifa = ? , destino_direccion = ?, destino_lat = ? , destino_lon = ? ';
        conexMysql.query('UPDATE tb_servicio SET '+datactualizar+' WHERE servicio_id = ? AND conductor_id = ? ', [Peajes,Parqueos,Espera,Paradas,Tarifa,DestinoDireccion,DestinoLat,DestinoLng,idServicio,idConductor], callback);
    }

    
}

module.exports.updateConductorVista = function(idconductor,EstadoServicio,contviaje, callback){

    conexMysql.query('UPDATE tb_conductor SET conductor_vista = ? , conductor_viajes = ? WHERE conductor_id = ? ', [EstadoServicio,contviaje,idconductor], callback);
}

module.exports.updateServicioEstadoAll = function(idservicio,idconductor,EstadoServicio,fecha, callback){

    if(EstadoServicio == 2){
        fechactualizar = ' fecha_recojo = ? ';
    }else if(EstadoServicio == 3){
        fechactualizar = ' hora_inicio = ? ';
    }else if(EstadoServicio == 6){
        fechactualizar = ' hora_fin = ? ';
    }else if(EstadoServicio == 10){
        fechactualizar = ' fecha_cancelar = ? ';
    }

    conexMysql.query('UPDATE tb_servicio SET estado = ? , '+fechactualizar+' WHERE servicio_id = ? AND conductor_id = ? ', [EstadoServicio,fecha,idservicio,idconductor], callback);
}

module.exports.findOneQueryServicio = function(idServicio,callback){
    conexMysql.query('SELECT * FROM tb_servicio WHERE servicio_id = ? ', [idServicio], callback);
}

module.exports.updateServicioEstado = function(idservicio,idconductor,EstadoServicio,fechaseleccion, callback){
    
    conexMysql.query('UPDATE tb_servicio SET estado = ? , fecha_seleccion = ? , conductor_id = ? WHERE servicio_id = ? ', [EstadoServicio,fechaseleccion,idconductor,idservicio], callback);
}

module.exports.ServicioConductor = function(idconductor,callback){
    conexMysql.query('SELECT servicio_id,cliente_id FROM tb_servicio WHERE conductor_id = ? AND estado NOT IN (0,6,10) ORDER BY servicio_id DESC ', [idconductor],callback);
}

module.exports.validarServicioConductor = function(conductor_id,callback){
    conexMysql.query('SELECT count(*) as total FROM tb_servicio WHERE conductor_id = ? and estado not in (0,6,10)', [conductor_id], callback);
}

module.exports.updateQueryCoordenations = function(conductor_id,conductor_lat, conductor_lng, callback){
    conexMysql.query('UPDATE tb_conductor SET conductor_lat = ? , conductor_lng = ? WHERE conductor_id = ? ', [conductor_lat, conductor_lng, conductor_id], callback);
}

module.exports.insertServicio = function(dataServicio, callback){
    conexMysql.query('INSERT INTO tb_servicio (cliente_id, tiposervicio_id, origen_id, origen_nombre, origen_direccion, origen_referencia, origen_lat, origen_lon, destino_id, destino_nombre, destino_direccion, destino_referencia, destino_lat, destino_lon, tipo_pago, tarifa, estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [ dataServicio.idCliente, dataServicio.TipoServicion, dataServicio.Origen, dataServicio.OrigeNombre, dataServicio.OrigeDirecc, dataServicio.OrigeRefere, dataServicio.OrigenLat, dataServicio.OrigenLng, dataServicio.Destino, dataServicio.DestiNombre, dataServicio.DestiDirecc, dataServicio.DestiRefere, dataServicio.DestinoLat, dataServicio.DestinoLng, dataServicio.TipoPago, dataServicio.Tarifa, dataServicio.Estado ] , callback);
}

module.exports.insertServicioReserva = function(dataServicio, callback){
    conexMysql.query('INSERT INTO tb_servicio (cliente_id,conductor_id, tiposervicio_id, origen_id, origen_nombre, origen_direccion, origen_referencia, origen_lat, origen_lon, destino_id, destino_nombre, destino_direccion, destino_referencia, destino_lat, destino_lon, tipo_pago, tarifa, estado,fecha_seleccion) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [ dataServicio.idCliente,dataServicio.idConductor, dataServicio.TipoServicion, dataServicio.Origen, dataServicio.OrigeNombre, dataServicio.OrigeDirecc, dataServicio.OrigeRefere, dataServicio.OrigenLat, dataServicio.OrigenLng, dataServicio.Destino, dataServicio.DestiNombre, dataServicio.DestiDirecc, dataServicio.DestiRefere, dataServicio.DestinoLat, dataServicio.DestinoLng, dataServicio.TipoPago, dataServicio.Tarifa, dataServicio.Estado,dataServicio.fecha_seleccion ] , callback);
}

module.exports.findOneQueryMapConductorsAll = function(estado, callback){
    conexMysql.query('SELECT * FROM tb_conductor c INNER JOIN tb_unidad u ON c.unidad_id = u.unidad_id INNER JOIN tb_tiposervicio t on u.tiposervicio_id = t.tiposervicio_id where c.conductor_status = 1', callback);
}

module.exports.findOneQueryMapConductorsEstados = function(estado, callback){

    var and = '';
    if(estado == 0){
        and = 'AND c.conductor_socket IS NULL';
        estado = 1;
    }else{
        and = 'AND c.conductor_socket IS NOT NULL';
    }

    conexMysql.query('SELECT * FROM tb_conductor c INNER JOIN tb_unidad u ON c.unidad_id = u.unidad_id INNER JOIN tb_tiposervicio t on u.tiposervicio_id = t.tiposervicio_id where c.conductor_status = 1 '+ and +' AND c.conductor_estado = ?', [estado], callback);

}

module.exports.findOneQueryMapConductors = function(estado, callback){

    conexMysql.query('SELECT * FROM tb_conductor c INNER JOIN tb_unidad u ON c.unidad_id = u.unidad_id INNER JOIN tb_tiposervicio t on u.tiposervicio_id = t.tiposervicio_id where c.conductor_socket IS NOT NULL AND c.conductor_estado = 1 AND c.conductor_status = 1', callback);
}

module.exports.findOneQueryMapConductorsTipoServicio = function(estado, callback){

    conexMysql.query('SELECT * FROM tb_conductor c INNER JOIN tb_unidad u ON c.unidad_id = u.unidad_id INNER JOIN tb_tiposervicio t on u.tiposervicio_id = t.tiposervicio_id where c.conductor_socket IS NOT NULL AND c.conductor_estado = 1 AND c.conductor_status = 1 AND t.tiposervicio_id = ? ',[estado], callback);
}

module.exports.findOneQueryConductorEstado = function(Conductor_id, Conductor_status,callback){
    conexMysql.query('SELECT * FROM tb_conductor WHERE conductor_id = ? AND conductor_status = ?', [Conductor_id,Conductor_status], callback);
}

module.exports.validarConductor = function(Conductor_id, callback){
    conexMysql.query('SELECT * FROM tb_conductor WHERE conductor_id = ?', [Conductor_id], callback);
}

module.exports.updateQuerySocket = function(socket_id, callback){
    conexMysql.query('UPDATE tb_conductor SET conductor_socket = null WHERE conductor_socket = ? ', [socket_id], callback);
}

module.exports.updateQuerySocketConductor = function(conductor_id,conductor_socket, callback){
    conexMysql.query('UPDATE tb_conductor SET conductor_socket = ?, conductor_estado = 1 WHERE conductor_id = ? ', [conductor_socket, conductor_id], callback);
}

module.exports.findOneQuery = function(socket, callback){
    conexMysql.query('SELECT * FROM tb_conductor WHERE conductor_socket = ? ', [socket], callback);
}

module.exports.ConductoralertaTaxi = function(Conductor_id, callback){
    conexMysql.query('SELECT * FROM tb_conductor c INNER JOIN tb_unidad u ON c.unidad_id = u.unidad_id INNER JOIN tb_tiposervicio t on u.tiposervicio_id = t.tiposervicio_id WHERE conductor_id = ? ', [Conductor_id], callback);
}

module.exports.searchConductradio = function(latitud, longitud, min_lat, max_lat, min_lng, max_lng, radio, tipo, maximo,  callback){
    conexMysql.query('SELECT u.unidad_id, c.conductor_id, c.conductor_nombre, c.conductor_socket, (6367 * ACOS(SIN(RADIANS(c.conductor_lat)) * SIN(RADIANS(?)) + COS(RADIANS(c.conductor_lng - ?)) * COS(RADIANS(c.conductor_lat)) * COS(RADIANS(?)))) AS radio FROM tb_conductor c INNER JOIN tb_unidad u on c.unidad_id = u.unidad_id INNER JOIN tb_tiposervicio s ON u.tiposervicio_id = s.tiposervicio_id WHERE (c.conductor_lat BETWEEN ? AND ? ) AND (c.conductor_lng BETWEEN ? AND ? ) AND s.tiposervicio_id BETWEEN ? AND ? AND c.conductor_estado = 1 AND c.conductor_recarga > 0 AND c.conductor_socket IS NOT NULL HAVING radio < ? ORDER BY radio ASC', [latitud, longitud, latitud, min_lat, max_lat, min_lng, max_lng, tipo, maximo, radio], callback);
}

module.exports.updateRecargaMonto = function(conductor_id,conductor_recarga, callback){
    conexMysql.query('UPDATE tb_conductor SET conductor_recarga = ? WHERE conductor_id = ? ', [conductor_recarga,conductor_id], callback);
}

module.exports.MaximoTipodeServicio = function(estado,callback){
    conexMysql.query('SELECT max(tiposervicio_id) as maximo FROM tb_tiposervicio WHERE tiposervicio_status = ? ',[estado] , callback);
}
module.exports.updateCancelarTipoUsuario = function(idservicio,tipo, callback){

    conexMysql.query('UPDATE tb_servicio SET tipocancelado = ? WHERE servicio_id = ?', [tipo,idservicio], callback);
}

