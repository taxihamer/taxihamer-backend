/*
var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'T4x1H4mm3r',
    database : 'taxihammer',
    port : 3306
});
*/

var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'homestead',
    password : 'secret',
    database : 'taxihammer',
    port : 3306
});


conexMysql.connect(function(err){
    if(err){ console.log('Error ->', err); }
});

module.exports.validarLugarId = function(lugar_id,callback){
    conexMysql.query('SELECT count(*) as total FROM tb_lugares WHERE lugares_id = ? and lugar_estado = 1', [lugar_id], callback);
}

module.exports.SolicitarTarifa = function(empresa_id,origen_id,destino_id,tiposervicio_id,callback){
	conexMysql.query('SELECT * FROM tb_tarifario  WHERE cliente_id = ? AND origen_id = ? AND destino_id = ? AND tiposervicio_id = ?', [empresa_id,origen_id,destino_id,tiposervicio_id], callback);
}

module.exports.ListarConfig = function(config_id,callback){
	conexMysql.query('SELECT * FROM tb_configuracion WHERE config_id = ?', [config_id], callback);
}

module.exports.RecargaFindquery = function(recarga_id,conductor_id,callback){
    conexMysql.query('SELECT * FROM tb_recarga WHERE recarga_id = ? AND conductor_id = ?', [recarga_id,conductor_id], callback);
}

module.exports.ListarConfigReserva = function(configreserva_status,callback){
	conexMysql.query('SELECT * FROM tb_config_reserva WHERE configreserva_status = ?', [configreserva_status], callback);
}

module.exports.ListarApiKey = function(api_status,callback){
    conexMysql.query('SELECT * FROM tb_api_key WHERE api_estado = ?', [api_status], callback);
}
