/*
var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'T4x1H4mm3r',
    database : 'taxihammer',
    port : 3306
});
*/

var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'homestead',
    password : 'secret',
    database : 'taxihammer',
    port : 3306
});



conexMysql.connect(function(err){
    if(err){ console.log('Error ->', err); }
});

module.exports.listaReservaConductorId = function(conductor_id,callback){
    conexMysql.query('SELECT * FROM tb_reserva WHERE conductor_id = ? and reserva_estado = ? and estado = 1', [conductor_id], callback);
}

module.exports.listaReservaId = function(reserva_id,callback){
    conexMysql.query('SELECT * FROM tb_reserva WHERE reserva_id = ?', [reserva_id], callback);
}

module.exports.listaReservasProximas = function(fecha_inicio,fecha_final,conductor_id,callback){
    conexMysql.query('SELECT * FROM tb_reserva where reserva_fecha between ? and ? and reserva_estado = 1 and conductor_id = ? order by reserva_fecha asc limit 1', [fecha_inicio,fecha_final,conductor_id], callback);
}

module.exports.UpdateReservas = function(reserva_estado,reserva_id,callback){
	conexMysql.query('UPDATE tb_reserva SET reserva_estado = ? WHERE reserva_id = ? ', [reserva_estado, reserva_id], callback);
}

module.exports.ReservasAproximadas = function(conductor_id, callback){
    conexMysql.query('SELECT * , DATE_FORMAT(reserva_fecha, "%H:%i:%s") as horaReserva , now() AS FechaSistema , DATE_FORMAT(now(), "%H:%i:%s") AS horaSistema,TIMESTAMPDIFF(MINUTE ,reserva_fecha,now()) AS diferencia FROM tb_reserva WHERE DATE_FORMAT(reserva_fecha, "%H:%i:%s") BETWEEN  DATE_FORMAT(now(), "%H:%i:%s") AND DATE_FORMAT(reserva_fecha, "%H:%i:%s") AND conductor_id = ? and date(reserva_fecha) = curdate() and reserva_estado = 1 and estado = 1 order by DATE_FORMAT(reserva_fecha, "%H:%i:%s") desc',[conductor_id], callback);
}

module.exports.ConfigReservaId = function(estado,callback){
    conexMysql.query('SELECT * FROM tb_config_reserva WHERE configreserva_status = ?', [estado], callback);
}
