//var mongoose = require('mongoose');
/*
var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'T4x1H4mm3r',
    database : 'taxihammer',
    port : 3306
});
*/

var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'homestead',
    password : 'secret',
    database : 'taxihammer',
    port : 3306
});


conexMysql.connect(function(err){
    if(err){ console.log('Error ->', err); }
});

//Instanciando el schema en mongoose
//var Schema = mongoose.Schema;

//Schema de cliente
/*var clientSchema = new Schema({
    idCliente : Number,
    alertEmail : String,
    alertCelPhone: String,
    alertName : String,
    socket : String,
    stateAlert: Number, // 1 = activo , 0 = desactivo
    stateCelPhone: Number // 1 = activo , 0 = desactivo
});*/

//Inicializamos el schema del conductor
//var Clients = module.exports = mongoose.model('clients', clientSchema);
module.exports.updateClienteViajes = function(cliente_id,cliente_vajes, callback){
    conexMysql.query('UPDATE tb_cliente SET cliente_viajes = ? WHERE cliente_id = ? ', [cliente_vajes,cliente_id], callback);
}

module.exports.updateClientePtos = function(idcliente,ptosCliente, callback){

    conexMysql.query('UPDATE tb_cliente SET cliente_puntos = ? WHERE cliente_id = ? ', [ptosCliente,idcliente], callback);
}

module.exports.updateClienteCalificacion = function(cliente_id, callback){
    conexMysql.query('UPDATE tb_cliente SET cliente_calificacion_flag = 1 WHERE cliente_id = ? ', [cliente_id], callback);
}

module.exports.updateClienteVista = function(cliente_id,cliente_vista, callback){
    conexMysql.query('UPDATE tb_cliente SET cliente_vista = ? WHERE cliente_id = ? ', [cliente_vista, cliente_id], callback);
}

module.exports.CustomServicio = function(idServicio, callback){
    conexMysql.query('SELECT s.*, c.cliente_socket, c.tipocliente_id FROM tb_servicio s INNER JOIN tb_cliente c ON s.cliente_id = c.cliente_id WHERE s.servicio_id = ?', [idServicio], callback);
}

module.exports.CustomTarifaMonto = function(origen,destino,tiposervicio, callback){
    if (destino == 0) {
        conexMysql.query('SELECT 0 AS tarifario_id, 0 AS tarifario_monto', callback);
    }
    else{
        conexMysql.query('SELECT tarifario_id, tarifario_monto FROM tb_tarifario WHERE origen_id = ? AND destino_id = ? AND tiposervicio_id = ? AND tarifario_status = 1', [origen,destino,tiposervicio], callback);
    }
}

module.exports.updateQuerySocketCliente = function(cliente_id,cliente_socket, callback){
    var socket_nuevo = cliente_socket.substring(2);
    conexMysql.query('UPDATE tb_cliente SET cliente_socket = ? WHERE cliente_id = ? ', [socket_nuevo, cliente_id], callback);
}

module.exports.findOneQueryCliente = function(Cliente_id, callback){
    conexMysql.query('SELECT * FROM tb_cliente WHERE cliente_id = ?', [Cliente_id], callback);
}

module.exports.alertaTaxistaEsperandoCliente = function(Cliente_id, callback){
    conexMysql.query('SELECT * FROM tb_cliente WHERE cliente_id = ? ', [Cliente_id], callback);
}

module.exports.buscardatoscliente = function(idCliente, callback){
    conexMysql.query('SELECT * FROM tb_cliente WHERE cliente_id = ?', [idCliente], callback);
}

module.exports.updateTokenCliente = function(cliente_id, token, callback){
    conexMysql.query('UPDATE tb_cliente SET cliente_token_android = ? WHERE cliente_id = ? ', [token, cliente_id], callback);
}
module.exports.findOneQueryClienteActivo = function(cliente_id,callback){
    conexMysql.query('SELECT * FROM tb_cliente WHERE cliente_id = ? AND cliente_estados = 1', [cliente_id], callback);
}
