/*
var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'T4x1H4mm3r',
    database : 'taxihammer',
    port : 3306
});
*/

var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'homestead',
    password : 'secret',
    database : 'taxihammer',
    port : 3306
});


conexMysql.connect(function(err){
    if(err){ console.log('Error ->', err); }
});

module.exports.validarTipoServicioId = function(servicio_id,callback){
    conexMysql.query('SELECT count(*) as total FROM tb_tiposervicio WHERE tiposervicio_id = ? and tiposervicio_status = 1', [servicio_id], callback);
}
//821,1,1
module.exports.recuperarEstado = function(conductor_id,flag,flag_null,callback){
	if(flag == 1){
		respuesta = ' conductor_id = ? ';
	}else{
		respuesta = ' cliente_id = ? ';
	}

    if(flag_null != 1){
        $not_in = ' IN (6) ';
    }else{
        $not_in = ' NOT IN (0,6,10) ';
    }

	conexMysql.query('SELECT *,CONVERT_TZ(fecha_recojo,"+00:00","+00:00") as fecha_recojo_hora FROM tb_servicio WHERE '+respuesta+' AND estado '+$not_in+' ORDER BY servicio_id DESC', [conductor_id], callback);
}

module.exports.validarVale = function(servicio_vale,callback){
    conexMysql.query('SELECT count(*) as TotalVale FROM tb_servicio where servicio_vale = ?', [servicio_vale], callback);
}

module.exports.configPtos = function(estado,callback){
    conexMysql.query('SELECT * FROM tb_puntos WHERE puntos_estado = ? and puntos_delete = 1', [estado], callback);
}

module.exports.updateConductorVale = function(servicio_vale,servicio_id, callback){

    conexMysql.query('UPDATE tb_servicio SET servicio_vale = ? WHERE servicio_id = ? ', [servicio_vale,servicio_id], callback);
}

module.exports.findQueryEstadoCuentaId = function(servicio_id,callback){
    conexMysql.query('SELECT * FROM tb_estado_cuenta WHERE servicio_id = ?', [servicio_id], callback);
}

module.exports.validarEstadoCuentaId = function(servicio_id,callback){
    conexMysql.query('SELECT count(*) as total FROM tb_estado_cuenta WHERE servicio_id = ?', [servicio_id], callback);
}

module.exports.insertCta = function(dataCta, callback){
    conexMysql.query('INSERT INTO tb_estado_cuenta (conductor_id, servicio_id, cta_tipo, cta_fecha, cta_monto, cta_check) VALUES (?,?,?,?,?,?)', [ dataCta.idConductor, dataCta.idServicio, dataCta.Ctatipo, dataCta.Ctafecha, dataCta.Ctamonto, dataCta.CtaCheck] , callback);
}

module.exports.MinutoServicios = function(servicio_id,callback){
    conexMysql.query('SELECT CAST(time_to_sec(timediff(hora_fin,hora_inicio))/60 as signed) as minutos FROM tb_servicio WHERE servicio_id = ?',[servicio_id],callback);
}

module.exports.updateTarifaNewKilometraje = function(servicio_id, tarifa, callback){
     conexMysql.query('UPDATE tb_servicio SET tarifa = ? WHERE servicio_id = ? ', [tarifa,servicio_id], callback);
}

module.exports.seguridadCliente = function(idcliente,callback){
    conexMysql.query('SELECT * FROM tb_datos_seguridad WHERE dseg_id = ? and dseg_status = 1', [idcliente], callback);
}

module.exports.montoCancelacion = function(idconfig,callback){
    conexMysql.query('SELECT * FROM tb_configuracion WHERE config_id = ?', [idconfig], callback);
}

module.exports.updateMontoCancelacionTarifa = function(servicio_id, tarifa, callback){
     conexMysql.query('UPDATE tb_servicio SET tarifa = ? WHERE servicio_id = ? ', [tarifa,servicio_id], callback);
}
module.exports.updateEstadoCuentaMonto = function(cta_monto, cta_id, callback){
     conexMysql.query('UPDATE tb_estado_cuenta SET cta_monto = ? WHERE cta_id = ? ', [cta_monto,cta_id], callback);
}
