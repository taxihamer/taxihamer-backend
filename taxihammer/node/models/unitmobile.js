//var mongoose = require('mongoose');

/*
var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'T4x1H4mm3r',
    database : 'taxihammer',
    port : 3306
});
*/

var conexMysql = mysql.createConnection({
    host : 'localhost',
    user : 'homestead',
    password : 'secret',
    database : 'taxihammer',
    port : 3306
});



const _ = require('lodash');
conexMysql.connect(function(err){
    if(err){ console.log('Error ->', err); }
});

//Instanciando el schema en mongoose
//var Schema = mongoose.Schema;

//Schema de cliente
/*var unitMobileSchema = new Schema({
    idUnitMobile : Number,
    alias : String,
    nroPlaque : String,
    marke : String,
    model: String,
    chassisSeries: String,
    colour: String,
    type: Number, //1= economico, 2=ejecutivo, 3=van, 0= all
    yearFabrication: String,
    dateRevTechnical: Date,
    dateSoat: Date,
    caracteristicas: String,
    state: Number
});*/

//Inicializamos el schema de la unidad Movil
//var UnitMobile = module.exports = mongoose.model('unitMobile', unitMobileSchema);

/*module.exports.createUnitMobile = function(newUnitMobile, callback){
    newUnitMobile.save(callback);
}

module.exports.findOneQuery = function(params, callback){
    UnitMobile.findOne(params, callback);
}

module.exports.updateQuery = function(params, setvalues, callback){
    UnitMobile.update(params, setvalues, callback);
}

module.exports.searchIdUnitMobile = function(idUnitMobile, callback){
    conexMysql.query('SELECT * FROM tb_taxi WHERE taxi_idtaxi = ?',[idUnitMobile], callback);
}

module.exports.listaUploadToMongo = function (callback){
    conexMysql.query('SELECT * FROM tb_taxi', callback);
}
*/

module.exports.findOneQueryUnidad = function(Unidad_id, callback){
    conexMysql.query('SELECT * FROM tb_unidad WHERE unidad_id = ? ', [Unidad_id], callback);
}

module.exports.validatePlacaDNI = function( placa, dni, callback){
    conexMysql.query( 'SELECT unidad_placa FROM tb_unidad WHERE unidad_placa = ? ', [ placa ], (err, result)=>{
        if( result.length > 0 ){
            callback( {message: 'Placa ya existe' } );
        }else{
            conexMysql.query('SELECT conductor_dni FROM tb_conductor WHERE conductor_dni = ?', [dni], (err, result)=>{
                if( result.length> 0){
                    callback({ message: 'DNI ya existe'});
                }else{
                    callback(null)
                }
            });
        }
    })
}

module.exports.createUnidad = function( tipo_servicio, placa, opciones_servicio, unidad_foto, unidad_marca, unidad_modelo, unidad_color, callback){
    
    conexMysql.query('SELECT unidad_id, unidad_alias FROM tb_unidad ORDER BY unidad_alias desc', (error, result)=>{
        if( !error ){
            let onlyNumberAlias = _.map(_.filter( result, (row)=>{
                return (_.toNumber(row.unidad_alias) > 0)
            }), (row)=>{
                return { unidad_alias: Number(row.unidad_alias), unidad_id: Number( row.unidad_id)};
            });
            onlyNumberAlias = _.orderBy( onlyNumberAlias, ['unidad_alias'], ['desc']);
            conexMysql.query('INSERT INTO tb_unidad (tiposervicio_id, unidad_alias, unidad_placa, unidad_status, unidad_asignada, unidad_caracteristicas, unidad_foto, unidad_marca, unidad_modelo, unidad_color) VALUES (?,?,?,?,?,?,?,?,?,?)', [ tipo_servicio, onlyNumberAlias[0].unidad_alias+1, placa, 1, 1, opciones_servicio, unidad_foto, unidad_marca, unidad_modelo, unidad_color ], (error)=>{
                    callback(onlyNumberAlias[0].unidad_id + 1);
            } );
        }
    });
}

