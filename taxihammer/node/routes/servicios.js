const express = require('express');
const router = express.Router();
const servicioController = require('../controllers/servicios');

router.post('/upload-foto-detalle', (req, res)=>{
  servicioController.uploadFoto( req, res ).then(
    ( success)=>{
      res.json( success );
    },
    (err)=>{
      res.status(500).json({message: 'Ocurrio un error al subir la imagen'});
    }
  )
});

module.exports = router;
