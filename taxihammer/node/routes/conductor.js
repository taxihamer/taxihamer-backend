const express = require('express');
const router = express.Router();
const conductorController = require('../controllers/conductor');

// const multer = require('multer');

// let newName = null;
// Configuracion de donde se guardaran los archivos csv subidos
// const storage = multer.diskStorage({
//   destination: (req, file, callback) =>{
//     callback(null, '/var/www/html/taxihammer/dist/css/');
//   },
//   filename: (req, file, callback) => {
//     let ext = file.originalname.split('.')[file.originalname.split('.').length -1];
//     newName = Date.now()+'.'+ext;
//     callback(null, newName);
//   }
// });


// const upload = multer({ storage : storage})
// const cpUpload = upload.fields([{ name: 'perfil', maxCount: 1 }, { name: 'auto', maxCount: 1 }, { name: 'licencia', maxCount: 1 }, { name: 'soat', maxCount: 1 }, { name: 'tarjeta_propiedad', maxCount: 1 }]);

router.post('/',  (req, res)=>{
  conductorController.store( req, res ).then(
    ( success)=>{
      res.json( success );
    },
    (err)=>{
      console.log( err );
      res.status(500).json(err);
    }
  )
});

module.exports = router;
