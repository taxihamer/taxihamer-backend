// requiriendo e inicializando librerias
var express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    server = require('http').createServer(app),
    io  = require('socket.io').listen(server);

    mysql = require('mysql'),
    moment = require('moment-timezone'),
    mailer = require('express-mailer');

//Realizamos la conexion a mysql que es el que contiene informacion de los datos que aparecen en la web

var Conductors = require('./models/conductors');
var Clients = require('./models/clients');
var UnitMobile = require('./models/unitmobile');

//Ponemos a escuchar las notificaciones del servidor
server.listen(process.env.PORT || 3000);

//Parseamos en un formato necesario para la interpretacion de los parametros que se reciban
app.use(bodyParser.urlencoded({extended : false }));
app.use(bodyParser.json());
console.log('incio');

//Config send Mailer
mailer.extend(app, {
    from : 'info.prueba.appslovers@gmail.com',
    host : 'smtp.gmail.com',
    transportMethod: 'SMTP',
    secureConnection : true,
    port: 465,
    auth:{
        user: 'info.prueba.appslovers@gmail.com',
        pass: 'Appsl0v3rs'//''
    }
});

//Setear las vistas .jade 
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
//app.set('view engine', 'html');

var glob_destLatitud = 0; //docs.Destlatitud;
var glob_destLongitud = 0; // docs.Destlongitud;
var glob_idConductor = 0;
var glob_latitudTaxi = 0; //docs.latitud;
var glob_longitudTaxi = 0; //docs.longitud;
var glob_estadoCond = 1;
var glob_estadoSolicitud = 1;
var glob_diaHora = '';
var glob_latitudCliente = 0; //docs.latitud;
var glob_longitudCliente = 0; //docs.longitud;
var glob_idCliente = 0;
//var glob_socketCliente = docs.socketCliente;
var glob_origin = 0; //docs.origin;
var glob_refOrigin = 0; //docs.refOrigin;
var glob_destine =  0; //docs.destine;
var glob_refDestine = 0; //docs.refDestine;
var glob_typeService = 0; //docs.typeService;
var glob_cost = 0; //docs.cost;
var glob_reserve = 0;//docs.reserve;
var glob_idServicio = 0;
//Funcion para enviar por despacho web
app.get('/sendDespacho', function(req, res){
    console.log('juhm');
    if(typeof req.query.idConductor != 'undefined'){
        glob_destLatitud = 0; //docs.Destlatitud;
        glob_destLongitud = 0; // docs.Destlongitud;
        glob_idConductor = req.query.idConductor;
        glob_latitudTaxi = req.query.latitud; //docs.latitud;
        glob_longitudTaxi = req.query.longitud; //docs.longitud;
        glob_estadoCond = 1;
        glob_estadoSolicitud = 1;
        glob_now = moment().tz("America/Lima").format();
        glob_arr = glob_now.split('T');
        glob_diaHora = glob_arr[0]+' '+glob_arr[1].split('-')[0];
        glob_latitudCliente = req.query.latitud; //docs.latitud;
        glob_longitudCliente = req.query.longitud; //docs.longitud;
        glob_idCliente = req.query.idCliente;
        //glob_socketCliente = docs.socketCliente;
        glob_origin = req.query.origen; //docs.origin;
        glob_refOrigin = req.query.refOrigen; //docs.refOrigin;
        glob_destine =  req.query.destino; //docs.destine;
        glob_refDestine = req.query.refDestino; //docs.refDestine;
        glob_typeService = req.query.tipoServicio; //docs.typeService;
        glob_cost = req.query.costo; //docs.cost;
        glob_reserve = 0;//docs.reserve;
        glob_idServicio = req.query.idServicio;

        Conductors.findOneQuery({idConductor:glob_idConductor}, function(err, rowsConductor){
            if(err){
                console.log('ERROR SEND DESPACHO', err);
            }else{
                io.to(rowsConductor.socket).emit('solicitudListaTaxi', { nombre:req.query.cliente, origen:req.query.origen, destino:req.query.destino, latitud:glob_latitudCliente, longitud:glob_longitudCliente, socketCliente:null, tarifa:req.query.costo });
            }
        });
        res.send('ok');
    }else{
        res.send('no ok');
    }
});



//Actualizar Geolocalizacion de conductor
app.post('/setUpdateGeoConductor', function (req, res){
    var idConductor = parseFloat(req.body.idConductor);
    var latitud = parseFloat(req.body.latitud);
    var longitud = parseFloat(req.body.longitud);
    
    Conductors.updateQuery({idConductor: idConductor }, {$set:{ coordinates: [latitud, longitud] } }, function(err){
        if(err){
            console.log('Error app.post/setUpdateGeoConductor -> ', err);
        }else{
            console.log('Entro2!!');
            res.send('Actualizado');
        }
    });
});

//Lista el historial del cliente
app.post('/historyClient', function (req, res){
    console.log(req.body.idCliente, 'history!!!');
    if(typeof req.body.idCliente != 'undefined' ){
        Clients.searchServiceIdClient( req.body.idCliente, function(err, result){
            res.json(result);
            console.log(result);
        });
    }
});

//Lista el historial de servicio del conductor
app.post('/historyService', function (req, res){
    if(typeof req.body.idConductor != 'undefined'){
        var idConductor = req.body.idConductor;
        console.log('entra histoy service', idConductor);
        Conductors.searchServiceIdConductor( req.body.idConductor, function(err, result){
            var servicio = [];
            for(var i in result){
                servicio.push({destino: result[i].dserv_destino, origen: result[i].dserv_origen, fecha: result[i].fecha, hora:result[i].hora, costo: result[i].dserv_costo, nombreCliente: result[i].cli_nombres, apellidosCliente: result[i].cli_apellidos});
                if(i == (result.length-1) ){
                    console.log({ totalDia:result[0].totalDia, servicio: servicio });
                    res.json({ totalDia:result[0].totalDia, servicio: servicio });
                }
            }
        });
    }
});

//Lista las rutas frecuentes del cliente
app.post('/frequentClient', function (req, res){
    if( typeof req.body.idCliente != 'undefined' ){
        Clients.searchFrequentIdClient(req.body.idCliente, function(err, result){
            res.json(result);
        });
    }
});

app.get('/viewMapConductor', function(req, res){
    if(typeof req.query.servicio != "undefined"){
        Conductors.findOneQuery({idDetalleServicio: req.query.servicio}, function(err, result){
            if(result){
                res.render('mapConductor', { idDetalleServicio: req.query.servicio, latitud:result.coordinates[0], longitud:result.coordinates[1]});
            }else{
                res.send('Servicio no existe o terminado');
            }
        });
    }
});

app.post('/saveFrequentClient', function(req, res){
    console.log(req.body.idCliente, 'id');
    if(typeof req.body.idCliente != 'undefined'){
        console.log(req.body.idCliente, 'id');
        Clients.updateFrequentClient(req.body.idCliente, 1, function(err, result){
            res.send('OK');
        });
    }
});

app.post('/removeFrequentClient', function(req, res){
    if(typeof req.body.idServicio != 'undefined'){
        Clients.removeFrequentClient(req.body.idServicio, function(err,result){
            res.send('OK');
        });
    }
});


app.get('/alertReservaConductor', function (req, res){
     if(typeof req.query.idConductor != 'undefined'){
        glob_destLatitud = 0; //docs.Destlatitud;
        glob_destLongitud = 0; // docs.Destlongitud;
        glob_idConductor = req.query.idConductor;
        glob_latitudTaxi = req.query.latitud; //docs.latitud;
        glob_longitudTaxi = req.query.longitud; //docs.longitud;
        glob_estadoCond = 1;
        glob_estadoSolicitud = 1;
        glob_now = moment().tz("America/Lima").format();
        glob_arr = glob_now.split('T');
        glob_diaHora = glob_arr[0]+' '+glob_arr[1].split('-')[0];
        glob_latitudCliente = req.query.latitud; //docs.latitud;
        glob_longitudCliente = req.query.longitud; //docs.longitud;
        glob_idCliente = req.query.idCliente;
        //glob_socketCliente = docs.socketCliente;
        glob_origin = req.query.origen; //docs.origin;
        glob_refOrigin = req.query.refOrigen; //docs.refOrigin;
        glob_destine =  req.query.destino; //docs.destine;
        glob_refDestine = req.query.refDestino; //docs.refDestine;
        glob_typeService = req.query.tipoServicio; //docs.typeService;
        glob_cost = req.query.costo; //docs.cost;
        glob_reserve = 0;//docs.reserve;
        glob_idServicio = req.query.idServicio;

        Conductors.findOneQuery({idConductor:glob_idConductor}, function(err, rowsConductor){
            if(err){
                console.log('ERROR SEND DESPACHO', err);
            }else{
                io.to(rowsConductor.socket).emit('solicitudListaTaxi', { latitud:glob_latitudCliente, longitud:glob_longitudCliente, socketCliente:null });
            }
        });
        res.send('ok');
    }else{
        res.send('no ok');
    }
})

app.get('/updateCalificationConductor', function (req, res){
    var idConductor = req.query.idConductor;
    var calificacion = req.query.calificacion;
    console.log(calificacion);
    
    Conductors.updateQuery({idConductor:idConductor}, {$set:{ calification:calificacion}}, function (err){
        if(err){ console.log('ERROR app.get(updateCalificationConductor)'); }
    });
    res.send('OK');
});

app.get('/resetSolicitud', function(req, res){
    Conductors.resetSolicitud(function(err){
        if(!err){ res.send('echo'); }
    });
});

//HTTP para saber estado del taxista, si esta ocupado, disponible, en camino, etc.
app.post('/estadoConductor', function (req, res){
    var idConductor = req.body.idConductor;
    console.log('estadoConductor 1', idConductor);
    Conductors.findOneQuery({ idConductor: idConductor}, function(err, respConductors){
        
        var returnConductor = null;
        if(respConductors){
            if(respConductors.stateConductor == 1){
                returnConductor = { stateConductor: 1, client:null };
                res.json(returnConductor);
            }else{
                Clients.findOneQuery({ idCliente : respConductors.idCliente }, function(err, respClients){
                    if(respClients){
                        Clients.searchIdClient(respClients.idCliente, function(err,result){
                            if(err){
                                res.json({stateConductor: respConductors.stateConductor , client : returnConductor });
                            }else{
                                if ( respConductors.stateConductor == 2 || respConductors.stateConductor ==3 || respConductors.stateConductor  == 4){
                                    Conductors.searchIdDetalleServicio(respConductors.idDetalleServicio, function(err, resultDetalle){
                                        if(err){ console.log('Error ->', err); 
                                            res.json({stateConductor: respConductors.stateConductor , client : returnConductor });
                                        }else if(resultDetalle){ 
                                            returnConductor = { 
                                                empresa: result[0].emp_idempresa,
                                                pasajero : resultDetalle[0].dserv_pasajero,
                                                origen : resultDetalle[0].dserv_origen,
                                                latOrigen : resultDetalle[0].dserv_latorigen,
                                                lonOrigen : resultDetalle[0].dserv_lonorigen,
                                                refOrigen : resultDetalle[0].dserv_reforigen,
                                                destino : resultDetalle[0].dserv_destino,
                                                refDestino : resultDetalle[0].dserv_refdestino,
                                                tipo : resultDetalle[0].dserv_tipo,
                                                costo : resultDetalle[0].dserv_costo,
                                                clienteCorreo : result[0].cli_correo,
                                                clienteNombres : result[0].cli_nombres,
                                                clienteApellidos : result[0].cli_apellidos,
                                                clienteCelular : result[0].cli_ncelular,
                                                tipoCliente : 0, //1 particular //2 empresa
                                                idServicio : respConductors.idDetalleServicio,
                                                idCliente : respConductors.idCliente,
                                                nameEmpresa: result.emp_nombre,
                                                Destlatitud: resultDetalle[0].dserv_latdestino,
                                                Destlongitud: resultDetalle[0].dserv_londestino

                                            };
                                            console.log('estadoConductor 2',returnConductor);
                                            res.json({stateConductor: respConductors.stateConductor , client : returnConductor });
                                        }
                                    });
                                }else{
                                    res.json({stateConductor: respConductors.stateConductor , client : returnConductor });
                                }
                            }
                        });
                    }else{
                        res.json({stateConductor: respConductors.stateConductor , client : returnConductor });
                    }
                });
            }
        }else{
            console.log('oj');
            res.json({stateConductor: null, client : null });
        }
    });
});

//HTTP para saber estado del cliente, en caso este en un taxi o este solicitando algun servicio, etc.
app.post('/estadoCliente', function (req, res){
    var idCliente = req.body.idCliente;
    var datoConductors = [];
    Conductors.searchServiceIdConductorAll(idCliente, function(err, respService){
        if(err){
            console.log('Error -> mysql -> searchServiceIdConductorAll');
        }else if(respService[0]){
            if(typeof respService[0].dserv_latdestino != 'undefined'){
                Conductors.findOneQuery({ idCliente: idCliente }, function(err, respConductors){
                    if(err){
                        console.log('Error -> POst/estadoCliente/Conductors.findOneQuery ',err);
                    }else if(respConductors){
                        UnitMobile.findOneQuery({ idUnitMobile: respConductors.unitMobile }, function(errUnitMobile, respUnitMobile){
                            if(errUnitMobile){
                                console.log('Error -> POst/estadoCliente/UnitMobile.findOneQuery ',err);
                                res.json(datoConductors);
                            }else if(respUnitMobile){
                                datoConductors = {
                                    idConductor: respConductors.idConductor,
                                    name: respConductors.name,
                                    lastName: respConductors.lastName,
                                    email: respConductors.email,
                                    celPhone: respConductors.celPhone,
                                    unitMobile: respConductors.unitMobile,
                                    photo: respConductors.photo,
                                    coordinates: respConductors.coordinates,
                                    socket: respConductors.socket,
                                    stateConductor: respConductors.stateConductor,
                                    idCliente: respConductors.idCliente,
                                    idDetalleServicio: respConductors.idDetalleServicio,
                                    socketViewWeb: respConductors.socketViewWeb,
                                    marca : respUnitMobile.marke,
                                    modelo: respUnitMobile.model,
                                    nroPlaca: respUnitMobile.nroPlaque,
                                    color: respUnitMobile.colour,
                                    tipo: respUnitMobile.type,
                                    latOrigen: respService[0].dserv_latorigen,
                                    lonOrigen: respService[0].dserv_lonorigen,
                                    Destlatitud: respService[0].dserv_latdestino,
                                    Destlongitud: respService[0].dserv_londestino
                                };
                                console.log('Send datoConductors', datoConductors);
                                res.json([datoConductors]);
                            }else{
                                res.json(datoConductors);
                            }
                        });
                        
                    }else{
                        res.json(datoConductors);
                    }
                });
            }else{
                res.json(datoConductors);
            }
        
        }else{
            Conductors.findOneQuery({ idCliente: idCliente }, function(err, respConductors){
                if(err){
                    console.log('Error -> POst/estadoCliente/Conductors.findOneQuery ',err);
                }else if(respConductors){
                    UnitMobile.findOneQuery({ idUnitMobile: respConductors.unitMobile }, function(errUnitMobile, respUnitMobile){
                        if(errUnitMobile){
                            console.log('Error -> POst/estadoCliente/UnitMobile.findOneQuery ',err);
                            res.json(datoConductors);
                        }else if(respUnitMobile){
                            datoConductors = {
                                idConductor: respConductors.idConductor,
                                name: respConductors.name,
                                lastName: respConductors.lastName,
                                email: respConductors.email,
                                celPhone: respConductors.celPhone,
                                unitMobile: respConductors.unitMobile,
                                photo: respConductors.photo,
                                coordinates: respConductors.coordinates,
                                socket: respConductors.socket,
                                stateConductor: respConductors.stateConductor,
                                idCliente: respConductors.idCliente,
                                idDetalleServicio: respConductors.idDetalleServicio,
                                socketViewWeb: respConductors.socketViewWeb,
                                marca : respUnitMobile.marke,
                                modelo: respUnitMobile.model,
                                nroPlaca: respUnitMobile.nroPlaque,
                                color: respUnitMobile.colour,
                                tipo: respUnitMobile.type,
                                latOrigen: 0,
                                lonOrigen: 0,
                                Destlatitud: 0,
                                Destlongitud: 0
                            };
                            console.log('Send datoConductors', datoConductors);
                            res.json([datoConductors]);
                        }else{
                            res.json(datoConductors);
                        }
                    });
                    
                }else{
                    res.json(datoConductors);
                }
            });
        }
    });
});

//HTPP para actualizar los datos de alerta del cliente
app.post('/updateDataAlertClient', function (req, res){
    console.log('updateDataAlertClient', req.body.idCliente);
    Clients.updateQuery({idCliente: req.body.idCliente}, {$set:{alertEmail: req.body.alertEmail, alertCelPhone: req.body.alertCelPhone, alertName: req.body.alertName, stateAlert: req.body.stateAlert, stateCelPhone:req.body.stateCelPhone }}, function(err){
        if(!err){ res.send(true); }
    });
});

//HTPP para retornar los datos de alerta del cliente
app.post('/dataAlertClient', function (req, res){
    console.log('dataAlertClient', req.body.idCliente);
    Clients.findOneQuery({idCliente: req.body.idCliente}, function(err, result){
        if(result){
            res.json(result);
        }else{
            var newCliente = new Clients({
                idCliente : req.body.idCliente,
                alertEmail : null,
                stateAlert : 0,
                alertCelPhone : null,
                alertName : null,
                stateCelPhone: 0,
                socket : null
            });
            //Insertar en la tabla clients de mongo
            Clients.createClients( newCliente, function(err){
                if(!err){ res.send('OK'); console.log('OK'); }
            });
            res.json({
                alertEmail : null,
                stateAlert : 0,
                alertCelPhone : null,
                alertName : null,
                stateCelPhone: 0
            });
        }
    });
});

var datoOcupado;
function iniciarRecojo (docs, respt){
    var now = moment().tz("America/Lima").format();
    var arr = now.split('T');
    docs.diaHora = arr[0]+' '+arr[1].split('-')[0];
    
    console.log(docs, 'returl');
    datoOcupado = 0;
    Conductors.findOneQuery({idConductor:docs.idConductor, stateConductor:1},function(err, rowsConductor){

        if(err){
            console.log('ERROR - >Conductors.findOneQuery PARTE 1', err);
            console.log(datoOcupado,'<--- UHMMMM1');
            //respt(datoNum);
            respt('ok');
        }else{
            if(rowsConductor){
                console.log(datoOcupado,'<--- UHMMMM1.2');
                if(typeof docs.idDetaService == 'undefined'){
                    console.log(datoOcupado,'<--- UHMMMM1.3');
                    var dataInsertDetalleServicio = { idCliente:docs.idCliente, origen:docs.origin, refOrigen:docs.refOrigin, latitud:docs.latitud, longitud:docs.longitud, destino:docs.destine, refDestino : docs.refDestine, tipoServicio: docs.typeService, costo:docs.costo, reserva:docs.reserve, idReserva : null, diaHora: docs.diaHora, destLatitud:docs.Destlatitud, destLongitud:docs.Destlongitud };
                     console.log(dataInsertDetalleServicio);
                    Conductors.insertDetalleServicio( dataInsertDetalleServicio, function(err, resultDetalle){
                        if(err){ 
                            console.log('ERROR socket.on(solicitarServicioConductor) parte 1', err); 
                        }else{ 
                            console.log(datoOcupado,'<--- UHMMMM1.4');
                            var dataInsertMovimiento = { idConductor:docs.idConductor, latitudTaxi:docs.latitud, longitudTaxi:docs.longitud, estadoCond:1, estadoSolicitud:1, diaHora:docs.diaHora, latitudCliente:docs.latitud, longitudCliente:docs.longitud, idDetalleServicio:resultDetalle.insertId , idCliente:docs.idCliente};
                            //Insertando en moviminetos  en mysql
                            Conductors.insertMovimientos(dataInsertMovimiento, function(err){
                                if(err){ console.log('Error ->', err);
                                }else{ 
                                    console.log('Solicitud de movimiento Registrado en mysql'); 
                                    registerMovs(docs, resultDetalle.insertId); 
                                }
                            });
                            
                        }
                    });
                }else{
                    registerMovs(docs, docs.idDetaService); 
                }
                
                console.log(datoOcupado,'<--- UHMMMM2');
            }
            datoOcupado = 1;
            console.log(datoOcupado,'<--- UHMMMM3');
//            respt(datoNum);
            respt('ok');
        }
    });
    //console.log(datoNum,'<--- UHMMMM3');
    //return datoNum;

}

var registerMovs = function(docs, idDetaService){
    Conductors.updateQuery({idConductor: docs.idConductor, stateConductor:1}, {$set:{ stateConductor: 3, idDetalleServicio:idDetaService, idCliente:docs.idCliente } }, function(err){
        if(err){ console.log(err, 'Erro aceptar');
        }else{  
            

             var dataInsertMovimiento = { idConductor:docs.idConductor, latitudTaxi:docs.latitud, longitudTaxi:docs.longitud, estadoCond:2, estadoSolicitud:2, diaHora:docs.diaHora, latitudCliente:docs.latitud, longitudCliente:docs.longitud, idDetalleServicio:idDetaService , idCliente:docs.idCliente};
            //Insertando en moviminetos  en mysql
            Conductors.insertMovimientos(dataInsertMovimiento, function(err){
                if(err){ console.log('Error ->', err);
                }else{ console.log('Inserta movimiento Registrado en mysql'); }
            });


            Conductors.findOneQuery({idConductor:docs.idConductor }, function(err, rowsConductor){
                if(err){
                    console.log('ERROR -> RespuestaSolicitudServicio', err);
                }else if(rowsConductor){
                    //Buscar los datos del cliente que solicita el servicio segun ID
                    Clients.searchIdClient(docs.idCliente, function(err, result){
                        if(err){
                            console.log('Error ->', err);
                        }else{
                            result = result[0];
                            console.log('Updateando');
                            //Enviar una notificacion de servicio a un conductor
                            io.to(rowsConductor.socket).emit('respuestaSolicitudServicio', 
                            {
                                empresa: result.emp_idempresa,
                                pasajero : '?',
                                origen : docs.origin,
                                latOrigen : docs.latitud,
                                lonOrigen : docs.longitud,
                                refOrigen : docs.refOrigin,
                                destino : docs.destine,
                                refDestino : docs.refDestine,
                                Destlatitud : docs.Destlatitud,
                                Destlongitud : docs.Destlongitud,
                                tipo : docs.typeService,
                                costo : docs.costo,
                                clienteCorreo : result.cli_correo,
                                clienteNombres : result.cli_nombres,
                                clienteApellidos : result.cli_apellidos,
                                clienteCelular : result.cli_ncelular,
                                tipoCliente : 0,
                                idServicio : idDetaService,
                                idCliente : docs.idCliente,
                                nameEmpresa: result.emp_nombre
                            }); 
                            console.log('Solicitud enviada');
                            console.log(docs.costo);
                        }
                    });
                    
                }
            });
        }
    });
}
//Conectantando al socket del servidor en nodeJS
io.sockets.on('connection', function(socket){

    
    //En caso un conductor se desconecte se actualiza el socket necesario para tenerlo en cuenta.
    socket.on('disconnect', function(){
        Conductors.findOneQuery({ socket:socket.id }, function(err, result){
            if(result){
                Conductors.updateQuery({socket : socket.id},{ $set:{ socket:null} }, function(err){
                    if(!err){ 
                        console.log('user disconnected', socket.id); 
                        io.sockets.emit('refreshCoordinates', {idConductor: result.idConductor, latitud: result.coordinates[0], longitud: result.coordinates[1], socket:null, stateConductor: result.stateConductor, tlf: result.celPhone });
                    }; 
                })
            }
        });
    });

    //EDITADO 
    //Lista de conductores segun latitud y longitud en orden de mayor cercania 
    socket.on('getListConductorGeo', function (docs){
        docs = (typeof docs.idConductor == 'undefined')? JSON.parse(docs) : docs;
        var latitud = docs.latitud;
        var longitud = docs.longitud;
        var typeMobile = docs.typeMobile;
        var socketCliente = socket.id;
        var idCliente = docs.idCliente;
        var tarifa = docs.tarifa;
        var origen = docs.origen;
        var destino = docs.destino;
        console.log("TIPO DE MOVIL --->");
        console.log(typeMobile);
        console.log("ID CLIENTE --->");
        console.log(idCliente);

        Conductors.buscardatoscliente(idCliente,function (err, dac){
            console.log(dac);

            if(err){
                console.log('ERROR socket.on(getListConductorGeo) part 0', err);
            }else{
                if(dac){
                
                        nombreCliente = dac[0].cli_nombres;
                        telefonoCliente = dac[0].cli_ncelular;
                        calificacion = dac[0].cli_calificacion;
                        console.log(calificacion);

                    }
            }
        });

        Conductors.findCoordinates(latitud, longitud, typeMobile, function (err, docs){
            if(err){
                console.log('ERROR socket.on(getListConductorGeo) part 1', err);
            }else{
                if(docs){
                    console.log(docs);
                    if(typeof docs[0] != 'undefined'){
                        docs.forEach( function (docsRows) {
                            Conductors.validarConductor(docsRows.idConductor, function (err, da){
                                if(da[0].cond_estado != 0){
                                    io.to(docsRows.socket).emit('solicitudListaTaxi', { nombre: nombreCliente, celular: telefonoCliente, calificacion: calificacion,  tarifa: tarifa, origen: origen, destino: destino, latitud:latitud, longitud:longitud, socketCliente:socketCliente });
                                }
                            });
                        });
                    }
                }
            }
        });
    });

    //NUEVO
    //Recibe los conductores que aceptaron estar en la lista de solicitudes
    socket.on('solicitudAceptaListaTaxi', function(docs){
        if(typeof docs.idConductor == 'undefined'){
            docs = JSON.parse(docs);
        }
        console.log('DOCS --->');
        console.log(docs);
        if(typeof docs.socketCliente != 'undefined'){

            Conductors.findOneQuery({ idConductor: docs.idConductor }, function (err, rowsConductor){
                console.log('rowsConductor ------>');
                console.log(rowsConductor);
                if(err){
                    console.log('ERROR socket.on(getListConductorGeo) part 2 ');

                }else if(rowsConductor){
                    console.log(rowsConductor.unitMobile);
                    UnitMobile.findOneQuery({ idUnitMobile:rowsConductor.unitMobile }, function(err, rowsUnitMobile){
                        console.log('unitmobile ----->');
                        console.log(rowsUnitMobile);
                        if(err){
                            console.log('ERROR socket.on(getListConductorGeo) part 3 ');

                        }else if(rowsUnitMobile){
                            console.log('AQUI ABAJO CARACTERISTICAS --->>_>>>_>>_>_>_>_>');
                            if(rowsUnitMobile.caracteristicas==undefined){
                                rowsUnitMobile.caracteristicas = " ";
                            }
                            console.log(rowsConductor.caracteristicas);
                            io.to(docs.socketCliente).emit('listaTaxiAceptado', {
                                idConductor: docs.idConductor,
                                name: rowsConductor.name,
                                lastName: rowsConductor.lastName,
                                email: rowsConductor.email,
                                celPhone: rowsConductor.celPhone,
                                photo: rowsConductor.photo,
                                coordinates: rowsConductor.coordinates,
                                calification : rowsConductor.calification,
                                nroPlaca: rowsUnitMobile.nroPlaque,
                                marca: rowsUnitMobile.marke,
                                modelo: rowsUnitMobile.model,
                                color: rowsUnitMobile.colour,
                                tipo: rowsUnitMobile.type,
                                caracteristicas: rowsUnitMobile.caracteristicas
                            });
                        }
                    });
                }
            });

        }else{  

            Conductors.findOneQuery({ idConductor: docs.idConductor }, function (err, rowsConductor){
                if(err){
                    console.log('ERROR socket.on(getListConductorGeo) part 2 ');
                }else if(rowsConductor){
                    console.log('Send de frente',rowsConductor.socket);
                    var docs = {Destlatitud:glob_destLatitud, Destlongitud: glob_destLongitud, idConductor:glob_idConductor, latitud:glob_latitudTaxi, longitud:glob_longitudTaxi, idCliente:glob_idCliente, origin:glob_origin, refOrigin:glob_refOrigin, destine:glob_destine, refDestine:glob_refDestine, typeService:glob_typeService, cost:glob_cost,  reserve:glob_reserve, idDetaService:glob_idServicio};
                    iniciarRecojo(docs, function(rpta){
                        console.log(rpta);
                    });
                }
            });
        }
    });

    //Modificar a ocupado por transeunte
    /*
    socket.on('ocupadoPorTransito', function(docs){
        if(typeof docs.idConductor == 'undefined'){
            docs = JSON.parse(docs);
        }
        var idConductor = docs.idConductor;
        var tipo = docs.tipo;
        var estado = 1;
        if( tipo == 1){
            estado = 5;
        }
        console.log(estado, tipo, idConductor, 'ocupadoPorTransito');
        Conductors.updateQuery({idConductor:idConductor}, {$set:{stateConductor: estado} }, function(err){
            if(!err){
                console.log('Ok ');
            }
        });
    });*/

    //Avisar que el taxista esta esperando
    socket.on('alertaTaxistaEsperando', function (docs){
        if( typeof docs.idCliente == 'undefined' ){
            docs= JSON.parse(docs);
        }
        var idCliente = docs.idCliente;
        Clients.findOneQuery({idCliente:idCliente}, function(err, respCliente){
            if(err){
                console.log('Error socketOn alertaTaxistaEsperando -> ', err);
            }else if(respCliente){
                io.to(respCliente.socket).emit('alertaTaxistaEsperando', true);
            }
        });
    });

    //Alerta de taxista para cpanel
    socket.on('alertaTaxi', function(docs){
        if(typeof docs.idConductor == 'undefined'){
            docs = JSON.parse(docs);
        }
        var idConductor = docs.idConductor;
        var latitud = docs.latitud;
        var longitud = docs.longitud;
        console.log(idConductor, 'Alertaaa');
        Conductors.findOneQuery({idConductor: idConductor}, function(err, respConductor){
            console.log('emit aterta taxi web');
            io.sockets.emit('alertaTaxiWeb', {latitud: latitud, longitud:longitud, nombre: respConductor.name, apellido: respConductor.lastName, idConductor:idConductor });
        });
    });

    //Lista los conductores para el mapa en la web
    socket.on('dataMapConductors', function(tipoServicio){
        Conductors.findQuery({ stateConductor:{$ne:0}, tipo: tipoServicio}, function(err, result){
            var cont = 0;
            if(result){
                for(var i in result){
                    var result = JSON.parse(JSON.stringify(result));
                    console.log(result[i].unitMobile);
                    UnitMobile.searchIdUnitMobile(result[i].unitMobile, function(err, respUnitMobile){
                        if(err){
                            console.log('ERROR -> UnitMobile.searchIdUnitMobile', err);
                        }else{
                            if(typeof respUnitMobile != 'undefined' ){
                                if(typeof respUnitMobile[0] != 'undefined'){
                                    if(typeof respUnitMobile[0].taxi_nplaca != 'undefined'){
                                        result[cont].nroplaca = respUnitMobile[0].taxi_nplaca;
                                        result[cont].marca = respUnitMobile[0].taxi_marca;
                                        result[cont].modelo = respUnitMobile[0].taxi_modelo;
                                        result[cont].unitAlias = respUnitMobile[0].taxi_alias;
                                    }
                                }
                            }
                        }
                        cont++;
                        if( cont == result.length){
                            io.sockets.emit('listaConductors', result);
                        }
                    });
                }
            }
        });
    });

    //Actualizacion de sockets web
    socket.on('updateSocketWeb', function(idDetalleServicio){
        console.log('socket', socket.id, idDetalleServicio);
        Conductors.updateQuery({ idDetalleServicio : idDetalleServicio}, {$set:{ socketViewWeb: socket.id } }, function(err){
            if(err){ console.log('err'); }
        });
    });

    //Actualizacion socket de conductores al momento que inicia la aplicacion
    socket.on('setUpdateSocketConductor', function(docs){
        if( typeof docs.idConductor == 'undefined'){
            docs = JSON.parse(docs); 
        }
        var idConductor = docs.idConductor;
        var socketConductor = docs.socketConductor;
        console.log('Entro setUpdateSocketConductor', socketConductor);
        Conductors.updateQuery({ idConductor: idConductor }, {$set:{socket: socketConductor }}, function(err){
                if(!err){
                    Conductors.validarConductor(idConductor, function (err, da){
                    if(da[0].cond_estado != 0){
                        io.to(socketConductor).emit('respUpdateSocketConductor', 'true');
                    }else{
                        io.to(socketConductor).emit('respUpdateSocketConductor', 'false');
                    }

               }); 
            }
        });
    });

    //Actualizacion del socket de clientes al momento que incia la aplicacion
    //CHECKEAR SI ESTA BLOQUEADO NEPE
    socket.on('setUpdateSocketCliente', function(docs){
        console.log('entra cliente', docs);
        if(typeof docs.idCliente == "undefined"){
            docs = JSON.parse(docs);
        }
        console.log('params ',docs.idCliente, docs.socketCliente);
        
        var idCliente = docs.idCliente;
        var socketCliente = docs.socketCliente;
        console.log('params ',docs.idCliente, docs.socketCliente);
        Clients.findOneQuery({ idCliente : idCliente }, function(err, result){
            if(!result){
                var newCliente = new Clients({
                    idCliente : idCliente,
                    alertEmail : null,
                    stateAlert : 0,
                    alertCelPhone : null,
                    alertName : null,
                    stateCelPhone: 0,
                    socket : socketCliente
                });
                //Insertar en la tabla clients de mongo
                Clients.createClients( newCliente, function(err){
                    if(!err){ res.send('OK'); }
                });
            }else{
                console.log('update cliente ---->>', idCliente);
                Clients.updateQuery( { idCliente:idCliente },{ $set:{ socket:socketCliente }}, function(err){
                    if(!err){ console.log('ok'); }
                });
            }
            
            console.log(socketCliente, 'socket cliente');
            io.to(socketCliente).emit('respUpdateSocketCliente', 'true');
        });
    });

    //Actualiza la geolocalizacion de conductor para visualizar en el mapa la posicion del conductor.
    socket.on('setUpdateGeoConductor', function(docs){
        if(typeof docs.idConductor == 'undefined'){
            docs = JSON.parse(docs);    
        }
        var idConductor = parseFloat(docs.idConductor);
        var latitud = parseFloat(docs.latitud);
        var longitud = parseFloat(docs.longitud);

        Conductors.updateQuery({idConductor: idConductor }, {$set:{ coordinates: [latitud, longitud] } }, function(err){
            Conductors.findOneQuery({ idConductor:idConductor }, function(err, result){
                if(result){
                    console.log('actualiza geoconductor');
                    Clients.findOneQuery({ idCliente: result.idCliente }, function(err, resultCliente){
                        if(err){
                            console.log('ERROR setUpdateGeoConductor',err);
                        }else{
                            if(resultCliente){
                                if(result.stateConductor == 3){
                                    io.to(resultCliente.socket).emit('getGeoConductor', {latitud : result.coordinates[0], longitud: result.coordinates[1] } ); 
                                }else if(result.stateConductor == 2){
                                    io.to(resultCliente.socket).emit('getGeoConductor', {latitud : result.coordinates[0], longitud: result.coordinates[1] } ); 
                                    if(resultCliente.stateAlert == 1){
                                        io.to(result.socketViewWeb).emit('refreshCoordinatesWeb', {latitud : result.coordinates[0], longitud: result.coordinates[1], finished:false } );
                                    }
                                }        
                            }
                        }
                        
                    });
                }
                console.log(idConductor, latitud, longitud);
                io.sockets.emit('refreshCoordinates', {idConductor: docs.idConductor, latitud: latitud, longitud: longitud, socket:result.socket, stateConductor: result.stateConductor });
            }); 
        });
    });

    //Aviso al conductor que expiro el tiempo de espera 
    socket.on('avisoAbortarSolicitud', function(docs){
        if(typeof docs.tipo == 'undefined'){
            docs = JSON.parse(docs);
        }
        var tipo = docs.tipo; // 1=Tiempo - 2= Cancelar
        var idConductor = docs.idConductor;
        console.log('Abortar');

        var now = moment().tz("America/Lima").format();
        var arr = now.split('T');
        var diaHora = arr[0]+' '+arr[1].split('-')[0];
        //var diaHora = '2015-10-02 15:33:00';
        
        Conductors.findOneQuery({ idConductor: idConductor, $or:[{stateConductor: 4}, {stateConductor:3}] }, function(err, result){
            if(result){
                //Cancelar servicio
                Conductors.updateQuery({ idConductor: idConductor }, {$set:{idCliente: null, stateConductor:1, idDetalleServicio : null}}, function(err){
                    if(!err){ 
                        io.to(result.socket).emit('sendAvisoAbortoSolicitud', tipo); 
                        var dataInsertMovimiento = { idConductor:idConductor, latitudTaxi:0, longitudTaxi:0, estadoCond:1, estadoSolicitud:3, diaHora:diaHora, latitudCliente:0, longitudCliente:0, idDetalleServicio:747, idCliente:0};
                        //Insertando en moviminetos  en mysql
                        Conductors.insertMovimientos(dataInsertMovimiento, function(err){
                            if(err){ console.log('Error ->', err);
                            }else{ console.log('Solicitud de Servicio Registrado en mysql'); }
                        });
                    }
                })
            }
        });
    });


    //EDITADO
    //Recibe envio desde movil seleccionando a un conductor
    socket.on('aceptarServicioConductor', function(docs, callback){
        docs = (typeof docs.idConductor == 'undefined')? JSON.parse(docs) : docs;
        console.log('aceptarServicioConductor=>',docs);
        iniciarRecojo(docs, function(rpta){
            console.log(rpta, 'dato');
            callback(datoOcupado);
        });
        //var dato = iniciarRecojo(docs);
        //console.log(dato,'dato');
        //callback(dato);
        
        
    });


    //Iniciar servicio por parte del conductor
    socket.on('iniciarFinalizarServicioConductor', function(docs){
        console.log('iniciarFinalizarServicioConductor');
        if(typeof docs.tipo == 'undefined'){
            docs = JSON.parse(docs);
        }
        var tipo = docs.tipo; // 1=Iniciar , 2=Finalizar //obligatorio
        var idConductor = docs.idConductor; //obligatorio
        var now = moment().tz("America/Lima").format();
        var arr = now.split('T');
        var diaHora = arr[0]+' '+arr[1].split('-')[0];
        //var diaHora = '2015-10-02 15:33:00';
        var detalleId = 0;
        Conductors.findOneQuery({idConductor: idConductor}, function(err, resConductor){
            if(err){
            }else{
                if(resConductor){
                    var latitudTaxi = resConductor.coordinates[0];
                    var longitudTaxi = resConductor.coordinates[1];
                    var latitudCliente = resConductor.coordinates[0];
                    var longitudCliente = resConductor.coordinates[1];
                    if(resConductor){
                        if(tipo == 1 ){  //Inicia el servicio
                            if(resConductor.stateConductor == 3){
                                Conductors.updateQuery({idConductor: idConductor}, {$set: {stateConductor:2} }, function(err){
                                    if(err){
                                        console.log(err, 'Error iniciarFinalizarServicioConductor');
                                    }else{
                                        Clients.findOneQuery({idCliente: resConductor.idCliente }, function(err, respClient){
                                            if(err){
                                                console.log('ERROR -> iniciarFinalizarServicioConductor', err);
                                            }else{
                                                if(respClient){
                                                    if(respClient.stateAlert == 1 && respClient.alertEmail != ''){
                                                        app.mailer.send('emailInicializaServicio', {
                                                            to: respClient.alertEmail,
                                                            subject: 'Correo de Seguridad', // REQUIRED. 
                                                            idDetalleServicio: resConductor.idDetalleServicio 
                                                            //otherProperty: 'Other Property' // All additional properties are also passed to the template as local variables. 
                                                        }, function (err) {
                                                            if (err) {
                                                                console.log(err); return;
                                                            }else{
                                                                console.log('send email');
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        });

                                        var dataInsertMovimiento = { idConductor:idConductor, latitudTaxi:latitudTaxi, longitudTaxi:longitudTaxi, estadoCond:2, estadoSolicitud:5, diaHora:diaHora, latitudCliente:latitudCliente, longitudCliente:longitudCliente, idDetalleServicio:resConductor.idDetalleServicio , idCliente:resConductor.idCliente};
                                        //Insertando en moviminetos  en mysql
                                        Conductors.insertMovimientos(dataInsertMovimiento, function(err){
                                            if(err){ console.log('Error ->', err);
                                            }else{ console.log('Solicitud de Servicio Registrado en mysql'); }
                                        });
                                    }
                                });
                            }
                        }else{
                            if(resConductor.stateConductor == 2){
                                Conductors.updateQuery({ idConductor: idConductor }, {$set: {stateConductor:1, idCliente:null, idDetalleServicio: null} }, function(err){
                                    if(err){
                                        console.log(err, 'Error iniciarFinalizarServicioConductor 2');
                                    }else{
                                        io.sockets.emit('refreshCoordinatesWeb', {finished:true});
                                        Clients.findOneQuery({idCliente: resConductor.idCliente }, function(err, respClient){
                                            if(err){
                                                console.log('ERROR ->', err);
                                            }else{
                                                if(respClient){
                                                    if(respClient.stateAlert == 1 && respClient.alertEmail != ''){
                                                        app.mailer.send('emailFinalizoServicio', {
                                                            to: respClient.alertEmail,
                                                            subject: 'Mail Seguridad Finalizo'//, // REQUIRED. 
                                                            //otherProperty: 'Other Property' // All additional properties are also passed to the template as local variables. 
                                                        }, function (err) {
                                                            if (err) {
                                                                console.log(err); return;
                                                            }else{
                                                                console.log('send email');
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        });

                                        var dataInsertMovimiento = { idConductor:idConductor, latitudTaxi:latitudTaxi, longitudTaxi:longitudTaxi, estadoCond:2, estadoSolicitud:6, diaHora:diaHora, latitudCliente:latitudCliente, longitudCliente:longitudCliente, idDetalleServicio:resConductor.idDetalleServicio , idCliente:resConductor.idCliente};
                                        //Insertando en moviminetos  en mysql
                                        Conductors.insertMovimientos(dataInsertMovimiento, function(err){
                                            if(err){ console.log('Error ->', err);
                                            }else{ console.log('Solicitud de Servicio Registrado en mysql'); }
                                        });

                                    }
                                });
                            }
                        }

                        Clients.findOneQuery({idCliente : resConductor.idCliente}, function(err, resCliente){
                            if(err){
                                console.log('error',err);
                            }else{
                                if(resCliente){
                                    console.log('Send enviar iniciar', resCliente.socket);
                                    io.to(resCliente.socket).emit('iniciarFinalizarServicioConductor', tipo);
                                }
                            }
                        });
                    }  
                }
            }
        });
    });
});

