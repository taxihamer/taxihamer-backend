<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
<title>Page Not Found</title> 
<style type="text/css"> 
* { 
margin: 0; 
} 

html, body { 
height:100%; 
margin-left: 0px; 
background:url(../includes/img/csstricks.jpg) no-repeat fixed center;
} 

#missingimage { 
/*background: url(missingpage.jpg) no-repeat right;*/
width:100%; 
bottom:0; 
right:0; 
} 

#texto { 
font-family:Arial, Helvetica, sans-serif; 
font-size:1.5em; 
letter-spacing:-1px; 
padding:20px; 
text-align:justify; 
padding-top:40px; 
width:600px; 
margin: 0 auto;
} 
</style> 
</head> 

<body> 
<div id="missingimage"> 
<div id="texto"> 
<strong>La p&aacutegina que intent&oacute acceder no existe en este servidor. 
<br />Esta p&aacutegina no puede existir debido a las siguientes razones: </strong> 
<br /> <br /> 
<ol style="margin-top: 154px;"> 
<li>La URL que ha introducido en su navegador es incorrecta. Por favor, vuelva a introducir la direcci&oacuten URL e int&eacutentelo de nuevo. </li> 
<li>P&oacutengase en contacto con el propietario de este sitio web para informarles de esta situaci&oacuten.</li> 
</ol> 
</div> 
</div> 
</body> 
</html> 