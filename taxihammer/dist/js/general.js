$(document).ready(function(){	

	var b=$.AdminLTE.options.screenSizes;
	$(window).width()>b.sm-1?
		$("body").hasClass("sidebar-collapse")?
		$("body").removeClass("sidebar-collapse").trigger("expanded.pushMenu"):$("body").addClass("sidebar-collapse").trigger("collapsed.pushMenu"):$("body").hasClass("sidebar-open")?$("body").removeClass("sidebar-open").removeClass("sidebar-collapse").trigger("collapsed.pushMenu"):$("body").addClass("sidebar-open").trigger("expanded.pushMenu")
	//$("[data-toggle='offcanvas']").

	socket = io.connect(URL_CONTROLLER_SOCKET);
	socket.on('alertaEspera', function(data){

		var htmlsrv = '';
        var tipo = data.tipo;
        var obj = data.send;

        var alertaid = obj.alerta_id;

        var refreshIntervalAlerta = setInterval(function() {

			$.ajax({
			      url:'/BL/alertaController.php',
			      method: 'post',
			      data: {hidden_data_verif: 'hidden_data_verif', codigo:alertaid, tipo:1},
			      success: function(data) {
			      	console.log(data);
			      	if(data.status == 0){
			      		$("#dialog-general").dialog( "close" );
			      		document.getElementById('rington').pause();
			      		clearInterval(refreshIntervalAlerta);
			      	}
			      }
			 });

		 console.log('recargas id'+alertaid+'--->');

		}, 2000)

        	htmlsrv = '<div class="form-group" id="dataservice">'+
						'<h3>Servicio</h3>'+
						'<label for="servicio">Servicio: <span id="srvid"></span></label></br>'+
						'</div>'+
						'<div class="form-group" id="datacliente">'+
						'<h3>Cliente</h3>'+
						'<label for="nombrecli">Cliente: <span id="namcl"></span></label></br>'+
						'<label for="correocli">Correo: <span id="email"></span></label></br>'+
						'<label for="celulacli">Celular: <span id="cel"></span></label>'+
						'</div>'+
						'<div class="form-group" id="dataconductor">'+
						'<h3>Conductor</h3>'+
						'<label for="conductor">Conductor: <span id="cdt"></span></label></br>'+
						'<label for="correocdt">Correo: <span id="emailcdt"></span></label></br>'+
						'<label for="dnicdt">Dni: <span id="dni"></span></label></br>'+
						'</div>'+
						'<div class="form-group" id="dataunidad">'+
						'<h3>Vehiculo</h3>'+
						'<label for="unidadalias">Alias: <span id="ali"></span></label></br>'+
						'<label for="unidadplaca">Placa: <span id="pla"></span></label></br>'+
						'<label for="unidadmodel">Modelo: <span id="mdl"></span></label></br>'+
						'<label for="unidadmarca">Marca: <span id="mrc"></span></label></br>'+
						'<label for="unidadcolor">Color: <span id="clo"></span></label>'+
						'</div>';

        $("#details").html(htmlsrv);

	        $('#music').html('<audio src="'+URL_CONTROLLER_IMG+'files/Barco-sirena.mp3" controls autoplay loop id="rington"></audio>');

	        if(tipo == 1){
	        	$("#dataservice").html('');
	        	$("#dataconductor").html('');
	        	$("#dataunidad").html('');
	        	$("#namcl").html(obj.clienteNombre);
	        	$("#email").html(obj.clienteCorreo);
	        	$("#cel").html(obj.clienteCelular);
	        }else if(tipo == 2){
	        	$("#dataservice").html('');
	        	$("#datacliente").html('');
	        	$("#cdt").html(obj.conductorNombre);
	        	$("#emailcdt").html(obj.conductorCorreo);
	        	$("#dni").html(obj.conductorDni);
	        	$("#ali").html(obj.unidadAlias);
	        	$("#pla").html(obj.unidadPlaca);
	        	$("#mdl").html(obj.unidadModelo);
	        	$("#mrc").html(obj.unidadMarca);
	        	$("#clo").html(obj.unidadColor);
	        }else{
	        	$("#srvid").html(obj.servicio_id);
	        	$("#namcl").html(obj.clienteNombre);
	        	$("#email").html(obj.clienteCorreo);
	        	$("#cel").html(obj.clienteCelular);
	        	$("#cdt").html(obj.conductorNombre);
	        	$("#emailcdt").html(obj.conductorCorreo);
	        	$("#dni").html(obj.conductorDni);
	        	$("#ali").html(obj.unidadAlias);
	        	$("#pla").html(obj.unidadPlaca);
	        	$("#mdl").html(obj.unidadModelo);
	        	$("#mrc").html(obj.unidadMarca);
	        	$("#clo").html(obj.unidadColor);
	        }

	        $( "#dialog-general" ).dialog({
		    	width: 500,
		      	resizable: false,
		      	closeText: "hide",
		      	height: "auto",
		      	modal: true,
		      	dialogClass: "no-close",
		      	title: 'ALERTA',
		      	buttons: {
			        Aceptar: function() {

			        	$.ajax({
						      url:'/BL/alertaController.php',
						      method: 'post',
						      data: {hidden_data_verif: 'hidden_data_verif', codigo:alertaid, tipo:2},
						      success: function(data) {
						      	console.log(data);
						      	window.location = '../index.php?seccion=alertas';
						      }
						 });
						 	
			          $( this ).dialog( "close" );
			          document.getElementById('rington').pause();
			        }
		      }
			});
	});

	socket.on('alertaReservaPanel', function(data){
		var htmlReserva = '';
        var reserva_id = data.reserva_id;
        var cliente_nombre = data.cliente_nombre;
        var cliente_id = data.cliente_id;

        	htmlReserva = '<div class="form-group">'+
						  '<h3>'+cliente_nombre+'</h3>'+
						  '<p>Ha solicitado una reserva :' +reserva_id+' por favor Asignar un Conductor</p>'+
						  '</div>';

        $("#details_reserva").html(htmlReserva);
        $('#music').html('<audio src="'+URL_CONTROLLER_IMG+'files/Barco-sirena.mp3" controls autoplay loop id="rington"></audio>');

       	$( "#dialog-reseva_movil" ).dialog({
		    	width: 500,
		      	resizable: false,
		      	closeText: "hide",
		      	height: "auto",
		      	modal: true,
		      	dialogClass: "no-close",
		      	title: 'RESERVA SOLICITADA',
		      	buttons: {
			        Aceptar: function() {
			          $( this ).dialog( "close" );
			          document.getElementById('rington').pause();
			          window.location = '../index.php?seccion=nuevareserva';
			        }
		      }
		});
		
	});

	socket.on('alertaRecargaPanel', function(data){

		var recarga_id = data.recarga_id;
		var refreshIntervalId = setInterval(function() {

			$.ajax({
			      url:'/BL/recargasController.php',
			      method: 'post',
			      data: {hidden_data_verif: 'hidden_data_verif', codigo:recarga_id, tipo:1},
			      success: function(data) {
			      	if(data.status == 0){
			      		$("#dialog-recarga_movil").dialog( "close" );
			      		document.getElementById('rington').pause();
			      		clearInterval(refreshIntervalId);
			      	}
			      }
			 });

		 console.log('recargas id'+recarga_id+'--->');

		}, 2000)
        var conductor_id = data.conductor_id;
        var conductor_nombre = data.conductor_nombre;
        var recarga_monto = data.recarga_monto;
        var recarga_operacion = data.recarga_operacion;
        var recarga_fecha = new Date(data.recarga_fecha);
        var fechactual = recarga_fecha.getFullYear()+'-'+(recarga_fecha.getMonth()+1)+"-"+recarga_fecha.getDate();
		var htmlReserva = '';
        	 
htmlReserva = '<h3 style="text-align: left;">El Conductor : <span style="color: #0000ff;"><strong>'+conductor_nombre+'</strong></span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>&nbsp;N&ordm; '+recarga_id+'</strong></h3>'+
				'<p style="text-align: left;">Ha hecho una recarga el dia &nbsp;'+fechactual+' falta por Aprobar.</p>'+
				'<p style="text-align: left;"><strong>N&ordm; Operacion :</strong>&nbsp; '+recarga_operacion+'</p>'+
				'<p style="text-align: left;"><strong>Monto :</strong> s/ '+ recarga_monto.toFixed(2)+'</p>'+
				'<p style="text-align: center;">&nbsp;</p>';

        $("#details_recarga").html(htmlReserva);
        $('#music').html('<audio src="'+URL_CONTROLLER_IMG+'files/son_02.mp3" controls autoplay loop id="rington"></audio>');

       	$( "#dialog-recarga_movil" ).dialog({
		    	width: 500,
		      	resizable: false,
		      	closeText: "hide",
		      	height: "auto",
		      	modal: true,
		      	dialogClass: "no-close",
		      	title: 'RECARGA SOLICITADA',
		      	buttons: {
			        Aceptar: function() {

			        	$.ajax({
						      url:'/BL/recargasController.php',
						      method: 'post',
						      data: {hidden_data_verif: 'hidden_data_verif', codigo:recarga_id, tipo:2},
						      success: function(data) {
						      	console.log(data);
						      }
						 });

			        	$( this ).dialog( "close" );
			          document.getElementById('rington').pause();
			          window.location = '../index.php?seccion=recargas';
			        }
		      }
		});
		
	});
});

//Dialogo General de Confirmacion
function deleteGeneral(id,controller,callback){
    $.confirm({
      'title'   : 'Confirmar',
      'message' : 'Estás a punto de eliminar este elemento. <br /> No se podrá recuperar! ¿Desea continuar?',
      'buttons' : {
        'Yes' : {
          'class' : 'blue',
          'action': function(){
                $.ajax({
                    url: controller,
                    data: {
                        Gid: id,
                        delete_general: 'delete_general'
                    },
                    cache: false,
                    method: 'post'
                }).done(function (data) {
                    if(data.status == true){
                    	console.info('Ok');
                    	callback();
                   	}else{
                   		console.warn('Error revivar Controller', controller);
                   	}
                });
          }
        },
        'No'  : {
          'class' : 'gray',
          'action': function(){} 
        }
      }
    });
}

function KendoSettings() {
    this.url = '';
    this.rowTemplate = '';
    this.columns = '';
    this.page = 10;
    this.wrapper = '#grid';
    this.data = {};
    this.dataBound = function () {
    };
}

KendoSettings.prototype.setUrl = function (url) {
    this.url = url;
    console.log(url);
    return this;
};
KendoSettings.prototype.setRowTemplate = function (template) {
    this.rowTemplate = template;
    return this;
};
KendoSettings.prototype.setcolumns = function (columns) {
    this.columns = columns;
    return this;
};
KendoSettings.prototype.setPage = function (page) {
    this.page = page;
    return this;
};
KendoSettings.prototype.setWrapper = function (wrapper) {
    this.wrapper = wrapper;
    return this;
};

KendoSettings.prototype.setData = function (data) {
    this.data = data;
    return this;
}

KendoSettings.prototype.setDataBound = function (fnc) {
    this.dataBound = fnc;
    return this;
}


KendoSettings.prototype.render = function () {

    if (typeof window.kendo !== 'object') {
        alert('Kendo no ha sigo cargado aún');
        return;
    }

    var settings = {
        dataSource: {
            transport: {
                read: {url: this.url, dataType: 'json', type: 'GET', data: this.data}
            },
            schema: {data: 'data', total: 'total'},
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        scrollable: true,
        resizable: true,
        dataBound: this.dataBound,
        filterable: false,
        sortable: true,
        pageable: {pageSize: this.page, refresh: true, pageSizes: true},
        columns: this.columns

    }

    if (this.rowTemplate !== '') {
        settings.rowTemplate = kendo.template(this.rowTemplate);
    }


    $(this.wrapper).kendoGrid(settings);
};
