<?php
session_start();

include'includes/seguridad.php';
include'DAL/constantes.php';
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="<?=URL_CORREO;?>includes/img/logo_hammer.png" />
    <title><?=NAME_EMPRESA?> - Plataforma</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
       <!-- Dialog Confirm -->
    <link rel="stylesheet" href="<?=URL_CORREO?>plugins/confirm/jquery.confirm/jquery.confirm.css">
    <link rel="stylesheet" href="<?=URL_CORREO?>plugins/confirm/css/styles.css">
    <!-- Bootstrap 3.3.5 -->
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-switch.min.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-slider.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link href="dist/css/jqueyrui.autocomplete.custom.css" rel="stylesheet"/>
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>   
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
     <!-- Dialog Confirm -->
    <script src="<?=URL_CORREO?>plugins/confirm/jquery.confirm/jquery.confirm.js"></script>   
    <script src="<?=URL_CORREO?>plugins/confirm/js/script.js"></script>   
     <!-- End Dialog -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <script src="bootstrap/js/bootstrap-slider.min.js"></script>   
    <!--script src="http://maps.google.com/maps?file=api&v=3&key=ABQIAAAAFnFO3ySluSuha6r1vYekiRQNy_ewe8RNLHRa7cwLE-yWPUZNWBSV43OE1hhpvzIXRf04qzvWdmKBEw&sensor=false"></script-->
    <!--script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script-->
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?=KEY_PANEL_GOOGLE;?>&signed_in=true&libraries=geometry,places"></script>
    <script type="text/javascript" src="dist/js/constantes.js"></script>
    <script src="<?=IP_SOCKET?>socket.io/socket.io.js"></script>
    <script src="dist/js/general.js?version=<?=RANDOM_APP?>"></script>   
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <style type="text/css">
    .ui-dialog .ui-dialog-titlebar{
      background-color: red;
      color: #FFFFFF;
    }
  </style>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>T</b>H</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><?=NAME?> </b><?=EMPRESA?> </span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo $_SESSION['user']; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $_SESSION['user']; ?> - <?php echo $_SESSION['rol']; ?>
                      <small><?=NAME_EMPRESA?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="cambiar.php?mail=<?php echo $_SESSION['mail']; ?>" class="btn btn-default btn-flat">Cambio Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="includes/cerrarsesion.php" class="btn btn-default btn-flat">Cerrar Sesion</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <?php
          ## INCLUYE MENU PRINCIPAL ##
            include 'includes/menu.php';

          ?>

          
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 >
            <em><?=NAME_EMPRESA?> </em>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
          </ol>
        </section>

        <!-- Main content style="display:inline-block;" -->
        <section class="content">
          <?php
          ## INCLUYE SECCION PRINCIPAL ##
          if(!isset($_GET['seccion'])){
            include 'includes/start.php';
          }else{
            if($_GET['seccion']=="conductores"){
              include 'includes/conductores.php';
            }
            if($_GET['seccion']=="servicios"){
              include 'includes/tipos_servicio.php';
            }
            if($_GET['seccion']=="taxis"){
              include 'includes/taxis.php';
            }
            if($_GET['seccion']=="clientes"){
              include 'includes/clientes.php';
            }
            if($_GET['seccion']=="listazona"){
              include 'includes/zonas.php';
            }
            if($_GET['seccion']=="agregarzona"){
              include 'includes/agregar_zona.php';
            }
            if($_GET['seccion']=="tarifario"){
              include 'includes/tarifario.php';
            }
            if($_GET['seccion']=="reservas"){
              include 'includes/reservas.php';
            }
            if($_GET['seccion']=="nuevareserva"){
              include 'includes/nueva_reserva.php';
            }
            if($_GET['seccion']=="despacho"){
              include 'includes/despacho.php';
            }
            if($_GET['seccion']=="alertas"){
              include 'includes/alertas.php';
            }
            if($_GET['seccion']=="recargas"){
              include 'includes/recargas.php';
            }
            if($_GET['seccion']=="configuracion"){
              include 'includes/configuracion.php';
            }
            if($_GET['seccion']=="tarjeta"){
              include 'includes/tarjetas.php';
            }
            if($_GET['seccion']=="modulo"){
              include 'includes/modulos.php';
            }
            if($_GET['seccion']=="empresas"){
              include 'includes/empresas.php';
            }
            if($_GET['seccion']=="general"){
              include 'includes/general.php';
            }
            if($_GET['seccion']=="promo"){
              include 'includes/promocion.php';
            }
            if($_GET['seccion']=="canje"){
              include 'includes/canjes.php';
            }
            if($_GET['seccion']=="mapa"){
              include 'includes/mapaconductores.php';
            }
            if($_GET['seccion']=="movim"){
              include 'includes/movimientos.php';
            }
            if($_GET['seccion']=="alertas"){
              include 'includes/alertas.php';
            }
            if($_GET['seccion']=="ptos"){
              include 'includes/puntos.php';
            }
            if($_GET['seccion']=="stacta"){
              include 'includes/estadocuenta.php';
            }
            if($_GET['seccion']=="rangohoras"){
              include 'includes/configrango.php';
            }
            if($_GET['seccion']=="configreserva"){
              include 'includes/configreserva.php';
            }
            if($_GET['seccion'] == "apikey"){
               include 'includes/apis.php';
            }
            if($_GET['seccion'] == "configcorreo"){
               include 'includes/configCorreo.php';
            }
            if($_GET['seccion'] == "liquidacion"){
               include 'includes/liquidaciones.php';
            }
            if($_GET['seccion'] == "rptpuntos"){
              include 'includes/reporte_puntos.php';
            }
          }
          ?>    
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
        <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          <a href="http://tickets.tutaxidispatch.com/" target="_black">soporte@appslovers.com</a>
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2016 <a href="http://www.appslovers.com/view/" target="_black">Appslovers</a>.</strong> All rights reserved.
      </footer>
    <!-- /. alertas panel -->
    <div id="dialog-general" style="display:none;" class="container">
      <form role="form" id="details"></form>
    </div>
    <div id="music" style="display:none"></div>
    <!-- /. alertas panel -->

    <!-- /. alertas reserva -->
    <div id="dialog-reseva_movil" style="display:none;" class="container">
      <form role="form" id="details_reserva"></form>
    </div>
    <!-- /. alertas reserva -->

            <!-- /. alertas recarga -->
    <div id="dialog-recarga_movil" style="display:none;" class="container">
      <form role="form" id="details_recarga"></form>
    </div>
    <!--audio src=""></audio-->
    <!-- REQUIRED JS SCRIPTS -->

    
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/bootstrap-switch.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
<link rel="stylesheet" href="bootstrap/css/angular-ui-switch.css"/>
<link data-require="datatable-css@1.10.7" data-semver="1.10.7" rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" />
<!--link rel="stylesheet" href="plugins/kendo/css/kendo.default.min.css"/>
<link rel="stylesheet" href="plugins/kendo/css/kendo.common.min.css"/-->
<link rel="stylesheet" href="//kendo.cdn.telerik.com/2015.2.902/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="//kendo.cdn.telerik.com/2015.2.902/styles/kendo.material.min.css" />
<script src="plugins/datatables/jquery.dataTables.min.js"></script> 

<!--script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://code.angularjs.org/1.4.5/angular-route.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular-resource.min.js"></script-->

<!--script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-resource.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-route.min.js"></script-->

<script src="plugins/Angular/angular.min.js"></script> 
<script src="plugins/Angular/angular-route.js"></script> 
<script src="plugins/Angular/angular-resource.min.js"></script>

<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="plugins/Angular/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!--script src='https://rawgit.com/ghostbar/angular-file-model/master/angular-file-model.js'></script-->
<script src="plugins/Angular/angular-file-model.js"></script>
<script src="plugins/Angular/angular-datatables.min.js"></script>
<script src="plugins/Angular/angularjs-google-maps.js"></script>
<!--script src="http://jvandemo.github.io/angularjs-google-maps/dist/angularjs-google-maps.js"></script-->

<script src="plugins/Angular/dataTables.columnFilter.js"></script>
<script src="plugins/Angular/angular-datatables.columnfilter.min.js"></script>

<script src="plugins/Angular/app.js?version=<?=RANDOM_APP?>"></script>
<script src="plugins/Angular/appApi.js"></script>
<script src="plugins/Angular/appCorreo.js"></script>
<script src="plugins/Angular/appPuntos.js"></script>

<!--script src="plugins/kendo/js/kendo.all.min.js"></script-->
<script src="//kendo.cdn.telerik.com/2015.2.902/js/kendo.all.min.js"></script>

  </body>
</html>
