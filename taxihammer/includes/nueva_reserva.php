<?php
include'seguridad.php';

require_once'DAL/reservaDAO.php';
require_once'DAL/constantes.php';

$reservaDAO = new reservaDAO();
$ConfigHoras = $reservaDAO->listaConfigreservaActivo(); 

$generalDAO = new generalDAO();
$config = $generalDAO->lista();

?>
<style type="text/css">

	.ui-dialog .ui-dialog-titlebar-close{
		display: none!important;
	}

    #divContador {
        font-size: 65px;
        text-align: center;
        margin-top: 40px;
        padding-left: 10px;
        padding-right: 10px;
        font-family: Tahoma;
    }
    
    table {
    	border-collapse: collapse;
    	border-spacing: 0;
    	width: 100%;
	}	

</style>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">

<!-- <script src="<?=URL_CORREO?>bootstrap/js/dataTables.responsive.min.js"></script> 
<script src="<?=URL_CORREO?>bootstrap/js/responsive.bootstrap.min.js"></script> -->

<div class="row" ng-app="myapp" ng-controller="tasksController">
	<div class="col-md-3">
	<!--div class="col-md-3" ng-controller="tasksController"-->
		<div class="box box-primary">
	      <div class="box-header with-border">
	        <h3 class="box-title">Nueva Reserva</h3>
	      </div>
	      <input type="hidden" name="configId" id="configId" value="<?=$config[0]['tipo_tarifa']?>">
	      <form role="form" name="reservaForm" id="reservaForm">
	      	<input type="hidden" ng-model="cliente_id">
	      	<input type="hidden" name="confighoras" id="confighoras" value="<?=$ConfigHoras[0]['configreserva_horas']?>">
	        <div class="box-body">
	         	<div class="form-group">
	         		<label for="celular">Fecha de Reserva </label>
	                <div class='input-group date' id='datetimepicker2'>
	                    <input type='text' class="form-control" width="350" ng-model="cliente_fecha"/>
	                    <span class="input-group-addon">
	                        <span class="glyphicon glyphicon-calendar"></span>
	                    </span>
	                </div>
            	</div>
	          <div class="form-group">
	            <label for="celular">Ingrese Nº Celular / Nombre </label>
	            <input type="text" ng-model="cliente_cel" class="form-control" name="celular" id="celular" placeholder="Ingrese Nº Celular / Nombre " required>
	          </div>
	          <div class="form-group">
	            <label for="nombres">Cliente</label>
	            <input type="text" ng-model="cliente_nombre" class="form-control" name="cliente" placeholder="Cliente" id="cliente" required>
	            <input type="hidden" ng-model="tipocliente">
	          </div>
	           <div class="form-group">
	            <label for="correo">Correo del Cliente</label>
	            <input type="email" ng-model="cliente_correo" class="form-control" name="correo" id="correo" placeholder="Correo" required>
	          </div>
	          <div class="form-group">
	            <label for="sercicio">Tipo Servicio</label>
				    <select ng-model="item" class="form-control" ng-change="selectAction()" id="tiposervicioid">
				    	<option value="">-- Seleccione tipo --</option>
						<option ng-repeat="x in myOptions" value="{{x.tiposervicio_id}}">{{x.tiposervicio_nombre}}</option>
					</select>
          	  </div>
	          <div class="form-group">
	            <label for="sercicio">Tipo Pago</label>
	            <select class="form-control" name="tipopago" ng-model="tipopago">
	            	  <option value=""> Seleccione Pago </option>
		              <option value="1"> Efectivo </option>
		              <option value="2"> Vale </option>
		        </select>
          	  </div>
	          <div class="form-group">
	            <label for="nombres">Dirección Origen</label>
	            <input id="fromorigen" type="text" ng-model="org.origen" placeholder="Referencia de Origen" class="form-control ng-pristine ng-valid" autocomplete="off" required>
	            <input type="hidden" ng-model="org.latorg">
			    <input type="hidden" ng-model="org.lngorg">
	            <input type="hidden" ng-model="idorg">
	            <input type="hidden" id="reforilatresr">
	            <input type="hidden" id="reforilngresr">
	          </div>
	          <div class="form-group">
	            <label for="nombres">Dirección Destino</label>
	            <input id="fromdestino" type="text" ng-model="des.destino" placeholder="Referencia de Destino" class="form-control ng-pristine ng-valid" autocomplete="off" required>
	            <input type="hidden" ng-model="des.latdes">
			    <input type="hidden" ng-model="des.lngdes">
	            <input type="hidden" ng-model="iddes">
	            <input type="hidden" id="refdeslatresr">
	            <input type="hidden" id="refdeslngresr">
	          </div>
	          <div class="form-group">
	            <label for="costo">Costo</label>
	            <input type="number" ng-model="recosto" class="form-control" name="costo" placeholder="Costo" id="costo" required>
	          </div>
	          <div class="form-group">
	            <label for="sercicio">Conductor</label>
				    <select ng-model="selectedConductor" class="form-control">
				    	<option value="">-- Seleccione Conductor --</option>
						<option ng-repeat="x in myConductor" value="{{x.conductor_id}}">{{x.conductor_nombre}}</option>
					</select>
          	  </div>
	        </div>

	        <div class="box-footer">
	          	<button type="submit" class="btn btn-primary" ng-click="addTask()">Guardar Reserva</button>
	        </div>
	      </form>
	       <div mhi-dialog 
			            title="{{titleform}}"                
			            open="{{dialogOpenform}}"
			            ok-button="OK"
			            ok-callback="handleOk"
			            cancel-button="Cancel"
			            cancel-callback="handleCancel">
			            Message: {{message2}}
        	</div>
	    </div>
	</div>
	<div class="col-md-9">
	  <div class="box box-primary" >
			<div class="box-header with-border">
				<h3 class="box-title">Listado de Reservas</h3>
			</div>



			<div >
					<div class="col-md-12">
				<div class="col-md-3">
					<label>Estado</label>
					<select class="form-control" ng-model="filtro.estado">
 	 					<option value="todos" selected>Todos</option>
 	 					<option value="0">No Asignado</option>
						<option value="1">Asignado</option>
 	 					<option value="2">Enviado</option>
 	 					<option value="3">Atendida</option>
					</select>
				</div>

				<div class="col-md-2">
					<label>Fecha Desde:</label>
				    <div class='input-group date'>
                    <input type='text' class="form-control datetime" ng-model="filtro.fecha.desde" id ="desde" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                </div>


				<div class="col-md-2">
					<label>Fecha Hasta:</label>
				    <div class='input-group date'>
                    <input type='text' class="form-control datetime"  ng-model="filtro.fecha.hasta" id="hasta" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                	</div>                

				</div>


				<div class="col-md-3">
					<label>Horas</label>
					<select class="form-control" ng-model="filtro.hora">
						<option value="null">--</option>
 	 					<option value="1">1 Hora</option>
 	 					<option value="2">2 Horas</option>
						<option value="3">3 Horas</option>
 	 					<option value="4">4 Horas</option>
 	 					<option value="5">5 Horas</option>
					</select>
				</div>

				<div class="col-md-2">
					<button ng-click="filtro.buscar()" class="btn btn-facebook">Buscar</button>
				</div>


			</div>

			</div>
			
		  	<br>
			    <!--div ng-controller="MyCtrl"-->
			    	<div class="table-responsive" style="display: inline-block; width: 100%;">
				      <table datatable="ng" dt-options="dtOptions" class="table table-hover" >
				        <thead>
				            <tr>
				                <th>No.</th>
				                <th>Nom Cliente</th>
				                <th>Nom Conductor</th>
				                <th>Fecha de Reserva</th>
				                <th>Origen</th>
				                <th>Destino</th>
				                <th>Tipo Servicio</th>
				                <th>Tipo Pago</th>
				                <th>Tarifa</th>
				                <th>Estado</th>
				                <th>Actions</th>
				            </tr>
				        </thead>
				        <tbody>
				            <tr ng-repeat="user in users">
				                <td>{{$index + 1}}</td>
				                <td>{{user.NombreCliente}}</td>
				                <td>{{user.NombreConductor}}</td>
				                <td>{{user.reserva_fecha}}</td>
				                <td>{{user.dir_origen}}</td>
				                <td>{{user.dir_destino}}</td>
				                <td>{{user.tiposervicio_nombre}}</td>
				                <td>{{user.tipo_pago}}</td>
				                <td>S/. {{user.tarifa}}</td>
				                <td>{{user.reserva_estado_text}}</td>
				                <td>
				                	<button ng-if="0==user.reserva_estado" class="btn btn-warning" ng-click="editMyCtrl(user.reserva_id,user.reserva_fecha,user.tipo_pago,user.tarifa,user.tiposervicio_id)">
						            <i class="fa fa-edit"></i>
						            </button>
						            <a href="#" ng-if="0==user.reserva_estado" class="btn btn-danger" ng-click="deleteMyCtrl(user.reserva_id)">
						            <i class="fa fa-trash-o"></i></a>
				                </td>
				            </tr>
				        </tbody>
				    </table>
				    <div mhi-dialog 
			            title="{{title}}"                
			            open="{{dialogOpen}}"
			            reserva="{{reservaid}}"
			            ok-button="OK"
			            ok-callback="handleOkK"
			            cancel-button="Cancel"
			            cancel-callback="handleCancell">
			            Message: ({{message1}})
        			</div>
        			<modal title="Asignar Conductor" visible="showModal" style="display:none;" id="modal">
             			<form class="form-horizontal" role="form" name="reservaUpdateForm">
             				<input type="hidden" ng-model="idpopupser">
             				<div class="alert alert-danger" id="msgError">
							  <button type="button" class="close" ng-click="Close()">&times;</button>
							  <strong>¡Cuidado!</strong> {{messageError}}
							</div>
             				<div class="form-group">
				            	<label for="sercicio" class="control-label col-sm-3" >Fecha de Reserva</label>
				            	<div class="col-sm-9" style="padding-top:9px;"> 
								    {{fechapopup}}	
								</div>
			          	  	</div>
			          	  	<div class="form-group">
				            	<label for="sercicio" class="control-label col-sm-3" >Tipo de Pago</label>
				            	<div class="col-sm-9" style="padding-top:9px;"> 
								    {{tpagopopup}}	
								</div>
			          	  	</div>
			          	  	<div class="form-group">
				            	<label for="sercicio" class="control-label col-sm-3" >Tarifa</label>
				            	<div class="col-sm-9" style="padding-top:9px;"> 
								    {{tarifpopup | currency:"s/ " }}	
								</div>
			          	  	</div>
						   <div class="form-group">
				            	<label for="sercicio" class="control-label col-sm-3" >Tipo Servicio</label>
				            	<div class="col-sm-9">
								    <select required ng-model="SimpleSelectedData" class="form-control" ng-change="selectActionPopup()" id="tiposervicioidpopup">
								    	<option value="" ng-selected="true">-- Seleccione tipo --</option>
										<option ng-repeat="x in myOptionspopup" value="{{x.tiposervicio_id}}" ng-selected="{{x.tiposervicio_id === SimpleSelectedData}}">{{x.tiposervicio_nombre}}</option>
									</select>
								</div>
			          	  </div>
						  <div class="form-group">
				            <label for="sercicio" class="control-label col-sm-3" >Conductor</label>
				            <div class="col-sm-9"> 
							    <select required ng-model="selectedConductorUpdate" class="form-control" id="conductorupdate">
							    	<option value="">-- Seleccione Conductor --</option>
									<option ng-repeat="x in myConductorpopup" value="{{x.conductor_id}}">{{x.conductor_nombre}}</option>
								</select>
							</div>
			          	  </div>
						  <div class="form-group"> 
						    <div class="col-sm-offset-2 col-sm-10 ">
						      <button type="submit" class="btn btn-primary pull-right" ng-click="uppReserva()">Submit</button>
						    </div>
						  </div>
						</form>
          			</modal>
          			<div id="dialog-confirm-angular" title="Empty the recycle bin?" style="display:none;">
					 	<div id="divContador">
						    20
					    </div>
					</div>
				</div>
		</div>
    </div>
</div>
<script src="<?=IP_SOCKET?>socket.io/socket.io.js"></script>
<script type="text/javascript">
var Rptadocs = false;
var seleccionado;
var SessionServiceId;
$(function() {

	$('#datetimepicker2').datetimepicker();

	$('.datetime').datetimepicker({
		format : 'YYYY/MM/DD'
	});

	$('.alert').hide();

	socket = io.connect(URL_CONTROLLER_SOCKET);
	socket.on('connect', function(sockett){
		console.log('socket web'+socket.id);
		socket.emit('SocketWeb',{socketweb: socket.id});
	});

	socket.on('SolicitudAceptarReserva',function(docs){
		console.log('respondio Javier Quiroz Reserva 222');
		console.log(docs);
		seleccionado = docs;
		Rptadocs = true; 
	});

});	
</script>
<style>
	.spanc{
		margin-right: 5px;
		margin-left: 5px;
	}
	#DataTables_Table_0_filter{
		display: none !important;
	}
</style>
<link data-require="datatable-css@1.10.7" data-semver="1.10.7" rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" />
<!--script src="plugins/datatables/jquery.dataTables.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<script src="https://code.angularjs.org/1.4.5/angular-route.js"></script> 
<script src="plugins/Angular/angular-datatables.min.js"></script>
<script src="http://jvandemo.github.io/angularjs-google-maps/dist/angularjs-google-maps.js"></script>
<script src="plugins/Angular/app.js"></script>

