

<style type="text/css">
  .global-12{padding-bottom: inherit;border: inherit;}
  .openclose{display: none;}
  .nullOld{display: none;text-align: center;color: #e23558;}
  #dialog-delete .ui-dialog-titlebar-close{display: none;}
  #textconfirma {
      font-size: 20px;
      text-align: center;
      margin-top: 40px;
      padding-left: 10px;
      padding-right: 10px;
      font-family: Tahoma;
  }
  table td{ cursor: pointer; }
</style>
<div class="row" ng-app="myappApi" ng-controller="apisController">
  <div class="col-md-4">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Crear Api Key</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" id="valForm" ng-submit="saveApis()">
        <div class="box-body">
          <div class="row show-grid">
            <div class="col-md-12 global-12">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="key_web">Api Google Web</label>
                     <input type="text" class="form-control" name="key_web" ng-model="key_web" placeholder="Ingrese key de api google maps" required>
                  </div>
                </div>
              </div>
            </div>
          </div> 

          <div class="row show-grid">
            <div class="col-md-12 global-12">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="key_android">Api Google  Android</label>
                     <input type="text" class="form-control" name="key_android" ng-model="key_android" placeholder="Ingrese key android de api google maps" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="key_ios">Api Google IOS</label>
                     <input type="text" class="form-control" name="key_ios" ng-model="key_ios" placeholder="Ingrese key ios de api google maps" required>
                  </div>
                </div>
              </div>
            </div>
          </div> 

          <div class="row show-grid">
            <div class="col-md-12 global-12">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="key_foursquare">Api Foursquare  key</label>
                     <input type="text" class="form-control" name="key_foursquare" ng-model="key_foursquare" placeholder="Ingrese key del fourquare" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="secret_foursquare">Api Foursquare  Secret</label>
                     <input type="text" class="form-control" name="secret_foursquare" ng-model="secret_foursquare" placeholder="Ingrese secret del foursquare" required>
                  </div>
                </div>
              </div>
            </div>
          </div> 

          <div class="row show-grid">
            <div class="col-md-12 global-12">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="key_here">Api Here  key</label>
                     <input type="text" class="form-control" name="key_here" ng-model="key_here" placeholder="Ingrese key del here" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="secret_here">Api Here  Secret</label>
                     <input type="text" class="form-control" name="secret_here" ng-model="secret_here" placeholder="Ingrese secret del here" required>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row show-grid">
            <div class="col-md-12 global-12">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="inputinicio">Periodo de Inicio : </label>
                    <input id="inputinicio" class="form-control" type="month" name="inputinicio" ng-model="inicio.value" placeholder="yyyy-MM" min="" max="" required />
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="inputfinal">Periodo de Final : </label>
                   <input id="inputfinal" class="form-control" type="month" name="inputfinal" ng-model="final.value" placeholder="yyyy-MM" min="" max="" required />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <input type="submit" id="submit" value="Crear Api" class="submitbtn btn btn-primary" />
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-8">
    <div class="box box-primary">
      <div class="alert alert-danger openclose" id="msgErrorApi">
        <button type="button" class="close" ng-click="CloseApi(1)">&times;</button>
        <strong>Error!</strong> {{messageApi}}
      </div>
      <div class="alert alert-success openclose" id="msgSuccessApi">
        <button type="button" class="close" ng-click="CloseApi(2)">&times;</button>
        <strong>¡Éxito!</strong> {{messageApi}}
      </div>
      <div class="alert alert-warning openclose" id="msgErrorValid">
        <button type="button" class="close" >&times;</button>
        <strong>¡Cuidado!</strong> {{messageApi}}
      </div>
      <section id="main-content" class="animated fadeInRight">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title"> Listado de Apis Google , Foursquare , Here </h3>
                      </div>
                      <div class="panel-body">
                        <div class="dataTables_wrapper form-inline" role="grid">                      
                          <div style="overflow-x:auto;">
                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                              <thead>
                                  <tr>
                                      <th>Api Google Web</th>
                                      <th>Api Google Android</th>
                                      <th>Api Google IOS</th>
                                      <th>Api Foursquare Key</th>
                                      <th>Api Foursquare Secret</th>
                                      <th>Api Here Key</th>
                                      <th>Api Here Secret</th>
                                      <th>Fecha Inicio</th>
                                      <th>Fecha Final</th>
                                      <th>Estado</th>
                                  </tr>
                              </thead>

                              <tbody>
                                  <tr ng-repeat = "api in apis | orderBy:columnToOrder:reverse ">
                                    <td title="{{api.api_google_web}}">{{api.api_google_web | cortarTexto:10}}</td>
                                    <td title="{{api.api_google_android}}">{{api.api_google_android | cortarTexto:10}}</td>
                                    <td title="{{api.api_google_ios}}">{{api.api_google_ios | cortarTexto:10}}</td>
                                    <td title="{{api.api_foursquare_key}}">{{api.api_foursquare_key | cortarTexto:10}}</td>
                                    <td title="{{api.api_foursquare_secret}}">{{api.api_foursquare_secret | cortarTexto:10}}</td>
                                    <td title="{{api.api_here_key}}">{{api.api_here_key | cortarTexto:10}}</td>
                                    <td title="{{api.api_here_secret}}">{{api.api_here_secret | cortarTexto:10}}</td>
                                    <td>{{api.api_fecha_create}}</td>
                                    <td>{{api.api_fecha_fin}}</td>
                                    <td>
                                      <switch name="enabled" ng-model="api.enabled" ng-change="changeCallback(api.api_id, $index + 1)" on="on" off="off"></switch>
                                    </td>
                                  </tr>
                                  <tr class="nullOld" id="nullOld">
                                    <td colspan="9">No hay datos a mostrar</td>
                                  </tr>
                              </tbody>
                            </table>
                            <div class="row" ng-class="{openclose: (totalItems == 0)}">
                                <div class="col-sm-6">
                                    <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="dataTables_paginate paging_simple_numbers">
                                      <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                                    </div>
                                </div>
                            </div>
                          </div>                      
                        </div>
                      </div>
                    </div>
                </div>
            </div>
      </section>
    </div>
  </div>
  <!-- Modal in Angular -->
    <div class="modal" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Editar Grupo : <b>{{fechagrupo | date:'medium' }}</b></h4>
          </div>
          <form role="form" method="post" id="formuploadajax">
            <input type="hidden" name="grupo_id" id="grupo_id">
            <input type="hidden" ng-model="indexEdit">
            <div class="modal-body">
              <div class="box-body">

                <div class="row show-grid">
                  <div class="col-md-12 global-12">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <label for="grupo">Grupo</label>
                            <input type="text" class="form-control" name="grupo" ng-model="Editgrupo" placeholder="Ingrese nombre de grupo" required>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 

                <div class="row show-grid">
                  <div class="col-md-12 global-12">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <label for="countgrupo">Cantidad de Conductores</label>
                            <input type="number" class="form-control" name="countgrupo" ng-model="Editcountgrupo" placeholder="Ingrese cantidad de conductores" required>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary" ng-click="updateGrupos()">Guardar Cambios</button>
            </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Modal in Angular -->
</div>

<div id="dialog-delete" style="display:none;">
  <p><span style="float:left; margin:12px 12px 20px 0;">
  </span><label id="textconfirma">Estas seguro de eliminar el Grupo </label></p>
</div>