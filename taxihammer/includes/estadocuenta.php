<style>
  .contact-info-form {
    list-style-type: none;
    margin: 30px 0;
    padding: 0;
  }

  .contact-info-form li {
    margin: 10px 0;
  }

  .contact-info-form label {
    display: inline-block;
    width: 100px;
    text-align: right;
    font-weight: bold;
  }
</style>
<div id="example" ng-app="myapp" ng-controller="stactaController">

  
    <div class="row">
        <div class="col-md-12" style="margin-left:20px;">
            <label>Exportar : </label>
            <form role="form" action="../BL/exportarController.php" enctype="multipart/form-data" method="post" style="width: 30%;
              display: inline-block;">
              <input type="hidden" name="hidden_exportar" value="1">
              <input type="hidden" name="exportar" value="7">
              <input type="hidden" name="hsearch" value="{{searchText}}">
              <button type="submit" class="btn btn-primary btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
            </form>
        </div>
      </div>

      <br> 


     <div class="row">
      <div class="col-sm-12">
          <div class="dataTables_filter">
              <label style="float: right;"><input type="search" class="form-control input-sm ng-pristine ng-valid ng-touched" aria-controls="example" ng-model="searchText" placeholder="Buscar:" ng-change="SearchTextby()"  ></label>
          </div>
      </div>
    </div>



    <div>
        <kendo-grid options="mainGridOptions" k-data-source="gridDataSource">
            <div k-detail-template>
                <div kendo-grid k-options="detailGridOptions(dataItem)"></div>
            </div>
        </kendo-grid>
    </div>
</div>