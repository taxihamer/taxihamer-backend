<?php
  include'seguridad.php';
?>
<div class="row" ng-app="myapp">
  <div class="container">
    <div class="col-sm-9">
      <div class="widget-box" id="recent-box" ng-controller="ptosController">
      <div class="widget-header header-color-blue">
      <div class="row">
      <div class="col-sm-6">
      <h4 class="bigger lighter">
        <i class="glyphicon glyphicon-align-justify"></i>&nbsp;
        Configuracion de Puntos
      </h4>
      </div>
      <div class="col-sm-3">
      <button ng-click="addNewClicked=!addNewClicked;" class="btn btn-sm btn-danger header-elements-margin"><i class="glyphicon  glyphicon-plus"></i>&nbsp;Agregar</button>
      </div>
      <!--div class="col-sm-3">

      <input type="text" ng-model="filterTask" class="form-control search header-elements-margin" placeholder="Filtrar ptos">

      </div-->
      </div></div>
      <div class="widget-body ">
      <form ng-init="addNewClicked=false; " ng-if="addNewClicked" id="newTaskForm" class="add-task">
        <div class="form-actions" >
          <div class="input-group" style="width:100%;">
             <label for="sercicio">Cliente</label>
            <select class="form-control" name="tipoUsuario" ng-model="tipoUsuario">
              <option value=""> Seleccione Usuario </option>
              <option value="1"> Cliente </option>
            </select><br>
            <label for="sercicio">Puntos Cliente</label>
            <input type="number" class="form-control" name="comment" ng-model="ptos" placeholder="Agregar puntos" ng-focus="addNewClicked"><br>
            <label for="sercicio">Cliente Valor $</label>
            <input type="number" class="form-control" name="comment" ng-model="valor" placeholder="Agregar valor monetario">
            <!---div class="input-group-btn">
              <button class="btn btn-default" type="submit" ng-click="addPuntos(ptos,valor,tipoUsuario)"><i class="glyphicon glyphicon-plus"></i>&nbsp;Agregar</button>
            </div-->
          </div>
        </div>
        <p></p>
        <div class="form-actions" >
          <div class="input-group" style="width:100%;">
             <label for="sercicio">Conductor</label>
            <select class="form-control" name="tipoUsuario" ng-model="tipoUsuariocdt">
              <option value=""> Seleccione Usuario </option>
              <option value="2"> Conductor </option>
            </select><br>
            <label for="sercicio">Puntos Conductor</label>
            <input type="number" class="form-control" name="comment" ng-model="ptoscdt" placeholder="Agregar puntos" ng-focus="addNewClicked"><br>
            <label for="sercicio">Conductor Valor $</label>
            <input type="number" class="form-control" name="comment" ng-model="valorcdt" placeholder="Agregar valor monetario">
            <!--div class="input-group-btn">
              <button class="btn btn-default" type="submit" ng-click="addPuntos(ptos,valor,tipoUsuario)"><i class="glyphicon glyphicon-plus"></i>&nbsp;Agregar</button>
            </div-->
          </div>
        </div>
        <div class="input-group-btn">
              <button class="btn btn-default" type="submit" ng-click="addPuntos(ptos,valor,tipoUsuario,ptoscdt,valorcdt,tipoUsuariocdt)"><i class="glyphicon glyphicon-plus"></i>&nbsp;Agregar</button>
        </div>
      </form>
      <div class="task">
        <!--label class="checkbox" ng-repeat="task in tasks | filter : filterTask">
          <input 
        type="checkbox"
        value="{{task.puntos_estado}}"
        ng-checked="task.puntos_estado==1"
        ng-click="toggleStatus(task.puntos_id,task.puntos_estado, task.TASK)"/> 
        <span ng-class="{strike:task.puntos_estado==1}">Conductor Puntos : {{task.puntos_conductor}} Conductor Valor : {{task.puntos_factor_conductor}}</span>
        <span ng-class="{strike:task.puntos_estado==1}">Cliente Puntos : {{task.puntos_cliente}} Cliente Valor : {{task.puntos_factor_cliente}}</span>
        <a ng-click="deleteTask(task.puntos_id)" class="pull-right"><i class="glyphicon glyphicon-trash"></i></a>
        </label-->
        <table datatable="ng" dt-options="dtOptions" class="table table-hover" >
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Tipo Usuario</th>
                      <th>Puntos 1</th>
                      <th>Valor 1</th>
                      <th>Tipo Usuario</th>
                      <th>Puntos 2</th>
                      <th>Valor 2</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                  <tr ng-repeat="task in tasks">
                      <td><input 
                          type="checkbox"
                          value="{{task.puntos_estado}}"
                          ng-checked="task.puntos_estado==1"
                          ng-click="toggleStatus(task.puntos_id,task.puntos_estado)"/></td>
                      <td>{{task.cliente}}</td>
                      <td>{{task.puntos_cliente}}</td>
                      <td>{{task.puntos_factor_cliente}}</td>
                      <td>{{task.conductor}}</td>
                      <td>{{task.puntos_conductor}}</td>
                      <td>{{task.puntos_factor_conductor}}</td>
                      <td>
                        <a href="#" class="btn btn-danger" ng-click="deletePtos(task.puntos_id)">
                        <i class="fa fa-trash-o"></i></a>
                      </td>
                  </tr>
              </tbody>
          </table>
      </div>
      </div>
      </div>
    </div>
  </div>
</div>

<link data-require="datatable-css@1.10.7" data-semver="1.10.7" rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" />
<script src="plugins/datatables/jquery.dataTables.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<script src="https://code.angularjs.org/1.4.5/angular-route.js"></script> 
<script src="plugins/Angular/angular-datatables.min.js"></script>
<script src="plugins/Angular/app.js"></script>