<?php
include'seguridad.php';
require_once'DAL/usuarioDAO.php';
$data['rol_id'] = $_SESSION['rol_id'];
$data['mail'] = $_SESSION['mail'];
$usuarioDAO = new usuarioDAO();
$result = $usuarioDAO->listacoordenadas($data);

?>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css">

<style>
  #mapMov{
    height:600px;
    width:100%;
  }
  #mapMov img{
    max-width:none!important;
    background:none!important;
  }
  #modal .modal-content { height: 620px !important; }

    .warning{color: red;}
  .ok{color: green;}

            table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
}

</style>

<div class="row" ng-app="myapp">
	<div class="col-md-12">
	  <div class="box box-primary" >
			<div class="box-header with-border">
				<h3 class="box-title">Listado de servicios</h3>
			</div>
			 <div class="row">
        		<div class="col-md-12" style="margin-left:20px;">
          			<label>Exportar : </label>
				 	<form role="form" action="../BL/exportarController.php" enctype="multipart/form-data" method="post" style="width: 30%;
		            	display: inline-block;">
		            	<input type="hidden" name="hidden_exportar" value="1">
		            	<input type="hidden" name="exportar" value="3">
		            	<button type="submit" class="btn btn-primary btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
		          	</form>
		        </div>
			</div>
		  	<br>
			    <div ng-controller="movementsController" class="table-responsive" style="display: inline-block;width:100%;" >
				      <table datatable="ng" dt-options="dtOptions" dt-columns="dtColumns" class="table table-hover" >
				        <thead>
				            <tr>
				            	<th>Indice</th>
				                <th>Fecha</th>
				                <th>H. Inicio</th>
				                <th>H. Fin</th>
				                <th>Nom. Cliente</th>
				                <th>Usuario</th>
				                <th>Origen</th>
				                <th>Destino</th>
				                <th>Tarifa</th>
				                <th>Adicionales</th>
				                <th>Tarifa Final</th>
				                <th>Tipo Pago</th>
				                <th>Nom. Conductor</th>
				                <th>Estado </th>
				                <th>Actions</th>
				            </tr>
				        </thead>
				        <tbody>
				            <tr ng-repeat="movimientos in movimiento  | orderBy:columnToOrder:reverse">
				            	<td>{{$index + 1}}</td>
				                <td style="width:6%;">{{movimientos.fecha_seleccion}}</td>
				                <td>{{movimientos.Inicio}}</td>
				                <td>{{movimientos.Fin}}</td>
				                <td style="width:10%;">{{movimientos.NombreCliente}}</td>
				                <td>{{movimientos.tipo_descripcion}}</td>
				                <td style="width:16%">{{movimientos.origen_nombre}}</td>
				                <td>{{movimientos.destino_nombre}}</td>
				                <td>{{movimientos.tarifa | currency:"s/ ":0}}</td>
				                <td>{{movimientos.adicionales | currency:"s/ ":0}}</td>
				                <td>{{movimientos.tarifa + movimientos.adicionales | currency:"s/ ":0}}</td>
				                <td>{{movimientos.tipo_pago}} {{movimientos.servicio_vale}}</td>
				                <td>{{movimientos.NombreConductor}}</td>
				                <td ng-class="{warning: (movimientos.estado == 10), ok: (movimientos.estado == 6)}" >{{movimientos.estadoservice}}</td>
				                <td style="width: 6%;">
				                	<button class="btn btn-info" ng-click="ModalView(movimientos.servicio_id)">
						            <i class="glyphicon glyphicon-list-alt"></i>
						            </button>
						            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal" ng-click="ModalMovim(movimientos.servicio_id,movimientos.origen_lat,movimientos.origen_lon,movimientos.destino_lat,movimientos.destino_lon)">
						            <i class="fa fa-map-marker"></i></a>
				                </td>
				            </tr>
				        </tbody>
				    </table>
				    <modal title="Resumen de Servicio" visible="showModal" style="display:none;" id="modal" data-backdrop="static" data-keyboard="false">
							<div class="col-md-12">
								<div class="col-md-6">
									<span><b>Conductor: </b>{{detailmovi.NombreConductor}}</span>
								</div>
								<div class="col-md-6">
									<span><b>Cliente: </b>{{detailmovi.NombreCliente}}</span>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-6">
									<span><b>Unidad Móvil: </b>{{detailmovi.Unidad}}</span>
								</div>
								<div class="col-md-6">
									<span><b>Tiempo servicio: </b> {{detailmovi.diferencia}} mins.</span>
								</div>
							</div>
							<hr>

							<table class="margin-top-50 tablita" id="tmant" align="center" width="80%">
								<thead>
									<tr class="thead">
										<th>Fecha</th>
										<th>Proceso</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>{{detailmovi.fecha_recojo}} </td>
										<td><img src="includes/img/carro_verde.png" align="absmiddle" width="30"> CONFIRMACIÓN</td>
									</tr>
									<tr>
										<td>{{detailmovi.hora_inicio}} </td>
										<td><img src="includes/img/carro_amarillo.png" align="absmiddle" width="30"> INICIO DE CARRERA</td>
									</tr>
									<tr>
										<td>{{detailmovi.hora_fin}}</td>
										<td><img src="includes/img/carro_rojo.png" align="absmiddle" width="30"> FIN DE CARRERA</td>
									</tr>
								</tbody>
							</table>

							<hr style="margin-bottom:5px;">

							<div class="col-md-12">
								<div class="col-md-6">
									<span><b>Tipo de Pago :</b> {{detailmovi.tipo_pago}}</span>
								</div>
								<div ng-if="detailmovi.tipo_pago =='Vale'" class="col-md-6">
									<span style="color:red;"><b>Nº de Vale :</b> {{detailmovi.servicio_vale}}</span>
								</div>
							</div>

							<div class="col-md-12">
								<div class="col-md-6">
									<span><b>Origen: </b> {{detailmovi.origen_nombre}}</span>
								</div>
								<div class="col-md-6">
									<span><b>Destino: </b> {{detailmovi.destino_nombre}}</span>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-6">

								</div>
								<div class="col-md-6">
									<span><b>Costo: </b>{{detailmovi.tarifa | currency:"S/. ":2}}</span>
								</div>
								<div  ng-if="detailmovi.adicional!=0" class="col-md-6 pull-right">
									<span><b>Adicionales: </b><br>
										 <ul>
											<li>Peaje : {{detailmovi.peaje | currency:"s/ "}}</li>
											<li>Parqueo: {{detailmovi.parqueo | currency:"s/ "}}</li>
											<li>Espera : {{detailmovi.espera | currency:"s/ "}}</li>
											<li>Parada : {{detailmovi.parada | currency:"s/ "}}</li>
									    </ul>
									</span>
								</div>
							</div>
<hr><hr>
              <div ng-if="detailmovi.comentario">
                <strong>Detalle</strong>
                <div class="row">
                  <div class="col-md-6">
                    {{ detailmovi.comentario }}
                  </div>
                  <div class="col-md-6">
                    <img src="http://208.68.39.4/BL/{{ detailmovi.imagen}}" alt="">
                  </div>
                </div>
              </div>
							<hr>
							<div class="form-group"> 
							    <div class="col-sm-offset-2 col-sm-10 ">
							      <button type="submit" class="btn btn-default pull-right" data-dismiss="modal">Aceptar</button>
							    </div>
							</div>
          			</modal>
				</div>
		</div>
    </div>
</div>


  <div class="modal" id="myModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Ruta de Movimiento</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div style='overflow:hidden;height:600px;width:100%;'>
              <div id='mapMov'></div>
            </div>
          </div><!-- /.box-body -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" >Cerrar</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

<script type='text/javascript'>

var servicioDireccion;
var directionsDisplay;
var map;
var marker;
var markers = [];

$(function() {init_mapa();});
// Creacion del Map Google.Maps.api
function init_mapa() {

    directionsDisplay = new google.maps.DirectionsRenderer;
    servicioDireccion = new google.maps.DirectionsService();

    var bangalore = {lat: <?=$result[0]['usuario_latitud'];?>, lng: <?=$result[0]['usuario_longitud']?>};
    map = new google.maps.Map(document.getElementById('mapMov'), {
      zoom: 14,
      center: bangalore,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    $("#myModal").on("shown.bs.modal", function () {
       google.maps.event.trigger(map, 'resize');
       moveToLocation(<?=$result[0]['usuario_latitud'];?>,<?=$result[0]['usuario_longitud']?>);
    });

}

// Establece el centro de la posicion de la matriz.
function moveToLocation(lat, lng){
    var center = new google.maps.LatLng(lat, lng);
    map.panTo(center);
    map.setZoom(15); 
}

// Establece el mapa de todos los marcadores de la matriz.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Elimina los marcadores del mapa, pero los mantiene en la matriz.
function clearMarkers() {
  setMapOnAll(null);
}

// Animacion de Marker 
function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}

</script>

<link data-require="datatable-css@1.10.7" data-semver="1.10.7" rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" />
<!--script src="plugins/datatables/jquery.dataTables.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<script src="https://code.angularjs.org/1.4.5/angular-route.js"></script> 
<script src="plugins/Angular/angular-datatables.min.js"></script>
<script src="http://jvandemo.github.io/angularjs-google-maps/dist/angularjs-google-maps.js"></script>
<script src="plugins/Angular/app.js"></script>

