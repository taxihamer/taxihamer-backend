<?php
include'seguridad.php';
require_once'DAL/promocionDAO.php';
$promocionDAO = new promocionDAO();
$result = $promocionDAO->listaGral();
?>
<style type="text/css"> .nullOld{text-align: center;color: #e23558;}</style>
<div class="row" ng-app="myapp" ng-controller="promocionesController">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Registrar Promocion</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" action="BL/promoController.php" method="post" enctype="multipart/form-data">
        <div class="box-body">
          <div class="form-group">
            <label for="nombrepromo">Nombre Promo</label>
            <input type="text" class="form-control" name="nombrepromo" placeholder="Nombre de Promoción" required>
            <input type="hidden" class="form-control" name="hidden_insert_promocion" value="1">
          </div>
          <div class="form-group">
            <label for="imagen">Imagen Promo</label>
            <input type="file" name="imagen" class="form-control" required>
            <p class="help-block">Tamano mínimo 320 x 120 píxeles.</p>
          </div>
          <div class="form-group">
            <label for="tipo">Tipo de Persona</label>
            <select class="form-control" name="tipo">
              <option value="2">Conductor</option>
              <option value="1">Cliente</option>
            </select>
          </div>
          <div class="form-group">
            <label for="fvigencia">Fecha Inicio</label>
            <input type="date" class="form-control" name="finicio" placeholder="Fecha de Inicio" required>
          </div>
          <div class="form-group">
            <label for="fvigencia">Fecha Final</label>
            <input type="date" class="form-control" name="fvigencia" placeholder="Fecha de Final" required>
          </div>
          <div class="form-group">
            <label for="cantidad">Puntos</label>
            <input type="number" class="form-control" name="puntos" placeholder="Puntos necesarios para promocion" required>
          </div>
          <div class="form-group">
            <label for="cantidad">Cantidad</label>
            <input type="number" class="form-control" name="cantidad" placeholder="Cantidad de Promociones" required>
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
  <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado de Promociones</h3>
      </div><!-- /.box-header -->
      <br>
      <div class="dataTables_wrapper form-inline" role="grid">
        <div class="row" style="margin-left:0px;margin-right:0px;">
          <div class="col-sm-12" style="margin-top: 15px;">
              <div id="example_filter" class="dataTables_filter" style="width:100%">
               <label style="margin-right: 30px; float:left;">Tipo de Usuario : <select class="form-control" id="tipe" ng-change="tipoUsuario()" ng-model="stipoUsuario">
               <option value="">Seleccione</option>
                <option value="2">Conductor</option>
                <option value="1">Cliente</option>
              </select></label>
                  <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                    <input style="margin-left:0px;" type="search" class="form-control input-sm" aria-controls="example" ng-model="searchText" ng-change="searchTextChanged()" placeholder="Palabra clave">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign" style="color:#3c8dbc"></i></span>
                  </div>
              </div>
          </div>
        </div>
        <br>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Imagen</th>
                <th>Puntos</th>
                <th>Fecha Inicio</th>
                <th>Fecha Final</th>
                <th>Cantidad</th>
                <th>Stock</th>
                <th>Tipo</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody id="response" class="buscar">
              <tr ng-repeat = "promocion in promociones |orderBy:columnToOrder:reverse">
                <td>{{promocion.promo_id}}</td>
                <td>{{promocion.promo_nombre}}</td>
                <td><img ng-src="BL/{{promocion.promo_imagen}}" alt="taxi" width="50px" class="img-circle img-circle center-block" /></td>
                <td>{{promocion.promo_puntos}}</td>
                <td>{{promocion.promo_finicio | date : "dd-MM-y" }}</td>
                <td>{{promo_fvigencia | date : "dd-MM-y" }}</td>
                <td>{{promocion.promo_cantidad}}</td>
                <td>{{promocion.promo_stock}}</td>
                <td>{{promocion.promo_texto}}</td>
                  <td>
                    <div ng-if="promocion.promo_flag==1">
                      <switch name="enabled" ng-model="promocion.enabled" ng-change="changeEstado(promocion.promo_id,promocion.enabled)" on="on" off="off"></switch>
                      <button class="btn btn-default btn-circle" style="margin-top: -25px;"><i class="glyphicon glyphicon-edit"></i></button>
                      <a class="btn btn-default btn-circle" style="margin-top: -25px;" href="javascript:void(0);"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                    <div ng-if="promocion.promo_flag==0">
                        <switch name="enabled" ng-model="promocion.enabled" ng-change="changeEstado(promocion.promo_id,promocion.enabled)" on="on" off="off"></switch>
                         <button class="btn btn-warning btn-circle" style="margin-top: -25px;" data-toggle="modal" data-target="#myModal" ng-click="Editar(promocion.promo_id)"><i class="glyphicon glyphicon-edit"></i></button>
                        <a class="btn btn-danger btn-circle" style="margin-top: -25px;" ng-click="EliminarPromo(promocion.promo_id)"><i class="glyphicon glyphicon-remove"></i></a>
                    </div> 
                  </td>
              </tr>
              <tr class="nullOld" id="nullOld">
                <td colspan="10">No hay datos a mostrar</td>
              </tr>
            </tbody>
          </table>
          <div class="row">
              <div class="col-sm-5" style="margin-left:15px;">
                  <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                  <!-- Showing 1 to 10 of 848 entries -->
              </div>
              <div class="col-sm-6" style="text-align: right;">
                  <div class="dataTables_paginate paging_simple_numbers">
                    <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                  </div>
              </div>
          </div>
      </div>
    </div t="primary">
  </div>
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar Promociones</h4>
        </div>
        <form role="form" action="BL/promoController.php" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="nombrepromo">Nombre Promo</label>
              <input type="text" class="form-control" name="nombrepromo1" id="nombrepromo1" placeholder="Nombre de Promoción" required>
              <input type="hidden" class="form-control" name="hidden_update_promocion" value="1">
              <input type="hidden" class="form-control" name="idpromocion1" id="idpromocion1" value="0">
            </div>
            <div id="foto2"></div>
            <div class="form-group">
              <label for="imagen">Imagen Promo</label>
              <input type="file" name="imagen1" class="form-control" id="imagen1">
              <p class="help-block">Tamano mínimo 320 x 120 píxeles.</p>
            </div>
            <div class="form-group">
              <label for="tipo">Tipo de Persona</label>
              <select class="form-control" name="tipo1" id="tipo1">
                <option value="2">Conductor</option>
                <option value="1">Cliente</option>
              </select>
            </div>
            <div class="form-group">
              <label for="finicio">Fecha Inicio</label>
              <input type="date" class="form-control" name="finicio1" placeholder="Fecha de Inicio" id="finicio1" required>
            </div>
            <div class="form-group">
              <label for="fvigencia">Fecha Final</label>
              <input type="date" class="form-control" name="fvigencia1" id="fvigencia1" placeholder="Fecha de Vigencia" required>
            </div>
            <div class="form-group">
              <label for="cantidad">Puntos</label>
              <input type="number" class="form-control" name="puntos1" id="puntos1" placeholder="Puntos necesarios para promocion" required>
            </div>
            <div class="form-group">
              <label for="cantidad">Cantidad</label>
              <input type="number" class="form-control" name="cantidad1" id="cantidad1" placeholder="Cantidad de Promociones" required>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</div>

<script type="text/javascript">
  $(document).ready(function () {
    
    (function ($) {

        $('#name').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.buscar tr').hide();
            $('.buscar tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
 
  });

</script>
