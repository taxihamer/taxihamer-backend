<?php
  include'seguridad.php';
?>
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Módulo de Tarjetas</h3>
      </div>
      <br>
      <div class="row">
      <div class="col-md-3">
        <div class="col-md-12">
          <select class="form-control">
            <option>Conductor</option>
            <option>Cliente</option>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="col-md-12">
          <input type="date" class="form-control">
        </div>
      </div>
      <div class="col-md-3">
        <div class="col-md-12">
          <input type="date" class="form-control">
        </div>
      </div>
        <div class="col-md-3">
          <form role="form">
            <div class="col-md-8">
              <input type="text" class="form-control" name="conductor" placeholder="Buscar por Conductor" required >
            </div>
            <div class="col-md-2">
              <button class="btn btn-primary">Buscar</button>
            </div>
          </form>
        </div>
      </div>
      <br>

        <table class="table table-striped">
          <thead>
            <tr>
              <th>Tipo Persona</th>
              <th>Nombres</th>
              <th>Fecha Alerta</th>
              <th>Estado</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>data</td>
              <td>data</td>
              <td>data</td>
              <td>data</td>
              <td><button class="btn btn-success"><i class="glyphicon glyphicon-check"></i></button> <button class="btn btn-primary"><i class=" glyphicon glyphicon-share"></i></button></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!--style type="text/css">
  table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
  }
  .sortNew{
    color: #3c8dbc;
  }

  .sortNew:hover, .sortNew:active, .sortNew:focus{
    color: #3c8dbc;
  }
  .nullOld{
    display: none;
    text-align: center;
    color: #e23558;
  }
</style>
<section id="main-content" class="animated fadeInRight" ng-app="myapp">
    <div class="row">
        <div class="col-md-12" ng-controller="activityTableCtrl">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Listado de Conductores</h3>
              </div>
              <div class="panel-body">
              <div class="dataTables_wrapper form-inline" role="grid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="dataTables_length" id="example_length">
                            <label>
                            <select name="example_length" aria-controls="example" class="form-control input-sm" ng-model="pageSize" ng-change="pageSizeChanged()">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> records per page</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div id="example_filter" class="dataTables_filter">
                            <label>Buscar :<input type="search" class="form-control input-sm" aria-controls="example" ng-model="searchText" ng-change="searchTextChanged()" placeholder="Nombre de conductor"></label>
                        </div>
                    </div>
                </div>                        
                <div style="overflow-x:auto;">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Foto</th>
                        <th>Conductor</th>
                        <th>Correo</th>
                        <th>Nro. Telf.</th>
                        <th>Taxi Asignado</th>
                        <th><a ng-click="sort('tiposervicio_nombre')" href="#" class="sortNew"> Tipo Servicio <span class="{{Header[0]}}"></span></a></th>
                        <th><a ng-click="sort('conductor_calificacion')" href="#" class="sortNew"> Calificacion <span class="{{Header[1]}}"></span></a></th>
                        <th><a ng-click="sort('conductor_status')" href="#" class="sortNew"> Estado <span class="{{Header[2]}}"></span></a></th>
                        <th>Accion</th>
                    </tr>
                </thead>

                <tbody>
                    <tr ng-repeat = "conductor in activity | orderBy:columnToOrder:reverse ">
                      <td><img ng-src="BL/{{conductor.conductor_foto}}" alt="taxi" width="50px" class="img-circle" /></td>
                      <td>{{conductor.conductor_nombre}} {{conductor.conductor_apellido}}</td>
                      <td>{{conductor.conductor_correo}}</td>
                      <td>{{conductor.conductor_celular}}</td>
                      <td>{{conductor.unidad_alias}}</td>
                      <td>{{conductor.tiposervicio_nombre}}</td>
                      <td>{{conductor.conductor_calificacion | number:1}}</td>
                      <td>
                        <div ng-if="conductor.conductor_status==1">
                             <switch name="enabled" ng-model="conductor.enabled" ng-change="changeCallback(conductor.conductor_id, $index + 1)" on="on" off="off"></switch>
                        </div> 
                        <div ng-if="conductor.conductor_status==0">
                               <switch name="enabled" ng-model="conductor.enabled" ng-change="changeCallback(conductor.conductor_id, $index + 1)" on="on" off="off"></switch>
                        </div>  
                      </td>
                      <td style="width:8%;">
                        <div ng-if="conductor.conductor_status==0" class="pull-left">
                          <button class="btn btn-warning" data-toggle="modal" data-target="#myModal" ng-click="llenar(conductor.conductor_id)"><i class="glyphicon glyphicon-edit"></i></button>

                        </div>
                        <div ng-if="conductor.conductor_status==1" class="pull-left">
                          <button class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></button>
                        </div>
                        <a class="btn btn-danger pull-right" ng-click="EliminarConductor(conductor.conductor_id)"><i class="glyphicon glyphicon-remove"></i></a>
                      </td>
                    </tr>
                    <tr class="nullOld" id="nullOld">
                      <td colspan="9">No hay datos a mostrar</td>
                    </tr>
                </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Showing {{startItem}} to {{endItem}} of {{totalItems}} entries</div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_simple_numbers">
                        <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                        </div>
                    </div>
                </div>
              </div>                      
              </div>
              </div>
            </div>
        </div>
    </div>
 </section-->
