<?php
  include'seguridad.php';
?>

<!--<link href="../bootstrap/css/main.css" rel="stylesheet">-->
<style type="text/css">
  .show-grid [class^="col-"]{
    border: inherit !important; 
  }
  .widt{width:100%;}
  .tilabel{margin-bottom:15px;}
  td {text-align: center;}
</style>
<div class="row" ng-app="myappCorreo">
  <div class="container">
    <div class="col-sm-9">
      <div class="widget-box" id="recent-box" ng-controller="correosController">
      <div class="widget-header header-color-blue">
        <div class="row">
          <div class="col-sm-6">
            <h4 class="bigger lighter">
              <i class="glyphicon glyphicon-align-justify"></i>&nbsp;
              Configuracion de Correos Salientes
            </h4>
          </div>
          <!--div class="col-sm-3">
            <button ng-click="addNewClicked=!addNewClicked;" class="btn btn-sm btn-danger header-elements-margin"><i class="glyphicon  glyphicon-plus"></i>&nbsp;Agregar</button>
          </div-->
        </div>
      </div>
      <div class="widget-body ">
        <form id="newTaskForm" class="add-task">
          <div class="row show-grid">
            <div class="col-md-12" style="padding-bottom: inherit;border: inherit;">
              <!-- Level 1: .col-md-9 -->
              <h3><label> Informacion de la Conexion </label></h3>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-actions" >
                      <div class="input-group widt">
                        <label for="servidor">Servidor SMTP (Host)</label>
                        <input type="hidden" name="tipo" ng-model="tipo">
                        <input type="text" class="form-control" name="servidor" ng-model="servidor" placeholder="Agregar servidor smtp" required><br>
                      </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-actions" >
                      <div class="input-group widt">
                        <label for="puerto">Puerto SMTP</label>
                        <input type="number" class="form-control" name="puerto" ng-model="puerto" placeholder="Agregar puerto smtp" required>
                      </div>
                    </div>
                </div>
              </div>
              <h3><label>Seguridad y Autenticacion</label></h3>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-actions" >
                      <div class="input-group widt">
                        <label for="tiposeguridad" class="tilabel">Seguridad de la conexion</label></br>
                      </div>
                      <div class="input-group widt">
                        <label for="usuario" class="tilabel">Nombre de Usuario</label></br>
                      </div>
                      <div class="input-group widt">
                        <label for="password" class="tilabel">Contraseña</label></br>
                      </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-actions" >
                      <div class="input-group widt">
                        <select class="form-control" name="tiposeguridad" ng-model="tiposeguridad"><option value="1" selected> tls </option></select>
                      </div>
                      <div class="input-group widt">
                        <input type="text" class="form-control" name="usuario" ng-model="usuario" placeholder="Agregar Usuario">
                      </div>
                      <div class="input-group widt">
                        <input type="password" class="form-control" name="password" ng-model="password" placeholder="Agregar Password">
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="input-group-btn" ng-if="total==0">
                <button class="btn btn-default" type="submit" ng-click="addCorreos(servidor,puerto,tiposeguridad,usuario,password,tipo)"><i class="glyphicon glyphicon-plus"></i>&nbsp;Agregar</button>
          </div>
        </form>
      <div class="task">
        <table datatable="ng" dt-options="dtOptions" class="table table-hover" >
              <thead>
                  <tr>
                      <th>Servidor SMTP (Host)</th>
                      <th>Puerto SMTP</th>
                      <th>Seguridad Conexion</th>
                      <th>Usuario</th>
                      <th>Password</th>
                      <th>Accion</th>
                  </tr>
              </thead>
              <tbody>
                  <tr ng-repeat="correo in correos">
                      <td>{{correo.config_server_smtp}}</td>
                      <td>{{correo.config_puerto_smtp}}</td>
                      <td>{{correo.smtp}}</td>
                      <td>{{correo.config_usuaro_smtp}}</td>
                      <td>{{correo.config_password_smtp}}</td>
                      <td><button class="btn btn-warning" data-toggle="modal" data-target="#myModal" ng-click="llenar(correo.config_id,$index +1)"><i class="glyphicon glyphicon-edit"></i></button></td>
                  </tr>
              </tbody>
        </table>
      </div>
        <!-- Modal in Angular -->
          <modal title="Editar servidor correo" visible="showModal" style="display:none;" id="modal">
            <form id="formuploadajax" class="form-horizontal" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" ng-model="idconductor" id="idconductor">
                    <input type="hidden" ng-model="indexEdit">
                    <div class="row show-grid">
            <div class="col-md-12" style="padding-bottom: inherit;border: inherit;">
              <!-- Level 1: .col-md-9 -->
              <h3><label> Informacion de la Conexion </label></h3>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-actions" >
                      <div class="input-group widt">
                        <label for="servidor">Servidor SMTP (Host)</label>
                        <input type="hidden" name="tipo_edit" ng-model="tipo_edit">
                        <input type="text" class="form-control" name="servidor_edit" ng-model="servidor_edit" placeholder="Agregar servidor smtp"><br>
                      </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-actions" >
                      <div class="input-group widt">
                        <label for="puerto">Puerto SMTP</label>
                        <input type="number" class="form-control" name="puerto_edit" ng-model="puerto_edit" placeholder="Agregar puerto smtp">
                      </div>
                    </div>
                </div>
              </div>
              <h3><label>Seguridad y Autenticacion</label></h3>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-actions" >
                      <div class="input-group widt">
                        <label for="tiposeguridad" class="tilabel">Seguridad de la conexion</label></br>
                      </div>
                      <div class="input-group widt">
                        <label for="usuario" class="tilabel">Nombre de Usuario</label></br>
                      </div>
                      <div class="input-group widt">
                        <label for="password" class="tilabel">Contraseña</label></br>
                      </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-actions" >
                      <div class="input-group widt">
                        <select class="form-control" name="tiposeguridad" ng-model="tiposeguridad_edit"><option value="1" selected> tls </option></select>
                      </div>
                      <div class="input-group widt">
                        <input type="text" class="form-control" name="usuario_edit" ng-model="usuario_edit" placeholder="Agregar Usuario">
                      </div>
                      <div class="input-group widt">
                        <input type="password" class="form-control" name="password_edit" ng-model="password_edit" placeholder="Agregar Password">
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>

                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10 ">
                          <button type="button" class="btn btn-default pull-right" data-dismiss="modal" style="margin-left: 2px;">Cerrar</button>
                          <button type="submit" class="btn btn-primary pull-right" ng-click="addCorreos(servidor_edit,puerto_edit,tiposeguridad_edit,usuario_edit,password_edit,tipo_edit)">Guardar Cambios</button>
                        </div>
                      </div>
            </form>
          </modal>

      </div>
      </div>
    </div>
  </div>
</div>

<link data-require="datatable-css@1.10.7" data-semver="1.10.7" rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" />
<script src="plugins/datatables/jquery.dataTables.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<script src="https://code.angularjs.org/1.4.5/angular-route.js"></script> 
<script src="plugins/Angular/angular-datatables.min.js"></script>
<script src="plugins/Angular/appCorreo.js"></script>
