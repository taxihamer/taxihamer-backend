<style>
  .contact-info-form {
    list-style-type: none;
    margin: 30px 0;
    padding: 0;
  }

  .contact-info-form li {
    margin: 10px 0;
  }

  .contact-info-form label {
    display: inline-block;
    width: 100px;
    text-align: right;
    font-weight: bold;
  }

  .k-header.k-grid-toolbar {
    background-color: #3c8dbc !important;
    border-color: #3c8dbc !important;
  }

  .k-grid .k-header .k-button{
    background-color: #449d44 !important;
    border-color: #449d44 !important;
  }
</style>
<div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Config Horas Tarifa</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="BL/configuracionController.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="config_insert">
                <input type="hidden" name="result" id="result">
                <div class="box-body">
                    <div class="form-group">
                        <lavel for="horainicio">Hora Inicio</lavel>
                        <input id="horainicio" class="form-control" name="horainicio" value="12:00 AM" style="width: 100%;" />
                    </div>
                    <div class="form-group">
                        <lavel for="horafinal">Hora Final</lavel>
                        <input id="horafinal" class="form-control" name="horafinal" value="11:30 PM" style="width: 100%;" />
                    </div>
                    <div class="form-group">
                        <label>Dias de Semana</label>
                        <div id="treeviewcheck"></div>
                    </div>
                    <div class="form-group" id="input"></div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </form>
            </div><!-- /.box -->
        </div>
        <div class="col-md-9">
             <?php
                if(isset($_REQUEST['status'])){
                  if($_REQUEST['status']=="true"){
                    echo '<div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              Operación Realizada con Éxito</div>';
                  }elseif ($_REQUEST['status']=="bool") {
                    echo '<div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              Ups, seleccione un dia de semana </div>';
                  }elseif ($_REQUEST['status']=="validar") {
                    echo '<div class="alert alert-danger alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      Ups, ya existe un registro con ese rango de hora. </div>';
                  }else{
                    echo '<div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              Ups, ocurrio un error inesperado.</div>';
                  }
                }
            ?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Listado de Incremento en Porcentajes</h3>
                </div><!-- /.box-header -->
                <div id="example" >
                    <div id="grid"></div>

                <script type="text/javascript">

                function timeEditor(container, options) {
                    $('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
                            .appendTo(container)
                            .kendoTimePicker({});
                }

                    $(document).ready(function () {
                            dataSource = new kendo.data.DataSource({
                                transport: {
                                    read: function(options){
                                        $.ajax({ 
                                            url: "../BL/configuracionController.php",
                                            dataType: "json",
                                            type: "GET",
                                            contentType: "application/json; charset=utf-8",
                                            cache: false,
                                            data : {hidden_read:'hidden_read'},
                                            success: function (result) {
                                                options.success(result);
                                            },
                                            error: function (result) {
                                                options.error(result);
                                            }
                                        });
                                    },
                                    parameterMap: function (options, operation) {
                                        if (operation !== "read" && options.models) {
                                            return { models: kendo.stringify(options.models) };
                                        }
                                    }
                                },
                                batch: true,
                                pageSize: 20,
                                schema: {
                                    model: {
                                        id: "rangohora_id",
                                        fields: {
                                            rangohora_id: { editable: false, nullable: true },
                                            rango_hinicio: { type : "string" },
                                            rango_hfin: { type : "string" },
                                            rango_lun: { type: "number" }, //validation: { required: true, min: 1}
                                            rango_mar: { type: "number" },
                                            rango_mie: { type: "number" },
                                            rango_jue: { type: "number" },
                                            rango_vie: { type: "number" },
                                            rango_sab: { type: "number" },
                                            rango_dom: { type: "number" },
                                            rango_estado_new: { type: "string"}
                                        }
                                    }
                                }
                            });

                        $("#grid").kendoGrid({
                            dataSource: dataSource,
                            pageable: true,
                            //height: auto,
                            //toolbar: ["create"],

                            columns: [
                                { field: "rango_hinicio" , title: "Hora Inicio", format:"{0:HH:mm}",editor: timeEditor, width: "100px"},
                                { field: "rango_hfin" , title: "Hora Fin", format:"{0:HH:mm}",editor: timeEditor, width: "100px"},
                                { field: "rango_lun", title: "Lunes", width: "100px" }, // format: "{0:c}",
                                { field: "rango_mar", title: "Martes", width: "100px" },
                                { field: "rango_mie", title: "Miercoles", width: "100px" },
                                { field: "rango_jue", title: "Jueves", width: "100px" },
                                { field: "rango_vie", title: "Viernes", width: "100px" },
                                { field: "rango_sab", title: "Sabado", width: "100px" },
                                { field: "rango_dom", title: "Domingo", width: "100px" },
                                //{ field: "rango_estado", title: "Estado", width: "110px" },
                                { field: "rango_estado_new", title: "Estado" ,width: "110px" },
                                { title: " Opciones ", width: 240, template: "<a title='Editar' href=\"javascript:editController('#: rangohora_id #')\" type=\"button\" class=\"btn btn-warning btn-xs\"><span class=\"glyphicon glyphicon-pencil\"></span></a> <a title='Eliminar' href=\"javascript:deleteController('#: rangohora_id #')\" type=\"button\" class=\"btn btn-danger btn-xs\"><span class=\"glyphicon glyphicon-remove\"></span></a>"}],
                                //{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px"}],
                                //" <a title='Eliminar' href=\"javascript:deleteController('#: rangohora_id #')\" type=\"button\" class=\"btn btn-danger btn-xs\"><span class=\"glyphicon glyphicon-remove\"></span></a>
                            editable: "inline"
                        });

                        setTimeout(function(){ 

                            var grid = $("#grid").data("kendoGrid");
                            var row = grid.tbody.find("tr");
                            for (var i = 0; i < row.length; i++) {
                                var data = grid.dataItem(row[i]);
                                //console.log(i);
                                //console.log('estado'+ data.rango_estado);
                                //$("tr:nth-last-child(2)").css( "background", "#ff0000" );
                                //console.log(data.rango_estado);
                            }
 
                        }, 1000);

                    });
                    //console.log(data.age); // displays "Jane Doe"
                </script>
                </div>
            </div>
        </div>
</div>
<script type="text/javascript">
    var checkboxValues = new Array();

    $(document).ready(function(){


        $('.children').click(function(event) {

              var inputs, x, selecionados=0;
              var idmenu = this.id;
              var childVal = $(this).val();
              var index = checkboxValues.indexOf(childVal);

                inputs = document.getElementsByTagName('input');
                  for(x=0;x<inputs.length;x++){
                    if(inputs[x].type=='checkbox'){
                      if(inputs[x].checked==true && inputs[x].id == idmenu){
                        selecionados++;
                      }
                    }
                  }

                if(selecionados == 0){
                    $("#par-"+idmenu).prop('checked', false);
                }else{
                    $("#par-"+idmenu).prop('checked', true);
                    if($(".chekchil_"+childVal).is(':checked')){
                         checkboxValues.push(childVal);
                    }else{
                        var removed = checkboxValues.splice(index, 1);
                    }
                }

            if($("#dv_"+childVal).length > 0 ){
                $("#dv_"+childVal).remove();
            }else{
                if($(".chekchil_"+childVal).is(':checked')) {
                    if(checkboxValues.length == 7){
                        createInputChidl(99);
                    }else{
                        createInputChidl(childVal);
                    }
                }else{
                    if(checkboxValues.length < 7){

                        var htmlChild = [];
                        for(x=0;x<checkboxValues.length;x++){
                            var label = labelSwitch(checkboxValues[x]);
                            htmlChild += "<div class='form-group' style='margin-right:0px;margin-left:0px' id='dv_"+checkboxValues[x]+"'><label>"+label+"</label><input id='currency_"+checkboxValues[x]+"' name='currency_"+checkboxValues[x]+"' type='number' value='0' min='0' max='100' style='width: 100%;' required/></div>";

                        }   
                            $("#inputEdit").html('');
                            $("#inputEdit").append(htmlChild);
                            OrdenarInput();
                    }
                }
            }

        });

        $('.check-all-parent').click(function(event) {
            var idmenu = this.id;
            var PareVal = $(this).val();

            var partes = idmenu.split("-");
            if(this.checked) {
                checkboxValues.length=0;
                $('.chekchil_'+partes[1]).each(function() { 
                    this.checked = true;
                });

                $('input[name="chil[]"]:checked').each(function() {
                    checkboxValues.push($(this).val());
                });
                CreateInput_99();
            }else{
                $('.chekchil_'+partes[1]).each(function() { 
                    this.checked = false;                       
                });
                checkboxValues.length=0;         
                $("#inputEdit").html('');
            } 

        });
    });

    function createInputChidl(id){

        if(id != '99'){
            if($("#dv_99").length > 0 ){$("#dv_99").remove();}
            var label = labelSwitch(id);
            var htmlChild = "<div class='form-group' style='margin-right:0px;margin-left:0px' id='dv_"+id+"'><label>"+label+"</label><input id='currency_"+id+"' name='currency_"+id+"' type='number' value='0' min='0' max='100' style='width: 100%;' required/></div>"

            $("#inputEdit").append(htmlChild);
            OrdenarInput();
        }else{
            CreateInput_99();
        }
    }

    function labelSwitch(id){

            var label = '';
            switch(id){
                case '1': label = 'Lunes';break;
                case '2': label = 'Martes';break;
                case '3': label = 'Miercoles';break;
                case '4': label = 'Jueves';break;
                case '5': label = 'Viernes';break;
                case '6': label = 'Sabado';break;
                case '7': label = 'Domingo';break;
            }

        return label;
    }

    function CreateInput_99(){

        $("#inputEdit").html("<div class='form-group' style='margin-right:0px;margin-left:0px' id='dv_99'><label>Todos</label><input id='currency_99' name='currency_99' type='number' value='0' min='0' max='100' style='width: 100%;' /></div>");
            document.getElementById("currency_99").required = true;

    }

    function OrdenarInput(){

            var main = document.getElementById('inputEdit');
            [].map.call( main.children, Object ).sort( function ( a, b ) {
                return +a.id.match( /\d+/ ) - +b.id.match( /\d+/ );
            }).forEach( function ( elem ) {
                main.appendChild( elem );
            });
    }

</script>
<div id="editController" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" id="frmUpdateConfig" name="frmUpdateConfig" action="../BL/configuracionController.php">
            <input type="hidden" name="config_update">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="modal-cerrar">×</button-->
                <h4 class="modal-title">EDITAR RANGO DE HORAS <span class="edi-cod-mod"></span></h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4>
                                    <i class="icon fa fa-check"></i>
                                    <span class="mensajeAlerta">Exito</span>
                                </h4>
                                <span id="message"></span>
                            </div>
                        </div>
                    </div>           
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible" role="alert" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4>
                                    <i class="icon fa fa-remove"></i>
                                    <span class="mensajeAlerta">Error</span>
                                </h4>
                                <span id="message"></span>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="rango_id" name="rango_id"/>
                    <div class="form-group">
                      <lavel for="my-checkbox">Estado : </lavel><!--checked="checked"-->
                      <input type="checkbox" id="notifications-switch" class="form-control" data-size="mini" name="my-checkbox" />
                    </div>
                    <div class="form-group">
                        <lavel for="horainicioEdit">Hora Inicio</lavel>
                        <input id="horainicioEdit" class="form-control" name="horainicioEdit" style="width: 100%;" />
                    </div>
                    <div class="form-group">
                       <lavel for="horafinalEdit">Hora Final</lavel>
                        <input id="horafinalEdit" class="form-control" name="horafinalEdit" style="width: 100%;" />
                    </div>
                    <div class="form-group">
                        <label>Dias de Semana</label>
                        <!--div id="treeviewcheckEdit"></div-->
                         <ul class="sidebar-menu">
                            <li>
                                <div class="checkbox">
                                    <label><input name="parent[]" type="checkbox" id="par-99" class="chekparet_99 check-all-parent" value="99"><span class="glyphicon glyphicon-calendar"></span> Todos los dias</label>
                                    <ul>
                                        <div class="checkbox">
                                            <label><input name="chil[]" id="99" type="checkbox" class="children chekchil_99 chekchil_1" value="1"><span class="glyphicon glyphicon-calendar"></span> Lunes</label><br>
                                            <label><input name="chil[]" id="99" type="checkbox" class="children chekchil_99 chekchil_2" value="2"><span class="glyphicon glyphicon-calendar"></span> Martes</label><br>
                                            <label><input name="chil[]" id="99" type="checkbox" class="children chekchil_99 chekchil_3" value="3"><span class="glyphicon glyphicon-calendar"></span> Miercoles</label><br>
                                            <label><input name="chil[]" id="99" type="checkbox" class="children chekchil_99 chekchil_4" value="4"><span class="glyphicon glyphicon-calendar"></span> Jueves</label><br>
                                            <label><input name="chil[]" id="99" type="checkbox" class="children chekchil_99 chekchil_5" value="5"><span class="glyphicon glyphicon-calendar"></span> Viernes</label><br>
                                            <label><input name="chil[]" id="99" type="checkbox" class="children chekchil_99 chekchil_6" value="6"><span class="glyphicon glyphicon-calendar"></span> Sabado</label><br>
                                            <label><input name="chil[]" id="99" type="checkbox" class="children chekchil_99 chekchil_7" value="7"><span class="glyphicon glyphicon-calendar"></span> Domingo</label>
                                        </div>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="form-group" id="inputEdit"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> CERRAR</button>
                <button type="submit" class="btn btn-primary">Guardar Cambios</button>
            </div>
            </form>
        </div>
    </div>
</div><!-- /.modal  -->
<script type="text/javascript">
      
    $(document).ready(function(){

        $('input[name="my-checkbox"]').bootstrapSwitch();

        $("#horainicio").kendoTimePicker({ format: "HH:mm tt",animation: {close: {effects: "zoom:out",duration: 300}}});
        $("#horafinal").kendoTimePicker({format: "HH:mm tt",animation: {close: {effects: "zoom:out",duration: 300}}});

        $("#horainicioEdit").kendoTimePicker({ format: "HH:mm tt",animation: {close: {effects: "zoom:out",duration: 300}}});
        $("#horafinalEdit").kendoTimePicker({format: "HH:mm tt",animation: {close: {effects: "zoom:out",duration: 300}}});

        $("#treeviewcheck").kendoTreeView({
            checkboxes: {
                 checkChildren: true
            },
            check: onCheck,
            dataSource: [{
                id: 99, text: "Todos los dias", expanded: true, spriteCssClass: "rootfolder", items: [
                    {id: 1, text: "Lunes", expanded: true, spriteCssClass: "folder"},
                    {id: 2, text: "Martes", expanded: true, spriteCssClass: "folder"},
                    {id: 3, text: "Miercoles", expanded: true, spriteCssClass: "folder"},
                    {id: 4, text: "Jueves", expanded: true, spriteCssClass: "folder"},
                    {id: 5, text: "Viernes", expanded: true, spriteCssClass: "folder"},
                    {id: 6, text: "Sabado", expanded: true, spriteCssClass: "folder"},
                    {id: 7, text: "Domingo", expanded: true, spriteCssClass: "folder"},
                ]
            }]
        });
});

    // función que reúne a los ID de nodos seleccionados
    function checkedNodeIds(nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].id);
            }

            if (nodes[i].hasChildren) {
                checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    }

    // Mostrar chekboxs ID de nodo seleccionado de datos para el cambio
    function onCheck() {
        var checkedNodes = [],
            treeView = $("#treeviewcheck").data("kendoTreeView"),
            message;

        checkedNodeIds(treeView.dataSource.view(), checkedNodes);
        var todos = '';
        if (checkedNodes.length > 0) {
            var htmlinput = label = '';
            message = checkedNodes.join(",");
            $("#result").val(message);
            for (var i = 0; i < checkedNodes.length; i++) {
                if(checkedNodes[i] == 99){todos = 99;}
                switch(checkedNodes[i]){
                    case 1: label = 'Lunes';break;
                    case 2: label = 'Martes';break;
                    case 3: label = 'Miercoles';break;
                    case 4: label = 'Jueves';break;
                    case 5: label = 'Viernes';break;
                    case 6: label = 'Sabado';break;
                    case 7: label = 'Domingo';break;
                }
                htmlinput+="<div class='form-group'><label>"+label+"</label><input id='currency_"+checkedNodes[i]+"' name='currency_"+checkedNodes[i]+"' type='number' value='0' min='0' max='100' style='width: 100%;' required/></div>";
               
            }
            message = "";
        }else{
             $("#result").val(0);
        }

        if(todos == '99'){
            htmlinput = '';
            $("#input").html("<div class='form-group'><label>Todos</label><input id='currency_99' name='currency_99' type='number' value='0' min='0' max='100' style='width: 100%;' /></div>");
            document.getElementById("currency_99").required = true;
        }else if(checkedNodes.length == 0){
            $("#input").html('');
        }else{
            $("#input").html(htmlinput);
        }
        
    }
    function reload(){
        window.location = '../index.php?seccion=rangohoras&status=true';
    }

    function deleteController(id) {
        var Url = 'BL/configuracionController.php';
        deleteGeneral(id,Url,reload);
    }

function editController (id) {

    $(".children").prop('checked',false);

    $.ajax({
        url: "../BL/configuracionController.php",
        type:'POST',
        data: {ID:id,hidden_ll_config:'hidden_ll_config'},
        dataType:"json"
    }).done(function(res){

        if(res){

            if(res[0].rango_estado == "1"){
                $('input[name="my-checkbox"]').bootstrapSwitch('state', true);
            }else{
                $('input[name="my-checkbox"]').bootstrapSwitch('state', false);
            }

            $("#rango_id").val(id);
            var check_dias = JSON.parse(res[0].rango_check);
            $("#horafinalEdit").val(res[0].rango_hfin);
            $("#horainicioEdit").val(res[0].rango_hinicio);
            var todos = htmlinput = label = '';
            var valor = 0;
            $.each(check_dias,function(index,obj)
            {   
                checkboxValues.push(obj);
                $(".chekchil_"+obj).prop('checked', true);

                if(obj == 99){todos = 99;}
                switch(obj){
                    case '1': label = 'Lunes';break;
                    case '2': label = 'Martes';break;
                    case '3': label = 'Miercoles';break;
                    case '4': label = 'Jueves';break;
                    case '5': label = 'Viernes';break;
                    case '6': label = 'Sabado';break;
                    case '7': label = 'Domingo';break;
                }
 
                htmlinput+="<div class='form-group' style='margin-right:0px;margin-left:0px' id='dv_"+obj+"'><label>"+label+"</label><input id='currency_"+obj+"' name='currency_"+obj+"' type='number' value='0' min='0' max='100' style='width: 100%;' required/></div>";
            });

            if(todos == '99'){
                htmlinput = '';
                $("#inputEdit").html("<div class='form-group' style='margin-right:0px;margin-left:0px' id='dv_99'><label>Todos</label><input id='currency_99' name='currency_99' type='number' value='0' min='0' max='100' style='width: 100%;' /></div>");
                document.getElementById("currency_99").required = true;
            }else{
                $("#inputEdit").html(htmlinput);
                $.each(check_dias,function(index,obj)
                {
                    switch(obj){
                        case '1': valor = res[0].rango_lun;break;
                        case '2': valor = res[0].rango_mar;break;
                        case '3': valor = res[0].rango_mie;break;
                        case '4': valor = res[0].rango_jue;break;
                        case '5': valor = res[0].rango_vie;break;
                        case '6': valor = res[0].rango_sab;break;
                        case '7': valor = res[0].rango_dom;break;
                    }

                $("#currency_"+obj).val(valor);
                 
                });
            }
            $("#editController").modal({backdrop:'static'});

        }else{

        }
    }).fail(function(){
    });
}
</script>
<style type="text/css">
    .k-treeview .k-plus {background-position: 0 -20px !important;}
    .k-treeview .k-minus {background-position: 0 -36px !important;}
    #treeviewcheck .k-sprite {background-image: url("http://demos.telerik.com/kendo-ui/content/web/treeview/coloricons-sprite.png");}
    .rootfolder { background-position: 0 0; }
    .folder     { background-position: 0 -16px; }

</style>