<?php
include'seguridad.php';
require_once'DAL/lugaresDAO.php';
require_once'DAL/usuarioDAO.php';
require_once'DAL/empresaDAO.php';
require_once'DAL/tipoServicioDAO.php';
require_once'DAL/generalDAO.php';

$data['rol_id'] = $_SESSION['rol_id'];
$data['mail'] = $_SESSION['mail'];
$usuarioDAO = new usuarioDAO();
$result = $usuarioDAO->listacoordenadas($data);

$lugaresDAO = new lugaresDAO();
$result_lugares = $lugaresDAO->lista();

$empresaDAO = new empresaDAO();
$result_empresa = $empresaDAO->listempresa();

$tipoServicioDAO = new tipoServicioDAO();
$result_tipo = $tipoServicioDAO->lista();

$generalDAO = new generalDAO();
$config = $generalDAO->lista();

?>
<style>
  #gmap_despacho{
    height:750px;
    width:100%;
  }
  #gmap_despacho img{
    max-width:none!important;
    background:none!important;
  }
  #coordspoligonoedit , #centerpoligonoedit{
    display: none;
  }
	.ui-dialog .ui-dialog-titlebar-close{
		display: none!important;
	}
</style>
<div class="row">
	<div class="col-md-3">
		<div class="box box-primary">
	      <div class="box-header with-border">
	        <h3 class="box-title">Nuevo Servicios</h3>
	      </div><!-- /.box-header -->
	      <!-- form start -->
	      <input type="hidden" name="configId" id="configId" value="<?=$config[0]['tipo_tarifa']?>">
	      <form role="form" id="formDespacho" name="formDespacho" action="" method="POST">
	      	<input type="hidden" name="idco" id="idco">
	      	<input type="hidden" name="idcl" id="idcl">
	        <div class="box-body">
	          <div class="form-group">
	            <label for="fecha">Fecha / Hora : </label>
	            <label class="pull-right"><em><h5><?=date("d/m/Y H:i a"); ?></h5></em></label>
	          </div>
	          <div class="form-group">
	            <label for="celular">Ingrese Nº Celular / Nombre </label>
	            <input type="text" class="form-control" name="celular" id="celular" placeholder="Ingrese Nº Celular / Nombre " required>
	          </div>
	          <div class="form-group">
	            <label for="cliente">Nombre del Cliente</label>
	            <input type="text" class="form-control" name="cliente" id="cliente" placeholder="Nombre del cliente" required>
	            <input type="hidden" name="empid" id="empid">
	          </div>
	          <div class="form-group">
	            <label for="apecliente">Apellido del Cliente</label>
	            <input type="text" class="form-control" name="apecliente" id="apecliente" placeholder="Apellido del cliente" required>
	          </div>
	           <div class="form-group">
	            <label for="correo">Correo del Cliente</label>
	            <input type="email" class="form-control" name="correo" id="correo" placeholder="Correo" required>
	          </div>
	          <div class="form-group">
	            <label for="sercicio">Tipo Pago</label>
	            <select class="form-control" name="tipopago" id="tipopago">
	            	  <option value="0"> Seleccione Pago </option>
		              <option value="1"> Efectivo </option>
		              <option value="2"> Vale </option>
		        </select>
          	  </div>
	          <div class="form-group">
	            <label for="sercicio">Tipo Servicio</label>
	            <select class="form-control" name="tiposervicio_id" id="tiposervicio_id" onchange="Serviciotipo(document.getElementById('tiposervicio_id').value)">
	            	  <option value="0"> Seleccione tipo </option>
		            <?php
		            foreach($result_tipo AS $res_tipo){
		              ?>
		              <option value="<?php echo $res_tipo['tiposervicio_id']; ?>"><?php echo $res_tipo['tiposervicio_nombre']; ?></option>
		              <?php
		              
		            }
		            ?>
		        </select>
          	  </div>
	          <div class="form-group">
	            <label for="reforigen">Referencia de Origen</label>
	            <input type="text" class="form-control" name="reforigen" id="reforigen" placeholder="Referencia de Origen" required>
	            <input type="hidden" name="reforilat" id="reforilat">
	            <input type="hidden" name="reforilng" id="reforilng">
	            <input type="hidden" name="origenlugar" id="origenlugar">
	          </div>
	          <div class="form-group">
	             	<label for="refdestinno">Referencia de Destino</label>
	            	<input type="text" class="form-control" name="refdestinno" id="refdestinno" placeholder="Referencia de Destino" required>
	            	<input type="hidden" name="refdeslat" id="refdeslat">
	            	<input type="hidden" name="refdeslng" id="refdeslng">
	            	<input type="hidden" name="destinolugar" id="destinolugar">
	          </div>
	          <div class="form-group">
	            <label for="costo">Tarifa</label>
	            <input type="number" class="form-control" name="costo" id="costo" placeholder="Costo" required readonly="readonly">
	          </div>
	          <div class="form-group">
	          <label for="nombres">Seleccione Conductor del Mapa</label>
	            <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombre del Conductor" required>
	          </div>
	        </div><!-- /.box-body -->

	        <div class="box-footer">
	          <button type="submit" class="btn btn-primary">Enviar Despacho</button>
	        </div>
	      </form>
	    </div><!-- /.box -->
	</div>
	<div class="col-md-9">
		<div style='overflow:hidden;height:100%;width:100%;'>
            <div id='gmap_despacho'></div>
          </div>
	</div>
</div>
<script src="<?=IP_SOCKET?>socket.io/socket.io.js"></script>
<!--script src="http://alo45satelital.tutaxidispatch.com:8081/socket.io/socket.io.js" ></script-->
<script type='text/javascript'>

var map;
var coords;
var marker = [];
var markers = new Array();
var datos = [];
var infowindow = [];
var socket = null;
var conductors;
var logoestado = '';
var flag = '';
var idsConductorDisponibles = [];
var markerorigen;
var geocoder = null;
var lugares;
var poligono;
var Rptadocs = false;
var seleccionado;
var SessionServiceId;
var clear;

$(function() {

	$("#formDespacho").validate({
	
    submitHandler: function(form) {
    	
    	var str = $( "#formDespacho" ).serialize();
    	var flag = '';
    	if(clear == 2){
    		console.log('elegir a otro conductor');
    		flag = 'edit';
    	}else{
    		flag = 'new';
    	}
    	$.ajax({
		    url : "../BL/servicioController.php",
		    type: "POST",
		    async: true,
	        cache: false,
		    data : str+'&hidden_servicio=hidden_servicio&flag='+flag,
		    success: function(data)
		    	{	
		    		if(data.status == true){
		    			console.log('socket web panel '+ socket.id);
		    			var socketCliente = '';
		    			var cantcliente = $("#idcl").val().length;
					    	if(cantcliente <= 0){
					    		socketCliente = socket.id;
					    	}else{
					    		if(data.send.socketCliente != null){
					    			socketCliente = data.send.socketCliente;
					    		}else{
					    			socketCliente = socket.id;
					    		}
					    	}
		    			
		    			socket.emit('nuevaSolicitudConductorWeb',{
		    					IdCliente:data.send.IdCliente, 
		    					latOrig:data.send.latOrig,
		    					latDest:data.send.latDest,
		    					lngOrig:data.send.lngOrig, 
		    					lngDest:data.send.lngDest,
		    					Origen:data.send.Origen,
		    					Destino:data.send.Destino,
		    					dirOrigen:data.send.dirOrigen,
                                dirDestino:data.send.dirDestino,
                                refOrigen:data.send.refOrigen,
                                refDestino:data.send.refDestino,
                                tipoServicio:data.send.tipoServicio,
                                tarifa:data.send.tarifa, 
                                socketCliente:socketCliente,
                                ServicioId:data.servicioId,
                                socketConductor:data.send.socketConductor,
                                tipo_pago:data.send.tipo_pago
                                                    });

		    			SessionServiceId = data.servicioId;
		    			console.log('Se envio el emit al Conductor');
		    			DialogoTimer();

		    		}else{
		    			console.log('Erro al insertar');
		    		}
		    	
		     	}
    	});
    }
});

	init_mapa();
	LlenarZonas();

	$("#celular").keyup(function(){
		var cant = $(this).val().length;
		if(cant <= 0){
			flag = 1;
			clearForm(flag);
		}
    });

    $("#nombres").keyup(function(){
    	var cant = $(this).val().length;
    	if(cant <= 0){
    		flag = 2;
    		clearForm(flag);
    	}
    });

    $("#reforigen").keyup(function(){
    	var cant = $(this).val().length;
    	if(cant <= 0){
    		flag = 3;
    		clearForm(flag);
    	}
    });

    $("#refdestinno").keyup(function(){
    	var cant = $(this).val().length;
    	if(cant <= 0){
    		flag = 4;
    		clearForm(flag);
    	}
    });

	//LISTAR CLIENTES / CELULARES SEARCH
    $( "#celular" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "BL/clientesController.php",
          method: 'post',
          data: {q: request.term, autocom:'autocom'},
          success: function( data ) {
            response(data);
          }
        });
      },
      minLength: 2,
      select: function( event, ui ) {
         if (ui.item) {
         		console.log(ui.item);
                $("#cliente").val(ui.item.cliente);
                $("#idcl").val(ui.item.idCliente);
                $("#empid").val(ui.item.emp);
                $("#correo").val(ui.item.correo);
                $("#apecliente").val(ui.item.ape);
                document.getElementById("correo").disabled = true;
            }
      }
    });

	socket = io.connect(URL_CONTROLLER_SOCKET);
	socket.emit('dataMapConductorsWeb',{estado: 'all'});

	socket.on('connect', function(sockett){
		console.log('socket web'+socket.id);
		socket.emit('SocketWeb',{socketweb: socket.id});
	});

	socket.on('listaConductors',function(result){
		conductors = result;
		for (var i = 0; i < result.length; i++) {
		 		console.log('Total de conductores inicial : '+result.length);
			 	datos = {
	        			Nombre:result[i].conductor_nombre, 
	        			Apellido:result[i].conductor_apellido, 
	        			Foto:result[i].conductor_foto, 
	        			Estado:result[i].conductor_estado,
	        			Celular:result[i].conductor_celular,
	        			Placa:result[i].unidad_placa,
	        			Marca:result[i].unidad_marca,
	        			Servicio:result[i].tiposervicio_nombre,
	        			Socket:result[i].conductor_socket
	        			};
	        	idsConductorDisponibles.push(result[i].conductor_id);
	        	addMarker(result[i].conductor_id,result[i].conductor_lat,result[i].conductor_lng,map,datos);		
		}

	});

	socket.on('refreshCoordinates', function(data){

		//console.log(data);
		//var data = JSON.parse(JSON.stringify(data));
		if(data.socketMiss){
			//console.log('remove markers');
			marker[data.idConductor].setMap(null);
			delete marker[data.idConductor];
		}

		if(data.socket_conductor != null){

			if(marker[data.idConductor]){
			
				//console.log(data.idConductor+'--'+data.latitud+'---'+data.longitud);
				var latitud = parseFloat(data.latitud);
				var longitud = parseFloat(data.longitud)
				var newLatLng = new google.maps.LatLng(latitud,longitud);
				marker[data.idConductor].setPosition(newLatLng);
			}else{

				//console.log('nuevo conductor');
				var esrefresh = (data.stateConductor == 1)?'Disponible':'Ocupado';
				datos = {
	        			Nombre:data.nombre, 
	        			Apellido:data.apellido, 
	        			Foto:data.foto, 
	        			Estado:esrefresh,
	        			Celular:data.celular,
	        			Placa:data.placa,
	        			Marca:data.marca,
	        			Servicio:data.servicio
	        			};

	        	idsConductorDisponibles.push(data.idConductor);
				addMarker(data.idConductor,data.latitud,data.longitud,map,datos);
			}
		}
	});

	socket.on('SolicitudAceptar',function(docs){
		console.log('respondio Javier Quiroz');
		console.log(docs);
		seleccionado = docs;
		Rptadocs = true; 
	});
	
});

function clearForm(flag)
{
	if(flag == 1){
		$("#idcl").val('');
		$("#cliente").val('');
		$("#correo").val('');
		document.getElementById("correo").disabled = false;
	}else if(flag == 2){
		$("#idco").val('');
	}else if(flag == 3){
		$("reforilat").val('');
	    $("#reforilng").val('');
	    $("origenlugar").val('');
	}else if(flag == 4){
		$("refdeslat").val('');
	    $("#refdeslng").val('');
	    $("destinolugar").val('');
	}
}




function init_mapa() {

	geocoder = new google.maps.Geocoder();

	//var bangalore = {lat: -12.046374, lng: -77.0427934};
    var bangalore = {lat: <?=$result[0]['usuario_latitud'];?>, lng: <?=$result[0]['usuario_longitud']?>};
    map = new google.maps.Map(document.getElementById('gmap_despacho'), {
      zoom: 16,
      center: bangalore,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
 
    var inputorigen = (document.getElementById('reforigen'));
    var autocompleteorigen = new google.maps.places.Autocomplete(inputorigen);
  		autocompleteorigen.bindTo('bounds', map);
  	markerorigen = new google.maps.Marker({map: map,anchorPoint: new google.maps.Point(0, -29),draggable: true});

  	var inputdestino = (document.getElementById('refdestinno'));
  	var autocompletedestino = new google.maps.places.Autocomplete(inputdestino);
  		autocompletedestino.bindTo('bounds', map);
  	var markerdestino = new google.maps.Marker({map: map,anchorPoint: new google.maps.Point(0, -29),draggable: true});

  	  // Create the search box and link it to the UI element.
	  //var input = document.getElementById('pac-input');
	var searchBoxOrigen = new google.maps.places.SearchBox(inputorigen);
	var searchBoxDestino = new google.maps.places.SearchBox(inputdestino);
	  //map.controls[google.maps.ControlPosition.TOP_LEFT].push(inputorigen);

	  // Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBoxOrigen.setBounds(map.getBounds());
		searchBoxDestino.setBounds(map.getBounds());
	});

	// var markers = [];
  	// [START region_getplaces]
  	// Listen for the event fired when the user selects a prediction and retrieve
  	// more details for that place.
  	searchBoxOrigen.addListener('places_changed', function() {

  		markerorigen.setVisible(false);
	    var places = searchBoxOrigen.getPlaces();

	    if (places.length == 0) {
	      return;
	    }
	    // For each place, get the icon, name and location.
	    var bounds = new google.maps.LatLngBounds();
	    places.forEach(function(place) {

	      /*var icon = {
	          url: 'includes/img/ic_cliente_origen.png',
	          size: new google.maps.Size(42, 65),
		      origin: new google.maps.Point(0, 0),
		      anchor: new google.maps.Point(17, 34),
		      scaledSize: new google.maps.Size(42, 65)
	      };

	      // Create a marker for each place.
	      markers.push(new google.maps.Marker({
	        map: map,
	        icon: icon,
	        title: place.name,
	        position: place.geometry.location
	      }));*/

	      	markerorigen.setIcon(({
		      url: 'includes/img/ic_cliente_origen.png',
		      size: new google.maps.Size(42, 65),
		      origin: new google.maps.Point(0, 0),
		      anchor: new google.maps.Point(17, 34),
		      scaledSize: new google.maps.Size(42, 65)
		    }));

	   		markerorigen.setPosition(place.geometry.location);
	   		markerorigen.setVisible(true);

	   		$('#reforilat').val(place.geometry.location.lat);
	    	$('#reforilng').val(place.geometry.location.lng);

		      if (place.geometry.viewport) {
		        // Only geocodes have viewport.
		        bounds.union(place.geometry.viewport);
		      } else {
		        bounds.extend(place.geometry.location);
		      }

		   	console.log('ingrese placer origen');
			var latOri = $('#reforilat').val();
	    	var lngOri = $('#reforilng').val();
	    	ObtenerIdZonas(latOri,lngOri,'origen');		
	    	ConsultarTarifaServicio();

	    });
	    map.fitBounds(bounds);
	 });

  	 searchBoxDestino.addListener('places_changed', function() {

  		markerdestino.setVisible(false);
	    var places = searchBoxDestino.getPlaces();

	    if (places.length == 0) {
	      return;
	    }
	    // For each place, get the icon, name and location.
	    var bounds = new google.maps.LatLngBounds();
	    places.forEach(function(place) {

	      /*var icon = {
	          url: 'includes/img/ic_cliente_origen.png',
	          size: new google.maps.Size(42, 65),
		      origin: new google.maps.Point(0, 0),
		      anchor: new google.maps.Point(17, 34),
		      scaledSize: new google.maps.Size(42, 65)
	      };

	      // Create a marker for each place.
	      markers.push(new google.maps.Marker({
	        map: map,
	        icon: icon,
	        title: place.name,
	        position: place.geometry.location
	      }));*/

	     markerdestino.setIcon(({
	      url: 'includes/img/ic_cliente_destino.png',
	      size: new google.maps.Size(42, 65),
	      origin: new google.maps.Point(0, 0),
	      anchor: new google.maps.Point(17, 34),
	      scaledSize: new google.maps.Size(42, 65)
	    }));

	    markerdestino.setPosition(place.geometry.location);
	    markerdestino.setVisible(true);
	    $('#refdeslat').val(place.geometry.location.lat);
	    $('#refdeslng').val(place.geometry.location.lng);

	      if (place.geometry.viewport) {
	        // Only geocodes have viewport.
	        bounds.union(place.geometry.viewport);
	      } else {
	        bounds.extend(place.geometry.location);
	      }

	   	console.log('ingrese placer destino');
  		var latDes = $('#refdeslat').val();
		var lngDes = $('#refdeslng').val();
		ObtenerIdZonas(latDes,lngDes,'destino');
		ConsultarTarifaServicio();

	    });
	    map.fitBounds(bounds);
	 });
	//Autocomplete de Referencia Origen  
  	/*autocompleteorigen.addListener('place_changed', function() {

	    markerorigen.setVisible(false);
	    var place = autocompleteorigen.getPlace();
	    if (!place.geometry) {
	      window.alert("Ingresar una referencia de origen real");
	      return;
	    }

	    if (place.geometry.viewport) {
	      map.fitBounds(place.geometry.viewport);
	    } else {

	      map.setCenter(place.geometry.location);
	      //map.setZoom(16); 
	    }

	    markerorigen.setIcon(({
	      url: 'includes/img/ic_cliente_origen.png',
	      size: new google.maps.Size(42, 65),
	      origin: new google.maps.Point(0, 0),
	      anchor: new google.maps.Point(17, 34),
	      scaledSize: new google.maps.Size(42, 65)
	    }));

	    markerorigen.setPosition(place.geometry.location);
	    markerorigen.setVisible(true);

	    $('#reforilat').val(place.geometry.location.lat);
	    $('#reforilng').val(place.geometry.location.lng);
	    
    	console.log('ingrese placer origen');
		var latOri = $('#reforilat').val();
    	var lngOri = $('#reforilng').val();
    	ObtenerIdZonas(latOri,lngOri,'origen');		
    	ConsultarTarifaServicio();

  	});*/

  	//Autocomplete de Referencia Destino
  	/* autocompletedestino.addListener('place_changed', function() {

	    markerdestino.setVisible(false);
	    var place = autocompletedestino.getPlace();
	    if (!place.geometry) {
	      window.alert("Ingresar una referencia de origen real");
	      return;
	    }

	    if (place.geometry.viewport) {
	      map.fitBounds(place.geometry.viewport);
	    } else {
	      map.setCenter(place.geometry.location);
	      //map.setZoom(16); 
	    }

	    markerdestino.setIcon(({
	      url: 'includes/img/ic_cliente_destino.png',
	      size: new google.maps.Size(42, 65),
	      origin: new google.maps.Point(0, 0),
	      anchor: new google.maps.Point(17, 34),
	      scaledSize: new google.maps.Size(42, 65)
	    }));

	    markerdestino.setPosition(place.geometry.location);
	    markerdestino.setVisible(true);
	    $('#refdeslat').val(place.geometry.location.lat);
	    $('#refdeslng').val(place.geometry.location.lng);

    	console.log('ingrese placer destino');
	  	var latDes = $('#refdeslat').val();
    	var lngDes = $('#refdeslng').val();
    	ObtenerIdZonas(latDes,lngDes,'destino');

    	ConsultarTarifaServicio();

  	});*/

  	google.maps.event.addListener(markerorigen, 'dragend', function(event) {

        	var tipo = 'origen';
	    	latOrigen = event.latLng.lat();
            lonOrigen = event.latLng.lng();
            $("#reforilat").val(latOrigen);
            $("#reforilng").val(lonOrigen);
            obtenerAddresReferencia(latOrigen,lonOrigen,tipo);
            ObtenerIdZonas(latOrigen,lonOrigen,tipo);

            ConsultarTarifaServicio();
    });

    google.maps.event.addListener(markerdestino, 'dragend', function(event) {

        	var tipo = 'destino';
	    	latDestino = event.latLng.lat();
            lonDestino = event.latLng.lng();
            $("#refdeslat").val(latDestino);
            $("#refdeslng").val(lonDestino);
            obtenerAddresReferencia(latDestino,lonDestino,tipo);
            ObtenerIdZonas(latDestino,lonDestino,tipo);

            ConsultarTarifaServicio();
    });

}



function obtenerAddresReferencia(lat,lng,tipo){
	
    var latLng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latLng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                var address = results[0].formatted_address;
                if (address.indexOf(',') != -1) {
                    var add = address.split(',');
                    address = add[0];
                }

                if(tipo == 'origen'){
                	$("#reforigen").val(address);
                }else{
                	$("#refdestinno").val(address);
                }
            }
        } else {
            console.log("No se pudo obtener la dirección");
        }
    });
}


function addMarker(idConductor, lat, lng, map, datos) {

    	var latitud = parseFloat(lat);
        var longitud = parseFloat(lng);

	    image = new google.maps.MarkerImage('includes/img/carrito-verde.png',
	      new google.maps.Size(50, 50),
	      new google.maps.Point(1,1),
	      new google.maps.Point(1, 1)
		);

        marker[idConductor] = new google.maps.Marker({
	   	  icon : image,
	      position: {lat: latitud, lng: longitud},
	      map: map
	    });

	    markers.push(marker[idConductor]);
        marker[idConductor].setMap(map);

         var contentString = '<div id="content">'+
						      '<div id="siteNotice">'+
						      '</div>'+
						      '<p class="firstHeading"><b>'+datos.Nombre+' '+datos.Apellido+'</b></p>'+
						      '<div id="bodyContent">'+
						      '<div style="width:100%;display: inline-block;">'+
						      '<div style="float:left;margin-right: 15px;">'+
						      '<img src="BL/'+datos.Foto+'" class="img-thumbnail" width="100" height="100">'+
						      '</div>'+
						      '<div style="float:right">'+
						      '<lalel><b>Nro.Placa : </b>'+datos.Placa+'</label><br>'+
						      '<lalel><b>Marca : </b>'+datos.Marca+'</label><br>'+
						      '<lalel><b>Telefono : </b>'+datos.Celular+'</label><br>'+
						      '<lalel><b>Servicio : </b>'+decodeURIComponent(escape(datos.Servicio))+'</label>'+
						      '</div>'+	
						      '</div>'+
						      '</div>'+
						      '</div>';
		infowindow[idConductor] = new google.maps.InfoWindow({
		    content: contentString
		});

        marker[idConductor].addListener('click', function() {
        	var nomconductor = datos.Nombre+' '+datos.Apellido;
        	$("#idco").val(idConductor);
        	$("#nombres").val(nomconductor);
    		infowindow[idConductor].open(map, marker[idConductor]);
  	 	});
}

function Serviciotipo(servicetipo){

	if(servicetipo != 0 ){
		console.log(idsConductorDisponibles.length);
		$.each(idsConductorDisponibles,function(index,obj)
	    {	
	    	marker[obj].setMap(null);
	    });

		socket.emit('dataMapConductorsWeb',{estado: servicetipo});

		ConsultarTarifaServicio();
	}
}

function ConsultarTarifaServicio(){

	var tipo_config = $('#configId').val();
	console.log('tipo configuracion...'+tipo_config);
	if(tipo_config != 2){

		var origen = $("#origenlugar").val();
		var destino = $("#destinolugar").val();
		var tipo = $("#tiposervicio_id").val();
		var empresa = $("#empid").val();

		console.log('origen'+origen);
		console.log('destino'+destino);
		console.log('tipo'+tipo);

		if(empresa.length == 0){
			empresa = 0;
		}

		if(origen.length > 0 && destino.length > 0 && tipo != 0){

			socket.emit('setServicioTarifaCliente',{origen_id:origen,destino_id:destino,tiposervice:tipo,empresaid:empresa});
			socket.on('Tarifacalculada',function(result){
				console.log(result.length);
				$("#costo").val('');
				if(result.length > 0){
					$("#costo").val(result[0].tarifario_monto);
				}
			});
		
		}else{
			console.log('Falta Ingresar un dato....');
		}
	}else{

		var latOri = $('#reforilat').val();
    	var lngOri = $('#reforilng').val();
    	var latDes = $('#refdeslat').val();
    	var lngDes = $('#refdeslng').val();

    	var tipo = $("#tiposervicio_id").val();

    	var nombreOrigen = $("#reforigen").val();
    	var nombreDestin = $("#refdestinno").val();

    	var siorigen = nombreOrigen.indexOf("Aeropuerto");
    	var sidestin = nombreDestin.indexOf("Aeropuerto");
    	var aeropuerto = '';
    	if(sidestin == 0 && siorigen != 0 || sidestin != 0 && siorigen == 0){
    		aeropuerto = 1;
    	}else{
    		aeropuerto = 2;
    	}

    	if(latOri.length > 0 && lngOri.length > 0 && latDes.length > 0 && lngDes.length > 0 && tipo != 0){

    	$.ajax({
		    url : "../ws/cliente/ws_consultar_tarifa_panel_kilometraje.php",
		    type: "POST",
		    async: true,
	        cache: false,
		    data : {latOri:latOri,lngOri:lngOri,latDes:latDes,lngDes:lngDes,tiposervicio_id:tipo,reserva:0,activo:aeropuerto},
	    success: function(data)
	    	{	
	    		var obj = JSON.parse(data);
	    		console.log(obj.status);
	    		if(obj.status == true){
	    			$("#costo").val(parseFloat(obj.tarifa));
	    		}
	      	}
    	});
          
         /* var origin1 = new google.maps.LatLng(latOri, lngOri);
          var destinationB = new google.maps.LatLng(latDes,lngDes);

          var service = new google.maps.DistanceMatrixService();
          service.getDistanceMatrix(
            {
              origins: [origin1],
              destinations:  [destinationB],
              travelMode: google.maps.TravelMode.DRIVING,
              unitSystem: google.maps.UnitSystem.METRIC,
              avoidHighways: false,
              avoidTolls: false,
            }, callback);
      
          function callback(response, status) {
            console.log('call ',response,status);
            var originList = response.originAddresses;
            var destinationList = response.destinationAddresses;

              for (var i = 0; i < originList.length; i++) {
                  var results = response.rows[i].elements;
                  for (var j = 0; j < results.length; j++) {
                    var tiempos = results[j].duration.text.split('min');
                    var distancia = results[j].distance.text.split('km');
					SearchCostoKilometraje(distancia[0],tiempos[0],tipo);
                  }
              }

          }*/

    	}else{
    		console.log('Falta Ingresar un dato Kilometraje....');
    	}
	}

}

function SearchCostoKilometraje(distancia,tiempo,tiposervicio){

	    $.ajax({
		    url : "../ws/cliente/ws_consultar_tarifa_panel_kilometraje.php",
		    type: "POST",
		    async: true,
	        cache: false,
		    data : { distancia:distancia,tiempo:tiempo,tiposervicio_id:tiposervicio,reserva:0},
	    success: function(data)
	    	{	
	    		var obj = JSON.parse(data);
	    		console.log(obj);
	    		if(obj.status == true){
	    			$("#costo").val(parseInt(obj.tarifa));
	    		}
	      	}
    	});
}

function ObtenerIdZonas(lat,lng,tipo) {

    var LatLonMOV = new google.maps.LatLng(lat, lng);

    for (var i = 0; i < lugares.length; i++) {
        var path = [];
        var idlugar = lugares[i].lugares_id;
        var coordlugar = lugares[i].new;
        var lugar = lugares[i].lugar_descripcion;

        if (coordlugar.length > 1) {
            for (var x = 0; x < coordlugar.length; x++) {
                coordlugar[x].lat= parseFloat(coordlugar[x].lat);
                coordlugar[x].lng = parseFloat(coordlugar[x].lng);
            }

            poligono = new google.maps.Polygon({
                paths: coordlugar
            });
 			
 			if(google.maps.geometry.poly.containsLocation(LatLonMOV, poligono) == true) {
 				if(tipo == 'origen'){
            		$("#origenlugar").val(idlugar);
            	}else{
            		$("#destinolugar").val(idlugar);
            	}
			}
        }
    }
}

function LlenarZonas() {

    $.ajax({
	    url : "../BL/lugaresController.php",
	    type: "POST",
	    async: true,
        cache: false,
	    data : { hidden_lugar_map: 'hidden_lugar_map'},
	    success: function(data)
	    	{	
	    		lugares = data;
	      	}
    	});
}

function DialogoTimer(){

	var dialog, formulario;
	var tiempo = 20;
	var contador = 0;
	var intervalTimer;

    $( "#dialog-confirm" ).dialog({
    	width: 500,
      	resizable: false,
      	closeText: "hide",
      	height: "auto",
      	modal: true,
      	dialogClass: "no-close",
      	title: 'Espera de Confirmación de Servicio'
    });

    intervalTimer = setInterval(function () {
        contador = contador + 1;
        if (contador <= tiempo) {
            if((tiempo - contador) < 20) {
                cambiarDivTimer((tiempo - contador));
            }

            if (contador % 2 == 0) {
               console.log('1 segundos...'+Rptadocs);
                if(Rptadocs == true){
                	console.log('Conductor respondio la solicitud');
                	if(SessionServiceId == seleccionado.ServicioId){
                		var arr = {idservicio:SessionServiceId,idconductor:seleccionado.IdConductor,tipo:0,idarray:null};
                		var objetc = JSON.stringify(arr);
                		socket.emit('SeleccionConductor',objetc);
                  		$( "#dialog-confirm" ).dialog( "close" );
			            clearInterval(intervalTimer);
			            $("#divContador").html(20);
			            var nombreConfirmado = $("#nombres").val();
			            clearFormulario();
			            diaglogViewService(seleccionado.ServicioId,nombreConfirmado);
			            SessionServiceId='';

                	}
                }
            }
        } else {
        	clear = 2;
			$('#nombres').val('');
			$('#idco').val('');
            $( "#dialog-confirm" ).dialog( "close" );
            clearInterval(intervalTimer);
            $("#divContador").html(20);
        }
    }, 1000);

}

/*$("#formDespacho").validate({

    submitHandler: function(form) {
    	
    	var str = $( "#formDespacho" ).serialize();
    	var flag = '';
    	if(clear == 2){
    		console.log('elegir a otro conductor');
    		flag = 'edit';
    	}else{
    		flag = 'new';
    	}
    	$.ajax({
		    url : "../BL/servicioController.php",
		    type: "POST",
		    async: true,
	        cache: false,
		    data : str+'&hidden_servicio=hidden_servicio&flag='+flag,
		    success: function(data)
		    	{	
		    		if(data.status == true){
		    			console.log('socket web panel '+ socket.id);
		    			var socketCliente = '';
		    			var cantcliente = $("#idcl").val().length;
					    	if(cantcliente <= 0){
					    		socketCliente = socket.id;
					    	}else{
					    		if(data.send.socketCliente != null){
					    			socketCliente = data.send.socketCliente;
					    		}else{
					    			socketCliente = socket.id;
					    		}
					    	}
		    			

		    			socket.emit('nuevaSolicitudConductorWeb',{
		    					IdCliente:data.send.IdCliente, 
		    					latOrig:data.send.latOrig,
		    					latDest:data.send.latDest,
		    					lngOrig:data.send.lngOrig, 
		    					lngDest:data.send.lngDest,
		    					Origen:data.send.Origen,
		    					Destino:data.send.Destino,
		    					dirOrigen:data.send.dirOrigen,
                                dirDestino:data.send.dirDestino,
                                refOrigen:data.send.refOrigen,
                                refDestino:data.send.refDestino,
                                tipoServicio:data.send.tipoServicio,
                                tarifa:data.send.tarifa, 
                                socketCliente:socketCliente,
                                ServicioId:data.servicioId,
                                socketConductor:data.send.socketConductor
                                                    });

		    			SessionServiceId = data.servicioId;
		    			console.log('Se envio el emit al Conductor');
		    			DialogoTimer();

		    		}else{
		    			console.log('Erro al insertar');
		    		}
		    	
		     	}
    	});
    }
});*/

function diaglogViewService(serviceID,nombreConductor){

	$("#textconfirma").html('El servicio Nº-'+serviceID+' ha sido asignado al conductor '+nombreConductor);
	$( "#dialog-service" ).dialog({
    	width: 500,
      	resizable: false,
      	closeText: "hide",
      	height: "auto",
      	modal: true,
      	dialogClass: "no-close",
      	title: 'Servicio Exitoso',
      	buttons: {
	        Aceptar: function() {
	          $( this ).dialog( "close" );
	        }
      }
    });
}

function clearFormulario(){

	$("#idco").val('');
	$("#idcl").val('');
	$("#empid").val('');
	$("#celular").val('');
	$("#cliente").val('');
	$("#correo").val('');
	$("#reforigen").val('');
	$("#reforilat").val('');
	$("#reforilng").val('');
	$("#origenlugar").val('');
	$("#refdestinno").val('');
	$("#refdeslat").val('');
	$("#refdeslng").val('');
	$("#destinolugar").val('');
	$("#costo").val('');
	$("#nombres").val('');
	$('select[name=tipopago]').val(0);
	$("#").focus();

}

function cambiarDivTimer(texto) {
    $("#divContador").html(texto);
}

</script>

<style type="text/css">
	#dialog-confirm .ui-dialog-titlebar-close{
		display: none;
	}

    #divContador {
        font-size: 65px;
        text-align: center;
        margin-top: 40px;
        padding-left: 10px;
        padding-right: 10px;
        font-family: Tahoma;
    }
</style>

<div id="dialog-confirm" title="Empty the recycle bin?" style="display:none;">
 	<div id="divContador">
	    20
    </div>
</div>
<div id="dialog-service" style="display:none;">
	<p><span class="ui-icon ui-icon-circle-check" style="float:left; margin:12px 12px 20px 0;">
	</span><label id="textconfirma">These items will be permanently deleted and cannot be recovered. Are you sure?</label></p>
</div>