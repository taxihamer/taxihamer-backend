<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include'seguridad.php';
require_once'DAL/conductorDAO.php';
require_once'DAL/tipoServicioDAO.php';
require_once'DAL/vehiculoDAO.php';
require_once'DAL/constantes.php';

$tipoServicioDAO = new tipoServicioDAO();
$result_tipo = $tipoServicioDAO->lista();
$conductorDAO = new conductorDAO();
$result = $conductorDAO->lista();
$vehiculoDAO = new vehiculoDAO();
$result_vehiculo = $vehiculoDAO->listaDropdown();

?>
<style type="text/css">
  .loading {border: 1px solid #ddd;padding: 20px;margin: 40px 5px;}
  .modal-content {padding: 0px 25px 0px 25px;}
  #mtncd , #mtncdedit{display: none;}
</style>
<script type="text/javascript">
  window.URL = window.URL || window.webkitURL;
  $(document).ready(function () {
    $('.ang').hide();

    $("#image-file").change(function(e) {

        var fileInput = $("input[type=file]")[0],
        file = fileInput.files && fileInput.files[0];

          if( file ) {
              var img = new Image();
              img.src = window.URL.createObjectURL( file );

              img.onload = function() {
                  var width = img.naturalWidth,
                      height = img.naturalHeight;

                  window.URL.revokeObjectURL( img.src );

                  if( width <= 180 && height <= 180 ) {
                     $("#msfile").css({"color":"#737373"});
                  }
                  else {
                      var control = $('#image-file');
                      control.replaceWith( control = control.val('').clone( true ) );
                      $("#msfile").css({"color":"#dd4b39"});
                  }
              };
          }
    });

  });
</script>
<div class="row" ng-app="myapp" ng-controller="conductorControllerEdit">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="alert alert-danger" id="msgError" style="display:none">
        <button type="button" class="close" ng-click="CloseConductor()">&times;</button>
        <strong>¡Cuidado!</strong> {{messageError}}
      </div>
      <div class="alert alert-success" id="msgSuccess" style="display:none">
        <button type="button" class="close" ng-click="CloseConductorSucces()">&times;</button>
        <strong>¡Éxito!</strong> {{messageSuccess}}
      </div>
      <div class="box-header with-border">
        <h3 class="box-title">Registrar Conductor</h3>
      </div><!-- /.box-header "BL/conductorController.php -->
      <!-- form start -->
      <form id="formuploadajaxEdit" class="form-horizontal" enctype="multipart/form-data" method="post" role="form">
        <input type="hidden" name="conductorid" id="conductorid" value="<?=$_SESSION['id']?>">
        <div class="box-body">
          <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" required>
            <input type="hidden" class="form-control" name="hidden_insert_conductor" value="1">
          </div>
          <div class="form-group">
            <label for="apellido">Apellido</label>
            <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apellido" required>
          </div>
          <div class="form-group">
            <label for="email">Correo Electrónico</label>
            <input type="email" class="form-control" name="correo" id="correo" placeholder="Email" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" class="form-control" name="password"  id="password" placeholder="Password" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="docidentidad">Doc. Identidad</label>
            <input type="text" class="form-control" name="dni" id="dni" placeholder="Documento de Identidad">
          </div>
          <div class="form-group">
            <label for="celular">Nro. Celular</label>
            <input type="text" class="form-control" name="celular" id="celular" placeholder="Nro de Celular">
          </div>
          <div class="form-group">
            <label for="ciudad">Ciudad</label>
            <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ciudad">
          </div>
          <div class="form-group">
            <label for="distrito">Distrito</label>
            <input type="text" class="form-control" name="distrito" id="distrito" placeholder="Distrito">
          </div>
          <div class="form-group">
            <label for="nrolicencia">Nro. Licencia</label>
            <input type="text" class="form-control" name="licencia" id="licencia" placeholder="Nro de licencia">
          </div>
          <div class="form-group">
            <label for="catlicencia">Categoria de Licencia</label>
            <input type="text" class="form-control" name="catlicencia" id="catlicencia" placeholder="Categoria de la licencia">
          </div>
          <div class="form-group">
            <label for="catlicencia">Fecha Vencimiento Brevete</label>
            <input type="date" class="form-control" name="fecha" id="brevete">
          </div>
          <div class="form-group">
            <small>Foto Actual</small>
            <div id="foto2">
              <img src="" alt="taxi" id="fotoview" class="img-responsive" />
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Foto del Conductor</label>
            <input type="file" name="foto" class="form-control" id="image-file">
            <p class="help-block" id="msfile">Tamaño 180x180 pixeles.</p>
          </div>
          <div class="form-group">
            <label for="soat">Fecha SOAT</label>
            <input type="date" class="form-control" name="soat" id="soat">
          </div>
          <div class="form-group">
            <small>Foto del Brevete</small>
            <div id="fotosoat">
              <img src="" alt="taxi" id="fotoviewsoat" class="img-responsive" />
            </div>
          </div>
          <div class="form-group">
            <label for="foto1">Foto del Brevete</label>
            <input type="file" name="foto1" id="foto1" class="form-control">
          </div>
          <div class="form-group">
            <label for="soat">Fecha Carnet Vial</label>
            <input type="date" class="form-control" name="carnetvial" id="carnetvial">
          </div>
          <div class="form-group">
            <small>Foto Carnet Vial</small>
            <div id="fotocarnet">
              <img src="" alt="taxi" id="fotoviewcarnet" class="img-responsive" />
            </div>
          </div>
          <div class="form-group">
            <label for="foto2">Foto de Carnet Vial</label>
            <input type="file" name="foto_2" id="foto_2" class="form-control">
          </div>
          <div class="form-group">
            <label for="soat">Codigo de lunas Polarizadas</label>
            <input type="text" class="form-control" name="polarizadas" id="polarizadas">
          </div>
          <div class="form-group">
            <label for="banco">Entidad Financiera</label>
            <select ng-model="select_defaultEdit" class="form-control" id="select_defaultEdit" ng-options="c.key as c.value for c in bancosEdit" required></select>
          </div>
          <div class="form-group">
            <label for="soat">Codigo de CCI</label>
            <input type="number" class="form-control" name="cii_banco" id="cii_banco">
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
           <button type="submit" class="btn btn-primary pull-right" ng-click="updateSthEdit()">Guardar Cambios</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  </div>
<script type="text/javascript">

var idConductor = $("#conductorid").val();
$(document).ready(function(){

        $.ajax({
              url : "../BL/conductorController.php",
              type: "POST",
              data : { idConductor: idConductor, iframe:'iframe'},
              success: function(data)
              { 
                  //conductor_foto
                $("#nombre").val(data[0].conductor_nombre);
                $("#apellido").val(data[0].conductor_apellido);
                $("#correo").val(data[0].conductor_correo);
                $("#password").val(data[0].conductor_password);
                $("#dni").val(data[0].conductor_dni);
                $("#celular").val(data[0].conductor_celular);
                $("#ciudad").val(data[0].conductor_ciudad);
                $("#distrito").val(data[0].conductor_distrito);
                $("#licencia").val(data[0].conductor_licencia);
                $("#catlicencia").val(data[0].conductor_catlicencia);
                $("#brevete").val(data[0].conductor_fechabrevete);
                var fotoactual = 'http://45.55.92.243/BL/'+data[0].conductor_foto;
                $("#fotoview").attr('src',fotoactual);

                var fotoactualsoat = 'http://45.55.92.243/BL/'+data[0].conductor_fotosoat;
                $("#fotoviewsoat").attr('src',fotoactualsoat);

                var fotoactualcarnet = 'http://45.55.92.243/BL/'+data[0].conductor_fotocarnetvial;
                $("#fotoviewcarnet").attr('src',fotoactualcarnet);

                $("#cii_banco").val(data[0].conductor_cci);
                $("#polarizadas").val(data[0].conductor_polarizadas);
                $("#carnetvial").val(data[0].conductor_fechacarnetvial);
                $("#soat").val(data[0].conductor_fechasoat);
                
                var banco = 'string:'+data[0].banco_id;
                $("#select_defaultEdit").val(banco);
              }
        });
});

</script>
<style type="text/css">
  #dialog-delete .ui-dialog-titlebar-close{
    display: none;
  }

  #textconfirma {
      font-size: 20px;
      text-align: center;
      margin-top: 40px;
      padding-left: 10px;
      padding-right: 10px;
      font-family: Tahoma;
  }

  .form-horizontal .form-group {
    margin-right: 0px !important;
    margin-left: 0px !important;
  }
</style>