<?php
  include'seguridad.php';
  require_once'DAL/menuDAO.php';
  require_once'DAL/rolDAO.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

  $rolDAO = new rolDAO();
  $result = $rolDAO->listmenurolid($_SESSION['rol_id']);

  $menuDAO = new menuDAO();
  $data['menurol_id'] = $result[0]['menurol_id'];
  $listmenu = $menuDAO->listaoperadoresById($data);
  $menu_parent = (!empty($listmenu[0]['menurol_jsonparent']))?json_decode($listmenu[0]['menurol_jsonparent']):'';
  $menu_children = (!empty($listmenu[0]['menurol_jsonchildren']))?json_decode($listmenu[0]['menurol_jsonchildren']):'';
?>
<style>
/*.fa-car:before {
     content:url(dist/img/menu/ic_conductor.svg);
  }*/
</style>
<!-- Sidebar user panel (optional) -->
<div class="user-panel">
  <div class="pull-left image">
    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
  </div>
  <div class="pull-left info">
    <p><?php echo $_SESSION['user']; ?></p>
    <!-- Status -->
    <a href="#"><i class="fa fa-unlock-alt text-success" <?=$_SESSION['rol_id']?> ></i> <?php echo $_SESSION['rol']; ?></a>
  </div>
</div>

<!-- Sidebar Menu -->
<?php if(!empty($menu_parent)){ ?>

<ul class="sidebar-menu">
  <li class="header">MENU PRINCIPAL</li>
  <?php foreach ($menu_parent as $key => $menuid) {
        $menudata = $menuDAO->listmenuId($menuid);
        if(!empty($menudata[0]['menu_url'])){ ?>
          <li><a href="index.php?seccion=<?=$menudata[0]['menu_url'];?>"><i class="<?=$menudata[0]['menu_icon'];?>"></i> <span><?=$menudata[0]['menu_nombre'];?></span></a></li>
       <?php }else{ ?>
          <li class="treeview">
            <a href="#"><i class="<?=$menudata[0]['menu_icon'];?>"></i> <span><?=$menudata[0]['menu_nombre'];?></span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
            <?php 
              $resultsub = $menuDAO->listsubmenuId($menudata[0]['menu_id']);
              if(!empty($resultsub)){
                foreach ($resultsub as $key => $submenu) {
                  foreach ($menu_children as $i => $child) { 
                    if($submenu['submenu_id'] == $child){ ?>
                      <li><a href="index.php?seccion=<?=$submenu['submenu_url']?>"><?=$submenu['submenu_nombre']?></a></li>
                   <?php }
                     ?>
                <?php   }
                  ?>
                 
            <?php }
              } ?>
            </ul>
          </li>
       <?php } ?>
  <?php } ?>
</ul><!-- /.sidebar-menu -->
<?php } ?>