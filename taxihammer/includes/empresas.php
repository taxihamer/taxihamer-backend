<?php
  include'seguridad.php';
  require_once'DAL/empresaDAO.php';

  $empresaDAO = new empresaDAO();
  $empresa = $empresaDAO->listempresa();

?>
<style type="text/css">
  .nullOld{text-align: center;color: #e23558;}

  .floatRight{
    float:right;
    margin-right: 18px;
  }

  .has-error{
    color:red;
  }

  .formcontainer{
    background-color: #DAE8E8;
    padding: 20px;
  }

  .tablecontainer{
    padding-left: 20px;
  }

  .generic-container {
    width:80%;
    margin-left: 20px;
    margin-top: 20px;
    margin-bottom: 20px;
    padding: 20px;
    background-color: #EAE7E7;
    border: 1px solid #ddd;
    border-radius: 4px;
    box-shadow: 0 0 30px black;
  }

  .custom-width {
      width: 80px !important;
  }

  .email.ng-valid{background-color: lightgreen;}
  .ruc.ng-valid{background-color: lightgreen;}

  .codigo.ng-dirty.ng-invalid-required{background-color: red;}
  .email.ng-dirty.ng-invalid-required{background-color: red;}
  .rzonsocial.ng-dirty.ng-invalid-required{background-color: red;}
  .ruc.ng-dirty.ng-invalid-required{background-color: red;}
  .direccion.ng-dirty.ng-invalid-required{background-color: red;}
  .telefono.ng-dirty.ng-invalid-required{background-color: red;}
  .contacto.ng-dirty.ng-invalid-required{background-color: red;}

  .ruc.ng-dirty.ng-invalid-minlength { background-color: yellow;}

  /*.email.ng-valid {
      background-color: lightgreen;
  }
  .email.ng-dirty.ng-invalid-required {
      background-color: red;
  }*/
  .email.ng-dirty.ng-invalid-email {
      background-color: yellow;
  }

</style>
<div class="row" ng-app="myapp" ng-controller="empresasController as ctrl">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Mantenimiento de Empresas</h3>
      </div>
        <div class="box-body">
          <div class="formcontainer">
            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">

                <div class="form-group">
                  <label for="codigo" >Codigo</label>
                  <input type="text" class="codigo form-control" name="codigo" id="codigo" placeholder="Codigo de empresa" ng-model="ctrl.empre.codigo" required>
                  <div class="has-error" ng-show="myForm.$dirty">
                      <span ng-show="myForm.codigo.$error.required">Este es un campo obligatorio</span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="razonsocial" >Razon Social</label>
                  <input type="text" class="rzonsocial form-control" name="rzonsocial" id="rzonsocial" placeholder="Razon Social" ng-model="ctrl.empre.rzonsocial" required>
                  <div class="has-error" ng-show="myForm.$dirty">
                      <span ng-show="myForm.rzonsocial.$error.required">Este es un campo obligatorio</span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="ruc">Ruc</label>
                  <input type="number" class="ruc form-control" name="ruc" id="ruc" placeholder="Ruc" pattern="[0-9]{11}" ng-model="ctrl.empre.ruc" required ng-minlength="11">
                  <div class="has-error" ng-show="myForm.$dirty">
                      <span ng-show="myForm.ruc.$error.required">Este es un campo obligatorio</span>
                      <span ng-show="myForm.ruc.$error.minlength">La longitud mínima requerida es de 11</span>
                      <span ng-show="myForm.ruc.$invalid">Este campo no es válido. </span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="ruc">Direccion</label>
                  <input type="text" class="direccion form-control" name="direccion" id="direccion" placeholder="Direccion" ng-model="ctrl.empre.direccion" required>
                  <div class="has-error" ng-show="myForm.$dirty">
                      <span ng-show="myForm.direccion.$error.required">Este es un campo obligatorio</span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="telefono">Telefono</label>
                  <input type="text" class="telefono form-control" name="telefono" id="telefono" placeholder="Telefono" ng-model="ctrl.empre.telefono" required>
                  <div class="has-error" ng-show="myForm.$dirty">
                      <span ng-show="myForm.telefono.$error.required">Este es un campo obligatorio</span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="email">Correo electronico</label>
                  <input type="email" class="email form-control" name="email" id="email" placeholder="Correo" ng-model="ctrl.empre.email" required>
                  <div class="has-error" ng-show="myForm.$dirty">
                    <span ng-show="myForm.email.$error.required">Este es un campo obligatorio</span>
                    <span ng-show="myForm.email.$invalid">Este campo no es válido. </span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="contacto">Contacto</label>
                  <input type="text" class="contacto form-control" name="contacto" id="contacto" placeholder="Contacto" ng-model="ctrl.empre.contacto" required>
                  <div class="has-error" ng-show="myForm.$dirty">
                      <span ng-show="myForm.contacto.$error.required">Este es un campo obligatorio</span>
                  </div>
                </div>
                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit"  value="Guardar" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <!--input type="submit"  value="{{!ctrl.user.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button-->
                    </div>
                </div>
            </form>
          </div>
        </div>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
      <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="alert alert-danger" id="msgError" style="display:none">
        <button type="button" class="close" ng-click="CloseEmpresa(1)">&times;</button>
        <strong>¡Cuidado!</strong> {{messageError}}
      </div>
      <div class="alert alert-success" id="msgSuccess" style="display:none">
        <button type="button" class="close" ng-click="CloseEmpresa(2)">&times;</button>
        <strong>¡Éxito!</strong> {{messageSuccess}}
      </div>

      <div class="box-header with-border">
        <h3 class="box-title">Listado de Empresas</h3>
      </div><!-- /.box-header -->
      <br>
      <div class="dataTables_wrapper form-inline" role="grid">
        <div class="row" style="margin-left:0px;margin-right:0px;">
          <div class="col-sm-12" style="margin-top: 15px;">
              <div id="example_filter" class="dataTables_filter" style="width:100%">
                  <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                    <input style="margin-left:0px;" type="search" class="form-control input-sm" aria-controls="example" ng-model="searchText" ng-change="searchTextChanged()" placeholder="Palabra clave">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign" style="color:#3c8dbc"></i></span>
                  </div>
              </div>
          </div>
        </div>
        <br>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Codigo</th>
                <th>Ruc</th>
                <th>Razon Social</th>
                <th>Telefono</th>
                <th>Correo</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody class="buscar">
              <tr ng-repeat = "empresa in empresas |orderBy:columnToOrder:reverse">
                <td>{{empresa.empresa_codigo}}</td>
                <td>{{empresa.empresa_ruc}}</td>
                <td>{{empresa.empresa_razonsocial}}</td>
                <td>{{empresa.empresa_telefono}}</td>
                <td>{{empresa.empresa_correo}}</td>
                <td><a class="btn btn-warning btn-circle" data-toggle="modal" data-target="#myModal" ng-click="Editar(empresa.empresa_id)"><i class="glyphicon glyphicon-edit"></i></a> <a class="btn btn-danger btn-circle" ng-click="EliminarEmpresa(empresa.empresa_id)"><i class="glyphicon glyphicon-remove"></i></a></td>
              </tr>
                <tr class="nullOld" id="nullOld">
                  <td colspan="6">No hay datos a mostrar</td>
                </tr>
            </tbody>
          </table>
          <div class="row">
              <div class="col-sm-5" style="margin-left:15px;">
                  <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                  <!-- Showing 1 to 10 of 848 entries -->
              </div>
              <div class="col-sm-6" style="text-align: right;">
                  <div class="dataTables_paginate paging_simple_numbers">
                    <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                  </div>
              </div>
          </div>
      </div>
    </div t="primary">
  </div>
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar Empresas</h4>
        </div>
        <form role="form" method="post" action="../BL/empresaController.php">
        <input type="hidden" name="hidden_empresa_update">
        <input type="hidden" id="empid" name="empid">
          <div class="modal-body">
            <div class="box-body">
              <div class="form-group" id="codigo">
                <label for="codigo" >Codigo</label>
                <input type="text" class="form-control" name="codigoedit" id="codigoedit" placeholder="Codigo de empresa" required>
              </div>
              <div class="form-group" id="razonsocial">
                <label for="razonsocial" >Razon Social</label>
                <input type="text" class="form-control" name="razonsocialedit" id="razonsocialedit" placeholder="Razon Social" required>
              </div>
              <div class="form-group" id="ruc">
                <label for="ruc">Ruc</label>
                <input type="tel" class="form-control" name="rucedit" id="rucedit" pattern="[0-9]{11}" placeholder="Ruc" required>
              </div>
              <div class="form-group">
                <label for="direccion">Direccion</label>
                <input type="text" class="form-control" name="direccionedit" id="direccionedit" placeholder="Direccion" required>
              </div>
              <div class="form-group">
                <label for="telefono">Telefono</label>
                <input type="text" class="form-control" name="telefonoedit" id="telefonoedit" placeholder="Telefono" required>
              </div>
              <div class="form-group">
                <label for="correo">Correo electronico</label>
                <input type="email" class="form-control" name="correoedit" id="correoedit" placeholder="Correo" required>
              </div>
              <div class="form-group">
                <label for="contacto">Contacto</label>
                <input type="text" class="form-control" name="contactoedit" id="contactoedit" placeholder="Contacto" required>
              </div>
            </div><!-- /.box-body -->
          
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Guardar Cambios</button>
        </div>
      </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</div>
<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {
        $('#filtrar').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('.buscar tr').hide();
            $('.buscar tr').filter(function () {
                return rex.test($(this).text());
            }).show();
        })
    }(jQuery));
 
  });
</script>

