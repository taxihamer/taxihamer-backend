<?php
  include'seguridad.php';
?>
<link rel="stylesheet" href="dist/css/angular-validate.css">
<style type="text/css">
  .warning{color: red;}
  .ok{color: green;}
  .pend{color: blue;}
  .rechazar{padding: 4px 6px;
    /*margin: -53px 0px -26px 10px;*/ margin-left: 10px; position: absolute;}
    table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
}
</style>
<div class="row" ng-app="myapp" ng-controller="liquidacionController">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Liquidaciones</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" name="valForm" novalidate>
        <div class="box-body">
          <div class="form-group">
            <label for="destino">Conductor</label>
            <input class="vaform form-control" name="selected" ng-model="visitorInfo.selected" placeholder="Buscar nombre conductor" id="celularConductor" required>
            <span class="valid" ng-show="valForm.selected.$valid">&#10004;</span>
            <input type="hidden" ng-model="conductor_id">
          </div>
          <div class="form-group">
            <label for="monto">Monto a Liquidar</label>
            <input type="number" class="vaform form-control" name="monto" min="0" ng-model="visitorInfo.monto" placeholder="Monto a Liquidar" required>
            <span class="valid" ng-show="valForm.monto.$valid">&#10004;</span>
          </div>
          <div class="form-group">
            <label for="msg">Observacion</label> <!--input-no-width rounded shaded left clearboth-->
            <textarea ng-model="visitorInfo.msg" name="msg" rows="8" cols="50" class="vaform form-control" id="msg" required></textarea>
            <!--input type="number" class="vaform form-control" name="noperacion" ng-model="visitorInfo.noperacion" placeholder="Nro de Operación" required-->
            <span class="valid" ng-show="valForm.msg.$valid">&#10004;</span>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="submitbtn btn btn-primary" ng-disabled="valForm.$invalid" ng-click="saveLiquidacion(visitorInfo)">Guardar</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
    <div class="box box-primary">
      <div class="alert alert-danger" id="msgErrorLiquidacion" style="display:none">
        <button type="button" class="close" ng-click="CloseLiquidacion(1)">&times;</button>
        <strong>Error!</strong> {{messageLiquidacion}}
      </div>
      <div class="alert alert-success" id="msgSuccessLiquidacion" style="display:none">
        <button type="button" class="close" ng-click="CloseLiquidacion(2)">&times;</button>
        <strong>¡Éxito!</strong> {{messageLiquidacion}}
      </div>
      <div class="box-header with-border">
          <!--div class="form-group">
           <label for="fecha">Exportar :</label>
          <form role="form" action="../BL/exportarController.php" enctype="multipart/form-data" method="post" style="width: 30%;
                    display: inline-block;">
                    <input type="hidden" name="hidden_exportar" value="1">
                    <input type="hidden" name="exportar" value="7">
                    <button type="submit" class="btn btn-primary btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
          </form>
          </div-->
           <h3 class="box-title">Listado de Liquidaciones</h3>
      </div-->
         <loading></loading>
        <div style="overflow-x:auto;">
        <table class="table table-striped" datatable="ng" dt-options="dtOptions">
          <thead>
            <tr>
              <th>Conductor</th>
              <th>DNI</th>
              <th>Teléfono</th>
              <th>Monto</th>
              <th>Observacion</th>
              <th>Fecha</th>
              <th>Hora</th>
            </tr>
          </thead>
          <tbody>
             <tr ng-repeat="liquidacion in liquidaciones">
              <td>{{liquidacion.conductor_nombre}} {{liquidacion.conductor_apellido}}</td>
              <td>{{liquidacion.conductor_dni}}</td>
              <td>{{liquidacion.conductor_celular}}</td>
              <td>{{liquidacion.liquidacion_monto | currency:"S/. ":2}}</td>
              <td>{{liquidacion.liquidacion_observacion}}</td>
              <td>{{liquidacion.liquidacion_fecha}}</td>
              <td>{{liquidacion.liquidacion_hora}}</td>
            </tr>
          </tbody>
        </table>
      </div>
      </div>
    </div>
  </div>
