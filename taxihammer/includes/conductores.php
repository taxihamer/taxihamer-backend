<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include'seguridad.php';
require_once'DAL/conductorDAO.php';
require_once'DAL/tipoServicioDAO.php';
require_once'DAL/vehiculoDAO.php';
require_once'DAL/constantes.php';

$tipoServicioDAO = new tipoServicioDAO();
$result_tipo = $tipoServicioDAO->lista();
$conductorDAO = new conductorDAO();
$result = $conductorDAO->lista();
$vehiculoDAO = new vehiculoDAO();
//$result_vehiculo = $vehiculoDAO->listaDropdown();

?>
<style type="text/css">
  .loading {border: 1px solid #ddd;padding: 20px;margin: 40px 5px;}
  .modal-content {padding: 0px 25px 0px 25px;}
  #mtncd , #mtncdedit{display: none;}
  table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
  }
  .sortNew{color: #3c8dbc;}
  .sortNew:hover, .sortNew:active, .sortNew:focus{color: #3c8dbc;}
  .nullOld{display: none;text-align: center;color: #e23558;}
  .panel-default{margin-bottom: 0px;}

  .warning{color: red;}
  .ok{color: green;}
  .pend{color: blue;}

</style>
<script type="text/javascript">
  window.URL = window.URL || window.webkitURL;
  $(document).ready(function () {
    $('.ang').hide();

    $("#image-file").change(function(e) {

        var fileInput = $("input[type=file]")[0],
        file = fileInput.files && fileInput.files[0];

          if( file ) {
              var img = new Image();
              img.src = window.URL.createObjectURL( file );

              img.onload = function() {
                  var width = img.naturalWidth,
                      height = img.naturalHeight;

                  window.URL.revokeObjectURL( img.src );

                  if( width <= 180 && height <= 180 ) {
                     $("#msfile").css({"color":"#737373"});
                  }
                  else {
                      var control = $('#image-file');
                      control.replaceWith( control = control.val('').clone( true ) );
                      $("#msfile").css({"color":"#dd4b39"});
                  }
              };
          }
    });

  });
</script>
<div class="row" ng-app="myapp" ng-controller="conductorController">
  <input type="hidden" id="rol_tran" value="<?=$_SESSION['trans']?>">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Registrar Conductor</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" action="BL/conductorController.php" method="post" enctype="multipart/form-data">
        <div class="box-body">
         <?php if((int)$_SESSION['rol_id'] == 1){ ?>
            <div class="checkbox">
              <label><input type="checkbox" name="check_test">Conductor de Prueba</label>
            </div>
          <?php } ?>
          <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" name="nombre" placeholder="Nombre" required>
            <input type="hidden" class="form-control" name="hidden_insert_conductor" value="1">
          </div>
          <div class="form-group">
            <label for="apellido">Apellido</label>
            <input type="text" class="form-control" name="apellido" placeholder="Apellido" required>
          </div>
          <div class="form-group">
            <label for="email">Correo Electrónico</label>
            <input type="email" class="form-control" name="correo" placeholder="Email" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off" required>
          </div>
          <div class="form-group">
            <label for="docidentidad">Doc. Identidad</label>
            <input type="text" class="form-control" name="dni" placeholder="Documento de Identidad" maxlength="8">
          </div>
          <div class="form-group">
            <label for="celular">Nro. Celular</label>
            <input type="text" class="form-control" name="celular" placeholder="Nro de Celular">
          </div>
          <div class="form-group">
            <label for="ciudad">Ciudad</label>
            <input type="text" class="form-control" name="ciudad" placeholder="Ciudad">
          </div>
          <div class="form-group">
            <label for="distrito">Distrito</label>
            <input type="text" class="form-control" name="distrito" placeholder="Distrito">
          </div>
          <div class="form-group">
            <label for="nrolicencia">Nro. Licencia</label>
            <input type="text" class="form-control" name="licencia" placeholder="Nro de licencia">
          </div>
          <div class="form-group">
            <label for="catlicencia">Categoria de Licencia</label>
            <input type="text" class="form-control" name="catlicencia" placeholder="Categoria de la licencia">
          </div>
          <div class="form-group">
            <label for="catlicencia">Fecha Vencimiento Brevete</label>
            <input type="date" class="form-control" name="fecha" id="brevete">
          </div>
          <div class="form-group">
            <label for="exampleInputFile">Foto del Conductor</label>
            <input type="file" name="foto" class="form-control" id="image-file" required>
            <p class="help-block" id="msfile">Tamaño 180x180 pixeles.</p>
          </div>
          <div class="form-group">
            <label for="catlicencia">Asignar Vehículo</label>
            <select class="form-control" name="unidadid">
               <?php $result_vehiculo1 = $vehiculoDAO->listaAsignada();
                if(!empty($result_vehiculo1)){ ?>
                     <option value ="0">Seleccione Unidad</option>
                <?php  foreach($result_vehiculo1 AS $vehiculo){ ?>
                    <option value ="<?php echo $vehiculo['unidad_id']; ?>"><?php echo $vehiculo['unidad_alias']; ?></option>
                  <?php } 
                }else{ ?>
                    <option value ="-1">Unidades no disponibles</option>
                <?php } ?>
            </select>
          </div>
                                <!--- Nuevos Campo -->
                      <div class="form-group">
                        <label for="soatAdd">Fecha SOAT</label>
                        <input type="date" class="form-control" name="soatAdd" id="soatAdd" required>
                      </div>
                      <div class="form-group">
                        <label for="foto6">Foto del Brevete</label>
                        <input type="file" name="foto6" id="foto6" class="form-control" required>
                      </div>
                      <!--div class="form-group">
                        <label for="carnetvialAdd">Fecha Carnet Vial</label>
                        <input type="date" class="form-control" name="carnetvialAdd" id="carnetvialAdd">
                      </div-->
                      <div class="form-group">
                        <label for="foto7">Foto de Soat</label>
                        <input type="file" name="foto_7" id="foto_7" class="form-control" required>
                      </div>
                      <div class="form-group">
                        <label for="polarizadasAdd">Codigo de lunas Polarizadas</label>
                        <input type="text" class="form-control" name="polarizadasAdd" id="polarizadasAdd">
                      </div>
                      <div class="form-group">
                        <label for="banco">Entidad Financiera</label>
                        <select ng-model="select_defaultEdit" class="form-control" name="select_defaultAdd" ng-options="c.key as c.value for c in bancosEdit" required></select>
                      </div>
                      <div class="form-group">
                        <label for="cii_bancoAdd">Codigo de CCI</label>
                        <input type="number" class="form-control" name="cii_bancoAdd" id="cii_bancoAdd">
                      </div>
                      <!--- End /// -->
          <?php //if($_SESSION['trans'] != 2){ ?>
          <!--div class="form-group">
            <label for="catlicencia">Asignar Comision</label>
            <select class="form-control" name="comisionid" id="comisionid" onchange="show(document.getElementById('comisionid').value,'new')">
                <option value ="0">Seleccione una Comision</option>
                <option value ="1">% Porcentaje</option>
                <option value ="2">S/ Monto</option>
            </select>
          </div>
          <div class="form-group" id="mtncd">
            <label for="montoconductor">Ingresar el Monto/Comision</label>
            <input type="number" step="any" class="form-control" name="montoconductor" id="montoconductor" placeholder="Ingresar el Monto/Comision">
          </div-->
          <?php //} ?>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
  <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert" id="successInicio">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert" id="errorInicio">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="alert alert-danger" id="msgError" style="display:none">
        <button type="button" class="close" ng-click="CloseConductor()">&times;</button>
        <strong>¡Cuidado!</strong> {{messageError}}
      </div>
      <div class="alert alert-success" id="msgSuccess" style="display:none">
        <button type="button" class="close" ng-click="CloseConductorSucces()">&times;</button>
        <strong>¡Éxito!</strong> {{messageSuccess}}
      </div>
      <!--div class="box-header with-border">
        <h3 class="box-title">Listado de Conductores</h3>
      </div>
      <br>
      <!--div class="row">
        <div class="col-md-12" style="margin-left:20px;">
          <label>Exportar : </label>
          <form role="form" action="../BL/exportarController.php" enctype="multipart/form-data" method="post" style="width: 30%;
            display: inline-block;">
            <input type="hidden" name="hidden_exportar" value="1">
            <input type="hidden" name="exportar" value="2">
            <button type="submit" class="btn btn-primary btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
          </form>
        </div>
      </div>
      <br>
        <!--loading></loading>
        <div style="overflow-x:auto;">
        <table class="table table-striped" datatable="ng" dt-options="dtOptions">
          <thead>
            <tr>
              <th>Foto</th>
              <th>Conductor</th>
              <th>Correo</th>
              <th>Nro. Telf.</th>
              <th>Taxi Asignado</th>
              <th>Tipo Servicio</th>
              <th>Calificacion</th>
              <th>Estado</th>
              <th>Accion</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="conductor in conductors">
              <td><img ng-src="BL/{{conductor.conductor_foto}}" alt="taxi" width="50px" class="img-circle" /></td>
              <td>{{conductor.conductor_nombre}} {{conductor.conductor_apellido}}</td>
              <td>{{conductor.conductor_correo}}</td>
              <td>{{conductor.conductor_celular}}</td>
              <td>{{conductor.unidad_alias}}</td>
              <td>{{conductor.tiposervicio_nombre}}</td>
              <td>{{conductor.conductor_calificacion | number:1}}</td>
              <td>
                <div ng-if="conductor.conductor_status==1">
                     <switch name="enabled" ng-model="conductor.enabled" ng-change="changeCallback(conductor.conductor_id, $index + 1)" on="on" off="off"></switch>
                </div> 
                <div ng-if="conductor.conductor_status==0">
                       <switch name="enabled" ng-model="conductor.enabled" ng-change="changeCallback(conductor.conductor_id, $index + 1)" on="on" off="off"></switch>
                </div>  
              </td>
              <td style="display:inline-block;width: 83px;">
                <div ng-if="conductor.conductor_status==0" class="pull-left">
                  <button class="btn btn-warning" data-toggle="modal" data-target="#myModal" ng-click="llenar(conductor.conductor_id,conductor.conductor_nombre,conductor.conductor_apellido,conductor.unidad_id,conductor.conductor_correo, conductor.conductor_password,conductor.conductor_dni, conductor.conductor_celular, conductor.conductor_ciudad, conductor.conductor_distrito, conductor.conductor_licencia, conductor.conductor_catlicencia, conductor.conductor_fechabrevete, conductor.conductor_foto,conductor.unidad_alias, $index +1, conductor.conductor_comision, conductor.conductor_comision_total, conductor.conductor_fechasoat, conductor.conductor_fotosoat,conductor.conductor_fotocarnetvial,conductor.conductor_fechacarnetvial,conductor.conductor_polarizadas,conductor.conductor_cci,conductor.banco_id)"><i class="glyphicon glyphicon-edit"></i></button>
                  
                </div>
                <div ng-if="conductor.conductor_status==1" class="pull-left">
                    <button class="btn btn-default"><i class="glyphicon glyphicon-edit"></i></button>
                </div>
                 <a class="btn btn-danger pull-right" ng-click="EliminarConductor(conductor.conductor_id)"><i class="glyphicon glyphicon-remove"></i></a>
              </td>
            </tr>
          </tbody>
        </table>
        </div-->
        <section id="main-content" class="animated fadeInRight">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">Listado de Conductores</h3>
                      </div>
                      <div class="panel-body">
                      <!-- TOP OF TABLE: shows page size and search box -->
                        <div class="dataTables_wrapper form-inline" role="grid">
                          <div class="row">
                              <div class="col-sm-6">
                                  <div class="dataTables_length" id="example_length">
                                      <!--label>
                                      <select name="example_length" aria-controls="example" class="form-control input-sm" ng-model="pageSize" ng-change="pageSizeChanged()">
                                          <option value="10">10</option>
                                          <option value="25">25</option>
                                          <option value="50">50</option>
                                          <option value="100">100</option>
                                      </select> records per page</label-->
                                      <label>Exportar : </label>
                                      <form role="form" action="../BL/exportarController.php" enctype="multipart/form-data" method="post" style="width: 30%;
                                        display: inline-block;">
                                        <input type="hidden" name="hidden_exportar" value="1">
                                        <input type="hidden" name="exportar" value="2">
                                        <button type="submit" class="btn btn-primary btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                                      </form>
                                  </div>
                              </div>
                              <div class="col-sm-12">
                                  <div id="example_filter" class="dataTables_filter">
                                      <label>Buscar :<input type="search" class="form-control input-sm" aria-controls="example" ng-model="searchText" ng-change="searchTextChanged()" placeholder="Nombre de conductor"></label>
                                  </div>
                              </div>
                          </div>                        
                          <!--loading></loading-->
                          <div style="overflow-x:auto;">
                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                              <thead>
                                  <tr>
                                      <th>Foto</th>
                                      <th>Conductor</th>
                                      <th>Correo</th>
                                      <th>Nro. Telf.</th>
                                      <th>Taxi Asignado</th>
                                      <th><a ng-click="sort('tiposervicio_nombre')" href="#" class="sortNew"> Tipo Servicio <span class="{{Header[0]}}"></span></a></th>
                                      <th><a ng-click="sort('conductor_calificacion')" href="#" class="sortNew"> Calificacion <span class="{{Header[1]}}"></span></a></th>
                                      <!--th><a ng-click="sort('conductor_status')" href="#" class="sortNew"> Estado <span class="{{Header[2]}}"></span></a></th-->
                                      <th>Estado</th>
                                      <th>Accion</th>
                                  </tr>
                              </thead>

                              <tbody>
                                  <tr ng-repeat = "conductor in conductors | orderBy:columnToOrder:reverse ">
                                    <td><img ng-src="BL/{{conductor.conductor_foto}}" alt="taxi" width="50px" class="img-circle" /></td>
                                    <td>{{conductor.conductor_nombre}} {{conductor.conductor_apellido}}</td>
                                    <td>{{conductor.conductor_correo}}</td>
                                    <td>{{conductor.conductor_celular}}</td>
                                    <td ng-class="{warning: (conductor.unidad_id == 1)}">{{conductor.unidad_alias}}</td>
                                    <td>{{conductor.tiposervicio_nombre}}</td>
                                    <td>{{conductor.conductor_calificacion | number:1}}</td>
                                    <td>
                                      <div ng-if="conductor.conductor_status==1">
                                           <switch name="enabled" ng-model="conductor.enabled" ng-change="changeCallback(conductor.conductor_id, $index + 1,conductor.enabled)" on="on" off="off"></switch>
                                      </div> 
                                      <div ng-if="conductor.conductor_status==0">
                                             <switch name="enabled" ng-model="conductor.enabled" ng-change="changeCallback(conductor.conductor_id, $index + 1,conductor.enabled)" on="on" off="off"></switch>
                                      </div>  
                                    </td>
                                    <td style="width:10%;">
                                      <div ng-if="conductor.conductor_status==0" class="pull-left">
                                        <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#myModal" ng-click="llenar(conductor.conductor_id,$index +1)"><i class="glyphicon glyphicon-edit"></i></button>

                                      </div>
                                      <div ng-if="conductor.conductor_status==1" class="pull-left">
                                        <button class="btn btn-default btn-circle"><i class="glyphicon glyphicon-edit"></i></button>
                                      </div>
                                      <a class="btn btn-danger pull-right btn-circle" ng-click="EliminarConductor(conductor.conductor_id)"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                  </tr>
                                  <tr class="nullOld" id="nullOld">
                                    <td colspan="9">No hay datos a mostrar</td>
                                  </tr>
                              </tbody>
                            </table>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                                    <!-- Showing 1 to 10 of 848 entries -->
                                </div>
                                <div class="col-sm-6">
                                    <div class="dataTables_paginate paging_simple_numbers">
                                      <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                                    </div>
                                </div>
                            </div>
                          </div>                      
                        </div>
                      </div>
                    </div>
                </div>
            </div>
         </section>
      </div>
    </div>
    <!-- Modal in Angular -->
          <modal title="Editar Conductores" visible="showModal" style="display:none;" id="modal">
            <form id="formuploadajax" class="form-horizontal" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" ng-model="idconductor" id="idconductor">
                    <input type="hidden" ng-model="indexEdit">
                      <div class="form-group">
                        <label for="correo">Nombre</label>
                        <input type="text" class="form-control" name="nombre1" id="nombre1" ng-model="nameConductor" placeholder="Nombre del Conductor" ng-required="true" required>
                      </div>
                      <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" name="apellido1" id="apellido1" ng-model="apellidoConductor" placeholder="Apellido del Conductor" ng-required="true" required>
                      </div>
                      <div class="form-group">
                        <label for="tipo">Vehículo Asignado</label>
                        <div ng-if="vehiculos.length>0">
                        <select id="conductorsDis" ng-options="item as item.unidad_alias for item in vehiculos track by item.unidad_id" ng-model="selected" class="form-control">
                          <option value="">-- Seleccione Unidad --</option>
                        </select>
                        </div>
                         <div ng-if="vehiculos.length==0">
                           <select ng-model="selected" class="form-control">
                               <option value ="">Unidades no disponibles</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="correo">Correo Electrónico</label>
                        <input type="email" class="form-control" id="correo" name="correo1" ng-model="emailConductor.text" placeholder="Ej. alex@dominio.com" ng-required="true">
                      </div>
                      <div class="form-group">
                        <label for="correo">Contraseña</label>
                        <input type="password" class="form-control" id="pass1" name="pass1" mg-model="passConductor.text" placeholder="Contraseña" ng-required="true">
                      </div>
                      <div class="form-group">
                        <label for="correo">Documento de Identidad</label>
                        <input type="text" class="form-control" name="dni1" id="dni1" placeholder="Documento de Identidad" ng-maxlengt="8" ng-model="dniConductor" ng-required="true">
                      </div>
                      <div class="form-group">
                        <label for="correo">Nro Celular</label>
                        <input type="text" class="form-control" name="celular1" id="celular1" placeholder="Nro de Celular" ng-model="celConductor" ng-required="true">
                      </div>
                      <div class="form-group">
                        <label for="correo">Ciudad</label>
                        <input type="text" class="form-control" name="ciudad1" id="ciudad1" placeholder="Ej. Lima" ng-model="cityConductor" ng-required="true">
                      </div>
                      <div class="form-group">
                        <label for="soat">Distrito</label>
                        <input type="text" class="form-control" name="distrito1" id="distrito1" ng-model="distConductor" ng-required="true">
                      </div>
                      <div class="form-group">
                        <label for="modelo">Nro. Licencia</label>
                        <input type="text" class="form-control" name="licencia1" id="licencia1" ng-model="liceConductor" ng-required="true">
                      </div>
                      <div class="form-group">
                        <label for="modelo">Categoría de Licencia</label>
                        <input type="text" class="form-control" name="catlicencia1" id="catlicencia1" ng-model="catConductor" ng-required="true">
                      </div>
                      <div class="form-group">
                        <label for="modelo">Fecha Vencimiento Brevete</label>
                        <input type="date" class="form-control" name="fechabrevete1" id="fechabrevete1" ng-model="example.value" ng-required="true">
                      </div>
                      <div class="form-group">
                        <small>Foto Actual</small>
                        <div id="foto2">
                          <img ng-src="BL/{{fotoedit}}" alt="taxi" class="img-responsive" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="foto1">Foto del Conductor</label>
                        <input type="file" class="form-control" name="foto1" id="foto1">
                        <small id="smgpopup">Imagen en JPG máximo tamaño 180 x 180px</small>
                      </div>
                      <!--- Nuevos Campo 
                      <div class="form-group">
                        <label for="soat">Fecha SOAT</label>
                        <input type="date" class="form-control" name="soat" id="soat" ng-model="examplesoat.value" ng-required="true">
                      </div>-->
                      <div class="form-group">
                        <small>Foto Brevete</small>
                        <div id="fotosoat">
                          <img ng-src="BL/{{fotoeditlicencia}}" alt="taxi" class="img-responsive" />
                          <!--img src="" alt="taxi" id="fotoviewsoat" class="img-responsive" /-->
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="foto3">Foto del Brevete</label>
                        <input type="file" name="foto3" id="foto3" class="form-control">
                      </div>
		     <div class="form-group">
                        <label for="soat">Fecha SOAT</label>
                        <input type="date" class="form-control" name="soat" id="soat" ng-model="examplesoat.value" ng-required="true">
                      </div>
                      <!--div class="form-group">
                        <label for="carnetvial">Fecha Carnet Vial</label>
                        <input type="date" class="form-control" name="carnetvial" id="carnetvial" ng-model="examplecarnet.value">
                      </div-->
                      <div class="form-group">
                        <small>Foto Soat</small>
                        <div id="fotocarnet">
                          <img ng-src="BL/{{fotoeditsoat}}" alt="taxi" class="img-responsive" />
                          <!--img src="" alt="taxi" id="fotoviewcarnet" class="img-responsive" /-->
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="foto4">Foto de SOAT</label>
                        <input type="file" name="foto_4" id="foto_4" class="form-control">
                      </div>
                      <div class="form-group">
                        <label for="polarizadas">Codigo de lunas Polarizadas</label>
                        <input type="text" class="form-control" name="polarizadas" id="polarizadas" ng-model="polarizadas">
                      </div>
                      <div class="form-group">
                        <label for="banco">Entidad Financiera</label>
                        <select ng-model="select_defaultEdit" class="form-control" id="select_defaultEdit" ng-options="c.key as c.value for c in bancosEdit" required></select>
                      </div>
                      <div class="form-group">
                        <label for="cii_banco">Codigo de CCI</label>
                        <input type="number" class="form-control" name="cii_banco" id="cii_banco" ng-model="cci">
                      </div>
                      <!--- End /// -->
                      <?php if($_SESSION['trans'] != 2){ ?>
                      <div class="form-group">
                        <label for="comisionidedit">Asignar Comision</label>
                         <select ng-model="Comisionselected" class="form-control" name="comisionidedit" id="comisionidedit" ng-options="c.key as c.value for c in comisiones" onchange="show(document.getElementById('comisionidedit').value,'edit')" ></select>
                      </div>
                      <div class="form-group" id="mtncdedit">
                        <label for="montoconductor">Ingresar el Monto/Comision</label>
                        <input type="number" step="any" class="form-control" name="montoconductoredit" id="montoconductoredit" ng-model="comisionTotal" placeholder="Ingresar el Monto/Comision">
                      </div>
                      <?php } ?>
                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10 ">
                          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-left: -88px;">Cerrar</button>
                          <button type="submit" class="btn btn-primary pull-right" ng-click="updateSth()">Guardar Cambios</button>
                        </div>
                      </div>
            </form>
          </modal>
  </div>
<script type="text/javascript">
  
function show(idtipo,flag){

  if(flag == 'new'){
      if(idtipo == 1 || idtipo == 2){
        $("#mtncd").show();
        $("#montoconductor").focus();
      }else{
        $("#mtncd").hide();
        $("#montoconductor").val('');
      }
  }else{
    var n = idtipo.split("string:");
      if(n[1] == 1 || n[1] == 2){
        $("#mtncdedit").show();
        $("#montoconductoredit").focus();
      }else{
        $("#mtncdedit").hide();
        $("#montoconductoredit").val('');
      }
  }

}

</script>
<style type="text/css">
  #dialog-delete .ui-dialog-titlebar-close{
    display: none;
  }

  #textconfirma {
      font-size: 20px;
      text-align: center;
      margin-top: 40px;
      padding-left: 10px;
      padding-right: 10px;
      font-family: Tahoma;
  }
</style>
<div id="dialog-delete" style="display:none;">
  <p><span style="float:left; margin:12px 12px 20px 0;">
  </span><label id="textconfirma">Estas seguro de eliminar al Conductor</label></p>
</div>
