<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include'seguridad.php';
require_once'DAL/menuDAO.php';
require_once'DAL/rolDAO.php';

$menuDAO = new menuDAO();
$result = $menuDAO->listmenu();

$listado = $menuDAO->listaoperadores();

$rolDAO = new rolDAO();
$rol = $rolDAO->listrol();
?>
<div class="row">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Agregar Permisos</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" method="post" action="../BL/menuController.php">
        <input type="hidden" name="hidden_menu_insert">
        <div class="box-body">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
              <div class="form-group">
                <label for="destino">Operadores</label>
                <select class="form-control" name="operadores" id="ct">
                  <option value="0">Seleccione</option>
                  <?php foreach($rol as $i => $roles ){ ?>
                      <option value="<?=$roles['rol_id']?>"><?=$roles['rol_nombre']?></option>
                  <?php } ?>
                </select>
              </div>
              <!-- Optionally, you can add icons to the links -->
            <?php   
              foreach ($result as $i => $menu) { ?>
                <li>
                  <div class="checkbox">
                      <label><input name="parent[]" type="checkbox" id="par-<?=$menu['menu_id']?>" class="check-all-parent" value="<?=$menu['menu_id']?>"><?=$menu['menu_nombre']?></label>
                  <?php $resultsub = $menuDAO->listsubmenuId($menu['menu_id']);
                  if(!empty($resultsub)){?>
                  <ul>
                    <?php foreach ($resultsub as $key => $submenu) { ?>
                      <div class="checkbox">
                          <label><input name="chil[]" id="<?=$menu['menu_id']?>" type="checkbox" class="children check-all-child_<?=$menu['menu_id']?>" value="<?=$submenu['submenu_id']?>"><?=$submenu['submenu_nombre']?></label>
                      </div>
                    <?php }?>
                  </ul>
                  <?php } ?>
                  </div>
                </li>
            <?php } ?>
              
            </ul><!-- /.sidebar-menu -->
        </div>

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar Usuario</button>
        </div>

      </form>
    </div>
  </div>
  <div class="col-md-9">
    <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }else if($_REQUEST['status'] == "valid"){
         echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, No puede crear mas Permisos.</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado de Operadores</h3>
      </div><!-- /.box-header -->
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Nº</th>
              <th>Operador</th>
              <th>Accion</th>
            </tr>
          </thead>
          <tbody>
          <?php
          if(!empty($listado)){
            foreach($listado as $res){
          ?>
            <tr>
              <td><?=$res['menurol_id']?></td>
              <td><?=$res['rol_nombre']?></td>
              <td><a class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="llenar('<?=$res['menurol_id'];?>');"><i class="glyphicon glyphicon-edit"></i></a></td>
            </tr>
          <?php
            }
          }else{ ?>
            <tr>
              <td>No hay data a mostrar..</td>
            </tr>
          <?php }
          ?>
          </tbody>
        </table>
      </div>
  </div>
</div>
<script>
  $(document).ready(function(){

      if(<?=$_SESSION['rol_id'];?> == 1){
        $("#ct option[value='<?=$_SESSION['rol_id'];?>']").remove();
      }else{
        $("#ct option[value='1']").remove();
        $("#ct option[value='<?=$_SESSION['rol_id'];?>']").remove();
      }
      //document.getElementById("mySelect").selectedIndex = <?=$_SESSION['rol_id'];?>;
      //$('#mySelect').prop('disabled', 'disabled');

      $('.check-all-parent').click(function(event) {
        var idmenu = this.id;
        var partes = idmenu.split("-");
        if(this.checked) { // check select status
            $('.check-all-child_'+partes[1]).each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.check-all-child_'+partes[1]).each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        } 
      });

      $('.children').click(function(event) {
          
          var inputs, x, selecionados=0;
          var idmenu = this.id;
          inputs = document.getElementsByTagName('input');
          for(x=0;x<inputs.length;x++){
            if(inputs[x].type=='checkbox'){
              if(inputs[x].checked==true && inputs[x].id == idmenu){
                selecionados++;
              }
            }
          }

          if(selecionados == 0){
            $("#par-"+idmenu).prop('checked', false);
          }
      });

  });


</script>
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Editar Permisos</h4>
      </div>
      <form role="form" method="post" action="../BL/menuController.php" name="f1">
      <input type="hidden" name="hidden_menu_update">
      <input type="hidden" name="operadorup" id="operadorup">
        <div class="modal-body">
          <div class="box-body">
            <ul class="sidebar-menu">
              <div class="form-group">
                <select class="form-control" name="operadores" id="mySelect">
                  <option value="0">Seleccione</option>
                  <?php foreach($rol as $i => $roles ){ ?>
                      <option value="<?=$roles['rol_id']?>"><?=$roles['rol_nombre']?></option>
                  <?php } ?>
                </select>
              </div>
              <?php   
              foreach ($result as $i => $menu) { ?>
                <li>
                  <div class="checkbox">
                      <label><input name="parent[]" type="checkbox" id="par-<?=$menu['menu_id']?>" class="chekparet<?=$menu['menu_id']?> check-all-parent" value="<?=$menu['menu_id']?>"><?=$menu['menu_nombre']?></label>
                  <?php $resultsub = $menuDAO->listsubmenuId($menu['menu_id']);
                  if(!empty($resultsub)){?>
                  <ul>
                    <?php foreach ($resultsub as $key => $submenu) { ?>
                      <div class="checkbox">
                          <label><input name="chil[]" id="<?=$menu['menu_id']?>" type="checkbox" class="children chekchil<?=$submenu['submenu_id']?> check-all-child_<?=$menu['menu_id']?>" value="<?=$submenu['submenu_id']?>"><?=$submenu['submenu_nombre']?></label>
                      </div>
                    <?php }?>
                  </ul>
                  <?php } ?>
                  </div>
                </li>
            <?php } ?>
            </ul>
          </div><!-- /.box-body -->
        
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
      </div>
    </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
      function llenar(menuid){

        for (i=0;i<document.f1.elements.length;i++) 
          if(document.f1.elements[i].type == "checkbox")  
              document.f1.elements[i].checked=0
            
        $.ajax({
            url : "../BL/menuController.php",
            type: "POST",
            data : { id: menuid,hidden_menu_select: 'hidden_menu_select'},
            success: function(data)
            {

                $('#operadorup').val(data[0].rol_id);
                $('#mySelect option:eq('+data[0].rol_id+')').attr('selected', 'selected');
                $('#mySelect').prop('disabled', 'disabled');

                var menuparent = JSON.parse(data[0].menurol_jsonparent);
                var menuchildr = JSON.parse(data[0].menurol_jsonchildren);
 
              $.each(menuparent,function(index,obj)
              {
                  $(".chekparet"+obj).prop('checked', true);
              });

              $.each(menuchildr,function(i,o)
              {
                  $(".chekchil"+o).prop('checked', true);
              });
            }
        });
      }
</script>