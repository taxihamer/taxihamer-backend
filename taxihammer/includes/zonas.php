<?php
  include'seguridad.php';

  require_once'DAL/lugaresDAO.php';
  require_once'DAL/usuarioDAO.php';
$data['rol_id'] = $_SESSION['rol_id'];
$data['mail'] = $_SESSION['mail'];
  $usuarioDAO = new usuarioDAO();
  $result = $usuarioDAO->listacoordenadas($data);

  $lugaresDAO = new lugaresDAO();
  $listazona = $lugaresDAO->lista();
?>
<style>

  #gmap_canvas_edit{
    height:600px;
    width:100%;
  }
  #gmap_canvas_edit img{
    max-width:none!important;
    background:none!important;
  }
  #coordspoligonoedit , #centerpoligonoedit{
    display: none;
  }

  .inpdisplay{display: none;}
  .glyphicon-pencil{color: blue;margin-left:15px;cursor: pointer;}
  .iconjava{
    margin-left: 10px;
    cursor: pointer;
  }
</style>
<div class="row">
  <div class="col-md-12">
   <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado Tipos de Zonas</h3>
      </div><!-- /.box-header -->
      <br>
      <div class="row">
        <div class="col-md-12">
          <form role="form">
            <div class="col-md-10">
              <input type="text" class="form-control" id="filtrar" name="nombres" placeholder="Buscar por Zona">
            </div>
            <div class="col-md-2">
            </div>
          </form>
        </div>
      </div>
      <br>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Descripcion</th>
              <th>Ultima Actualización</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody class="buscar">
          <?php if(!empty($listazona)) { 
            $i = 0;
            foreach ($listazona as $key => $rowlist) { ?>
            <tr>
              <td><?=$i+1;?></td>
              <td id="td_<?=$rowlist['lugares_id']?>">
                <?=$rowlist['lugar_descripcion']?>
                <span class="glyphicon glyphicon-pencil edit" id="<?=$rowlist['lugares_id']?>" onclick="editZona('<?=$rowlist['lugares_id']?>')" aria-hidden="true"></span>
                <input type="hidden" name="desc" id="hidden_<?=$rowlist['lugares_id']?>" value="<?=$rowlist['lugar_descripcion']?>">
              </td>
              <td><?=$rowlist['lugar_fecharegistro']?></td>
              <td><button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="llenar('<?=$rowlist['lugares_id']?>')"><i class="glyphicon glyphicon-edit"></i></button> <a class="btn btn-danger" href="BL/lugaresController.php?hidden_delete_lugar=1&id=<?=$rowlist['lugares_id']; ?>"><i class="glyphicon glyphicon-remove"></i></a></td>
            </tr>

          <?php $i++; }
              }else{ ?>
            <tr><td colspan="4">No hay data a mostrar</td></tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {
        $('#filtrar').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('.buscar tr').hide();
            $('.buscar tr').filter(function () {
                return rex.test($(this).text());
            }).show();
        })
    }(jQuery));
 
  });
</script>
<div class="modal" id="myModal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="removeLine();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Editar Zona</h4>
      </div>
          <input type="hidden" name="lugares_id" id="lugares_id">
      <div class="modal-body">
        <div class="box-body">
        <input type="hidden" name="lugar_id" id="lugar_id">
          <div style='overflow:hidden;height:600px;width:100%;'>
            <div id='gmap_canvas_edit'></div>
          </div>
          <div id="coordspoligonoedit"></div>
          <div id="centerpoligonoedit"></div>
        </div><!-- /.box-body -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="removeLine();">Cerrar</button>
        <button class="btn btn-success" onclick="ActualizarCambios();">
        <i class="glyphicon glyphicon-floppy-saved"></i> &nbsp;
        Actualizar cambios
      </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type='text/javascript'>

var map;
var poligono;
var coords;
var poligonoedit
var coordsedit;
var marker;
var markers = new Array();
var polyPoints = new Array();
var polyShape;

$(function() {
  init_mapa();
});

function editZona(id_zona){

  if($('.buscar').children('tr').hasClass('active')){
      return false;
  }else{
    var descrip = $('#hidden_'+id_zona).val();
    $('#'+id_zona).parent().parent().attr('class','active');
    $('#td_'+id_zona).html('<input type="text" class="form-control" name="editdescripcion" id="lu_'+id_zona+'" style="width:50%;display: inline-block;"/><span class="iconjava glyphicon glyphicon-ok" aria-hidden="true" style="color:green;" onclick="nanId ('+id_zona+')"></span><span class="iconjava glyphicon glyphicon-remove" aria-hidden="true" style="color:red;" id="iconjava'+id_zona+'" onclick="nan('+id_zona+')"></span>');
    $('#lu_'+id_zona).val(descrip);
    $('#lu_'+id_zona).focus();
  }

}

function nan(id_zona){

   var descrip = $('#lu_'+id_zona).val();
   $('#td_'+id_zona).html(descrip+" <span id='"+id_zona+"' class='glyphicon glyphicon-pencil' aria-hidden='true' onclick='editZona("+id_zona+")'></span><input type='hidden' name='desc' id='hidden_"+id_zona+"' value='"+descrip+"'>");
   $('.buscar').children('tr').removeClass('active');
}

function nanId(id_zona){

  var descripnew = $('#lu_'+id_zona).val();
  var espacio_blanco    = /[a-z]/i;  

  if(!espacio_blanco.test(descripnew))
  {
    $('#lu_'+id_zona).css({"border-color":"red"});
    $('#lu_'+id_zona).focus();

  }else{

    $.ajax({
            url : "BL/lugaresController.php",
            type: "POST",
            data : {update_nombre_zona:'update_nombre_zona', idlugar: id_zona, descrip:descripnew},
            success: function(data)
            {   
              if(data == true){
                  $('#lu_'+id_zona).val(descripnew);
                  nan(id_zona);
              }
            }
          });
  }
  
}


function llenar(id){

  $("#lugar_id").val(id);

   var id = $("#lugar_id").val();

        setTimeout(function(){
          
          $.ajax({
            url : "BL/lugaresController.php",
            type: "POST",
            data : {listar:'listar', idlugar: id},
            success: function(data)
            {   

              if ( data.length > 0 ) {
                for (var x=0; x<data.length; x++) {
                  coords = data[x].new;
                  for (var i=0; i<coords.length; i++) {
                      coords[i].lat= parseFloat(coords[i].lat);
                      coords[i].lng = parseFloat(coords[i].lng);
                  }

                  poligono = new google.maps.Polygon({
                    paths: coords,
                    strokeColor: "#125412",
                    strokeOpacity: 3,
                    strokeWeight: 2,
                    fillColor: "#458b45",
                    fillOpacity: .3
                  });

                  poligono.setMap(map);
                }
              }
            }
          });
          cargareditado(id);
        }, 1000);


}

function init_mapa() {

    var bangalore = {lat: <?=$result[0]['usuario_latitud'];?>, lng: <?=$result[0]['usuario_longitud']?>};
    map = new google.maps.Map(document.getElementById('gmap_canvas_edit'), {
      zoom: 14,
      center: bangalore,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    google.maps.event.addListener(map, 'click', function(event) {
      leftClick2(event.latLng);
    });

    $("#myModal").on("shown.bs.modal", function () {
       google.maps.event.trigger(map, 'resize');
       moveToLocation(<?=$result[0]['usuario_latitud'];?>,<?=$result[0]['usuario_longitud']?>);
       
    });
}

function moveToLocation(lat, lng){
    var center = new google.maps.LatLng(lat, lng);
    map.panTo(center);
    map.setZoom(15); 
}

/*function addMarker(location, map) {
    var marker = new google.maps.Marker({
      position: location,
      map: map
    });
}*/

function removeLine() {
  poligono.setMap(null);
  parent.location.reload();
}

function cargareditado(id) {
  $.ajax({
            url : "BL/lugaresController.php",
            type: "POST",
            data : {listar_in:'listar_in', idlugar: id},
            success: function(data)
            {   
              var latcenter = data[0].lugar_lat;
              var lnhcenter = data[0].lugar_lon;
              moveToLocation(latcenter,lnhcenter);

              if ( data.length > 0 ) {
                document.getElementById("coordspoligonoedit").innerHTML = "";
                for (var x=0; x<data.length; x++) {
                  coordsedit = data[x].new;
                  for (var i=0; i<coordsedit.length; i++) {

                      coordsedit[i].lat= parseFloat(coordsedit[i].lat);
                      coordsedit[i].lng = parseFloat(coordsedit[i].lng);
                      var latitud = parseFloat(coordsedit[i].lat);
                      var longitud = parseFloat(coordsedit[i].lng);


                      image = new google.maps.MarkerImage('includes/img/square.png',
                              new google.maps.Size(11, 11),
                              new google.maps.Point(5,5),
                              new google.maps.Point(5, 5)
                      );

                      marker = new google.maps.Marker({
                        position: {lat: latitud, lng: longitud},
                        map: map,
                        icon:image,
                        draggable: true, 
                        bouncy: false, 
                        dragCrossMove: true
                      });

                      markers.push(marker);
                      marker.setMap(map);
                      
                      document.getElementById("coordspoligonoedit").innerHTML += "(" + latitud +", "+ longitud + "),<br>";

                      google.maps.event.addListener(marker, "drag", function () {
                            
                             if (poligonoedit)poligonoedit.setMap(null);
                                coordsedit.length = 0;
                                document.getElementById("coordspoligonoedit").innerHTML = "";
                                for (var i = 0; i < markers.length; i++) {
                                coordsedit.push(markers[i].getPosition());
                                document.getElementById("coordspoligonoedit").innerHTML += "" + markers[i].getPosition() + ",<br>";
                              }

                              /****/
                                poligonoedit = new google.maps.Polygon({
                                  paths: coordsedit,
                                  strokeColor: "#b23325",
                                  strokeOpacity: 3,
                                  strokeWeight: 2,
                                  fillColor: "#ea5d4d",
                                  fillOpacity: .3
                                });

                                var unit = " km&sup2;"; 
                                  //var area = polyShape.getPath() / (1000 * 1000); falta hacer esto

                                if (markers.length <= 2) {
                                    //report.innerHTML = "&nbsp;";
                                }
                                else if (markers.length > 2) {
                                    //report.innerHTML = area.toFixed(3) + unit;
                                }

                                poligonoedit.setMap(map);
                              /****/

                      });

                      google.maps.event.addListener(marker, "click", function () {
                          for (var n = 0; n < markers.length; n++) {
                              if (markers[n] == marker) {
                                  markers[n].setMap(null);
                                  break;
                              }
                          }
                          markers.splice(n, 1);
                              
                              if (poligonoedit)poligonoedit.setMap(null);
                              coordsedit.length = 0;
                              document.getElementById("coordspoligonoedit").innerHTML = "";
                              for (var i = 0; i < markers.length; i++) {
                                coordsedit.push(markers[i].getPosition());
                                document.getElementById("coordspoligonoedit").innerHTML += "" + markers[i].getPosition() + ",<br>";
                              }

                              /****/
                                poligonoedit = new google.maps.Polygon({
                                  paths: coordsedit,
                                  strokeColor: "#b23325",
                                  strokeOpacity: 3,
                                  strokeWeight: 2,
                                  fillColor: "#ea5d4d",
                                  fillOpacity: .3
                                });

                                var unit = " km&sup2;"; 
                                  //var area = polyShape.getPath() / (1000 * 1000); falta hacer esto
                                if (markers.length <= 2) {
                                    //report.innerHTML = "&nbsp;";
                                }
                                else if (markers.length > 2) {
                                    //report.innerHTML = area.toFixed(3) + unit;
                                }
                                poligonoedit.setMap(map);
                              /****/

                      });
                  }

                  poligonoedit = new google.maps.Polygon({
                    paths: coordsedit,
                    strokeColor: "#b23325",
                    strokeOpacity: 3,
                    strokeWeight: 2,
                    fillColor: "#ea5d4d",
                    fillOpacity: .3
                  });

                  var unit = " km&sup2;"; 
                  //var area = polyShape.getPath() / (1000 * 1000); falta hacer esto

                  if (markers.length <= 2) {
                      //report.innerHTML = "&nbsp;";
                  }
                  else if (markers.length > 2) {
                      //report.innerHTML = area.toFixed(3) + unit;
                  }
                  poligonoedit.setMap(map);

                }
              }
            }
          });
}

/*******************/
function drawPoly() {

    if (poligonoedit)poligonoedit.setMap(null);
    coordsedit.length = 0;
    document.getElementById("coordspoligonoedit").innerHTML = "";
    for (var i = 0; i < markers.length; i++) {
        coordsedit.push(markers[i].getPosition());
        document.getElementById("coordspoligonoedit").innerHTML += "" + markers[i].getPosition() + ",<br>";

    }
  // Construct the polygon.
   poligonoedit = new google.maps.Polygon({
                    paths: coordsedit,
                    strokeColor: "#b23325",
                    strokeOpacity: 3,
                    strokeWeight: 2,
                    fillColor: "#ea5d4d",
                    fillOpacity: .3
                  });

    var unit = " km&sup2;"; 
    //var area = polyShape.getPath() / (1000 * 1000); falta hacer esto

    if (markers.length <= 2) {
        //report.innerHTML = "&nbsp;";
    }
    else if (markers.length > 2) {
        //report.innerHTML = area.toFixed(3) + unit;
    }

    poligonoedit.setMap(map);
}

function leftClick2(location) {
    if (map) {
        // NUMERO MAXIMO DE VERTICES
        var MaxMarker = 200;
        if (markers.length < MaxMarker) {

      image = new google.maps.MarkerImage('includes/img/square.png',
              new google.maps.Size(11, 11),
              new google.maps.Point(5,5),
              new google.maps.Point(5, 5)
            );

      var marker = new google.maps.Marker({
          position: location,
          map: map,
          icon:image,
          draggable: true, 
          bouncy: false, 
          dragCrossMove: true
        });

      markers.push(marker);
      marker.setMap(map)

            google.maps.event.addListener(marker, "drag", function () {
                drawPoly();
            });

            google.maps.event.addListener(marker, "click", function () {

                for (var n = 0; n < markers.length; n++) {
                    if (markers[n] == marker) {
                        markers[n].setMap(null);
                        break;
                    }
                }
                markers.splice(n, 1);
                drawPoly();
            });
            drawPoly();
        } else {
            alert("Sólo puedes registrar hasta " + MaxMarker + " vértices");
        }
    
  }
}

function ActualizarCambios() {

    var coordsarray = $("#coordspoligonoedit").html();
      var id = $("#lugar_id").val();
      var arrayg = coordsarray.split(',<br>');

  if (arrayg.length < 3 ) {
    alert("Debe indicar por lo menos 3 vértices");  
  } else {
    $.ajax({
            url : "../BL/lugaresController.php",
            type: "POST",
            data : { edit: 'edit',coordenadas: arrayg,idlugar:id},
            success: function(data)
            { 
              if(data == true){
                clearEdit();
                window.location = '../index.php?seccion=listazona&status=true';
              }
              
            }
        });
  }
}

function clearEdit() 
{
    poligonoedit.length = 0;
    markers.length = 0;
    $("#coordspoligonoedit").html("");
    $("#centerpoligonoedit").html("");

}
</script>