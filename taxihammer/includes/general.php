<?php
include'seguridad.php';
require_once'DAL/generalDAO.php';
$generalDAO = new generalDAO();
$result = $generalDAO->lista();
?>
<style type="text/css">
  #group-kilometraje{display: none;}
    .slider{margin-left: 37px;}
      #mtncd {display: none;}

</style>
<div class="row" ng-app="myapp" ng-controller="GeneralController">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Información General</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" method="post" action="../BL/generalController.php" name="myConfig">
        <input type="hidden" name="insert_config">
        <div class="box-body">
        <div class="form-group">
            <label for="correo">Correo</label>
            <input type="email" class="form-control" name="correo" placeholder="Ej. shuanay0@appslovers.com" required>
          </div>
         <div class="form-group">
            <label for="telefono">Teléfono</label>
            <input type="text" class="form-control" name="telefono" placeholder="Ej: 943501461" required>
          </div>
          <div class="form-group">
            <label for="web">Web</label>
            <input type="text" class="form-control" name="web" placeholder="Ej: www.saul-huanay-web-senior.com" required>
          </div>
          <div class="form-group">
            <label for="costocancel">Costo de cancelacion Coorporativo</label>
            <input type="number" class="form-control" name="costocancel" placeholder="Ingrese costo de cancelacion" required>
          </div>
          <div class="form-group">
            <label>Configurar Radio : </label> <strong id="rdjson">1</strong>
            <input id="ex13" type="text" data-slider-ticks="[1, 2, 3, 4, 5]" data-slider-ticks-snap-bounds="30" data-slider-ticks-labels='["1 Km", "2 Km", "3 Km", "4 Km", "5 Km"]'/>
          </div>
          <div class="form-group">
            <div class="radio">
              <label><input type="radio" name="optradio" value="1" checked>Por Zonas</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="optradio" value="2">Por Kilometrajes</label>
            </div>
          </div>

          <div class="form-group" id="group-kilometraje">
            <!-- Factor Empresarial -->

            <div class="input-group">
                <div class="radio" style="margin: 0px;">
                  <input type="checkbox" name="checkboxFactorEmpresarial" id="checkboxFactorEmpresarial"> Factor Empresarial
                </div>
            </div>
            <br>
            <div id="contentEmpresarial" style="display:none"  >
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">Tarifa Base </span>
              <input type="text" class="form-control" name="tarifaminEmpresarial" id="tarifaminEmpresarial" aria-describedby="basic-addon1" placeholder="Ingrese una tarifa base.">
            </div>
            <br>
             <div class="input-group">
              <span class="input-group-addon" id="basic-addon2">Factor por Km </span>
              <input type="text" class="form-control" name="factkmEmpresarial" id="factkmEmpresarial" aria-describedby="basic-addon2" placeholder="Factor de tarifa por KM">
            </div>
            <br>
             <div class="input-group">
              <span class="input-group-addon" id="basic-addon3">Factor por Minuto </span>
              <input type="text" class="form-control" name="factimEmpresarial" id="factimEmpresarial" aria-describedby="basic-addon3" placeholder="Factor de tarifa por Tiempo">
            </div>
            <br>
            </div>
              <!-- Factor Particular -->
            <div class="input-group">
              <div class="radio" style="margin: 0px;">
               <input type="checkbox" name="checkboxFactorParticular" id="checkboxFactorParticular" > Factor Particular
              </div>
            </div>

            <div id="contentParticular" style="display:none" >
            <br>

            <div class="input-group">
              <span class="input-group-addon" id="basic-addon4">Tarifa Base </span>
              <input type="text" class="form-control" name="tarifaminParticular" id="tarifaminParticular" aria-describedby="basic-addon4" placeholder="Ingrese una tarifa base.">
            </div>
            <br>
             <div class="input-group">
              <span class="input-group-addon" id="basic-addon5">Factor por Km </span>
              <input type="text" class="form-control" name="factkmParticular" id="factkmParticular" aria-describedby="basic-addon5" placeholder="Factor de tarifa por KM">
            </div>
            <br>
             <div class="input-group">
              <span class="input-group-addon" id="basic-addon6">Factor por Minuto </span>
              <input type="text" class="form-control" name="factimParticular" id="factimParticular" aria-describedby="basic-addon6" placeholder="Factor de tarifa por Tiempo">
            </div>
            <br>

            </div>
            <!-- END -->
            <br>
            <br>
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon4">Tarifa Minima </span>
                  <input type="text" class="form-control" name="tarifaminima" id="tarifaminima" aria-describedby="basic-addon4" placeholder="Ingrese una tarifa minima.">
                </div>
              <br>
                <!-- 08/10/16-->
            <!-- END -->
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <?php if(count($result) == 0 || count($result) < 1){ ?>
            <button type="submit" class="btn btn-primary">Guardar Datos</button>
          <?php } ?>
        </div>
      </form>
    </div><!-- /.box -->
  </div>

  <div class="col-md-9">
    <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Operación Realizada con Éxito</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado de Usuarios</h3>
      </div><!-- /.box-header -->
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Id</th>
              <th>Correo</th>
              <th>Teléfono</th>
              <th>Web</th>
              <th>Radio</th>
              <th>Tarifa</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat = "general in generales |orderBy:columnToOrder:reverse">
              <td>{{general.config_id}}</td>
              <td>{{general.email}}</td>
              <td>{{general.telefono}}</td>
              <td>{{general.web}}</td>
              <td>{{general.radio}} KM</td>
              <td>{{general.texto}}</td>
              <td><a class="btn btn-warning btn-circle" data-toggle="modal" data-target="#myModal" ng-click="Editar(general.config_id)"><i class="glyphicon glyphicon-edit"></i></a></button></td>
            </tr>
          </tbody>
        </table>
        <div class="row">
            <div class="col-sm-5" style="margin-left:15px;">
                <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                <!-- Showing 1 to 10 of 848 entries -->
            </div>
            <div class="col-sm-6" style="text-align: right;">
                <div class="dataTables_paginate paging_simple_numbers">
                  <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar Usuarios</h4>
        </div>
        <form role="form" method="post" action="../BL/generalController.php">
           <input type="hidden" name="id" id="idconfig">
           <input type="hidden" name="hidden_config_update">
        <div class="modal-body">
          <div class="box-body">
              <div class="form-group">
                <label for="correoedit">Correo</label>
                <input type="email" class="form-control" id="correoedit" name="correoedit" placeholder="Ej. dalva210@gmail.com" required>
              </div>
             <div class="form-group">
                <label for="telefonoedit">Teléfono</label>
                <input type="text" class="form-control" name="telefonoedit" id="telefonoedit" placeholder="Ej: 943501461" required>
              </div>
              <div class="form-group">
                <label for="web">Web</label>
                <input type="text" class="form-control" name="webedit" id="webedit" placeholder="Ej: www.danny-alva-ios-senior.com" required>
              </div>
              <div class="form-group">
                <label for="costocancel">Costo de cancelacion Coorporativo</label>
                <input type="number" class="form-control" name="costocanceledit" id="costocanceledit" placeholder="Ingrese costo de cancelacion" required>
              </div>

              <div class="form-group">
                <label for="catlicencia">Asignar Comision</label>
                <select class="form-control" name="comisionid" id="comisionid" onchange="show(document.getElementById('comisionid').value)" required>
                    <option value ="">Seleccione una Comision</option>
                    <option value ="1">% Porcentaje</option>
                    <option value ="2">S/ Monto</option>
                </select>
              </div>
              <div class="form-group" id="mtncd">
                <label for="montoconductor">Ingresar el Monto/Comision</label>
                <input type="number" step="any" class="form-control" name="montoconductor" id="montoconductor" placeholder="Ingresar el Monto/Comision" required>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="incraeroO">Aeropuerto por traslado</label>
                    <input type="number" class="form-control" name="incraeroO" id="incraeroO" min="0" placeholder="Incremento" required style="width:60%">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="incraeroD">Aeropuerto por recojo</label>
                    <input type="number" class="form-control" name="incraeroD" id="incraeroD" min="0" placeholder="Incremento" required style="width:60%">
                  </div>
                </div>
              </div>
              <div class="form-group" id="slider">
                <label>Configurar Radio : </label>
                <input id="ex14" type="text" data-slider-ticks="[1, 2, 3, 4, 5]" data-slider-ticks-snap-bounds="30" data-slider-ticks-labels='["1 Km", "2 Km", "3 Km", "4 Km", "5 Km"]'/>
              </div>
              <div class="form-group">
                <div class="radio">
                  <label><input type="radio" name="optradioedit" id="zonas" value="1" disabled>Por Zonas</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="optradioedit" id="kilom" value="2">Por Kilometrajes</label>
                </div>
              </div>
              <div class="" id="group-kilometrajedit">

              <!-- Factor Empresarial -->

              <div class="input-group">
                  <div class="radio" style="margin: 0px;">
                    <input type="checkbox" name="checkboxFactorEditEmpresarial" id="checkboxFactorEditEmpresarial" disabled> Factor Empresarial
                  </div>
              </div>
              <div id="contentEditEmpresarial" >
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Tarifa Base </span>
                <input type="text" class="form-control" name="tarifamineditEmpresarial" id="tarifamineditEmpresarial" aria-describedby="basic-addon1" placeholder="Ingrese una tarifa base.">
              </div>
               <br>
               <div class="input-group">
                <span class="input-group-addon" id="basic-addon2">Factor por Km </span>
                <input type="text" class="form-control" name="factkmeditEmpresarial" id="factkmeditEmpresarial" aria-describedby="basic-addon2" placeholder="Factor de tarifa por KM">
              </div>
               <br>
               <div class="input-group">
                <span class="input-group-addon" id="basic-addon3">Factor por Minuto </span>
                <input type="text" class="form-control" name="factimeditEmpresarial" id="factimeditEmpresarial" aria-describedby="basic-addon3" placeholder="Factor de tarifa por Tiempo">
              </div>
              </div>
                <!-- Factor Particular -->
              <div class="input-group">
                <div class="radio" style="margin: 0px;">
                 <input type="checkbox" name="checkboxFactorEditParticular" id="checkboxFactorEditParticular" > Factor Particular
                </div>
              </div>

              <div id="contentEditParticular" >
              <br>

              <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">Tarifa Base </span>
                <input type="text" class="form-control" name="tarifamineditParticular" id="tarifamineditParticular" aria-describedby="basic-addon4" placeholder="Ingrese una tarifa base.">
              </div>
              <br>
               <div class="input-group">
                <span class="input-group-addon" id="basic-addon5">Factor por Km </span>
                <input type="text" class="form-control" name="factkmeditParticular" id="factkmeditParticular" aria-describedby="basic-addon5" placeholder="Factor de tarifa por KM">
              </div>
              <br>
               <div class="input-group">
                <span class="input-group-addon" id="basic-addon6">Factor por Minuto </span>
                <input type="text" class="form-control" name="factimeditParticular" id="factimeditParticular" aria-describedby="basic-addon6" placeholder="Factor de tarifa por Tiempo">
              </div>
              <br>

              </div>

              <!-- END -->
              <br>
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon4">Tarifa Minima </span>
                    <input type="text" class="form-control" name="tarifaminimaedit" id="tarifaminimaedit" aria-describedby="basic-addon4" placeholder="Ingrese una tarifa minima.">
                  </div>
                <br>
                 

                  <!-- 08/10/16-->
            


              <!-- END -->
              </div>
          </div><!-- /.box-body -->
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Actualizar Cambios</button>
        </div>
      </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</div>

<script type="text/javascript">


  $("#ex13").slider({
    ticks: [1, 2, 3, 4, 5],
    ticks_labels: ["1 Km", "2 Km", "3 Km", "4 Km", "5 Km"],
    ticks_snap_bounds: 30
  });

  var slider = new Slider("#ex13", {
    ticks: [1, 2, 3, 4, 5],
    ticks_labels: ["1 Km", "2 Km", "3 Km", "4 Km", "5 Km"],
    ticks_snap_bounds: 30,
    value:1
  });


  $("#ex14").slider({
    ticks: [1, 2, 3, 4, 5],
    ticks_labels: ["1 Km", "2 Km", "3 Km", "4 Km", "5 Km"],
    ticks_snap_bounds: 30
  });

    var rad = document.myConfig.optradio;
    var prev = null;

    for(var i = 0; i < rad.length; i++) {
        rad[i].onclick = function() {
            (prev)?prev.value:null;
            if(this !== prev) {
                prev = this;
            }

            if(this.value != 1){
              $("#group-kilometraje").css({"display":"block"});
              clearTarifa();
            }else{
              $("#group-kilometraje").css({"display":"none"});
            }
        };
    }


$(document).ready(function(){
  
  $('#ex13').change(function() {
    $('#rdjson').text($(this).val());
  });

  $('input[name=optradioedit]').click(function() {
    if($('input[name=optradioedit]:checked').val() == 1){
      $('#group-kilometrajedit').hide();
      //clearTarifa();
    }else{
      $('#group-kilometrajedit').show();
    }
  });
});

function clearTarifa(){

  $('#tarifamin').val('');
  $('#factkm').val('');
  $('#factime').val('');
  $('#fachora').val('');
  $('#tarifaminimaedit').val('');
  $('#factempresarialedit').val('');
  $('#factparticularedit').val('');
}

function show(idtipo){

    if(idtipo == 1 || idtipo == 2){
      $("#mtncd").show();
      $("#montoconductor").focus();
    }else{
      $("#mtncd").hide();
      $("#montoconductor").val('');
    }
}


/* Edwin */
$(document).on('click','#checkboxFactorEditEmpresarial',function(){
  $('#contentEditEmpresarial').fadeToggle();
});

$(document).on('click','#checkboxFactorEditParticular',function(){
  $('#contentEditParticular').fadeToggle();
});

$(document).on('click','#checkboxFactorEmpresarial',function(){
  $('#contentEmpresarial').fadeToggle();
});

$(document).on('click','#checkboxFactorParticular',function(){
  $('#contentParticular').fadeToggle();
});

</script>
<script type="text/javascript">

  $(document).ready(function(){
      $('#ex14').change(function() {
        var valor = parseInt($(this).val());

        $.ajax({
              url : "../BL/generalController.php",
              type: "POST",
              data : { valor : valor , radio : 'radio'},
              success: function(data)
              { 
                  if(data = true){
                    window.location = '../index.php?seccion=general&status=true';
                  }
              }
        });

      });
  });
</script>