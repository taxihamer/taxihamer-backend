<?php
  include'seguridad.php';
  require_once'DAL/clientesDAO.php';

  $clientesDAO = new clientesDAO();
  $tipocli = $clientesDAO->listypeclientes(); 

  $start = 0;
  $per_page_inic = 10;
  //$result_Cliente = $clientesDAO->listaPaginacion($start,$per_page_inic);
  $listadocliente = $clientesDAO->lista();
  $totallista = count($listadocliente);
  $paginacion = ceil(count($listadocliente)/$per_page_inic);

?>
<style type="text/css">
  #page{
    text-align: right;
    padding-bottom: 15px;
    padding-right: 15px;
  }
  ul#pagination {
    list-style: none;
  }

  ul#pagination li {
    display: inline-block;
    padding: 5px 10px;
    border: 1px solid #999;
    border-radius: 5px;
    margin: 3px;
    cursor: pointer;
  }

  #textmost{
    text-align: left;margin-left: 15px;margin-bottom: 15px;
  }

  table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
  }

  .nullOld{text-align: center;color: #e23558;}
</style>
<div class="row" ng-app="myapp" ng-controller="clientesController">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Mantenimiento de Clientes</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" method="post" action="../BL/clientesController.php">
        <input type="hidden" name="hidden_cliente_insert">
        <div class="box-body">
          <div class="form-group">
            <label for="tipo">Tipo</label>
            <select class="form-control" name="tipo" id="tipo" onchange="show(document.getElementById('tipo').value,'new')">
              <option value="-1">Seleccione</option>
            <?php foreach($tipocli as $i => $type){ ?>
              <option value="<?=$type['tipocliente_id']?>"><?=$type['tipocliente_descripcion']?></option>
            <?php } ?>
            </select>
          </div>
          <div class="form-group" style="display:none;" id="groupempresa">
            <label for="empresa">Empresas</label>
            <select class="form-control" name="empresa" id="empresa"></select>
          </div>
          <div class="form-group" id="nombres">
            <label for="nombres" >Nombres</label>
            <input type="text" class="form-control" name="nombres" placeholder="Nombres" required>
          </div>
          <div class="form-group" id="apellidos">
            <label for="apellidos">Apellidos</label>
            <input type="text" class="form-control" name="apellidos" placeholder="Apellidos" required>
          </div>
          <div class="form-group" style="display:none" id="groupcosto">
            <label for="groupcosto">Centro de costos</label>
            <input type="text" class="form-control" name="costo" placeholder="Costos">
          </div>
          <div class="form-group" style="display:none" id="supervisor">
            <label for="supervisor">Nombre de Supervisor</label>
            <input type="text" class="form-control" name="supervisor" placeholder="Nombre de Supervisor">
          </div>
          <div class="form-group">
            <label for="celular">Celular</label>
            <input type="text" class="form-control" name="celular" placeholder="Celular" required>
          </div>
          <div class="form-group">
            <label for="correo">Correo</label>
            <input type="email" class="form-control" name="correo" placeholder="correo" required>
          </div>
          <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" class="form-control" name="password" placeholder="Contraseña" required>
          </div>
          <div class="form-group" style="display:none" id="status">
            <label class="c-input c-radio" style="margin-right:15px;">
              <input id="radio1" name="radio" type="radio" value="1">
              <span class="c-indicator"></span>
              Activo
            </label>
            <label class="c-input c-radio">
              <input id="radio2" name="radio" type="radio" value="0">
              <span class="c-indicator"></span>
              Inactivo
            </label>
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
   <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="alert alert-danger" id="msgError" style="display:none">
        <button type="button" class="close" onclick="CloseConductor()">&times;</button>
        <strong>¡Cuidado!</strong> <span></span>
      </div>
      <div class="box-header with-border">
        <h3 class="box-title">Listado de Clientes</h3>
      </div><!-- /.box-header -->
      <div class="dataTables_wrapper form-inline" role="grid">
          <div class="row" style="margin-left:0px;margin-right:0px;">
            <div class="col-sm-6">
                <div class="dataTables_length" id="example_length">
                    <label>Exportar : </label>
                    <form role="form" action="../BL/exportarController.php" enctype="multipart/form-data" method="post" style="width: 30%;
                      display: inline-block;">
                      <input type="hidden" name="hidden_exportar" value="1">
                      <input type="hidden" name="exportar" value="5">
                      <button type="submit" class="btn btn-primary btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
            <div class="col-sm-12" style="margin-top: 15px;">
                <div id="example_filter" class="dataTables_filter" style="width:100%">
                 <label style="margin-right: 30px; float:left;">Tipo de Cliente : <select class="form-control" id="tipoipo" ng-change="tipoCliente()" ng-model="stipoCliente">
                    <option value="">Seleccione </option>  
                    <?php foreach($tipocli as $i => $type){ ?>
                      <option value="<?=$type['tipocliente_id']?>"><?=$type['tipocliente_descripcion']?></option>
                    <?php } ?>
                    </select></label>
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                      <input style="margin-left:0px;" type="search" class="form-control input-sm" aria-controls="example" ng-model="searchText" ng-change="searchTextChanged()" placeholder="Palabra clave">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign" style="color:#3c8dbc"></i></span>
                    </div>
                </div>
            </div>
          </div>
        <br>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Tipo</th>
                <th>Nombres y Apellidos</th>
                <th>Costo</th>
                <th>Nombre Supervisor</th>
                <th>Celular</th>
                <th>Correo</th>
                <th onclick="order(this)" id="asc" style="cursor:pointer;color:#3c8dbc;">Calificacion <span id="order" aria-hidden="true"></span></th>
                <th>Estado</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody id="response" class="buscar">
              <tr ng-repeat = "cliente in clientes |orderBy:columnToOrder:reverse">
                <td>{{cliente.tipocliente_descripcion}}</td>
                <td>{{cliente.cliente_nombre}} {{cliente.cliente_apellido}}</td>
                <td>{{cliente.cliente_costos}}</td>
                <td>{{cliente.cliente_supervisor}}</td>
                <td>{{cliente.cliente_celular}}</td>
                <td>{{cliente.cliente_correo}}</td>
                <td>{{cliente.cliente_calificacion}}</td>
                <td>
                  <div ng-if="cliente.cliente_estados==1">
                    <switch name="enabled" ng-model="cliente.enabled" ng-change="changeEstado(cliente.cliente_id,cliente.enabled)" on="on" off="off"></switch>
                  </div> 
                  <div ng-if="cliente.cliente_estados==0">
                      <switch name="enabled" ng-model="cliente.enabled" ng-change="changeEstado(cliente.cliente_id,cliente.enabled)" on="on" off="off"></switch>
                  </div>   
                </td>
                <td><button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#myModal" ng-click="Editar(cliente.cliente_id)"><i class="glyphicon glyphicon-edit"></i></button> <a class="btn btn-danger btn-circle" ng-click="EliminarCliente(cliente.cliente_id)"><i class="glyphicon glyphicon-remove"></i></a></td>
              </tr>
              <tr class="nullOld" id="nullOld">
                <td colspan="9">No hay datos a mostrar</td>
              </tr>
            </tbody>
          </table>
          <div class="row">
              <div class="col-sm-5" style="margin-left:15px;">
                  <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                  <!-- Showing 1 to 10 of 848 entries -->
              </div>
              <div class="col-sm-6" style="text-align: right;">
                  <div class="dataTables_paginate paging_simple_numbers">
                    <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                  </div>
              </div>
          </div>
          <!-- Paginacion -->
          <!--div id="textmost">Mostrando <b id="to">1</b> a <b id="ta">10</b> de <b><?=$totallista?></b> entradas</div>
          <div id="page">
            <input type="hidden" name="filtropage" id="filtropage">
            <ul id="pagination">
                  <?php
                  $screen = 10;
                  $v = 0;
                  if ($paginacion > 0) {
                    echo '<li id="0" data="prev" class="pprev" onclick="pagi(this,2)">Previous</li>';
                  }

                  for ($i=1; $i <=$paginacion; $i++) { 
                      if($i <= $screen){
                        echo '<li id="'.$i.'" onclick="pagi(this,2)" >'.$i.'</li>';
                        $v = $i;
                      }
                  }

                  if($paginacion > $v){
                    $v = $v+1;
                    echo '<li id="'.$v.'" class="pmor" onclick="pagi(this,2)">...</li>';
                  }

                  if ($paginacion > $screen) {
                    echo '<li id="1" data="next" class="pnext" onclick="pagi(this,2)">Next</li>';
                  } 

                  ?>
            </ul>
          </div-->
          <!-- End Paginacion -->
      </div>
    </div t="primary">
  </div t="9">
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar Cliente</h4>
        </div>
        <form role="form" method="post" action="../BL/clientesController.php">
            <input type="hidden" name="cliente_id" id="cliente_id">
            <input type="hidden" name="hidden_update_cliente" value="1">
        <div class="modal-body">
          <div class="box-body">
          <div class="form-group">
              <label for="tipo">Tipo</label>
              <select class="form-control" name="tipo" id="tipoedit" onchange="show(document.getElementById('tipoedit').value,'edit')">
                <option value="-1">Seleccione</option>
              <?php foreach($tipocli as $i => $type){ ?>
                <option value="<?=$type['tipocliente_id']?>"><?=$type['tipocliente_descripcion']?></option>
              <?php } ?>
              </select>
            </div>
            <div class="form-group" style="display:none;" id="groupempresaedit">
              <label for="empresa">Empresas</label>
              <select class="form-control" name="empresa" id="empresaedit"></select>
            </div>
            <div class="form-group" id="nombres">
              <label for="nombres" >Nombres</label>
              <input type="text" class="form-control" id="nombresedit" name="nombres" placeholder="Nombres" required>
            </div>
            <div class="form-group" id="apellidos">
              <label for="apellidos">Apellidos</label>
              <input type="text" class="form-control" id="apellidosedit" name="apellidos" placeholder="Apellidos" required>
            </div>
            <div class="form-group" style="display:none" id="groupcostoedit">
              <label for="groupcosto">Centro de costos</label>
              <input type="text" class="form-control" id="costoedit" name="costo" placeholder="Costos">
            </div>
            <div class="form-group" style="display:none" id="groupsupervisoredit">
              <label for="supervisor">Nombre de Supervisor</label>
              <input type="text" class="form-control" id="supervisoredit" name="supervisor" placeholder="Nombre de Supervisor">
            </div>
            <div class="form-group">
              <label for="celular">Celular</label>
              <input type="text" class="form-control" id="celularedit" name="celular" placeholder="Celular" required>
            </div>
            <div class="form-group">
              <label for="correo">Correo</label>
              <input type="email" class="form-control" id="correoedit" name="correo" placeholder="correo" required>
            </div>
            <div class="form-group">
              <label for="password">Contraseña</label>
              <input type="password" class="form-control" id="passwordedit" name="password" placeholder="Contraseña" required>
            </div>
            <div class="form-group"  id="statusedit">
              <label class="c-input c-radio" style="margin-right:15px;">
                <input id="radio1edit" name="radioedit" type="radio" value="1">
                <span class="c-indicator"></span>
                Activo
              </label>
              <label class="c-input c-radio">
                <input id="radio2edit" name="radioedit" type="radio" value="0">
                <span class="c-indicator"></span>
                Inactivo
              </label>
            </div>
          </div><!-- /.box-body -->
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Guardar Cambios</button>
        </div>
      </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</div>
<script type="text/javascript">

  /*
  var PageInici = <?=$screen?>;
  var PageTotal = <?=$paginacion?>;

  $(document).ready(function () {

    $('#pagination li:nth-child(2)').css({'color':'#FF9900'});
    $('input[name="my-checkbox"]').bootstrapSwitch();
  
        $('#name').keyup(function () {
          var rex = new RegExp($(this).val(), 'i');
            $.ajax({
                url: 'BL/clientesController.php',
                data: {
                    nombre: rex,
                    buscar: 1
                },
                cache: false,
                method: 'post'
            }).done(function (data) {
                console.log(data);
                html_paginacion(data);
                $('#pagination li:nth-child(2)').css({'color':'#FF9900'});
                $('input[name="my-checkbox"]').bootstrapSwitch();
            });
        });
    
  });
  */

  function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
  }

  function order(data){
    var send = 0;
    $("#order").removeClass();
    if(data.id == "asc"){
      $("#order").addClass("glyphicon glyphicon-menu-down");
      data.id = "desc";
      send = 'asc';
    }else{
      $("#order").addClass("glyphicon glyphicon-menu-up");
      data.id = "asc";
      send = 'desc';
    }
    
    $.ajax({
        url: 'BL/clientesController.php',
        data: {
            order: send,
            orders: "orders"
        },
        cache: false,
        method: 'post'
    }).done(function (data) {

        html_paginacion(data);
        $('#pagination li:nth-child(2)').css({'color':'#FF9900'});
        $('input[name="my-checkbox"]').bootstrapSwitch();
        
    });
  }
  /*
  function html_paginacion(data){

    html = '';
    li = '';

    if(data.Request.length > 0){

      $.each( data.Request, function( key, value ) {
        var cliente_costos = (value.cliente_costos != null)?value.cliente_costos:"";
        var cliente_supervisor = (value.cliente_supervisor != null)?value.cliente_supervisor:"";

        html+= '<tr>'+
                    '<td>'+value.tipocliente_descripcion+'</td>'+
                    '<td>'+value.cliente_nombre+' '+value.cliente_apellido+'</td>'+
                    '<td>'+cliente_costos+'</td>'+
                    '<td>'+cliente_supervisor+'</td>'+
                    '<td>'+value.cliente_celular+'</td>'+
                    '<td>'+value.cliente_correo+'</td>'+
                    '<td>'+number_format(value.cliente_calificacion,1,'.',' ')+'</td>';
                      if(true){
                        var checked = (value.cliente_estados == 1)?'checked':'';
                        html+= '<td><input id="switch-size'+value.cliente_id+'" type="checkbox" name="my-checkbox" '+checked+' data-size="mini" onchange="Update('+value.cliente_id+')" dtat="'+value.cliente_estados+'"></td>';
                      }else{
                        html+= '<td></td>';
                      }
                      html +='<td><button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="llenar('+value.cliente_id+')"><i class="glyphicon glyphicon-edit"></i></button><a class="btn btn-danger" href="BL/clientesController.php?hidden_delete_cliente=1&id='+value.cliente_id+'"><i class="glyphicon glyphicon-remove"></i></a></td>'+
               '</tr>';
      });

      

      var screen = 10;
      var v = 0;


      if (data.Paginacion > 0) {
        li+='<li id="0" data="prev" class="pprev" onclick="pagi(this,1)">Previous</li>';
      }

      for (var i=1; i <=data.Paginacion; i++) { 
          if(i <= screen){
            li+='<li id="'+i+'" onclick="pagi(this,1)">'+i+'</li>';
            v = i;
          }
      }

      if(data.Paginacion > v){
        v = v+1;
        li+='<li id="'+v+'" class="pmor" onclick="pagi(this,1)">...</li>';
      }

      if (data.Paginacion > screen) {
        li+='<li id="1" data="next" class="pnext" onclick="pagi(this,1)" >Next</li>';
      } 

    }else{

      html='<tr><td colspan="10" align="center">No hay datos a Mostrar</td></tr>';

    }

    $('#response').html('');
    $('#response').html(html);
    $("#textmost").children('b').last().html(data.TotalCount);
    $("#pagination").html(li);

  }

  function pagi(input,type){

    $('#pagination li').css({'color':'#000'});
    $(input).css({'color':'#FF9900'});
    var pageNum = input.id;
    PageMostradoText(pageNum);
    var nextval = parseInt(pageNum)+1;
    var flag = $(input).attr('data');
    if(PageTotal == pageNum || pageNum == 0){
              if(pageNum == 0){
                $("#1").css({'color':'green'});
              }else{
                  $(".pmor").html(PageTotal);
                  $(".pmor").css({'color':'green'});
              }
      }else{
          if (typeof flag != "undefined") {
            if(flag == "next"){
                if(nextval > PageInici){
                  $(".pmor").html(nextval);
                  $(".pnext").css({'color':'#FF9900'});
                  $(".pmor").css({'color':'green'});
                }else{
                  $("#"+nextval).css({'color':'green'});
                }

                $(input).attr('id',nextval);
                $(".pprev").attr('id',nextval);
                pageNum = nextval;

            }else{

                pageNum = pageNum -1;

                if(pageNum > PageInici ){
                  $(".pmor").html(pageNum);
                  $(".pprev").css({'color':'#FF9900'});
                  $(".pmor").css({'color':'green'});
                }else{
                  $(".pmor").html('...');
                  $("#"+pageNum).css({'color':'green'});
                  $("#"+pageNum).attr('class','hola');
                }

                if(pageNum == 0){
                  pageNum = parseInt(pageNum)+1;
                  $(".pnext").attr('id',pageNum);
                  $("#"+pageNum).css({'color':'green'})
                }else{  
                  $(".pnext").attr('id',pageNum);
                }

                $(input).attr('id',pageNum);
                
            }
          }else{

            $(".pnext").attr('id',pageNum);
            $(".pprev").attr('id',pageNum);
          }

  
              $.ajax({
                  url: 'BL/clientesController.php',
                  data: {
                      hidden_clientes_paginacion: 1,
                      page: pageNum,
                      nombre:$('#name').val()
                  },
                  cache: false,
                  method: 'post'
              }).done(function (data) {
                  $('#response').html(data);
                  $('input[name="my-checkbox"]').bootstrapSwitch();
              });
      }
  }
  
  function PageMostradoText(pageNum){

      var ta = parseInt(parseInt(parseInt(pageNum)-1)+1)*10;
      var to = parseInt(parseInt(ta)-10)+1;
      $("#ta").html(ta);
      $("#to").html(to);
  } 
  */

  /*
  function Update(id){

    var estados = $('input[id="switch-size'+id+'"]').bootstrapSwitch('state');
    var es = (estados == true)?1:0;
     $.ajax({
          url: 'BL/clientesController.php',
          data: {
              tipo: es,
              cliente: id,
              estado: 1
          },
          cache: false,
          method: 'post'
      }).done(function (data) {
          window.location = '../index.php?seccion=clientes&status=true';
      });
  }

  function Search() {

    tipo = document.getElementById('tipe').value;
    nombre = document.getElementById('name').value;

    if(tipo != "-1" && nombre.length > 0){
      $("#msgError").hide();
      $.ajax({
          url: 'BL/clientesController.php',
          data: {
              tipo: tipo,
              nombre: nombre,
              buscar: 1
          },
          cache: false,
          method: 'post'
      }).done(function (data) {

          html_paginacion(data);
          $('#pagination li:nth-child(2)').css({'color':'#FF9900'});
          $('input[name="my-checkbox"]').bootstrapSwitch();
          
      });
      
    }else{
      $("#msgError").show();
      $("#msgError").children('span').html('Seleccione un tipo de cliente y ingrese un nombre');
    }
  }
  */
  function CloseConductor(){
    $("#msgError").hide();
  }

  function show(idtipo,flag){

    if(idtipo == 1 || idtipo == '-1'){
      switch(flag) {
        case 'edit' :
        
              $('#groupcostoedit').hide();
              $('#groupsupervisoredit').hide();
              $('#groupempresaedit').hide();
              //$('#statusedit').hide();
            break;
        default:
            $('#groupcosto').hide();$('#supervisor').hide();$('#groupempresa').hide();$('#status').hide();
        }
    }else{
        switch(flag) {
          case 'edit' :
                $('#groupcostoedit').show();
                $('#groupsupervisoredit').show();
                $('#groupempresaedit').show();
                //$('#statusedit').show();
                listaempresas(idtipo,2);
              break;
          default:
              $('#groupcosto').show();$('#supervisor').show();$('#groupempresa').show();listaempresas(idtipo,1);
              //$('#status').show();
          }
    }
  }

  function listaempresas(idtipo,flag){

    $.ajax({
            url : "../BL/empresaController.php",
            type: "POST",
            data : { id: idtipo,hidden_tipo_select: 'hidden_tipo_select'},
            success: function(data)
            {

              var htmls = '';
              $.each(data,function(index,obj)
              {
                htmls +='<option value="'+obj.empresa_id+'" >'+obj.empresa_razonsocial+'</option>';   
              });

              if(flag == 1){
                $("#empresa").html(htmls);
              }else{
                $("#empresaedit").html(htmls);
              }
            }
        });
  }

</script>
