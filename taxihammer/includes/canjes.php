<?php
include'seguridad.php';

?>
<style type="text/css">
	table.table > thead > tr > th, table.table > tbody > tr > td{
		text-align: center;
	}
  .nullOld{text-align: center;color: #e23558;}
</style>
<div class="row" ng-app="myapp" ng-controller="canjesController">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado de Canjes</h3>
      </div><!-- /.box-header -->
      <br>
      <div class="dataTables_wrapper form-inline" role="grid">
        <div class="row" style="margin-left:0px;margin-right:0px;">
          <div class="col-sm-12" style="margin-top: 15px;">
              <div id="example_filter" class="dataTables_filter" style="width:100%">
               <label style="margin-right: 30px; float:left;">Tipo de Usuario : <select class="form-control" id="tipe" ng-change="tipoUsuario()" ng-model="stipoUsuario">
               <option value="">Seleccione</option>
                <option value="2">Conductor</option>
                <option value="1">Cliente</option>
              </select></label>
                  <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                    <input style="margin-left:0px;" type="search" class="form-control input-sm" aria-controls="example" ng-model="searchText" ng-change="searchTextChanged()" placeholder="Palabra clave">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign" style="color:#3c8dbc"></i></span>
                  </div>
              </div>
          </div>
        </div>
        <br>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nº</th>
                <th>Nombre Promocion</th>
                <th>Imagen</th>
                <th>Tipo Usuario</th>
                <th>Usuario</th>
                <th>Fecha</th>
              </tr>
            </thead>
            <tbody id="response" class="buscar">
              <tr ng-repeat = "canje in canjes |orderBy:columnToOrder:reverse">
                <td>{{$index + 1}}</td>
                <td>{{canje.promo_nombre}}</td>
                <td><img ng-src="BL/{{canje.promo_imagen}}" alt="taxi" width="50px" class="img-circle img-circle center-block" /></td>
                <td>{{canje.TipoUsuario}}</td>
                <td>{{canje.Nombre}}</td>
                <td>{{canje.detalle_fecha}}</td>
              </tr>
                <tr class="nullOld" id="nullOld">
                  <td colspan="6">No hay datos a mostrar</td>
                </tr>
            </tbody>
          </table>
          <div class="row">
              <div class="col-sm-5" style="margin-left:15px;">
                  <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                  <!-- Showing 1 to 10 of 848 entries -->
              </div>
              <div class="col-sm-6" style="text-align: right;">
                  <div class="dataTables_paginate paging_simple_numbers">
                    <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                  </div>
              </div>
          </div>
      </div>
    </div t="primary">
  </div t="col12">
</div t="ro1">
<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#name').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.buscar tr').hide();
            $('.buscar tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
 
  });
</script>