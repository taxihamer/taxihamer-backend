<?php
  include'seguridad.php';

  require_once'DAL/usuarioDAO.php';

$usuarioDAO = new usuarioDAO();
$data['rol_id'] = $_SESSION['rol_id'];
$data['mail'] = $_SESSION['mail'];
$result = $usuarioDAO->listacoordenadas($data);
?>
<style>
  #gmap_reserva{
    height:750px;
    width:100%;
  }
  #gmap_reserva img{
    max-width:none!important;
    background:none!important;
  }
  #coordspoligonoedit , #centerpoligonoedit{
    display: none;
  }

  #divLeyenda{
  	border-radius: 5px;
    box-shadow: inset 0 0 10px #0B3285;
    background-color: #fff;
    width: 180px;
    /*height: 249px;*/
    position: absolute;
    bottom: 45px;
    left: 50px;
    font-size: 12px;
    font-weight: bold;
    padding: 20px;
    line-height: 17px;
  }

</style>
<div class="row">
	<div class="col-md-12">
	        <div class="box-body">
	          	<div style='overflow:hidden;height:100%;width:100%;'>
            		<div id='gmap_reserva'></div>
          	  	</div>
          	   	<div id="divLeyenda">
				    <table width="100%">
				        <tr height="55">
				            <td align="center">
				            	<a href="javascript:CambiarEstadoVisual(0);">
				                	<img src="includes/img/carrito-negro.png" width="35" height="35" class="img-responsive"></a>
				            </td>
				            <td align="right">:</td>
				            <td style="color:black" align="center">INACTIVO</td>
				        </tr>
				        <tr height="55">
				            <td align="center">
				            	<a href="javascript:CambiarEstadoVisual(2);">
				                	<img src="includes/img/carrito-naranja.png" width="35" height="35" class="img-responsive"></a>
				            </td>
				            <td align="right">:</td>
				            <td style="color:#DE8500" align="center">OCUPADO</td>
				        </tr>
				        <tr height="55">
				            <td align="center">
				            	<a href="javascript:CambiarEstadoVisual(1);">
				                	<img src="includes/img/carrito-verde.png" width="35" height="35" class="img-responsive"></a>
				            </td>
				            <td align="right">:</td>
				            <td style="color:green" align="center">DISPONIBLE</td>
				        </tr>
				        <tr height="55">
				          <td height="25" colspan="3" align="center">
				          	<a href="javascript:CambiarEstadoVisual('all');">[ VER TODOS ]</a>
				          </td>
				        </tr>
			            <tr height="25" >
				        	<td height="25" colspan="3" align="center">
				            	<input class="form-control" placeholder="Nro. Placa" id="nroplaca">
				            	<br>
				           	 	<buton class="btn btn-primary form-control" onclick="buscar(document.getElementById('nroplaca').value)">Buscar</buton>
				          	</td>
				        </tr>
				    </table>
				</div>
	        </div><!-- /.box-body -->
	</div>
</div>
<script src="<?=IP_SOCKET?>socket.io/socket.io.js"></script>
<script type='text/javascript'>

var map;
var idsConductor = [];
var marker = [];
var markers = new Array();
var datos = [];
var infowindow = [];
var socket = null;
var conductors;
var logoestado = '';

$(function() {

	init_mapa();

	socket = io.connect(URL_CONTROLLER_SOCKET);
	socket.emit('dataMapConductorsWeball',{estado: 'all'});
	socket.on('listaConductorsall',function(result){

		conductors = result;
		console.log(conductors);
		 for (var i = 0; i < result.length; i++) {
		 		console.log('Total de conductores inicial : '+result.length);
			 	datos = {
	        			Nombre:result[i].conductor_nombre, 
	        			Apellido:result[i].conductor_apellido, 
	        			Foto:result[i].conductor_foto, 
	        			Estado:result[i].conductor_estado,
	        			Celular:result[i].conductor_celular,
	        			Placa:result[i].unidad_placa,
	        			Marca:result[i].unidad_marca,
	        			Servicio:result[i].tiposervicio_nombre,
	        			Socket:result[i].conductor_socket
	        			};
	        	idsConductor.push(result[i].conductor_id);
	        	addMarker(result[i].conductor_id,result[i].conductor_lat,result[i].conductor_lng,map,datos);		
		 }
	});

	socket.on('refreshCoordinates', function(data){
		//console.log(data);
		if(data.campo_new == 1){
			if(marker[data.idConductor]){
				console.log('Conductor existente :'+ data.idConductor + ' Estado New :'+ data.campo_new + ' Estados de Servicio : '+ data.stateConductor);
				if(data.socket_conductor != null){
					switch(data.stateConductor) {
					    case 1:
					    	marker[data.idConductor].setIcon('includes/img/carrito-verde.png');
					        break;
					    case 2:
					    	marker[data.idConductor].setIcon('includes/img/carrito-naranja.png');
					        break;
					}
				}else{
					marker[data.idConductor].setIcon('includes/img/carrito-negro.png');

				}
					var newLatLng = new google.maps.LatLng(data.latitud, data.longitud);
					marker[data.idConductor].setPosition(newLatLng);
			}
			else
			{	
				console.log('Conductor nuevo :'+ data.idConductor);

				datos = {
	        			Nombre:data.nombre, 
	        			Apellido:data.apellido, 
	        			Foto:data.foto, 
	        			Estado:data.stateConductor,
	        			Celular:data.celular,
	        			Placa:data.placa,
	        			Marca:data.marca,
	        			Servicio:data.servicio,
	        			Socket:data.conductor_socket
		        		};

		       	idsConductor.push(data.idConductor);
				addMarker(data.idConductor,data.latitud,data.longitud,map,datos);
			}
		}
	});

});

function init_mapa() {
	
	//var bangalore = {lat: -12.046374, lng: -77.0427934};
    //var bangalore = {lat: <?=$result[0]['usuario_latitud'];?>, lng: <?=$result[0]['usuario_longitud']?>};
    var bangalore = {lat: <?=$result[0]['usuario_latitud'];?>, lng: <?=$result[0]['usuario_longitud']?>};
    map = new google.maps.Map(document.getElementById('gmap_reserva'), {
      zoom: 16,
      center: bangalore,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
}


function addMarker(idConductor, lat, lng, map, datos) {

    	var latitud = parseFloat(lat);
        var longitud = parseFloat(lng);
        //console.log('Nombre => '+datos.Nombre+' '+datos.Apellido+' socket => '+datos.Socket+' estado => '+datos.Estado);
        if(datos.Socket != null)
        {
        	switch(datos.Estado) {
			    case 1:
			        logoestado = 'includes/img/carrito-verde.png';
			        break;
			    case 2:
			        logoestado = 'includes/img/carrito-naranja.png';
			        break;
			}
        }	
        else
        {
        	logoestado = 'includes/img/carrito-negro.png';
        }

	    image = new google.maps.MarkerImage(logoestado,
	      new google.maps.Size(50, 50),
	      new google.maps.Point(1,1),
	      new google.maps.Point(1, 1)
		);

        marker[idConductor] = new google.maps.Marker({
	   	  icon : image,
	      position: {lat: latitud, lng: longitud},
	      map: map
	    });

	    markers.push(marker[idConductor]);
        marker[idConductor].setMap(map);

         var contentString = '<div id="content">'+
						      '<div id="siteNotice">'+
						      '</div>'+
						      '<p class="firstHeading"><b>'+datos.Nombre+' '+datos.Apellido+'</b></p>'+
						      '<div id="bodyContent">'+
						      '<div style="width:100%;display: inline-block;">'+
						      '<div style="float:left;margin-right: 15px;">'+
						      '<img src="BL/'+datos.Foto+'" class="img-thumbnail" width="100" height="100">'+
						      '</div>'+
						      '<div style="float:right">'+
						      '<lalel><b>Nro.Placa : </b>'+datos.Placa+'</label><br>'+
						      '<lalel><b>Marca : </b>'+datos.Marca+'</label><br>'+
						      '<lalel><b>Telefono : </b>'+datos.Celular+'</label><br>'+
						      '<lalel><b>Servicio : </b>'+decodeURIComponent(escape(datos.Servicio))+'</label>'+
						      '</div>'+	
						      '</div>'+
						      '</div>'+
						      '</div>';
		infowindow[idConductor] = new google.maps.InfoWindow({
		    content: contentString
		});

        marker[idConductor].addListener('click', function() {
    		infowindow[idConductor].open(map, marker[idConductor]);
  	 	});
}

function CambiarEstadoVisual(estado){
	$.each(idsConductor,function(index,obj)
    {	
    	marker[obj].setMap(null);
    });
	console.log(estado);
	socket.emit('dataMapConductorsWeball',{estado: estado});
	
}

function buscar(placa){
	console.info('placa',placa);
    nplaca = placa;
    mapOptions = {
        zoom : 14,
        center: new google.maps.LatLng(<?=$result[0]['usuario_latitud'];?>, <?=$result[0]['usuario_longitud']?>)
    }
    map = new google.maps.Map(document.getElementById('gmap_reserva'), mapOptions);

    socket.emit('dataMapConductorsWeball',{estado: 'all'});
    socket.on('listaConductorsall', function(result){
        if(typeof result != 'undefined'){
            for(var j in result){
                if(result[j].unidad_placa == placa){
                	if(marker[result[j].conductor_id]){

                		var lat = parseFloat(result[j].conductor_lat);
                		var lng = parseFloat(result[j].conductor_lng);
                		var conductor_id = result[j].conductor_id;
                		google.maps.event.trigger(marker[conductor_id], 'click');

                		moveToLocation(lat,lng);
                		$("#nroplaca").val('');
                	}
                }
            }
        }
    });
}

function moveToLocation(lat, lng){
    var center = new google.maps.LatLng(lat, lng);
    map.panTo(center);
    map.setZoom(14); 
}

</script>