<?php
include'seguridad.php';
require_once'DAL/usuarioDAO.php';

//$usuarioDAO = new usuarioDAO();
//$result = $usuarioDAO->listaNuevaUsuario($_SESSION['rol_id']);

$rolDAO = new rolDAO();
$rol = $rolDAO->listrol();

?>

<style type="text/css">
td > a{ margin-left: 10px; } 
</style>
<div class="row" ng-app="myapp" ng-controller="UsuarioController">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Agregar Usuario</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" method="post" action="../BL/usuarioController.php">
        <div class="box-body">
        <div class="form-group">
            <label for="correo">Nombres y Apellidos</label>
            <input type="text" class="form-control" name="nombre" placeholder="Nombres y apellidos del usuario" required>
            <input type="hidden" name="hidden_usuario_insert">
          </div>
         <div class="form-group">
            <label for="correo">Correo de Usuario</label>
            <input type="email" class="form-control" name="correo" placeholder="Correo de Usuario" required>
          </div>
          <div class="form-group">
            <label for="correo">Password de Usuario</label>
            <input type="password" class="form-control" name="pass1" placeholder="Password de usuario" required>
          </div>
          <div class="form-group">
            <label for="correo">Repita Password</label>
            <input type="password" class="form-control" name="pass2" placeholder="Repetir Password" required>
          </div>
          <div class="form-group">
            <label for="correo">Latitud</label>
            <input type="text" class="form-control" name="latitud" placeholder="Latitud de trabajo" required>
          </div>
          <div class="form-group">
            <label for="correo">Longitud</label>
            <input type="text" class="form-control" name="longitud" placeholder="Longitud de trabajo" required>
          </div>
          <div class="form-group">
            <label for="destino">Status</label>
            <select class="form-control" name="status">
              <option value="1">Activo</option>
              <option value="0">Bloqueado</option>
            </select>
          </div>
          <div class="form-group">
            <label for="destino">Permisos</label>
            <select class="form-control" name="permisos" id="perm">
                  <option value="0" selected="disabled">Seleccione</option>
                  <?php foreach($rol as $i => $roles ){ ?>
                      <option value="<?=$roles['rol_id']?>"><?=$roles['rol_nombre']?></option>
                  <?php } ?>
            </select>
          </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar Usuario</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
    <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado de Usuarios</h3>
      </div><!-- /.box-header -->
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Correo</th>
              <th>Rol</th>
              <th>Status</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat = "usuario in usuarios |orderBy:columnToOrder:reverse">
              <td>{{usuario.usuario_nombre}}</td>
              <td>{{usuario.usuario_mail}}</td>
              <td>{{usuario.rol_nombre}}</td>
              <!--td>{{usuario.usuario_status}}</td-->
              <td ng-if=usuario.rol_id!=1>
                <div ng-if="usuario.usuario_status==1">
                    <switch name="enabled" ng-model="usuario.enabled" ng-change="changeEstado(usuario.usuario_id,usuario.enabled)" on="on" off="off"></switch>
                </div> 
                <div ng-if="usuario.usuario_status==0">
                    <switch name="enabled" ng-model="usuario.enabled" ng-change="changeEstado(usuario.usuario_id,usuario.enabled)" on="on" off="off"></switch>
                </div>  
              </td>
              <td ng-if=usuario.rol_id==1></td>
              <td style="width:10%"><a class="btn btn-warning btn-circle" data-toggle="modal" data-target="#myModal" ng-click="Editar(usuario.usuario_id)"><i class="glyphicon glyphicon-edit"></i></a><a ng-click="deleteC(usuario.usuario_id)" class="btn btn-danger btn-circle" ><i class="fa fa-close" aria-hidden="true"></i></a></td>
            </tr>
          </tbody>
        </table>
        <div class="row">
            <div class="col-sm-5" style="margin-left:15px;">
                <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                <!-- Showing 1 to 10 of 848 entries class="deleteconfi" href="javascript:void(0);" -->
            </div>
            <div class="col-sm-6" style="text-align: right;">
                <div class="dataTables_paginate paging_simple_numbers">
                  <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Editar Usuarios</h4>
      </div>
      <form role="form" method="post" action="../BL/usuarioController.php">
      <div class="modal-body">
        <div class="box-body">
        <div class="form-group">
            <label for="correo">Nombres y Apellidos</label>
            <input type="text" class="form-control" name="nombre1" id="nombre1" placeholder="Nombres y apellidos del usuario" required>
            <input type="hidden" name="id" id="idusuario">
            <input type="hidden" name="hidden_usuario_update">
          </div>
         <div class="form-group">
            <label for="correo">Correo de Usuario</label>
            <input type="email" class="form-control" name="correo1" id="correo1" placeholder="Correo de Usuario" required>
          </div>
          <div class="form-group">
            <label for="correo">Password de Usuario</label>
            <input type="password" class="form-control" name="pass11" id="pass11" placeholder="Password de usuario" required>
          </div>
          <div class="form-group">
            <label for="correo">Repita Password</label>
            <input type="password" class="form-control" name="pass22" id="pass22" placeholder="Repetir Password" required>
          </div>
          <div class="form-group">
            <label for="correo">Latitud</label>
            <input type="text" class="form-control" name="latitud1" id="latitud1" placeholder="Latitud de trabajo" required>
          </div>
          <div class="form-group">
            <label for="correo">Longitud</label>
            <input type="text" class="form-control" name="longitud1" id="longitud1" placeholder="Longitud de trabajo" required>
          </div>
          <div class="form-group">
            <label for="destino">Status</label>
            <select class="form-control" name="status1" id="status1">
              <option value="1">Activo</option>
              <option value="0">Bloqueado</option>
            </select>
          </div>
          <div class="form-group">
            <label for="destino">Permisos</label>
             <select class="form-control" name="permisos1" id="permisos1">
                  <option value="0" selected="disabled">Seleccione</option>
                  <?php foreach($rol as $i => $roles ){ ?>
                      <option value="<?=$roles['rol_id']?>"><?=$roles['rol_nombre']?></option>
                  <?php } ?>
            </select>
          </div>

        </div><!-- /.box-body -->
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
      </div>
    </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
<script>

$(document).ready(function(){

    $('input[name="my-checkbox"]').bootstrapSwitch();

    if(<?=$_SESSION['rol_id'];?> == 1){
      $("#perm option[value='<?=$_SESSION['rol_id'];?>']").remove();
    }else{
      $("#perm option[value='1']").remove();
      $("#perm option[value='<?=$_SESSION['rol_id'];?>']").remove();
    }
});

</script>

