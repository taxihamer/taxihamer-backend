<?php
include'seguridad.php';
?>
<style>
  #mapMov{
    height:600px;
    width:100%;
  }
  #mapMov img{
    max-width:none!important;
    background:none!important;
  }
  #modal .modal-content { display: inline-block; }
  .global-12{padding-bottom: inherit;border: inherit;margin-left: 15px;}
  .warning{color: red;}
  .ok{color: green;}
  th{text-align: center;}
  .nullOld{text-align: center;color: rebeccapurple;}
  .al{text-align:center;}
</style>
<div class="row" ng-app="myappPuntos" ng-controller="reportespuntosController">
  <div class="col-md-12">
    <div class="box box-primary" >
          <div>
            <section id="main-content" class="animated fadeInRight">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h3 class="panel-title">REPORTE DE PUNTOS POR USUARIO</h3>
                          </div>
                          <div class="panel-body">
                          <!-- TOP OF TABLE: shows page size and search box -->
                            <div class="dataTables_wrapper form-inline" role="grid">
                              <div class="row">
                                  <form id="formuploadajax" class="form-horizontal" method="post" role="form">
                                    <div class="show-grid">
                                     <div class="col-md-12 global-12">
                                        <div class="row">
                                          <div class="col-md-3">
                                            <div class="form-group">
                                              <label for="noperacion">Tipo de Usuario</label>
                                              <select ng-model="tipoUsuario_a" class="form-control" name="tipoUsuario_a" ng-change="searchTextChangedComb()" ng-options="c.key as c.value for c in tipoUsuario" required></select>
                                            </div>
                                          </div>
                                          <!--div class="col-md-3">
                                            <div class="form-group">
                                              <button type="submit" class="btn btn-danger" name="search" ng-click="Searchddd()">Buscar</button>
                                            </div>
                                          </div-->
                                        </div>
                                      </div>
                                    </div>
                                  </form>
                                  <div class="col-sm-12">
                                      <div id="example_filter" class="dataTables_filter">
                                          <label>Buscar :<input type="search" class="form-control input-sm" aria-controls="example" ng-model="searchText" ng-change="searchTextChanged()" placeholder="Nombre de conductor"></label>
                                      </div>
                                  </div>
                              </div>                        
                              <!--loading></loading-->
                              <div style="overflow-x:auto;">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                  <thead>
                                      <tr>
                                          <th>Puesto</th>
                                          <th>Nombre</th>
                                          <th>Correo</th>
                                          <th>Celular</th>
                                          <th>Puntos Total</th>
                                      </tr>
                                  </thead>

                                  <tbody>
                                      <tr ng-repeat = "reporteConductor in reportesConductores | orderBy:columnToOrder:reverse ">
                                          <td class="al">{{$index + 1}}º</td>
                                          <td class="al">{{reporteConductor.Nombre}}</td>
                                          <td class="al">{{reporteConductor.email}}</td>
                                          <td class="al">{{reporteConductor.celular}}</td>
                                          <td class="al">{{reporteConductor.puntos}}</td>
                                      </tr>
                                      <tr class="nullOld" id="nullOld">
                                        <td colspan="12">No hay datos a mostrar</td>
                                      </tr>
                                  </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-sm-6">
                                      <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_simple_numbers">
                                          <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                                        </div>
                                    </div>
                                </div>
                              </div>                      
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    </div>
</div>

