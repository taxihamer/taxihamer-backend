<link rel="stylesheet" href="dist/css/angular-validate.css">
<style>
  .warning{color: red;}
  .ok{color: green;}
  .pend{color: blue;}
  .rechazar{padding: 4px 6px;
    margin: -53px 0px -26px 10px;}
</style>
<div class="row" ng-app="myapp" ng-controller="configreservaController">
        <div class="col-md-3">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Config de Reservas</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
              <form role="form" name="configReserva" novalidate>
                <div class="box-body">
                    <div class="form-group">
                        <lavel for="hanti">Hora Anticipacion</lavel>
                        <input type="number" ng-model="visitorConfig.horareserva" class="configReserva form-control" name="hanti" placeholder="Hora de reservas"  ng-required="true"/>
                         <span class="valid" ng-show="configReserva.horareserva.$valid">&#10004;</span>
                    </div>
                    <div class="form-group">
                        <lavel for="hmovil">Minutos Alerta Movil</lavel>
                        <input type="number" ng-model="visitorConfig.horalertmovil" class="configReserva form-control" name="hmovil" placeholder="Minutos de alerta movil" required/>
                         <span class="valid" ng-show="configReserva.horalertmovil.$valid">&#10004;</span>
                    </div>
                    <div class="form-group">
                        <lavel for="hpanel">Hora Alerta Panel</lavel>
                        <input type="number" ng-model="visitorConfig.horalertapanel" class="configReserva form-control" name="hpanel" placeholder="Hora de alerta panel" required/>
                        <span class="valid" ng-show="configReserva.horalertapanel.$valid">&#10004;</span>
                    </div>
                     <div class="form-group">
                        <lavel for="hpanel">Minutos de Bloqueo de Servicio/Reserva</lavel>
                        <select ng-model="horabloqueoservicio" class="configReserva form-control" name="select_defaultAdd" ng-options="c.key as c.value for c in bloqueoMin" required></select>
                        <span class="valid" ng-show="configReserva.horabloqueoservicio.$valid">&#10004;</span>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                  <button ng-if="contador<10" type="submit" class="submitbtn btn btn-primary" ng-click="addConfigreserva(configReserva)" ng-disabled="configReserva.$invalid">Guardar</button>
                </div>
              </form>
            </div><!-- /.box -->
        </div>
        <div class="col-md-9">
             <?php
                if(isset($_REQUEST['status'])){
                  if($_REQUEST['status']=="true"){
                    echo '<div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              Operación Realizada con Éxito</div>';
                  }elseif ($_REQUEST['status']=="bool") {
                    echo '<div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              Ups, seleccione un dia de semana </div>';
                  }else{
                    echo '<div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              Ups, ocurrio un error inesperado.</div>';
                  }
                }
            ?>
            <div class="box box-primary">
                <div class="alert alert-danger" id="msgErrorConfig" style="display:none">
                    <button type="button" class="close" ng-click="CloseConfig()">&times;</button>
                    <strong>¡Cuidado!</strong> {{messageErrorConfig}}
                </div>
                <div class="alert alert-success" id="msgSuccessConfig" style="display:none">
                    <button type="button" class="close" ng-click="CloseConfigSucces()">&times;</button>
                    <strong>¡Éxito!</strong> {{messageSuccessConfig}}
                </div>
                <div class="box-header with-border">
                    <h3 class="box-title">Listado de Configuracion Reserva</h3>
                </div><!-- /.box-header -->
                <div>
                    <table datatable="ng" dt-options="dtOptions" class="table table-hover" >
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Horas Reserva</th>
                                <th>Horas Alerta Movil</th>
                                <th>Horas Alerta Panel</th>
                                <th>Estado</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="configreserva in configreservas">
                                <td>{{$index + 1}}</td>
                                <td>{{configreserva.configreserva_horas}}</td>
                                <td>{{configreserva.configreserva_alerta}}</td>
                                <td>{{configreserva.configreserva_panel}}</td>
                                <td>
                                    <div ng-if="configreserva.configreserva_status==1">
                                         <switch name="enabled" ng-model="configreserva.enabled" ng-change="changeCallbackConfig(configreserva.configreserva_id, $index + 1)" on="on" off="off"></switch>
                                    </div> 
                                    <div ng-if="configreserva.configreserva_status==0">
                                           <switch name="enabled" ng-model="configreserva.enabled" ng-change="changeCallbackConfig(configreserva.configreserva_id, $index + 1)" on="on" off="off"></switch>
                                    </div>  
                                </td>
                                <td>
                                    <button ng-if="1==configreserva.configreserva_status" class="btn btn-warning" ng-click="editConfigReserva($index +1,configreserva.configreserva_id,configreserva.configreserva_horas,configreserva.configreserva_alerta,configreserva.configreserva_panel,configreserva.configreserva_bloqueo )">
                                    <i class="fa fa-edit"></i>
                                    </button>
                                    <span ng-if="0==configreserva.configreserva_status" ng-class="{warning: (configreserva.configreserva_status == 0)}">{{configreserva.detalle}}</span>
                                    <!--a href="#" ng-if="0==configreserva.configreserva_status" class="btn btn-danger" ng-click="deleteConfigReserva(configreserva.configreserva_id)">
                                    <i class="fa fa-trash-o"></i></a-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            <!-- Modal in Angular -->
            <modal title="Editar Conductores" visible="showModal" style="display:none;" id="modal">
                <form role="form" name="configReservaEdit" novalidate>
                    <input type="hidden" ng-model="idconfig" id="idconfig">
                    <input type="hidden" ng-model="indexEdit">
                    <div class="form-group">
                        <lavel for="hantiEdit">Hora Anticipacion</lavel>
                        <input type="number" id="hantiEdit" ng-model="visitorConfigEdit.horareservaEdit" class="configReservaEdit form-control" name="hantiEdit" placeholder="Hora de reservas"  required/>
                         <span class="valid" ng-show="configReservaEdit.horareservaEdit.$valid">&#10004;</span>
                    </div>
                    <div class="form-group">
                        <lavel for="hmovilEdit">Minutos Alerta Movil</lavel>
                        <input type="number" id="hmovilEdit" ng-model="visitorConfigEdit.horalertmovilEdit" class="configReservaEdit form-control" name="hmovilEdit" placeholder="Minutos de alerta movil" required/>
                         <span class="valid" ng-show="configReservaEdit.horalertmovilEdit.$valid">&#10004;</span>
                    </div>
                    <div class="form-group">
                        <lavel for="hpanelEdit">Hora Alerta Panel</lavel>
                        <input type="number" id="hpanelEdit" ng-model="visitorConfigEdit.horalertapanelEdit" class="configReservaEdit form-control" name="hpanelEdit" placeholder="Hora de alerta panel" required/>
                        <span class="valid" ng-show="configReservaEdit.horalertapanelEdit.$valid">&#10004;</span>
                    </div>
                    <div class="form-group">
                      <lavel for="hbloedit">Minutos de Bloqueo de Servicio/Reserva</lavel>
                        <select ng-model="horabloqueoservicioEdit" class="configReservaEdit form-control" id="hbloedit" name="hbloedit" ng-options="c.key as c.value for c in bloqueoMin" required></select>
                        <span class="valid" ng-show="configReservaEdit.horabloqueoservicioEdit.$valid">&#10004;</span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="submitbtn btn btn-primary" ng-click="UpdateConfigreserva(configReservaEdit)" ng-disabled="configReservaEdit.$invalid">Guardar</button>
                    </div>
                </form>
            </modal>
</div>
