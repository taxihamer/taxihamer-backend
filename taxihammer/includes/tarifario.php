<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include'seguridad.php';
require_once'DAL/tipoServicioDAO.php';
require_once'DAL/empresaDAO.php';
require_once'DAL/tarifarioDAO.php';
require_once'DAL/lugaresDAO.php';
$tipoServicioDAO = new tipoServicioDAO();
$result_tipo = $tipoServicioDAO->lista();
$empresaDAO = new empresaDAO();
$result_empresa = $empresaDAO->listempresa();

$tarifarioDAO = new tarifarioDAO();
$start = 0;
$per_page_inic = 400;
$result_tarifario = $tarifarioDAO->listaPaginacion($start,$per_page_inic);
$lista = $tarifarioDAO->lista();
$paginacion = ceil(count($lista)/$per_page_inic);

$lugaresDAO = new lugaresDAO();
$result_lugares = $lugaresDAO->lista();
?>
<style type="text/css">
  #page{
    text-align: center;
    padding-bottom: 20px;
  }
  ul#pagination {
    list-style: none;
  }

  ul#pagination li {
    display: inline-block;
    padding: 5px 10px;
    border: 1px solid #999;
    border-radius: 5px;
    margin: 3px;
    cursor: pointer;
  }
</style>
<div class="row">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Agregar nueva Tarifa</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" method="post" action="BL/tarifarioController.php">
        <div class="box-body">
          
          <div class="form-group">
            <label for="tipocilente">Cliente</label>
            <input type="hidden" name="hidden_tarifario_insert" value="1">
            <select class="form-control" name="cliente_id">
              <option value="0">Particular</option>
            <?php
            foreach($result_empresa AS $res_empresa){
              ?>

              <option value="<?php echo $res_empresa['empresa_id']; ?>"><?php echo $res_empresa['empresa_razonsocial']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
          <div class="form-group">
            <label for="sercicio">Tipo Servicio</label>
            <select class="form-control" name="tiposervicio_id">
            <?php
            foreach($result_tipo AS $res_tipo){
              ?>
              <option value="<?php echo $res_tipo['tiposervicio_id']; ?>"><?php echo $res_tipo['tiposervicio_nombre']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
          <div class="form-group">
            <label for="origen">Origen</label>
            <select class="form-control" name="origen_id">
            <?php
            foreach($result_lugares AS $res_lugar){
              ?>
              <option value="<?php echo $res_lugar['lugares_id']; ?>"><?php echo $res_lugar['lugar_descripcion']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
          
          <div class="form-group">
            <label for="destino">Destino</label>
            <select class="form-control" name="destino_id">
            <?php
            foreach($result_lugares AS $res_lugar){
              ?>
              <option value="<?php echo $res_lugar['lugares_id']; ?>"><?php echo $res_lugar['lugar_descripcion']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>

          <div class="form-group">
            <label for="monto">Monto</label>
            <input type="number" class="form-control" name="monto" placeholder="Monto" required>
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
  <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado Tarifas</h3>
      </div><!-- /.box-header -->
      <div class="col-md-12" style="margin-left:20px;">
          <label>Exportar : </label>
          <form role="form" action="../BL/exportarController.php" enctype="multipart/form-data" method="post" style="width: 30%;
            display: inline-block;">
            <input type="hidden" name="hidden_exportar" value="1">
            <input type="hidden" name="exportar" value="4">
            <button type="submit" class="btn btn-primary btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
          </form>
      </div>
      <br>
      <div class="col-md-12">
        
          <div class="col-md-2">
            <label for="origen">Origen</label>
            <input name="hidde_tarifario_buscar" value="1" type="hidden">
            <select class="form-control" name="origen" id="origen">
            <option value="todos">Todos</option>
            <?php
            foreach($result_lugares AS $res_lugar){
              ?>
              <option value="<?php echo $res_lugar['lugares_id']; ?>"><?php echo $res_lugar['lugar_descripcion']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
          <div class="col-md-2">
            <label for="destino">Destino</label>
            <select class="form-control" name="destino" id="destino">
            <option value="todos">Todos</option>
            <?php
            foreach($result_lugares AS $res_lugar){
              ?>
              <option value="<?php echo $res_lugar['lugares_id']; ?>"><?php echo $res_lugar['lugar_descripcion']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
          <div class="col-md-3">
            <label for="empresa">Empresa</label>
            <select class="form-control" name="empresa" id="empresa">
            <option value="todos">Todas</option>
            <option value="0">Particular</option>
            <?php
            foreach($result_empresa AS $res_empresa){
              ?>
              <option value="<?php echo $res_empresa['empresa_id']; ?>"><?php echo $res_empresa['empresa_razonsocial']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
          <div class="col-md-3">
            <label for="empresa">Tipo de Servicio</label>
            <select class="form-control" name="servicio" id="servicio">
            <option value="todos">Todos</option>
            <?php
            foreach($result_tipo AS $res_tipo){
              ?>
              <option value="<?php echo $res_tipo['tiposervicio_id']; ?>"><?php echo $res_tipo['tiposervicio_nombre']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
          <div class="col-md-2">
          <label for="submit">&nbsp;</label>
          <button class="btn btn-primary form-control" onclick="Buscar()">Buscar</button>
          </div>
        
      </div>
      <br>
        <table class="table table-striped">
          <thead>
            <tr>
              <th></th>
              <th>Cliente</th>
              <th>Origen</th>
              <th>Destino</th>
              <th>Tipo Servicio</th>
              <th>Monto</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody id="response">
            <?php
          foreach($result_tarifario as $res){
          
          ?>
            <tr>
              <td></td>
              <td><?php echo $res['empresa_razonsocial']; ?></td>
              <td>
              <?php 
              foreach($result_lugares as $resu){
                if($res['origen_id'] == $resu['lugares_id']){
                  echo $resu['lugar_descripcion'];
                }
              }
              ?></td>
              <td>
              <?php 
              foreach($result_lugares as $resu){
                if($res['destino_id'] == $resu['lugares_id']){
                  echo $resu['lugar_descripcion'];
                }
              }
              ?>
              </td>
              <td><?php echo $res['tiposervicio_nombre']; ?></td>
              <td><?php echo number_format($res['tarifario_monto'], 2, '.', ','); ?></td>
              
              <td><button class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="llenar('<?php echo $res['tarifario_id']; ?>', '<?php echo $res['cliente_id']; ?>', '<?php echo $res['origen_id']; ?>','<?php echo $res['destino_id']; ?>','<?php echo $res['tiposervicio_id']; ?>','<?php echo $res['tarifario_monto']; ?>')"><i class="glyphicon glyphicon-edit"></i></button>
                  <a href="../BL/tarifarioController.php?hidden_delete_tarifario=1&id=<?php echo $res['tarifario_id']; ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>

              </td>
            </tr>
          <?php
          }
          ?>
          </tbody>
        </table>
        <div id="page">
          <input type="hidden" name="filtropage" id="filtropage">
          <ul id="pagination">
                <?php for ($i=1; $i <=$paginacion; $i++) { 
                    echo '<li id="'.$i.'">'.$i.'</li>';
                }?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
  $('#pagination li:first').css({'color':'#FF9900'});

  $('#pagination li').click(function(){

      origen = document.getElementById('origen').value;
      destino = document.getElementById('destino').value;
      cliente = document.getElementById('empresa').value;
      servicio = document.getElementById('servicio').value;

      $('#pagination li').css({'color':'#000'});
      $(this).css({'color':'#FF9900'});
      var pageNum = this.id;
      $.ajax({
          url: 'BL/tarifarioController.php',
          data: {
              hidden_tarifario_paginacion: 1,
              page: pageNum,
              origen: origen,
              destino: destino,
              cliente: cliente,
              servicio: servicio
          },
          cache: false,
          method: 'post'
      }).done(function (data) {
          $('#response').html(data);
      });
  });

})



function llenar(id,cliente,origen,destino,tiposervicio,monto){

  $('#tarifario_id').val(id);
  $('#cliente_id1').val(cliente);
  $('#tiposervicio_id1').val(tiposervicio);
  $('#origen_id1').val(origen);
  $('#destino_id1').val(destino);
  $('#monto1').val(monto);

}
function Buscar() {

  origen = document.getElementById('origen').value;
  destino = document.getElementById('destino').value;
  cliente = document.getElementById('empresa').value;
  servicio = document.getElementById('servicio').value;

    $.ajax({
        url: 'BL/tarifarioController.php',
        data: {
            hidden_tarifario_buscar: 1,
            origen: origen,
            destino: destino,
            cliente: cliente,
            servicio: servicio
        },
        cache: false,
        method: 'post'
    }).done(function (data) {
        var contador;
        $('#response').html(data);
        PaginacionFiltro(origen,destino,cliente,servicio);

       setTimeout(function(){
          contador = $("#filtropage").val();

          if(contador > 0){
            var htmlli = '';
            for (var i = 1; i <=contador; i++) {
              htmlli += '<li id="'+i+'" onclick="page('+i+')">'+i+'</li>';
            }

            $('#pagination').html(htmlli);
            $('#pagination li:first').css({'color':'#FF9900'});
          }else{
             $('#pagination').html('');
          }
        }, 200);
       
    });

}

function page(id) {

  var origen = document.getElementById('origen').value;
  var destino = document.getElementById('destino').value;
  var cliente = document.getElementById('empresa').value;
  var servicio = document.getElementById('servicio').value;

  $('#pagination li').css({'color':'#000'});
  $("li#"+id).css({'color':'#FF9900'});
  var pageNum = id;
  $.ajax({
      url: 'BL/tarifarioController.php',
      data: {
          hidden_tarifario_paginacion: 1,
          page: pageNum,
          origen:origen,
          destino:destino,
          cliente:cliente,
          servicio:servicio
      },
      cache: false,
      method: 'post'
  }).done(function (data) {
      $('#response').html(data);
  });
}

function PaginacionFiltro(origen,destino,cliente,servicio){

    $.ajax({
        url: 'BL/tarifarioController.php',
        data: {
            hidden_tarifario_buscar_paginacion: 1,
            origen: origen,
            destino: destino,
            cliente: cliente,
            servicio: servicio
        },
        cache: false,
        method: 'post'
    }).done(function (data) {
        $('#filtropage').val(data);
    });
}
</script>
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Editar Tarifario</h4>
      </div>
      <form role="form" method="post" action="../BL/tarifarioController.php">
      <div class="modal-body">
        <div class="box-body">
        <div class="form-group">
            <label for="tipocilente">Cliente</label>
            <input type="hidden" name="tarifario_id" id="tarifario_id">
            <input type="hidden" name="hidden_tarifario_update" value="1">
            <select class="form-control" name="cliente_id1" id="cliente_id1">
            <option value="0">Particular</option>
            <?php
            foreach($result_empresa AS $res_empresa){
              ?>
              <option value="<?php echo $res_empresa['empresa_id']; ?>"><?php echo $res_empresa['empresa_razonsocial']; ?></option>;
              <?php
              
            }
            ?>
            </select>
          </div>
          <div class="form-group">
            <label for="sercicio">Tipo Servicio</label>
            <select class="form-control" name="tiposervicio_id1" id="tiposervicio_id1">
            <?php
            foreach($result_tipo AS $res_tipo){
              ?>
              <option value="<?php echo $res_tipo['tiposervicio_id']; ?>"><?php echo $res_tipo['tiposervicio_nombre']; ?></option>;
              <?php
              
            }
            ?>
            </select>
          </div>
          <div class="form-group">
            <label for="origen">Origen</label>
            <select class="form-control" name="origen_id1" id="origen_id1">
            <?php
            foreach($result_lugares AS $res_lugar){
              ?>
              <option value="<?php echo $res_lugar['lugares_id']; ?>"><?php echo $res_lugar['lugar_descripcion']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
          
          <div class="form-group">
            <label for="destino">Destino</label>
            <select class="form-control" name="destino_id1" id="destino_id1">
            <?php
            foreach($result_lugares AS $res_lugar){
              ?>
              <option value="<?php echo $res_lugar['lugares_id']; ?>"><?php echo $res_lugar['lugar_descripcion']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>

          <div class="form-group">
            <label for="monto">Monto</label>
            <input type="number" class="form-control" name="monto1" placeholder="Monto" id="monto1" required>
          </div>
          
        </div><!-- /.box-body -->
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
      </div>
    </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->