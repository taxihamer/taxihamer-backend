<?php
  include'seguridad.php';
  require_once'DAL/usuarioDAO.php';
  require_once'DAL/lugaresDAO.php';

$data['rol_id'] = $_SESSION['rol_id'];
$data['mail'] = $_SESSION['mail'];
  $usuarioDAO = new usuarioDAO();
  $result = $usuarioDAO->listacoordenadas($data);

  $lugaresDAO = new lugaresDAO();
  $listazona = $lugaresDAO->lista();

?>
<style>

	#gmap_canvas{
		height:600px;
		width:100%;
	}
	#gmap_canvas img{
		max-width:none!important;
		background:none!important;
	}
	#coordspoligono , #centerpoligono{
		display: none;
	}

</style>
<div class="row">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Creacion de Zonas</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
        <input type="hidden" name="hidden_zona_insert">
        <div class="box-body">
          <div class="form-group">
            <label for="lugar">Lugar</label>
             <input id="address" type="textbox" class="form-control" name="address" placeholder="Ingresar un Lugar" required>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
        	<input id="submit" type="button" class="btn btn-primary" value="Crear">
        </div>
    </div><!-- /.box -->
  </div>
  	<div class="col-md-9">
	      <?php
	    if(isset($_REQUEST['status'])){
	      if($_REQUEST['status']=="true"){
	        echo '<div class="alert alert-success alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  Operación Realizada con Éxito</div>';
	      }else{
	        echo '<div class="alert alert-danger alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  Ups, ocurrio un error inesperado.</div>';
	      }
	    }
	    ?>
	    <div class="box box-primary">
	      <div class="box-header with-border">
	        <h3 class="box-title">Lugar : <span id="desclugar"></span></h3>
	        <input type="hidden" name="lat" id="lat">
	        <input type="hidden" name="lon" id="lon">
	      </div>
	      <!--Aqui va el map-->
	      	<div style='overflow:hidden;height:600px;width:100%;'>
				    <div id='gmap_canvas'></div>
			   </div>
	    </div>
	    <div id="coordspoligono"></div>
	    <div id="centerpoligono"></div>
	    <button class="btn btn-success" onClick="GrabarCambios();">
    		<i class="glyphicon glyphicon-floppy-saved"></i> &nbsp;
    		Grabar cambios
    	</button>
    </div>

			
<script type='text/javascript'>

var map;
var polyShape;
var polyLineColor = "#3355ff";
var polyFillColor = "#335599";
var polyPoints = new Array();
var markers = new Array();
//var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
//var labelIndex = 0;
var image = 'includes/img/square.png';

$(function() {
	init_map();
	cargarLugares();

  $("#address").keyup(function(){
    var cant = $(this).val().length;
      if(cant > 0){
        $(this).css({"border-color": "#d2d6de"});   
      }else{
        $(this).css({"border-color": "red"});
      }
  });

});

function init_map() {
	
	  var bangalore = {lat: <?=$result[0]['usuario_latitud'];?>, lng: <?=$result[0]['usuario_longitud']?>};
    map = new google.maps.Map(document.getElementById('gmap_canvas'), {
    	zoom: 14,
    	center: bangalore,
    	mapTypeId: google.maps.MapTypeId.ROADMAP
  	});

  	var geocoder = new google.maps.Geocoder();

  	document.getElementById('submit').addEventListener('click', function() {
    	geocodeAddress(geocoder, map);
  	});

	  google.maps.event.addListener(map, 'click', function(event) {
    	leftClick(event.latLng, map);
  	});

	//addMarker(bangalore, map);

}

/*function addMarker(location, map) {
  	var marker = new google.maps.Marker({
	    position: location,
	    label: labels[labelIndex++ % labels.length],
	    map: map
  	});
}*/

function geocodeAddress(geocoder, resultsMap) {
  	var address = document.getElementById('address').value;

  	geocoder.geocode({'address': address}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      	resultsMap.setCenter(results[0].geometry.location);
	     	updatePosition(results[0].geometry.location,address);
	    } else {
	      alert('Geocode no tuvo éxito por la siguiente razón: ' + status);
	    }
  	});
}

function updatePosition(latLng,nombre){
	$("#lat").val(latLng.lat());
	$("#lon").val(latLng.lng());
	$('#desclugar').text(nombre);

}

function drawPoly(point) {

    if (polyShape) polyShape.setMap(null);
    polyPoints.length = 0;
    document.getElementById("coordspoligono").innerHTML = "";
    for (var i = 0; i < markers.length; i++) {
        polyPoints.push(markers[i].getPosition());
        document.getElementById("coordspoligono").innerHTML += "" + markers[i].getPosition() + ",<br>";

    }
  // Construct the polygon.
	polyShape = new google.maps.Polygon({
	    paths: polyPoints,
	    strokeColor: polyLineColor,
	    strokeOpacity: 3,
	    strokeWeight: .8,
	    fillColor: polyFillColor,
	    fillOpacity: .3,
	    //editable: true
	  });

    var unit = " km&sup2;"; 
    //var area = polyShape.getPath() / (1000 * 1000); falta hacer esto

    if (markers.length <= 2) {
        //report.innerHTML = "&nbsp;";
    }
    else if (markers.length > 2) {
        //report.innerHTML = area.toFixed(3) + unit;
    }

    polyShape.setMap(point);
}

function leftClick(location, point) {
	map = point;
    if (point) {
        // NUMERO MAXIMO DE VERTICES
        var MaxMarker = 200;
        if (markers.length < MaxMarker) {

			image = new google.maps.MarkerImage('includes/img/square.png',
				  		new google.maps.Size(11, 11),
				  		new google.maps.Point(5,5),
				  		new google.maps.Point(5, 5)
  					);

			var marker = new google.maps.Marker({
    			position: location,
    			map: point,
    			icon:image,
    			draggable: true, 
    			bouncy: false, 
    			dragCrossMove: true
  			});

			markers.push(marker);
			marker.setMap(point)

            google.maps.event.addListener(marker, "drag", function () {
                drawPoly(point);
            });

            google.maps.event.addListener(marker, "click", function () {

                for (var n = 0; n < markers.length; n++) {
                    if (markers[n] == marker) {
                        markers[n].setMap(null);
                        break;
                    }
                }
                markers.splice(n, 1);
                drawPoly(point);
            });
            drawPoly(point);
        } else {
            alert("Sólo puedes registrar hasta " + MaxMarker + " vértices");
        }
    }
}

function GrabarCambios() {

	  var coordsarray = $("#coordspoligono").html();
    var nombrevalor = $("#address").val();

  if(nombrevalor.length > 0){

  	if (polyPoints.length < 3 ) {
  		alert("Debe indicar por lo menos 3 vértices");	
  	} else {

          var bounds = new google.maps.LatLngBounds();
          var i;

          for (i = 0; i < polyPoints.length; i++) {
              bounds.extend(polyPoints[i]);
          }
          map.fitBounds(bounds);
          document.getElementById("centerpoligono").innerHTML += "" + bounds.getCenter().lat() + ","+ bounds.getCenter().lng() +"";

          var coordscentr = $("#centerpoligono").html();
          var arrayg = coordsarray.split(',<br>');
          var centro = coordscentr.split(',');

  		$.ajax({
              url : "../BL/lugaresController.php",
              type: "POST",
              data : { nombre: $("#address").val(),zona: 'zona',coordenadas: arrayg,centercoord: centro},
              success: function(data)
              {	
              	if(data = true){
              		clearPoly();
              		window.location = '../index.php?seccion=listazona&status=true';
              	}
              	
              }
          });
  	}

  }else{
    $("#address").css({
      "border-color": "red"
    });

    $("#address").focus();
    
  }
}

function clearPoly() 
{
    polyPoints.length = 0;
    markers.length = 0;
    $("#coordspoligono").html("");
    $("#centerpoligono").html("");

}

function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    if (arr[i] !== undefined) rv[i] = arr[i];
  return rv;
}

function cargarLugares() {

  var id = -1;

  $.ajax({
        url : "BL/lugaresController.php",
        type: "POST",
        data : {listar:'listar',idlugar: id},
        success: function(data)
        {   
          if ( data.length > 0 ) {
            for (var x=0; x<data.length; x++) {
              var poligono;
              var poliPuntos = new Array();
              var coords = data[x].new;
              for (var i=0; i<coords.length; i++) {
                  coords[i].lat= parseFloat(coords[i].lat);
                  coords[i].lng = parseFloat(coords[i].lng);
              }
              poligono = new google.maps.Polygon({
                paths: coords,
                strokeColor: "#125412",
                strokeOpacity: 3,
                strokeWeight: 2,
                fillColor: "#458b45",
                fillOpacity: .3
              });
 
              poligono.setMap(map);
              
            }
          }
        }
    });
}
</script>