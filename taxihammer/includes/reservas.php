<?php
  include'seguridad.php';
?>
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado Reservas</h3>
      </div><!-- /.box-header -->
      <br>
      <div class="row">
        <div class="col-md-12">
          <form role="form">
            <div class="col-md-10">
              <input type="text" class="form-control" name="nombres" placeholder="Buscar por Cliente" required>
            </div>
            <div class="col-md-2">
              <button class="btn btn-primary">Buscar</button>
            </div>
          </form>
        </div>
      </div>
      <br>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Código</th>
              <th>Cliente</th>
              <th>Nro. Celular</th>
              <th>Origen</th>
              <th>Destino</th>
              <th>Conductor</th>
              <th>Hora Inicio</th>
              <th>Costo</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>data</td>
              <td>data</td>
              <td>data</td>
              <td>data</td>
              <td>data</td>
              <td>data</td>
              <td>data</td>
              <td>data</td>
              <td><button class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i></button> <button class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>