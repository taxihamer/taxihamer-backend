<?php
  include'seguridad.php';
?>
<link rel="stylesheet" href="dist/css/angular-validate.css">
<style type="text/css">
  .warning{color: red;}
  .ok{color: green;}
  .pend{color: blue;}
  .rechazar{padding: 4px 6px;
    margin: -53px 0px -26px 10px;}
</style>
<div class="row" ng-app="myapp" ng-controller="recargasController">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Agregar Recarga</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" name="valForm" novalidate>
        <div class="box-body">
          <div class="form-group">
            <label for="destino">Conductor</label>
            <input class="vaform form-control" name="selected" ng-model="visitorInfo.selected" placeholder="Buscar nombre conductor" id="celularConductor" required>
            <span class="valid" ng-show="valForm.selected.$valid">&#10004;</span>
            <input type="hidden" ng-model="conductor_id">
          </div>
          <div class="form-group">
            <label for="monto">Monto a Recarar</label>
            <input type="number" class="vaform form-control" name="monto" ng-model="visitorInfo.monto" placeholder="Monto a Recargar" required>
            <span class="valid" ng-show="valForm.monto.$valid">&#10004;</span>
          </div>
          <div class="form-group">
            <label for="noperacion">Número de Operación</label>
            <input type="number" class="vaform form-control" name="noperacion" ng-model="visitorInfo.noperacion" placeholder="Nro de Operación" required>
            <span class="valid" ng-show="valForm.noperacion.$valid">&#10004;</span>
          </div>
          <div class="form-group">
            <label for="fecha">Fecha de Recarga</label>
            <!--input type="datetime-local" class="vaform form-control" name="fecha" ng-model="visitorInfo.fecha" required>
            <span class="valid" ng-show="valForm.fecha.$valid">&#10004;</span-->
            <div class='input-group date' id='datetimepicker2'>
              <input type='text' class="form-control" width="310" ng-model="visitorInfo.fecha" name="fecha"/>
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
            </span>
            </div>
          </div>
          <div class="form-group">
            <label for="banco">Entidad Financiera</label>
            <select ng-model="select_default" class="form-control" ng-options="c.key as c.value for c in bancos" required></select>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="submitbtn btn btn-primary" ng-disabled="valForm.$invalid" ng-click="saveRecarga(visitorInfo)">Guardar</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
    <div class="box box-primary">
      <div class="alert alert-danger" id="msgErrorRecarga" style="display:none">
        <button type="button" class="close" ng-click="CloseRecarga(1)">&times;</button>
        <strong>Error!</strong> {{messageRecarga}}
      </div>
      <div class="alert alert-success" id="msgSuccessRecarga" style="display:none">
        <button type="button" class="close" ng-click="CloseRecarga(2)">&times;</button>
        <strong>¡Éxito!</strong> {{messageRecarga}}
      </div>
      <div class="box-header with-border">
        <h3 class="box-title">Listado de Recargas</h3>
      </div><!-- /.box-header -->
         <loading></loading>
        <div style="display: inline-block;overflow-x: auto;width: 100%;">
        <table class="table table-striped" datatable="ng" dt-options="dtOptions">
          <thead>
            <tr>
              <th>Conductor</th>
              <th>Teléfono</th>
              <th>Monto</th>
              <th>Banco</th>
              <th>Nro. Operación</th>
              <th>Fecha</th>
              <th>Status</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
             <tr ng-repeat="recarga in recargas">
              <td>{{recarga.conductor_nombre}} {{recarga.conductor_apellido}}</td>
              <td>{{recarga.conductor_celular}}</td>
              <td>{{recarga.recarga_monto | currency:"S/. ":2}}</td>
              
              <td>{{recarga.tiponombre}}</td>
              <td>{{recarga.recarga_num_oper}}</td>
              <td>{{recarga.recarga_fecha}}</td>
              
              
              <td ng-class="{warning: (recarga.recarga_check == 0 || recarga.recarga_check == 99), ok: (recarga.recarga_check == 1), pend: (recarga.recarga_check == 2)}">{{recarga.detalle}}</td>
              <td>
                <div ng-if="recarga.recarga_check==1">
                     <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color: green;"></span>
                </div> 
                <div ng-if="recarga.recarga_check==0">
                       <switch name="enabled" ng-model="recarga.enabled" ng-change="changeRecarga(recarga.recarga_id, $index + 1)" on="on" off="off"></switch>
                </div>
                <div ng-if="recarga.recarga_check==2">
                       <switch name="enabled" ng-model="recarga.enabled" ng-change="changeRecarga(recarga.recarga_id, $index + 1)" on="on" off="off"></switch>
                      <button type="button" class="rechazar btn btn-danger" ng-click="singleModel(recarga.recarga_id, $index + 1, 99)" >
                         Rechazar
                      </button>
                </div>   
              </td>
            </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
    $(function() {

      $('#datetimepicker2').datetimepicker({ format: 'YYYY-MM-DD H:m',});

    });
</script>
