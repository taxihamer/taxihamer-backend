<?php
  include'seguridad.php';
  require_once'DAL/menuDAO.php';
  require_once'DAL/rolDAO.php';

  $rolDAO = new rolDAO();
  $result = $rolDAO->listmenurolid($_SESSION['rol_id']);

  $menuDAO = new menuDAO();
  $data['menurol_id'] = $result[0]['menurol_id'];
  $listmenu = $menuDAO->listaoperadoresById($data);
  
  $menu_parent = (!empty($listmenu[0]['menurol_jsonparent']))?json_decode($listmenu[0]['menurol_jsonparent']):'';

  $menu_children = (!empty($listmenu[0]['menurol_jsonchildren']))?json_decode($listmenu[0]['menurol_jsonchildren']):'';
?>
<?php foreach ($menu_parent as $key => $menuid) {
        $menudata = $menuDAO->listmenuId($menuid); 
        if(!empty($menudata[0]['menu_url'])){ ?>
          <a href="index.php?seccion=<?=$menudata[0]['menu_url']?>">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon <?=$menudata[0]['menu_color']?>"><i class="<?=$menudata[0]['menu_icon']?>"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text"><?=$menudata[0]['menu_nombre']?></span>
                  <span class="info-box-number"><small><?=$menudata[0]['menu_nombre']?></small></span>
                </div>
              </div>
            </div>
          </a>
        <?php } ?>
        <?php 
          $resultsub = $menuDAO->listsubmenuId($menudata[0]['menu_id']);
              if(!empty($resultsub)){
                foreach ($resultsub as $key => $submenu) { 
                  foreach ($menu_children as $i => $child) { 
                    if($submenu['submenu_id'] == $child){ ?>
                      <a href="index.php?seccion=<?=$submenu['submenu_url']?>">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box">
                            <span class="info-box-icon <?=$submenu['submenu_color']?>"><i class="<?=$menudata[0]['menu_icon']?>"></i></span>
                            <div class="info-box-content">
                              <span class="info-box-text"><?=$submenu['submenu_nombre']?></span>
                              <span class="info-box-number"><small><?=$menudata[0]['menu_nombre']?></small></span>
                            </div>
                          </div>
                        </div>
                      </a>
              <?php } ?>
            <?php } ?>
          <?php } ?>
        <?php } ?>
<?php } ?>
<script type="text/javascript">
  $(document).ready(function(){
    console.log(<?php echo count($menu_parent) ?>);
    if(<?php echo count($menu_parent) ?> >= 10){
      console.log('mayor');
      $(".wrapper > .content-wrapper > .content").css({'display':'inline-block'});
    }
  });
</script>