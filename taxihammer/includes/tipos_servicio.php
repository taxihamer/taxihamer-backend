<?php
include'seguridad.php';
require_once'DAL/tipoServicioDAO.php';
$tipoServicioDAO = new tipoServicioDAO();
$result = $tipoServicioDAO->lista();
?>
<div class="row" ng-app="myapp" ng-controller="TipoServicioController">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Agregar Tipos de Servicio</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" method="post" action="../BL/tipoServicioController.php">
        <div class="box-body">
          <div class="form-group">
            <label for="servicio">Tipo de Servicio</label>
            <input type="text" class="form-control" name="nombre" placeholder="Tipo del Servicio" required>
            <input type="hidden" class="form-control" name="hidden_insert_tiposervicio" value="1">
          </div>
          <div class="form-group">
            <label for="factor">Factor (% incremento)</label>
            <input type="number" class="form-control" name="factor" placeholder="Ingrese Factor" required>
          </div>
          <div class="form-group">
            <label for="factor">Tarifa Aeropuerto</label>
            <input type="number" class="form-control" min="0" name="aeropuerto" placeholder="Ingrese tarifa de aeropuerto" required>
          </div>
          <div class="form-group">
            <label for="factor">Tarifa Reserva</label>
            <input type="number" class="form-control" min="0" name="reserva" placeholder="Ingrese tarifa de reserva" required>
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
  <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado Tipos de Servicio</h3>
      </div><!-- /.box-header -->
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Nombre del Servicio</th>
              <th>Factor (%)</th>
              <th>Tarifa Aeropuerto</th>
              <th>Tarifa Reserva</th>
              <th>Status</th>
              <th>Accion</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat = "tiposerv in tiposervs |orderBy:columnToOrder:reverse">
              <td>{{tiposerv.tiposervicio_nombre}}</td>
              <td>{{tiposerv.tiposervicio_factor}}</td>
              <td>{{tiposerv.tiposervicio_aeropuerto}}</td>
              <td>{{tiposerv.tiposervicio_reserva}}</td>
              <td ng-if=tiposerv.tiposervicio_status==1><span class="label label-success">Activo</span></td>
              <td ng-if=tiposerv.tiposervicio_status==0><span class="label label-danger">Bloqueado</span></td>
              <td><button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#myModal" ng-click="Editar(tiposerv.tiposervicio_id)"><i class="glyphicon glyphicon-edit"></i></button> 
              <a ng-click="deleteC(tiposerv.tiposervicio_id)" class="btn btn-danger btn-circle" ><i class="fa fa-close" aria-hidden="true"></i></a></td>
            </tr>
          </tbody>
        </table>
        <div class="row">
            <div class="col-sm-5" style="margin-left:15px;">
                <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                <!-- Showing 1 to 10 of 848 entries -->
            </div>
            <div class="col-sm-6" style="text-align: right;">
                <div class="dataTables_paginate paging_simple_numbers">
                  <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar Tipos de Servicio</h4>
        </div>
        <form role="form" method="post" action="../BL/tipoServicioController.php">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label for="correo">Tipo de Servicio</label>
              <input type="text" class="form-control" name="servicio" id="servicio" placeholder="Tipo de Servicio" required>
              <input type="hidden" name="id" id="idservicio">
              <input type="hidden" name="hidden_update_tiposervicio" value="1">
            </div>
            <div class="form-group">
              <label for="factorEdit">Factor (% incremento)</label>
              <input type="number" class="form-control" name="factorEdit" id="factorEdit" placeholder="Ingrese Factor" required>
            </div>
            <div class="form-group">
              <label for="aeropuertoEdit">Tarifa Aeropuerto</label>
              <input type="number" class="form-control" min="0" name="aeropuertoEdit" id="aeropuertoEdit" placeholder="Ingrese tarifa de aeropuerto" required>
            </div>
            <div class="form-group">
              <label for="reservaEdit">Tarifa Reserva</label>
              <input type="number" class="form-control" min="0" name="reservaEdit" id="reservaEdit" placeholder="Ingrese tarifa de reserva" required>
            </div>
          </div><!-- /.box-body -->
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Guardar Cambios</button>
        </div>
      </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</div>

