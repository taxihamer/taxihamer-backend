<?php
  include'seguridad.php';
  require_once'DAL/usuarioDAO.php';

$data['rol_id'] = $_SESSION['rol_id'];
$data['mail'] = $_SESSION['mail'];
  $usuarioDAO = new usuarioDAO();
  $result = $usuarioDAO->listacoordenadas($data);

?>
<link rel="stylesheet" href="dist/css/angular-validate.css">
<style>
  #map{
    height:600px;
    width:100%;
  }
  #map img{
    max-width:none!important;
    background:none!important;
  }

  .warning{color: red;}
  .ok{color: green;}
  .pend{color: blue;}

  #modalview .modal-content { height: 200px; }
</style>

<div class="row" ng-app="myapp">
  <div class="col-md-12" ng-controller="alerta">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado de Alertas</h3>
      </div>
      <div class="row">
        <div class="col-md-8">
              <div class="col-md-8">
                <input type="text" class="form-control" placeholder="Buscar por Nombres" ng-model="searchText">
              </div>
        </div>
      </div>
      <br>
      <div>
      <div style="display: inline-block;overflow-x: auto;width: 100%;">
        <table datatable="ng" dt-options="dtOptionsd" class="table table-hover" >
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tipo Alerta</th>
                    <th>Nombre</th>
                    <th>Unidad</th>
                    <th>Latitud</th>
                    <th>Longitud</th>
                    <th>Fecha</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="alert in alerts | filter:searchText track by $index " ng-hide="hide">
                    <td>{{$index + 1}}</td>
                    <td>{{alert.alertatipoid}}</td>
                    <td>{{alert.alerta_personaid}}</td>
                    <td>{{alert.alerta_unidadid}}</td>
                    <td>{{alert.alerta_latitud}}</td>
                    <td>{{alert.alerta_longitud}}</td>
                    <td>{{alert.alerta_fecha}}</td>
                    <td ng-class="{ok: (alert.alerta_ckeck == 1), pend: (alert.alerta_ckeck == 0)}">{{alert.detalle}}</td>
                    <td style="display:inline-block;width: 100%;">
                      <div class="pull-left">
                        <button class="btn btn-default" data-toggle="modal" data-target="#myModal" ng-click="llenarMap(alert.alerta_id,alert.alerta_latitud,alert.alerta_longitud)"><i class="fa fa-map-marker"></i></button>
                      </div>
                      <div class="pull-left" style="margin:0px 5px;">
                        <button class="btn btn-info" ng-click="toggleModal(alert.alerta_id,alert.alerta_tipo)"><i class="glyphicon glyphicon-list-alt"></i>
                        </button>
                      </div>
                      <div ng-if="alert.alerta_ckeck==0">
                             <switch name="enabled" ng-model="alert.enabled" ng-change="toggleAlert(alert.alerta_id, $index + 1)" on="on" off="off"></switch>
                      </div>
                      <!---->
                      <!--input ng-if="1!=alert.alerta_ckeck" type="checkbox" ng-click="toggleAlert(alert.alerta_id)"/-->
                    </td>
                </tr>
            </tbody>
        </table>
      </div>
        <modal title="Detalle de Alerta" visible="showModal" style="display:none;" id="modalview" data-backdrop="static" data-keyboard="false">
              <div ng-if="indetails.T==3" class="col-md-12">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <span><b>Conductor: </b>{{indetails.conductor_nombre}} {{indetails.conductor_apellido}}</span>
                  </div>
                  <div class="col-md-6">
                    <span><b>Cliente: </b>{{indetails.cliente_nombre}} {{indetails.cliente_apellido}}</span>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <span><b>Celular: </b>{{indetails.conductor_celular}}</span>
                  </div>
                  <div class="col-md-6">
                    <span><b>Celular: </b>{{indetails.cliente_celular}}</span>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <span><b>Correo: </b>{{indetails.conductor_correo}}</span>
                  </div>
                  <div class="col-md-6">
                    <span><b>Correo: </b>{{indetails.cliente_correo}}</span>
                  </div>
                </div>
              </div>

              <div ng-if="indetails.T==2" class="col-md-12" style="text-align:center;">
                <div class="col-md-12">
                  <div class="col-md-12">
                    <span><b>Conductor: </b>{{indetails.conductor_nombre}} {{indetails.conductor_apellido}}</span>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-12">
                    <span><b>Celular: </b>{{indetails.conductor_celular}}</span>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-12">
                    <span><b>Correo: </b>{{indetails.conductor_correo}}</span>
                  </div>
                </div>
              </div>

              <div ng-if="indetails.T==1" class="col-md-12" style="text-align:center;">
                 <div class="col-md-12">
                  <div class="col-md-12">
                    <span><b>Cliente: </b>{{indetails.cliente_nombre}} {{indetails.cliente_apellido}}</span>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-12">
                    <span><b>Celular: </b>{{indetails.cliente_celular}}</span>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-12">
                    <span><b>Correo: </b>{{indetails.cliente_correo}}</span>
                  </div>
                </div>
              </div>
              <hr>
              <div class="form-group"> 
                  <div class="col-sm-offset-2 col-sm-10 ">
                    <button type="submit" class="btn btn-default pull-right" data-dismiss="modal">Aceptar</button>
                  </div>
              </div>
        </modal>
      </div>
    </div>
  </div>
</div>

  <div class="modal" id="myModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Alerta</h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div style='overflow:hidden;height:600px;width:100%;'>
              <div id='map'></div>
            </div>
          </div><!-- /.box-body -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" >Cerrar</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

<script type='text/javascript'>

var map;
var marker;
var markers = [];

$(function() {init_mapa();});
// Creacion del Map Google.Maps.api
function init_mapa() {

    var bangalore = {lat: <?=$result[0]['usuario_latitud'];?>, lng: <?=$result[0]['usuario_longitud']?>};
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: bangalore,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    $("#myModal").on("shown.bs.modal", function () {
       google.maps.event.trigger(map, 'resize');
       //moveToLocation(<?=$result[0]['usuario_latitud'];?>,<?=$result[0]['usuario_longitud']?>);
    });
    
     map.addListener('center_changed', function() {
    // 3 seconds after the center of the map has changed, pan back to the
    // marker.
      window.setTimeout(function() {
        map.panTo(marker.getPosition());
      }, 10);
    });
}

// Establece el centro de la posicion de la matriz.
function moveToLocation(lat, lng){
    var center = new google.maps.LatLng(lat, lng);
    map.panTo(center);
    map.setZoom(14); 
}

// Establece el mapa de todos los marcadores de la matriz.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Elimina los marcadores del mapa, pero los mantiene en la matriz.
function clearMarkers() {
  setMapOnAll(null);
}

// Animacion de Marker 
function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}

</script>

<link data-require="datatable-css@1.10.7" data-semver="1.10.7" rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" />
<!--script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<script src="https://code.angularjs.org/1.4.5/angular-route.js"></script> 
<script src="plugins/Angular/angular-datatables.min.js"></script>
<script src="http://jvandemo.github.io/angularjs-google-maps/dist/angularjs-google-maps.js"></script>
<!--script src="plugins/Angular/app.js"></script>

