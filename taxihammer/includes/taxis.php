<?php
  include'seguridad.php';
  require_once'DAL/tipoServicioDAO.php';
  require_once'DAL/vehiculoDAO.php';
  $tipoServicioDAO = new tipoServicioDAO();
  $Tipo = $tipoServicioDAO->lista();
  $vehiculoDAO = new vehiculoDAO();
  $Res = $vehiculoDAO->lista();
?>
<style type="text/css">
  .nullOld{text-align: center;color: #e23558;}
  .ui-dialog-titlebar-close{display: none !important;}
  .ok{color:green;}
  .error{color:red;} 
</style>
<script type="text/javascript">
  window.URL = window.URL || window.webkitURL;
  $(document).ready(function () {
    $('.ang').hide();

    $("#image-file").change(function(e) {

        var fileInput = $("input[type=file]")[0],
        file = fileInput.files && fileInput.files[0];

          if( file ) {
              var img = new Image();
              img.src = window.URL.createObjectURL( file );

              img.onload = function() {
                  var width = img.naturalWidth,
                      height = img.naturalHeight;
      
                  window.URL.revokeObjectURL( img.src );

                  if( width <= 450 && height <= 450 ) {
                     $("#msfile").css({"color":"#737373"});
                  }
                  else {
                      var control = $('#image-file');
                      control.replaceWith( control = control.val('').clone( true ) );
                      $("#msfile").css({"color":"#dd4b39"});
                  }
              };
          }
    });

  });
</script>
<div class="row" ng-app="myapp" ng-controller="UnidadesController">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Mantenimiento de Vehiculos</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form role="form" action="../BL/vehiculoController.php" enctype="multipart/form-data" method="post">
        <div class="box-body">
          <div class="form-group">
            <label for="alias">Alias</label>
            <input type="text" class="form-control" name="alias" placeholder="Ej. Vehiculo 1" required>
            <input type="hidden" name="hidden_insert_vehiculo" value="1">
          </div>
          <div class="form-group">
            <label for="tipo">Tipo Servicio</label>
            <select class="form-control" name="tiposervicio" required>
            <?php
            foreach($Tipo AS $tipo){
              ?>
              <option value ="<?php echo $tipo['tiposervicio_id']; ?>"><?php echo $tipo['tiposervicio_nombre']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
          <div class="form-group">
            <label for="placa">Placa</label>
            <input type="text" class="form-control" name="placa" placeholder="Ej. AQG332" required>
          </div>
          <div class="form-group">
            <label for="marca">Marca</label>
            <input type="text" class="form-control" name="marca" placeholder="Ej. Toyota" required>
          </div>
          <div class="form-group">
            <label for="modelo">Modelo</label>
            <input type="text" class="form-control" name="modelo" placeholder="Ej. Yaris" required>
          </div>
          <div class="form-group">
            <label for="color">Color</label>
            <input type="text" class="form-control" name="color" placeholder="Ej. Rojo" required>
          </div>
          <div class="form-group">
            <label for="fabricacion">Año Fabricación</label>
            <input type="number" class="form-control" name="fabricacion" placeholder="Ej. 2016" required>
          </div>
          <div class="form-group">
            <label for="soat">Fecha Vencimiento SOAT</label>
            <input type="date" class="form-control" name="fechasoat" required>
          </div>
          <div class="form-group">
            <label for="modelo">Fecha Vencimiento Revisión Técnica</label>
            <input type="date" class="form-control" name="fecharevtec" required>
          </div>
          <div class="form-group">
            <label for="imagen">Imagen</label>
            <input type="file" class="form-control" name="foto" id="image-file" required>
            <small id="msfile">Imagen en JPG máximo tamaño 450 x 450px</small>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="caracteristicas[]" value="1">
              Aire Acondicionado y Calefacción
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="caracteristicas[]" value="2">
              Aeropuerto 
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="caracteristicas[]" value="3">
              Cargador de Celular
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="caracteristicas[]" value="4">
              Transporte de Mascotas
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="caracteristicas[]" value="5"><!--Transporte de Documentos-->
              Emitir Factura o Boleta
            </label>
          </div>
           <div class="checkbox">
            <label>
              <input type="checkbox" name="caracteristicas[]" value="6">
              Wifi
            </label>
          </div> 
          <div class="checkbox" style="display:none">
            <label>
              <input type="checkbox" name="caracteristicas[]" value="7">
              Transporte de Niños
            </label>
          </div> 
          <div class="checkbox" style="display:none">
            <label>
              <input type="checkbox" name="caracteristicas[]" value="8">
              Transporte de Adultos Mayores
            </label>
          </div> 
          <div class="checkbox">
            <label>
              <input type="checkbox" name="caracteristicas[]" value="9">
              Playas
            </label>
          </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
    </div><!-- /.box -->
  </div>
  <div class="col-md-9">
  <?php
    if(isset($_REQUEST['status'])){
      if($_REQUEST['status']=="true"){
        echo '<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Operación Realizada con Éxito</div>';
      }elseif($_REQUEST['status'] == "valid"){
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups. El n&uacute;mero de placa ya est&aacute; resgistrado.</div>';
      }else{
        echo '<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Ups, ocurrio un error inesperado.</div>';
      }
    }
    ?>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Listado de Vehículos</h3>
      </div><!-- /.box-header -->
      <br>
        <div class="dataTables_wrapper form-inline" role="grid">
          <div class="row" style="margin-left:0px;margin-right:0px;">
            <div class="col-sm-6">
                <div class="dataTables_length" id="example_length">
                    <label>Exportar : </label>
                    <form role="form" action="../BL/exportarController.php" enctype="multipart/form-data" method="post" style="width: 30%;
                      display: inline-block;">
                      <input type="hidden" name="hidden_exportar" value="1">
                      <input type="hidden" name="exportar" value="1">
                      <button type="submit" class="btn btn-primary btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div id="example_filter" class="dataTables_filter">
                 <label style="margin-right: 30px;">Tipo de Servicio : <select class="form-control" id="tipoipo" ng-change="tipovehiculo()" ng-model="stipoServicio">
                    <option value="">Seleccione </option>  
                    <?php foreach($Tipo AS $tipo){ ?>
                      <option value ="<?php echo $tipo['tiposervicio_id']; ?>"><?php echo $tipo['tiposervicio_nombre']; ?></option>
                    <?php } ?>
                    </select></label>
                    <label>Buscar :<input type="search" class="form-control input-sm" aria-controls="example" ng-model="searchText" ng-change="searchTextChanged()" placeholder="Palabra clave"></label>
                </div>
            </div>
          </div>
          <br>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Imagen</th>
                  <th>Alias</th>
                  <th>Nro. Placa</th>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>Color</th>
                  <th>Año Fabricación</th>
                  <th>Tipo Servicio</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody id="response" class="buscar">
                <tr ng-repeat = "vehiculo in vehiculos |orderBy:columnToOrder:reverse">
                  <td><img ng-src="BL/{{vehiculo.unidad_foto}}" alt="taxi" width="50px" class="img-circle" /></td>
                  <td>{{vehiculo.unidad_alias}}</td>
                  <td>{{vehiculo.unidad_placa}}</td>
                  <td>{{vehiculo.unidad_marca}}</td>
                  <td>{{vehiculo.unidad_modelo}}</td>
                  <td>{{vehiculo.unidad_color}}</td>
                  <td>{{vehiculo.unidad_fabricacion}}</td>
                  <td>{{vehiculo.tiposervicio_nombre}}</td>
                  <td>
                    <button class="btn btn-warning btn-circle" data-toggle="modal" data-target="#myModal" ng-click="Editar(vehiculo.unidad_id)"><i class="glyphicon glyphicon-edit"></i></button>
                    <a class="btn btn-danger btn-circle" ng-click="EliminarVehiculo(vehiculo.unidad_id)"><i class="glyphicon glyphicon-remove"></i></a>
                  </td>
                </tr>
                <tr class="nullOld" id="nullOld">
                    <td colspan="9">No hay datos a mostrar</td>
                </tr>
              </tbody>
            </table>
            <div class="row">
                <div class="col-sm-5" style="margin-left:15px;">
                    <div class="dataTables_info" ole="alert" aria-live="polite" aria-relevant="all">Mostrando {{startItem}} a {{endItem}} de {{totalItems}} entradas</div>
                    <!-- Showing 1 to 10 of 848 entries -->
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <div class="dataTables_paginate paging_simple_numbers">
                      <pagination total-items="totalItems" ng-model="currentPage" ng-change="pageChanged()" items-per-page="pageSize" max-size="3" boundary-links="true" rotate="false" ></pagination>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar Vehículos</h4>
        </div>
        <form role="form" method="post" action="../BL/vehiculoController.php" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="box-body">
          <div class="form-group">
            <label for="correo">Alias</label>
            <input type="text" class="form-control" name="alias1" id="alias1" placeholder="Ej. Vehiculo 1" required>
            <input type="hidden" name="unidad_id" id="unidad_id">
            <input type="hidden" name="hidden_update_vehiculo" value="1">
          </div>
          <div class="form-group">
            <label for="tipo">Tipo Servicio</label>
            <select class="form-control" name="tiposervicio1" id="tiposervicio1" required>
            <?php
            foreach($Tipo AS $tipo){
              ?>
              <option value ="<?php echo $tipo['tiposervicio_id']; ?>"><?php echo $tipo['tiposervicio_nombre']; ?></option>
              <?php
              
            }
            ?>
            </select>
          </div>
           <div class="form-group">
              <label for="correo">Placa</label>
              <input type="text" class="form-control" name="placa1" id="placa1" placeholder="Ej. AQG332" required>
            </div>
            <div class="form-group">
              <label for="correo">Marca</label>
              <input type="text" class="form-control" name="marca1" id="marca1" placeholder="Ej. Toyota" required>
            </div>
            <div class="form-group">
              <label for="correo">Modelo</label>
              <input type="text" class="form-control" name="modelo1" id="modelo1" placeholder="Ej. Yaris" required>
            </div>
            <div class="form-group">
              <label for="correo">Color</label>
              <input type="text" class="form-control" name="color1" id="color1" placeholder="Ej. Azul" required>
            </div>
            <div class="form-group">
              <label for="correo">Año Fabricación</label>
              <input type="number" class="form-control" name="fabricacion1" id="fabricacion1" placeholder="Ej. 2016" required>
            </div>
            <div class="form-group">
              <label for="soat">Fecha Vencimiento SOAT</label>
              <input type="text" class="form-control" name="fechasoat1" id="fechasoat1" required>
            </div>
            <div class="form-group">
              <label for="modelo">Fecha Vencimiento Revisión Técnica</label>
              <input type="text" class="form-control" name="fecharevtec1" id="fecharevtec1" required>
            </div>
            <div class="form-group">
              <small>Foto Actual</small>
              <div id="foto2">
              </div>
            </div>
            <div class="form-group">
              <label for="imagen">Imagen</label>
              <input type="file" class="form-control" name="foto1" id="foto1">
              <small>Imagen en JPG máximo tamaño 450 x 450px</small>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="caracteristicasedit[]" class="chek" value="1">
                Aire Acondicionado y Calefacción
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="caracteristicasedit[]" class="chek" value="2">
                Aeropuerto 
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="caracteristicasedit[]" class="chek" value="3">
                Cargador de Celular
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="caracteristicasedit[]" class="chek" value="4">
                Transporte de Mascotas
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="caracteristicasedit[]" class="chek" value="5"><!--Transporte de Documentos-->
                Emitir Factura o Boleta
              </label>
            </div>
             <div class="checkbox">
              <label>
                <input type="checkbox" name="caracteristicasedit[]" class="chek" value="6">
                Wifi
              </label>
            </div> 
            <div class="checkbox" style="display:none">
              <label>
                <input type="checkbox" name="caracteristicasedit[]" class="chek" value="7">
                Transporte de Niños
              </label>
            </div> 
            <div class="checkbox" style="display:none">
              <label>
                <input type="checkbox" name="caracteristicasedit[]" class="chek" value="8">
                Transporte de Adultos Mayores
              </label>
            </div> 
            <div class="checkbox">
              <label>
                <input type="checkbox" name="caracteristicasedit[]" class="chek" value="9">
                Playas
              </label>
            </div>
          </div><!-- /.box-body -->
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Guardar Cambios</button>
        </div>
      </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</div>
<script>


function Exportar(){
  $.ajax({
      url: 'BL/exportarController.php',
      data: {
        hidden_exportar:'hidden_exportar'
      },
      cache: false,
      method: 'post'
  }).done(function(data){

  });
}

function Buscar() {
  tipo = document.getElementById('tipoipo').value;
  alias = document.getElementById('aliasas').value;
    $.ajax({
        url: 'BL/vehiculoController.php',
        data: {
            tipo: tipo,
            alias: alias,
            buscar: 1
        },
        cache: false,
        method: 'post'
    }).done(function (data) {
        $('#response').html(data);
    });
}
</script>
<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#aliasas').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.buscar tr').hide();
            $('.buscar tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
 
  });
</script>
