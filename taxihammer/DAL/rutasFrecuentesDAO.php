<?php
require_once'db.php';
require_once'character.php';

class rutasFrecuentesDAO{
	
	public function lista($data){
		try {
			$database = new ConexionBD();
			//Modificado por Danny
			/*if ($data['tipo_tarifa']==1) {
				$sql = "SELECT a.*, b.tarifario_monto AS tarifa FROM  tb_rutas_frecuentes a, tb_tarifario b WHERE b.origen_id = a.id_origen AND b.destino_id = a.id_destino AND a.tiposervicio_id = b.tiposervicio_id AND b.tarifario_status = 1 AND a.cliente_id = :cliente_id";
			}
			else{
				$sql = "SELECT *, '0' AS tarifa FROM  tb_rutas_frecuentes WHERE cliente_id = :cliente_id";
			}*/
			//Modificado por Danny
			$sql = "CALL sp_listar_rutas_frecuentes(:tarifa, :cliente_id)";
			$database->query($sql);
			$database->bind(':tarifa', $data['tipo_tarifa']);
			$database->bind(':cliente_id', $data['cliente_id']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertarRutasFrecuentes($data){
		try{
			$database = new ConexionBD();

			/*$sql = "INSERT INTO tb_rutas_frecuentes 
			(cliente_id, rutas_nombre, origen_latitud, origen_longitud, destino_latitud, destino_longitud, id_origen, id_destino, tiposervicio_id, direccion_origen, direccion_destino, referencia_origen, referencia_destino, nombre_origen, nombre_destino) 
			VALUES 
			(:cliente_id, :rutas_nombre, :origen_latitud, :origen_longitud, :destino_latitud, :destino_longitud, :id_origen, :id_destino, :tiposervicio_id, :direccion_origen, :direccion_destino, :referencia_origen, :referencia_destino, :nombre_origen, :nombre_destino)"; */

			$sql = "CALL sp_rutasfrecuentes_mantenimiento(@a_message,'nuevo','',:cliente_id,:rutas_nombre,:origen_latitud,:origen_longitud,:destino_latitud,:destino_longitud,:id_origen,:id_destino,:tiposervicio_id,:direccion_origen,:direccion_destino,:referencia_origen,:referencia_destino,:nombre_origen,:nombre_destino)";
			$database->query($sql);
	        $database->bind(':cliente_id', $data['cliente_id']);
	        $database->bind(':rutas_nombre', SanitizeCharacter($data['rutas_nombre']));
	        $database->bind(':origen_latitud', $data['origen_latitud']);
	        $database->bind(':origen_longitud', $data['origen_longitud']);
	        $database->bind(':destino_latitud', $data['destino_latitud']);
	        $database->bind(':destino_longitud', $data['destino_longitud']);
	        $database->bind(':id_origen', $data['id_origen']);
	        $database->bind(':id_destino', $data['id_destino']);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':direccion_origen', SanitizeCharacter($data['direccion_origen']));
	        $database->bind(':direccion_destino', SanitizeCharacter($data['direccion_destino']));
	        $database->bind(':referencia_origen', SanitizeCharacter($data['referencia_origen']));
	        $database->bind(':referencia_destino',SanitizeCharacter($data['referencia_destino']));
	        $database->bind(':nombre_origen', SanitizeCharacter($data['nombre_origen']));
	        $database->bind(':nombre_destino', SanitizeCharacter($data['nombre_destino']));
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function Eliminar($data){
		try {

			$database = new ConexionBD();
			//$sql = "DELETE FROM tb_rutas_frecuentes WHERE rutas_id = :rutas_id";
			$sql = "CALL sp_rutasfrecuentes_mantenimiento(@a_message,'delete',:cliente_id,'','','','','','','','','','','','','','')";
			$database->query($sql);
			$database->bind(':rutas_id', $data['rutas_id']);
	        $database->execute();
	        return true;

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function actualizarRutasFrecuentes($data){
		try{
			$database = new ConexionBD();

			$sql = "UPDATE tb_rutas_frecuentes SET
			cliente_id = :cliente_id, rutas_nombre = :rutas_nombre, rutas_latitud = :rutas_latitud, rutas_longitud = :rutas_longitud, id_origen = :id_origen, id_destino = :id_destino, tiposervicio_id = :tiposervicio_id, direccion_origen = :direccion_origen, direccion_destino = :direccion_destino, referencia_origen = :referencia_origen, referencia_destino = :referencia_destino
			WHERE rutas_id = :rutas_id";

			$database->query($sql);
			$database->bind(':rutas_id', $data['rutas_id']);
	        $database->bind(':cliente_id', $data['cliente_id']);
	        $database->bind(':rutas_nombre', SanitizeCharacter($data['rutas_nombre']));
	        $database->bind(':rutas_latitud', $data['rutas_latitud']);
	        $database->bind(':rutas_longitud', $data['rutas_longitud']);
	        $database->bind(':id_origen', $data['id_origen']);
	        $database->bind(':id_destino', $data['id_destino']);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':direccion_origen', SanitizeCharacter($data['direccion_origen']));
	        $database->bind(':direccion_destino', SanitizeCharacter($data['direccion_destino']));
	        $database->bind(':referencia_origen', SanitizeCharacter($data['referencia_origen']));
	        $database->bind(':referencia_destino', SanitizeCharacter($data['referencia_destino']));
	        
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
}