<?php
require_once'db.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class puntosDAO{

	public function listaptos(){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_puntos where puntos_delete = 1";
			$sql = "CALL sp_listar_puntos()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertPuntos($data){

		try {
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_puntos (puntos_cliente, puntos_conductor, puntos_factor_cliente, puntos_factor_conductor) VALUES (:puntos_cliente, :puntos_conductor, :puntos_factor_cliente, :puntos_factor_conductor)";
			$sql = "CALL sp_puntos_mantenimiento(@a_message, 'nuevo','',:puntos_cliente,:puntos_factor_cliente,:puntos_conductor,:puntos_factor_conductor,'','')";
			$database->query($sql);
	        $database->bind(':puntos_cliente', $data['puntos_cliente']);
	        $database->bind(':puntos_factor_cliente', $data['puntos_factor_cliente']);
	        $database->bind(':puntos_conductor', $data['puntos_conductor']);
	        $database->bind(':puntos_factor_conductor', $data['puntos_factor_conductor']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function updaPuntos($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_puntos SET puntos_estado = :puntos_estado WHERE puntos_id = :puntos_id";
			$sql = "CALL sp_puntos_mantenimiento(@a_message, 'update',:puntos_id,'','','','',:puntos_estado,'')";
			$database->query($sql);
			$database->bind(':puntos_estado', $data['puntos_estado']);
	        $database->bind(':puntos_id', $data['puntos_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function deletPuntos($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_puntos SET puntos_delete = :puntos_delete WHERE puntos_id = :puntos_id";
			$sql = "CALL sp_puntos_mantenimiento(@a_message, 'delete',:puntos_id,'','','','','',:puntos_delete)";
			$database->query($sql);
			$database->bind(':puntos_delete', $data['puntos_delete']);
	        $database->bind(':puntos_id', $data['puntos_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}
	public function validPuntos(){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*)as total FROM tb_puntos WHERE puntos_estado = 1";
			$sql = "CALL sp_validar_puntos()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function updatestados(){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_puntos SET puntos_estado = 0 ";
			$sql = "CALL sp_puntos_mantenimiento(@a_message, 'estados','','','','','','','')";
			$database->query($sql);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}
	
	public function CountReportePuntos($where, $select){
		try {
			$database = new ConexionBD();
			if($select != 1){

				$sql = "SELECT count(*) as total FROM tb_conductor a LEFT JOIN tb_unidad b ON a.unidad_id = b.unidad_id LEFT JOIN tb_tiposervicio c ON b.tiposervicio_id = c.tiposervicio_id WHERE conductor_status not in (2)".$where;

			}else{

				$sql = "SELECT count(*) as total FROM tb_cliente a WHERE cliente_status = 1".$where;
			}

			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}
}