<?php
require_once'db.php';
require_once'character.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class datosSeguridadDAO{
	public function lista(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_datos_seguridad";
			$sql = "CALL sp_lista_datos_seguridad(0)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaSeguridad($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_datos_seguridad WHERE dseg_id = :dseg_id AND dseg_status = 1";
			$sql = "CALL sp_lista_datos_seguridad(:dseg_id)";
			$database->query($sql);
			$database->bind(':dseg_id', $data['cliente_id']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insert($data){
		try {
			$database = new ConexionBD();

			//$sql = "INSERT INTO tb_datos_seguridad (dseg_id, dseg_nombre, dseg_correo, dseg_status) VALUES (:dseg_id, :dseg_nombre, :dseg_correo, :dseg_status)";
			$sql = "CALL sp_datos_seguridad_mantenimiento(@a_message, :tipo, :dseg_id, :dseg_nombre, :dseg_correo, :dseg_status)";
			$database->query($sql);
			$database->bind(':tipo', 'nuevo');
			$database->bind(':dseg_id', $data['dseg_id']);
			$database->bind(':dseg_nombre', SanitizeCharacter($data['dseg_nombre']));
			$database->bind(':dseg_correo', $data['dseg_correo']);
			$database->bind(':dseg_status', $data['dseg_status']);
	        $database->execute();
	        return true;

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function update($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_datos_seguridad SET dseg_nombre = :dseg_nombre, dseg_correo = :dseg_correo, dseg_status = :dseg_status WHERE dseg_id = :dseg_id";
			$sql = "CALL sp_datos_seguridad_mantenimiento(@a_message, :tipo, :dseg_id, :dseg_nombre, :dseg_correo, :dseg_status)";
			$database->query($sql);
			$database->bind(':tipo', 'update');
			$database->bind(':dseg_id', $data['dseg_id']);
			$database->bind(':dseg_nombre', $data['dseg_nombre']);
			$database->bind(':dseg_correo', $data['dseg_correo']);
			$database->bind(':dseg_status', $data['dseg_status']);
	        $database->execute();
	        return true;

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function validSeguridadCliente($cliente_id){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT count(*) as Total FROM tb_datos_seguridad WHERE dseg_id = :cliente_id";
			$sql = "CALL sp_validar_seguridad(:cliente_id)";
			$database->query($sql);
			$database->bind(':cliente_id', $cliente_id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
}