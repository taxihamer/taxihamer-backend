<?php
require_once'db.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class alertaDAO{

	public function insertAlerta($data){

		$unidad_id = ($data['alerta_tipo'] == 1)?'null':(($data['alerta_tipo'] == 2)?$data['alerta_unidadid']:$data['alerta_unidadid']); 

		try {
			
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_alerta (alerta_personaid, alerta_unidadid, alerta_latitud, alerta_longitud, alerta_tipo, alerta_fecha, alerta_flag) VALUES (:alerta_personaid, :alerta_unidadid, :alerta_latitud, :alerta_longitud, :alerta_tipo, :alerta_fecha, :alerta_flag)";
			$sql = "CALL sp_alerta_mantenimiento(@a_message, :tipo, :alerta_personaid, :alerta_unidadid, :alerta_latitud, :alerta_longitud, :alerta_tipo, :alerta_flag , 0 , 0)";
			$database->query($sql);

			$database->bind(':tipo', 'nuevo');
	        $database->bind(':alerta_personaid', $data['alerta_personaid']);
	        $database->bind(':alerta_unidadid', $unidad_id);
	        $database->bind(':alerta_latitud', $data['alerta_latitud']);
	        $database->bind(':alerta_longitud', $data['alerta_longitud']);
	        $database->bind(':alerta_tipo', $data['alerta_tipo']);
	        //$database->bind(':alerta_fecha',  date("Y/m/d H:i:s"));
	        $database->bind(':alerta_flag',  $data['alerta_flag']);

	        $database->execute();
	        //$id = $database->lastInsertId();
	        //return array('status' => true, 'lastid' => $id);
	        return $database->resultSet();

		}catch(Exception $e){
			throw $e;
		}
	}

	public function listaAlert(){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_alerta WHERE alerta_estado = 1";
			$sql = "CALL sp_listar_alertas_activas(0)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

		}catch (Exception $e) {
			throw $e;
		}
	}

	public function updaChek($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_alerta SET alerta_ckeck = :alerta_ckeck WHERE alerta_id = :alerta_id";
			$sql = " CALL sp_alerta_mantenimiento(@a_message, :tipo, 0, 0, '', '', '', '' , :alerta_id , :alerta_ckeck)";
			$database->query($sql);
			$database->bind(':tipo', 'check');
			$database->bind(':alerta_ckeck', $data['alerta_ckeck']);
	        $database->bind(':alerta_id', $data['alerta_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
	    	throw $e;
	    }
	}

	public function listaAlertId($alerta_id){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_alerta WHERE alerta_estado = 1 AND alerta_id = :alerta_id";
			$sql = "CALL sp_listar_alertas_activas(:alerta_id)";
			$database->query($sql);
	        $database->bind(':alerta_id', $alerta_id);
	        $database->execute();
	        return $database->resultSet();
	    } catch (Exception $e) {
	    	throw $e;
	    }
	}

	public function actualizarClick($alerta_id){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_alerta SET alerta_click = :alerta_click WHERE alerta_id = :alerta_id";
			$sql = " CALL sp_alerta_mantenimiento(@a_message, :tipo, 0, 0, '', '', '', '' , :alerta_id , 0)";
			$database->query($sql);
			$database->bind(':tipo', 'click');
			//$database->bind(':alerta_click', 0);
	        $database->bind(':alerta_id', $alerta_id);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaByVerifi($alerta_id){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_alerta WHERE alerta_id = :alerta_id";
			$sql = "CALL sp_listar_alerta_id(:alerta_id)";
			$database->query($sql);
			$database->bind(':alerta_id', $alerta_id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
}