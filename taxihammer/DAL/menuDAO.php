<?php
require_once'db.php';
class menuDAO{

	public function listaoperadores(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT mr.*,r.rol_nombre FROM  tb_menu_rol mr inner join tb_rol r on mr.rol_id = r.rol_id where mr.menurol_estado = 1";
			$sql = "CALL sp_listar_operadores()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaoperadoresById($data){

		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM  tb_menu_rol WHERE menurol_id = :menurol_id";
			$sql = "CALL sp_listar_operadores_por_id(:menurol_id)";
			$database->query($sql);
			$database->bind(':menurol_id', $data['menurol_id']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function listmenuId($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_menu WHERE menu_id = :menu_id";
			$sql = "CALL sp_listar_menu(:menu_id)";
			$database->query($sql);
			$database->bind(':menu_id', $data);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listmenu(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_menu";
			$sql = "CALL sp_listar_menu(0)";

			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function listsubmenuId($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_submenu WHERE menu_id = :id AND submenu_estado = 1";
			$sql = "CALL sp_listar_submenu(:id)";
			$database->query($sql);
	        $database->bind(':id', $data);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertarmenurol($data){
		try{
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_menu_rol (rol_id, menurol_fecharegistro, menurol_fechaupdate, menurol_jsonparent, menurol_jsonchildren) VALUES (:rol_id, :menurol_fecharegistro, :menurol_fechaupdate, :menurol_jsonparent, :menurol_jsonchildren)";
			$sql = "CALL sp_menu_rol_mantenimiento(@a_message, :tipo, 0 , :rol_id, :menurol_fecharegistro, :menurol_fechaupdate, :menurol_jsonparent, :menurol_jsonchildren, '', '')";
			$database->query($sql);
			$database->bind(':tipo', 'nuevo');
	        $database->bind(':rol_id', $data['rol_id']);
	        $database->bind(':menurol_fecharegistro', date("Y/m/d h:i:s"));
	        $database->bind(':menurol_fechaupdate', date("Y/m/d h:i:s"));
	        $database->bind(':menurol_jsonparent', json_encode($data['menurol_jsonparent']));
	        $database->bind(':menurol_jsonchildren', json_encode($data['menurol_jsonchildren']));

	        $database->execute();
	        $id = $database->lastInsertId();

	        return array('status' => true, 'lastid' => $id );

		}catch(Exception $e){
			throw $e;
		}
	}
	public function actualizarmenurol($data){

		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_menu_rol SET menurol_jsonparent = :menurol_jsonparent , menurol_jsonchildren = :menurol_jsonchildren , menurol_fechaupdate = :menurol_fechaupdate WHERE rol_id = :rol_id";
			$sql = "CALL sp_menu_rol_mantenimiento(@a_message, :tipo ,'',:rol_id,'', :menurol_fechaupdate, :menurol_jsonparent, :menurol_jsonchildren, '', '')";
			$database->query($sql);
			$database->bind(':tipo', 'update');
	        $database->bind(':rol_id', $data['rol_id']);
	        $database->bind(':menurol_jsonparent', json_encode($data['menurol_jsonparent']));
	        $database->bind(':menurol_jsonchildren', json_encode($data['menurol_jsonchildren']));
	        $database->bind(':menurol_fechaupdate', date("Y/m/d h:i:s"));
	        $database->execute();
	        return true;

	    } catch (Exception $e) {
            throw $e;
        }
	}

	public function validarRol($rol_id){
		try{

			$database = new ConexionBD();
			
			//$sql = "SELECT count(*) as Total FROM tb_menu_rol WHERE menurol_estado = 1 AND rol_id=:rol_id";
			$sql = "CALL sp_validar_rol(:rol_id)";
			$database->query($sql);
	        $database->bind(':rol_id', $rol_id);
	        $database->execute();
	        return true;


		} catch(Exception $e) {

		}
	}
	
	public function actualizarmenurolMovil($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_menu_rol SET menurol_json_movil = :menurol_json_movil WHERE rol_id = :rol_id AND menurol_estado = 1";
			$sql = "CALL sp_menu_rol_mantenimiento(@a_message, :tipo , '',:rol_id, '' , '', '', '','',:menurol_json_movil)";
			$database->query($sql);
			$database->bind(':tipo', 'movil');
	        $database->bind(':rol_id', $data['rol_id']);
	        $database->bind(':menurol_json_movil', json_encode($data['menurol_json_movil']));
	        $database->execute();
	        return true;

	    } catch (Exception $e) {
            throw $e;
        }
	}
}
