<?php
require_once'db.php';
require_once'character.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class clientesDAO{

	public function listypeclientes(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM  tb_tipocliente";
			$sql = "CALL sp_listar_tipo_clientes()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function lista(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT c.*,tc.tipocliente_descripcion FROM  tb_cliente c INNER JOIN  tb_tipocliente tc on c.tipocliente_id = tc.tipocliente_id WHERE c.cliente_status = 1";
			$sql = "CALL sp_listar_clientes()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function listaSimple(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_cliente";
			$sql = "CALL sp_lista_simple_clientes(0)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function listaClienteId($clienteid){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_cliente WHERE cliente_id = :cliente_id";
			$sql = "CALL sp_lista_simple_clientes(:cliente_id)";
			$database->query($sql);
			$database->bind(':cliente_id', $clienteid);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	
	public function listOrderBy($data){

		try {
				/*$sqlin = '';

			if($data['order'] == 1){
				$sqlin = " ORDER BY cliente_calificacion ASC";
			}else{
				$sqlin = " ORDER BY cliente_calificacion DESC";
			}*/


			$database = new ConexionBD();

			//$sql = "SELECT c.*,tc.tipocliente_descripcion FROM  tb_cliente c INNER JOIN  tb_tipocliente tc on c.tipocliente_id = tc.tipocliente_id WHERE c.cliente_status = 1".$sqlin;
			$sql = "CALL sp_listar_clientes_order_by()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listBy($data){

		//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		try {
			/*	$sqlin = '';
				$limit = '';

			if($data['tipocliente_id'] != "-1" && $data['cliente_nombre'] != ""){
				$sqlin = " AND tc.tipocliente_id = :tipocliente_id AND c.cliente_nombre LIKE :cliente_nombre";
			}elseif($data['tipocliente_id'] == "-1" && $data['cliente_nombre'] != ""){
				$sqlin = " AND c.cliente_nombre LIKE :cliente_nombre";
			}elseif($data['tipocliente_id'] != "-1" && $data['cliente_nombre'] == ""){
				$sqlin = " AND tc.tipocliente_id = :tipocliente_id";
			}*/


			$database = new ConexionBD();

			/*$sql = "SELECT c.*,tc.tipocliente_descripcion FROM  tb_cliente c INNER JOIN  tb_tipocliente tc on c.tipocliente_id = tc.tipocliente_id WHERE c.cliente_status = 1".$sqlin;

			$database->query($sql);
			if($data['tipocliente_id'] != "-1"){
				$database->bind(':tipocliente_id', $data['tipocliente_id']);
			}
			if($data['cliente_nombre'] != ""){
				$database->bind(':cliente_nombre', '%'.$data['cliente_nombre'].'%');
			}*/

			if($data['cliente_nombre'] != ""){
				$var = str_replace(" ","%",$data['cliente_nombre']);
			}

			$sql = "CALL sp_listar_clientes_by(:tipocliente_id, :cliente_nombre)";
			$database->query($sql);
			$database->bind(':tipocliente_id', $data['tipocliente_id']);
			$database->bind(':cliente_nombre', $var);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertarCliente($data){	
		try{
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_cliente (cliente_nombre, cliente_apellido, cliente_correo, cliente_celular, cliente_costos, cliente_supervisor, cliente_password, tipocliente_id, empresa_id, cliente_status, cliente_calificacion,cliente_estados)VALUES (:cliente_nombre, :cliente_apellido, :cliente_correo, :cliente_celular, :cliente_costos, :cliente_supervisor, :cliente_password, :tipocliente_id, :empresa_id, :cliente_status, :cliente_calificacion,:cliente_estados)";
			$sql = "CALL sp_clientes_mantenimiento(@a_message, :tipo, :cliente_nombre, :cliente_apellido, :cliente_correo, :cliente_celular, :cliente_costos, :cliente_supervisor, :cliente_password, :tipocliente_id, :empresa_id, :cliente_status, :cliente_estados, :cliente_prueba, 0, 0, 0, 0, 0, 0)";
			$database->query($sql);
			$database->bind(':tipo','nuevo');
	        $database->bind(':cliente_nombre', SanitizeCharacter($data['cliente_nombre']));
	        $database->bind(':cliente_apellido', SanitizeCharacter($data['cliente_apellido']));
	        $database->bind(':cliente_correo', $data['cliente_correo']);
	        $database->bind(':cliente_celular', $data['cliente_celular']);
	        $database->bind(':cliente_costos', $data['cliente_costos']);
	        $database->bind(':cliente_supervisor', $data['cliente_supervisor']);
	        $database->bind(':cliente_password', $data['cliente_password']);
	        $database->bind(':tipocliente_id', $data['tipocliente_id']);
	        $database->bind(':empresa_id', $data['empresa_id']);
	        $database->bind(':cliente_status', $data['cliente_status']);
	       	//$database->bind(':cliente_calificacion', 5);

	       	/*if($data['tipocliente_id'] == '1'){
	       		$database->bind(':cliente_estados', 1);
	       	}else{
	       		$database->bind(':cliente_estados', $data['cliente_estados']);
	       	}*/
	       	$database->bind(':cliente_prueba', $data['cliente_prueba']);
	       	$database->bind(':cliente_estados', $data['cliente_estados']);
	        $database->execute();
	        //return $database->resultSet();
            echo  "resulset::".json_encode($database->resultSet());

	        $id = $database->lastInsertId();
// 
	        return array('status' => 1, 'lastid' => $id);


		}catch(Exception $e){
			throw $e;
		}
	}

	public function insertarFb($data){	
		try{
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_cliente (cliente_nombre, cliente_apellido, cliente_correo, fb_id, cliente_status, cliente_calificacion)VALUES (:cliente_nombre, :cliente_apellido, :cliente_correo, :fb_id, :cliente_status, :cliente_calificacion)";
			$sql = "CALL sp_clientes_mantenimiento(@a_message, :tipo, :cliente_nombre, :cliente_apellido, :cliente_correo, '', '', '', '', '', '', '', '', '', 0, 0, 0,0,:fb_id,0)";
			$database->query($sql);
			$database->bind(':tipo','fb');
      $database->bind(':cliente_nombre', SanitizeCharacter($data['cliente_nombre']));
      $database->bind(':cliente_apellido', SanitizeCharacter($data['cliente_apellido']));
      $database->bind(':cliente_correo', $data['cliente_correo']);
      //$database->bind(':cliente_status', 1);
      $database->bind(':fb_id', $data['fb_id']);
     	//$database->bind(':cliente_calificacion', 5);
      $database->execute();
      return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function deleteCliente($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_cliente SET cliente_status = :cliente_status WHERE cliente_id = :cliente_id";
			$sql = "CALL sp_clientes_mantenimiento(@a_message, :tipo, '', '', '', 0, '', '', '', 0, 0, 0, 0, 0, :cliente_id,0,0,0,0,0)";
			$database->query($sql);
			//$database->bind(':cliente_status', 0);
			$database->bind(':tipo','delete');
	        $database->bind(':cliente_id', $data['cliente_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function actualizarEstado($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_cliente SET cliente_estados = :cliente_estados WHERE cliente_id = :cliente_id";
			$sql = "CALL sp_clientes_mantenimiento(@a_message, :tipo, '', '', '', 0, '', '', '', 0, 0, 0, :cliente_estados, 0, :cliente_id,0,0,0,0,0)";
			$database->query($sql);
			$database->bind(':tipo','estado');
			$database->bind(':cliente_estados', $data['cliente_estados']);
	        $database->bind(':cliente_id', $data['cliente_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function updateCliente($data){
		//var_dump($data);exit;
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_cliente SET cliente_nombre = :cliente_nombre, cliente_apellido = :cliente_apellido, cliente_correo = :cliente_correo, cliente_celular = :cliente_celular, cliente_costos = :cliente_costos, cliente_supervisor = :cliente_supervisor, cliente_password = :cliente_password, tipocliente_id = :tipocliente_id, empresa_id = :empresa_id , cliente_estados = :cliente_estados WHERE cliente_id = :cliente_id";
			$sql = "CALL sp_clientes_mantenimiento(@a_message, :tipo, :cliente_nombre, :cliente_apellido, :cliente_correo, :cliente_celular, :cliente_costos, :cliente_supervisor, :cliente_password, :tipocliente_id, :empresa_id, 0 , :cliente_estados, 0, :cliente_id, 0,0,0,0,0)";
			$database->query($sql);
			$database->bind(':tipo','update');
			$database->bind(':cliente_nombre', SanitizeCharacter($data['cliente_nombre']));
	        $database->bind(':cliente_apellido', SanitizeCharacter($data['cliente_apellido']));
	        $database->bind(':cliente_correo', $data['cliente_correo']);
	        $database->bind(':cliente_celular', $data['cliente_celular']);
	        $database->bind(':cliente_costos', $data['cliente_costos']);
	        $database->bind(':cliente_supervisor', $data['cliente_supervisor']);
	        $database->bind(':cliente_password', $data['cliente_password']);
	        $database->bind(':tipocliente_id', $data['tipocliente_id']);
	        $database->bind(':empresa_id', $data['empresa_id']);
	        $database->bind(':cliente_id', $data['cliente_id']);

	        //if($data['tipocliente_id'] == '1'){
	       		//$database->bind(':cliente_estados', 1);
	       	//}else{
	       		$database->bind(':cliente_estados', $data['cliente_estados']);
	       	//}

	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateClienteExpress($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_cliente SET cliente_express = :cliente_express WHERE cliente_id = :cliente_id";
			$sql = "CALL sp_clientes_mantenimiento(@a_message, :tipo, '', '', '', '', '', '', '', '', '', '', '', '', :cliente_id,:cliente_express,0,0,0,0)";
			$database->query($sql);
			$database->bind(':tipo', 'express');
			$database->bind(':cliente_express', $data['cliente_express']);
	        $database->bind(':cliente_id', $data['cliente_id']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function filtrarClientesPorCelular($data)
    {	
        try {

            $database = new ConexionBD();
            //$sql = " SELECT * FROM tb_cliente WHERE cliente_celular like :cliente_celular OR CONCAT(cliente_nombre,' ',cliente_apellido) LIKE :cliente_celular ";
            $sql = "CALL sp_filtrar_clientes_por_celular(:cliente_celular)";
            $database->query($sql);
            $database->bind(':cliente_celular', $data['cliente_celular']);
            $database->execute();
            return $database->resultSet();

        } catch (Exception $e) {
            throw $e;
        }
    }
    public function updateCalificacion($cliente_id,$cliente_calificacion){
		try{
			$database = new ConexionBD();

			/*$sql = "UPDATE tb_cliente SET
			cliente_calificacion = :cliente_calificacion
			WHERE cliente_id = :cliente_id";*/
			$sql = "CALL sp_clientes_mantenimiento(@a_message, :tipo, '', '', '', '', '', '', '', '', '', '', '', '', :cliente_id,0,:cliente_calificacion,0,0,0)";
			$database->query($sql);
			$database->bind(':tipo', 'calificacion');
			$database->bind(':cliente_id', $cliente_id);
	        $database->bind(':cliente_calificacion', $cliente_calificacion);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateClienteToken($tipo, $token, $flag, $idCliente){
		try {

			if($flag == 1){
				if($tipo == 1){
					$a = " cliente_token_ios = null ";
					$c = " cliente_token_ios = :cliente_token_ios";
				}else{
					$a = " cliente_token_android = null ";
					$c = " cliente_token_android = :cliente_token_android";
				}	
			}else{
				if($tipo == 1){
					$a = " cliente_token_ios = :cliente_token_ios ";
					
				}else{
					$a = " cliente_token_android = :cliente_token_android ";
				}

				$c = " cliente_id = :cliente_id";
			}
			
			$database = new ConexionBD();
			$sql = "UPDATE tb_cliente SET ".$a." WHERE ".$c;
			$database->query($sql);
			if($flag == 1){
				if($tipo == 1){
					$database->bind(':cliente_token_ios', $token);
				}else{
					$database->bind(':cliente_token_android', $token);
				}	
			}else{
				if($tipo == 1){
					$database->bind(':cliente_token_ios', $token);
				}else{
					$database->bind(':cliente_token_android', $token);
				}
				$database->bind(':cliente_id', $idCliente);
			}
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function validarClienteToken($token,$tipo){
		try {

			$sqlin = '';
			if($tipo == 1){
				$sqlin = " cliente_token_ios = ':cliente_token_ios' ";
			}else{
				$sqlin = " cliente_token_android = ':cliente_token_android '";
			}

			
			$database = new ConexionBD();
			$sql = "SELECT COUNT(*) AS TOTAL FROM tb_cliente WHERE ".$sqlin;

			$database->query($sql);
			if($tipo == 1){
	       		$database->bind(':cliente_token_ios', $token);
	       	}else{
	       		$database->bind(':cliente_token_android', $token);
	       	}

	      	$database = new ConexionBD();
	       	//$sql = "CALL sp_validar_cliente_token(:cliente_token, :tipo)";
	       	$database->query($sql);
	       	$database->bind(':cliente_token', $token);
	       	$database->bind(':tipo', $tipo);
	        $database->execute();
	        return $database->resultSet();

		}catch(Exception $e){
			throw $e;
		}
	}

	public function listaBylogin($user,$password){
		try {
			$database = new ConexionBD();

			$sql = "SELECT * FROM tb_cliente WHERE cliente_correo = :cliente_correo AND cliente_password = :cliente_password AND cliente_status = 1";
			//$sql = "CALL sp_listar_clientes_por_login(:cliente_correo, :cliente_password)";
			$database->query($sql);
	        $database->bind(':cliente_correo', $user);
	        $database->bind(':cliente_password', $password);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function validarCorreo($correo){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*) as total, cliente_nombre, cliente_apellido, cliente_password FROM tb_cliente WHERE cliente_correo = :cliente_correo and cliente_status in (1)";
			$sql = "CALL sp_validar_correo_cliente(:cliente_correo)";
			$database->query($sql);
			$database->bind(':cliente_correo', $correo);
	        $database->execute();
	         return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function validarCorreoCelular($correo,$celular){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*) as total , cliente_nombre, cliente_apellido, cliente_password FROM tb_cliente WHERE cliente_correo = :cliente_correo and cliente_celular = :cliente_celular and cliente_status in (1)";
			$sql = "CALL sp_validar_correo_celular_cliente(:cliente_correo, :cliente_celular)";
			$database->query($sql);
			$database->bind(':cliente_correo', $correo);
			$database->bind(':cliente_celular', $celular);
	        $database->execute();
	        return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function validarCliente($idCliente){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*) as total, cliente_token_ios FROM tb_cliente WHERE cliente_id = :cliente_id and cliente_status in (1)";
			$sql = "CALL sp_validar_cliente(:cliente_id)";
			$database->query($sql);
			$database->bind(':cliente_id', $idCliente);
	        $database->execute();
	         return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}

	/*public function listaPaginacion($start,$per_page){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT c.*,tc.tipocliente_descripcion FROM  tb_cliente c INNER JOIN  tb_tipocliente tc on c.tipocliente_id = tc.tipocliente_id WHERE c.cliente_status = 1 limit :start , :per_page";
			$sql = "CALL sp_paginacion_clientes(:start, :per_page)";
			$database->query($sql);
			$database->bind(':start', $start);
	        $database->bind(':per_page', $per_page);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}*/

	public function listaPaginacionby($data){
		//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		try {
			$database = new ConexionBD();
			
			//$like = (!empty($data['cliente_nombre']))?" AND c.cliente_nombre LIKE :cliente_nombre ":'';

			//$sql = "SELECT c.*,tc.tipocliente_descripcion FROM  tb_cliente c INNER JOIN  tb_tipocliente tc on c.tipocliente_id = tc.tipocliente_id WHERE c.cliente_status = 1 ".$like." limit :start , :per_page";
			//echo '<pre>'; print_r($sql); echo '</pre>'; exit;
			$sql = "CALL sp_paginacion_clientes_by(:start, :per_page, :cliente_nombre)";
			$database->query($sql);
			$database->bind(':start', $data['start']);
	        $database->bind(':per_page', $data['per_page']);
	        $database->bind(':cliente_nombre',  $data['cliente_nombre']);
	        /*if(!empty($data['cliente_nombre'])){
	        	$database->bind(':cliente_nombre',  '%'.$data['cliente_nombre'].'%');
	        }*/
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaPaginacionPuntos($offset, $pagesize, $search){
		try {
			$database = new ConexionBD();
			$var = '';
			if ($search != "") {
				$var = str_replace(" ","%",$search);
			}

			//$sql = "SELECT CONCAT(c.cliente_nombre,' ',c.cliente_apellido) as Nombre, c.cliente_puntos as puntos , c.cliente_celular as celular , c.cliente_correo as email FROM  tb_cliente c INNER JOIN  tb_tipocliente tc on c.tipocliente_id = tc.tipocliente_id WHERE c.cliente_status = 1 ".$where." ORDER BY c.cliente_puntos DESC LIMIT :offset, :pagesize";
			$sql = "CALL sp_paginacion_puntos_clientes(:offset, :pagesize, :search)";
			$database->query($sql);
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
			$database->bind(':search', $var);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function CountClientes($select, $text){
		try {
			$database = new ConexionBD();
			$sqlin = '';
			if(!empty($select)){
				$sqlin = ' AND tc.tipocliente_id = :tipocliente_id ';
			}

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( CONCAT(c.cliente_nombre," ",c.cliente_apellido) LIKE :data OR c.cliente_celular LIKE :data OR c.cliente_correo LIKE :data OR c.cliente_costos LIKE :data OR c.cliente_supervisor LIKE :data OR c.cliente_calificacion LIKE :data )';
			}

			$sql = "SELECT count(*) as total FROM tb_cliente c INNER JOIN tb_tipocliente tc on c.tipocliente_id = tc.tipocliente_id WHERE c.cliente_status = 1".$sqlin." ".$like;

			$database->query($sql);
			if(!empty($select)){
				 $database->bind(':tipocliente_id', $select);
			}
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacion($offset, $pagesize, $select, $text){
		try {
			$database = new ConexionBD();
			$sqlin = '';

			if(!empty($select)){
				$sqlin = ' AND tc.tipocliente_id = :tipocliente_id ';
			}
			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( CONCAT(c.cliente_nombre," ",c.cliente_apellido) LIKE :data OR c.cliente_celular LIKE :data OR c.cliente_correo LIKE :data OR c.cliente_costos LIKE :data OR c.cliente_supervisor LIKE :data OR c.cliente_calificacion LIKE :data )';
			}

			$sql = "SELECT c.*,tc.tipocliente_descripcion FROM  tb_cliente c INNER JOIN tb_tipocliente tc on c.tipocliente_id = tc.tipocliente_id WHERE c.cliente_status = 1 ".$sqlin." ".$like." ORDER BY c.cliente_nombre DESC LIMIT :offset, :pagesize";

			$database->query($sql);
			if(!empty($select)){
				$database->bind(':tipocliente_id', $select);
			}
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}

	}
}
