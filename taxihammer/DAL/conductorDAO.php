<?php
require_once'db.php';
require_once'character.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class conductorDAO{

	public function logIn($data){
		try {

			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_conductor WHERE  conductor_correo = :user AND conductor_password = :pass AND conductor_status in (0,1)";
			$sql = "CALL sp_login_conductor(:user, :pass)";
			$database->query($sql);
	        $database->bind(':user', $data['mail']);
	        $database->bind(':pass', $data['pass']);
	        $database->execute();
	        return $database->resultSet();

    	} catch (Exception $e) {
            throw $e;
        }
	}

	public function lista(){
		try {
			$database = new ConexionBD();

			/*$sql = "SELECT a.*, b.unidad_alias, c.tiposervicio_id, c.tiposervicio_nombre FROM tb_conductor a, tb_unidad b, tb_tiposervicio c WHERE a.conductor_status = 1 AND a.unidad_id = b.unidad_id AND b.tiposervicio_id = c.tiposervicio_id";*/

			//$sql = "SELECT a.*, b.unidad_alias, c.tiposervicio_id, c.tiposervicio_nombre FROM tb_conductor a LEFT JOIN tb_unidad b ON a.unidad_id = b.unidad_id LEFT JOIN tb_tiposervicio c ON b.tiposervicio_id = c.tiposervicio_id WHERE conductor_status not in (2)";
			$sql = "CALL sp_listar_conductor_por_id(0)";

			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaBy($idconductor){

		try {
			$database = new ConexionBD();
			//$sql = "SELECT a.*, b.unidad_alias, c.tiposervicio_id, c.tiposervicio_nombre FROM tb_conductor a LEFT JOIN tb_unidad b ON a.unidad_id = b.unidad_id LEFT JOIN tb_tiposervicio c ON b.tiposervicio_id = c.tiposervicio_id WHERE conductor_status not in (2) AND a.conductor_id = :conductor_id ";
			$sql = "CALL sp_listar_conductor_por_id(:conductor_id)";
			$database->query($sql);
			$database->bind(':conductor_id', $idconductor);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaConductorId($idconductor){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_conductor where conductor_id = :conductor_id";
			$sql = "CALL sp_listar_conductor_por_id2(:conductor_id)";
			$database->query($sql);
			$database->bind(':conductor_id', $idconductor);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaSimple(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_conductor";
			$sql = "CALL sp_lista_simple_conductor()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function deleteConductor($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_conductor SET conductor_status = 2 WHERE conductor_id = :conductor_id";
			$sql = "CALL sp_conductores_mantenimiento(@a_message, :tipo, :conductor_id , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '', '', '', :conductor_status , '')";
			$database->query($sql);
			$database->bind(':tipo', 'estado');
			$database->bind(':conductor_id', $data['conductor_id']);
			$database->bind(':conductor_status', 2);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function insertarConductor($data){

		$explode = explode('string:', $data['banco_id']);
		$data['banco_id'] = $explode[1];
		//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		try{
			$database = new ConexionBD();

			/*$sql = "INSERT INTO tb_conductor 
			(conductor_nombre, conductor_apellido, conductor_correo, conductor_password, conductor_dni, conductor_celular, conductor_ciudad, conductor_distrito, conductor_licencia, conductor_catlicencia, conductor_fechabrevete, conductor_foto, conductor_status, unidad_id,conductor_comision,conductor_comision_total,conductor_fotosoat,conductor_fotocarnetvial,conductor_fechasoat,conductor_fechacarnetvial,conductor_polarizadas,conductor_cci,banco_id,conductor_fecha) 
			VALUES 
			(:conductor_nombre, :conductor_apellido, :conductor_correo, :conductor_password, :conductor_dni, :conductor_celular, :conductor_ciudad, :conductor_distrito, :conductor_licencia, :conductor_catlicencia, :conductor_fechabrevete, :conductor_foto, :conductor_status, :unidad_id, :conductor_comision, :conductor_comision_total, :conductor_fotosoat, :conductor_fotocarnetvial, :conductor_fechasoat, :conductor_fechacarnetvial, :conductor_polarizadas, :conductor_cci, :banco_id, :conductor_fecha)";*/

			$sql = "CALL sp_conductores_mantenimiento(@a_message, :tipo, 0 , :unidad_id , :conductor_nombre , :conductor_apellido , :conductor_correo , :conductor_password , :conductor_dni , :conductor_celular , :conductor_ciudad , :conductor_distrito , :conductor_licencia , :conductor_catlicencia , :conductor_fechabrevete , :conductor_foto, :conductor_comision, :conductor_comision_total, :conductor_fotosoat , :conductor_fotocarnetvial , :conductor_fechasoat , :conductor_fechacarnetvial , :conductor_polarizadas , :conductor_cci , :banco_id, :conductor_prueba, '', '', 5, 0, 0)";
			$database->query($sql);
			$database->bind(':tipo', 'nuevo');
	        $database->bind(':conductor_nombre', SanitizeCharacter($data['conductor_nombre']));
	        $database->bind(':conductor_apellido', SanitizeCharacter($data['conductor_apellido']));
	        $database->bind(':conductor_correo', $data['conductor_correo']);
	        $database->bind(':conductor_password', $data['conductor_password']);
	        $database->bind(':conductor_dni', $data['conductor_dni']);
	        $database->bind(':conductor_celular', $data['conductor_celular']);
	        $database->bind(':conductor_ciudad', $data['conductor_ciudad']);
	        $database->bind(':conductor_distrito', $data['conductor_distrito']);
	        $database->bind(':conductor_licencia', $data['conductor_licencia']);
	        $database->bind(':conductor_catlicencia', $data['conductor_catlicencia']);
	        $database->bind(':conductor_fechabrevete', $data['conductor_fechabrevete']);
	        $database->bind(':conductor_foto', $data['conductor_foto']);
	        $database->bind(':unidad_id', $data['unidad_id']);
	        /*if(!empty($data['unidad_id'])){
	        	$database->bind(':conductor_status', 1);
	        }else{
	        	$database->bind(':conductor_status', 0);
	        }*/
	        $database->bind(':conductor_comision', $data['conductor_comision']);
	        $database->bind(':conductor_comision_total', $data['conductor_comision_total']);
			$database->bind(':conductor_fotosoat',$data['conductor_fotosoat']);
			$database->bind(':conductor_fotocarnetvial' ,'');
			$database->bind(':conductor_fechasoat', $data['conductor_fechasoat']);
			$database->bind(':conductor_fechacarnetvial','');
			$database->bind(':conductor_polarizadas',$data['conductor_polarizadas']); 
			$database->bind(':conductor_cci',$data['conductor_cci']); 
			$database->bind(':banco_id',$data['banco_id']);
			$database->bind(':conductor_prueba', $data['conductor_prueba']);
			//$database->bind(':conductor_fecha', date_format( new DateTime(), 'Y-m-d H:i:s'));
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function insertarConductorWs($data){
		try{
			$database = new ConexionBD();

			/*$sql = "INSERT INTO tb_conductor 
			(conductor_nombre, conductor_apellido, conductor_correo, conductor_password, conductor_dni, conductor_celular, conductor_ciudad, conductor_distrito, conductor_licencia, conductor_catlicencia, conductor_fechabrevete, conductor_status) 
			VALUES 
			(:conductor_nombre, :conductor_apellido, :conductor_correo, :conductor_password, :conductor_dni, :conductor_celular, :conductor_ciudad, :conductor_distrito, :conductor_licencia, :conductor_catlicencia, :conductor_fechabrevete, :conductor_status)";*/
			//echo '<pre>'; print_r($data); echo '</pre>'; exit;
			$sql = "CALL sp_conductores_mantenimiento(@a_message, :tipo, 0 , 0 , :conductor_nombre , :conductor_apellido , :conductor_correo , :conductor_password , '' , :conductor_celular , '' , '' , '' , '' , '' , '', 0, 0, '' , '' , '' , '' , '' , '', 0 , 0, '', '', 0, 0, 0)";
			$database->query($sql);
			$database->bind(':tipo', 'nuevo_ws');
	        $database->bind(':conductor_nombre', SanitizeCharacter($data['conductor_nombre']));
	        $database->bind(':conductor_apellido', SanitizeCharacter($data['conductor_apellido']));
	        $database->bind(':conductor_correo', $data['conductor_correo']);
	        $database->bind(':conductor_password', $data['conductor_password']);
	        //$database->bind(':conductor_dni', $data['conductor_dni']);
	        $database->bind(':conductor_celular', $data['conductor_celular']);
	        //$database->bind(':conductor_ciudad', $data['conductor_ciudad']);
	        //$database->bind(':conductor_distrito', $data['conductor_distrito']);
	        //$database->bind(':conductor_licencia', $data['conductor_licencia']);
	        //$database->bind(':conductor_catlicencia', $data['conductor_catlicencia']);
	        //$database->bind(':conductor_fechabrevete', $data['conductor_fechabrevete']);
	        //$database->bind(':conductor_foto', $data['conductor_foto']);
	        //$database->bind(':unidad_id', $data['unidad_id']);
	        //$database->bind(':conductor_prueba', 0);
	        //$database->bind(':conductor_status', $data['conductor_status']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function insertarFb($data){
		try{
			$database = new ConexionBD();
			$sql = "INSERT INTO tb_conductor (conductor_nombre, conductor_apellido, conductor_correo, conductor_status, fb_id)
			VALUES (:conductor_nombre, :conductor_apellido, :conductor_correo, 0, :fb_id)";
			$database->query($sql);
			$database->bind(':conductor_nombre', SanitizeCharacter($data['conductor_nombre']));
	        $database->bind(':conductor_apellido', SanitizeCharacter($data['conductor_apellido']));
	        $database->bind(':conductor_correo', $data['conductor_correo']);
	        $database->bind(':fb_id', $data['fb_id']);
	        $database->execute();
	        return true;
		}catch(Exception $e){
			throw $e;
		}
	}
	public function updateConductor($data){

		if($_SESSION['trans'] != 2){
			$comision = explode("string:",$data['conductor_comision']);
			$data['conductor_comision'] = $comision[1];
		}else{
			$data['conductor_comision'] = 0;
			$data['conductor_comision_total'] = 0;
		}

		$explode = explode('string:', $data['banco_id']);
		$data['banco_id'] = $explode[1];
		
		if(!empty($data['conductor_foto']) && !empty($data['conductor_fotosoat']) && !empty($data['conductor_fotocarnetvial'])){

			$sqlFoto = "conductor_foto = :conductor_foto , conductor_fotosoat = :conductor_fotosoat, conductor_fotocarnetvial = :conductor_fotocarnetvial";

		}elseif (!empty($data['conductor_foto']) && !empty($data['conductor_fotosoat']) && empty($data['conductor_fotocarnetvial'])) {

			$sqlFoto = "conductor_foto = :conductor_foto , conductor_fotosoat = :conductor_fotosoat";

		}elseif (!empty($data['conductor_foto']) && empty($data['conductor_fotosoat']) && !empty($data['conductor_fotocarnetvial'])) {

			$sqlFoto = "conductor_foto = :conductor_foto , conductor_fotocarnetvial = :conductor_fotocarnetvial";

		}elseif (empty($data['conductor_foto']) && !empty($data['conductor_fotosoat']) && !empty($data['conductor_fotocarnetvial'])) {

			$sqlFoto = "conductor_fotosoat = :conductor_fotosoat, conductor_fotocarnetvial = :conductor_fotocarnetvial";

		}elseif (!empty($data['conductor_foto']) && empty($data['conductor_fotosoat']) && empty($data['conductor_fotocarnetvial'])) {

			$sqlFoto = "conductor_foto = :conductor_foto";

		}elseif (empty($data['conductor_foto']) && !empty($data['conductor_fotosoat']) && empty($data['conductor_fotocarnetvial'])) {

			$sqlFoto = "conductor_fotosoat = :conductor_fotosoat";

		}elseif (empty($data['conductor_foto']) && empty($data['conductor_fotosoat']) && !empty($data['conductor_fotocarnetvial'])) {

			$sqlFoto = "conductor_fotocarnetvial = :conductor_fotocarnetvial";

		}

		try{
			$database = new ConexionBD();

			$sql = "UPDATE tb_conductor SET
			unidad_id = :unidad_id, conductor_nombre = :conductor_nombre, conductor_apellido = :conductor_apellido, conductor_correo = :conductor_correo, conductor_password = :conductor_password, conductor_dni = :conductor_dni, conductor_celular = :conductor_celular, conductor_ciudad = :conductor_ciudad, conductor_distrito = :conductor_distrito, conductor_licencia = :conductor_licencia, conductor_catlicencia = :conductor_catlicencia, conductor_fechabrevete = :conductor_fechabrevete, ".$sqlFoto." , unidad_id = :unidad_id, conductor_status = :conductor_status, conductor_comision = :conductor_comision, conductor_comision_total = :conductor_comision_total, conductor_fechasoat = :conductor_fechasoat, conductor_fechacarnetvial = :conductor_fechacarnetvial , conductor_polarizadas = :conductor_polarizadas , conductor_cci = :conductor_cci, banco_id = :banco_id WHERE conductor_id = :conductor_id";
			
			$database->query($sql);
			$database->bind(':conductor_id', $data['conductor_id']);
	        $database->bind(':unidad_id', $data['unidad_id']);
	        $database->bind(':conductor_nombre', SanitizeCharacter($data['conductor_nombre']));
	        $database->bind(':conductor_apellido', SanitizeCharacter($data['conductor_apellido']));
	        $database->bind(':conductor_correo', $data['conductor_correo']);
	        $database->bind(':conductor_password', $data['conductor_password']);
	        $database->bind(':conductor_dni', $data['conductor_dni']);
	        $database->bind(':conductor_celular', $data['conductor_celular']);
	        $database->bind(':conductor_ciudad', $data['conductor_ciudad']);
	        $database->bind(':conductor_distrito', $data['conductor_distrito']);
	        $database->bind(':conductor_licencia', $data['conductor_licencia']);
	        $database->bind(':conductor_catlicencia', $data['conductor_catlicencia']);
	        $database->bind(':conductor_fechabrevete', $data['conductor_fechabrevete']);
	    if(!empty($data['conductor_foto']) && !empty($data['conductor_fotosoat']) && !empty($data['conductor_fotocarnetvial'])){

			$database->bind(':conductor_foto', $data['conductor_foto']);
			$database->bind(':conductor_fotosoat', $data['conductor_fotosoat']);
			$database->bind(':conductor_fotocarnetvial', $data['conductor_fotocarnetvial']);

		}elseif (!empty($data['conductor_foto']) && !empty($data['conductor_fotosoat']) && empty($data['conductor_fotocarnetvial'])) {

			$database->bind(':conductor_foto', $data['conductor_foto']);
			$database->bind(':conductor_fotosoat', $data['conductor_fotosoat']);

		}elseif (!empty($data['conductor_foto']) && empty($data['conductor_fotosoat']) && !empty($data['conductor_fotocarnetvial'])) {

			$database->bind(':conductor_foto', $data['conductor_foto']);
			$database->bind(':conductor_fotocarnetvial', $data['conductor_fotocarnetvial']);

		}elseif (empty($data['conductor_foto']) && !empty($data['conductor_fotosoat']) && !empty($data['conductor_fotocarnetvial'])) {

			$database->bind(':conductor_fotosoat', $data['conductor_fotosoat']);
			$database->bind(':conductor_fotocarnetvial', $data['conductor_fotocarnetvial']);

		}elseif (!empty($data['conductor_foto']) && empty($data['conductor_fotosoat']) && empty($data['conductor_fotocarnetvial'])) {

			$database->bind(':conductor_foto', $data['conductor_foto']);

		}elseif (empty($data['conductor_foto']) && !empty($data['conductor_fotosoat']) && empty($data['conductor_fotocarnetvial'])) {

			$database->bind(':conductor_fotosoat', $data['conductor_fotosoat']);

		}elseif (empty($data['conductor_foto']) && empty($data['conductor_fotosoat']) && !empty($data['conductor_fotocarnetvial'])) {

			$database->bind(':conductor_fotocarnetvial', $data['conductor_fotocarnetvial']);

		};
	        $database->bind(':unidad_id', $data['unidad_id']);
	        $database->bind(':conductor_status', $data['conductor_status']);
	        $database->bind(':conductor_comision', $data['conductor_comision']);
	        $database->bind(':conductor_comision_total', $data['conductor_comision_total']);
	        $database->bind(':conductor_fechasoat', $data['conductor_fechasoat']);
			$database->bind(':conductor_fechacarnetvial', $data['conductor_fechacarnetvial']);
    		$database->bind(':conductor_polarizadas', $data['conductor_polarizadas']);
    		$database->bind(':conductor_cci', $data['conductor_cci']);
    		$database->bind(':banco_id', $data['banco_id']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateConductorEdit($data){

		//echo '<pre>'; print_r($data); echo '</pre>';

		if($data['conductor_foto'] != '0' && $data['conductor_fotosoat'] != '0' && $data['conductor_fotocarnetvial'] != '0' ){

			$sqlFoto = "conductor_foto = :conductor_foto , conductor_fotosoat = :conductor_fotosoat, conductor_fotocarnetvial = :conductor_fotocarnetvial";

		}elseif ($data['conductor_foto'] != '0' && $data['conductor_fotosoat'] != '0' && $data['conductor_fotocarnetvial'] == '0' ) {

			$sqlFoto = "conductor_foto = :conductor_foto , conductor_fotosoat = :conductor_fotosoat";

		}elseif ($data['conductor_foto'] != '0' && $data['conductor_fotosoat'] == '0' && $data['conductor_fotocarnetvial'] != '0' ) {

			$sqlFoto = "conductor_foto = :conductor_foto , conductor_fotocarnetvial = :conductor_fotocarnetvial";

		}elseif ($data['conductor_foto'] == '0' && $data['conductor_fotosoat'] != '0' && $data['conductor_fotocarnetvial'] != '0' ) {

			$sqlFoto = "conductor_fotosoat = :conductor_fotosoat, conductor_fotocarnetvial = :conductor_fotocarnetvial";

		}elseif ($data['conductor_foto'] != '0' && $data['conductor_fotosoat'] == '0' && $data['conductor_fotocarnetvial'] == '0' ) {

			$sqlFoto = "conductor_foto = :conductor_foto";

		}elseif ($data['conductor_foto'] == '0' && $data['conductor_fotosoat'] != '0' && $data['conductor_fotocarnetvial'] == '0' ) {

			$sqlFoto = "conductor_fotosoat = :conductor_fotosoat";

		}elseif ($data['conductor_foto'] == '0' && $data['conductor_fotosoat'] == '0' && $data['conductor_fotocarnetvial'] != '0' ) {

			$sqlFoto = "conductor_fotocarnetvial = :conductor_fotocarnetvial";

		}

		//echo '<pre>'; print_r($sqlFoto); echo '</pre>'; exit('DAO');
		try{
			$database = new ConexionBD();

			$sql = "UPDATE tb_conductor SET conductor_nombre = :conductor_nombre, conductor_apellido = :conductor_apellido, conductor_correo = :conductor_correo, conductor_password = :conductor_password, conductor_dni = :conductor_dni, conductor_celular = :conductor_celular, conductor_ciudad = :conductor_ciudad, conductor_distrito = :conductor_distrito, conductor_licencia = :conductor_licencia, conductor_catlicencia = :conductor_catlicencia, conductor_fechabrevete = :conductor_fechabrevete, ".$sqlFoto." , conductor_fechasoat = :conductor_fechasoat, conductor_fechacarnetvial = :conductor_fechacarnetvial , conductor_polarizadas = :conductor_polarizadas , conductor_cci = :conductor_cci, banco_id = :banco_id WHERE conductor_id = :conductor_id";
			//echo '<pre>'; print_r($sql); echo '</pre>'; exit('DAO');
			$database->query($sql);
			$database->bind(':conductor_id', $data['conductor_id']);
	        $database->bind(':conductor_nombre', SanitizeCharacter($data['conductor_nombre']));
	        $database->bind(':conductor_apellido', SanitizeCharacter($data['conductor_apellido']));
	        $database->bind(':conductor_correo', $data['conductor_correo']);
	        $database->bind(':conductor_password', $data['conductor_password']);
	        $database->bind(':conductor_dni', $data['conductor_dni']);
	        $database->bind(':conductor_celular', $data['conductor_celular']);
	        $database->bind(':conductor_ciudad', $data['conductor_ciudad']);
	        $database->bind(':conductor_distrito', $data['conductor_distrito']);
	        $database->bind(':conductor_licencia', $data['conductor_licencia']);
	        $database->bind(':conductor_catlicencia', $data['conductor_catlicencia']);
	        $database->bind(':conductor_fechabrevete', $data['conductor_fechabrevete']);

	    if($data['conductor_foto'] != '0' && $data['conductor_fotosoat'] != '0' && $data['conductor_fotocarnetvial'] != '0' ){

			$database->bind(':conductor_foto', $data['conductor_foto']);
			$database->bind(':conductor_fotosoat', $data['conductor_fotosoat']);
			$database->bind(':conductor_fotocarnetvial', $data['conductor_fotocarnetvial']);

		}elseif ($data['conductor_foto'] != '0' && $data['conductor_fotosoat'] != '0' && $data['conductor_fotocarnetvial'] == '0' ) {

			$database->bind(':conductor_foto', $data['conductor_foto']);
			$database->bind(':conductor_fotosoat', $data['conductor_fotosoat']);

		}elseif ($data['conductor_foto'] != '0' && $data['conductor_fotosoat'] == '0' && $data['conductor_fotocarnetvial'] != '0' ) {

			$database->bind(':conductor_foto', $data['conductor_foto']);
			$database->bind(':conductor_fotocarnetvial', $data['conductor_fotocarnetvial']);

		}elseif ($data['conductor_foto'] == '0' && $data['conductor_fotosoat'] != '0' && $data['conductor_fotocarnetvial'] != '0' ) {

			$database->bind(':conductor_fotosoat', $data['conductor_fotosoat']);
			$database->bind(':conductor_fotocarnetvial', $data['conductor_fotocarnetvial']);

		}elseif ($data['conductor_foto'] != '0' && $data['conductor_fotosoat'] == '0' && $data['conductor_fotocarnetvial'] == '0' ) {

			$database->bind(':conductor_foto', $data['conductor_foto']);

		}elseif ($data['conductor_foto'] == '0' && $data['conductor_fotosoat'] != '0' && $data['conductor_fotocarnetvial'] == '0' ) {

			$database->bind(':conductor_fotosoat', $data['conductor_fotosoat']);

		}elseif ($data['conductor_foto'] == '0' && $data['conductor_fotosoat'] == '0' && $data['conductor_fotocarnetvial'] != '0' ) {

			$database->bind(':conductor_fotocarnetvial', $data['conductor_fotocarnetvial']);

		};
	       
	        $database->bind(':conductor_fechasoat', $data['conductor_fechasoat']);
    		$database->bind(':conductor_fechacarnetvial', $data['conductor_fechacarnetvial']);
    		$database->bind(':conductor_polarizadas', $data['conductor_polarizadas']);
    		$database->bind(':conductor_cci', $data['conductor_cci']);
    		$database->bind(':banco_id', $data['banco_id']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function updateConductorSinFotoEdit($data){
		//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		try{
			$database = new ConexionBD();

			//$sql = "UPDATE tb_conductor SET conductor_nombre = :conductor_nombre, conductor_apellido = :conductor_apellido, conductor_correo = :conductor_correo, conductor_password = :conductor_password, conductor_dni = :conductor_dni, conductor_celular = :conductor_celular, conductor_ciudad = :conductor_ciudad, conductor_distrito = :conductor_distrito, conductor_licencia = :conductor_licencia, conductor_catlicencia = :conductor_catlicencia, conductor_fechabrevete = :conductor_fechabrevete, conductor_fechasoat = :conductor_fechasoat, conductor_fechacarnetvial = :conductor_fechacarnetvial , conductor_polarizadas = :conductor_polarizadas , conductor_cci = :conductor_cci, banco_id = :banco_id WHERE conductor_id = :conductor_id";
			
			$sql = "CALL sp_conductores_mantenimiento(@a_message, :tipo, :conductor_id, '' , :conductor_nombre , :conductor_apellido , :conductor_correo , :conductor_password , :conductor_dni , :conductor_celular , :conductor_ciudad , :conductor_distrito , :conductor_licencia , :conductor_catlicencia , :conductor_fechabrevete , '', '', '', '' , '' , :conductor_fechasoat , :conductor_fechacarnetvial , :conductor_polarizadas , :conductor_cci , :banco_id, '', '', '', '', '', '')";
			$database->query($sql);
			$database->bind(':tipo', 'update_n_foto');
			$database->bind(':conductor_id', $data['conductor_id']);
	        $database->bind(':conductor_nombre', SanitizeCharacter($data['conductor_nombre']));
	        $database->bind(':conductor_apellido',SanitizeCharacter($data['conductor_apellido']));
	        $database->bind(':conductor_correo', $data['conductor_correo']);
	        $database->bind(':conductor_password', $data['conductor_password']);
	        $database->bind(':conductor_dni', $data['conductor_dni']);
	        $database->bind(':conductor_celular', $data['conductor_celular']);
	        $database->bind(':conductor_ciudad', $data['conductor_ciudad']);
	        $database->bind(':conductor_distrito', $data['conductor_distrito']);
	        $database->bind(':conductor_licencia', $data['conductor_licencia']);
	        $database->bind(':conductor_catlicencia', $data['conductor_catlicencia']);
	        $database->bind(':conductor_fechabrevete', $data['conductor_fechabrevete']);
	       	$database->bind(':conductor_fechasoat', $data['conductor_fechasoat']);
    		$database->bind(':conductor_fechacarnetvial', $data['conductor_fechacarnetvial']);
    		$database->bind(':conductor_polarizadas', $data['conductor_polarizadas']);
    		$database->bind(':conductor_cci', $data['conductor_cci']);
    		$database->bind(':banco_id', $data['banco_id']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateConductorSinFoto($data){
		//echo '<pre>', print_r($data);echo '</pre>';exit;
		if($_SESSION['trans'] != 2){
			$comision = explode("string:",$data['conductor_comision']);
			$data['conductor_comision'] = $comision[1];
		}else{
			$data['conductor_comision'] = 0;
			$data['conductor_comision_total'] = 0;
		}

		try{
			$database = new ConexionBD();

			$sql = "UPDATE tb_conductor SET
			unidad_id = :unidad_id, conductor_nombre = :conductor_nombre, conductor_apellido = :conductor_apellido, conductor_correo = :conductor_correo, conductor_password = :conductor_password, conductor_dni = :conductor_dni, conductor_celular = :conductor_celular, conductor_ciudad = :conductor_ciudad, conductor_distrito = :conductor_distrito, conductor_licencia = :conductor_licencia, conductor_catlicencia = :conductor_catlicencia, conductor_fechabrevete = :conductor_fechabrevete, unidad_id = :unidad_id, conductor_status = :conductor_status , conductor_comision = :conductor_comision, conductor_comision_total = :conductor_comision_total, conductor_fechasoat = :conductor_fechasoat, conductor_fechacarnetvial = :conductor_fechacarnetvial , conductor_polarizadas = :conductor_polarizadas , conductor_cci = :conductor_cci, banco_id = :banco_id WHERE conductor_id = :conductor_id";

			$database->query($sql);
			$database->bind(':conductor_id', $data['conductor_id']);
	        $database->bind(':unidad_id', $data['unidad_id']);
	        $database->bind(':conductor_nombre', SanitizeCharacter($data['conductor_nombre']));
	        $database->bind(':conductor_apellido', SanitizeCharacter($data['conductor_apellido']));
	        $database->bind(':conductor_correo', $data['conductor_correo']);
	        $database->bind(':conductor_password', $data['conductor_password']);
	        $database->bind(':conductor_dni', $data['conductor_dni']);
	        $database->bind(':conductor_celular', $data['conductor_celular']);
	        $database->bind(':conductor_ciudad', $data['conductor_ciudad']);
	        $database->bind(':conductor_distrito', $data['conductor_distrito']);
	        $database->bind(':conductor_licencia', $data['conductor_licencia']);
	        $database->bind(':conductor_catlicencia', $data['conductor_catlicencia']);
	        $database->bind(':conductor_fechabrevete', $data['conductor_fechabrevete']);
	        $database->bind(':unidad_id', (empty($data['unidad_id']))?NULL:$data['unidad_id']);
	        $database->bind(':conductor_status', $data['conductor_status']);
	        $database->bind(':conductor_comision', $data['conductor_comision']);
	        $database->bind(':conductor_comision_total', $data['conductor_comision_total']);
	    	$database->bind(':conductor_fechasoat', $data['conductor_fechasoat']);
    		$database->bind(':conductor_fechacarnetvial', $data['conductor_fechacarnetvial']);
    		$database->bind(':conductor_polarizadas', $data['conductor_polarizadas']);
    		$database->bind(':conductor_cci', $data['conductor_cci']);
    		$database->bind(':banco_id', $data['banco_id']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function listBy($data){
		try{
			$database = new ConexionBD();
			/*if($data['tiposervicio_id'] != "" && $data['conductor_nombre'] != ""){
				$sql = "SELECT a.*, b.unidad_alias, c.tiposervicio_nombre FROM tb_conductor a, tb_unidad b, tb_tiposervicio c WHERE a.conductor_status = 1 AND a.unidad_id = b.unidad_id AND b.tiposervicio_id = c.tiposervicio_id AND b.tiposervicio_id = :tiposervicio_id AND a.conductor_nombre LIKE :conductor_nombre";
			}elseif($data['tiposervicio_id'] == "" && $data['conductor_nombre'] != ""){
				$sql = "SELECT a.*, b.unidad_alias, c.tiposervicio_nombre FROM tb_conductor a, tb_unidad b, tb_tiposervicio c WHERE a.conductor_status = 1 AND a.unidad_id = b.unidad_id AND b.tiposervicio_id = c.tiposervicio_id AND a.conductor_nombre LIKE :conductor_nombre";
			}elseif($data['tiposervicio_id'] != "" && $data['conductor_nombre'] == ""){
				$sql = "SELECT a.*, b.unidad_alias, c.tiposervicio_nombre FROM tb_conductor a, tb_unidad b, tb_tiposervicio c WHERE a.conductor_status = 1 AND a.unidad_id = b.unidad_id AND b.tiposervicio_id = c.tiposervicio_id AND b.tiposervicio_id = :tiposervicio_id";
			}else{
				$sql = "SELECT a.*, b.unidad_alias, c.tiposervicio_nombre FROM tb_conductor a, tb_unidad b, tb_tiposervicio c WHERE a.conductor_status = 1 AND a.unidad_id = b.unidad_id AND b.tiposervicio_id = c.tiposervicio_id";
			}
			$database->query($sql);
			if($data['tiposervicio_id'] != ""){
				$database->bind(':tiposervicio_id', $data['tiposervicio_id']);
			}
			if($data['conductor_nombre'] != ""){
				$database->bind(':conductor_nombre', '%'.$data['conductor_nombre'].'%');
			}*/
			$sql = "CALL sp_listar_conductor_por(:tiposervicio_id, :conductor_nombre)";
			$database->bind(':tiposervicio_id', $data['tiposervicio_id']);
			$database->bind(':conductor_nombre', '%'.$data['conductor_nombre'].'%');
	        $database->execute();
	        return $database->resultSet();
		}catch(Exception $e){
			throw $e;
		}
	}

	public function filtrarConductorPorCelular($data){	
        try {
        	$var = str_replace(" ","%",$data['conductor_celular']);
            $database = new ConexionBD();
            //$sql = " SELECT * FROM tb_conductor WHERE conductor_celular like :conductor_celular OR CONCAT(conductor_nombre,' ',conductor_apellido) LIKE :conductor_celular ";
            $sql = "CALL sp_filtrar_conductores_por_celular(:conductor_celular)";
            $database->query($sql);
            $database->bind(':conductor_celular', '%'.$var.'%');
            $database->execute();
            return $database->resultSet();

        } catch (Exception $e) {
            throw $e;
        }
    }

	public function updateCoordinates($data){
		try{
			$database = new ConexionBD();

			/*$sql = "UPDATE tb_conductor SET
			conductor_lat = :conductor_lat, conductor_lng = :conductor_lng 
			WHERE conductor_id = :conductor_id";*/

			$sql = "CALL sp_conductores_mantenimiento(@a_message, :tipo, :conductor_id , '' , '' , '' , '' , '' , '' , '', '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '', :conductor_lat, :conductor_lng, '', '', '')";
			
			$database->query($sql);
			$database->bind(':tipo', 'coordenadas');
			$database->bind(':conductor_id', $data['conductor_id']);
	        $database->bind(':conductor_lat', $data['conductor_lat']);
	        $database->bind(':conductor_lng', $data['conductor_lng']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateCalificacion($conductor_id,$conductor_calificacion){
		try{
			$database = new ConexionBD();

			/*$sql = "UPDATE tb_conductor SET
			conductor_calificacion = :conductor_calificacion
			WHERE conductor_id = :conductor_id";*/
			
			$sql = "CALL sp_conductores_mantenimiento(@a_message, :tipo, :conductor_id , 0 , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , 0 , 0 , '' , '' , '' , '' , '' , '' , 0 , 0 , '', '', :conductor_calificacion, 0, 0)";
			$database->query($sql);
			$database->bind(':tipo', 'calificacion');
			$database->bind(':conductor_id', $conductor_id);
	        $database->bind(':conductor_calificacion', $conductor_calificacion);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function actualizarEstado($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_conductor SET conductor_status = :conductor_status WHERE conductor_id = :conductor_id";
			$sql = "CALL sp_conductores_mantenimiento(@a_message, :tipo, :conductor_id , 0 , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , 0 , 0 , '' , '' , '' , '' , '' , '' , 0 , 0 , '', '', 0, :conductor_status, 0)";
			$database->query($sql);
			$database->bind(':tipo', 'estado');
			$database->bind(':conductor_status', $data['conductor_status']);
	        $database->bind(':conductor_id', $data['conductor_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}
	
	public function validUnidadConductor($conductor_id){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT unidad_id FROM tb_conductor WHERE conductor_id = :conductor_id";
			$sql = "CALL sp_listar_conductor_por_id2(:conductor_id)";
			$database->query($sql);
			$database->bind(':conductor_id', $conductor_id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function updateUnidadConductor($conductor_id){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_conductor SET unidad_id = null WHERE conductor_id = :conductor_id";
			$sql = "CALL sp_conductores_mantenimiento(@a_message, :tipo, :conductor_id , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '', '', '', '', '')";
			$database->query($sql);
			$database->bind(':tipo', 'unidad');
	        $database->bind(':conductor_id', $conductor_id);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function findAllConductor($conductor_id){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_conductor WHERE conductor_id = :conductor_id";
			$sql = "CALL sp_listar_conductor_por_id2(:conductor_id)";
			$database->query($sql);
	        $database->bind(':conductor_id', $conductor_id);
	        $database->execute();
	         return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function findAllConductorSelect($data){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_conductor c, tb_unidad u WHERE c.unidad_id = u.unidad_id AND c.conductor_socket IS NOT NULL AND u.tiposervicio_id = :tiposervicio_id";
			$sql = "CALL sp_find_conductor_select(:tiposervicio_id)";
			$database->query($sql);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->execute();
	         return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function actualizarMontoRecarga($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_conductor SET conductor_recarga = :conductor_recarga WHERE conductor_id = :conductor_id";
			$sql = "CALL sp_conductores_mantenimiento(@a_message, :tipo, :conductor_id , 0 , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , 0 , 0 , '' , '' , '' , '' , '' , '' , 0 , 0 , '', '', 0, 0, :conductor_recarga)";
			$database->query($sql);
			$database->bind(':tipo', 'recarga');
			$database->bind(':conductor_recarga', $data['conductor_recarga']);
	        $database->bind(':conductor_id', $data['conductor_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listardistincConductor($search){
		try {

			$search_where = "";
			if(!empty($search)){
				$search_where = "AND CONCAT(c.conductor_nombre,' ',c.conductor_apellido) LIKE '%{$search}%' ";
			}
			

			$database = new ConexionBD();
			$sql = "SELECT DISTINCT ec.conductor_id FROM tb_estado_cuenta ec INNER JOIN tb_conductor c ON ec.conductor_id =c.conductor_id WHERE c.conductor_status not in (2) AND c.unidad_id IS NOT NULL  {$search_where}";
			$database->query($sql);
	        $database->execute();
	         return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaBylogin($user,$password){
		try {
			$database = new ConexionBD();

			$sql = "SELECT a.*,b.unidad_alias, c.tiposervicio_id, c.tiposervicio_nombre,DATE_FORMAT(conductor_fechabrevete , '%d/%m/%Y') AS conductor_fechabrevete FROM tb_conductor a LEFT JOIN tb_unidad b ON a.unidad_id = b.unidad_id LEFT JOIN tb_tiposervicio c ON b.tiposervicio_id = c.tiposervicio_id WHERE conductor_correo = :conductor_correo AND conductor_password = :conductor_password AND conductor_status = 1";
			//$sql = "CALL sp_listar_conductor_login(:conductor_correo, :conductor_password)";
			$database->query($sql);
	        $database->bind(':conductor_correo', $user);
	        $database->bind(':conductor_password', $password);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function validarCorreo($correo){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*) as total, conductor_nombre, conductor_apellido, conductor_password FROM tb_conductor WHERE conductor_correo = :conductor_correo and conductor_status in (0,1)";
			$sql = "CALL sp_validar_correo(:conductor_correo)";
			$database->query($sql);
			$database->bind(':conductor_correo', $correo);
	        $database->execute();
	         return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function validarCorreoCelular($correo,$celular){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*) as total, conductor_nombre, conductor_apellido, conductor_password FROM tb_conductor WHERE conductor_correo = :conductor_correo and conductor_celular = :conductor_celular and conductor_status in (0,1)";
			$sql = "CALL sp_validar_correo_celular_conductor(:conductor_correo, :conductor_celular)";
			$database->query($sql);
			$database->bind(':conductor_correo', $correo);
			$database->bind(':conductor_celular', $celular);
	        $database->execute();
	         return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function CountConductores($where){
		try {
			$database = new ConexionBD();

			$var = str_replace(" ","%",$where);
			//$sql = "SELECT count(*) as total FROM tb_conductor a LEFT JOIN tb_unidad b ON a.unidad_id = b.unidad_id LEFT JOIN tb_tiposervicio c ON b.tiposervicio_id = c.tiposervicio_id WHERE conductor_status not in (2) ".$where;
			$sql = "CALL sp_count_conductores(:data)";

			$database->query($sql);
			$database->bind(':data', $var);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacion($offset, $pagesize, $search){

		try {

			$database = new ConexionBD();

				/*if ($search != "") {
				    $where = " AND concat(a.conductor_nombre,'',a.conductor_apellido) LIKE '%" . $search . "%'";
				} else {
				    $where = "";
				}*/

			//$sql = "SELECT a.conductor_id, a.conductor_correo, a.conductor_apellido, a.conductor_nombre, a.conductor_foto, a.conductor_celular, a.conductor_status, a.conductor_estado, a.conductor_calificacion, b.unidad_alias, c.tiposervicio_nombre, b.unidad_id FROM tb_conductor a LEFT JOIN tb_unidad b ON a.unidad_id = b.unidad_id LEFT JOIN tb_tiposervicio c ON b.tiposervicio_id = c.tiposervicio_id WHERE conductor_status not in (2) ".$where." ORDER BY a.conductor_nombre ASC LIMIT :offset, :pagesize";

            //OLD
            //$sql = "CALL sp_paginacion_conductores(:offset, :pagesize, :search)";
            //SE CREO EL SP PARA QUE ORDENE SEGUN EL ID DEL CONDUCTOR
            $sql = "CALL sp_paginacion_conductores_by_vehiculo(:offset, :pagesize, :search)";

			$database->query($sql);
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
			$database->bind(':search', $search);
	        $database->execute();

	        /*
            $Res =$database->resultSet();
            foreach ($Res as $key => $value) {
                echo $key."::".json_encode($value);
            }*/

	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacionPuntos($offset, $pagesize, $search){

		try {

			$database = new ConexionBD();
				$var = '';
				if ($search != "") {
					$var = str_replace(" ","%",$search);
				}

			//$sql = "SELECT concat(a.conductor_nombre,' ',a.conductor_apellido) as Nombre , a.conductor_celular as celular , a.conductor_puntos as puntos, a.conductor_correo as email FROM tb_conductor a LEFT JOIN tb_unidad b ON a.unidad_id = b.unidad_id LEFT JOIN tb_tiposervicio c ON b.tiposervicio_id = c.tiposervicio_id WHERE conductor_status not in (2) ".$where." GROUP BY a.conductor_id ORDER BY a.conductor_puntos DESC LIMIT :offset, :pagesize";
			$sql = "CALL sp_paginacion_conductores_puntos(:offset, :pagesize, :search)";
			$database->query($sql);
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
			$database->bind(':search', $var);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacionModal($conductor_id){

		try {
			$database = new ConexionBD();

			//$sql = "SELECT a.*, b.unidad_alias, c.tiposervicio_id, c.tiposervicio_nombre FROM tb_conductor a LEFT JOIN tb_unidad b ON a.unidad_id = b.unidad_id LEFT JOIN tb_tiposervicio c ON b.tiposervicio_id = c.tiposervicio_id WHERE conductor_status not in (2) AND a.conductor_id = :conductor_id";
			$sql = "CALL sp_paginacion_modal(:conductor_id)";
			$database->query($sql);
			$database->bind(':conductor_id', $conductor_id);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

}
