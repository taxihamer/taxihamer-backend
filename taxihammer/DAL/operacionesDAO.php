<?php
require_once'db.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class operacionesDAO{

	public function insertCta($data){
			//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		try {
			$database = new ConexionBD();

			//$fechaHasta = trim($data['cta_fecha']);

			//$sql = "INSERT INTO tb_estado_cuenta (conductor_id, recarga_id, cta_tipo, cta_fecha, cta_monto, cta_check) VALUES (:conductor_id, :recarga_id, :cta_tipo, :cta_fecha, :cta_monto, :cta_check)";
			$sql = "CALL sp_estado_cuenta_mantenimiento(@a_message, :tipo, 0 , :recarga_id , 0 , :cta_tipo, :cta_fecha , :cta_monto ,:conductor_id, :cta_check, 0)";
			$database->query($sql);
			$database->bind(':tipo', 'nuevo');
	        $database->bind(':conductor_id', $data['conductor_id']);
	        $database->bind(':recarga_id', $data['recarga_id']);
	        $database->bind(':cta_tipo', $data['cta_tipo']);
	        $database->bind(':cta_fecha', $data['cta_fecha']);
	       	$database->bind(':cta_monto', $data['cta_monto']);
	        $database->bind(':cta_check',$data['cta_check']);

	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateCta($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_estado_cuenta SET cta_check = :cta_check WHERE recarga_id = :recarga_id";
			$sql = "CALL sp_estado_cuenta_mantenimiento(@a_message, :tipo, 0 , :recarga_id , 0 , 0 , NOW() , 0 , 0 , :cta_check, 0)";
			$database->query($sql);
			$database->bind(':tipo', 'update');
			$database->bind(':cta_check', $data['recarga_check']);
	        $database->bind(':recarga_id', $data['recarga_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listarCta($data){
		try{
			$database = new ConexionBD();  
			//$sql = "SELECT * FROM tb_estado_cuenta WHERE conductor_id = :conductor_id AND cta_check = 1 AND date(cta_fecha) BETWEEN :cta_fechaInicio AND :cta_fechaFinal";
			$sql = "CALL sp_listar_estado_cuenta_fechas(:cta_fechaInicio, :cta_fechaFinal, :conductor_id, :inicio, :limite)";
			$database->query($sql);
			$database->bind(':cta_fechaInicio', $data['cta_fechaInicio']);
			$database->bind(':cta_fechaFinal', $data['cta_fechaFinal']);
			$database->bind(':conductor_id', $data['conductor_id']);
			$database->bind(':inicio', $data['inicio']);
			$database->bind(':limite', $data['limite']);
			$database->execute();
			return $database->resultSet();
			
		} catch(Exception $e){
			throw $e;
		}
	}
}