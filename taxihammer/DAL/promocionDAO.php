<?php
require_once'db.php';
require_once'character.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class promocionDAO{

	public function listaGral(){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT a.*, b.* FROM tb_promocion a, tb_promocion_detalle b WHERE promo_estado = 1";
			//$sql = "SELECT a.* FROM tb_promocion a WHERE a.promo_estado = 1";
			$sql = "CALL sp_listar_promociones()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaPromocionesBy($promo_id){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT a.*, b.* FROM tb_promocion a, tb_promocion_detalle b WHERE promo_estado = 1";
			$sql = "SELECT a.* FROM tb_promocion a WHERE a.promo_estado = 1 AND promo_id = :promo_id";
			$database->query($sql);
			$database->bind(':promo_id', $promo_id);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaCli($cliente_id){
		try {
			$database = new ConexionBD();
			//promocion_tipo 1 = CLiente, promo_estado 1 = activo.
			//$sql = "SELECT a.*, (SELECT COUNT(*) FROM tb_promocion_detalle WHERE promo_id = a.promo_id AND usuario_id = :cliente_id AND detalle_tipo = 1) AS promo_canjeado FROM tb_promocion a WHERE NOW() BETWEEN a.promo_finicio AND a.promo_fvigencia AND a.promo_estado = 1 AND a.promo_cantidad > 0 AND a.promo_tipo = 1";

				/*$sql = "SELECT a.* FROM tb_promocion a
						WHERE DATE_FORMAT(NOW(),'%Y-%m-%d') >= DATE_FORMAT(a.promo_finicio,'%Y-%m-%d')
						AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= DATE_FORMAT(a.promo_fvigencia,'%Y-%m-%d')
						AND a.promo_estado = 1 AND a.promo_cantidad > 0  AND a.promo_stock != 0 AND a.promo_tipo = 1 AND a.promo_flag = 1";*/
			$sql = "CALL sp_listar_promociones_por_tipo(1)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}
	public function listaCond($conductor_id){
		try {
			$database = new ConexionBD();
			//promocion_tipo 2 = Conductor, promo_estado 1 = activo.
			//$sql = "SELECT a.*, (SELECT COUNT(*) FROM tb_promocion_detalle WHERE promo_id = a.promo_id AND usuario_id = :conductor_id AND detalle_tipo = 2) AS promo_canjeado FROM tb_promocion a WHERE NOW() BETWEEN a.promo_finicio AND a.promo_fvigencia AND a.promo_estado = 1 AND a.promo_cantidad > 0 AND a.promo_tipo = 2";
			/*$sql = "SELECT a.* FROM tb_promocion a
					WHERE DATE_FORMAT(NOW(),'%Y-%m-%d') >= DATE_FORMAT(a.promo_finicio,'%Y-%m-%d')
					AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= DATE_FORMAT(a.promo_fvigencia,'%Y-%m-%d')
					AND a.promo_estado = 1 AND a.promo_cantidad > 0  AND a.promo_stock != 0 AND a.promo_tipo = 2 AND a.promo_flag = 1";*/
			$sql = "CALL sp_listar_promociones_por_tipo(2)";
			$database->query($sql);
			$database->bind(':conductor_id', $conductor_id);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}
	public function obtenerPuntosConductor($idConductor){
		try {
			$database = new ConexionBD();
			//promocion_tipo 2 = Conductor, promo_estado 1 = activo.
			//$sql = "SELECT conductor_puntos FROM tb_conductor WHERE conductor_id = :conductor_id";
			$sql = "CALL sp_puntos_conductor(:conductor_id)";
			$database->query($sql);
			$database->bind(':conductor_id', $idConductor);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}
	public function obtenerPuntosCliente($idCliente){
		try {
			$database = new ConexionBD();
			//promocion_tipo 1 = Cliente, promo_estado 1 = activo.
			//$sql = "SELECT cliente_puntos FROM tb_cliente WHERE cliente_id = :cliente_id";
			$sql = "CALL sp_puntos_cliente(:cliente_id)";
			$database->query($sql);
			$database->bind(':cliente_id', $idCliente);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}
	public function obtenerPuntosPromocion($promoId){
		try {
			$database = new ConexionBD();
			//promocion_tipo 2 = Conductor, promo_estado 1 = activo.
			//$sql = "SELECT promo_puntos, promo_cantidad, promo_flag , promo_stock FROM tb_promocion WHERE promo_id = :promo_id";
			$sql = "CALL sp_puntos_promocion(:promo_id)";
			$database->query($sql);
			$database->bind(':promo_id', $promoId);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}
	public function debitarPuntosCond($data){
		try {
			$res = $this->obtenerPuntosConductor($data['conductor_id']);

			$puntos_conductor = $res[0]['conductor_puntos'];

			$result = $this->obtenerPuntosPromocion($data['promo_id']);
			$puntos_promo = $result[0]['promo_puntos'];
			$cantidad_promo = $result[0]['promo_cantidad'];
			$stock = $result[0]['promo_stock'];
			$flag = $result[0]['promo_flag'];

			if($puntos_conductor >= $puntos_promo && $cantidad_promo > 0 && $stock != 0){
				$dataInsert['promo_id'] = $data['promo_id'];
				$dataInsert['usuario_id'] = $data['conductor_id'];
				$dataInsert['detalle_tipo'] = 2;
				$resInsert = $this->insert2($dataInsert);
				$resUpdate = $this->updateStockPromocion($data['promo_id']);
				if($flag != 1){
					$resFlag = $this->updatePromocionActivo($data['promo_id']);	
				}
				$database = new ConexionBD();
				$puntosActualConductor = $puntos_conductor - $puntos_promo;
				//promocion_tipo 2 = Conductor, promo_estado 1 = activo.
				$sql = "UPDATE tb_conductor SET conductor_puntos = :conductor_puntos WHERE conductor_id = :conductor_id";

				$database->query($sql);
				$database->bind(':conductor_puntos', $puntosActualConductor);
				$database->bind(':conductor_id', $data['conductor_id']);
		        $database->execute();
		        return true;
			}else{
				return false;
			}

	    } catch (Exception $e) {
        	throw $e;
        }
	}
	public function debitarPuntosCli($data){
		try {
			$res = $this -> obtenerPuntosCliente($data['cliente_id']);
			$puntos_cliente = $res[0]['cliente_puntos'];

			$result = $this -> obtenerPuntosPromocion($data['promo_id']);
			$puntos_promo = $result[0]['promo_puntos'];
			$cantidad_promo = $result[0]['promo_cantidad'];
			$stock = $result[0]['promo_stock'];
			$flag = $result[0]['promo_flag'];

			if($puntos_cliente >= $puntos_promo && $cantidad_promo > 0 && $stock != 0){

				$dataInsert['promo_id'] = $data['promo_id'];
				$dataInsert['usuario_id'] = $data['cliente_id'];
				$dataInsert['detalle_tipo'] = 1;
				$resInsert = $this-> insert2($dataInsert);
				$resUpdate = $this-> updateStockPromocion($data['promo_id']);
				if($flag != 1){
					$resFlag = $this->updatePromocionActivo($data['promo_id']);	
				}
				$database = new ConexionBD();
				$puntosActualCLiente = $puntos_cliente - $puntos_promo;
				//echo '<pre>'; print_r($puntosActualCLiente);echo '</pre>'; exit;
				//promocion_tipo 2 = Conductor, promo_estado 1 = activo.
				$sql = "UPDATE tb_cliente SET cliente_puntos = :cliente_puntos  WHERE cliente_id = :cliente_id";

				$database->query($sql);
				$database->bind(':cliente_puntos', $puntosActualCLiente);
				$database->bind(':cliente_id', $data['cliente_id']);
		        $database->execute();
		        return true;
			}else{
				return false;
			}

	    } catch (Exception $e) {
        	throw $e;
        }
	}
	public function insert($data){
		//echo '<pre>'; print_r($data); echo '</pre>';exit;
		try {
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_promocion (promo_imagen, promo_puntos, promo_nombre, promo_finicio, promo_fvigencia, promo_cantidad, promo_estado, promo_tipo, promo_stock) VALUES (:promo_imagen, :promo_puntos, :promo_nombre, :promo_finicio, :promo_fvigencia, :promo_cantidad, :promo_estado, :promo_tipo, :promo_stock)";
			$sql = "CALL sp_promociones_mantenimiento(@a_message, :tipo, 0 , :promo_imagen, :promo_puntos, :promo_nombre, :promo_finicio, :promo_fvigencia, :promo_cantidad, :promo_estado, :promo_tipo, :promo_stock, '')";
			$database->query($sql);
			$database->bind(':tipo', 'nuevo');
			$database->bind(':promo_imagen', $data['promo_imagen']);
			$database->bind(':promo_puntos', $data['promo_puntos']);
			$database->bind(':promo_nombre', SanitizeCharacter($data['promo_nombre']));
			$database->bind(':promo_finicio', $data['promo_finicio']);
			$database->bind(':promo_fvigencia', $data['promo_fvigencia']);
			$database->bind(':promo_cantidad', $data['promo_cantidad']);
			$database->bind(':promo_tipo', $data['promo_tipo']);
			$database->bind(':promo_estado', 1);
			$database->bind(':promo_stock', $data['promo_cantidad']);
			$database->execute();
		    return true;
		} catch (Exception $e) {
        	throw $e;
        }
	}
	public function insert2($data){
		try {
			$database = new ConexionBD();
			$sql = "INSERT INTO tb_promocion_detalle (promo_id, usuario_id, detalle_tipo, detalle_estado, detalle_fecha) VALUES (:promo_id, :usuario_id, :detalle_tipo, :detalle_estado, :detalle_fecha)";
			$database->query($sql);
			$database->bind(':promo_id', $data['promo_id']);
			$database->bind(':usuario_id', $data['usuario_id']);
			$database->bind(':detalle_tipo', $data['detalle_tipo']);
			$database->bind(':detalle_estado', 1);
			$database->bind(':detalle_fecha', date("Y/m/d H:i:s"));
			$database->execute();
		    return true;
		} catch (Exception $e) {
        	throw $e;
        }
	}

	public function updateStockPromocion($promo_id){
		try {
			$database = new ConexionBD();
			$sql = "UPDATE tb_promocion SET promo_stock = promo_stock - 1 WHERE promo_id = :promo_id";
			$database->query($sql);
			$database->bind(':promo_id', $promo_id);
			$database->execute();
		    return true;
		} catch (Exception $e) {
        	throw $e;
        }
	}

	public function updatePromocion($data){
		//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		try {
			$database = new ConexionBD();
			$imagen = '';
			if(!empty($data['promo_imagen'])){
				$imagen = ', promo_imagen = :promo_imagen';
			}

			$sql = "UPDATE tb_promocion SET promo_nombre = :promo_nombre, promo_tipo = :promo_tipo, promo_finicio = :promo_finicio, promo_fvigencia = :promo_fvigencia, promo_puntos = :promo_puntos, promo_cantidad = :promo_cantidad, promo_stock = :promo_stock". $imagen ." WHERE promo_id = :promo_id";
			$database->query($sql);
			$database->bind(':promo_id', $data['promo_id']);
			$database->bind(':promo_nombre', SanitizeCharacter($data['promo_nombre']));
			$database->bind(':promo_tipo', $data['promo_tipo']);
			$database->bind(':promo_finicio', $data['promo_finicio']);
			$database->bind(':promo_fvigencia', $data['promo_fvigencia']);
			$database->bind(':promo_puntos', $data['promo_puntos']);
			$database->bind(':promo_cantidad', $data['promo_cantidad']);
			$database->bind(':promo_stock', $data['promo_stock']);
			if(!empty($data['promo_imagen'])){
				$database->bind(':promo_imagen', $data['promo_imagen']);
			}
			$database->execute();
		    return true;
		} catch (Exception $e) {
        	throw $e;
        }
	}
	public function listBy($data){
		//echo '<pre>'; print_r($data); echo '</pre>';
		try{
			$database = new ConexionBD();
			/*if($data['promo_tipo'] != 0 && $data['promo_nombre'] != ""){
				$sql = "SELECT a.* FROM tb_promocion a WHERE a.promo_estado = 1 AND a.promo_tipo = :promo_tipo AND a.promo_nombre LIKE :promo_nombre";
			}elseif($data['promo_tipo'] == 0 && $data['promo_nombre'] != ""){
				$sql = "SELECT a.* FROM tb_promocion a WHERE a.promo_estado = 1 AND a.promo_nombre LIKE :promo_nombre";
			}elseif($data['promo_tipo'] != 0 && $data['promo_nombre'] == ""){
				$sql = "SELECT a.* FROM tb_promocion a WHERE a.promo_estado = 1 AND a.promo_tipo = :promo_tipo";
			}else{
				$sql = "SELECT a.* FROM tb_promocion a WHERE a.promo_estado = 1";
			}*/

			$sql = "CALL sp_listar_promociones_by(:promo_tipo, :promo_nombre)";
			$database->query($sql);
			$database->bind(':promo_tipo', $data['promo_tipo']);
			$database->bind(':promo_nombre', $data['promo_nombre']);

			/*if($data['promo_tipo'] != ""){
				$database->bind(':promo_tipo', $data['promo_tipo']);
			}
			if($data['promo_nombre'] != ""){
				$database->bind(':promo_nombre', '%'.$data['promo_nombre'].'%');
			}*/
	        $database->execute();
	        return $database->resultSet();
		}catch(Exception $e){
			throw $e;
		}
	}

	public function listByCanje($data){
		//echo '<pre>'; print_r($data); echo '</pre>';exit('saul');
		try{
			$database = new ConexionBD();
			/*if($data['detalle_tipo'] != 0 && $data['promo_nombre'] != ""){

				$sql = "SELECT a.*,d.* FROM tb_promocion_detalle d INNER JOIN  tb_promocion a ON a.promo_id = d.promo_id WHERE a.promo_estado = 1 AND d.detalle_tipo = :detalle_tipo AND a.promo_nombre LIKE :promo_nombre";

			}elseif($data['detalle_tipo'] == 0 && $data['promo_nombre'] != ""){
				$sql = "SELECT a.*,d.* FROM tb_promocion_detalle d INNER JOIN  tb_promocion a ON d.promo_id = a.promo_id WHERE a.promo_estado = 1 AND a.promo_nombre LIKE :promo_nombre";

			}elseif($data['detalle_tipo'] != 0 && $data['promo_nombre'] == ""){

				$sql = "SELECT a.*,d.* FROM tb_promocion_detalle d INNER JOIN  tb_promocion a ON d.promo_id = a.promo_id WHERE a.promo_estado = 1 AND d.detalle_tipo = :detalle_tipo";
			}else{
				$sql = "SELECT a.*,b.* FROM tb_promocion_detalle a INNER JOIN  tb_promocion b ON a.promo_id = b.promo_id WHERE a.detalle_estado = 1";
			}*/

			$sql = "CALL sp_listar_promociones_por_canje(:detalle_tipo, :promo_nombre)";
			$database->query($sql);
			$database->bind(':detalle_tipo', $data['detalle_tipo']);
			$database->bind(':promo_nombre', $data['promo_nombre']);

			/*if($data['detalle_tipo'] != ""){
				$database->bind(':detalle_tipo', $data['detalle_tipo']);
			}
			if($data['promo_nombre'] != ""){
				$database->bind(':promo_nombre', '%'.$data['promo_nombre'].'%');
			}*/
	        $database->execute();
	        return $database->resultSet();
		}catch(Exception $e){
			throw $e;
		}
	}

	public function updatePromocionDelete($promo_id){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_promocion SET promo_estado = 0 WHERE promo_id = :promo_id";
			$sql = "CALL sp_promociones_mantenimiento(@a_message, :tipo, :promo_id , '', '', '', '', '', '', :promo_estado , '', '', '')";
			$database->query($sql);
			$database->bind(':tipo', 'estado');
			$database->bind(':promo_estado', 1);
			$database->bind(':promo_id', $promo_id);
			$database->execute();
		    return true;
		} catch (Exception $e) {
        	throw $e;
        }
	}

	public function updatePromocionActivo($promo_id){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_promocion SET promo_flag = 1 WHERE promo_id = :promo_id";
			$sql = "CALL sp_promociones_mantenimiento(@a_message, :tipo, :promo_id , '', '', '', '', '', '', '' , '', '', :promo_flag)";
			$database->query($sql);
			$database->bind(':tipo', 'flag');
			$database->bind(':promo_flag', $data['promo_flag']);
			$database->bind(':promo_id', $promo_id);
			$database->execute();
		    return true;
		} catch (Exception $e) {
        	throw $e;
        }
	}

	public function actualizarEstado($data){
		try {
			$database = new ConexionBD();
			$sql = "UPDATE tb_promocion SET promo_flag = :promo_flag WHERE promo_id = :promo_id";
			$database->query($sql);
			$database->bind(':promo_flag', $data['promo_flag']);
	        $database->bind(':promo_id', $data['promo_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaCanje(){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT a.*, b.* FROM tb_promocion a, tb_promocion_detalle b WHERE promo_estado = 1";
			//$sql = "SELECT a.*,b.* FROM tb_promocion_detalle a INNER JOIN  tb_promocion b ON a.promo_id = b.promo_id WHERE a.detalle_estado = 1";
			$sql = "CALL sp_listar_canjes()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function CountPromociones($select, $text){
		try {
			$database = new ConexionBD();
			$sqlin = '';
			if(!empty($select)){
				$sqlin = ' AND a.promo_tipo = :promo_tipo ';
			}

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( a.promo_nombre LIKE :data OR a.promo_puntos LIKE :data )';
			}

			$sql = "SELECT COUNT(*) as total FROM tb_promocion a WHERE a.promo_estado = 1".$sqlin." ".$like;

			$database->query($sql);
			if(!empty($select)){
				 $database->bind(':promo_tipo', $select);
			}
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacion($offset, $pagesize, $select, $text){
		try {
			$database = new ConexionBD();
			$sqlin = '';

			if(!empty($select)){
				$sqlin = ' AND a.promo_tipo = :promo_tipo ';
			}
			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( a.promo_nombre LIKE :data OR a.promo_puntos LIKE :data )';
			}

			$sql = "SELECT a.* FROM tb_promocion a WHERE a.promo_estado = 1".$sqlin." ".$like." ORDER BY a.promo_id DESC LIMIT :offset, :pagesize";

			$database->query($sql);
			if(!empty($select)){
				$database->bind(':promo_tipo', $select);
			}
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}

	}

	public function CountCanjes($select, $text){
		try {
			$database = new ConexionBD();
			$sqlin = '';
			if(!empty($select)){
				$sqlin = ' AND b.promo_tipo = :promo_tipo ';
			}

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$search = ($select == 1)?'OR CONCAT(cl.cliente_nombre," ", cl.cliente_apellido) LIKE :data':(($select==2)?'OR CONCAT(c.conductor_nombre," ", c.conductor_apellido) LIKE :data':'');
				$like = 'AND ( b.promo_nombre LIKE :data '.$search.')';
			}

			$sql = "SELECT count(*) as total
						FROM tb_promocion_detalle a 
						INNER JOIN tb_promocion b ON a.promo_id = b.promo_id
						INNER JOIN tb_conductor c ON a.usuario_id = c.conductor_id
						INNER JOIN tb_cliente cl ON a.usuario_id = cl.cliente_id
						WHERE a.detalle_estado = 1".$sqlin." ".$like;

			$database->query($sql);
			if(!empty($select)){
				 $database->bind(':promo_tipo', $select);
			}
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacionCanjes($offset, $pagesize, $select, $text){
		try {
				$database = new ConexionBD();
				$sqlin = '';

				if(!empty($select)){
					$sqlin = ' AND b.promo_tipo = :promo_tipo ';
				}

				$like = '';
				if(!empty($text)){
					$var = str_replace(" ","%",$text);

					$search = ($select == 1)?'OR CONCAT(cl.cliente_nombre," ", cl.cliente_apellido) LIKE :data':(($select==2)?'OR CONCAT(c.conductor_nombre," ", c.conductor_apellido) LIKE :data':'');
					$like = 'AND ( b.promo_nombre LIKE :data '.$search.')';
				}

				$sql = "SELECT a.*, b.*,
							IF (a.detalle_tipo = 1, 'Cliente', 'Conductor') as TipoUsuario,
							Case 
					        When a.detalle_tipo = 1 Then Concat(cl.cliente_nombre, ' ' , cl.cliente_apellido) 
					        When a.detalle_tipo = 2 Then Concat(c.conductor_nombre, ' ' , c.conductor_apellido) 
					        End
					        AS Nombre
							FROM tb_promocion_detalle a 
							INNER JOIN tb_promocion b ON a.promo_id = b.promo_id
							INNER JOIN tb_conductor c ON a.usuario_id = c.conductor_id
							INNER JOIN tb_cliente cl ON a.usuario_id = cl.cliente_id
							WHERE a.detalle_estado = 1".$sqlin." ".$like." ORDER BY a.detalle_fecha DESC LIMIT :offset, :pagesize";

				$database->query($sql);
				if(!empty($select)){
					$database->bind(':promo_tipo', $select);
				}
				if(!empty($text)){
					$database->bind(':data', '%'.$var.'%');	
				}
				$database->bind(':offset', $offset);
				$database->bind(':pagesize', $pagesize);
		        $database->execute();
		        return $database->resultSet();

			} catch (Exception $e) {
				throw $e;
			}

		}
		
	public function listaCanjeMailer($data){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT a.*,b.* FROM tb_promocion_detalle a INNER JOIN  tb_promocion b ON a.promo_id = b.promo_id WHERE a.detalle_estado = 1 AND b.promo_id = :promo_id";
			$sql = "CALL sp_listar_canjes_mailer(:promo_id)";
			$database->query($sql);
			 $database->bind(':promo_id', $data['promo_id']);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}
}