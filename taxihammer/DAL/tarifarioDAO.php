<?php
require_once'db.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);
class tarifarioDAO{

	public function validarTarifa($data){
		
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT count(*) as Total FROM tb_tarifario WHERE cliente_id = :cliente_id AND tiposervicio_id = :tiposervicio_id AND origen_id = :origen_id AND destino_id = :destino_id AND tarifario_status = 1";
			$sql = "CALL sp_validar_tarifa(:cliente_id, :tiposervicio_id, :origen_id, :destino_id)";
			$database->query($sql);
			$database->bind(':cliente_id', $data['cliente_id']);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':origen_id', $data['origen_id']);
	        $database->bind(':destino_id', $data['destino_id']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function lista(){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT a.*, b.empresa_razonsocial, c.tiposervicio_nombre FROM tb_tarifario a, tb_empresa b, tb_tiposervicio c WHERE a.cliente_id = b.empresa_id AND a.tiposervicio_id = c.tiposervicio_id AND a.tarifario_status = 1";
			$sql = "CALL sp_listar_tarifarios()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaPaginacion($start,$per_page){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT a.*, b.empresa_razonsocial, c.tiposervicio_nombre FROM tb_tarifario a, tb_empresa b, tb_tiposervicio c WHERE a.cliente_id = b.empresa_id AND a.tiposervicio_id = c.tiposervicio_id AND a.tarifario_status = 1 limit :start , :per_page";
			$sql = "CALL sp_paginacion_tarifario(:start, :per_page)";
			$database->query($sql);
			$database->bind(':start', $start);
	        $database->bind(':per_page', $per_page);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaPaginacionExportar(){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT a.*, b.empresa_razonsocial, c.tiposervicio_nombre FROM tb_tarifario a, tb_empresa b, tb_tiposervicio c WHERE a.cliente_id = b.empresa_id AND a.tiposervicio_id = c.tiposervicio_id AND a.tarifario_status = 1";
			$sql = "CALL sp_paginacion_exportar()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function findTarifa($data){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT * FROM tb_tarifario  WHERE cliente_id = :cliente_id AND origen_id = :origen_id AND destino_id = :destino_id AND tiposervicio_id = :tiposervicio_id and tarifario_status = 1";
			$sql = "CALL sp_find_tarifa(:cliente_id, :tiposervicio_id, :origen_id, :destino_id)";
			$database->query($sql);
			$database->bind(':cliente_id', $data['cliente_id']);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':origen_id', $data['origen_id']);
	        $database->bind(':destino_id', $data['destino_id']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function insertarTarifario($data){
		try{
			$database = new ConexionBD();

			/*$sql = "INSERT INTO tb_tarifario 
			(cliente_id, tiposervicio_id, origen_id, destino_id, tarifario_monto) 
			VALUES 
			(:cliente_id, :tiposervicio_id, :origen_id, :destino_id, :tarifario_monto)"; */
			$sql = "CALL sp_tarifario_mantenimiento(@a_message, 'nuevo', :cliente_id, :tiposervicio_id, :origen_id, :destino_id, :tarifario_monto,'')";

			$database->query($sql);
	        $database->bind(':cliente_id', $data['cliente_id']);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':origen_id', $data['origen_id']);
	        $database->bind(':destino_id', $data['destino_id']);
	        $database->bind(':tarifario_monto', $data['tarifario_monto']);
	        
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function updateTarifario($data){
		try{
			$database = new ConexionBD();

			/*$sql = "UPDATE tb_tarifario SET
			cliente_id = :cliente_id, tiposervicio_id = :tiposervicio_id, origen_id = :origen_id, destino_id = :destino_id, tarifario_monto = :tarifario_monto WHERE tarifario_id = :tarifario_id"; */
			$sql = "CALL sp_tarifario_mantenimiento(@a_message, 'update', :cliente_id, :tiposervicio_id, :origen_id, :destino_id, :tarifario_monto, :tarifario_id)";
			$database->query($sql);
	        $database->bind(':cliente_id', $data['cliente_id']);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':origen_id', $data['origen_id']);
	        $database->bind(':destino_id', $data['destino_id']);
	        $database->bind(':tarifario_monto', $data['tarifario_monto']);
	        $database->bind(':tarifario_id', $data['tarifario_id']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function deleteTarifario($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_tarifario SET tarifario_status = 0 WHERE tarifario_id = :tarifario_id";
			$sql = "CALL sp_tarifario_mantenimiento(@a_message, 'delete', '', '', '', '', '', :tarifario_id)";
			$database->query($sql);
			$database->bind(':tarifario_id', $data['tarifario_id']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function listBy($data){
		try {
			$database = new ConexionBD();
			/*$sqlin = " ";
			if($data['origen']!="todos"){
				$sqlin = $sqlin."AND a.origen_id = :origen_id ";
			}
			if($data['destino']!="todos"){
				$sqlin = $sqlin."AND a.destino_id = :destino_id ";
			}
			if($data['tiposervicio']!="todos"){
				$sqlin = $sqlin."AND a.tiposervicio_id = :tiposervicio_id ";
			}
			if($data['cliente']!="todos"){
				$sqlin = $sqlin."AND a.cliente_id = :cliente_id ";
			}
			$sql = "SELECT a.*, b.empresa_razonsocial, c.tiposervicio_nombre FROM tb_tarifario a, tb_empresa b, tb_tiposervicio c WHERE a.cliente_id = b.empresa_id AND a.tiposervicio_id = c.tiposervicio_id AND a.tarifario_status = 1".$sqlin." limit :start, :per_page";


			$database->query($sql);
			if($data['origen']!="todos"){
				$database->bind(':origen_id', $data['origen']);
			}
			if($data['destino']!="todos"){
				$database->bind(':destino_id', $data['destino']);
			}
			if($data['tiposervicio']!="todos"){
				$database->bind(':tiposervicio_id', $data['tiposervicio']);
			}
			if($data['cliente']!="todos"){
				$database->bind(':cliente_id', $data['cliente']);
			}*/

			$sql = "CALL sp_listar_tarifario_by(:start, :per_page, :origen, :destino, :tipo_servicio, :clienteId)";
			$database->bind(':start', $data['start']);
			$database->bind(':per_page', $data['per_page']);
			$database->bind(':origen', $data['origen']);
			$database->bind(':destino', $data['destino']);
			$database->bind(':tipo_servicio', $data['tiposervicio']);
			$database->bind(':clienteId', $data['cliente']);
	        $database->execute();
	        return $database->resultSet();

		}catch(Exception $e){
			throw $e;
		}
	}
	public function listByFiltro($data){
		try {
			$database = new ConexionBD();
			/*$sqlin = " ";
			if($data['origen']!="todos"){
				$sqlin = $sqlin."AND a.origen_id = :origen_id ";
			}
			if($data['destino']!="todos"){
				$sqlin = $sqlin."AND a.destino_id = :destino_id ";
			}
			if($data['tiposervicio']!="todos"){
				$sqlin = $sqlin."AND a.tiposervicio_id = :tiposervicio_id ";
			}
			if($data['cliente']!="todos"){
				$sqlin = $sqlin."AND a.cliente_id = :cliente_id ";
			}*/
			//$sql = "SELECT a.*, b.empresa_razonsocial, c.tiposervicio_nombre FROM tb_tarifario a, tb_empresa b, tb_tiposervicio c WHERE a.cliente_id = b.empresa_id AND a.tiposervicio_id = c.tiposervicio_id AND a.tarifario_status = 1".$sqlin;

			/*$database->query($sql);
			if($data['origen']!="todos"){
				$database->bind(':origen_id', $data['origen']);
			}
			if($data['destino']!="todos"){
				$database->bind(':destino_id', $data['destino']);
			}
			if($data['tiposervicio']!="todos"){
				$database->bind(':tiposervicio_id', $data['tiposervicio']);
			}
			if($data['cliente']!="todos"){
				$database->bind(':cliente_id', $data['cliente']);
			}*/

			$sql = "CALL sp_listar_tarifario_by_filtro(:origen, :destino, :tipo_servicio, :clienteId)";
			$database->query($sql);
			$database->bind(':origen', $data['origen']);
			$database->bind(':destino', $data['destino']);
			$database->bind(':tipo_servicio', $data['tiposervicio']);
			$database->bind(':clienteId', $data['cliente']);
	        $database->execute();
	        return $database->resultSet();

		}catch(Exception $e){
			throw $e;
		}
	}

	public function listaConfigrango(){
		try {
			$database = new ConexionBD();	
			//$sql = "SELECT * FROM tb_rango_horas WHERE rango_status = 1";
			$sql = "CALL sp_lista_config_rango(0)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertarConfigHoras($data){
		try{
			$database = new ConexionBD();

			/*$sql = "INSERT INTO tb_rango_horas 
			(rango_hinicio, rango_hfin, rango_lun, rango_mar, rango_mie, rango_jue, rango_vie, rango_sab, rango_dom, rango_check) 
			VALUES 
			(:rango_hinicio, :rango_hfin, :rango_lun, :rango_mar, :rango_mie, :rango_jue, :rango_vie, :rango_sab, :rango_dom , :rango_check)"; */

			$sql = "CALL sp_rango_horas_mantenimiento(@a_message,'nuevo',:rango_hinicio,:rango_hfin,:rango_lun,:rango_mar,:rango_mie,:rango_jue,:rango_vie,:rango_sab,:rango_dom,:rango_check,0,0)";
			$database->query($sql);
	        $database->bind(':rango_hinicio', $data['rango_hinicio']);
	        $database->bind(':rango_hfin', $data['rango_finicio']);
	        $database->bind(':rango_lun', $data['rango_lun']);
	        $database->bind(':rango_mar', $data['rango_mar']);
	        $database->bind(':rango_mie', $data['rango_mie']);
	        $database->bind(':rango_jue', $data['rango_jue']);
	        $database->bind(':rango_vie', $data['rango_vie']);
			$database->bind(':rango_sab', $data['rango_sab']);  
			$database->bind(':rango_dom', $data['rango_dom']);
			$database->bind(':rango_check', $data['rango_check']); 
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function UpdateConfigHoras($data){
		try {
			$database = new ConexionBD();
			//$sql = "CALL SP_tb_rango_horas_update('".$data['rango_hinicio']."','".$data['rango_finicio']."','".$data['rango_lun']."','".$data['rango_mar']."','".$data['rango_mie']."','".$data['rango_jue']."','".$data['rango_vie']."','".$data['rango_sab']."','".$data['rango_dom']."','".$data['rango_check']."',".$data['rangohora_id'].",".$data['rango_estado'].")";
			$sql = "CALL sp_rango_horas_mantenimiento(@a_message,'update',:rango_hinicio,:rango_hfin,:rango_lun,:rango_mar,:rango_mie,:rango_jue,:rango_vie,:rango_sab,:rango_dom,:rango_check,:rango_estado,:rangohora_id)";
			$database->query($sql);
			$database->bind(':rango_hinicio', $data['rango_hinicio']);
	        $database->bind(':rango_hfin', $data['rango_finicio']);
	        $database->bind(':rango_lun', $data['rango_lun']);
	        $database->bind(':rango_mar', $data['rango_mar']);
	        $database->bind(':rango_mie', $data['rango_mie']);
	        $database->bind(':rango_jue', $data['rango_jue']);
	        $database->bind(':rango_vie', $data['rango_vie']);
			$database->bind(':rango_sab', $data['rango_sab']);  
			$database->bind(':rango_dom', $data['rango_dom']);
			$database->bind(':rango_check', $data['rango_check']);
			$database->bind(':rango_estado', $data['rango_estado']);
			$database->bind(':rangohora_id', $data['rangohora_id']);
			$database->execute();
			return true;		
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaConfigrangoId($id){
		try {
			$database = new ConexionBD();	
			//$sql = "SELECT * FROM tb_rango_horas WHERE rangohora_id = :rangohora_id and rango_status = 1";
			$sql = "CALL sp_lista_config_rango(:rangohora_id)";
			$database->query($sql);
			$database->bind(':rangohora_id', $id); 
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}	

	public function listRangoHorasWs(){
		try {
			
			$database = new ConexionBD();	
			/*$sql = "SELECT * FROM tb_rango_horas where date_format(now(), '%H:%i %p') between rango_hinicio and rango_hfin 
					 and rango_estado = 1 and rango_status = 1 order by rango_hinicio ASC";*/
			//$sql = "SELECT CONCAT(ELT(WEEKDAY(now()) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS Dia";
			$sql = "CALL sp_listar_rango_horas_ws()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function obtenerPorcentajeHora($campo){
		try {
			$database = new ConexionBD();	
			$sql = "SELECT ".$campo." FROM tb_rango_horas where date_format(now(), '%H:%i %p') between rango_hinicio and rango_hfin 
					 and rango_estado = 1 and rango_status = 1 order by rango_hinicio DESC LIMIT 0 , 1";

			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function deleteRango($rangohora_id){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_rango_horas SET rango_status = 2 WHERE rangohora_id = :rangohora_id";
			$sql = "CALL sp_rango_horas_mantenimiento(@a_message,'delete','','',0,0,0,0,0,0,0,'',0,:rangohora_id)";
			$database->query($sql);
			$database->bind(':rangohora_id', $rangohora_id);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function ValidarHoraRango($rango_hinicio, $rango_hfin){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT COUNT(*) as total FROM tb_rango_horas WHERE rango_hinicio = :rango_hinicio AND rango_hfin = :rango_hfin AND rango_status = 1";
			$sql = "CALL sp_validar_rango(:rango_hinicio, :rango_hfin)";
			$database->query($sql);
			$database->bind(':rango_hinicio', $rango_hinicio);
			$database->bind(':rango_hfin', $rango_hfin);
	        $database->execute();
	        return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}
}