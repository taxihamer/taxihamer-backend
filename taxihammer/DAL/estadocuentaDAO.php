<?php
require_once'db.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class estadocuentaDAO{

	public function listaEstadoCuenta($conductor_id){

		try {
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_estado_cuenta WHERE conductor_id = :conductor_id and cta_check = 1 ORDER BY cta_id DESC";
			$sql = "CALL sp_listar_estado_cuenta(:conductor_id)";
			$database->query($sql);
			$database->bind(':conductor_id', $conductor_id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function exportarEstadoCuenta($search){
		try{

			$search_where = "";
			if(!empty($search)){
				$search_where = "AND CONCAT(c.conductor_nombre,' ',c.conductor_apellido) LIKE '%{$search}%' ";
			}

			$database = new ConexionBD();
			$sql="SELECT CONCAT(c.conductor_nombre,' ',c.conductor_apellido) conductor,  u.unidad_alias unidad,
					c.conductor_dni dni, c.conductor_correo correo, c.conductor_celular celular, c.conductor_recarga saldo,
					IF(ec.cta_tipo = 1,'Recarga','Servicio') movimiento , ec.cta_fecha fecha, 
					IF(s.tipo_pago = 1,'Efectivo',IF(tipo_pago = 2,'Vale',IF(tipo_pago = 3 ,'Tarjeta','Otros'))) tipo_pago,
					IF(s.tipo_pago = 1, CONCAT('-',ec.cta_monto) ,CONCAT('+',ec.cta_monto)) monto
					FROM tb_estado_cuenta ec INNER JOIN tb_conductor c ON ec.conductor_id =c.conductor_id 
					INNER JOIN tb_unidad u ON c.unidad_id = u.unidad_id  
					LEFT JOIN tb_servicio s ON ec.servicio_id = s.servicio_id
					WHERE c.conductor_status not in (2) AND c.unidad_id IS NOT NULL {$search_where} ";

			$database->query($sql);
	        $database->execute();

	        return $database->resultSet();

		}catch(Exception $e){
			throw $e;
		}
	}
}