<?php
require_once'db.php';
require_once'character.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class servicioDAO{

	public function listaServicio($servicio_id){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT *, TIMEDIFF(hora_fin,hora_inicio) as diferencia, DATE_FORMAT(hora_inicio, '%Y-%m-%d') as fecha , DATE_FORMAT(hora_inicio, '%T') AS hora FROM tb_servicio WHERE servicio_id = :servicio_id";
			$sql = "CALL sp_listar_servicios_por_id(:servicio_id)";
			$database->query($sql);
			$database->bind(':servicio_id', $servicio_id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaCliente($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * , DATE_FORMAT(hora_inicio, '%Y-%m-%d') as fecha , DATE_FORMAT(hora_inicio, '%T') AS hora FROM tb_servicio WHERE cliente_id = :cliente_id AND estado = 6 order by servicio_id DESC LIMIT :inicio, :limite";
			$sql = "CALL sp_listar_servicios_por_cliente(:cliente_id, :inicio, :limite)";
			$database->query($sql);
			$database->bind(':cliente_id', (int)$data['cliente_id']);
			$database->bind(':inicio', (int)$data['inicio']);
			$database->bind(':limite', (int)$data['limite']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	
	public function listaConductor($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT *, DATE_FORMAT(hora_inicio, '%Y-%m-%d') as fecha , DATE_FORMAT(hora_inicio, '%T') AS hora FROM tb_servicio WHERE conductor_id = :conductor_id AND estado = 6 ORDER BY servicio_id DESC LIMIT :inicio, :limite";
			$sql = "CALL sp_listar_servicios_por_conductor(:conductor_id, :inicio, :limite)";
			$database->query($sql);
			$database->bind(':conductor_id', $data['conductor_id']);
			$database->bind(':inicio', $data['inicio']);
			$database->bind(':limite', $data['limite']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function recuperarEstadoCli($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT a.*, b.* FROM tb_servicio a, tb_conductor b WHERE cliente_id = :cliente_id AND a.conductor_id = b.conductor_id AND estado <> 5";
			$sql = "CALL sp_recuperar_estado_cliente(:cliente_id)";
			$database->query($sql);
			$database->bind(':cliente_id', $data['cliente_id']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function recuperarEstadoCond($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT a.*, b.* FROM tb_servicio a, tb_cliente b WHERE conductor_id = :conductor_id AND a.cliente_id = b.cliente_id AND estado <> 5";
			$sql = "CALL sp_recuperar_estado_conductor(:conductor_id)";
			$database->query($sql);
			$database->bind(':conductor_id', $data['conductor_id']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertarServicio($data){

		$data['fecha_seleccion'] = ($data['fecha_seleccion'] != 1)?NULL:date("Y/m/d H:i:s");
		$data['hora_inicio'] = ($data['hora_inicio'] != 1)?NULL:date("Y/m/d H:i:s");
		
		//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		try{
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_servicio (cliente_id, conductor_id, hora_inicio, tiposervicio_id, origen_id, origen_nombre, origen_direccion, origen_lat, origen_lon, destino_id, destino_nombre, destino_direccion, destino_lat, destino_lon, tipo_pago, tarifa, estado, fecha_seleccion,flag_reserva, origen_referencia, destino_referencia) VALUES (:cliente_id, :conductor_id, :hora_inicio, :tiposervicio_id, :origen_id, :origen_nombre, :origen_direccion, :origen_lat, :origen_lon, :destino_id, :destino_nombre, :destino_direccion, :destino_lat, :destino_lon, :tipo_pago, :tarifa, :estado, :fecha_seleccion, :flag_reserva, :origen_referencia, :destino_referencia)";
			$sql = "CALL sp_servicio_mantenimiento(@a_message,'nuevo',:cliente_id,0,:conductor_id,:hora_inicio,:tiposervicio_id,:origen_id,:origen_nombre,:origen_direccion,:origen_lat,:origen_lon,:destino_id,:destino_nombre,:destino_direccion,:destino_lat,:destino_lon,:tipo_pago,:tarifa,:estado,:fecha_seleccion,:flag_reserva,:origen_referencia,:destino_referencia,:imagen,:comentario)";

			$database->query($sql);
      $database->bind(':cliente_id', $data['cliente_id']);
      $database->bind(':conductor_id', $data['conductor_id']); 
     	$database->bind(':hora_inicio', $data['hora_inicio']);
      $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
			$database->bind(':origen_id', $data['origen_id']);
			$database->bind(':origen_nombre', SanitizeCharacter($data['origen_nombre']));
			$database->bind(':origen_direccion', SanitizeCharacter($data['origen_direccion']));
			$database->bind(':origen_lat', $data['origen_lat']);
			$database->bind(':origen_lon', $data['origen_lon']);
			$database->bind(':destino_id', $data['destino_id']);
			$database->bind(':destino_nombre', SanitizeCharacter($data['destino_nombre']));
			$database->bind(':destino_direccion', SanitizeCharacter($data['destino_direccion']));
			$database->bind(':destino_lat', $data['destino_lat']);
			$database->bind(':destino_lon', $data['destino_lon']);
			$database->bind(':tipo_pago', $data['tipo_pago']);
			$database->bind(':tarifa', $data['tarifa']);
			$database->bind(':estado', $data['estado']);
			$database->bind(':fecha_seleccion',$data['fecha_seleccion']);
			$database->bind(':flag_reserva',$data['flag_reserva']);
			$database->bind(':origen_referencia',(isset($data['ubicacion']))?SanitizeCharacter($data['origen_referencia']):null);
			$database->bind(':destino_referencia',(isset($data['ubicacion']))?SanitizeCharacter($data['destino_referencia']):null);
			$database->bind(':imagen', $data['imagen']);
			$database->bind(':comentario', $data['comentario']);
	        $database->execute();
	        return $database->resultSet();
	        //$id = $database->lastInsertId();
	        //return array('status' => true, 'lastid' => $id);

		}catch(Exception $e){
			throw $e;
		}
	}

	public function listMovimientos(){
		try {
			$database = new ConexionBD();
			/*$sql = "SELECT distinct CONCAT(cc.conductor_nombre, ' ', cc.conductor_apellido) As NombreConductor ,tc.tipocliente_descripcion ,s.servicio_id,
					 DATE_FORMAT(s.fecha_seleccion, '%Y-%m-%d') as fecha_seleccion ,
					 DATE_FORMAT(s.hora_inicio, '%T') as Inicio , DATE_FORMAT(s.hora_fin, '%T') as Fin,
					 s.tarifa, s.conductor_id,s.cliente_id,s.tipo_pago,s.destino_nombre,s.origen_nombre,
					 CONCAT(c.cliente_nombre,' ',c.cliente_apellido) AS NombreCliente , s.origen_id, s.destino_id , s.origen_lat, s.origen_lon, s.destino_lat, s.destino_lon, 
					 UNIX_TIMESTAMP(s.fecha_seleccion) as ff ,
					 IF(estado=6,'Finalizado','Cancelado') as estadoservice, s.estado, s.peaje, s.parqueo, s.espera, s.parada, s.origen_direccion, s.destino_direccion ,
					 (SELECT calificacion_value FROM tb_calificacion WHERE tb_calificacion.calificacion_tipoid = 1 AND tb_calificacion.servicio_id = s.servicio_id LIMIT 1) AS calificacion_conductor,
					 (SELECT calificacion_value FROM tb_calificacion WHERE tb_calificacion.calificacion_tipoid = 2 AND tb_calificacion.servicio_id = s.servicio_id LIMIT 1) AS calificacion_cliente
					 FROM tb_servicio s 
					INNER JOIN tb_cliente c  ON s.cliente_id = c.cliente_id 
					INNER JOIN tb_tipocliente tc ON tc.tipocliente_id = c.tipocliente_id 
					INNER JOIN tb_conductor cc ON cc.conductor_id = s.conductor_id
					LEFT JOIN tb_unidad u on cc.unidad_id = u.unidad_id 
					LEFT JOIN tb_calificacion cal ON cal.servicio_id = s.servicio_id
					WHERE s.estado in (6,10) ORDER BY s.fecha_seleccion DESC";*/

					// Consulta con las puntuaciones 

				// 	$sql = "SELECT distinct CONCAT(cc.conductor_nombre, ' ', cc.conductor_apellido) As NombreConductor ,tc.tipocliente_descripcion ,
				// 					s.servicio_id,if(c.tipocliente_id = 1,tc.tipocliente_descripcion,te.empresa_razonsocial) as tipo_descripcion,s.servicio_vale,
				// 	DATE_FORMAT(s.fecha_seleccion, '%Y-%m-%d') as fecha_seleccion ,
				// 	DATE_FORMAT(s.hora_inicio, '%T') as Inicio , DATE_FORMAT(s.hora_fin, '%T') as Fin,
				// 	s.tarifa, s.conductor_id,s.cliente_id,s.tipo_pago,s.destino_nombre,s.origen_nombre,
				// 	CONCAT(c.cliente_nombre,' ',c.cliente_apellido) AS NombreCliente , s.origen_id, s.destino_id , s.origen_lat, s.origen_lon, s.destino_lat, s.destino_lon, 
				// 	UNIX_TIMESTAMP(s.fecha_seleccion) as ff ,
				// 	IF(estado=6,'Finalizado','Cancelado') as estadoservice, s.estado, s.peaje, s.parqueo, s.espera, s.parada, s.origen_direccion, s.destino_direccion ,
				// 	(SELECT calificacion_value FROM tb_calificacion WHERE tb_calificacion.calificacion_tipoid = 1 AND tb_calificacion.servicio_id = s.servicio_id LIMIT 1) AS calificacion_conductor,
				// 	(SELECT calificacion_value FROM tb_calificacion WHERE tb_calificacion.calificacion_tipoid = 2 AND tb_calificacion.servicio_id = s.servicio_id LIMIT 1) AS calificacion_cliente

				// 	FROM tb_servicio s 
				// 	INNER JOIN tb_cliente c  ON s.cliente_id = c.cliente_id 
				// 	INNER JOIN tb_tipocliente tc ON tc.tipocliente_id = c.tipocliente_id
				//     LEFT JOIN tb_empresa te ON te.empresa_id = c.empresa_id
				// 	INNER JOIN tb_conductor cc ON cc.conductor_id = s.conductor_id
				// 	LEFT join tb_unidad u on cc.unidad_id = u.unidad_id 
				// 	LEFT JOIN tb_calificacion cal ON cal.servicio_id = s.servicio_id
				// 	WHERE s.estado in (6,10) ORDER BY s.servicio_id DESC;
				// END";

			$sql = "CALL sp_listar_movimientos()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
}


