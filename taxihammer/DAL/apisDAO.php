<?php

require_once'db.php';

class apisDAO
{	
	public function insertarApis($data){	
		try{
			$database = new ConexionBD();

			//$sql = "INSERT INTO tb_api_key (api_google_web, api_google_android, api_google_ios,api_foursquare_key,api_foursquare_secret,api_here_key,api_here_secret,api_fecha_create,api_fecha_fin, api_estado)VALUES (:api_google_web, :api_google_android, :api_google_ios, :api_foursquare_key, :api_foursquare_secret, :api_here_key, :api_here_secret, :api_fecha_create, :api_fecha_fin, :api_estado)";
			$sql = "CALL sp_apikey_mantenimiento(@a_message, :tipo, :api_google_web, :api_google_android, :api_google_ios, :api_foursquare_key, :api_foursquare_secret, :api_here_key, :api_here_secret, :api_fecha_create, :api_fecha_fin,0,0)";
			$database->query($sql);
			$database->bind(':tipo', 'nuevo');
	        $database->bind(':api_google_web', $data['api_google_web']);
	        $database->bind(':api_google_android', $data['api_google_android']);
	        $database->bind(':api_google_ios', $data['api_google_ios']);
	       	$database->bind(':api_foursquare_key', $data['api_foursquare_key']);
	        $database->bind(':api_foursquare_secret', $data['api_foursquare_secret']);
	        $database->bind(':api_here_key', $data['api_here_key']);
	        $database->bind(':api_here_secret', $data['api_here_secret']);
	        $database->bind(':api_fecha_create', $data['api_fecha_create']);
	        $database->bind(':api_fecha_fin', $data['api_fecha_fin']);
	        //$database->bind(':api_estado', 0);

	        $database->execute();
	        $id = $database->lastInsertId();

	        return array('status' => true, 'lastid' => $id);

		}catch(Exception $e){
			throw $e;
		}
	}

	public function CountApis(){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*) as total FROM tb_api_key ORDER BY api_id DESC ";
			$sql = "CALL sp_count_apis()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacion($offset, $pagesize){

		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_api_key ORDER BY api_id DESC LIMIT :offset, :pagesize";
			$sql = "CALL sp_paginacion_apis(:offset, :pagesize)";
			$database->query($sql);
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function updateAll($estado){
		try{
			$database = new ConexionBD();
			//$sql = "UPDATE tb_api_key SET api_estado = :api_estado ";
			$sql = "CALL sp_apikey_mantenimiento(@a_message, :tipo, '' , '' , '', '', '', '', '', '', '', 0, 0)";
			$database->query($sql);
			$database->bind(':tipo', 'all');
			//$database->bind(':api_estado', $estado);
			$database->execute();
			return true;
		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateFind($data){
		try{
			$database = new ConexionBD();
			//$sql = "UPDATE tb_api_key SET api_estado = :api_estado WHERE api_id = :api_id";
			$sql = "CALL sp_apikey_mantenimiento(@a_message, :tipo, '' , '' , '', '', '', '', '', '', '', :api_estado, :api_id)";
			$database->query($sql);
			$database->bind(':tipo', 'find');
			$database->bind(':api_estado', $data['api_estado']);
			$database->bind(':api_id', $data['api_id']);
			$database->execute();
			return true;
		}catch(Exception $e){
			throw $e;
		}
	}
}