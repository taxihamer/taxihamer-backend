<?php
require_once 'generalDAO.php';
require_once 'reservaDAO.php';

/*
define('NAME_EMPRESA', 'TaxiHammer');
define('NAME', 'Taxi');
define('EMPRESA','Hammer');
define('IMG_DEFAULT', 'BL/files/ic_carro.png');
define('IP_SOCKET','http://208.68.39.4:3000/');
define('IMG_DEFAULT1', 'files/ic_carro.png');
define('KEY','AIzaSyCGiUwgztFliJkBASvBKLLwt2HWW-UCyM4');
define('URL_CORREO','http://208.68.39.4/');
*/
define('NAME_EMPRESA', 'TaxiHammer');
define('NAME', 'Taxi');
define('EMPRESA','Hammer');
define('IMG_DEFAULT', 'BL/files/ic_carro.png');
define('IP_SOCKET','http://192.168.10.10:3000/');
define('IMG_DEFAULT1', 'files/ic_carro.png');
define('KEY','AIzaSyCGiUwgztFliJkBASvBKLLwt2HWW-UCyM4');
define('URL_CORREO','http://192.168.10.10/');





$correo = ConfigCorreo();
$api_key = Key_Google_Panel();
$horareserva = listaConfigreserva();
$random_app = generateRandomString();

define('HORA_RESERVA', (!empty($horareserva))?$horareserva[0]['configreserva_horas']:0);

$config_tipo_seguridad = ($correo[0]['config_tipo_seguridad'] == 1)?'tls':'';


define('HOST', $correo[0]['config_server_smtp']);
define('USERNAME', $correo[0]['config_usuaro_smtp']);
//define('PASSWORD', $correo[0]['config_password_smtp']);
define('PASSWORD', $correo[0]['config_password_smtp']);
define('SMTPSECURE', $config_tipo_seguridad);
define('PORT', $correo[0]['config_puerto_smtp']);
define('FROM', $correo[0]['config_usuaro_smtp']);

define('KEY_PANEL_GOOGLE', $api_key[0]['api_google_panel']);

define('RANDOM_APP', $random_app[0]);

function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function Key_Google_Panel(){
    $generalDAO = new generalDAO();
    $respta = $generalDAO->listKeyMoviles();
    return $respta;
}

function listaConfigreserva(){
    $reservaDAO = new reservaDAO();
    $rpta = $reservaDAO->listaConfigreservaActivo();
    $rptaReserva = false;
    if(!empty($rpta)){
        $rptaReserva = $rpta;
    }

    return $rptaReserva;
}

function ConfigCorreo(){
    $generalDAO = new generalDAO();
    $respta = $generalDAO->listaBy(1);
    return $respta;
}

function DelCharacter($string){
 
    $string = trim($string);
 
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );
 
    $string = str_replace(
        array("\\", "¨", "º", "-", "~",
             "#", "@", "|", "!", "\"",
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":"),
        '-',
        $string
    );
return $string;
}
