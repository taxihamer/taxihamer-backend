<?php
require_once'db.php';
require_once'character.php';

class lugaresDAO{

	public function lista(){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT * FROM tb_lugares where lugar_estado = 1 ORDER BY lugar_descripcion ASC";
			$sql = "CALL sp_listar_lugares()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaWS($data){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT * FROM tb_lugares LIMIT ".$data['lugar_tot'].",".$data['lugar_tot1']." UNION SELECT * FROM tb_lugares WHERE lugar_mod >".$data['lugar_mod'];
			$sql = "CALL sp_lista_lugares_ws()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function cantidadTotalDeLugares()
    {
        try {

            $database = new ConexionBD();

            //$sql = " SELECT count(*) as cantidad FROM tb_lugares";
            $sql = "CALL sp_cantidad_lugares()";
            $database->query($sql);
            $database->execute();
            return $database->resultSet();

        } catch (Exception $ex) {
            throw $ex;
        }
    }

	public function listabyon($id){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT * FROM tb_lugares where lugar_estado = 1 and lugares_id NOT IN (:lugares_id)";
			$sql = "CALL sp_listar_lugares_by(:lugares, '0')";
			$database->query($sql);
			$database->bind(':lugares_id', $id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listabyin($id){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT * FROM tb_lugares where lugar_estado = 1 and lugares_id IN (:lugares_id)";
			$sql = "CALL sp_listar_lugares_by(:lugares, '1')";
			$database->query($sql);
			$database->bind(':lugares_id', $id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function maxlugar(){
		try {
			$database = new ConexionBD();
			
			//$sql = "SELECT max(lugar_mod) as lugar_mod from tb_lugares";
			$sql = "CALL sp_max_lugar()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertarLugar($data){

		try{
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_lugares (lugar_descripcion, lugar_lat, lugar_lon, lugar_coordpolygono, lugar_fecharegistro, lugar_estado) VALUES (:lugar_descripcion, :lugar_lat, :lugar_lon, :lugar_coordpolygono, :lugar_fecharegistro, :lugar_estado)";
			$sql = "CALL sp_lugar_mantenimiento(@a_message, :tipo, 0, :lugar_descripcion, :lugar_lat, :lugar_lon, :lugar_coordpolygono, :lugar_fecharegistro, '', :lugar_estado, '')";
			$database->query($sql);
			$database->bind(':tipo', 'nuevo');
	        $database->bind(':lugar_descripcion', SanitizeCharacter($data['lugar_descripcion']));
	        $database->bind(':lugar_lat', $data['lugar_lat']);
	        $database->bind(':lugar_lon', $data['lugar_lon']);
	        $database->bind(':lugar_coordpolygono', json_encode($data['lugar_coordpolygono']));
	        $database->bind(':lugar_fecharegistro', date("Y/m/d H:i:s"));
	        $database->bind(':lugar_estado', 1);

	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateLugar($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_lugares SET lugar_coordpolygono = :lugar_coordpolygono, lugar_fechaupdate = :lugar_fechaupdate WHERE lugares_id = :lugares_id";
			$sql = "CALL sp_lugar_mantenimiento(@a_message, :tipo, :lugares_id , '' , '' , '' , :lugar_coordpolygono, '' , :lugar_fechaupdate, '' , '')";
			$database->query($sql);
			$database->bind(':tipo', 'update');
			$database->bind(':lugar_coordpolygono', json_encode($data['lugar_coordpolygono']));
			$database->bind(':lugar_fechaupdate', date("Y/m/d h:i:s"));
	        $database->bind(':lugares_id', $data['lugares_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function deleteLugar($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_lugares SET lugar_estado = :lugar_estado WHERE lugares_id = :lugares_id";
			$sql = "CALL sp_lugar_mantenimiento(@a_message, :tipo, :lugares_id , '' , '' , '' , '' , '' , '' , :lugar_estado , '')";
			$database->query($sql);
			$database->bind(':tipo', 'delete');
			$database->bind(':lugar_estado', 0);
	        $database->bind(':lugares_id', $data['lugares_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function updateLugarmax($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_lugares SET lugar_mod = :lugar_mod WHERE lugares_id = :lugares_id";
			$sql = "CALL sp_lugar_mantenimiento(@a_message, :tipo, :lugares_id , '' , '' , '' , '' , '' , '' , '' , :lugar_mod)";
			$database->query($sql);
			$database->bind(':tipo', 'max');
			$database->bind(':lugar_mod', $data['lugar_mod']);
	        $database->bind(':lugares_id', $data['lugares_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function validarLugarId($lugar_id){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT count(*) as total FROM tb_lugares WHERE lugares_id = :lugares_id and lugar_estado = 1";
			$sql = "CALL sp_validar_lugar(:lugares_id)";
			$database->query($sql);
	        $database->bind(':lugares_id', $lugar_id);
	        $database->execute();
	        return $database->resultSet();
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function SolicitarTarifa($data){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_tarifario  WHERE cliente_id = :cliente_id AND origen_id = :origen_id AND destino_id = :destino_id AND tiposervicio_id = :tiposervicio_id ";
			$sql = "CALL sp_solicitar_tarifa(:cliente_id, :origen_id, :destino_id, :tiposervicio_id)";
			$database->query($sql);
			$database->bind(':cliente_id', $data['cliente_id']);
			$database->bind(':origen_id', $data['origen_id']);
			$database->bind(':destino_id', $data['destino_id']);
			$database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->execute();
	        return $database->resultSet();
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function actualizarLugarnombre($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_lugares SET lugar_descripcion = :lugar_descripcion WHERE lugares_id = :lugares_id";
			$sql = "CALL sp_lugar_mantenimiento(@a_message, :tipo, :lugares_id , :lugar_descripcion, '' , '' , '' , '', '', '' , '')";
			$database->query($sql);
			$database->bind(':tipo', 'nombre');
			$database->bind(':lugar_descripcion', SanitizeCharacter($data['lugar_descripcion']));
	        $database->bind(':lugares_id', $data['lugares_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}
}