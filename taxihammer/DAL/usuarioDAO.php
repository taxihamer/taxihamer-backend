<?php
require_once'db.php';
class usuarioDAO{

	public function logIn($data){
		try {

			$database = new ConexionBD();

			$sql = "SELECT a.usuario_nombre, b.rol_nombre, a.usuario_mail, b.rol_id, b.menurol_id FROM tb_usuario a, tb_rol b WHERE a.rol_id = b.rol_id AND a.usuario_mail = :user AND a.usuario_password = :pass AND a.usuario_status = 1";
			//$sql = "CALL sp_login_usuario(:user, :pass)";
			$database->query($sql);
	        $database->bind(':user', $data['mail']);
	        $database->bind(':pass', $data['pass']);
	        $database->execute();
	        return $database->resultSet();

    	} catch (Exception $e) {
            throw $e;
        }
	}
	public function listByMail($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_usuario WHERE usuario_mail = :user";
			$sql = "CALL sp_listar_usuario_por_correo(:user)";

			$database->query($sql);
	        $database->bind(':user', $data['mail']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listEstado($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT a.usuario_nombre, b.rol_nombre, a.usuario_mail, b.rol_id, b.menurol_id,a.usuario_status FROM tb_usuario a, tb_rol b WHERE a.rol_id = b.rol_id AND a.usuario_mail = :user AND a.usuario_password = :pass";
			$sql = "CALL sp_listar_usuario_estado(:user, :pass)";
			$database->query($sql);
	        $database->bind(':user', $data['mail']);
	        $database->bind(':pass', $data['pass']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listById($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_usuario WHERE usuario_id = :id";
			$sql = "CALL sp_listar_usuario_por_id(:id, 0)";
			$database->query($sql);
	        $database->bind(':id', $data['usuario_id']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listByIdRolUsuario($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_usuario WHERE usuario_id = :id";
			$sql = "CALL sp_listar_usuario_por_id(:id, 1)";
			$database->query($sql);
	        $database->bind(':id', $data['usuario_id']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function updatePassword($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_usuario SET usuario_password = :pass WHERE usuario_mail = :mail";
			$sql = "CALL sp_usuario_mantenimiento(@a_message,'password','',:usuario_mail,'','','','','',:usuario_password)";
			$database->query($sql);
	        $database->bind(':mail', $data['mail']);
	        $database->bind(':pass', $data['pass']);
	        $database->execute();
	        return true;

	    } catch (Exception $e) {
            throw $e;
        }
	}
	public function insertarUsuario($data){
		try{
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_usuario (usuario_nombre, usuario_mail, rol_id, region_id, usuario_latitud, usuario_longitud, usuario_status, usuario_password) VALUES (:usuario_nombre, :usuario_mail, :rol_id, :region_id, :usuario_latitud, :usuario_longitud, :usuario_status, :usuario_password)";
			$sql = "CALL sp_usuario_mantenimiento(@a_message,'nuevo',:usuario_nombre,:usuario_mail,:rol_id,:region_id,:usuario_latitud,:usuario_longitud,:usuario_status,:usuario_password)";
			$database->query($sql);
	        $database->bind(':usuario_nombre', $data['usuario_nombre']);
	        $database->bind(':usuario_mail', $data['usuario_mail']);
	        $database->bind(':rol_id', $data['rol_id']);
	        $database->bind(':region_id', $data['region_id']);
	        $database->bind(':usuario_latitud', $data['usuario_latitud']);
	        $database->bind(':usuario_longitud', $data['usuario_longitud']);
	        $database->bind(':usuario_status', $data['usuario_status']);
	        $database->bind(':usuario_password', $data['usuario_password']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function lista(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT a.*, b.rol_nombre FROM tb_usuario a, tb_rol b WHERE a.rol_id = b.rol_id";
			$sql = "CALL sp_listar_usuarios_rol()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaNuevaUsuario($rol_id){

		try {
			$not_in = '';
			if($rol_id != 1 && $rol_id == 3 ){
				$not_in = ' AND a.rol_id NOT IN (1) ';
			}else if ($rol_id != 1 && $rol_id == 2 ) {
				$not_in = ' AND a.rol_id NOT IN (1,2) ';
			}

			$database = new ConexionBD();
			$sql = "SELECT a.*, b.rol_nombre FROM tb_usuario a, tb_rol b WHERE a.rol_id = b.rol_id".$not_in;

			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function updateUsuario($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_usuario SET usuario_nombre = :usuario_nombre, usuario_mail = :usuario_mail, rol_id = :rol_id, region_id = :region_id, usuario_latitud = :usuario_latitud, usuario_longitud = :usuario_longitud, usuario_status = :usuario_status, usuario_password = :usuario_password WHERE usuario_id = :usuario_id";
			$sql = "CALL sp_usuario_mantenimiento(@a_message,'update',:usuario_id,:usuario_nombre,:usuario_mail,:rol_id,:region_id,:usuario_latitud,:usuario_longitud,:usuario_status,:usuario_password)";
			$database->query($sql);
			$database->bind(':usuario_id', $data['usuario_id']);
	        $database->bind(':usuario_nombre', $data['usuario_nombre']);
	        $database->bind(':usuario_mail', $data['usuario_mail']);
	        $database->bind(':rol_id', $data['rol_id']);
	        $database->bind(':region_id', $data['region_id']);
	        $database->bind(':usuario_latitud', $data['usuario_latitud']);
	        $database->bind(':usuario_longitud', $data['usuario_longitud']);
	        $database->bind(':usuario_status', $data['usuario_status']);
	        $database->bind(':usuario_password', $data['usuario_password']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function eliminarUsuario($data){
		try {
			$database = new ConexionBD();

			//$sql = "DELETE FROM tb_usuario WHERE usuario_id = :id";
			$sql = "CALL sp_usuario_mantenimiento(@a_message,'delete',:usuario_id,'','','','','','','','')";
			$database->query($sql);
	        $database->bind(':id', $data['usuario_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listacoordenadas($data){

		try {
			$database = new ConexionBD();

			$sql = "SELECT * FROM tb_usuario WHERE rol_id = :id AND usuario_mail = :usuario_mail";
			//$sql = "CALL sp_lista_coordenadas(:id, :usuario_mail)";
			$database->query($sql);
	        $database->bind(':id', $data['rol_id']);
	        $database->bind(':usuario_mail', $data['mail']);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function actualizarEstado($data){		
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_usuario SET usuario_status = :usuario_status WHERE usuario_id = :usuario_id";
			$sql = "CALL sp_usuario_mantenimiento(@a_message,'estado',:usuario_id,'','','','','','',:usuario_status,'')";
			$database->query($sql);
			$database->bind(':usuario_status', $data['usuario_status']);
	        $database->bind(':usuario_id', $data['usuario_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function CountUsuarios($text){
		try {
			$database = new ConexionBD();

			$rol_id = $_SESSION['rol_id'];
			$not_in = '';

			if($rol_id != 1 && $rol_id == 3 ){
				$not_in = ' AND a.rol_id NOT IN (1) ';
			}else if ($rol_id != 1 && $rol_id == 2 ) {
				$not_in = ' AND a.rol_id NOT IN (1,2) ';
			}

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( b.rol_nombre LIKE :data OR a.usuario_mail LIKE :data OR a.usuario_nombre LIKE :data )';
			}

			$sql = "SELECT count(*) as total FROM tb_usuario a INNER JOIN tb_rol b ON a.rol_id = b.rol_id WHERE a.usuario_status = 1 ".$not_in. " ".$like;

			$database->query($sql);
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacion($offset, $pagesize, $text){
		try {
			$database = new ConexionBD();
			
			$rol_id = $_SESSION['rol_id'];
			$not_in = '';

			if($rol_id != 1 && $rol_id == 3 ){
				$not_in = ' AND a.rol_id NOT IN (1) ';
			}else if ($rol_id != 1 && $rol_id == 2 ) {
				$not_in = ' AND a.rol_id NOT IN (1,2) ';
			}

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( a.unidad_placa LIKE :data OR a.unidad_marca LIKE :data OR a.unidad_modelo LIKE :data OR a.unidad_color LIKE :data )';
			}

			$sql = "SELECT b.rol_id, b.rol_nombre , a.usuario_id, a.usuario_mail, a.usuario_nombre, a.usuario_status FROM tb_usuario a INNER JOIN tb_rol b ON a.rol_id = b.rol_id WHERE a.usuario_status = 1 ".$not_in." ".$like." ORDER BY a.usuario_id ASC LIMIT :offset, :pagesize";

			$database->query($sql);
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}


}
