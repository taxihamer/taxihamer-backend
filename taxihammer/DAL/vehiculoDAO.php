<?php
require_once'db.php';
require_once'character.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class vehiculoDAO{
	
	public function listaBy($unidad_id){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_unidad u INNER JOIN tb_tiposervicio s ON u.tiposervicio_id = s.tiposervicio_id WHERE u.unidad_id = :unidad_id";
			$sql = "CALL sp_lista_vehiculos_por_unidad(:unidad_id)";
			$database->query($sql);
			$database->bind(':unidad_id', $unidad_id);
	        $database->execute();
	        return $database->resultSet();

		}catch (Exception $e) {
			throw $e;
		}
	}

	public function lista(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT a.*, b.tiposervicio_nombre FROM tb_unidad a, tb_tiposervicio b WHERE a.tiposervicio_id = b.tiposervicio_id AND a.unidad_status = 1";
			$sql = "CALL sp_lista_vehiculos()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

		}catch (Exception $e) {
			throw $e;
		}
	}
	public function listaAsignada(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT a.*, b.tiposervicio_nombre FROM tb_unidad a, tb_tiposervicio b WHERE a.tiposervicio_id = b.tiposervicio_id AND a.unidad_status = 1 AND a.unidad_asignada = 0";
			$sql = "CALL sp_lista_vehiculos_asignados()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

		}catch (Exception $e) {
			throw $e;
		}
	}
	public function listaDropdown(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT a.*, b.tiposervicio_nombre FROM tb_unidad a, tb_tiposervicio b, tb_conductor c WHERE a.tiposervicio_id = b.tiposervicio_id AND a.unidad_status = 1 AND a.unidad_id <> c.unidad_id";
			$sql = "CALL sp_lista_dropdown_vehiculos()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

		}catch (Exception $e) {
			throw $e;
		}
	}
	public function deleteVehiculo($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_unidad SET unidad_status = 0 WHERE unidad_id = :unidad_id";
			$sql = "CALL sp_vehiculo_mantenimiento(@a_message,'delete',0,'','',0,'','','','',0,'','','',:unidad_id,0)";
			$database->query($sql);
			$database->bind(':unidad_id', $data['unidad_id']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function insertarVehiculo($data){
		//var_dump($data);exit;
			//echo '<pre>'; print_r($data); echo '</pre>'; exit;
		try {
			$database = new ConexionBD();
			/*$sql = "INSERT INTO tb_unidad (tiposervicio_id, unidad_caracteristicas, unidad_alias, unidad_status, unidad_placa, unidad_marca, unidad_modelo, unidad_color, unidad_fabricacion, unidad_fechasoat, unidad_fecharevtec, unidad_foto) VALUES (:tiposervicio_id, :unidad_caracteristicas, :unidad_alias, :unidad_status, :unidad_placa, :unidad_marca, :unidad_modelo, :unidad_color, :unidad_fabricacion, :unidad_fechasoat, :unidad_fecharevtec, :unidad_foto)";*/
			$sql = "CALL sp_vehiculo_mantenimiento(@a_message,'nuevo',:tiposervicio_id,:unidad_caracteristicas,:unidad_alias,:unidad_status,:unidad_placa,:unidad_marca,:unidad_modelo,:unidad_color,:unidad_fabricacion,:unidad_fechasoat,:unidad_fecharevtec,:unidad_foto,0,0)";
			$database->query($sql);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':unidad_caracteristicas', $data['unidad_caracteristicas']);
	        $database->bind(':unidad_alias', SanitizeCharacter($data['unidad_alias']));
	        $database->bind(':unidad_status', $data['unidad_status']);
	        $database->bind(':unidad_placa', $data['unidad_placa']);
	        $database->bind(':unidad_marca', $data['unidad_marca']);
	        $database->bind(':unidad_modelo', $data['unidad_modelo']);
	        $database->bind(':unidad_color', $data['unidad_color']);
	        $database->bind(':unidad_fabricacion', $data['unidad_fabricacion']);
	        $database->bind(':unidad_fechasoat', $data['unidad_fechasoat']);
	        $database->bind(':unidad_fecharevtec', $data['unidad_fecharevtec']);
	        $database->bind(':unidad_foto', $data['unidad_foto']);
	        $database->execute();
	        //$id = $database->lastInsertId();

	        //return array('status' => true, 'lastid' => $id);


	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function updateVehiculo($data){
		try {
			$database = new ConexionBD();
			/*$foto = '';
			 if(!empty($data['unidad_foto'])){
			 	$foto = ', unidad_foto = :unidad_foto ';
			 }*/
			//$sql = "UPDATE tb_unidad SET tiposervicio_id = :tiposervicio_id, unidad_caracteristicas = :unidad_caracteristicas, unidad_alias = :unidad_alias, unidad_status = :unidad_status, unidad_placa = :unidad_placa, unidad_marca = :unidad_marca, unidad_modelo = :unidad_modelo, unidad_color = :unidad_color, unidad_fabricacion = :unidad_fabricacion, unidad_fechasoat = :unidad_fechasoat, unidad_fecharevtec = :unidad_fecharevtec, unidad_foto = :unidad_foto WHERE unidad_id = :unidad_id";
			if(!empty($data['unidad_foto'])){
				$sql = "CALL sp_vehiculo_mantenimiento(@a_message,'update',:tiposervicio_id,:unidad_caracteristicas,:unidad_alias,:unidad_status,:unidad_placa,:unidad_marca,:unidad_modelo,:unidad_color,:unidad_fabricacion,:unidad_fechasoat,:unidad_fecharevtec,:unidad_foto,:unidad_id,0)";
			} else {
				$sql = "CALL sp_vehiculo_mantenimiento(@a_message,'update',:tiposervicio_id,:unidad_caracteristicas,:unidad_alias,:unidad_status,:unidad_placa,:unidad_marca,:unidad_modelo,:unidad_color,:unidad_fabricacion,:unidad_fechasoat,:unidad_fecharevtec,0,:unidad_id,0)";
			}
			$database->query($sql);
			$database->bind(':unidad_id', $data['unidad_id']);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':unidad_caracteristicas', $data['unidad_caracteristicas']);
	        $database->bind(':unidad_alias', SanitizeCharacter($data['unidad_alias']));
	        $database->bind(':unidad_status', $data['unidad_status']);
	        $database->bind(':unidad_placa', $data['unidad_placa']);
	        $database->bind(':unidad_marca', $data['unidad_marca']);
	        $database->bind(':unidad_modelo', $data['unidad_modelo']);
	        $database->bind(':unidad_color', $data['unidad_color']);
	        $database->bind(':unidad_fabricacion', $data['unidad_fabricacion']);
	        $database->bind(':unidad_fechasoat', $data['unidad_fechasoat']);
	        $database->bind(':unidad_fecharevtec', $data['unidad_fecharevtec']);
	       	if(!empty($data['unidad_foto'])){
	       		$database->bind(':unidad_foto', $data['unidad_foto']);	
	       	}
	        $database->execute();
	        return true;
	        
		}catch(Exception $e){
			throw $e;
		}
	}
	public function updateVehiculoSinFoto($data){
		
		

		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_unidad SET tiposervicio_id = :tiposervicio_id, unidad_caracteristicas = :unidad_caracteristicas, unidad_alias = :unidad_alias, unidad_status = :unidad_status, unidad_placa = :unidad_placa, unidad_marca = :unidad_marca, unidad_modelo = :unidad_modelo, unidad_color = :unidad_color, unidad_fabricacion = :unidad_fabricacion, unidad_fechasoat = :unidad_fechasoat, unidad_fecharevtec = :unidad_fecharevtec WHERE unidad_id = :unidad_id";
			$sql = "CALL sp_vehiculo_mantenimiento(@a_message,'update',:tiposervicio_id,:unidad_caracteristicas,:unidad_alias,:unidad_status,:unidad_placa,:unidad_marca,:unidad_modelo,:unidad_color,:unidad_fabricacion,:unidad_fechasoat,:unidad_fecharevtec,0,:unidad_id,0)";

			$database->query($sql);
			$database->bind(':unidad_id', $data['unidad_id']);
	        $database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':unidad_caracteristicas', $data['unidad_caracteristicas']);
	        $database->bind(':unidad_alias', SanitizeCharacter($data['unidad_alias']));
	        $database->bind(':unidad_status', $data['unidad_status']);
	        $database->bind(':unidad_placa', $data['unidad_placa']);
	        $database->bind(':unidad_marca', $data['unidad_marca']);
	        $database->bind(':unidad_modelo', $data['unidad_modelo']);
	        $database->bind(':unidad_color', $data['unidad_color']);
	        $database->bind(':unidad_fabricacion', $data['unidad_fabricacion']);
	        $database->bind(':unidad_fechasoat', $data['unidad_fechasoat']);
	        $database->bind(':unidad_fecharevtec', $data['unidad_fecharevtec']);
	        $database->execute();
	        return true;
	        
		}catch(Exception $e){
			throw $e;
		}
	}
	public function listBy($data){
		try {
			$database = new ConexionBD();
			/*if($data['tiposervicio_id'] != "" && $data['unidad_alias'] == ""){
				$sql = "SELECT a.*, b.tiposervicio_nombre FROM tb_unidad a, tb_tiposervicio b WHERE a.tiposervicio_id = b.tiposervicio_id AND a.unidad_status = 1 AND b.tiposervicio_id = :tiposervicio_id";
			}elseif($data['tiposervicio_id'] == "" && $data['unidad_alias'] !=""){
				$sql = "SELECT a.*, b.tiposervicio_nombre FROM tb_unidad a, tb_tiposervicio b WHERE a.tiposervicio_id = b.tiposervicio_id AND a.unidad_status = 1 AND a.unidad_alias LIKE :unidad_alias";
			}elseif($data['tiposervicio_id'] != "" && $data['unidad_alias'] !=""){
				$sql = "SELECT a.*, b.tiposervicio_nombre FROM tb_unidad a, tb_tiposervicio b WHERE a.tiposervicio_id = b.tiposervicio_id AND a.unidad_status = 1 AND a.unidad_alias LIKE :unidad_alias AND a.tiposervicio_id = :tiposervicio_id";
			}else{
				$sql = "SELECT a.*, b.tiposervicio_nombre FROM tb_unidad a, tb_tiposervicio b WHERE a.tiposervicio_id = b.tiposervicio_id AND a.unidad_status = 1";
			}*/

			$sql = "CALL sp_listar_vehiculos_por(:tiposervicio_id, :unidad_alias)";
			$database->query($sql);
			$database->bind(':tiposervicio_id', $data['tiposervicio_id']);
			$database->bind(':unidad_alias', $data['unidad_alias']);
			/*if($data['tiposervicio_id'] != ""){
				$database->bind(':tiposervicio_id', $data['tiposervicio_id']);
			}
			if($data['unidad_alias'] != ""){
				$database->bind(':unidad_alias', '%'.$data['unidad_alias'].'%');
			}*/
	        $database->execute();
	        return $database->resultSet();

		}catch (Exception $e) {
			throw $e;
		}
	}

	public function updateUnidadAsignada($data){
		
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_unidad SET unidad_asignada = :unidad_asignada WHERE unidad_id = :unida_id";
			$sql = "CALL sp_vehiculo_mantenimiento(@a_message,'asignar',0,'','',0,'','','','',0,'','','',:unidad_id,:unidad_asignada)";

			$database->query($sql);
	        $database->bind(':unidad_id', $data['unidad_id']);
	        $database->bind(':unidad_asignada', $data['unidad_asignada']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function validarPlaca($placa){
		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*) as total FROM tb_unidad WHERE unidad_placa = :unidad_placa";
			$sql = "CALL sp_validar_placa(:unidad_placa)";
			$database->query($sql);
			$database->bind(':unidad_placa', $placa);
	        $database->execute();
	        return $database->resultSet();

	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function CountVehiculos($select, $text){
		try {
			$database = new ConexionBD();
			$sqlin = '';
			if(!empty($select)){
				$sqlin = ' AND b.tiposervicio_id = :tiposervicio_id ';
			}

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( a.unidad_alias LIKE :data OR a.unidad_placa LIKE :data OR a.unidad_marca LIKE :data OR a.unidad_modelo LIKE :data OR a.unidad_color LIKE :data )';
			}

			$sql = "SELECT count(*) as total FROM tb_unidad a, tb_tiposervicio b WHERE a.tiposervicio_id = b.tiposervicio_id AND a.unidad_status = 1".$sqlin." ".$like;

			$database->query($sql);
			if(!empty($select)){
				 $database->bind(':tiposervicio_id', $select);
			}
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacion($offset, $pagesize, $select, $text){
		try {
			$database = new ConexionBD();
			$sqlin = '';

			if(!empty($select)){
				$sqlin = ' AND b.tiposervicio_id = :tiposervicio_id ';
			}
			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( a.unidad_alias LIKE :data OR a.unidad_placa LIKE :data OR a.unidad_marca LIKE :data OR a.unidad_modelo LIKE :data OR a.unidad_color LIKE :data )';
			}

			$sql = "SELECT a.*, b.tiposervicio_nombre FROM tb_unidad a INNER JOIN  tb_tiposervicio b ON a.tiposervicio_id = b.tiposervicio_id WHERE a.unidad_status = 1 ".$sqlin." ".$like." ORDER BY a.unidad_id DESC LIMIT :offset, :pagesize";

			$database->query($sql);
			if(!empty($select)){
				$database->bind(':tiposervicio_id', $select);
			}
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}

	}
}