<?php
require_once'db.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class recargasDAO{

	public function insertRecarga($data){

		try {
			$database = new ConexionBD();

			//$sql = "INSERT INTO tb_recarga (conductor_id, recarga_monto, recarga_num_oper, recarga_fecha, recarga_tipo, recarga_check, recarga_send) VALUES (:conductor_id, :recarga_monto, :recarga_num_oper, :recarga_fecha, :recarga_tipo ,:recarga_check, :recarga_send)";
			$sql = "CALL sp_recargas_mantenimiento(@a_message, 'nuevo', 0, :conductor_id, :recarga_monto, :recarga_num_oper, :recarga_fecha, :recarga_tipo ,:recarga_check, :recarga_send,0)";
			$database->query($sql);

	        $database->bind(':conductor_id', $data['conductor_id']);
	        $database->bind(':recarga_monto', $data['recarga_monto']);
	        $database->bind(':recarga_num_oper', $data['recarga_num_oper']);
	        $database->bind(':recarga_tipo', $data['recarga_tipo']);
	        $database->bind(':recarga_fecha', $data['recarga_fecha']);
	        $database->bind(':recarga_check', $data['recarga_check']);
	        $database->bind(':recarga_send', $data['recarga_send']);

	        $database->execute();
	        return $database->resultSet();
	        //$id = $database->lastInsertId();

	        //return array('status' => true, 'lastid' => $id);

		}catch(Exception $e){
			throw $e;
		}
	}

	public function listaEntidad(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_bancos WHERE banco_status = 1";
			$sql = "CALL sp_listar_bancos()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaBy($recarga_id){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_recarga WHERE recarga_status = 1 AND recarga_id = :recarga_id";
			$sql = "CALL sp_listar_recarga_por_id2(:recarga_id)";
			$database->query($sql);
			$database->bind(':recarga_id', $recarga_id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function lista(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT r.*, c.conductor_nombre,c.conductor_apellido,c.conductor_celular FROM tb_recarga r INNER JOIN tb_conductor c ON r.conductor_id = c.conductor_id WHERE r.recarga_status = 1 ORDER BY r.recarga_fecha DESC";
			$sql = "CALL sp_listar_recargas()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function actualizarEstado($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_recarga SET recarga_check = :recarga_check WHERE recarga_id = :recarga_id";
			$sql = "CALL sp_recargas_mantenimiento(@a_message, 'check', :recarga_id, 0, 0, 0, NOW(), 0 ,:recarga_check, 0,0)";
			$database->query($sql);
			$database->bind(':recarga_check', $data['recarga_check']);
	        $database->bind(':recarga_id', $data['recarga_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaByConductor($conductor_id, $inicio, $limite){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT r.*, b.banco_descripcion, DATE_FORMAT(r.recarga_fecha , '%Y-%m-%d') as fecha  FROM tb_recarga r LEFT JOIN tb_bancos b ON r.recarga_tipo = b.banco_id WHERE r.conductor_id = :conductor_id";
			$sql = "CALL sp_listar_recargas_por_conductor(:conductor_id, :inicio, :limite)";
			$database->query($sql);
			$database->bind(':conductor_id', $conductor_id);
			$database->bind(':inicio', $inicio);
			$database->bind(':limite', $limite);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function actualizarClick($recarga_id){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_recarga SET recarga_click = :recarga_click WHERE recarga_id = :recarga_id";
			$sql = "CALL sp_recargas_mantenimiento(@a_message, 'click', :recarga_id, '', '', '', '', '' ,'', '',:recarga_click)";
			$database->query($sql);
			$database->bind(':recarga_click', 0);
	        $database->bind(':recarga_id', $recarga_id);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listaByVerifi($recarga_id){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_recarga WHERE recarga_id = :recarga_id";
			$sql = "CALL sp_listar_recarga_por_id(:recarga_id)";
			$database->query($sql);
			$database->bind(':recarga_id', $recarga_id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	/************** Liquidacion ****************/

	public function listaLiquidacion(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT l.*, c.conductor_nombre,c.conductor_apellido,c.conductor_celular,c.conductor_dni FROM tb_liquidacion l INNER JOIN tb_conductor c ON l.conductor_id = c.conductor_id WHERE l.liquidacion_estado = 1 ORDER BY l.liquidacion_id DESC";
			$sql = "CALL sp_listar_liquidacion()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertLiquidacion($data){
		try {

			//var_dump($data);exit;

			$database = new ConexionBD();
			//$sql = "CALL sp_liquidacion_mantenimiento(@a_message,'nuevo', 0, :conductor_id, :liquidacion_monto, :liquidacion_fecha, :liquidacion_observacion, 0)";
			$sql = "INSERT INTO tb_liquidacion (conductor_id, liquidacion_monto, liquidacion_fecha, liquidacion_observacion) VALUES (:conductor_id, :liquidacion_monto, now(), :liquidacion_observacion)";
			$database->query($sql);

	        $database->bind(':conductor_id', $data['conductor_id']);
	        $database->bind(':liquidacion_monto', $data['liquidacion_monto']);
	        //$database->bind(':liquidacion_fecha', $data['liquidacion_fecha']);
	        $database->bind(':liquidacion_observacion', $data['liquidacion_observacion']);

	        $database->execute();
	        //return $database->resultSet();
	        $id = $database->lastInsertId();

	        return array('status' => true, 'lastid' => $id);

		}catch(Exception $e){
			throw $e;
		}
	}

	public function validarClickRecarga($recarga_id){

		try {
			$database = new ConexionBD();
			//$sql = "SELECT count(*) as total FROM movil.tb_recarga where recarga_check not in (1) AND recarga_id = :recarga_id AND recarga_status = 1";
			$sql = "CALL sp_validar_click_recarga(:recarga_id)";
			$database->query($sql);
			$database->bind(':recarga_id', $recarga_id);
	        $database->execute();
	        return $database->resultSet();
		} catch (Exception $e) {
			
		}
	}
}