<?php
require_once'db.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class tipoServicioDAO{
	public function lista(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_tiposervicio WHERE tiposervicio_status = 1";
			$sql = "CALL sp_listar_tipo_servicio(0)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listabancos(){
		try {
			$database = new ConexionBD();

			$sql = "SELECT * FROM tb_bancos";
			//$sql = "CALL sp_listar_tipo_servicio(:tiposervicio_id)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}



	public function listaTipoServiciosId($tiposervicio_id){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_tiposervicio WHERE tiposervicio_status = 1 AND tiposervicio_id = :tiposervicio_id";
			$sql = "CALL sp_listar_tipo_servicio(:tiposervicio_id)";
			$database->query($sql);
			$database->bind(':tiposervicio_id', $tiposervicio_id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function insertarTipoServicio($data){
		try{
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_tiposervicio (tiposervicio_nombre, tiposervicio_status, tiposervicio_factor, tiposervicio_aeropuerto, tiposervicio_reserva) VALUES (:tiposervicio_nombre, 1 , :tiposervicio_factor, :tiposervicio_aeropuerto, :tiposervicio_reserva)";
			//$sql = "CALL sp_tipo_servicio_mantenimiento(@a_message, 'nuevo',:tiposervicio_nombre, :tiposervicio_factor)";
			$sql = "CALL sp_tipo_servicio_mantenimiento(@a_message, 'nuevo',:tiposervicio_nombre, :tiposervicio_factor, 0,:tiposervicio_aeropuerto,:tiposervicio_reserva,0)";
			$database->query($sql);
	        $database->bind(':tiposervicio_nombre', $data['tiposervicio_nombre']);
	        $database->bind(':tiposervicio_factor', $data['tiposervicio_factor']);
	        $database->bind(':tiposervicio_aeropuerto', $data['tiposervicio_aeropuerto']);
	        $database->bind(':tiposervicio_reserva', $data['tiposervicio_reserva']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function updateTipoServicio($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_tiposervicio SET tiposervicio_nombre = :tiposervicio_nombre, tiposervicio_factor = :tiposervicio_factor, tiposervicio_aeropuerto = :tiposervicio_aeropuerto , tiposervicio_reserva = :tiposervicio_reserva WHERE tiposervicio_id = :tiposervicio_id";
			$sql = "CALL sp_tipo_servicio_mantenimiento(@a_message, 'update',:tiposervicio_nombre, :tiposervicio_factor, :tiposervicio_id, :tiposervicio_aeropuerto,:tiposervicio_reserva, 0)";
			$database->query($sql);
			$database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->bind(':tiposervicio_nombre', $data['tiposervicio_nombre']);
	        $database->bind(':tiposervicio_factor', $data['tiposervicio_factor']);
	        $database->bind(':tiposervicio_aeropuerto', $data['tiposervicio_aeropuerto']);
	        $database->bind(':tiposervicio_reserva', $data['tiposervicio_reserva']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}
	public function deleteTipoServicio($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_tiposervicio SET tiposervicio_status = 0 WHERE tiposervicio_id = :tiposervicio_id";
			$sql = "CALL sp_tipo_servicio_mantenimiento(@a_message, 'delete','', '', :tiposervicio_id, '','','')";
			$database->query($sql);
			$database->bind(':tiposervicio_id', $data['tiposervicio_id']);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function CountTipoServicio($text){
		try {
			$database = new ConexionBD();
			
			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( a.tiposervicio_nombre LIKE :data )';
			}

			$sql = "SELECT count(*) as total FROM tb_tiposervicio a WHERE a.tiposervicio_status = 1 ".$like;

			$database->query($sql);
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacion($offset, $pagesize, $text){
		try {
			$database = new ConexionBD();

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND ( a.tiposervicio_nombre LIKE :data )';
			}

			$sql = "SELECT a.tiposervicio_id, a.tiposervicio_nombre, a.tiposervicio_factor, a.tiposervicio_status, a.tiposervicio_reserva, a.tiposervicio_aeropuerto FROM tb_tiposervicio a WHERE a.tiposervicio_status = 1 ".$like." ORDER BY a.tiposervicio_id ASC LIMIT :offset, :pagesize";

			$database->query($sql);
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}
}