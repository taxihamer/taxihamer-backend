<?php
require_once'db.php';
require_once'character.php';

class calificacionDAO{

	public function insertarCalificacion($data){
		try{
			$database = new ConexionBD();

			/*$sql = "INSERT INTO tb_calificacion 
			(calificacion_tipoid, cliente_id, conductor_id, calificacion_value, servicio_id,comentario) 
			VALUES 
			(:calificacion_tipoid, :cliente_id, :conductor_id, :calificacion_value, :servicio_id,:comentario)";*/

			$sql = "CALL sp_calificacion_mantenimiento(@a_message, :tipo, :calificacion_tipoid, :cliente_id, :conductor_id, :calificacion_value, :servicio_id,:comentario)";
			$database->query($sql);
			$database->bind(':tipo','nuevo');
	        $database->bind(':calificacion_tipoid', $data['calificacion_tipoid']);
	        $database->bind(':cliente_id', $data['cliente_id']);
	        $database->bind(':conductor_id', $data['conductor_id']);
	        $database->bind(':servicio_id', $data['servicio_id']);
	        $database->bind(':calificacion_value', $data['calificacion_value']);
	        $database->bind(':comentario',SanitizeCharacter($data['mensaje']));	
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function calificacionPromedio($usuario_id,$tipo){
		try {
			$database = new ConexionBD();

			/*if ($tipo==1) {
				$sql = "SELECT (sum(c.calificacion_value)/(SELECT count(*) as totalcalificacion FROM tb_calificacion t WHERE t.conductor_id = :conductor_id AND t.calificacion_tipoid = :tipo)) AS promedio FROM tb_calificacion c WHERE c.conductor_id = :conductor_id AND c.calificacion_tipoid = :tipo";

				$database->query($sql);
				$database->bind(':conductor_id', $usuario_id);
		        $database->bind(':tipo', $tipo);
			}
			else{
				$sql = "SELECT (sum(c.calificacion_value)/(SELECT count(*) as totalcalificacion FROM tb_calificacion t WHERE t.cliente_id = :cliente_id AND t.calificacion_tipoid = :tipo)) AS promedio FROM tb_calificacion c WHERE c.cliente_id = :cliente_id AND c.calificacion_tipoid = :tipo";

			$database->query($sql);
			$database->bind(':cliente_id', $usuario_id);
	        $database->bind(':tipo', $tipo);
			}*/

			$sql = "CALL sp_calificacion_promedio(:cliente_id, :tipo)";
			$database->query($sql);
			$database->bind(':cliente_id', $usuario_id);
	        $database->bind(':tipo', $tipo);
			
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
	public function updateCalificacionflag($cliente_id,$cliente_calificacion){
		try{
			$database = new ConexionBD();

			/*$sql = "UPDATE tb_cliente SET
			cliente_calificacion_flag = :cliente_calificacion_flag
			WHERE cliente_id = :cliente_id";*/

			$sql = "CALL sp_clientes_mantenimiento(@a_message, :tipo, '', '', '', 0, '', '', '', 0, 0, 0, 0, 0, :cliente_id,0,0,0,0,:cliente_calificacion_flag)";
			$database->query($sql);
			$database->bind(':tipo','calificacion_flag');
			$database->bind(':cliente_id', $cliente_id);
	        $database->bind(':cliente_calificacion_flag', $cliente_calificacion);
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	/*COMIENZO DE LA FUNCION*/
	function obtenerCalficiacionPorServicio($idServicio){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_calificacion WHERE servicio_id = :servicio_id;";
			$sql = "CALL sp_calificacion_por_servicio(:servicio_id)";
			$database->query($sql);
			$database->bind(':servicio_id',$idServicio);
			$database->execute();
			return $database->resultSet();


		} catch (Exception $e) {
			throw new Exception($e);
			
		}
	}

	/*FIN DE LA FUNCION*/
}
	
