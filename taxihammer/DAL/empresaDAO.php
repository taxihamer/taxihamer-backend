<?php
require_once'db.php';
require_once'character.php';

class empresaDAO{

	public function listempresa(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM  tb_empresa WHERE empresa_estado = 1";
			$sql = "CALL sp_listar_empresas()";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function insertarEmpresa($data){
		try{
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_empresa (empresa_codigo, empresa_razonsocial, empresa_ruc, empresa_telefono, empresa_correo, empresa_contacto, empresa_estado, empresa_direccion)VALUES (:empresa_codigo, :empresa_razonsocial, :empresa_ruc, :empresa_telefono, :empresa_correo, :empresa_contacto, :empresa_estado, :empresa_direccion)";
			$sql = "CALL sp_empresa_mantenimiento(@a_message, :tipo, 0, :empresa_codigo, :empresa_razonsocial, :empresa_ruc, :empresa_telefono, :empresa_correo, :empresa_contacto, :empresa_direccion)";
			$database->query($sql);
			$database->bind(':tipo', 'nuevo');
	        $database->bind(':empresa_codigo', $data['empresa_codigo']);
	        $database->bind(':empresa_razonsocial', $data['empresa_razonsocial']);
	        $database->bind(':empresa_ruc', $data['empresa_ruc']);
	        $database->bind(':empresa_telefono', $data['empresa_telefono']);
	        $database->bind(':empresa_correo', $data['empresa_correo']);
	        $database->bind(':empresa_contacto', $data['empresa_contacto']);
	        //$database->bind(':empresa_estado', 1);
	        $database->bind(':empresa_direccion',SanitizeCharacter($data['empresa_direccion']));
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateEmpresa($data){
		try {
			$database = new ConexionBD();
			//$sql = "UPDATE tb_empresa SET empresa_codigo = :empresa_codigo, empresa_razonsocial = :empresa_razonsocial, empresa_ruc = :empresa_ruc, empresa_telefono = :empresa_telefono, empresa_correo = :empresa_correo, empresa_contacto = :empresa_contacto , empresa_direccion = :empresa_direccion WHERE empresa_id = :empresa_id";
			$sql = "CALL sp_empresa_mantenimiento(@a_message, :tipo, :empresa_id, :empresa_codigo, :empresa_razonsocial, :empresa_ruc, :empresa_telefono, :empresa_correo, :empresa_contacto, :empresa_direccion)";
			$database->query($sql);
			$database->bind(':tipo', 'update');
			$database->bind(':empresa_id', $data['empresa_id']);
	        $database->bind(':empresa_codigo', $data['empresa_codigo']);
	        $database->bind(':empresa_razonsocial', $data['empresa_razonsocial']);
	        $database->bind(':empresa_ruc', $data['empresa_ruc']);
	        $database->bind(':empresa_telefono', $data['empresa_telefono']);
	        $database->bind(':empresa_correo', $data['empresa_correo']);
	        $database->bind(':empresa_contacto', $data['empresa_contacto']);
	         $database->bind(':empresa_direccion', SanitizeCharacter($data['empresa_direccion']));
	        $database->execute();
	        return true;

		}catch(Exception $e){
			throw $e;
		}
	}

	public function deleteEmpresa($data){
		try {
			$database = new ConexionBD();

			//$sql = "UPDATE tb_empresa SET empresa_estado = :empresa_estado WHERE empresa_id = :empresa_id";
			$sql = "CALL sp_empresa_mantenimiento(@a_message, :tipo, :empresa_id, '', '' , '' , '' , '' , '' , '')";
			$database->query($sql);
			$database->bind(':tipo', 'delete');
			//$database->bind(':empresa_estado', 0);
	        $database->bind(':empresa_id', $data['empresa_id']);
	        $database->execute();
	        return true;
	    } catch (Exception $e) {
        	throw $e;
        }
	}

	public function listempresaBy($empresa_id){
		try {
			$database = new ConexionBD();

			$sql = "SELECT * FROM  tb_empresa WHERE empresa_estado = 1 AND empresa_id = :empresa_id";
			$database->query($sql);
			$database->bind(':empresa_id', $empresa_id);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function CountEmpresas($text){
		try {
			$database = new ConexionBD();

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND (empresa_codigo LIKE :data OR empresa_ruc LIKE :data OR empresa_razonsocial LIKE :data OR empresa_telefono LIKE :data OR empresa_correo LIKE :data)';
			}

			$sql = "SELECT count(*) as total FROM  tb_empresa WHERE empresa_estado = 1 ".$like;

			$database->query($sql);
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacion($offset, $pagesize, $text){
		try {
			$database = new ConexionBD();

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'AND (empresa_codigo LIKE :data OR empresa_ruc LIKE :data OR empresa_razonsocial LIKE :data OR empresa_telefono LIKE :data OR empresa_correo LIKE :data)';
			}

			$sql = "SELECT empresa_codigo, empresa_ruc, empresa_razonsocial, empresa_telefono, empresa_correo, empresa_id FROM tb_empresa WHERE empresa_estado = 1 ".$like." ORDER BY empresa_id DESC LIMIT :offset, :pagesize";

			$database->query($sql);
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}

	}

	public function validarRuc($empresa_ruc){
		try {
			$database = new ConexionBD();

			$sql = "SELECT count(*) as total FROM tb_empresa WHERE empresa_ruc = :empresa_ruc";
			$database->query($sql);
			$database->bind(':empresa_ruc', $empresa_ruc);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}
}