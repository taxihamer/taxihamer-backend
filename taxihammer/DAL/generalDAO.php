<?php
require_once'db.php';

class generalDAO{

	public function updateCorreo($data){
		try{
			$database = new ConexionBD();
			//$sql = "UPDATE tb_configuracion SET config_server_smtp = :config_server_smtp, config_puerto_smtp = :config_puerto_smtp, config_usuaro_smtp = :config_usuaro_smtp,config_password_smtp = :config_password_smtp WHERE config_id = :config_id";

			$sql = "CALL sp_configuracion_mantenimiento(@a_message, :tipo, :config_id ,'', '', '', 0, 0, 0, 0, 0 , 0, 0, 0, 0, 0 , 0, 0 ,0,:config_server_smtp,:config_puerto_smtp,0,:config_usuaro_smtp,:config_password_smtp,0,0,0,0)";
			
			$database->query($sql);
			$database->bind(':tipo', 'update_correo');
			$database->bind(':config_id', $data['config_id']);

			$database->bind(':config_server_smtp', $data['config_server_smtp']);
			$database->bind(':config_puerto_smtp', $data['config_puerto_smtp']);

			$database->bind(':config_usuaro_smtp', $data['config_usuaro_smtp']);
			$database->bind(':config_password_smtp', $data['config_password_smtp']);

			$database->execute();
			return true;
		}catch(Exception $e){
			throw $e;
		}
	}

	public function insertar($data){

		try{
			$database = new ConexionBD();
			//$sql = "INSERT INTO tb_configuracion (email, telefono, web, tipo_tarifa, tarifa_minima, factor_tarifa_km, factor_tarifa_tiempo, config_costo_co, tarifa_minima_m, factor_empresarial, factor_particular) VALUES (:email, :telefono, :web, :tipo_tarifa, :tarifa_minima, :factor_tarifa_km, :factor_tarifa_tiempo, :config_costo_co, :tarifa_minima_m, :factor_empresarial, :factor_particular)";
			$sql = "CALL sp_configuracion_mantenimiento(@a_message, :tipo, 0 ,:email, :telefono, :web, :tipo_tarifa, :tarifa_minima, :factor_tarifa_km, :factor_tarifa_tiempo, :config_costo_co, '', :tarifa_minima_m, :factor_empresarial, :factor_particular, '', '', '', '','','','','','','','','','')";

			$database->query($sql);
			$database->bind(':tipo', 'nuevo');
			$database->bind(':email', $data['email']);
			$database->bind(':telefono', $data['telefono']);
			$database->bind(':web', $data['web']);
			$database->bind(':tipo_tarifa', $data['tipo_tarifa']);
			$database->bind(':tarifa_minima', $data['tarifa_minima']);
			$database->bind(':factor_tarifa_km', $data['factor_tarifa_km']);
			$database->bind(':factor_tarifa_tiempo', $data['factor_tarifa_tiempo']);
			$database->bind(':config_costo_co', $data['config_costo_co']);
			$database->bind(':tarifa_minima_m', $data['tarifa_minima_m']);
			$database->bind(':factor_empresarial', $data['factor_empresarial']);
			$database->bind(':factor_particular', $data['factor_particular']);
			$database->execute();

			return true;
		}catch(Exception $e){
			throw $e;
		}
	}

	public function update($data){

		try{
			$database = new ConexionBD();

			$sql = "CALL sp_configuracion_mantenimiento(@a_message, :tipo, :config_id ,:email, :telefono, :web, :tipo_tarifa, :tarifa_minima, :factor_tarifa_km, :factor_tarifa_tiempo, :config_costo_co, 0, :tarifa_minima_m, :factor_empresarial, :factor_particular, :tarifa_minima_empresarial , :factor_tarifa_km_empresarial, :factor_tarifa_tiempo_empresarial, 0,'',0,0,'','',:aeropuertoO,:config_comision_tipo,:config_comision_total,:aeropuertoD)";
			/*$sql = "UPDATE tb_configuracion SET email = :email, telefono = :telefono, web = :web, tipo_tarifa = :tipo_tarifa,config_costo_co = :config_costo_co , 

				factor_particular = :factor_particular,
				tarifa_minima = :tarifa_minima, 
				factor_tarifa_km = :factor_tarifa_km, 
				factor_tarifa_tiempo = :factor_tarifa_tiempo,
				
				factor_empresarial = :factor_empresarial,
				tarifa_minima_empresarial = :tarifa_minima_empresarial,
				factor_tarifa_km_empresarial = :factor_tarifa_km_empresarial,
				factor_tarifa_tiempo_empresarial = :factor_tarifa_tiempo_empresarial,
				
				tarifa_minima_m = :tarifa_minima_m ,

				aeropuertoO = :aeropuertoO,
				aeropuertoD = :aeropuertoD,

				config_comision_tipo = :config_comision_tipo,
				config_comision_total =:config_comision_total

				  WHERE config_id = :config_id";*/


			$database->query($sql);
			$database->bind(':tipo', 'update');
			$database->bind(':config_id', $data['config_id']);
			$database->bind(':email', $data['email']);
			$database->bind(':telefono', $data['telefono']);
			$database->bind(':web', $data['web']);
			$database->bind(':tipo_tarifa', $data['tipo_tarifa']);
			$database->bind(':tarifa_minima', $data['tarifa_minima']);
			$database->bind(':factor_tarifa_km', $data['factor_tarifa_km']);
			$database->bind(':factor_tarifa_tiempo', $data['factor_tarifa_tiempo']);
			$database->bind(':config_costo_co', $data['config_costo_co']);
			$database->bind(':tarifa_minima_m', $data['tarifa_minima_m']);

			$database->bind(':factor_empresarial', $data['factor_empresarial']);
			$database->bind(':factor_particular', $data['factor_particular']);

			$database->bind(':tarifa_minima_empresarial', $data['tarifa_minima_empresarial']);
			$database->bind(':factor_tarifa_km_empresarial', $data['factor_tarifa_km_empresarial']);
			$database->bind(':factor_tarifa_tiempo_empresarial', $data['factor_tarifa_tiempo_empresarial']);

			$database->bind(':aeropuertoO', $data['aeropuertoO']);
			$database->bind(':aeropuertoD', $data['aeropuertoD']);

			$database->bind(':config_comision_tipo', $data['config_comision_tipo']);
			$database->bind(':config_comision_total', $data['config_comision_total']);
			
			$database->execute();
			
			return true;
		}catch(Exception $e){
			throw $e;
		}
	}
	public function lista(){
		try{
			$database = new ConexionBD();
			//sql = "SELECT * FROM tb_configuracion";
			$sql = "CALL sp_lista_general_config(0)";
			$database->query($sql);
			$database->execute();

			return $database->resultSet();
		}catch(Exception $e){
			throw $e;
		}
	}

	public function listaBy($config_id){
		try{
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_configuracion WHERE config_id = :config_id";
			$sql = "CALL sp_lista_general_config(:config_id)";
			$database->query($sql);
			$database->bind(':config_id', $config_id);
			$database->execute();

			return $database->resultSet();
		}catch(Exception $e){
			throw $e;
		}
	}

	public function updateradio($radio){
		try{
			$database = new ConexionBD();
			$sql = "UPDATE tb_configuracion SET radio = :radio where config_id = 1";
			$database->query($sql);
			$database->bind(':radio', $radio);
			$database->execute();
			return true;
		}catch(Exception $e){
			throw $e;
		}
	}

	public function listKeyMoviles(){
		try{
			$database = new ConexionBD();
			//$sql = "SELECT * FROM tb_api_key WHERE api_estado = 1";
			$sql = "CALL sp_listar_key_moviles()";
			$database->query($sql);
			$database->execute();

			return $database->resultSet();
		}catch(Exception $e){
			throw $e;
		}
	}

	public function showtables($tipo,$tables){
		try{
			$database = new ConexionBD();
			if($tipo == 1){
				$sql = "SHOW TABLES";
				$database->query($sql);
				$database->execute();
				return $database->resultSet();
			}else{
				$sql = "truncate table ".$tables;
				$database->query($sql);
				$database->execute();
				return true;
			}
		}catch(Exception $e){
			throw $e;
		}
	}

	public function CountGeneral($text){
		try {
			$database = new ConexionBD();
			
			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'WHERE ( a.web LIKE :data OR a.telefono LIKE :data OR a.email LIKE :data )';
			}

			$sql = "SELECT count(*) as total FROM tb_configuracion a ".$like;

			$database->query($sql);
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	public function listaPaginacion($offset, $pagesize, $text){
		try {
			$database = new ConexionBD();

			$like = '';
			if(!empty($text)){
				$var = str_replace(" ","%",$text);
				$like = 'WHERE ( a.web LIKE :data OR a.telefono LIKE :data OR a.email LIKE :data)';
			}

			$sql = "SELECT a.config_id, a.email , a.web, a.telefono, a.radio, a.tipo_tarifa FROM tb_configuracion a ".$like." ORDER BY a.config_id ASC LIMIT :offset, :pagesize";

			$database->query($sql);
			if(!empty($text)){
				$database->bind(':data', '%'.$var.'%');	
			}
			$database->bind(':offset', $offset);
			$database->bind(':pagesize', $pagesize);
	        $database->execute();
	        return $database->resultSet();

		} catch (Exception $e) {
			throw $e;
		}
	}

	function encriptar($cadena){
	    $key='';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
	    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));
	    return $encrypted; //Devuelve el string encriptado
 	}

 	function desencriptar($cadena){
	     $key='';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
	     $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($cadena), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
	    return $decrypted;  //Devuelve el string desencriptado
	}

	function encrypt($string, $key) 
	{
	   $result = '';
	   for($i=0; $i<strlen($string); $i++) {
	      $char = substr($string, $i, 1);
	      $keychar = substr($key, ($i % strlen($key))-1, 1);
	      $char = chr(ord($char)+ord($keychar));
	      $result.=$char;
	   }
	   return base64_encode($result);
	}

	function decrypt($string, $key) {
	   $result = '';
	   $string = base64_decode($string);
	   for($i=0; $i<strlen($string); $i++) {
	      $char = substr($string, $i, 1);
	      $keychar = substr($key, ($i % strlen($key))-1, 1);
	      $char = chr(ord($char)-ord($keychar));
	      $result.=$char;
	   }
	   return $result;
	}
}