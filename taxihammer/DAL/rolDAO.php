<?php
require_once'db.php';
class rolDAO{

	public function listrol(){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_rol";
			$sql = "CALL sp_listar_rol(0)";
			$database->query($sql);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

	public function updaterol($data){
		try {
			$database = new ConexionBD();

			$sql = "UPDATE tb_rol SET menurol_id = :menurol_id WHERE rol_id = :rol_id";
			$database->query($sql);
	        $database->bind(':rol_id', $data['rol_id']);
	        $database->bind(':menurol_id', $data['menurol_id']);
	        $database->execute();
	        return true;

	    } catch (Exception $e) {
            throw $e;
        }
	}

	public function listmenurolid($data){
		try {
			$database = new ConexionBD();

			//$sql = "SELECT * FROM tb_rol WHERE rol_id = :rol_id";
			$sql = "CALL sp_listar_rol(:rol_id)";
			$database->query($sql);
			$database->bind(':rol_id', $data);
	        $database->execute();
	        return $database->resultSet();

        } catch (Exception $e) {
        	throw $e;
        }
	}

}