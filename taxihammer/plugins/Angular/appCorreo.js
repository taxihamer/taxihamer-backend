var app = angular.module('myappCorreo', ['datatables','gm','file-model','datatables.columnfilter','kendo.directives','ui.bootstrap']);

app.service('filteredListService', function () {
     
    this.searched = function (valLists,toSearch) {
        return _.filter(valLists, 
        function (i) {
            /* Search Text in all 3 fields */
            return searchUtil(i, toSearch);
        });        
    };
    
    this.paged = function (valLists,pageSize)
    {
        retVal = [];
        for (var i = 0; i < valLists.length; i++) {
            if (i % pageSize === 0) {
                retVal[Math.floor(i / pageSize)] = [valLists[i]];
            } else {
                retVal[Math.floor(i / pageSize)].push(valLists[i]);
            }
        }
        return retVal;
    };
 
});

app.controller('correosController', function($scope, $http,DTOptionsBuilder, DTColumnBuilder) {
  
  $scope.showModal = false;
  $scope.tiposeguridad = "1";
  // Load all available tasks 
  $scope.usuario = '';
  $scope.password = '';
  $scope.puerto = 0;
  $scope.servidor = '';
  $scope.total = 0;

  $scope.tipo_edit = 2;
  $scope.tipo = 1;

  $scope.servidor_edit = '';
  $scope.puerto_edit = 0;
  $scope.password_edit = '';
  $scope.usuario_edit = '';

  getCorreos();
  function getCorreos(){

      $scope.correos = {}; 
      $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withDisplayLength(10)
      .withPaginationType('full_numbers')
      .withOption("order", [
                    [0, "desc"]
        ])
      .withOption('bLengthChange', false);

    var request = $http({
                    method: "post",
                    url: "../../BL/generalController.php",
                    data: {lista_correo: 'lista_correo'},
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });

      request.success(function (data) {
      	console.log(data);

        $.each(data, function( key, value ) {
          if(value.config_tipo_seguridad == 1){
            data[key].smtp = 'Tls';
          }
        });

        console.log(data);
      	 $scope.total = data.length;
         $scope.correos = data;
      });
      request.error(function(data, status){
          console.log('error'+ status);
      });
  };

  $scope.addCorreos = function (servidor,puerto,selecl,usuario,password,tipo) {

    var requestCorreos = $http({
                    method: "post",
                    url: "../../BL/generalController.php",
                    data: {
                        lista_add_config: 'lista_add_config',
                        servidor: servidor,
                        puerto:puerto,
                        usuario:usuario,
                        password:password
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
    requestCorreos.success(function (data) {
      if(tipo != 1){
        $scope.showModal = false;
      }
      getCorreos();
    });
  };

  $scope.llenar = function(id,indice){

    var requestlistCo = $http({
                  method: "post",
                  url: "../../BL/generalController.php",
                  data: {
                      lista_correos_modal: 'lista_correos_modal',
                      correo:id
                        },
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                  });
    requestlistCo.success(function (data) {

          $scope.servidor_edit = data[0].config_server_smtp;
          $scope.puerto_edit = parseInt(data[0].config_puerto_smtp);
          $scope.password_edit = data[0].config_password_smtp;
          $scope.usuario_edit = data[0].config_usuaro_smtp;

          $scope.showModal = !$scope.showModal;
    });  	
  }

});

app.directive('modal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true){
            $(element).modal('show');
          }
          else
          {
            $(element).modal('hide');
          }
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });