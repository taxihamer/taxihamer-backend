var app = angular.module('myappPuntos', ['datatables','gm','file-model','datatables.columnfilter','kendo.directives','ui.bootstrap']);

app.service('filteredListService', function () {
     
    this.searched = function (valLists,toSearch) {
        return _.filter(valLists, 
        function (i) {
            /* Search Text in all 3 fields */
            return searchUtil(i, toSearch);
        });        
    };
    
    this.paged = function (valLists,pageSize)
    {
        retVal = [];
        for (var i = 0; i < valLists.length; i++) {
            if (i % pageSize === 0) {
                retVal[Math.floor(i / pageSize)] = [valLists[i]];
            } else {
                retVal[Math.floor(i / pageSize)].push(valLists[i]);
            }
        }
        return retVal;
    };
 
});

app.controller('reportespuntosController',function($scope, $http, $filter, filteredListService){
    
  $scope.currentPage = 1;
  $scope.totalItems = 0;
  $scope.pageSize = 10;
  $scope.searchText = '';
  $scope.reverse = false;

  $scope.tipoUsuario = [
        { "key": "1", "value": "Clientes"},
        { "key": "2", "value": "Conductores"}
    ];

  $scope.tipoUsuario_a = "1";

  getReporpuntos();
  function getReporpuntos() {

        console.log($scope.tipoUsuario_a);

        var requestReportepuntos = $http({
            method: "post",
            url: "../../BL/puntosController.php",
            data: {
                page: $scope.currentPage,
                size: $scope.pageSize,
                search: $scope.searchText,
                select: $scope.tipoUsuario_a,
                listar_reporte_puntos: 'listar_reporte_puntos'
                  },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });

        requestReportepuntos.success(function (data) {

            console.log(data);
           if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
            $scope.totalItems = data.totalCount;
            $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
            $scope.endItem = $scope.currentPage * $scope.pageSize;
            if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

            $scope.reportesConductores = data.Request;
        });  
  }

  $scope.pageChanged = function() {
      getReporpuntos();
  }
  $scope.pageSizeChanged = function() {
      $scope.currentPage = 1;
      getReporpuntos();
  }
  $scope.searchTextChangedComb = function(){
      $scope.currentPage = 1;
      getReporpuntos();
  }

  $scope.searchTextChanged = function() {
      $scope.currentPage = 1;
      getReporpuntos();
  }

  $scope.resetAll = function () {
    $scope.Header = ['','',''];
  }

  $scope.sort = function (sortBy) {
      $scope.resetAll();

      $scope.columnToOrder = sortBy;

      $scope.filteredList = $filter('orderBy')($scope.filteredList, $scope.columnToOrder, $scope.reverse);

      if ($scope.reverse) iconName = 'glyphicon glyphicon-chevron-up';
      else iconName = 'glyphicon glyphicon-chevron-down';

      console.log(sortBy);
      /*if (sortBy === 'tiposervicio_nombre') {
        $scope.Header[0] = iconName;
      }else if(sortBy === 'conductor_calificacion'){
        $scope.Header[1] = iconName;
      }else{
        $scope.Header[2] = iconName;
      }*/

     $scope.reverse = !$scope.reverse;
     //getData();
  };  

});
