//Define an angular module for our app
var app2 = angular.module('myapp', ['datatables','gm','file-model','datatables.columnfilter','kendo.directives','ui.bootstrap']);

app2.service('filteredListService', function () {
     
    this.searched = function (valLists,toSearch) {
        return _.filter(valLists, 
        function (i) {
            /* Search Text in all 3 fields */
            return searchUtil(i, toSearch);
        });        
    };
    
    this.paged = function (valLists,pageSize)
    {
        retVal = [];
        for (var i = 0; i < valLists.length; i++) {
            if (i % pageSize === 0) {
                retVal[Math.floor(i / pageSize)] = [valLists[i]];
            } else {
                retVal[Math.floor(i / pageSize)].push(valLists[i]);
            }
        }
        return retVal;
    };
 
});

app2.factory('MantFactory', function ($http) {

    var MantFactoryGeneral = {}
    MantFactoryGeneral.getListados = function (array) {
      
        return $http({
                method: "post",
                url: array.baseUrl,
                data: {
                        variables : array,
                        list: array.list
                      },
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                });
    };

    MantFactoryGeneral.getEditado = function(array){

         return $http({
                method: "post",
                url: array.baseUrl,
                data: {
                        variables : array,
                        list: array.list
                      },
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                });
    };

    MantFactoryGeneral.getEstado = function(array){

         return $http({
                method: "post",
                url: array.baseUrl,
                data: {
                        variables : array,
                        list: array.list
                      },
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                });
    };

    MantFactoryGeneral.getInsertar = function(array){

         return $http({
                method: "post",
                url: array.baseUrl,
                data: {
                        variables : array,
                        list: array.list
                      },
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                });
    };

    return MantFactoryGeneral;

});

app2.service('MantService',function(MantFactory){

    this.getlistados = function(array) {
       return MantFactory.getListados(array);
    }

    this.geteditado = function(array) {
      return MantFactory.getEditado(array);
    }

    this.getestado = function(array) {
      return MantFactory.getEstado(array);
    }

    this.getinsertar = function(array) {
      return MantFactory.getInsertar(array);
    }

});

app2.controller('UsuarioController', function($scope, MantService){

    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;
    $scope.reverse = false;
    $scope.searchText = '';
    var Url = 'BL/usuarioController.php';

    getUsuarios();

    function getUsuarios(){

        var listado = {
          page: $scope.currentPage,
          size: $scope.pageSize,
          list: 'lista_usuario',
          baseUrl: Url,
          search: $scope.searchText
        };

        MantService.getlistados(listado)
          .then(function onSuccess(response){
            if (response.status == 200){
                
              if(response.data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              $scope.totalItems = response.data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

              $.each(response.data.Request, function( key, value ) {

                if(value.usuario_status == 1){
                  response.data.Request[key].enabled = true;
                }else{
                  response.data.Request[key].enabled = false;
                }

              });
              $scope.usuarios = response.data.Request;
            }
          }).
          catch(function onError(response){
             console.warn('Error en la BD :' + response);
          });
          /*.success(function (data) {

              if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              $scope.totalItems = data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

              $.each(data.Request, function( key, value ) {

                if(value.usuario_status == 1){
                  data.Request[key].enabled = true;
                }else{
                  data.Request[key].enabled = false;
                }

              });
              $scope.usuarios = data.Request;

          })
        .error(function (error) {
            console.warn('Error en la BD :' + error);
        });*/
    }

    $scope.Editar = function(id){

      var editar = {
        id: id,
        list: 'editado_usuario',
        baseUrl: Url
      };
      MantService.geteditado(editar)
        .success(function (data) {

            $('#nombre1').val(data.usuario_nombre);
            $('#correo1').val(data.usuario_mail);
            $('#pass11').val(data.usuario_password);
            $('#pass22').val(data.usuario_password);
            $('#latitud1').val(data.usuario_latitud);
            $('#longitud1').val(data.usuario_longitud);
            $('#status1').val(data.usuario_status);
            $('#permisos1').val(data.rol_id);
            $('#idusuario').val(data.usuario_id);

        }).
        error(function (error) {
            console.warn('Error en la BD :' + error);
        });
    }

    $scope.changeEstado = function(id,estado){

      var estado = (estado == false)?0:1;
      var update = {
        id: id,
        list: 'estado_usuario',
        estado: estado,
        baseUrl: Url
      };
      MantService.getestado(update)
      .success(function (data) {
        if(data == true)
          getUsuarios();
      }).
      error(function (error) {
        console.warn('Error en la BD :' + error);
      });
    }

    $scope.pageChanged = function() {
      getUsuarios();
    }

    $scope.pageSizeChanged = function() {
        $scope.currentPage = 1;
        getUsuarios();
    }

    $scope.searchTextChanged = function(){
        $scope.currentPage = 1;
        getUsuarios();
    }

    $scope.deleteC = function(id){
      deleteGeneral(id,Url,getUsuarios);
    }
});

app2.controller('GeneralController', function($scope, MantService){

    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;
    $scope.reverse = false;
    $scope.searchText = '';
    var Url = 'BL/generalController.php';

    getGeneral();

    function getGeneral(){

        var listado = {
          page: $scope.currentPage,
          size: $scope.pageSize,
          list: 'lista_general',
          baseUrl: Url,
          search: $scope.searchText
        };

        MantService.getlistados(listado)
          .success(function (data) {

              if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              $scope.totalItems = data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

               $.each(data.Request, function( key, value ) {

                if(value.tipo_tarifa == 1){
                  data.Request[key].texto = 'Zonas';
                }else{
                  data.Request[key].texto = 'Kilometros';
                }

              });

              $scope.generales = data.Request;

          })
          .error(function (error) {
            console.warn('Error en la BD :' + error);
          });
    }

    $scope.Editar = function(id){

      var editar = {
        id: id,
        list: 'editado_general',
        baseUrl: Url
      };
      MantService.geteditado(editar)
        .success(function (data) {

          $("#mtncd").show();
          $('#comisionid option:eq('+data.config_comision_tipo+')').attr('selected', 'selected');
          $("#montoconductor").val(data.config_comision_total);
          $("#incraeroO").val(parseInt(data.aeropuertoO));
          $("#incraeroD").val(parseInt(data.aeropuertoD));

          var sliderr = '';
          if($('#slider > .slider').length) { 
              console.log('ya existe el slider');
          } else {

            sliderr = new Slider("#ex14", {
              ticks: [1, 2, 3, 4, 5],
              ticks_labels: ["1 Km", "2 Km", "3 Km", "4 Km", "5 Km"],
              value:parseInt(data.radio)
            });

          }

          $('#correoedit').val(data.email);
          $('#telefonoedit').val(data.telefono);
          $('#webedit').val(data.web);
          $('#costocanceledit').val(data.config_costo_co)
          $('#idconfig').val(data.config_id);

            if(data.tipo_tarifa == 1){
              document.getElementById("zonas").checked = true;
              $('#group-kilometrajedit').hide();
              clearTarifa();
            }else{
              document.getElementById("kilom").checked = true;

              $('#checkboxFactorEditEmpresarial').prop('checked',(data.factor_empresarial == 1 ? true : false));
              $('#checkboxFactorEditParticular').prop('checked',(data.factor_particular == 1 ?true : false));

              //Ocultamos DIVS correspondientes
              if(data.factor_empresarial != 1) $('#contentEditEmpresarial').hide();
               if(data.factor_particular != 1) $('#contentEditParticular').hide();


              $('#group-kilometrajedit').show();
          
              $('#tarifaminimaedit').val(data.tarifa_minima_m);

              //Particular
              $('#tarifamineditParticular').val(data.tarifa_minima);
              $('#factkmeditParticular').val(data.factor_tarifa_km);
              $('#factimeditParticular').val(data.factor_tarifa_tiempo);

              //Empresarial
              $('#tarifamineditEmpresarial').val(data.tarifa_minima_empresarial);
              $('#factkmeditEmpresarial').val(data.factor_tarifa_km_empresarial);
              $('#factimeditEmpresarial').val(data.factor_tarifa_tiempo_empresarial);
            }

        }).
        error(function (error) {
            console.warn('Error en la BD :' + error);
        });
    }

    $scope.pageChanged = function() {
      getGeneral();
    }

    $scope.pageSizeChanged = function() {
        $scope.currentPage = 1;
        getGeneral();
    }

    $scope.searchTextChanged = function(){
        $scope.currentPage = 1;
        getGeneral();
    }
});

app2.controller('TipoServicioController',function($scope, MantService){

    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;
    $scope.reverse = false;
    $scope.searchText = '';
    var Url = 'BL/tipoServicioController.php';

    getTiposServicios();

    function getTiposServicios(){

        var listado = {
          page: $scope.currentPage,
          size: $scope.pageSize,
          list: 'lista_tipo_servicio',
          baseUrl: Url,
          search: $scope.searchText
        };

        MantService.getlistados(listado)
          .then(function onSuccess(response){
            
            if (response.status == 200){
                
              if(response.data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              $scope.totalItems = response.data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

              $scope.tiposervs = response.data.Request;
            }
          }).
          catch(function onError(response){
             console.warn('Error en la BD :' + response);
          });
    }

    $scope.Editar = function(id){

      var editar = {
        id: id,
        list: 'editado_tipo_servicio',
        baseUrl: Url
      };
      MantService.geteditado(editar)
        .then(function onSuccess(response){
        
            $('#idservicio').val(response.data.tiposervicio_id);
            $('#servicio').val(response.data.tiposervicio_nombre);
            $('#factorEdit').val(response.data.tiposervicio_factor);
            $('#aeropuertoEdit').val(response.data.tiposervicio_aeropuerto);
            $('#reservaEdit').val(response.data.tiposervicio_reserva);

        }).
        catch(function onError(response){
            console.warn('Error en la BD :' + response);
        });
    }

    $scope.pageChanged = function() {
      getTiposServicios();
    }

    $scope.pageSizeChanged = function() {
        $scope.currentPage = 1;
        getTiposServicios();
    }

    $scope.searchTextChanged = function(){
        $scope.currentPage = 1;
        getTiposServicios();
    }

    $scope.deleteC = function(id){
      deleteGeneral(id,Url,getTiposServicios);
    }
});

app2.controller('UnidadesController',function($scope, MantService){

    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;
    $scope.reverse = false;
    $scope.searchText = '';
    var Url = 'BL/vehiculoController.php';

    getVehiculos();

    function getVehiculos() {
        
      var listado = {
        page: $scope.currentPage,
        size: $scope.pageSize,
        list: 'lista_vehiculo',
        baseUrl: Url,
        select: $scope.stipoServicio,
        search: $scope.searchText
      };

        MantService.getlistados(listado)
          .success(function (data) {
              console.log(data.Request);
              if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              $scope.totalItems = data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

              $scope.vehiculos = data.Request;

          })
          .error(function (error) {
              console.warn('Error en la BD :' + error);
          });
    }

    $scope.Editar = function(id){

        var editar = {
          id: id,
          list: 'editado_vehiculo',
          baseUrl: Url
        };
        MantService.geteditado(editar)
          .success(function (data) {
              
              $('#alias1').val(data.unidad_alias);
              $('#unidad_id').val(data.unidad_id);
              $('#tiposervicio1').val(data.tiposervicio_id);
              $('#placa1').val(data.unidad_placa);
              $('#marca1').val(data.unidad_marca);
              $('#modelo1').val(data.unidad_modelo);
              $('#color1').val(data.unidad_color);
              $('#fabricacion1').val(data.unidad_fabricacion);
              $('#fechasoat1').val(data.unidad_fechasoat);
              $('#fecharevtec1').val(data.unidad_fecharevtec);
              url = "<img src='BL/"+data.unidad_foto+"' class='img-responsive'>";
              $('#foto2').html(url);
              $('.chek').prop('checked', false);
              if(data.unidad_caracteristicas.length > 0){
                var n = '';
                n = data.unidad_caracteristicas.split(",");
                for (var i = 0; i < n.length; i++) {
                    var myNodeList = document.querySelectorAll("input[class=chek]");
                    for (j = 0; j < myNodeList.length; j++) {
                      if(n[i] == myNodeList[j].value){
                        myNodeList[j].checked = true;
                      }
                    }
                }
              }else{
                $('.chek').prop('checked', false);
              }

          }).
          error(function (error) {
              console.warn('Error en la BD :' + error);
          });
    }

    $scope.EliminarVehiculo = function(id){
      deleteGeneral(id,Url,getVehiculos);
    }

    $scope.tipovehiculo = function(){
      $scope.currentPage = 1;
      getVehiculos();
    }

    $scope.pageChanged = function() {
      getVehiculos();
    }

    $scope.pageSizeChanged = function() {
        $scope.currentPage = 1;
        getVehiculos();
    }

    $scope.searchTextChanged = function(){
        $scope.currentPage = 1;
        getVehiculos();
    }
});

app2.controller('configreservaController',function($scope,$http,DTOptionsBuilder, DTColumnBuilder){

  $scope.bloqueoMin = [
        { "key": "0", "value": "Seleccione una opcion"},
        { "key": "1", "value": "30 min"},
        { "key": "2", "value": "40 min"},
        { "key": "3", "value": "60 min"},
        { "key": "4", "value": "120 min"},
  ];

  $scope.horabloqueoservicio = "1";
  $scope.CloseConfig = function(){
      $("#msgErrorConfig").hide();
  }
  $scope.CloseConfigSucces = function(){
      $("#msgSuccessConfig").hide();
  }

    getConfigReserva();
    $scope.visitorConfigEdit = {'horareservaEdit': '', 'horalertmovilEdit': '', 'horalertapanelEdit' : ''};
    $scope.showModal = false;
    function getConfigReserva(){

      $scope.loading = true;
      $scope.configreservas = {}; 
      $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withDisplayLength(10)
      .withPaginationType('full_numbers')
      .withOption("order", [
                    [0, "desc"]
        ])
      .withOption('bLengthChange', false);

    var requestConfigreserva = $http({
                    method: "post",
                    url: "../../BL/reservaController.php",
                    data: {lista_config_reserva: 'lista_config_reserva'},
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });

      requestConfigreserva.success(function (data) {
        console.log(data);
          $.each( data, function( key, value ) {
            if(value.configreserva_status == 1){
              data[key].enabled = true;
            }else{
              data[key].detalle = 'Desabilitado';
              data[key].enabled = false;
            }
          });

         $scope.configreservas = data;
         $scope.loading = false;
         $scope.contador = data.length;

      });

      requestConfigreserva.error(function(data, status){
          console.log('error'+ status);
      });
  };

  $scope.editConfigReserva = function(index,id,hora,alert,panel,bloqueo){

    $("#hantiEdit").val(hora);
    $("#hmovilEdit").val(alert);
    $("#hpanelEdit").val(panel);
    
    $scope.idconfig = id;
    $scope.indexEdit = index;
    
    if(bloqueo != 0){
      $scope.horabloqueoservicioEdit = bloqueo;
    }else{
      $scope.horabloqueoservicioEdit = "0";
    }
    
    $scope.showModal = !$scope.showModal;
  }

  $scope.UpdateConfigreserva = function(){

    var index = $scope.indexEdit
    var valorseleccinado = $("#hbloedit").val();
    $scope.updConfigReserva = [{
        configreserva_id:$scope.idconfig,
        configreserva_horas:$scope.visitorConfigEdit.horareservaEdit,
        configreserva_alerta:$scope.visitorConfigEdit.horalertmovilEdit,
        configreserva_panel:$scope.visitorConfigEdit.horalertapanelEdit,
        configreserva_bloqueo:valorseleccinado
    }];

    var restConfigReservaUpd = $http({
                  method: "post",
                  url: "../../BL/reservaController.php",
                  data: {
                    hidden_updateconfig_reserva: 'hidden_updateconfig_reserva',
                    data: $scope.updConfigReserva
                  },
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                  });
    restConfigReservaUpd.success(function (data){
          if(data.status == true){
            getConfigReserva();
            $scope.showModal = false; 
          }
    });
  }

  $scope.addConfigreserva = function(){
    
    console.log($scope.visitorConfig);
    $scope.addConfigReserva = [{
        configreserva_horas:$scope.visitorConfig.horareserva,
        configreserva_alerta:$scope.visitorConfig.horalertmovil,
        configreserva_panel:$scope.visitorConfig.horalertapanel,
        configreserva_bloqueo:$scope.horabloqueoservicio
    }];
    
    var restConfigReserva = $http({
                  method: "post",
                  url: "../../BL/reservaController.php",
                  data: {
                    hidden_insertconfig_reserva: 'hidden_insertconfig_reserva',
                    data: $scope.addConfigReserva
                  },
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                  });
    restConfigReserva.success(function (data){
            getConfigReserva();
            $scope.visitorConfig = {};
    });
  }

  $scope.changeCallbackConfig = function(id,index){
      var i = index-1;
      var switch_flag = $scope.configreservas[i].enabled;
      var requestoConduc = $http({
                    method: "post",
                    url: "../../BL/reservaController.php",
                    data: {
                        switch:switch_flag,
                        config:id,
                        configuracion:'configuracion'
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
    requestoConduc.success(function (data) {
        console.log(data);
        if(data.status == true){

          $scope.messageErrorConfig ='';
          $("#msgErrorConfig").hide();
          $scope.messageSuccessConfig = data.msg;
          $scope.configreservas[i].enabled = true;
          $scope.configreservas[i].configreserva_status = 1;
          $("#msgSuccessConfig").show();

        }else{

            $("#msgSuccessConfig").hide();
            $scope.configreservas[i].enabled = true;
            $("#msgErrorConfig").show();
            $scope.messageErrorConfig = data.msg;
        }

        getConfigReserva();
    });
  }
});

app2.controller('clientesController', function($scope, MantService){

    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;
    $scope.reverse = false;
    $scope.searchText = '';
    var Url = 'BL/clientesController.php';

    getClientes();
    function getClientes(){

        var listado = {
          page: $scope.currentPage,
          size: $scope.pageSize,
          list: 'lista_clientes',
          baseUrl: Url,
          select: $scope.stipoCliente,
          search: $scope.searchText
        };

        MantService.getlistados(listado)
          .success(function (data) {

              if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              $scope.totalItems = data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

              $.each(data.Request, function( key, value ) {
                data.Request[key].cliente_calificacion = number_format(value.cliente_calificacion,1);
                if(value.cliente_estados == 1){
                  data.Request[key].enabled = true;
                }else{
                  data.Request[key].enabled = false;
                }
              });

              $scope.clientes = data.Request;

          })
          .error(function (error) {
            console.warn('Error en la BD :' + error);
          });
    }

    $scope.Editar = function(id){

      var editar = {
        id: id,
        list: 'editado_cliente',
        baseUrl: Url
      };
      MantService.geteditado(editar)
        .success(function (data) {

          console.info(data);
          $('#cliente_id').val(data.cliente_id);
          $('#tipoedit').val(data.tipocliente_id);
          if(data.tipocliente_id != 1){

            $("#groupempresaedit").show();
            $("#groupcostoedit").show();
            $("#groupsupervisoredit").show();
       
            listaempresas(2,2);

            $('#costoedit').val(data.cliente_costos);
            $('#supervisoredit').val(data.cliente_supervisor);
            if(data.cliente_estados.length > 0){
              if(data.cliente_estados != 1){
                document.getElementById("radio2edit").checked = true;
              }else{
                document.getElementById("radio1edit").checked = true;
              }
            }else{
                $('input[name="radioedit"]').attr('checked','checked');
            }
            
            setTimeout(function(){
                var x = document.getElementById("empresaedit").options;
                for (var i = 0; i < x.length; i++) {
                  if(x[i].value == data.empresa_id){
                    x[i].selected = true;
                  }
                }
            }, 500);
            
          }else{
            $("#groupempresaedit").hide();
            $("#groupcostoedit").hide();
            $("#groupsupervisoredit").hide();
            $('#costoedit').val('');
            $('#supervisoredit').val('');
            if(data.cliente_estados.length > 0){
              if(data.cliente_estados != 1){
                document.getElementById("radio2edit").checked = true;
              }else{
                document.getElementById("radio1edit").checked = true;
              }
            }else{
                $('input[name="radioedit"]').attr('checked','checked');
            }
            //$('#statusedit').hide();
          }

          $('#nombresedit').val(data.cliente_nombre);
          $('#apellidosedit').val(data.cliente_apellido);
          $('#correoedit').val(data.cliente_correo);
          $('#celularedit').val(data.cliente_celular);
          $('#passwordedit').val(data.cliente_password);

        }).
        error(function (error) {
            console.warn('Error en la BD :' + error);
        });
    }

    $scope.changeEstado = function(id,estado){

      var estado = (estado == false)?0:1;
      var update = {
        id: id,
        list: 'estado_cliente',
        estado: estado,
        baseUrl: Url
      };
      MantService.getestado(update)
      .success(function (data) {
        if(data == true)
          getClientes();
      }).
      error(function (error) {
        console.warn('Error en la BD :' + error);
      }); 
    }

    $scope.EliminarCliente = function(id){
      deleteGeneral(id,Url,getClientes);
    }

    $scope.tipoCliente = function(){
      $scope.currentPage = 1;
      getClientes();
    }

    $scope.pageChanged = function() {
      getClientes();
    }

    $scope.pageSizeChanged = function() {
        $scope.currentPage = 1;
        getClientes();
    }

    $scope.searchTextChanged = function(){
        $scope.currentPage = 1;
        getClientes();
    }
});

app2.controller('promocionesController', function($scope, MantService){

    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;
    $scope.reverse = false;
    $scope.searchText = '';
    var Url = 'BL/promoController.php';

    getPromociones();
    function getPromociones(){

        var listado = {
          page: $scope.currentPage,
          size: $scope.pageSize,
          list: 'lista_promociones',
          baseUrl: Url,
          select: $scope.stipoUsuario,
          search: $scope.searchText
        };

        MantService.getlistados(listado)
          .success(function (data) {
              console.log(data);
              if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              $scope.totalItems = data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

              $.each(data.Request, function( key, value ) {
                
                if(value.promo_tipo == 1){
                  data.Request[key].promo_texto = 'Cliente';
                }else{
                  data.Request[key].promo_texto = 'Conductor';
                }

                if(value.promo_flag == 1){
                  data.Request[key].enabled = true;
                }else{
                  data.Request[key].enabled = false;
                }


              });

              $scope.promociones = data.Request;

          })
          .error(function (error) {
            console.warn('Error en la BD :' + error);
          });
    }

    $scope.Editar = function(id){

      var editar = {
        id: id,
        list: 'editado_promo',
        baseUrl: Url
      };
      MantService.geteditado(editar)
        .success(function (data) {

            var inicio = data.promo_finicio.split(' ');
            console.log(inicio);
            //var fi = inicio[0].split('-');
            data.promo_finicio = inicio[0]; 

            var final = data.promo_fvigencia.split(' ');
            console.log(final);
            //var ff = final[0].split('-');
            data.promo_fvigencia = final[0]; 

            $('#idpromocion1').val(data.promo_id);
            $('#nombrepromo1').val(data.promo_nombre);
            $('#tipo1').val(data.promo_tipo);
            $('#finicio1').val(inicio[0]);
            $('#fvigencia1').val(final[0]);
            $('#cantidad1').val(data.promo_cantidad);
            $('#puntos1').val(data.promo_puntos);

            var control = $('#imagen1');
            control.replaceWith( control = control.val('').clone( true ) );

            url = "<img src='BL/"+data.promo_imagen+"' class='img-responsive'>";
            $('#foto2').html(url);

        }).
        error(function (error) {
            console.warn('Error en la BD :' + error);
        });
    }

    $scope.changeEstado = function(id,estado){

      var estado = (estado == false)?0:1;
      var update = {
        id: id,
        list: 'estado_promocion',
        estado: estado,
        baseUrl: Url
      };
      MantService.getestado(update)
      .success(function (data) {
        if(data == true)
          getPromociones();
      }).
      error(function (error) {
        console.warn('Error en la BD :' + error);
      }); 
    }

    $scope.EliminarPromo = function(id){
      deleteGeneral(id,Url,getPromociones);
    }

    $scope.tipoUsuario = function(){
      $scope.currentPage = 1;
      getPromociones();
    }

    $scope.pageChanged = function() {
      getPromociones();
    }

    $scope.pageSizeChanged = function() {
        $scope.currentPage = 1;
        getPromociones();
    }

    $scope.searchTextChanged = function(){
        $scope.currentPage = 1;
        getPromociones();
    }
});

app2.controller('canjesController', function($scope, MantService){

    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;
    $scope.reverse = false;
    $scope.searchText = '';
    var Url = 'BL/promoController.php';

    getCanjes();
    function getCanjes(){

        var listado = {
          page: $scope.currentPage,
          size: $scope.pageSize,
          list: 'lista_canjes',
          baseUrl: Url,
          select: $scope.stipoUsuario,
          search: $scope.searchText
        };

        MantService.getlistados(listado)
          .success(function (data) {
              console.log(data);
              if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              $scope.totalItems = data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

              $scope.canjes = data.Request;

          })
          .error(function (error) {
            console.warn('Error en la BD :' + error);
          });
    }

    $scope.tipoUsuario = function(){
      $scope.currentPage = 1;
      getCanjes();
    }

    $scope.pageChanged = function() {
      getCanjes();
    }

    $scope.pageSizeChanged = function() {
        $scope.currentPage = 1;
        getCanjes();
    }

    $scope.searchTextChanged = function(){
        $scope.currentPage = 1;
        getCanjes();
    }
});

app2.controller('empresasController', function($scope, MantService){

    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;
    $scope.reverse = false;
    $scope.searchText = '';
    var Url = 'BL/empresaController.php';

    getEmpresas();
    function getEmpresas(){

        var listado = {
          page: $scope.currentPage,
          size: $scope.pageSize,
          list: 'lista_empresas',
          baseUrl: Url,
          //select: $scope.stipoCliente,
          search: $scope.searchText
        };

        MantService.getlistados(listado)
          .success(function (data) {

              if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              $scope.totalItems = data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

              $scope.empresas = data.Request;

          })
          .error(function (error) {
            console.warn('Error en la BD :' + error);
          });
    };

    var self = this;
    self.empre={codigo:'',rzonsocial:'',ruc:'',direccion:'',telefono:'',email:'',contacto:''};
    self.submit = function() {
        
        $("#msgSuccess").hide();
        $("#msgError").hide();

        var agregar = {
          data:self,
          list: 'insert_empresas',
          baseUrl: Url
        };

        MantService.getinsertar(agregar)
        .success(function (data) {

          if(data.status == true){
            $("#msgError").hide();
            $("#msgSuccess").show();
            $scope.messageSuccess = data.msg;
            getEmpresas();
          }else{
            $("#msgSuccess").hide();
            $("#msgError").show();
            $scope.messageError = data.msg;
          }
          
        })
        .error(function (error) {
          console.warn('Error en la BD :' + error);
        });

        /*if(self.user.id === null){ 12345678765
            self.user.id = self.id++;
            console.log('Saving New User', self.user);    
            self.users.push(self.user);//Or send to server, we will do it in when handling services
        }else{
            for(var i = 0; i < self.users.length; i++){
                if(self.users[i].id === self.user.id) {
                  self.users[i] = self.user;
                  break;
                }
            }
           console.log('User updated with id ', self.user.id);
        }
        self.reset();*/
    };

    $scope.Editar = function(id){

      var editar = {
        id: id,
        list: 'editado_empresa',
        baseUrl: Url
      };
      MantService.geteditado(editar)
        .success(function (data) {

          $('#empid').val(data.empresa_id);
          $('#codigoedit').val(data.empresa_codigo);
          $('#razonsocialedit').val(data.empresa_razonsocial);
          $('#rucedit').val(data.empresa_ruc);
          $('#telefonoedit').val(data.empresa_telefono);
          $('#correoedit').val(data.empresa_correo);
          $('#contactoedit').val(data.empresa_contacto);
          $('#direccionedit').val(data.empresa_direccion);

        }).
        error(function (error) {
            console.warn('Error en la BD :' + error);
        });
    }

    $scope.EliminarEmpresa = function(id){
      deleteGeneral(id,Url,getEmpresas);
    }

    $scope.pageChanged = function() {
      getEmpresas();
    }

    $scope.pageSizeChanged = function() {
        $scope.currentPage = 1;
        getEmpresas();
    }

    $scope.searchTextChanged = function(){
        $scope.currentPage = 1;
        getEmpresas();
    }
});

app2.controller('conductorControllerEdit',function($scope,$http){

      $scope.bancosEdit = [
        { "key": "0", "value": "Seleccione una opcion"},
        { "key": "1", "value": "BCP"},
        { "key": "2", "value": "BBVA"},
        { "key": "3", "value": "Scotiabank"},
        { "key": "4", "value": "Interbank"},
        { "key": "5", "value": "BanBif" },
        { "key": "6", "value": "Banco Financiero"}
      ];

    $scope.select_defaultEdit = '0';

  $scope.CloseConductor = function(){
      $("#msgError").hide();
  }
  $scope.CloseConductorSucces = function(){
      $("#msgSuccess").hide();
  }

  $scope.updateSthEdit = function(){

    var flag = true;

    var updateConductor = [{
        conductor_id:$("#conductorid").val(),
        conductor_nombre:$('#nombre').val(),
        conductor_apellido:$('#apellido').val(),
        conductor_correo:$('#correo').val(),
        conductor_password:$('#password').val(),
        conductor_celular:$('#celular').val(),
        conductor_dni:$('#dni').val(),
        conductor_ciudad:$('#ciudad').val(),
        conductor_distrito:$('#distrito').val(),
        conductor_licencia:$('#licencia').val(),
        conductor_categoria:$('#catlicencia').val(),
        conductor_fechabrevete:$("#brevete").val(),
        conductor_fechasoat:$("#soat").val(),
        conductor_fechacarnetvial:$("#carnetvial").val(),
        conductor_polarizadas:$("#polarizadas").val(),
        conductor_cci:$("#cii_banco").val(),
        banco_id:$scope.select_defaultEdit
    }];

  
          var form = $("#formuploadajaxEdit"); 
          //Foto del Conductor
          var fileInput = document.getElementById('image-file');
          var file = fileInput.files[0];
          //Foto del Soat
          var fileInput1 = document.getElementById('foto1');
          var file1 = fileInput1.files[0];
          //Foto Carnet Vial
          var fileInput2 = document.getElementById('foto_2');
          var file2 = fileInput2.files[0];

          var formData = new FormData();

          formData.append('file', file);
          formData.append('file1', file1);
          formData.append('file2', file2);
          
          formData.append('hidden_update_conductor','hidden_update_conductor');
          formData.append('hidden_editiframe','hidden_editiframe');
          formData.append('data', JSON.stringify(updateConductor)); 

          if(file){
              console.log('file');
              var img = new Image();
              img.src = window.URL.createObjectURL( file );

              img.onload = function() {
                  var width = img.naturalWidth,
                      height = img.naturalHeight;

                  window.URL.revokeObjectURL( img.src );

                  if( width <= 180 && height <= 180 ) {
                     $("#msfile").css({"color":"#737373"});
                    flag = true;
                  }
                  else {
                    flag = false;
                    var control = $('#foto');
                    control.replaceWith( control = control.val('').clone( true ) );
                    $("#msfile").css({"color":"#dd4b39"});
                  }
              };
          }

          setTimeout(function(){ 
              console.log(flag);
              if(flag == true){
                
                if($('#nombre').val().length > 0 && $('#apellido').val().length > 0 && $('#correo').val().length > 0 && $('#password').val().length > 0 && $('#dni').val().length > 0 && $('#licencia').val().length > 0){

                  $.ajax({
                    type: "POST",
                    url: '../../BL/conductorController.php',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data) 
                    { 
                      if(data.status == true){
                        $scope.messageSuccess = 'Actualizaciòn Realizada del Conductor';
                        $('#msgSuccess').show();
                      }else{
                        console.log('error');
                      }  
                    } 
                  });

                }else{
                  console.log('validando');
                }

              }else{
                console.log('flag is false');
              }
           }, 500);  
  }

});

app2.controller('stactaController', function($scope,$http){

  $scope.searchText = '';
  $scope.pageT = 1;

   $scope.optionCallback = undefined;

  $scope.gridDataSource = {
      transport: {
          read: function (e) {

            $.ajax({url: "../../BL/stactaController.php",method: "POST",dataType: 'json', 
              data: {filter : {filters : [
              {field: "hidden_listar_stcta", value: "hidden_listar_stcta"},
              {field: "searchText", value : $scope.searchText}
              ]}},
              success: function(result){
                //console.log(result);
                $(".k-pager-numbers li a[data-page='1']").click();
                e.success(result);
                $scope.optionCallback = e;
                
            }});

        }
      },
      filter:{ field: "hidden_listar_stcta", value: "hidden_listar_stcta"},
      pageSize: 10,
      serverPaging: false,
      serverSorting: true,
      serverFiltering: true
  };

  $scope.mainGridOptions = {
      sortable: true,
      pageable: true,
      /*dataBound: function() {
          this.expandRow(this.tbody.find("tr.k-master-row").first());
      },*/
      columns: [
        {field: "conductor_nombre",title: "Conductor",width: "120px"},
        {field: "unidad",title: "Unidad",width: "120px"},
        {field: "conductor_dni",title: "Dni", width: "120px"},
        {field: "conductor_correo",title: "Correo" ,width: "120px"},
        {field: "conductor_celular",title:'Celular',width: "120px"},
        {field: "conductor_recarga",title:'S/. Saldo',width: "120px"}
      ]
  };

  $scope.onChangeFilter = function(){
    console.info($scope.conductorNombre);
    if($scope.conductorNombre.length == 0){
        $scope.doSearch();
    }
  }

  $scope.doSearch = function() {
    var conductorNombre = $scope.conductorNombre || '';

    var filterStact = $http({
        url : ' ../../BL/stactaController.php',
        method    : 'POST',
        data : {
            filterConductor : 'filter',
        },
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }
    );
    filterStact.success(function(data){
      console.log(data);
      var dataMut = data.filter(function(value,key){
        if(typeof value.conductor_nombre != "undefined"){
            if(value.conductor_nombre.toUpperCase().indexOf(conductorNombre.toUpperCase()) != -1 ){
              return true;
            }
        }
      });
      $scope.gridDataSource = dataMut;
    });

  }; 

  $scope.detailGridOptions = function(dataItem) {

      return {
          dataSource: {
              transport: {
                  read: {
                      url: "../../BL/stactaController.php",
                      dataType: 'json',
                      type:'POST'
                  },
              },
              serverPaging: false,
              serverSorting: true,
              serverFiltering: true,
              pageSize: 10,
              filter: { field: "conductor_id", operator: "eq", value: dataItem.conductor_id}
          },
          scrollable: false,
          sortable: true,
          pageable: true,
          columns: [
            { field: "id_new", title:"Codigo",width: "50px" },
            { field: "cta_tipo", title:"Tipo Movimiento", width: "110px" , attributes: {style: "color: blue"}},
            { field: "cta_fecha", title:"Fecha",width: "190px" },
            { field: "tipo_pago", title:"Tipo de Pago", width: "100px"},
            { field: "cta_monto", title: "S/. Monto", width: "190px" }
          ]
      };
  };

  $scope.SearchTextby = function(){

    if ($scope.optionCallback !== undefined) {

        $scope.gridDataSource.transport.read($scope.optionCallback);

    }
    
  }

});
//attributes: { style: "text-align: right"}

app2.controller('conductorController',function($scope, $http, $filter, filteredListService, DTOptionsBuilder, DTColumnBuilder){
    
  $scope.currentPage = 1;
  $scope.totalItems = 0;
  $scope.pageSize = 10;
  $scope.searchText = '';
  $scope.reverse = false;
  var Url = 'BL/conductorController.php';

  $scope.comisiones = [
      {"key": "0","value": "Seleccione una opcion"},
      {"key": "1","value": "% Porcentaje"},
      {"key": "2","value": "S/ Monto"}
  ];

  $scope.bancosEdit = [
      { "key": "0", "value": "Seleccione una opcion"},
      { "key": "1", "value": "Continental"},
      { "key": "2", "value": "Credito"},
      { "key": "3", "value": "Interbank"}
  ];
    
   $scope.select_defaultEdit = '0';
   $scope.emailConductor = {text: ''};
   $scope.passConductor = {text: ''};

  getConductor();
  getUnidad();

  $scope.showModal = false;

  $("#msgError").hide();
  $('#msgSuccess').hide();

  $scope.CloseConductor = function(){
      $("#msgError").hide();
  }
  $scope.CloseConductorSucces = function(){
      $("#msgSuccess").hide();
  }

  $scope.updateSth = function(){
    

    var c = $("#conductorsDis").val();
    var i = $scope.indexEdit -1;
    var flag = true;
    var Comisionselected = ($("#comisionidedit").val() != 0)?$("#comisionidedit").val():0;
    var ComisionTotal = ($("#montoconductoredit").val() != 0)?$("#montoconductoredit").val():0;

    var updateConductor = [{
        conductor_id:$scope.idconductor,
        conductor_nombre:$('#nombre1').val(),
        conductor_apellido:$('#apellido1').val(),
        unidad_id:c,
        conductor_correo:$('#correo').val(),
        conductor_password:$('#pass1').val(),
        conductor_celular:$('#celular1').val(),
        conductor_dni:$('#dni1').val(),
        conductor_ciudad:$('#ciudad1').val(),
        conductor_distrito:$('#distrito1').val(),
        conductor_licencia:$('#licencia1').val(),
        conductor_categoria:$('#catlicencia1').val(),
        conductor_fechabrevete:$('#fechabrevete1').val(),
        conductor_comision:Comisionselected,
        conductor_comision_total:ComisionTotal,
        conductor_fechasoat:$("#soat").val(),
        conductor_fechacarnetvial:$("#carnetvial").val(),
        conductor_polarizadas:$("#polarizadas").val(),
        conductor_cci:$("#cii_banco").val(),
        banco_id:$('#select_defaultEdit').val()
    }];

  
          var form = $("#formuploadajax"); 
          var fileInput = document.getElementById('foto1');
          var file = fileInput.files[0];

          //Foto del Soat
          var fileInput1 = document.getElementById('foto3');
          var file1 = fileInput1.files[0];
          //Foto Carnet Vial
          var fileInput2 = document.getElementById('foto_4');
          var file2 = fileInput2.files[0];

          var formData = new FormData();

          formData.append('file', file);
          formData.append('file1', file1);
          formData.append('file2', file2);

          formData.append('hidden_update_conductor','hidden_update_conductor');
          formData.append('data', JSON.stringify(updateConductor)); 

          if(file){
              console.log('file');
              var img = new Image();
              img.src = window.URL.createObjectURL( file );

              img.onload = function() {
                  var width = img.naturalWidth,
                      height = img.naturalHeight;

                  window.URL.revokeObjectURL( img.src );

                  if( width <= 180 && height <= 180 ) {
                     $("#smgpopup").css({"color":"#737373"});
                    flag = true;
                  }
                  else {
                    flag = false;
                    var control = $('#foto1');
                    control.replaceWith( control = control.val('').clone( true ) );
                    $("#smgpopup").css({"color":"#dd4b39"});
                  }
              };
          }

          setTimeout(function(){ 
              console.log(flag);
              if(flag == true){
                
                if($('#nombre1').val().length > 0 && $('#apellido1').val().length > 0 && $('#correo').val().length > 0 && $('#pass1').val().length > 0 && $('#dni1').val().length > 0 && $('#licencia1').val().length > 0 && $("#soat").val().length > 0 /*&& $("#carnetvial").val().length > 0*/ && $('#fechabrevete1').val().length > 0 ){

                  $.ajax({
                    type: "POST",
                    url: '../../BL/conductorController.php',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data) 
                    { 
                      console.log(data);
                      if(data.status == true){
                          $scope.conductors[i].conductor_celular = data.telefono;
                          if(data.alias == null){
                            $scope.conductors[i].unidad_alias = 'Asig. Unidad';
                          }else{
                            $scope.conductors[i].unidad_alias = data.alias;
                          }

                          if(data.estado == '1'){
                            $scope.conductors[i].enabled = true;
                          }else{
                            $scope.conductors[i].enabled = false;
                          }

                          if(data.foto == null){
                             $scope.conductors[i].conductor_foto = IMG_DEFAULT;
                          }else{
                            var view = UrlExists('BL/'+data.foto);
                            if(view != true){
                              $scope.conductors[i].conductor_foto = IMG_DEFAULT;
                            }else{
                              $scope.conductors[i].conductor_foto = data.foto;
                            }
                          }
                          
                          $scope.conductors[i].conductor_nombre = data.nombre;
                          $scope.conductors[i].conductor_apellido = data.apellido;
                          $scope.conductors[i].conductor_correo = data.correo;
                          $scope.conductors[i].tiposervicio_nombre = data.servicio;
                          $scope.conductors[i].conductor_calificacion = data.calificacion;
                          $scope.conductors[i].conductor_status = data.estado;
                      }
                      
                      $('#modal').modal('hide');
                      $scope.messageSuccess = 'Actualizaciòn Realizada del Conductor :'+ data.nombre+' '+data.apellido;
                      $('#msgSuccess').show();
                      $scope.showModal = false;
                    } 
                  });

                }else{
                  console.log('validando');
                }

              }else{
                console.log('nada');
              }
           }, 500);  
  }

  /*$scope.llenar= function(id,nombre,apellido,unidad,correo,password,dni,celular,ciudad,distrito,licencia,catlicencia,fechabrevete,foto,unidadnom,indice,comision,comisiontotal,fechasoat,fotosoat,fotocarnet,fechacarnet,polarizadas,cci,banco){*/
  $scope.llenar= function(id,indice){

    var requestdtaModal = $http({
      method: "post",
      url: "../../BL/conductorController.php",
      data: {
          id: id,
          listar_modal: 'listar_modal'
            },
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });

    requestdtaModal.success(function (data) {
          console.log(data[0]);

        if(data[0].banco_id != 0 ){
         $scope.select_defaultEdit = data[0].banco_id;
        }else if(data[0].banco_id != null){
          $scope.select_defaultEdit = '0';
        }

        if(data[0].conductor_comision != 0){
            $scope.Comisionselected = data[0].conductor_comision;
            $("#mtncdedit").show();
            $scope.comisionTotal = parseFloat(data[0].conductor_comision_total);
        }else{
            $("#mtncdedit").hide();
            $scope.comisionTotal = '';
            $scope.Comisionselected = '0';
        }

        limpiarScopes();
        $scope.indexEdit = indice;
        $scope.nameConductor = data[0].conductor_nombre;
        $scope.apellidoConductor = data[0].conductor_apellido;
        $scope.idconductor = id;
        $("#celular1").val(data[0].conductor_celular);
        $scope.celConductor = data[0].conductor_celular;
        $("#dni1").val(data[0].conductor_dni);
        $scope.dniConductor = data[0].conductor_dni;
        $scope.cityConductor = data[0].conductor_ciudad;
        $scope.distConductor = data[0].conductor_distrito;
        $scope.liceConductor = data[0].conductor_licencia;
        $scope.catConductor = data[0].conductor_catlicencia;
        $scope.polarizadas = data[0].conductor_polarizadas;
        $("#cii_banco").val(data[0].conductor_cci);

        if(data[0].unidad_id != null){

          var index = 0;
          var index_last = 0;
          var entroCarga = false;
          if($scope.vehiculos.length != 0){
              for(var i=0; i< $scope.vehiculos.length; i++){
                if($scope.vehiculos[i].unidad_id == data[0].unidad_id){
                    entroCarga=true; 
                    index_last = $scope.vehiculos[i].index;
                }else{
                    index = i;
                }
              }

              if(entroCarga != true){
                newVendor = {unidad_alias:data[0].unidad_alias, unidad_id:data[0].unidad_id, index:index+1};
                $scope.vehiculos.push(newVendor);
                index = eval(index+1);
                $scope.selected = $scope.vehiculos[index];
              }else{
                $scope.selected = $scope.vehiculos[index_last];
              }
          }else{
     
              newVendor = {unidad_alias:data[0].unidad_alias, unidad_id:data[0].unidad_id, index:index+1};
              $scope.vehiculos.push(newVendor);
              index = eval(index);
              $scope.selected = $scope.vehiculos[index];
          }

        }else{
           getUnidad();
           $scope.selected = '';
        }

        if (data[0].conductor_fechabrevete != null ){
          var f = data[0].conductor_fechabrevete.split('-');
          $('#fechabrevete1').val(data[0].conductor_fechabrevete);
        }

        if(data[0].conductor_fechasoat != null){
          $('#soat').val(data[0].conductor_fechasoat);
        }
        
        if(data[0].conductor_fechacarnetvial != null){
          $('#carnetvial').val(data[0].conductor_fechacarnetvial);
        }
        
        $scope.emailConductor = {text: data[0].conductor_correo};
        $('#pass1').val(data[0].conductor_password);

          if(data[0].conductor_foto != null){
            $scope.fotoedit = data[0].conductor_foto;
          }else{
            $scope.fotoedit = IMG_DEFAULT;
          }

          if(data[0].conductor_fotosoat != null){
            $scope.fotoeditsoat = data[0].conductor_fotosoat ;
          }else{
            $scope.fotoeditsoat = IMG_DEFAULT;
          }

          if(data[0].conductor_fotocarnetvial  != null){
            $scope.fotoeditcarnet = data[0].conductor_fotocarnetvial ;
          }else{
            $scope.fotoeditcarnet = IMG_DEFAULT;
          }

	if(data[0].conductor_foto_licencia  != null){
          $scope.fotoeditlicencia = data[0].conductor_foto_licencia ;
        }else{
          $scope.fotoeditlicencia = IMG_DEFAULT;
        }
        $scope.showModal = !$scope.showModal;

    });
  }

  function limpiarScopes(){
    $scope.nameConductor = '';
    $scope.apellidoConductor = '';
    $scope.idconductor = '';
    $scope.dniConductor = '';
    $scope.celConductor = '';
    $scope.cityConductor = '';
    $scope.distConductor = '';
    $scope.liceConductor = '';
    $scope.catConductor = '';
  }

  $scope.EliminarConductor = function(conductor_id){
    deleteGeneral(conductor_id,Url,getConductor);
  }

  function getUnidad(){

     var requestUnidad = $http({
                    method: "post",
                    url: "../../BL/vehiculoController.php",
                    data: {
                        listar_vehiculo: 'listar_vehiculo'
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
    requestUnidad.success(function (data) {
      $scope.vehiculos = data;
    });
  }
  /********* Nuevo listado de Conductor ***********/
  function getConductor() {

      var requestoTarjeta = $http({
          method: "post",
          url: "../../BL/conductorController.php",
          data: {
              page: $scope.currentPage,
              size: $scope.pageSize,
              search: $scope.searchText,
              lista_conductor: 'lista_conductor'
                },
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          });

      requestoTarjeta.success(function (data) {
          console.log(data.Request.length);
          if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
          //$scope.conductors = [];
          $scope.totalItems = data.totalCount;
          $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
          $scope.endItem = $scope.currentPage * $scope.pageSize;
          if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}
          /*angular.forEach(data.Request, function(temp){

            temp.unidad_id = (temp.unidad_id == null)?1:2;

            if(temp.unidad_alias == null){
                temp.unidad_alias = 'Asig. Unidad'
            }

            if(temp.conductor_status == 1){
              temp.enabled = true;
            }else{
              temp.enabled = false;
            }
              console.log(temp);
              $scope.conductors.push(temp);
          });*/
          $.each(data.Request, function( key, value ) {

             data.Request[key].unidad_id = (value.unidad_id == null)?1:2;

            if(value.unidad_alias == null){
                data.Request[key].unidad_alias = 'Asig. Unidad'
            }
            if(value.conductor_status == 1){
              data.Request[key].enabled = true;
            }else{
              data.Request[key].enabled = false;
            }

          });
          $scope.conductors = data.Request;
      });  
  }

  $scope.pageChanged = function() {
      getConductor();
  }
  $scope.pageSizeChanged = function() {
      $scope.currentPage = 1;
      getConductor();
  }
  $scope.searchTextChanged = function() {
      $scope.currentPage = 1;
      getConductor();
  }

  $scope.resetAll = function () {
    $scope.Header = ['','',''];
  }

  $scope.sort = function (sortBy) {
      $scope.resetAll();

      $scope.columnToOrder = sortBy;
      //$Filter - Standard Service
      $scope.filteredList = $filter('orderBy')($scope.filteredList, $scope.columnToOrder, $scope.reverse);

      if ($scope.reverse) iconName = 'glyphicon glyphicon-chevron-up';
      else iconName = 'glyphicon glyphicon-chevron-down';

      console.log(sortBy);
      if (sortBy === 'tiposervicio_nombre') {
        $scope.Header[0] = iconName;
      }else if(sortBy === 'conductor_calificacion'){
        $scope.Header[1] = iconName;
      }else{
        $scope.Header[2] = iconName;
      }

     $scope.reverse = !$scope.reverse;
     //getData();
  };
    //$scope.sort('conductor_calificacion'); 
  /******* Final de lisatdo de Conductor **********/

  /*function getConductor(){
      $scope.loading = true;
      $scope.conductors = {}; 
      $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withDisplayLength(10)
      .withPaginationType('full_numbers')
      .withOption("order", [
                    [0, "desc"]
        ])
      .withOption('bLengthChange', false)
      .withColumnFilter({
            aoColumns: [
            {
                type: 'number'
            }, 
            {
                type: 'text',
                bRegex: true,
                bSmart: true
            }, 
            {
                type: 'select',
                bRegex: false,
                values: ['Yoda', 'Titi', 'Kyle', 'Bar', 'Whateveryournameis']
            }]
        });

    var requestConductor = $http({
                    method: "post",
                    url: "../../BL/conductorController.php",
                    data: {lista_conductor: 'lista_conductor'},
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });

      requestConductor.success(function (data) {
        console.log(data)
          $.each( data, function( key, value ) {
            if(value.unidad_alias == null){
                data[key].unidad_alias = 'Asig. Unidad'
            }
            if(value.conductor_status == 1){
              data[key].enabled = true;
            }else{
              data[key].enabled = false;
            }
          });
         $scope.conductors = data;
         $scope.loading = false;
      });
      requestConductor.error(function(data, status){
          console.log('error'+ status);
      });
  };*/

  function UrlExists(url)
  {
      var http = new XMLHttpRequest();
      http.open('HEAD', url, false);
      http.send();
      return http.status!=404;
  }

  $scope.changeCallback = function(id,index,enabled) {
        console.log(id);
        console.log(index);
        console.log(enabled);

      if($("#successInicio").is(':visible')){
        $("#successInicio").hide();
      }

      if($("#errorInicio").is(':visible')){
        $("#errorInicio").hide();
      }

      var i = index-1;
      //var switch_flag = $scope.conductors[i].enabled;
      var switch_flag = enabled;
      var requestoConduc = $http({
                    method: "post",
                    url: "../../BL/conductorController.php",
                    data: {
                        switch:switch_flag,
                        conductor:id,
                        estado:'estado'
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
    requestoConduc.success(function (data) {
        console.log(data);
        if(data.status == true){

          $scope.messageError ='';
          $("#msgError").hide();

          if(data.mensaje == true){
            $scope.conductors[i].enabled = true;
            $scope.conductors[i].conductor_status = 1;
          }else{
            $scope.conductors[i].enabled = false;
            $scope.conductors[i].conductor_status = 0;
          }
        }else{
            $scope.conductors[i].enabled = false;
            $("#msgError").show();
            $scope.messageError = 'No puede activar al Conductor sin unidad.';
        }
    });

  };
});

app2.controller('ptosController', function($scope, $http,DTOptionsBuilder, DTColumnBuilder) {

  getPtos();
  // Load all available tasks 
  function getPtos(){

      $scope.tasks = {}; 
      $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withDisplayLength(10)
      .withPaginationType('full_numbers')
      .withOption("order", [
                    [0, "desc"]
        ])
      .withOption('bLengthChange', false);

    var request = $http({
                    method: "post",
                    url: "../../BL/puntosController.php",
                    data: {lista_ptos: 'lista_ptos'},
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });

      request.success(function (data) {
          $.each( data, function( key, value ) {
            data[key].cliente = 'Cliente';
            data[key].conductor = 'Conductor';
          });
         
         $scope.tasks = data;
      });
      request.error(function(data, status){
          console.log('error'+ status);
      });
  };

  $scope.addPuntos = function (ptocl,valorcl,selecl,ptoco,valorco,seleco) {
    var requestptos = $http({
                    method: "post",
                    url: "../../BL/puntosController.php",
                    data: {
                        lista_add_ptos: 'lista_add_ptos',
                        cliente: ptocl,
                        puntos1:valorcl,
                        conductor: ptoco,
                        puntos2:valorco,
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
    requestptos.success(function (data) {
      getPtos();
      $scope.ptos = "";
      $scope.valor = "";
    });
  };

  $scope.deletePtos = function (id) {
     var Url = 'BL/puntosController.php';
    deleteGeneral(id,Url,getPtos);
    /*if(confirm("Esta seguro de eliminar ?")){
    var requeststatus = $http({
                    method: "post",
                    url: "../../BL/puntosController.php",
                    data: {
                        hidden_delete: 'hidden_delete',
                        taskID:task
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
    requeststatus.success(function (data) {
      getPtos();
    });
    }*/
  };

  $scope.toggleStatus = function(codigo, status) {
    if(status=='1'){status='0';}else{status='1';}
    var requestoggle = $http({
                    method: "post",
                    url: "../../BL/puntosController.php",
                    data: {
                        hidden_toggle: 'hidden_toggle',
                        taskID:codigo,
                        status:status
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
    requestoggle.success(function (data) {
      getPtos();
    });
  };

});

app2.controller('alerta',function($scope,$http, DTOptionsBuilder, DTColumnBuilder){

    getAlertas();
    $scope.showModal = false;
    $scope.searchFish   = ''; 

    $scope.toggleModal = function(id,tipo){

        var requesModal = $http({
          method: "post",
          url: "../../BL/alertaController.php",
          data: {
              hidden_toggle_modal: 'hidden_toggle_modal',
              AlertID:id,
              AlertTy:tipo
                },
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          });
      requesModal.success(function (data) {
          $scope.indetails = data;
          $scope.showModal = !$scope.showModal;
      });
    };

    $scope.llenarMap = function(id,lat,lng){

      clearMarkers();
      markers = [];
      
      var latitud = parseFloat(lat);
      var longitud = parseFloat(lng);
      var logoalerta = 'includes/img/alerta.png';
      var image = new google.maps.MarkerImage(logoalerta,
          new google.maps.Size(50, 65),
          new google.maps.Point(1,1),
          new google.maps.Point(1, 1)
      );

      marker = new google.maps.Marker({
          icon : image,
          position: {lat: latitud, lng: longitud},
          animation: google.maps.Animation.DROP,
          map: map
      });

      markers.push(marker);
      marker.setMap(map);
      moveToLocation(lat,lng);

      marker.addListener('click', toggleBounce);
    }

    function getAlertas(){

        $scope.alerts = [];
        //$scope.alerts = {};
        $scope.dtOptionsd = DTOptionsBuilder.newOptions()
          .withDisplayLength(10)
          .withPaginationType('full_numbers')
          .withOption("order", [
                      [6, "desc"]
          ])
          .withOption('bFilter', false)
          .withOption('bLengthChange', false);


        var requestalerta = $http({
                      method: "post",
                      url: "../../BL/alertaController.php",
                      data: {
                        listar_alerta: 'listar_alerta'
                      },
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                      });

        requestalerta.success(function (data) {

            $.each( data, function( key, value ) {
            
              if(value.alerta_ckeck != 1){
                data[key].enabled = false;
              }

          });
            $scope.alerts = data;
        });
        requestalerta.error(function(data, status){
            console.log('error'+ status);
        });
    };

    $scope.toggleAlert = function(id,index) {
      
      var i = index-1;
      var switch_flag = $scope.alerts[i].enabled;
      var requestoggle = $http({
                    method: "post",
                    url: "../../BL/alertaController.php",
                    data: {
                        status:switch_flag,
                        AlertID:id,
                        hidden_toggle_alert: 'hidden_toggle_alert'
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
          requestoggle.success(function (data) {
                $scope.alerts[i].detalle = 'Revisado';
                $scope.alerts[i].alerta_ckeck = 1;
          });

    };
});
//En la sección Reservas deben haber los siguientes filtros: Estado(Pendiente, Asignado, Realizado); 
//Fecha (Rango de Fecha);
//Hora(Rango por horas segun tabla, 1 hora, 2 horas, 3 horas, 4 horas y 5 horas).

app2.controller('tasksController', function($scope, $http, DTOptionsBuilder, DTColumnBuilder) {

  var Users ;

  $scope.contador = 20;
  $scope.titleform = "Guardar";
  $scope.message2 = "¿Está seguro de Guardar la reserva sin Asignar un conductor ?";
  $scope.dialogOpenform = false;

  var lugares;
  var poligono;

  LlenarZonas();

  $scope.org = {'fromorigen': '', 'latorg': '', 'lngorg' : ''};
  $scope.des = {'fromdestino': '', 'latdes': '', 'lngdes' : ''};

  //var options = {componentRestrictions: {country: "in"}};
  var inputFrom = document.getElementById('fromorigen');
  var autocompleteFrom = new google.maps.places.Autocomplete(inputFrom);
  google.maps.event.addListener(autocompleteFrom, 'place_changed', function() {
    var place = autocompleteFrom.getPlace();
    $scope.org.latorg = place.geometry.location.lat();
    $scope.org.lngorg = place.geometry.location.lng();
    $scope.org.fromorigen = place.formatted_address;
    $("#reforilatresr").val(place.geometry.location.lat());
    $("#reforilngresr").val(place.geometry.location.lng());
    ObtenerIdZonas(place.geometry.location.lat(),place.geometry.location.lng(),'origen');
    $scope.$apply();
    ConsultarTarifaServicio();

  });

  var inputFromdestino = document.getElementById('fromdestino');
  var autocompleteFromdestino = new google.maps.places.Autocomplete(inputFromdestino);
  google.maps.event.addListener(autocompleteFromdestino, 'place_changed', function() {
    var place = autocompleteFromdestino.getPlace();
    $scope.des.latdes = place.geometry.location.lat();
    $scope.des.lngdes = place.geometry.location.lng();
    $scope.des.fromdestino = place.formatted_address;
    $("#refdeslatresr").val(place.geometry.location.lat());
    $("#refdeslngresr").val(place.geometry.location.lng());
    ObtenerIdZonas(place.geometry.location.lat(),place.geometry.location.lng(),'destino');
    $scope.$apply();
    ConsultarTarifaServicio();

  });

  var requestdrop = $http({
                    method: "post",
                    url: "../../BL/reservaController.php",
                    data: {droplistipo: 'droplistipo'},
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
      requestdrop.success(function (data) {$scope.myOptions = data;});



  $scope.selectAction = function() {
    var selecteid = $scope.item;
    var requestdrop = $http({
                    method: "post",
                    url: "../../BL/reservaController.php",
                    data: {droplisConductor: 'droplisConductor',selecteid : selecteid},
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
      requestdrop.success(function (data) {
        console.log(data);
        $scope.myConductor = data;
      });

    ConsultarTarifaServicio();
  };

//AUTOCOMPLETE IN AJAX SEARCH CELULAR AND NAME
    $( "#celular" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "../../BL/clientesController.php",
          method: 'post',
          data: {q: request.term, autocom:'autocom'},
          success: function( data ) {
            response(data);
          }
        });
      },
      minLength: 2,
      select: function( event, ui ) {
         if (ui.item) {
                console.log(ui.item);
                $scope.cliente_nombre = ui.item.cliente;
                $scope.cliente_id = ui.item.idCliente;
                $scope.tipocliente = ui.item.emp;
                $scope.cliente_correo = ui.item.correo;
                $scope.cliente_cel = ui.item.value;
                $scope.$apply();
                document.getElementById("correo").disabled = true;
            }
      }
    });
//END AUTOCOMPLETE

  function LlenarZonas() {

    var lugardata = $http({
                    method: "post",
                    url: "../../BL/lugaresController.php",
                    data: {hidden_lugar_map: 'hidden_lugar_map'},
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
      lugardata.success(function (data) {
        lugares = data;
      });
  }

  function ObtenerIdZonas(lat,lng,tipo) {

    var LatLonMOV = new google.maps.LatLng(lat, lng);
    for (var i = 0; i < lugares.length; i++) {
        var path = [];
        var idlugar = lugares[i].lugares_id;
        var coordlugar = lugares[i].new;
        var lugar = lugares[i].lugar_descripcion;

        if (coordlugar.length > 1) {
            for (var x = 0; x < coordlugar.length; x++) {
                coordlugar[x].lat= parseFloat(coordlugar[x].lat);
                coordlugar[x].lng = parseFloat(coordlugar[x].lng);
            }

            poligono = new google.maps.Polygon({
                paths: coordlugar
            });
      
          if(google.maps.geometry.poly.containsLocation(LatLonMOV, poligono) == true) {
            if(tipo == 'origen'){
               $scope.idorg = idlugar;
               $scope.$apply();
            }else{
               $scope.iddes = idlugar;
               $scope.$apply();
            }
          }
        }
    }
  };

  function ConsultarTarifaServicio(){

    var tipo_config = $('#configId').val();
    if(tipo_config != 2){

        var origen = $scope.idorg;
        var destino = $scope.iddes;
        var tipo = $scope.item;
        var empresa = $scope.tipocliente;

        if(typeof empresa == 'undefined'){
          empresa = 0;
        }

        if( typeof origen != 'undefined' && typeof destino != 'undefined' && typeof tipo != 'undefined'){

          var costo = $http({
                        method: "post",
                        url: "../../BL/reservaController.php",
                        data: {
                          hidden_lugar_tarifa: 'hidden_lugar_tarifa',
                          origen_id:origen,
                          destino_id:destino,
                          tiposervicio:tipo,
                          empresa_id:empresa
                        },
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        });
          costo.success(function (data){
              console.log(data);
              $("#costo").val('');
              if(data.length > 0){
                $("#costo").val(data[0].tarifario_monto);
              }
              
          });

        }else{
          console.log('Falta Ingresar un dato....');
        }
    }else{

          var latOri = $("#reforilatresr").val();
          var lngOri = $("#reforilngresr").val();
          var latDes = $("#refdeslatresr").val();
          var lngDes = $("#refdeslngresr").val();
          var tipo = $scope.item;

          var nombreOrigen = $("#fromorigen").val();
          var nombreDestin = $("#fromdestino").val();

          if(latOri.length > 0  && lngOri.length > 0 && latDes.length > 0 && lngDes.length > 0 && typeof tipo != 'undefined'){

          var siorigen = nombreOrigen.indexOf("Aeropuerto");
          var sidestin = nombreDestin.indexOf("Aeropuerto");
          var aeropuerto = '';
          if(sidestin == 0 && siorigen != 0 || sidestin != 0 && siorigen == 0){
            aeropuerto = 1;
          }else{
            aeropuerto = 2;
          }

            $.ajax({
              url : "../ws/cliente/ws_consultar_tarifa_panel_kilometraje.php",
              type: "POST",
              async: true,
                cache: false,
              data : {latOri:latOri,lngOri:lngOri,latDes:latDes,lngDes:lngDes,tiposervicio_id:tipo,reserva:1,activo:aeropuerto},
            success: function(data)
              { 
                var obj = JSON.parse(data);
                console.log(obj.status);
                if(obj.status == true){
                  $("#costo").val(parseFloat(obj.tarifa));
                }
                }
            });
            /*var origin1 = new google.maps.LatLng(latOri, lngOri);
            var destinationB = new google.maps.LatLng(latDes,lngDes);
            var service = new google.maps.DistanceMatrixService();

            service.getDistanceMatrix(
              {
                origins: [origin1],
                destinations:  [destinationB],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false,
              }, callback);
        
            function callback(response, status) {
              console.log('call ',response,status);
              var originList = response.originAddresses;
              var destinationList = response.destinationAddresses;

                for (var i = 0; i < originList.length; i++) {
                    var results = response.rows[i].elements;
                    for (var j = 0; j < results.length; j++) {
                      var tiempos = results[j].duration.text.split('min');
                      var distancia = results[j].distance.text.split('km');
                      console.log(tiempos[0]);
                      console.log(distancia[0]);
                          SearchCostoKilometraje(distancia[0],tiempos[0],tipo);
                    }
                }

            }*/

        }else{
          console.log('Falta Ingresar un dato Kilometraje....');
        }
    }

  };

  function SearchCostoKilometraje(distancia,tiempo,tiposervicio){

      $.ajax({
        url : "../ws/cliente/ws_consultar_tarifa_panel_kilometraje.php",
        type: "POST",
        async: true,
          cache: false,
        data : { distancia:distancia,tiempo:tiempo,tiposervicio_id:tiposervicio,reserva:1},
      success: function(data)
        { 
          var obj = JSON.parse(data);
          console.log(obj);
          if(obj.status == true){
            $("#costo").val(parseInt(obj.tarifa));
          }
          }
      });
  }

  function fechaActual(){

    var f = new Date();
    var fechactual = f.getFullYear()+'-'+(f.getMonth()+1)+"-"+f.getDate();
    var horaactual = (f.getHours())+":"+f.getMinutes()+":"+f.getSeconds();
    var fecha = [];

        fecha['actual'] = fechactual+' '+horaactual;

    return fecha;
  };

  function clearForm(){
    document.getElementById("reservaForm").reset();
  }

  $scope.addTask = function () {

    var emp = (typeof $scope.cliente_id == 'undefined')?0:$scope.cliente_id;

      $scope.screensTask = [{
        cliente_id:emp,
        cliente_tipo:$scope.tipocliente,
        cliente_fecha:$("#datetimepicker2").find("input").val(),
        cliente_celular:$scope.cliente_cel,
        cliente_nombre:$scope.cliente_nombre,
        cliente_correo:$scope.cliente_correo,
        cliente_costo:$("#costo").val(),
        tiposervicio:$scope.item,
        tipopago:$scope.tipopago,
        origen_direccion:$scope.org.fromorigen,
        origen_lat:$scope.org.latorg,
        origen_lng:$scope.org.lngorg,
        origen_id:0,
        destino_direccion:$scope.des.fromdestino,
        destino_lat:$scope.des.latdes,
        destino_lng:$scope.des.lngdes,
        destino_id:0,
        conductor_id:$scope.selectedConductor,
        flag:1
      }];

    if(typeof $scope.selectedConductor == "undefined"){
      $scope.dialogOpenform = true;
    }else{
        var grabar = false;
        var f = new Date();
        var mes = ((f.getMonth()+1) < 10)?'0'+(f.getMonth()+1):(f.getMonth()+1);
        var dias = (f.getDate() < 10)?'0'+f.getDate():f.getDate();

        var fechactual = mes+'/'+dias+"/"+f.getFullYear();
        var horaactual = (f.getHours())+":"+f.getMinutes()+":"+f.getSeconds();
        var cliente_fecha = $("#datetimepicker2").find("input").val();

        /*var hours = f.getHours();
        var minutes = f.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;*/
        
        var horareserv = $("#confighoras").val();
        var fechaslect = new Date(cliente_fecha);
        var horaselect = (fechaslect.getHours())+":"+fechaslect.getMinutes()+":"+fechaslect.getSeconds();
        var diaselect = (fechaslect.getDate() < 10)?'0'+fechaslect.getDate():fechaslect.getDate();
        var messelect = ((fechaslect.getMonth()+1) < 10)?'0'+(fechaslect.getMonth()+1):(fechaslect.getMonth()+1);
        var fechaselectNew = messelect+'/'+diaselect+"/"+fechaslect.getFullYear();

        var hora1 = (horaselect).split(":"),
            hora2 = (horaactual).split(":"),
        t1 = new Date(),
        t2 = new Date();
      
        t1.setHours(hora1[0], hora1[1], hora1[2]);
        t2.setHours(hora2[0], hora2[1], hora2[2]);
        //Aquí hago la resta
        t1.setHours(t1.getHours() - t2.getHours());
        console.log("La diferencia es de: " + (t1.getHours()));

        var f1 = Date(fechaslect);
        var f2 = Date();

        if((Date.parse(f2)) < (Date.parse(f1))){
          alert('La fecha seleccionada debe ser mayor a la fecha Actual'+ fechactual);
          grabar = false;
        }else{
          if((Date.parse(f1)) >= (Date.parse(f2))){
              if((t1.getHours()) >= parseInt(horareserv)){
                  grabar = true;
              }else{
                  grabar = false;
              }
          }else{
            grabar = false;
          }
        }

        if(grabar == true){
          console.log('aca grabamos');
          console.log($scope.screensTask[0]);
          /*$.ajax({
            url: URL_CONTROLLER_SOCKET+'/w_reserva_asignado',
            data: $scope.screensTask[0],
            cache: false,
            method: 'post',
            success: function(data){
              console.log(data);
              if(data == true){
                DialogoTimerAngular(1);
              }
            }
          });*/
          /* Nueva implementacion del panel reserva */
          var requestInsert = $http({
          method: "post",
          url: "../../BL/reservaController.php",
          data: {
              hidden_insert_reserva: 'hidden_insert_reserva',
              data: $scope.screensTask
          },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          });

          requestInsert.success(function (data) {
              console.log(data);
              if(data.status == true){
                var reseva_id = parseInt(data.id);
                $scope.reserva_id_new = parseInt(data.id);
                reservasignadaycreadanew(3,reseva_id,$scope.selectedConductor,$scope.item);
                clearForm();
                initTables();
              }
          });
          requestInsert.error(function(data, status){
              console.log('error'+ status);
          });

        }else{
          grabar = false;
          alert('La reserva es con un minimo de '+horareserv+' hora de anticipacion');
        }
    }
  };

function reservasignadaycreadanew(flag,reserva_id,conductor_id,tiposervicio){

    $scope.screensUpdate = [{flag:flag,idReserva:reserva_id,idConductor:conductor_id,idTipoServicio:tiposervicio}];
    
    $.ajax({
        url: URL_CONTROLLER_SOCKET+'/w_reserva_asignado',
        data: $scope.screensUpdate[0],
        cache: false,
        method: 'post',
        success: function(data){
          $("#msgError").hide();
          if(data == true){
            DialogoTimerAngular(3);
          }
        }
    });
}


function agregarreserva(){
        console.log('ssssss');
        var restReserva = $http({
                      method: "post",
                      url: "../../BL/reservaController.php",
                      data: {
                        hidden_insert_reserva: 'hidden_insert_reserva',
                        data: $scope.screensTask
                      },
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                      });
        restReserva.success(function (data){
             console.log(data.id);
             setTimeout(function(){ 
                emitReservaConductor(data.id);
                initTables(); 
             }, 1000);
            
        });
}

function emitReservaConductor(id){

   $.ajax({
      url: URL_CONTROLLER_SOCKET+'/w_reserva_asignado_creado',
      data: {id:id},
      cache: false,
      method: 'post',
      success: function(data){
        console.log('w_reserva_asignado_creado');
        console.log(data);
      }
    });
}

function updatereserva(){

    var idreserva = $scope.idpopupser;
    var idConductor = $("#conductorupdate").val();
    var idTipoServicio = $("#tiposervicioidpopup").val();

        var requestupdateReserva = $http({
                  method: "post",
                  url: "../../BL/reservaController.php",
                  data: {update_reserva: 'update_reserva',conductor_id:idConductor, reserva_id:idreserva, tiposervicio_id:idTipoServicio},
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                  });
        requestupdateReserva.success(function (data) {
          initTables();
        });
}

function cambiarDivTimer(texto) {
    $("#divContador").html(texto);
}

function DialogoTimerAngular(flag){

  var dialog, formulario;
  var tiempo = 20;
  var contador = 0;
  var intervalTimer;

    $( "#dialog-confirm-angular" ).dialog({
      width: 500,
        resizable: false,
        closeText: "hide",
        height: "auto",
        modal: true,
        dialogClass: "no-close",
        title: 'Espera de Confirmación de Reserva'
    });

    intervalTimer = setInterval(function () {
        contador = contador + 1;
        if (contador <= tiempo) {
            if((tiempo - contador) < 20) {
                cambiarDivTimer((tiempo - contador));
            }

            if (contador % 2 == 0) {
               console.log('1 segundos...'+Rptadocs);
                if(Rptadocs == true){
                  console.log('Conductor respondio la solicitud');
                  console.log(flag);
                  if(flag == 1){
                    console.log('11');
                    agregarreserva();
                  }else if(flag == 2){
                    console.log('404 -->');
                    updatereserva();
                  }else{
                    console.log('NUEVA IMPLEMENTACION');
                    Rptadocs = false;
                    initTables();
                  }
                  
                  $( "#dialog-confirm-angular" ).dialog( "close" );
                  clearInterval(intervalTimer);
                  $("#divContador").html(20);
                }else{
                  //initTables();
                }
            }
        } else {
            
            if(flag == 3){
              actualizar_estado($scope.reserva_id_new,0);
            }

            $( "#dialog-confirm-angular" ).dialog( "close" );
            clearInterval(intervalTimer);
            $("#divContador").html(20);

        }
    }, 1000);

}

function actualizar_estado(reserva_id,estado){
        console.log('ssssss');
        var restReserva = $http({
                      method: "post",
                      url: "../../BL/reservaController.php",
                      data: {
                        update_estado_reserva: 'update_estado_reserva',
                        data: {reserva_id: reserva_id, estado: estado}
                      },
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                      });
        restReserva.success(function (data){
             console.log(data);
            initTables();
        });
}

  $scope.handleOk = function() {
    
    var emp = (typeof $scope.cliente_id == 'undefined')?0:$scope.cliente_id;
    var conductor = (typeof $scope.selectedConductor == 'undefined')?0:$scope.selectedConductor;
    $scope.screens = [{
          cliente_id:emp,
          cliente_tipo:$scope.tipocliente,
          cliente_fecha:$("#datetimepicker2").find("input").val(),
          cliente_celular:$scope.cliente_cel,
          cliente_nombre:$scope.cliente_nombre,
          cliente_correo:$scope.cliente_correo,
          cliente_costo:$("#costo").val(),
          tiposervicio:$scope.item,
          tipopago:$scope.tipopago,
          origen_direccion:$scope.org.fromorigen,
          origen_lat:$scope.org.latorg,
          origen_lng:$scope.org.lngorg,
          origen_id:$scope.idorg,
          destino_direccion:$scope.des.fromdestino,
          destino_lat:$scope.des.latdes,
          destino_lng:$scope.des.lngdes,
          destino_id:$scope.iddes,
          conductor_id:conductor,
    }];

    var requestInsert = $http({
              method: "post",
              url: "../../BL/reservaController.php",
              data: {
                  hidden_insert_reserva: 'hidden_insert_reserva',
                  data: $scope.screens
              },
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
              });

      requestInsert.success(function (data) {
          console.log(data);
          clearForm();
          initTables();
          $scope.dialogOpenform = false;
      });
      requestInsert.error(function(data, status){
          console.log('error'+ status);
          $scope.dialogOpenform = false;
      });  
  }

  $scope.handleCancel = function() {$scope.dialogOpenform = false;}

//});

//app.controller("MyCtrl", function($scope, $http, DTOptionsBuilder, DTColumnBuilder)
//{   
    $scope.title = "Delete";
    $scope.message1 = "¿Está seguro que desea borrar esta Reserva ?";
    $scope.dialogOpen = false;
    $scope.showModal = false;

    $scope.selectActionPopup = function() {

        var selecteid = $("#tiposervicioidpopup").val();
        var requestdrop = $http({
                        method: "post",
                        url: "../../BL/reservaController.php",
                        data: {droplisConductor: 'droplisConductor',selecteid : selecteid},
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        });
          requestdrop.success(function (data) {
            $scope.myConductorpopup = data;
          });
    };

    $scope.editMyCtrl = function(id, fecha, pago, tarifa, tiposervicio){
        console.log('tiposervicio....'+ tiposervicio);
        $scope.messageError='';
        $("#msgError").hide();
        var requestdropopup = $http({
                    method: "post",
                    url: "../../BL/reservaController.php",
                    data: {droplistipo: 'droplistipo'},
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
      requestdropopup.success(function (data) {
        $scope.myOptionspopup = data;

      });
        setTimeout(function(){
           $("#tiposervicioidpopup").val(tiposervicio);
           angular.element('#tiposervicioidpopup').triggerHandler('change');
        },500);

        $scope.idpopupser = id;
        $scope.tpagopopup = pago;
        $scope.tarifpopup = tarifa;
        $scope.fechapopup = fecha;
        $scope.showModal = !$scope.showModal;
    };

    $scope.Close = function(){
      $("#msgError").hide();
    }

    $scope.uppReserva = function(){

      var idreserva = $scope.idpopupser;
      var idConductor = $("#conductorupdate").val();
      var idTipoServicio = $("#tiposervicioidpopup").val();

      if(idTipoServicio.length == 0 && idConductor.length == 0){
          $("#msgError").show();
          $scope.messageError = 'Seleccione un elemento Tipo de servicio';
      }else if(idTipoServicio.length > 0 && idConductor.length == 0){
          $("#msgError").show();
          $scope.messageError = 'Seleccione un elemento Conductor';
      }else{
            $scope.screensUpdate = [{flag:2,idReserva:idreserva,idConductor:idConductor,idTipoServicio:idTipoServicio}];
            
            $.ajax({
                url: URL_CONTROLLER_SOCKET+'/w_reserva_asignado',
                data: $scope.screensUpdate[0],
                cache: false,
                method: 'post',
                success: function(data){
                  $('#modal').modal('hide');
                  $scope.showModal = false;
                  console.log(data);
                  $("#msgError").hide();
                  if(data == true){
                    
                    DialogoTimerAngular(2);
                  }
                }
            });
      }
    };

    initTables();
    function initTables(){

      $scope.users = {};

       $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withDisplayLength(10)
      .withPaginationType('full_numbers')
      .withOption("order", [
                    [0, "desc"]
        ])
      .withOption('bLengthChange', false);


     /* $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(10)
        .withOption('bLengthChange', false);*/


      var request = $http({
                    method: "post",
                    url: "../../BL/reservaController.php",
                    data: {
                      listar_reserva: 'listar_reserva'
                    },
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });

      request.success(function (data) {
          var estadoreserva = '';
          $.each( data, function( key, value ) {

            data[key].tipo_pago = (value.tipo_pago == 1)?'Efectivo':'Vale';
              switch(value.reserva_estado) {
                  case '0':estadoreserva = 'No Asignado';break;
                  case '1':estadoreserva = 'Asignado';break;
                  case '2':estadoreserva = 'Enviado';break;
                  case '3':estadoreserva = 'Atendida';break;
                  case '4':estadoreserva = 'Vencido';break;
                  case '5':estadoreserva = 'Por Confirmar';break;
              }
              data[key].reserva_estado_text = estadoreserva;

          });
          $scope.users = data;

          Users = data;


      });
      request.error(function(data, status){
          console.log('error'+ status);
      });

    };

    $scope.deleteMyCtrl = function (task) {
        $scope.reservaid = task;
        $scope.dialogOpen = true;
    };

    $scope.handleOkK = function() {
        
        var reservaid = this.$parent.reservaid;
        var requestdelete = $http({
                  method: "post",
                  url: "../../BL/reservaController.php",
                  data: {
                      delete_reserva: 'delete_reserva',
                      reserva_id: reservaid
                  },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                  });

          requestdelete.success(function (data) {
              if(data == true){
                initTables();
              }
              $scope.dialogOpen = false;
          });
          requestdelete.error(function(data, status){
              console.log('error'+ status);
              $scope.dialogOpen = false;
          });
    }

    $scope.handleCancell = function() {
        console.log('handle cancel');
        $scope.dialogOpen = false;
    }

  $scope.filtro = {
    estado : null,
    fecha : {
      desde : null,
      hasta : null
    },
    hora : null
  };

  $scope.filtro.buscar = function(){

    var fechaDesde  = Date.parse(new Date($('.datetime#desde').val()));
    var fechaHasta  = Date.parse(new Date($('.datetime#hasta').val()));

    fechaDesde = isNaN(fechaDesde) ? Date.parse('2000/01/01') : fechaDesde;
    fechaHasta = isNaN(fechaHasta) ? Date.parse('2050/12/12') : fechaHasta; 

    /* POR ESTADO */
    var lastUsers = Users;

    if($scope.filtro.estado != 'todos'){
      lastUsers = Users.filter(function(user) {
          return  user.reserva_estado == $scope.filtro.estado;       
      });
    }

    /* POR FECHA */
    lastUsers = lastUsers.filter(function(user) {
      fechaLoop = Date.parse(new Date((user.reserva_fecha).substring(0,10)));
      return (fechaLoop >= (fechaDesde - (24*60*60*1000))  && fechaLoop <= fechaHasta);
    });


    $scope.users = lastUsers;


    console.log($scope.filtro.hora);

    console.log($scope.filtro.hora);
    /*POR HORA CREO */
    if($scope.filtro.hora != null && $scope.filtro.hora != 'null'){

      var fechaActual = Date.parse(new Date());
      var filtroHora = $scope.filtro.hora * 60*60*1000;

      $scope.users =  Users.filter(function(user) {
        fechaLoop = Date.parse(new Date(user.reserva_fecha));
        //return (fechaLoop >= (fechaActual - filtroHora) && fechaLoop <= fechaActual)
        
      return (fechaLoop >= fechaActual && fechaLoop <= (fechaActual + filtroHora))
      });
      if($scope.filtro.estado != 'todos'){
        $scope.users = $scope.users.filter(function(user) {
        return  user.reserva_estado == $scope.filtro.estado;     
        });
      }

    }

    $scope.$digest();

  } // CIERRE EVENTO FILTRO





});

app2.controller('movementsController', function($scope, $http, DTOptionsBuilder, DTColumnBuilder) {

    $scope.message = '';
    $scope.movimiento = {};
    $scope.showModal = false;

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(12)
        .withPaginationType('full_numbers')
        /*.withOption("order", [
                      [0, "desc"]
          ])*/
        .withOption('bLengthChange', false);

    var request = $http({
                    method: "post",
                    url: "BL/movimientosController.php",
                    data: {
                      listar_movimiento: 'listar_movimiento'
                    },
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });

      request.success(function (data) {
        // console.log(data);
        $.each( data, function( key, value ) {

            var f = new Date(value.ff * 1000);
            var fechactual = f.getFullYear()+'-'+('0'+ (f.getMonth()+1))+"-"+('0' +(f.getDate())).slice(-2);
            data[key].ff = fechactual;

            data[key].tipo_pago = (value.tipo_pago == 1)?'Efectivo':((value.tipo_pago == 2)?'Vale':'Tarjeta');
            //data[key].tarifa = parseFloat(value.tarifa).toFixed(2);
            data[key].tarifa = parseInt(value.tarifa);

        });
          // console.log(data);
            $scope.movimiento = data;
      });
      request.error(function(data, status){
          console.log('error'+ status);
      });

  $scope.ModalMovim = function(id,latorigen,lngorigen,latdestino,lngdestino){

      var inicio = new google.maps.LatLng(parseFloat(latorigen),parseFloat(lngorigen));
      var fin = new google.maps.LatLng(parseFloat(latdestino),parseFloat(lngdestino));

      var peticion = {
        origin:inicio,
        destination:fin,
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
      };

      servicioDireccion.route(peticion, function(response, status) {

        if (status === google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
       
      });
       directionsDisplay.setMap(map);
  }

  $scope.ModalView = function(id){

     var request = $http({
          method: "post",
          url: "../../BL/movimientosController.php",
          data: {
            details_movimiento: 'details_movimiento',
            servicio_id:id
          },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          });

      request.success(function (data) {
          
          $.each( data, function( key, value ) {
             data[key].adicional = parseInt(value.peaje) + parseInt(value.parqueo) + parseInt(value.espera) + parseInt(value.parada);
              data[key].tarifa = parseInt(value.tarifa);
          });
          
          if(data[0].adicional > 0){
            $("#modal .modal-content").css("height","460px");
          }else{
            $("#modal .modal-content").css("height","360px");
          }

          $scope.detailmovi = data[0];
          $scope.showModal = !$scope.showModal;
      });

      request.error(function(data, status){
          console.log('error'+ status);
      });

  }

});

app2.controller('liquidacionController', function($scope, $http, DTOptionsBuilder, DTColumnBuilder){

    clearLiquidacionForm($scope);
    getLiquidacion();

      $scope.conductor_id = '';

      $( "#celularConductor" ).autocomplete({
        source: function( request, response ) {
          $.ajax({
            url: "../../BL/recargasController.php",
            method: 'post',
            data: {q: request.term, listar_recarga:'listar_recarga'},
            success: function( data ) {
              response(data);
            }
          });
        },
        minLength: 2,
        select: function( event, ui ) {
           if (ui.item) {
                  $scope.conductor_id = ui.item.idConductor;
              }
        }
      });

      $scope.CloseLiquidacion = function(id){
        if(id == 1){
          $("#msgErrorLiquidacion").show();
        }else{
          $("#msgSuccessLiquidacion").show();
        }
         $scope.messageLiquidacion = '';
      }

      function clearLiquidacionForm($scope){

        $scope.visitorInfo = {};
        $scope.conductor_id = '';
      }

      function getLiquidacion(){

          $scope.loading = true;
          $scope.liquidaciones = {}; 
          $scope.dtOptions = DTOptionsBuilder.newOptions()
          .withDisplayLength(10)
          .withPaginationType('full_numbers')
          .withOption("order", [
                        [5, "desc"]
            ])
          .withOption('bLengthChange', false);

        var requestLiquidacion = $http({
                        method: "post",
                        url: "../../BL/recargasController.php",
                        data: {lista_liquidacion: 'lista_liquidacion'},
                          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        });

          requestLiquidacion.success(function (data) {
            
              console.log(data);
              $scope.liquidaciones = data;
              $scope.loading = false;
          });

          requestLiquidacion.error(function(data, status){
              console.log('error'+ status);
          });
      };

      $scope.saveLiquidacion = function(formData) {

        var request = $http({
            method: "post",
            url: "../../BL/recargasController.php",
            data: {
              insert_liquidacion: 'insert_liquidacion',
              data:formData,
              id:$scope.conductor_id
            },
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });

        request.success(function (data) {
          
              if(data = true){
                $scope.messageLiquidacion = 'Operacion Realizada.'
                $("#msgSuccessLiquidacion").show();
                clearLiquidacionForm($scope);
                getLiquidacion();
              }else{
                $scope.messageLiquidacion = 'Ups, ocurrio un error inesperado.'
                $("#msgErrorLiquidacion").show();
              }
        });
        request.error(function(data, status){console.log('error'+ status);});
      };
});

app2.controller('recargasController',function($scope, $http, DTOptionsBuilder, DTColumnBuilder){

    clearRecargaForm($scope);
    getRecarga();
    $scope.bancos = [
        {
            "key": "0",
            "value": "Seleccione una opcion"
        },
        {
            "key": "1",
            "value": "BCP"
        },
        {
            "key": "2",
            "value": "BBVA"
        },
        {
            "key": "3",
            "value": "Scotiabank"
        },
        {
            "key": "4",
            "value": "Interbank"
        },
        {
            "key": "5",
            "value": "BanBif"
        },
        {
            "key": "6",
            "value": "Banco Financiero"
        },
        {
            "key": "7",
            "value": "Oficina central"
        }
    ];

    $scope.select_default = '0';
    $scope.conductor_id = '';

    $( "#celularConductor" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "../../BL/recargasController.php",
          method: 'post',
          data: {q: request.term, listar_recarga:'listar_recarga'},
          success: function( data ) {
            response(data);
          }
        });
      },
      minLength: 2,
      select: function( event, ui ) {
         if (ui.item) {
                $scope.conductor_id = ui.item.idConductor;
            }
      }
    });

    function clearRecargaForm($scope){

      $scope.visitorInfo = {};
      $scope.select_default = '0';
      $scope.conductor_id = '';
    }


    $scope.CloseRecarga = function(id){
      if(id == 1){
        $("#msgErrorRecarga").show();
      }else{
        $("#msgSuccessRecarga").show();
      }
       $scope.messageRecarga = '';
    }

    $scope.saveRecarga = function(formData) {
      console.log($("#datetimepicker2").find("input").val());
      var request = $http({
          method: "post",
          url: "../../BL/recargasController.php",
          data: {
            insert_recarga: 'insert_recarga',
            data:formData,
            select:$scope.select_default,
            fecha:$("#datetimepicker2").find("input").val()+':00',
            id:$scope.conductor_id
          },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          });

      request.success(function (data) {
            if(data = true){
              $scope.messageRecarga = 'Operacion Realizada.'
              $("#msgSuccessRecarga").show();
              $("#datetimepicker2").find("input").val('');
              clearRecargaForm($scope);
              getRecarga();
            }else{
              $scope.messageRecarga = 'Ups, ocurrio un error inesperado.'
              $("#msgErrorRecarga").show();
            }
      });
      request.error(function(data, status){console.log('error'+ status);});
    };

  $scope.singleModel = function(id,index, es){

    var i = index-1;
    var switch_flag = es;
    var requestoConduc = $http({
                    method: "post",
                    url: "../../BL/recargasController.php",
                    data: {
                        switch:switch_flag,
                        recarga:id,
                        estado:'estado'
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
    requestoConduc.success(function (data) {

        if(data.status == true){

          $scope.messageError ='';
          $("#msgErrorRecarga").hide();

          if(data.mensaje == true){
            $scope.recargas[i].enabled = true;
            $scope.recargas[i].recarga_check = 1;
          }else{
            $scope.recargas[i].enabled = false;
            $scope.recargas[i].recarga_check = 0;
          }

          $scope.recargas[i].detalle = data.tipo_recarga;

        }else{

            $scope.recargas[i].enabled = false;
            $("#msgErrorRecarga").show();
            $scope.messageRecarga = 'Ups Error Inesperado al Actualizar.';
        }
    });

  };
  $scope.changeRecarga = function(id,index) {
      
    var i = index-1;
    var switch_flag = $scope.recargas[i].enabled;
    var requestoConduc = $http({
                    method: "post",
                    url: "../../BL/recargasController.php",
                    data: {
                        switch:switch_flag,
                        recarga:id,
                        estado:'estado'
                          },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });
    requestoConduc.success(function (data) {

        if(data.status == true){

          if(data.mensaje2 != null){

            $("#msgSuccessRecarga").show();
            $scope.messageRecarga = data.mensaje2;
            getRecarga();

          }else{

              $scope.messageError ='';
              $("#msgErrorRecarga").hide();

            if(data.mensaje == true){
              $scope.recargas[i].enabled = true;
              $scope.recargas[i].recarga_check = 1;
            }else{
              $scope.recargas[i].enabled = false;
              $scope.recargas[i].recarga_check = 0;
            }

            $scope.recargas[i].detalle = data.tipo_recarga;
          }
        }else{

            $scope.recargas[i].enabled = false;
            $("#msgErrorRecarga").show();
            $scope.messageRecarga = 'Ups Error Inesperado al Actualizar.';
        }
    });

  };

  function getRecarga(){

      $scope.loading = true;
      $scope.recargas = {}; 
      $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withDisplayLength(10)
      .withPaginationType('full_numbers')
      .withOption("order", [
                    [5, "desc"]
        ])
      .withOption('bLengthChange', false);

    var requestRecarga = $http({
                    method: "post",
                    url: "../../BL/recargasController.php",
                    data: {lista_recargas: 'lista_recargas'},
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    });

      requestRecarga.success(function (data) {
       
          $.each( data, function( key, value ) {
              var variable='';
              switch(value.recarga_tipo){
                  case "1":variable = 'BCP';break;
                  case "2":variable = 'BBVA';break;
                  case "3":variable = 'Scotiabank';break;
                  case "4":variable = 'Interbank';break;
                  case "5":variable = 'BanBif';break;
                  case "6":variable = 'Banco Financiero';break;
                  case "7":variable = 'Oficina central';break;
              }
              data[key].tiponombre = variable;

              if(value.recarga_check == 1){
                data[key].detalle = 'Aprobado';
                data[key].enabled = true;
              }else if(value.recarga_check == 2){
                data[key].detalle = 'Pendiente';
                data[key].enabled = false;
              }else{
                data[key].detalle = 'Rechazado';
                data[key].enabled = false;
              }
          });
          console.log(data);
         $scope.recargas = data;
         $scope.loading = false;
      });
      requestRecarga.error(function(data, status){
          console.log('error'+ status);
      });
  };
});

//angular.module('components', [])
app2.directive('mhiDialog', function ($timeout) {
    return {
        scope: { 
            okButton: '@',
            okCallback: '=',
            cancelButton: '@',
            cancelCallback: '=',
            open: '@',
            title: '@',
            width: '@',
            height: '@',
            show: '@',
            hide: '@',
            autoOpen: '@',
            resizable: '@',
            closeOnEscape: '@',
            hideCloseButton: '@'
        },
        replace:false,
        transclude: true,    // transclusion allows for the dialog 
                              // contents to use angular {{}}
        template: '<div ng-transclude></div>',      // the transcluded content 
                                                    //is placed in the div
        link: function (scope, element, attrs) {
            // Close button is hidden by default
            var hideCloseButton = attrs.hideCloseButton || true;
            
            // Specify the options for the dialog
            var dialogOptions = {
                autoOpen: attrs.autoOpen || false,
                title: attrs.title,
                width: attrs.width || 350,
                height: attrs.height || 200, 
                modal: attrs.modal || false,
                show: attrs.show || '',
                hide: attrs.hide || '',
                draggable: attrs.draggable || true,
                resizable: attrs.resizable,
                closeOnEscape: attrs.closeOnEscape || false,
                
                close: function() {
                    console.log('closing...');
                    //console.log(scope);
                    /*
                    $timeout(function() {
                       scope.$apply(scope.cancelCallback());
                    },0);    
                    */
                },
                open: function(event, ui) { 
                    // Hide close button 
                    if(hideCloseButton == true) {
                        $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    }
                } 
            };
           
           // Add the buttons 
           dialogOptions['buttons'] = [];
           if(attrs.okButton) {
               var btnOptions = { 
                   text: attrs.okButton, 
                   click: function() { scope.$apply(scope.okCallback()); }
               };
               dialogOptions['buttons'].push(btnOptions);    
           }
           
            if(attrs.cancelButton) {
               var btnOptions = { 
                   text: attrs.cancelButton, 
                   click: function() { scope.$apply(scope.cancelCallback()); }
               };
               dialogOptions['buttons'].push(btnOptions);    
           }
           
           // Initialize the element as a dialog
           // For some reason this timeout is required, otherwise it doesn't work
           // for more than one dialog
           $timeout(function() {
               $(element).dialog(dialogOptions);
           },0);
            
            // This works when observing an interpolated attribute
            // e.g {{dialogOpen}}.  In this case the val is always a string and so
            // must be compared with the string 'true' and not a boolean
            // using open: '@' and open="{{dialogOpen}}"
            attrs.$observe('open', function(val) {
                console.log('observing open val=' + val);
                if (val == 'true') {
                    console.log('open');
                    $(element).dialog("open");
                } 
                else 
                {
                    console.log('close');
                    $(element).dialog("close");
                    
                }
            });
            
            // This allows title to be bound
            attrs.$observe('title', function(val) {
                console.log('observing title: val=' + val);
                //$(element).dialog("option", "title", val);                   
            });
        } 
    }
}); 

app2.directive('modal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true){
            $(element).modal('show');
          }
          else
          {
            $(element).modal('hide');
          }
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });

app2.directive('fileModel', [
  '$parse',
  function ($parse) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function(){
          scope.$apply(function(){
            if (attrs.multiple) {
              modelSetter(scope, element[0].files);
            }
            else {
              modelSetter(scope, element[0].files[0]);
            }
          });
        });
      }
    };
  }
]);


app2.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}]);

app2.directive('loading', function () {
      return {
        restrict: 'E',
        replace:true,
        template: '<div class="loading"><img src="http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif" width="20" height="20" />LOADING...</div>',
        link: function (scope, element, attr) {
              scope.$watch('loading', function (val) {
                  if (val)
                      $(element).show();
                  else
                      $(element).hide();
              });
        }
      }
  })

app2.directive("switch",
  function(){
  return{
    restrict:"AE",
    replace:!0,
    transclude:!0,
    template:function(n,e){
        var s="";return s+="<span",s+=' class="switch'+(e.class?" "+e.class:"")+'"',s+=e.ngModel?' ng-click="'+e.disabled+" ? "+e.ngModel+" : "+e.ngModel+"=!"+e.ngModel+(e.ngChange?"; "+e.ngChange+'()"':'"'):"",s+=' ng-class="{ checked:'+e.ngModel+", disabled:"+e.disabled+' }"',s+=">",s+="<small></small>",s+='<input type="checkbox"',s+=e.id?' id="'+e.id+'"':"",s+=e.name?' name="'+e.name+'"':"",s+=e.ngModel?' ng-model="'+e.ngModel+'"':"",s+=' style="display:none" />',s+='<span class="switch-text">',s+=e.on?'<span class="on">'+e.on+"</span>":"",s+=e.off?'<span class="off">'+e.off+"</span>":" ",s+="</span>"}}});

