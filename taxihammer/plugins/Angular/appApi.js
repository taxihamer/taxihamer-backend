var app = angular.module('myappApi', ['datatables','gm','file-model','datatables.columnfilter','kendo.directives','ui.bootstrap']);


app.service('filteredListService', function () {
     
    this.searched = function (valLists,toSearch) {
        return _.filter(valLists, 
        function (i) {
            /* Search Text in all 3 fields */
            return searchUtil(i, toSearch);
        });        
    };
    
    this.paged = function (valLists,pageSize)
    {
        retVal = [];
        for (var i = 0; i < valLists.length; i++) {
            if (i % pageSize === 0) {
                retVal[Math.floor(i / pageSize)] = [valLists[i]];
            } else {
                retVal[Math.floor(i / pageSize)].push(valLists[i]);
            }
        }
        return retVal;
    };
 
});

app.filter('cortarTexto', function(){
  return function(input, limit){
    return (input.length > limit) ? input.substr(0, limit)+'---' : input;
  };
});

app.controller('apisController',function($scope, $http, $filter, filteredListService){

    $scope.countgrupo = 10;
    $scope.grupo = '';

    $scope.searchText = '';
    $scope.messageApi = '';
    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;

    $scope.secret_here = '';
    $scope.key_here = '';
    $scope.secret_foursquare = '';
    $scope.key_foursquare = '';
    $scope.key_ios = '';
    $scope.key_android = '';
    $scope.key_web = '';

    $scope.reverse = false;

  var fechaActual = fechaActual();
  var res = fechaActual['actual'].split(" ");
  var mes = res[0].split("-");
  console.log(mes);
  //var minimo = mes[0]+'-'+mes[1]
  var minimo = '2016-01';
  //var final = parseInt(mes[1])+1;
  //var minimofinal = mes[0]+'-'+final
  var minimofinal = '2016-01';
  var sumaxi = parseInt(mes[0])+1;
  var maximo = sumaxi+'-12';

  $("#inputinicio").attr('min',minimo.toString());
  $("#inputinicio").attr('max',maximo.toString());

  $("#inputfinal").attr('min',minimofinal.toString());
  $("#inputfinal").attr('max',maximo.toString());

  $scope.inicio = { value: new Date(parseInt(mes[0]), parseInt(mes[1])-1, mes[2])};
  $scope.final = { value: new Date(parseInt(mes[0]), parseInt(mes[1])-1, mes[2])};

  function fechaActual(){

    var f = new Date();
    var fechactual = f.getFullYear()+'-'+(f.getMonth()+1)+"-"+f.getDate();
    var horaactual = (f.getHours())+":"+f.getMinutes()+":"+f.getSeconds();
    var fecha = [];

    fecha['actual'] = fechactual+' '+horaactual;

    return fecha;
  };

    function Mensajes(status,type){

        $(".alert").hide();
        if(status == true){
            if(type == 1){
                $("#msgSuccessApi").show();
            }else{
                $("#msgErrorValid").show();
            }
        }else{
        $("#msgErrorApi").show();
        }
    };

    $scope.changeCallback = function(codigo, index) {

      var i = index-1;
      var switch_flag = $scope.apis[i].enabled;

     
      var requestoggle = $http({
                      method: "post",
                      url: "../../BL/apisController.php",
                      data: {
                            witch:switch_flag,
                            api:codigo,
                            estado:'estado'
                            },
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                      });
      requestoggle.success(function (data) {
           console.log(data);
           if(data.status == false){
              $("#msgErrorApi").show();
              $("#msgSuccessApi").hide();
              $scope.messageApi = data.msg;
           }else{
              $("#msgSuccessApi").show();
              $("#msgErrorApi").hide();
              getApis();
           }
            
      });
    };

    $scope.saveApis = function(){

        var newDateInicial = $filter('date')(new Date($scope.inicio.value), 'yyyy-MM-dd');
        var newDateFinal = $filter('date')(new Date($scope.final.value), 'yyyy-MM-dd');

        var valfechaInicio = ($("#inputinicio").val().length > 0 )?$("#inputinicio").val()+'-01':newDateInicial;
        var valfechaFinal = ($("#inputfinal").val().length > 0)?$("#inputfinal").val()+'-01':newDateFinal;

        var saveApi = [{
             api_google_web:$scope.key_web,
             api_google_android:$scope.key_android,
             api_google_ios:$scope.key_ios,
             api_foursquare_key:$scope.key_foursquare,
             api_foursquare_secret:$scope.secret_foursquare,
             api_here_key:$scope.key_here,
             api_here_secret:$scope.secret_here,
             api_fecha_create:valfechaInicio,
             api_fecha_end:valfechaFinal
        }];

        var saveApis = $http({
                  method: "post",
                  url: "../../BL/apisController.php",
                  data: {
                    saveApis: 'saveApis',
                    send: saveApi
                  },
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                  });

        saveApis.success(function (data) {

            Mensajes(data.status,data.type);
            $scope.messageApi = data.mensaje;
            getApis();

        });
        saveApis.error(function(data, status){
            console.log('error'+ status);
        });
    };

  getApis();
  function getApis() {

        var requestApis = $http({
              method: "post",
              url: "../../BL/apisController.php",
              data: {
                  page: $scope.currentPage,
                  size: $scope.pageSize,
                  search: $scope.searchText,
                  lista_apis: 'lista_apis'
                    },
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
              });

        requestApis.success(function (data) {
              console.log(data.Request.length);

              if(data.Request.length <= 0){$("#nullOld").show();}else{$("#nullOld").hide();};
              //$scope.conductors = [];
              $scope.totalItems = data.totalCount;
              $scope.startItem = ($scope.currentPage - 1) * $scope.pageSize + 1;
              $scope.endItem = $scope.currentPage * $scope.pageSize;
              if ($scope.endItem > $scope.totalCount) {$scope.endItem = $scope.totalCount;}

                $.each(data.Request, function( key, value ) {

                  if(value.api_estado == 1){
                    data.Request[key].enabled = true;
                  }else{
                    data.Request[key].enabled = false;
                  }

                })

              $scope.apis = data.Request;
        });  
  };

});

app.directive("switch",
  function(){
  return{
    restrict:"AE",
    replace:!0,
    transclude:!0,
    template:function(n,e){
        var s="";return s+="<span",s+=' class="switch'+(e.class?" "+e.class:"")+'"',s+=e.ngModel?' ng-click="'+e.disabled+" ? "+e.ngModel+" : "+e.ngModel+"=!"+e.ngModel+(e.ngChange?"; "+e.ngChange+'()"':'"'):"",s+=' ng-class="{ checked:'+e.ngModel+", disabled:"+e.disabled+' }"',s+=">",s+="<small></small>",s+='<input type="checkbox"',s+=e.id?' id="'+e.id+'"':"",s+=e.name?' name="'+e.name+'"':"",s+=e.ngModel?' ng-model="'+e.ngModel+'"':"",s+=' style="display:none" />',s+='<span class="switch-text">',s+=e.on?'<span class="on">'+e.on+"</span>":"",s+=e.off?'<span class="off">'+e.off+"</span>":" ",s+="</span>"}}});

