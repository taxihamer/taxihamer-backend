<?php
ini_set('display_errors', 1);

require 'lib/PHPMailer/PHPMailerAutoload.php';
require 'DAL/datosSeguridadDAO.php';
require 'DAL/clientesDAO.php';
require 'DAL/conductorDAO.php';
require 'DAL/servicioDAO.php';
require 'DAL/vehiculoDAO.php';
require 'DAL/constantes.php';

$data['servicio_id'] = $_REQUEST['cliente_id'];

$servicioDAO = new servicioDAO();
$datosSeguridadDAO = new datosSeguridadDAO();
$clientesDAO = new clientesDAO();
$conductorDAO = new conductorDAO();
$vehiculoDAO = new vehiculoDAO();

$servicio = $servicioDAO->listaServicio($_REQUEST['cliente_id']);

$send['cliente_id'] = $servicio[0]['cliente_id'];
$Res = $datosSeguridadDAO->listaSeguridad($send);

$conductor = $conductorDAO->listaBy($servicio[0]['conductor_id']);
$cliente = $clientesDAO->listaClienteId($servicio[0]['cliente_id']);
$vehiculo = $vehiculoDAO->listaBy($conductor[0]['unidad_id']);

$enviar = true;
//var_dump($Res);exit;
$htmBody = '';
if($_REQUEST['tipo'] == '1'){//Correo de Seguridad

	if(!empty($Res)){

		$Address = $Res[0]['dseg_correo'];
		$Nombre = $Res[0]['dseg_nombre'];

		$htmBody = "<center><table border='0', cellpadding='0', cellspacing='0', align='center'>
					  <tr>
					    <td colspan='7' style='text-align:center;'>				    	
							<img src='".URL_CORREO."includes/img/logo_hammer.png' class='img-responsive' alt=''/>
							<p  style='display: inline-block;text-align: justify;'><b>GRACIAS POR UTILIZAR Y CONFIAR EN TAXI HAMMER</b><br>
							<b>APLICACION TOTALMENTE PERUANA. SIGUE UTILIZANDO LA</b><br>
							<b>APLICACION, SE VIENEN GRANDES SORPRESAS.</b> </p>
					    </td>
					  </tr>
					  <tr>
					    <td colspan='5' width='290', height='57'>Desde : <b>".NAME_EMPRESA."</b> <br>Asunto : Inicio Servicio</td>
					  </tr>
					  <tr>
					    <td colspan='5' width='290' height='40'>El pasajero <b>".utf8_decode($cliente[0]['cliente_nombre'])." ".utf8_decode($cliente[0]['cliente_apellido'])."</b> acaba de iniciar su servicio en <b>".NAME_EMPRESA."</b>.</td>
					  </tr>
					  <tr>
					    <td width='181' height='38'> Origen : <b>".utf8_decode($servicio[0]['origen_direccion'])." </b><br> Destino : <b>".utf8_decode($servicio[0]['destino_direccion'])."</b> </td>
					  </tr>
					  <tr>
					    <td width='181' height='49'>Conductor : <b>".utf8_decode($conductor[0]['conductor_nombre'])." ".utf8_decode($conductor[0]['conductor_apellido'])."</b></td>
					    <td width='181' height='49'>Nro. Placa : <b>".utf8_decode($vehiculo[0]['unidad_placa'])."</b></td>
					    <td width='181' height='49'>Telefono : <b>".utf8_decode($conductor[0]['conductor_celular'])."</b></td>
					  </tr>
					  <tr>
					  	<td width='181' height='34'>Dia : <b>".$servicio[0]['fecha']."</b> <br> Hora : <b>".$servicio[0]['hora']."</b></td>
					  </tr>
					  <tr>
					  	<td colspan='4' width='271' height='23'>| Por favor monitorea el trayecto.<a href='".URL_CORREO."index.php?viewMapConductor=".$_REQUEST['cliente_id']."' target='_blank'>Click Aqui</a></td>
					  </tr>
					</table></center>";
	}else{
		$enviar = false;
	}
}else{
	if(!empty($cliente[0]['cliente_correo'])){

			$Address = $cliente[0]['cliente_correo'];
			$Nombre = $cliente[0]['cliente_nombre'];

			$htmBody = "
						<style type='text/css'>

						.tg  {border-spacing:0;border:none;}
						.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;overflow:hidden;word-break:normal;}
						.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;overflow:hidden;word-break:normal;}
						.tg .tg-s6z2{text-align:center}
						.tg .tg-baqh{text-align:center;vertical-align:top}
						.tg .tg-yw4l{vertical-align:top}
						p { color:black !important;}
						</style>
						<div class='container'>
							<center>
							<div>
								<img src='".URL_CORREO."includes/img/logo_hammer.png' class='img-responsive' alt='' />
								<p style='display: inline-block;text-align: justify;'><b>GRACIAS POR UTILIZAR Y CONFIAR EN TAXI HAMMER</b><br>
								<b>APLICACION TOTALMENTE PERUANA. SIGUE UTILIZANDO LA</b><br>
								<b>APLICACION, SE VIENEN GRANDES SORPRESAS.</b> </p>
							</div>
							<div >
								<img src='".URL_CORREO."BL/".$conductor[0]['conductor_foto']."' style='border-radius:70px;border: 1px solid rgba(175, 175, 175, 0.33);'/>
							</div>
							<h4>Gracias por viajar con : <b>".utf8_decode($conductor[0]['conductor_nombre'])." ".utf8_decode($conductor[0]['conductor_apellido'])."</b></h4>
							<p>Conductor : <b>".utf8_decode($conductor[0]['conductor_nombre'])." ".utf8_decode($conductor[0]['conductor_apellido'])."</b></p>
							<p>Nro.Placa : <b>".utf8_decode($vehiculo[0]['unidad_placa'])."</b></p>
							<p>Telefono : <b>".utf8_decode($conductor[0]['conductor_celular'])."</b></p>
							<p>Origen de Viaje : <b>".$servicio[0]['hora_inicio']." ".utf8_decode($servicio[0]['origen_direccion'])."</b> </p>
							<p>Destino de viaje : <b>".$servicio[0]['hora_fin']." ".utf8_decode($servicio[0]['destino_direccion'])."</b></p>
						</center>
						</div>
						";
	}else{

		$enviar = false;
	}
}

//echo '<pre>'; print_r($htmBody); echo '</pre>'; exit;
//<p>Tarifa : <b>".intval($servicio[0]['tarifa'])."</b></p> movil.pe1@outlook.com
if($enviar != false){
	//var_dump(HOST,USERNAME,PASSWORD,SMTPSECURE,PORT);
	//exit;
	$mail = new PHPMailer;
	//$mail->SMTPDebug = 3;                               // Enable verbose debug output
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = HOST;//'smtp.live.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = USERNAME;                 // SMTP username
	$mail->Password = PASSWORD;
	$mail->SMTPSecure = SMTPSECURE; 
	$mail->Port = PORT;  
	$mail->setFrom(FROM, NAME_EMPRESA);
	//$mail->addAddress('shuanay@appslovers.com', 'Saul'); 
	$mail->addAddress($Address, $Nombre);

	$mail->isHTML(true);  
	// Set email format to HTML
	$mail->Subject = utf8_decode('Informacion de Servicio: '.$cliente[0]['cliente_nombre'].$cliente[0]['cliente_apellido']);
	$mail->Body    = $htmBody;
	$mail->AltBody = URL_CORREO;


	if(!$mail->send()) {
	   echo 'Message could not be sent.';
	   echo 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
	   echo 'Message has been sent';
	}

}else{
	echo 'Mensaje no pudo ser enviado.';
}


/*$mail->From = trim_input("receivingEmailAdress@outlook.com");
$mail->FromName = trim_input($_POST['Name']);
$mail->AddAddress("receivingEmailAdress@outlook.com", "my name");
$mail->AddReplyTo(trim_input($_POST['Email']), trim_input($_POST['Name']));*/
/*
require 'lib/PHPMailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp-relay.gmail.com';  // Specify main and backup SMTP servers
//$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'info@appslovers.com';                 // SMTP username
$mail->Password = 'Appsl0vers101';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->setFrom('kelvin.ca91@gmail.com', 'Mailer');
$mail->addAddress('kelvin.ca91@gmail.com', 'Joe User');     // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Here is the subject';
$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
/*

include 'lib/mailin.php';
$mailin = new Mailin('kelvin.ca91@gmail.com', 'PnNLgw8T4KEZUtCR');
$mailin->
addTo('kelvin.ca91@gmail.com', 'Appslovers')->
setFrom('info@appslovers.com', 'Appslovers')->
setSubject('Escriba el asunto aquí')->
setText('Hola')->
setHtml('<strong>Hola</strong>');
if($mailin->send()){
	echo 'enviado';
	var_dump($mailin->send());
}else{
	echo 'No enviado';
}
/**
El mensaje de éxito se enviará de esta forma:
{'result' => true, 'message' => 'E-MAILS enviados'}
*/

