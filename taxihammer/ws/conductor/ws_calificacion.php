<?php
header('Content-Type: application/json');

$data['cliente_id'] = $_REQUEST['cliente_id'];
$data['conductor_id'] = $_REQUEST['conductor_id'];
$data['calificacion_tipoid'] = $_REQUEST['tipo'];
$data['servicio_id'] = $_REQUEST['servicio_id'];
$data['calificacion_value'] = $_REQUEST['calificacion'];

error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once'../../DAL/calificacionDAO.php';
require_once '../../DAL/clientesDAO.php';

$calificacionDAO = new calificacionDAO();
$res = $calificacionDAO->insertarCalificacion($data);
$promedio = $calificacionDAO->calificacionPromedio($_REQUEST['cliente_id'],$_REQUEST['tipo']);
// echo '<pre>'; print_r($promedio[0]); echo '</pre>';
$cliente_calificacion = $promedio[0]['promedio'];

$clientesDAO = new clientesDAO();
$result = $clientesDAO->updateCalificacion($_REQUEST['cliente_id'],$cliente_calificacion);
// echo '<pre>'; print_r($result); echo '</pre>';

if($result == true){
	$json['mensaje'] = 'Calificación Actualizada con Éxito';
	$json['status'] = true;
}else{
	$json['mensaje'] = 'Error actualizando calificación';
	$json['status'] = false;
}

echo json_encode($json);