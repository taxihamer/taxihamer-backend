<?php
header('Content-Type: application/json');

require '../../lib/PHPMailer2/PHPMailerAutoload.php';
require_once'../../DAL/promocionDAO.php';
require_once'../../DAL/conductorDAO.php';
require_once '../../DAL/constantes.php';

$promocionDAO = new promocionDAO();
$conductorDAO = new conductorDAO();

$data['conductor_id'] = $_REQUEST['conductor_id'];
$data['promo_id'] = $_REQUEST['promo_id'];

error_reporting(E_ALL);
ini_set('display_errors', 1);

$correo = $conductorDAO->listaConductorId($data['conductor_id']);
$res = $promocionDAO->debitarPuntosCond($data);
$promocion = $promocionDAO->listaCanjeMailer($data);

if($res == true){

		$to = $correo[0]['conductor_correo'];
		$Nombre = $correo[0]['conductor_nombre'].' '.$correo[0]['conductor_apellido'];

		$message = '<html><body>
					<center>
						<div style="margin-bottom:20px;">
							<img src="'.URL_CORREO.'includes/img/logo_hammer.png" class="img-responsive" alt="" />
							<p style="display: inline-block;text-align: justify;"><b>GRACIAS POR UTILIZAR Y CONFIAR EN TAXI HAMMER</b><br>
							<b>APLICACION TOTALMENTE PERUANA. SIGUE UTILIZANDO LA</b><br>
							<b>APLICACION, SE VIENEN GRANDES SORPRESAS.</b> </p>
						</div>
						</center>
					';
		$message .= '<p>Usted a canjeado esta promocion</p>';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Nombre:</strong> </td><td>" . $correo[0]['conductor_nombre'] . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . $correo[0]['conductor_correo'] . "</td></tr>";
		$message .= "<tr><td><strong>Promocion:</strong> </td><td>" . $promocion[0]['promo_nombre'] . "</td></tr>";

		$message .= "<tr><td><strong>Taxi Hammer:</strong> </td><td><a href='www.taxihammer.com'>Taxi Hammer</a></td></tr>";
		$message .= "</table>";
		$message .= "</body></html>";

		$mail = new PHPMailer;
		//$mail->SMTPDebug = 2;                               // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = HOST;//'smtp.live.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = USERNAME;                 // SMTP username
		$mail->Password = PASSWORD;
		$mail->SMTPSecure = SMTPSECURE; 
		$mail->Port = PORT;  
		$mail->setFrom(FROM, NAME_EMPRESA);
		$mail->addAddress('shuanay@appslovers.com', 'Saul'); 
		$mail->addAddress($to, $Nombre);
		$mail->isHTML(true);  
		                                // Set email format to HTML
		$mail->Subject = 'Confirmacion de Canje';
		//$mail->Subject = 'Informaci&oacute;n de Servicio:';
		$mail->Body    = $message;
		$mail->AltBody = URL_CORREO;

		if(!$mail->send()) {
			$json['mensaje_email'] = "Message could not be sent.";
		} else {
			$json['mensaje_email'] = 'Message has been sent';
		    //echo 'Message has been sent';
		}

	$json['status'] = true;
	$json['mensaje'] = "Canje Correcto";


}else{

	$json['status'] = false;
	$json['mensaje'] = "Puntos Insuficientes";

}
echo json_encode($json);