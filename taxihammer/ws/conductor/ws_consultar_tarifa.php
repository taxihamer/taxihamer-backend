<?php
header('Content-Type: application/json');

$data['origen_id'] = $_REQUEST['origen_id'];
$data['destino_id'] = $_REQUEST['destino_id'];
$data['tiposervicio_id'] = $_REQUEST['tiposervicio_id'];
$data['cliente_id'] = $_REQUEST['empresa_id'];
$data['distancia'] = $_REQUEST['distancia'];
$data['tiempo'] = $_REQUEST['tiempo'];
$Activo = $_REQUEST['activo']; // 1 = aeropuesto / !1 = no aeropuerto
$Reserva = $_REQUEST['reserva']; // 1 = reserva / !1 = no reserva

require_once'../../DAL/tarifarioDAO.php';
require_once'../../DAL/tipoServicioDAO.php';
require_once'../../DAL/generalDAO.php';
require_once'../../DAL/tarifarioDAO.php';

$tipoServicioDAO = new tipoServicioDAO();
$tarifarioDAO = new tarifarioDAO();
$generalDAO = new generalDAO();
$tarifarioDAO = new tarifarioDAO();

$config = $generalDAO->listaBy(1);
$tipo = $tipoServicioDAO->listaTipoServiciosId($_REQUEST['tiposervicio_id']);

$RangoHoras = $tarifarioDAO->listRangoHorasWs();

$valor = 0;

if(!empty($RangoHoras)){

	$dia = $RangoHoras[0]['Dia'];

	switch ($dia) {
	    case "Lunes": $campo = 'rango_lun'; break;
	    case "Martes": $campo = 'rango_mar'; break;
	    case "Miercoles": $campo = 'rango_mie'; break;
	    case "Jueves": $campo = 'rango_jue'; break;
		case "Viernes": $campo = 'rango_vie'; break;
		case "Sabado": $campo = 'rango_sab'; break;
		case "Domingo": $campo = 'rango_dom'; break;
	}
	/*$array = json_decode($RangoHoras[0]['rango_check']);
	setlocale(LC_TIME,"es_ES");
	$HoraActual = strftime("%H:%M %p");
	// del día de la semana, desde 1 (lunes) hasta 7 (domingo)
	$Dia = $campo = '';
	$DiaActual = date("N");
	switch ($DiaActual) {
	    case 1: $Dia = "Lunes"; $campo = 'rango_lun'; break;
	    case 2: $Dia = "Martes"; $campo = 'rango_mar'; break;
	    case 3: $Dia = "Miercoles"; $campo = 'rango_mie'; break;
	    case 4: $Dia = "Jueves"; $campo = 'rango_jue'; break;
		case 5: $Dia = "Viernes"; $campo = 'rango_vie'; break;
		case 6: $Dia = "Sabado"; $campo = 'rango_sab'; break;
		case 7: $Dia = "Domingo"; $campo = 'rango_dom'; break;
	}

	$clave = '';
	if (in_array($DiaActual, $array)) {
		$clave = "Activo";
	}else{
		$clave = "Inactivo";
	};
	$valor = 0;
	if($clave == "Activo"){*/
		$Porcentaje = $tarifarioDAO->obtenerPorcentajeHora($campo);
		if(!empty($Porcentaje)){
			switch ($campo) {
				case 'rango_lun': $valor = $Porcentaje[0][$campo];break;
				case 'rango_mar': $valor = $Porcentaje[0][$campo];break;
				case 'rango_mie': $valor = $Porcentaje[0][$campo];break;
				case 'rango_jue': $valor = $Porcentaje[0][$campo];break;
				case 'rango_vie': $valor = $Porcentaje[0][$campo];break;
				case 'rango_sab': $valor = $Porcentaje[0][$campo];break;
				case 'rango_dom': $valor = $Porcentaje[0][$campo];break;
			}
		}else{
			$valor = 0;
		}
	//}
}

//echo '<pre>'; print_r($config); echo '</pre>'; exit;
if(!empty($config)){

	if($config[0]['tipo_tarifa'] != 1){//Por Kilometraje
		
		$Pbase = $config[0]['tarifa_minima'];
		$Tiempo = $_REQUEST['tiempo'];
		$CostoKilometro = $config[0]['factor_tarifa_km'];
		$Distancia = $_REQUEST['distancia'];
		$CostoTiempo = $config[0]['factor_tarifa_tiempo'];
		$TarifaMinima = $config[0]['tarifa_minima_m'];
		 
		$f1 = $Pbase+($Tiempo*$CostoTiempo)+($Distancia*$CostoKilometro);
		$fservicio = 1+(floatval($tipo[0]['tiposervicio_factor'])/100);
		$f1fservicio = floatval($f1)*floatval($fservicio);

		if($f1fservicio < $TarifaMinima){ $f1fservicio = $TarifaMinima;}

		$aeropuerto = 0;
		if($Activo == 1){
			if($_REQUEST['tiposervicio_id'] == 1){
				$aeropuerto	= floatval($config[0]['aeropuerto']);
			}else{
				$aeropuerto	= floatval($config[0]['aeropuerto']);
			}
		}

		$MontoReserva = 0;
		if($Reserva == 1){//Es una reserva
			if($_REQUEST['tiposervicio_id'] == 1){
				$MontoReserva = 5;
			}else{
				$MontoReserva = 10;
			}
		}


		$TarifaInicial = floatval($f1fservicio)+floatval($aeropuerto)+floatval($MontoReserva);
		$HorasPuntas = 1+(floatval($valor)/100);
		$TarifaFinal = floatval($TarifaInicial)*$HorasPuntas;

		$in = $TarifaFinal*2;
		$tarifa_new = ceil($in)/2;
		
		$json['status'] = true;
		$json['tarifa'] = floatval($tarifa_new);
		$json['incremento'] = (!empty($valor))?1:2;
		$json['Pbase'] = $Pbase;
		$json['CostoKilometro'] = $CostoKilometro;
		$json['CostoTiempo'] = $CostoTiempo;
		$json['TarifaMinima'] = $TarifaMinima;

	}else{//Por Zonas

		$Res = $tarifarioDAO->findTarifa($data);

		$contador = 0;
		$fth = floatval($valor)/100;
		$tarifatipo_servicio = floatval($tipo[0]['tiposervicio_factor'])/100;
		foreach($Res AS $res){
			$json['status'] = true;
			$json['tarifa'] = (1+floatval($tarifatipo_servicio))*floatval($res['tarifario_monto'])*(1+$fth);
			$json['incremento'] = (!empty($valor))?1:2;
			$contador = 1;
		}
		if($contador == 0){
			$json['status'] = false;
			$json['tarifa'] = 0;
		}
	}
}

echo json_encode($json);