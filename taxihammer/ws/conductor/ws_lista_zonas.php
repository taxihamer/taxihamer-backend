<?php
header('Content-Type: application/json');
$data['lugar_mod'] = $_REQUEST['mod'];
$data['lugar_tot'] = $_REQUEST['tot'];
require_once'../../DAL/lugaresDAO.php';
if(isset($_REQUEST['mod']) || isset($_REQUEST['mod'])){

		$lugaresDAO = new lugaresDAO();

		$limit1 = $data['lugar_tot'];

     	$limit2 = $lugaresDAO->cantidadTotalDeLugares();

     	$data['lugar_tot1'] = $limit2[0]['cantidad'];
		$Res = $lugaresDAO->listaWS($data);
		foreach ($Res as $key => $row) {
			$res[$key]['id'] = $row['lugares_id'];
			$res[$key]['nombre'] = $row['lugar_descripcion'];
			$res[$key]['centro'] = array("latitud"=>floatval($row['lugar_lat']), "longitud"=>floatval($row['lugar_lon']));
			$res[$key]['mod'] = $row['lugar_mod'];
			$res[$key]['estado'] = $row['lugar_estado'];

			$res[$key]['lugar_coordpolygono'] = json_decode($row['lugar_coordpolygono']);
			$a = str_replace("(", "", json_decode($row['lugar_coordpolygono']));
			$c = str_replace(")", "", $a);

			$res[$key]['lugar_coordpolygono'] = $c;
			unset($res[$key]['lugar_coordpolygono']);
			foreach ($c as $k => $exp) {
				$explode = explode(',', $exp);
				$res[$key]['coordenadas'][] = $explode;
			}

			$i = 0;
			foreach ($res[$key]['coordenadas'] as $ky => $indice) {
					$res[$key]['coordenadas'][$i]['latitud'] = floatval($res[$key]['coordenadas'][$i][0]);
					$res[$key]['coordenadas'][$i]['longitud'] = floatval($res[$key]['coordenadas'][$i][1]);
					unset($res[$key]['coordenadas'][$i][0]);
					unset($res[$key]['coordenadas'][$i][1]);
				$i++;
			}

		}

		header('Content-Type: application/json');
	    
	$array = [];
	if(empty($res)){
		$res = $array;
	}
		
	
}else{
	$res['success'] = false;
	$res['mensaje'] = "Error, tot y mod requeridos";
}

echo json_encode($res);