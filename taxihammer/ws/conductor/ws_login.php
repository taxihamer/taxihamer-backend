<?php
header('Content-Type: application/json');
$user = $_REQUEST['correo'];
$pass = $_REQUEST['password'];

require_once'../../DAL/conductorDAO.php';
require_once'../../DAL/generalDAO.php';
$conductorDAO = new conductorDAO();
$generalDAO = new generalDAO();

$Res = $conductorDAO->listaBylogin($user,$pass);
$listkey = $generalDAO->listKeyMoviles();
$json['status'] = false;
$json['mensaje'] = "Usuario o Password incorrectos";
$contador = 0;
//echo '<pre>'; print_r($Res); echo '</pre>'; exit;
/*foreach($Res AS $res){
	if($res['conductor_correo'] == $user && $res['conductor_password'] == $pass && $res['conductor_status'] == 1){
		
		$json['nombre'] = $res['conductor_nombre'];
		$json['apellido'] = $res['conductor_apellido'];
		$json['calificacion'] = $res['conductor_calificacion'];
		list($ano, $mes, $dia) = split('[/.-]',$res['conductor_fechabrevete']);
		$json['fecha_brevete'] = $dia.'/'.$mes.'/'.$ano;
		$json['conductor_id'] = $res['conductor_id'];
		$json['unidad_id'] = $res['unidad_id'];
		$json['celular'] = $res['conductor_celular'];
		$json['tipo_id'] = $res['tiposervicio_id'];
		$json['tipo_nombre'] = $res['tiposervicio_nombre'];
		$json['puntos'] = $res['conductor_puntos'];
		$json['status'] = true;
		$json['conductor_status'] = $res['conductor_status'];
		$json['mensaje'] = "Inicio de sesión exitoso";
		
	}
	if($res['conductor_correo'] == $user && $res['conductor_password'] == $pass && $res['conductor_status'] != 1){
		$json['status'] = false;
		$json['mensaje'] = "Usuario Inhabilitado, comuníquese con la central";
	}
}*/

if(!empty($Res)){

		$json['nombre'] = $Res[0]['conductor_nombre'];
		$json['apellido'] = $Res[0]['conductor_apellido'];
		$json['calificacion'] = $Res[0]['conductor_calificacion'];
		/* FORMATEANDO FECHA */
		//list($ano, $mes, $dia) = split('[/.-]',$Res[0]['conductor_fechabrevete']);
		$json['fecha_brevete'] = $Res[0]['conductor_fechabrevete'];
		/* END FORMATEANDO FECHA */
		$json['conductor_id'] = $Res[0]['conductor_id'];
		$json['unidad_id'] = $Res[0]['unidad_id'];
		$json['celular'] = $Res[0]['conductor_celular'];
		$json['tipo_id'] = $Res[0]['tiposervicio_id'];
		$json['tipo_nombre'] = $Res[0]['tiposervicio_nombre'];
		$json['puntos'] = $Res[0]['conductor_puntos'];
		$json['status'] = true;
		$json['conductor_status'] = $Res[0]['conductor_status'];

		$json['foursquare_key'] = $listkey[0]['api_foursquare_key'];
		$json['foursquare_secret'] = $listkey[0]['api_foursquare_secret'];
		$json['here_key'] = $listkey[0]['api_here_key'];
		$json['here_secret'] = $listkey[0]['api_here_secret'];
		$json['google_ios'] = $listkey[0]['api_google_ios'];
		$json['google_android'] = $listkey[0]['api_google_android'];

		$json['mensaje'] = "Inicio de sesión exitoso";

		if($Res[0]['conductor_status'] == 0){
			$contador = $contador +1;
		}
}

if($contador != 0){
	unset($json);
	$json['status'] = false;
	$json['mensaje'] = "Conductor aún no activado";
}

echo json_encode($json);