<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

header('Content-Type: application/json');

$data['conductor_id'] = $_REQUEST['conductor_id'];
$data['recarga_monto'] = $_REQUEST['recarga_monto'];
$data['recarga_num_oper'] = $_REQUEST['recarga_num_oper'];
$data['recarga_fecha'] = $_REQUEST['recarga_fecha'];
$data['recarga_tipo'] = $_REQUEST['recarga_tipo'];
$data['recarga_send'] = 2;
$data['recarga_check'] = 2;
 
require_once'../../DAL/recargasDAO.php';
require_once'../../DAL/conductorDAO.php';
require_once'../../DAL/operacionesDAO.php';
require_once'../../DAL/constantes.php';

$recargasDAO = new recargasDAO();
$rest = $recargasDAO->insertRecarga($data);

if($rest[0]['p_return_code'] != '-1' || $rest[0]['p_return_code'] != '-2'){

		$operacionesDAO = new operacionesDAO();
		$conductorDAO = new conductorDAO();

		$send['recarga_id'] = $rest[0]['p_return_code'];
		$send['cta_tipo'] = 1;
		$send['cta_fecha'] = $_REQUEST['recarga_fecha'];
		$send['cta_monto'] = $_REQUEST['recarga_monto'];
		$send['conductor_id'] = $_REQUEST['conductor_id'];
		$send['cta_check'] = 2;

		$operacion = $operacionesDAO->insertCta($send);
		$res = $operacion;
	
}else{
	$res = false;
	$valid = 1;
}


if($res == true){

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, IP_SOCKET.'w_recarga_panel?recarga_id='.$rest[0]['p_return_code'].'&conductor_id='.$_REQUEST['conductor_id']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$output = curl_exec($ch);
	curl_close($ch);
	
	$json['status'] = true;
	$json['mensaje'] = "Recarga guardada con éxito";
	
}else{
	if($valid == 1){
		$json['mensaje'] = "Ups, hubo un error";
	}else{
		$json['mensaje'] = "Ups, ya existe este numero de Operacion";
	}
	$json['status'] = false;
}
echo json_encode($json);