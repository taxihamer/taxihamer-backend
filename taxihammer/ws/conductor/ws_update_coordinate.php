<?php
header('Content-Type: application/json');
$data['conductor_id'] = $_REQUEST['id'];
$data['conductor_lat'] = $_REQUEST['latitud'];
$data['conductor_lng'] = $_REQUEST['longitud'];

require_once'../../DAL/conductorDAO.php';
$conductorDAO = new conductorDAO();
$Res = $conductorDAO->updateCoordinates($data);

if($Res == true){
	$json['status'] = true;
	$json['mensaje'] = 'Coordenadas actualizadas con éxito';
	$json['latitud'] = $_REQUEST['latitud'];
	$json['longitud'] = $_REQUEST['longitud'];
}else{
	$json['status'] = false;
	$json['mensaje'] = 'Error en actualizar coordenadas';
}
echo json_encode($json);