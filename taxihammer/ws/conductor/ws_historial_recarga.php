<?php
header('Content-Type: application/json');
require_once'../../DAL/recargasDAO.php';
require_once'../../DAL/conductorDAO.php';

$conductor_id = $_REQUEST['conductor_id'];
$inicio = $_REQUEST['inicio'];
$limite = $_REQUEST['limite'];

$recargasDAO = new recargasDAO();
$conductorDAO = new conductorDAO();

$Res = $recargasDAO->listaByConductor($conductor_id, $inicio, $limite);
$saldo = $conductorDAO->listaConductorId($conductor_id);

$sendSaldo = $saldo[0]['conductor_recarga'];

$contador = 0;
$cadena = '';
foreach($Res AS $res){
	$banco_descripcion = (!empty($res['banco_descripcion']))?$res['banco_descripcion']:'Oficina Central';
	$recarga_check = ($res['recarga_check'] == 1)?'APROBADO':(($res['recarga_check'] == 2)?'PENDIENTE':'RECHAZADO');

	$cadena = $cadena.'{"recarga_id": '.$res["recarga_id"].', "recarga_monto": '.$res["recarga_monto"].
						', "recarga_num_oper": '.$res["recarga_num_oper"].', "fecha": "'.$res["fecha"].'", "banco_descripcion": "'.$banco_descripcion.'", "recarga_tipo": "'.$recarga_check.'"},';
	$contador++;
}

$json = '{"status": true, "saldo" : '.$sendSaldo.' , "historial":['.substr($cadena,0,-1).']}'; 

if($contador == 0){
	$json = '{"status": false, "mensaje": "No se encontraron registros" }';
}

echo $json;