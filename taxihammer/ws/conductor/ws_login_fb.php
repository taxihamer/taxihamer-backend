<?php
header('Content-Type: application/json');
$correo = $_REQUEST['correo'];
$fbid = $_REQUEST['fbid'];
$nombre = $_REQUEST['nombre'];
$apellido = $_REQUEST['apellido'];

require_once'../../DAL/conductorDAO.php';
$contador = 0;
/*-----BUSCANDO SI EXISTE FB ID----*/
$conductorDAO = new conductorDAO();
$Res = $conductorDAO->listaSimple();

foreach($Res AS $res){
	if($res['fb_id'] == $fbid){
		$contador = 1;
		$json['status'] = "success";
		$json['mensaje'] = "Inicio de sesión exitoso";
		$json['nombre'] = $nombre;
		$json['apellido'] = $apellido;
		$json['correo'] = $correo;
	}
}
/*-----INSERTANDO CONDUCTOR CON STATUS 0----*/
if($contador == 0){
	$data['conductor_nombre'] = $nombre;
	$data['conductor_apellido'] = $apellido;
	$data['conductor_correo'] = $correo;
	$data['fb_id'] = $fbid;
	$result = $conductorDAO->insertarFb($data);
	if($result == true){
		$json['status'] = "success"; 
		$json['mensaje'] = "Usuario registrado con éxito";
	}else{
		$json['status'] = "failed"; 
		$json['mensaje'] = "Hubo un error registrando conductor";
	}
}
echo json_encode($json);