<?php
header('Content-Type: application/json');
$data['cliente_id'] = $_REQUEST['cliente_id'];
$data['reserva_fecha'] = $_REQUEST['reserva_fecha'];
$data['origen_id'] = $_REQUEST['origen_id'];
$data['lat_origen'] = $_REQUEST['lat_origen'];
$data['lon_origen'] = $_REQUEST['lon_origen'];
$data['dir_origen'] = $_REQUEST['dir_origen'];
$data['destino_id'] = $_REQUEST['destino_id'];
$data['lat_destino'] = $_REQUEST['lat_destino'];
$data['lon_destino'] = $_REQUEST['lon_destino'];
$data['dir_destino'] = $_REQUEST['dir_destino'];
$data['tiposervicio_id'] = $_REQUEST['tiposervicio_id'];
$data['tipo_pago'] = $_REQUEST['tipo_pago'];
$data['tarifa'] = $_REQUEST['tarifa'];
$data['estado'] = $_REQUEST['estado'];

require_once'../../DAL/reservaDAO.php';
$reservaDAO = new reservaDAO();
$res = $reservaDAO->insertar($data);

if($res['status'] == true){
	$json['status'] = true;
	$json['mensaje'] = "Reserva guardada con éxito";
}else{
	$json['status'] = false;
	$json['mensaje'] = "Ups, hubo un error";
}
echo json_encode($json);