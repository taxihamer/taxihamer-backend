<?php
header('Content-Type: application/json');

error_reporting(E_ALL);
ini_set('display_errors', 1);

$conductor_id = $_REQUEST['conductor_id'];

require_once'../../DAL/promocionDAO.php';
$promocionDAO = new promocionDAO();
$Res = $promocionDAO->obtenerPuntosConductor($conductor_id);
$contador = 0;
$puntos = 0;
// echo '<pre>'; print_r($Res); echo '</pre>';

foreach($Res AS $res){
	$puntos = $res["conductor_puntos"];
	$contador++;
}

$json = '{"status": true, "puntos":'.$puntos.'}'; 

if($contador == 0){
	$json = '{"status": false, "mensaje": "No se encontraron registros" }';
}

echo $json;