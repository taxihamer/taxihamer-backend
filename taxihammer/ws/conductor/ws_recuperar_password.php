<?php
header('Content-Type: application/json');

$cor = $_REQUEST['correo'];
//require_once'../../lib/PHPMailer2/PHPMailerAutoload.php';
require_once'../../lib/PHPMailer2/class.smtp.php';
include_once'../../lib/PHPMailer2/class.phpmailer.php';
require_once'../../DAL/conductorDAO.php';
require_once'../../DAL/constantes.php';
$conductorDAO = new conductorDAO();
//$Res = $conductorDAO->listaSimple();
$Res = $conductorDAO->validarCorreo($cor);
$contador = 0;

if($Res[0]['total'] >= 1){
    /*-------------------------INICIO CORREO-----------------------------*/
    $to = $cor;
    $message = '<html><body>';
    $message .= '<img src="cid:logo" alt="Taxi Hammer" />';
    $message .= '<p>Correo de Recuperación de password, a continuacion sus credenciales de la Empresa </p>';
    $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
    $message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . $Res[0]['conductor_nombre'] . "</td></tr>";
    $message .= "<tr><td><strong>Email:</strong> </td><td>" . $cor . "</td></tr>";
    $message .= "<tr><td><strong>Password:</strong> </td><td>" . $Res[0]['conductor_password'] . "</td></tr>";


    $message .= "<tr><td><strong>Taxi Hammer:</strong> </td><td><a href='http://taxihammer.com'>Taxi Hammer</a></td></tr>";
    $message .= "</table>";
    $message .= "</body></html>";

    $mail = new PHPMailer();
    //$mail->SMTPDebug = 2;                               // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = HOST;//'smtp.live.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = USERNAME;                 // SMTP username
    $mail->Password = PASSWORD;
    $mail->SMTPSecure = SMTPSECURE;
    $mail->Port = PORT;
    $mail->setFrom(FROM, NAME_EMPRESA);
    //$mail->addAddress('shuanay@appslovers.com', 'Saul');
    $mail->addAddress($to);
    $mail->isHTML(true);

    $mail->isHTML(true);
    // Set email format to HTML
    $mail->Subject = utf8_decode('Recuperar Contraseña');
    $mail->AddEmbeddedImage("../../includes/img/logo_hammer.png", "logo", "../../includes/img/logo_hammer.png");
    $mail->Body    = $message;
    $mail->AltBody = URL_CORREO;


    if(!$mail->send()) {
        echo json_encode($mail->ErrorInfo);
    }else {
        echo '{"status":true, "mensaje":"Se envio la constraseña a su correo"}';
    }

}
else{
    echo '{"status":false, "mensaje":"No existe el correo indicado en nuestra base de datos"}';
}
/*
if($contador == 0){
	echo '{"status":false, "mensaje":"No existe el correo indicado en nuestra base de datos"}';
}
*/
