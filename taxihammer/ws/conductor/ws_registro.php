<?php
header('Content-Type: application/json');

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once'../../DAL/conductorDAO.php';
require_once'../../DAL/constantes.php';

$data['conductor_nombre'] = DelCharacter($_REQUEST['nombre']);
$data['conductor_apellido'] = DelCharacter($_REQUEST['apellido']);
$data['conductor_correo'] = $_REQUEST['correo'];
$data['conductor_password'] = $_REQUEST['password'];
//$data['conductor_dni'] = $_REQUEST['dni'];
$data['conductor_celular'] = $_REQUEST['celular'];
//$data['conductor_ciudad'] = $_REQUEST['ciudad'];
//$data['conductor_distrito'] = $_REQUEST['distrito'];
//$data['conductor_licencia'] = $_REQUEST['licencia'];
//$data['conductor_catlicencia'] = (!empty($_REQUEST['catlicencia']))?$_REQUEST['catlicencia']:'';
//$data['conductor_fechabrevete'] = (!empty($_REQUEST['fechabrevete']))?$_REQUEST['fechabrevete']:'';
$data['conductor_status'] = 0;

$conductorDAO = new conductorDAO();
$Result = $conductorDAO->listaSimple();
$validar = $conductorDAO->validarCorreoCelular($_REQUEST['correo'],(int)$_REQUEST['celular']);

$contador = 0;
if($validar[0]['total'] > 0 ){
	$json['success'] = false;
	$json['mensaje'] = "Correo ingresado ya ha sido registrado";
	$contador = 1;
}else{
	$res = $conductorDAO->insertarConductorWs($data);
	if($res == true){
		$json['success'] = true;
		$json['mensaje'] = "Conductor guardado con éxito, para completar el proceso de registro, acérquese a la central.";
	}else{
		$json['success'] = false;
		$json['mensaje'] = "Error al registrarse, inténtelo nuevamente";
	}
}

/*foreach($Result AS $result){
	if($result['conductor_correo'] == $data['conductor_correo']){
		$json['success'] = false;
		$json['mensaje'] = "Correo ingresado ya ha sido registrado";
		$json['conductor_id'] = $result['conductor_id'];
		$contador = 1;
	}
}*/

echo json_encode($json);
