<?php
header('Content-Type: application/json');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '../../DAL/clientesDAO.php';
require_once '../../DAL/constantes.php';

$idCliente = $_REQUEST['cliente_id'];

$clientesDAO = new clientesDAO();
$dataCliente = $clientesDAO->listaClienteId($idCliente);
$url = 'https://fcm.googleapis.com/fcm/send';

if(!empty($dataCliente)){	

		$json = [ 'data' =>
				[
				'nombreCliente' => $dataCliente[0]['cliente_nombre'].$dataCliente[0]['cliente_apellido'],
				'correoCliente' => $dataCliente[0]['cliente_correo'],
				'cliente_id'	=> $dataCliente[0]['cliente_id']
				],
				'to'=> $dataCliente[0]['cliente_token_android']
			];

		$json = json_encode($json);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,array('Content-Type: application/json','Authorization : key= '.KEY));
		curl_setopt($curl,CURLOPT_POSTFIELDS, $json);
		$result = curl_exec($curl);
		curl_close($curl);

		$jsonT['status'] = true;
		$jsonT['mensaje'] = 1;

}else{

	$jsonT['status'] = false;
	$jsonT['mensaje'] = "Ups, hubo un error ";
}

echo json_encode($jsonT);