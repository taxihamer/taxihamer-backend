<?php
header('Content-Type: application/json');

require_once'../../DAL/recargasDAO.php';

$recargasDAO = new recargasDAO();
$Rest = $recargasDAO->listaEntidad();

$contador = 0;
$cadena = '';
foreach($Rest AS $row){
	$cadena = $cadena.'{"banco_id": '.$row["banco_id"].',
						"banco_descripcion": "'.$row["banco_descripcion"].'"},';
	$contador++;
}
$json = '{"status": true, "result":['.substr($cadena,0,-1).']}'; 

if($contador == 0){
	$json = '{"status": false, "mensaje": "No existen servicios para mostrar" }';
}

echo $json;