<?php
header('Content-Type: application/json');

$data['conductor_id'] = $_REQUEST['conductor_id'];
$data['cta_fechaInicio'] = $_REQUEST['fecha_inicio'];
$data['cta_fechaFinal'] = $_REQUEST['fecha_final'];
$data['inicio'] = $_REQUEST['inicio'];
$data['limite'] = $_REQUEST['limite'];

require_once'../../DAL/operacionesDAO.php';
require_once'../../DAL/servicioDAO.php';

$servicioDAO = new servicioDAO();
$operacionesDAO = new operacionesDAO();
$Res = $operacionesDAO->listarCta($data);

$contador = 0;
$cadena = '';
foreach($Res AS $res){

	if($res['cta_tipo'] != 1){
		$serv = $servicioDAO->listaServicio($res['servicio_id']);
		if(!empty($serv)){

			if($res["cta_monto"] < 0){
				if($serv[0]['tipo_pago'] == 1){
					$res["cta_monto"] = $res["cta_monto"] * -1;
				}else{
					$res["cta_monto"] = $res["cta_monto"] * -1;
				}
			}	

			$signo = ($serv[0]['tipo_pago'] == 1)?'-':'+';
		}
		$signo = '';
	}else{

		if($res["cta_monto"] < 0){
			if($serv[0]['tipo_pago'] == 1){
				$res["cta_monto"] = $res["cta_monto"] * -1;
			}else{
				$res["cta_monto"] = $res["cta_monto"] * -1;
			}
		}

		$signo = '+';
	}

	$TipoMovimiento = ($res['cta_tipo'] != 1)?'Servicio':'Recarga';
	$mm = $signo.number_format($res["cta_monto"], 2);

	$cadena = $cadena.'{"cta_id": '.$res["cta_id"].', "cta_monto": '.floatval($mm).
						',"cta_fecha": "'.$res['cta_fecha'].'", "cta_tipo":"'.$TipoMovimiento.'"},';
	$contador++;
}

$json = '{"status": true, "result":['.substr($cadena,0,-1).']}'; 

if($contador == 0){
	$json = '{"status": false, "mensaje": "No se encontraron registros de movimientos" }';
}

echo $json;