<?php
header('Content-Type: application/json');
$data['cliente_id'] = $_REQUEST['cliente_id'];

require_once'../../DAL/promocionDAO.php';
$promocionDAO = new promocionDAO();
$Res = $promocionDAO->obtenerPuntosCliente($_REQUEST['cliente_id']);
$contador = 0;
$puntos = 0;


foreach($Res AS $res){
	$puntos = $res["cliente_puntos"];
	$contador++;
}

$json = '{"status": true, "puntos":'.$puntos.'}'; 

if($contador == 0){
	$json = '{"status": false, "mensaje": "No se encontraron registros" }';
}

echo $json;