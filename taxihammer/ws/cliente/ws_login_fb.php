<?php
header('Content-Type: application/json');
$correo = $_REQUEST['correo'];
$fbid = $_REQUEST['fbid'];
$nombre = $_REQUEST['nombre'];
$apellido = $_REQUEST['apellido'];

require_once'../../DAL/clientesDAO.php';
$contador = 0;
/*-----BUSCANDO SI EXISTE FB ID----*/
$clienteDAO = new clientesDAO();
$Res = $clienteDAO->listaSimple();

foreach($Res AS $res){
	if($res['fb_id'] == $fbid){
		$contador = 1;
		$json['status'] = true;
		$json['mensaje'] = "Inicio de sesión exitoso";
		$json['nombre'] = $nombre;
		$json['apellido'] = $apellido;
		$json['correo'] = $correo;
	}
}
/*-----INSERTANDO CLIENTE CON STATUS 1----*/
if($contador == 0){
	$data['cliente_nombre'] = $nombre;
	$data['cliente_apellido'] = $apellido;
	$data['cliente_correo'] = $correo;
	$data['fb_id'] = $fbid;
	$result = $clienteDAO->insertarFb($data);
	if($result == true){
		$json['status'] = true; 
		$json['mensaje'] = "Usuario registrado con éxito";
	}else{
		$json['status'] = false; 
		$json['mensaje'] = "Hubo un error registrando Cliente";
	}
}
echo json_encode($json);