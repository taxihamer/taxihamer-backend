<?php
header('Content-Type: application/json');
$cor = $_REQUEST['correo'];

require_once'../../lib/PHPMailer2/PHPMailerAutoload.php';
require_once'../../DAL/clientesDAO.php';
require_once'../../DAL/constantes.php';
$clienteDAO = new clientesDAO();
//$Res = $clienteDAO->listaSimple();
$Res = $clienteDAO->validarCorreo($cor);
$contador = 0;
if($Res[0]['total'] >= 1){
		/*-------------------------INICIO CORREO-----------------------------*/
		$to = $cor;
		$message = '<html><body>';
		$message .= '<img src="cid:logo" alt="Taxi Hammer" />';
		$message .= '<p>Correo de Recuperaci&oacute;n de password, a continuacion sus credenciales de Taxi Hammer </p>';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Nombre:</strong> </td><td>" . $Res[0]['cliente_nombre'] . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . $cor . "</td></tr>";
		$message .= "<tr><td><strong>Password:</strong> </td><td>" . $Res[0]['cliente_password'] . "</td></tr>";


		$message .= "<tr><td><strong>Taxi Hammer:</strong> </td><td><a href='http://taxihammer.com'>Taxi Hammer</a></td></tr>";
		$message .= "</table>"; 
		$message .= "</body></html>";
		/*-----------------------------END----------------------------*/
		$mail = new PHPMailer;
		//$mail->SMTPDebug = 2;                               // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = HOST;//'smtp.live.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = USERNAME;                 // SMTP username
		$mail->Password = PASSWORD;
		$mail->SMTPSecure = SMTPSECURE; 
		$mail->Port = PORT;  
		$mail->setFrom(FROM, NAME_EMPRESA);
		//$mail->addAddress('shuanay@appslovers.com', 'Saul'); 
		$mail->addAddress($to);
		$mail->isHTML(true); 
			// Set email format to HTML
			$mail->Subject = utf8_decode('Recuperar Contraseña');
			$mail->AddEmbeddedImage("../../includes/img/logo_hammer.png", "logo", "../../includes/img/logo_hammer.png");
			$mail->Body    = $message;
			$mail->AltBody = URL_CORREO;


			if(!$mail->send()) {
			   echo 'Message could not be sent.';
			   echo 'Mailer Error: ' . $mail->ErrorInfo;
			}
		$contador = 1;
}
if($contador == 0){
	echo '{"status":false, "mensaje":"No existe el correo indicado en nuestra base de datos"}';
}else{
	echo '{"status":true, "mensaje":"Se ha enviado la contraseña a su correo"}';
}