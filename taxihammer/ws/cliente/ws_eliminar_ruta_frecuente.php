<?php
header('Content-Type: application/json');
$data['rutas_id'] = $_REQUEST['id'];

require_once'../../DAL/rutasFrecuentesDAO.php';
$rutasFrecuentesDAO = new rutasFrecuentesDAO();
$res = $rutasFrecuentesDAO->Eliminar($data);

if($res == true){
	$json['status'] = true;
	$json['mensaje'] = "Ruta Frecuente eliminada con éxito";
}else{
	$json['status'] = false;
	$json['mensaje'] = "Error al eliminar ruta frecuente";
}
echo json_encode($json);