<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
header('Content-Type: application/json');
//MOSTRAR RUTAS FRECUENTES
if(!isset($_REQUEST['cliente_id'])){
	$data['cliente_id'] = "babyrage";

}else{
	$data['cliente_id'] = $_REQUEST['cliente_id'];
}

require_once'../../DAL/rutasFrecuentesDAO.php';

//Agregado por Danny
require_once'../../DAL/generalDAO.php';
$generalDAO = new generalDAO();
$config = $generalDAO->listaBy(1);
$data['tipo_tarifa'] = $config[0]['tipo_tarifa'];
//Agregado por Danny
$rutasFrecuentesDAO = new rutasFrecuentesDAO();
$Res = $rutasFrecuentesDAO->lista($data);
$contador = 0;
$cadena = '';

function utf8array(&$value,$key){

	$a = ['Ã¡','Ã©','Ã*','Ã³','Ãº','Ã','Ã‰','Ã','Ã“','Ãš','Ã±','Ã§','Ã‘','Ã‡','Â©','Â®','â„¢','Ã˜','Âª','Ã¤','Ã«','Ã¯','Ã¶','Ã¼','Ã„','Ã‹','Ã ','Ã–','Ãœ','Ã­','ÃÂº'];
	$b = ['á','é','í','ó','ú','Á','É','Í','Ó','Ú','ñ','ç','Ñ','Ç','©','®','™','Ø','ª','ä','ë','ï','ö','ü','Ä','Ë','Ï','Ö','Ü','í','ú'];

     $value = (utf8_decode($value));
}

foreach($Res AS $res){
	
	array_walk($res, 'utf8array');
	
	$cadena = $cadena.'{"cliente_id": '.$res["cliente_id"].', "ruta_nombre": "'.$res["rutas_nombre"].
						'", "latitud_origen": '.$res["origen_latitud"].', "nombre_origen": "'.$res["nombre_origen"].'", "nombre_destino": "'.$res["nombre_destino"].'", "longitud_origen": '.$res["origen_longitud"].
						', "latitud_destino": '.$res["destino_latitud"].', "longitud_destino": '.$res["destino_longitud"].
						', "id_origen": '.$res["id_origen"].', "id_destino": '.$res["id_destino"].
						', "tiposervicio": '.$res["tiposervicio_id"].', "origen": "'.$res["direccion_origen"].
						'", "destino": "'.$res["direccion_destino"].'", "ref_origen": "'.$res["referencia_origen"].
						'", "ref_destino": "'.$res["referencia_destino"].'", "tarifa": '.floatval($res["tarifa"]).', "rutasid":'.$res["rutas_id"].'},';
	$contador++;
}
$json = '{"status": true, "result":['.substr($cadena,0,-1).']}'; 

if($contador == 0){
	$json = '{"status": false, "mensaje": "No existen rutas frecuentes para este usuario" }';
}

echo $json;