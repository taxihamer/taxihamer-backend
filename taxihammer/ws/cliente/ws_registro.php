<?php
header('Content-Type: application/json');

require_once'../../DAL/empresaDAO.php';
require_once'../../DAL/constantes.php';
require_once'../../DAL/clientesDAO.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$tipo = $_REQUEST['tipo'];
$token = $_REQUEST['token'];

$data['cliente_nombre'] = DelCharacter($_REQUEST['nombre']);
$data['cliente_apellido'] = DelCharacter($_REQUEST['apellido']);
$data['cliente_correo'] = $_REQUEST['correo'];
$data['cliente_celular'] = $_REQUEST['celular'];
$data['cliente_password'] = $_REQUEST['password'];
$data['cliente_status'] = 1;
$data['cliente_estados'] = 1;
$data['cliente_calificacion'] = 5;
$data['cliente_calificacion'] = 5;
$data['cliente_prueba'] = 0;
$json['success'] = true;
$json['tipo'] = $tipo;
$json['token'] = $token;
$data['cliente_costos'] = 0;
$data['cliente_supervisor'] = 0;

/*---------------VERIFICANDO SI LA EMPRESA EXISTE--------------*/
if(isset($_REQUEST['ruc'])){
	$ruc = $_REQUEST['ruc'];
	$data['cliente_estados'] = 0;

	$empresaDAO = new empresaDAO();
	$Resultado = $empresaDAO->listempresa();
	$i = 0;
	foreach($Resultado AS $resultado){
		if($ruc == $resultado['empresa_ruc']){
			$data['empresa_id'] = $resultado['empresa_id'];
			$i++;
		}
	}
	if($i == 0){
		$json['success'] = false;
		$json['mensaje'] = "Empresa no encontrada.";
	}
	$data['tipocliente_id'] = 2;

}else{
	$data['empresa_id'] = 0;
	$data['tipocliente_id'] = 1;
}

/*---------------VERIFICANDO DUPLICIDAD DE CORREO------------------*/
if($json['success'] != false){

	$clienteDAO = new clientesDAO();
	$Result = $clienteDAO->listaSimple();
	$validar = $clienteDAO->validarCorreoCelular($_REQUEST['correo'],(int)$_REQUEST['celular']);
	$contador = 0;
	
	if($validar[0]['total'] > 0){
		$json['success'] = false;
		$json['mensaje'] = "Correo ingresado ya ha sido registrado";
		//$json['cliente_id'] = $result['cliente_id'];
		$contador = 1;
	}	
	
	/*---------------REGISTRANDO CLIENTE------------------*/
    /*El contador es para saber si ocurrió algún otro error antes*/

    //TODO OLD LOGIN salia error porque no encontraba el p_return_code, ya que no devolvia eso en insertar cliente
    /*if($contador == 0){
		$res = $clienteDAO->insertarCliente($data);

		if($res[0]['p_return_code'] != '-1' || $res[0]['p_return_code'] != '-2' ){
			$validtoken = $clienteDAO->validarClienteToken($token,$tipo);
			if($validtoken[0]['TOTAL'] != 0){
				$tokencliente = $clienteDAO->updateClienteToken($tipo,$token,1,$res[0]['p_return_code']);
			}
			$tokencliente = $clienteDAO->updateClienteToken($tipo,$token,2,$res[0]['p_return_code']);

			$json['success'] = true;
			$json['mensaje'] = "Se ha registrado correctamente y podrá usar el aplicativo cuando su usuario sea verificado.";
			$json['nombre'] = $_REQUEST['nombre'];
			$json['apellido'] = $_REQUEST['apellido'];
			$json['correo'] = $_REQUEST['correo'];
			$json['celular'] = $_REQUEST['celular'];
			$json['cliente_id'] = $res[0]['p_return_code'];
			$json['calificacion'] = 5;
			$json['tipo'] = $tipo;
			$json['token'] = $token;
		}else{
			$json['success'] = false;
			$json['mensaje'] = "Error al registrarse, inténtelo nuevamente.";
		}
		
	}*/

    if($contador == 0){
        $res = $clienteDAO->insertarCliente($data);

        if($res['status'] == 1 ){
            $validtoken = $clienteDAO->validarClienteToken($token,$tipo);
            if($validtoken[0]['TOTAL'] != 0){
                $tokencliente = $clienteDAO->updateClienteToken($tipo,$token,1,$res['lastid']);
            }
            $tokencliente = $clienteDAO->updateClienteToken($tipo,$token,2,$res['lastid']);

            $json['success'] = true;
            $json['mensaje'] = "Se ha registrado correctamente ya puede ingresar y solicitar un taxi.";
            $json['nombre'] = $_REQUEST['nombre'];
            $json['apellido'] = $_REQUEST['apellido'];
            $json['correo'] = $_REQUEST['correo'];
            $json['celular'] = $_REQUEST['celular'];
            $json['cliente_id'] = $res['lastid'];
            $json['calificacion'] = 5;
            $json['tipo'] = $tipo;
            $json['token'] = $token;
        }else{
            $json['success'] = false;
            $json['mensaje'] = "Error al registrarse, inténtelo nuevamente.";
        }

    }

}
echo json_encode($json);