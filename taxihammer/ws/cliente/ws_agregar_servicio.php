<?php 
require_once'../../DAL/servicioDAO.php';
require_once'../../DAL/constantes.php';

$data = $_REQUEST['data'];
    
    
	$send['conductor_id'] = (isset($data['conductor_id']))?$data['conductor_id']:NULL;
	$send['cliente_id'] = $data['idCliente'];
    $send['tiposervicio_id'] = $data['TipoServicion']; 
    $send['origen_id'] = $data['Origen'];
    $send['destino_id'] = $data['Destino']; 
    $send['origen_nombre'] = $OrigeNombre = DelCharacter($data['OrigeNombre']); 
    $send['destino_nombre'] = $DestiNombre = DelCharacter($data['DestiNombre']); 
    $send['origen_direccion'] = $OrigeDirecc = DelCharacter($data['OrigeDirecc']);  
    $send['destino_direccion'] = $DestiDirecc = DelCharacter($data['DestiDirecc']); 
    $send['origen_referencia'] = $OrigeRefere = DelCharacter($data['OrigeRefere']);             
    $send['destino_referencia'] = $DestiRefere = DelCharacter($data['DestiRefere']);            
    $send['origen_lat'] = $data['OrigenLat']; 
    $send['origen_lon'] = $data['OrigenLng'];
    $send['destino_lat'] = $data['DestinoLat']; 
    $send['destino_lon'] = $data['DestinoLng'];
    $send['tarifa'] = $data['Tarifa']; 
    $send['tipo_pago'] = $data['TipoPago']; 
    $send['estado'] = $data['Estado']; 
    $send['fecha_seleccion'] = (isset($data['fecha_seleccion']))?1:2;
    $send['hora_inicio'] = (isset($data['hora_inicio']))?1:2;
    $send['flag_reserva'] = (isset($data['flag_reserva']))?$data['flag_reserva']:0;
    $send['ubicacion'] = 'node';
    $send['imagen'] = (empty($data['imagen']))? '' :$data['imagen'];
    $send['comentario'] = (empty($data['comentario']))? '' : $data['comentario'];

$servicioDAO = new servicioDAO();
$Resultado = $servicioDAO->insertarServicio($send);

if($Resultado[0]['p_return_code'] != '-1' || $Resultado[0]['p_return_code'] != '-2'){

	$json['status'] = true;
	$json['id'] = $Resultado[0]['p_return_code'];
	$json['mesnaje'] = "Su servicio fue agregada con éxito";
}else{
	$json['status'] = false;
	$json['mensaje'] = "Error al agregar el servicio";
}

echo json_encode($json);

