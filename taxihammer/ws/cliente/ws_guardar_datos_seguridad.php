<?php
header('Content-Type: application/json');

require_once'../../DAL/datosSeguridadDAO.php';
require_once'../../DAL/constantes.php';

$data['dseg_id'] = $_REQUEST['id'];//id cliente 
$data['dseg_nombre'] = DelCharacter($_REQUEST['nombre']);
$data['dseg_correo'] = $_REQUEST['correo'];
$data['dseg_status'] = $_REQUEST['status'];

$seguridadDAO = new datosSeguridadDAO();
$Res = $seguridadDAO->validSeguridadCliente($data['dseg_id']);
$contador = 0;
$json['status'] = false;
$json['mensaje'] = 'Error Inesperado'; 

if($Res[0]['Total'] > 0 ){
	$contador = $contador+1;
}

if($contador == 0){
	//INSERTAMOS------
	$Res = $seguridadDAO->insert($data);
	if($Res == true){
		$json['status'] = true;
		$json['mensaje'] = 'Datos guardados con éxito'; 
	}else{
		$json['status'] = false;
		$json['mensaje'] = 'Ups, hubo un error'; 
	}
}else{
	//UPDATE------
	$Res = $seguridadDAO->update($data);
	if($Res == true){
		$json['status'] = true;
		$json['mensaje'] = 'Datos guardados con éxito'; 
	}else{
		$json['status'] = false;
		$json['mensaje'] = 'Ups, hubo un error'; 
	}
}
echo json_encode($json);