<?php
//ACTUALIZAR EL CLIENTE EXPRESS
header('Content-Type: application/json');
$data['cliente_id'] = $_REQUEST['cliente_id'];
$data['cliente_express'] = $_REQUEST['cliente_express']; // 1 o 0

require_once'../../DAL/clientesDAO.php';
$clienteDAO = new clientesDAO();

$res = $clienteDAO->updateClienteExpress($data);

if($res == true){
	$json['status'] = true;
	$json['mensaje'] = "Actualizacion con éxito";
}else{
	$json['status'] = false;
	$json['mensaje'] = "Error al actualizar";
}
echo json_encode($json);