<?php
header('Content-Type: application/json');


require_once'../../DAL/clientesDAO.php';

$cliente_id = $_REQUEST['idCliente'];

$clientesDAO = new clientesDAO();
$Res = $clientesDAO->validarCliente($cliente_id);

if(!empty($Res)){
	if($Res[0]['total'] > 0 ){
		$update = $clientesDAO->updateClienteToken(1, $Res[0]['cliente_token_ios'], 1, $cliente_id);
		if($update == true){
			$json['status'] = true;
			$json['mensaje'] = "Se actualizado correctamente el token";
		}else{
			$json['status'] = false;
			$json['mensaje'] = "Error al actualizar el token";
		}
	}
}else{
	$json['status'] = false;
	$json['mensaje'] = "No existe cliente en la BD";
}

echo json_encode($json);
