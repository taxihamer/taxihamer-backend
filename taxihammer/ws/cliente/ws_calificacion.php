<?php
header('Content-Type: application/json');

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once'../../DAL/calificacionDAO.php';
require_once '../../DAL/conductorDAO.php';
require_once '../../DAL/constantes.php';

$data['cliente_id'] = $_REQUEST['cliente_id'];
$data['conductor_id'] = $_REQUEST['conductor_id'];
$data['calificacion_tipoid'] = $_REQUEST['tipo'];
$data['servicio_id'] = $_REQUEST['servicio_id'];
$data['calificacion_value'] = $_REQUEST['calificacion'];
$data['mensaje'] = (isset($_REQUEST['mensaje']))?DelCharacter($_REQUEST['mensaje']):'';

$calificacionDAO = new calificacionDAO();
$res = $calificacionDAO->insertarCalificacion($data);
$update = $calificacionDAO->updateCalificacionflag($_REQUEST['cliente_id'],0);
$promedio = $calificacionDAO->calificacionPromedio($_REQUEST['conductor_id'],$_REQUEST['tipo']);
$conductor_calificacion = $promedio[0]['promedio'];

$conductorDAO = new conductorDAO();
$result = $conductorDAO->updateCalificacion($_REQUEST['conductor_id'],$conductor_calificacion); 

if($result == true){
	$json['mensaje'] = 'Calificación Actualizada con Éxito';
	$json['status'] = true;
}else{
	$json['mensaje'] = 'Error actualizando calificación';
	$json['status'] = false;
}

echo json_encode($json);