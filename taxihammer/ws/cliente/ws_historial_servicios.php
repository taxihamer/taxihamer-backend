<?php 
header('Content-Type: application/json, utf-8');
//nombre origen destino, direccione origen destino, fechainicio, fechafin, tarifa
$data['cliente_id'] = $_REQUEST['cliente_id'];
$data['inicio'] = $_REQUEST['inicio'];
$data['limite'] = $_REQUEST['limite'];

require_once'../../DAL/servicioDAO.php';
require_once'../../DAL/constantes.php';

$servicioDAO = new servicioDAO();
$Res = $servicioDAO->listaCliente($data);
$contador = 0;
$cadena = '';


function utf8array(&$value,$key){

	$a = ['Ã¡','Ã©','Ã*','Ã³','Ãº','Ã','Ã‰','Ã','Ã“','Ãš','Ã±','Ã§','Ã‘','Ã‡','Â©','Â®','â„¢','Ã˜','Âª','Ã¤','Ã«','Ã¯','Ã¶','Ã¼','Ã„','Ã‹','Ã ','Ã–','Ãœ','Ã­','ÃÂº'];
	$b = ['á','é','í','ó','ú','Á','É','Í','Ó','Ú','ñ','ç','Ñ','Ç','©','®','™','Ø','ª','ä','ë','ï','ö','ü','Ä','Ë','Ï','Ö','Ü','í','ú'];

     $value = utf8_decode(str_replace($a,$b,($value)));
}




foreach($Res AS $res){
	array_walk($res, 'utf8array');

	$total = $res["tarifa"]+$res['espera']+$res['parada']+$res['parqueo']+$res['peaje'];

	$cadena = $cadena.'{"origen": "'.DelCharacter($res["origen_nombre"]).'",
						"direccion_origen": "'.DelCharacter($res["origen_direccion"]).'",
						"destino": "'.DelCharacter($res["destino_nombre"]).'",
						"direccion_destino": "'.DelCharacter($res["destino_direccion"]).'",
						"fecha_inicio": "'.$res["hora_inicio"].'",
						"fecha_fin": "'.$res["hora_fin"].'",
						"fecha": "'.$res["fecha"].'",
						"hora": "'.$res["hora"].'",
						"tarifa": '. round($total,2).' },';
	$contador++;

}

$json = '{"status": true, "result":['.substr($cadena,0,-1).']}'; 

if($contador == 0){
	$json = '{"status": false, "mensaje": "No existen servicios para mostrar" }';
}

echo $json;