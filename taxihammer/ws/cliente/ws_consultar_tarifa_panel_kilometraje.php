<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once'../../DAL/tarifarioDAO.php';
require_once'../../DAL/generalDAO.php';
require_once'../../DAL/tipoServicioDAO.php';
require_once'../../DAL/constantes.php';

$tarifarioDAO = new tarifarioDAO();
$generalDAO = new generalDAO();
$tipoServicioDAO = new tipoServicioDAO();

$LatOrigin = $_REQUEST['latOri'];
$LonOrigin = $_REQUEST['lngOri'];
$LatDestin = $_REQUEST['latDes'];
$LonDestin = $_REQUEST['lngDes'];

$tiposervicio_id = $_REQUEST['tiposervicio_id'];
$Reserva = $_REQUEST['reserva'];
$Activo = $_REQUEST['activo'];
$envio = (isset($_REQUEST['envio']))?$_REQUEST['envio']:null; //origen o destino
$urlDirecction = "https://maps.googleapis.com/maps/api/directions/json?origin=".$LatOrigin.",".$LonOrigin."&destination=".$LatDestin.",".$LonDestin."&key=".KEY_PANEL_GOOGLE."&mode=driving&departure_time=now&alternatives=true";


$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL,$urlDirecction);
$result=curl_exec($ch);
curl_close($ch);

$result = json_decode($result, true);

for ($i=0; $i<count($result['routes']); $i++) {

   $explode = explode(" mins", $result['routes'][$i]['legs'][0]['duration_in_traffic']['text']);
   $index = $explode[0];
   $valor[] = array('distancia'=>$result['routes'][$i]['legs'][0]['distance']['text'],'tiempo'=>$result['routes'][$i]['legs'][0]['duration_in_traffic']['value']);
   
}

$total = count($valor);
$quiero = [];
if($total < 2){
	$quiero = $valor[0];
}else if($total > 1 && $total <= 2){
	$quiero = $valor[1];
}else{
	$quiero = $valor[1];
}


$distanciaNew = explode(" km", $quiero['distancia']);
$tiemposNew = explode(" mins", $quiero['tiempo']);

$config = $generalDAO->listaBy(1);
$tipo = $tipoServicioDAO->listaTipoServiciosId($tiposervicio_id);
$RangoHoras = $tarifarioDAO->listRangoHorasWs();

$valor = 0;

if(!empty($RangoHoras)){

	$dia = $RangoHoras[0]['Dia'];

	switch ($dia) {
	    case "Lunes": $campo = 'rango_lun'; break;
	    case "Martes": $campo = 'rango_mar'; break;
	    case "Miercoles": $campo = 'rango_mie'; break;
	    case "Jueves": $campo = 'rango_jue'; break;
		case "Viernes": $campo = 'rango_vie'; break;
		case "Sabado": $campo = 'rango_sab'; break;
		case "Domingo": $campo = 'rango_dom'; break;
	}
	/*$array = json_decode($RangoHoras[0]['rango_check']);
	setlocale(LC_TIME,"es_ES");
	$HoraActual = strftime("%H:%M %p");
	// del día de la semana, desde 1 (lunes) hasta 7 (domingo)
	$Dia = $campo = '';
	$DiaActual = date("N");
	switch ($DiaActual) {
	    case 1: $Dia = "Lunes"; $campo = 'rango_lun'; break;
	    case 2: $Dia = "Martes"; $campo = 'rango_mar'; break;
	    case 3: $Dia = "Miercoles"; $campo = 'rango_mie'; break;
	    case 4: $Dia = "Jueves"; $campo = 'rango_jue'; break;
		case 5: $Dia = "Viernes"; $campo = 'rango_vie'; break;
		case 6: $Dia = "Sabado"; $campo = 'rango_sab'; break;
		case 7: $Dia = "Domingo"; $campo = 'rango_dom'; break;
	}

	$clave = '';
	if (in_array($DiaActual, $array)) {
		$clave = "Activo";
	}else{
		$clave = "Inactivo";
	};
	$valor = 0;
	if($clave == "Activo"){*/
		$Porcentaje = $tarifarioDAO->obtenerPorcentajeHora($campo);
		if(!empty($Porcentaje)){
			switch ($campo) {
				case 'rango_lun': $valor = $Porcentaje[0][$campo];break;
				case 'rango_mar': $valor = $Porcentaje[0][$campo];break;
				case 'rango_mie': $valor = $Porcentaje[0][$campo];break;
				case 'rango_jue': $valor = $Porcentaje[0][$campo];break;
				case 'rango_vie': $valor = $Porcentaje[0][$campo];break;
				case 'rango_sab': $valor = $Porcentaje[0][$campo];break;
				case 'rango_dom': $valor = $Porcentaje[0][$campo];break;
			}
		}else{
			$valor = 0;
		}
	//}

}

if(!empty($config)){
	
	if($config[0]['tipo_tarifa'] != 1){//Por Kilometraje
		
		$Pbase = $config[0]['tarifa_minima'];
		$Tiempo = floatval($tiemposNew[0])/60;
		$CostoKilometro = $config[0]['factor_tarifa_km'];
		$Distancia = $distanciaNew[0];
		$CostoTiempo = $config[0]['factor_tarifa_tiempo'];
		$TarifaMinima = $config[0]['tarifa_minima_m'];
		
		$f1 = $Pbase+($Tiempo*$CostoTiempo)+($Distancia*$CostoKilometro);
		$fservicio = 1+(floatval($tipo[0]['tiposervicio_factor'])/100);
		

		if($f1 < $TarifaMinima){ $f1 = $TarifaMinima;}
		
		$f1fservicio = floatval($f1)*floatval($fservicio);
		
		$aeropuerto = 0;
		if($Activo == 1){
				$aeropuerto = floatval($tipo[0]['tiposervicio_aeropuerto']);
				if(!empty($envio)){
					if($envio == 'origen'){
						$aeropuerto = $aeropuerto + floatval($config[0]['aeropuertoD']);
					}else{
						$aeropuerto = $aeropuerto + floatval($config[0]['aeropuertoO']);
					}
				}else{
					$envio = 'panel';
				}

		}

		$MontoReserva = 0;
		if($Reserva == 1){//Es una reserva
			$MontoReserva = floatval($tipo[0]['tiposervicio_reserva']);
			/*if($_REQUEST['tiposervicio_id'] == 1){
				$MontoReserva = 5;
			}else{
				$MontoReserva = 10;
			}*/
		}

		$TarifaInicial = floatval($f1fservicio)+floatval($aeropuerto)+floatval($MontoReserva);
		$HorasPuntas = 1+(floatval($valor)/100);
		$TarifaFinal = floatval($TarifaInicial)*$HorasPuntas;

		$json['status'] = true;
		/*if($Activo == 1){
			$new = floatval($TarifaFinal)-5;
		}else{
			$new = floatval($TarifaFinal);
		}*/

		$in = $TarifaFinal*2;
		$tarifa_new = ceil($in)/2;
		
		$json['tarifa'] = floatval($tarifa_new);//$tarifa_new
		$json['incremento'] = (!empty($valor))?1:2;
		$json['envio'] = $envio;
		$json['tiposervicio_id'] = $tiposervicio_id;
	}
}
//$json['tarifa'] = 1;
echo json_encode($json);
//var_dump(json_decode($result, true));


