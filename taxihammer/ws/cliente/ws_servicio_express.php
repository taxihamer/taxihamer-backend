<?php
header('Content-Type: application/json');
$cliente_id = $_REQUEST['cliente_id'];

require_once'../../DAL/clientesDAO.php';
$clienteDAO = new clientesDAO();
$Res = $clienteDAO->listaSimple();

foreach($Res AS $res){
	if($res['cliente_id'] == $cliente_id){
		$estado = $res['cliente_express'];
		$nombre = $res['cliente_nombre']." ".$res['cliente_apellido'];
	}
}
if($estado == 0){
	$json['estado'] = 0;
	$json['mensaje'] = "Desactivado";
	$json['nombre'] = $nombre;
}else{
	$json['estado'] = 1;
	$json['mensaje'] = "Activado";
	$json['nombre'] = $nombre;
}
echo json_encode($json);