<?php

$token = $_REQUEST["device"];
// $token = 'a3450fd196304ab8741b81b8a082020394466b524a86c5ce72a6da48a7f14dc2';
// $token = 'f749a02afd0d24734149560d81c19324adb57dfc1d88aacc8281602583732ef0';
$token = 'c0346a5d8ab71f99dee45a811d89000019dfdbcc8741b480376f6b9f8b94256a';

$deviceToken = $token;

// $pem = 'HLArticulosDev.pem';
$pem = '../../notificacion/CertDevPush.pem';
$passphrase = '123456';

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// ssl://gateway.sandbox.push.apple.com:2195
// prod: ssl://gateway.push.apple.com:2195

$fp = stream_socket_client(
    'ssl://gateway.sandbox.push.apple.com:2195', $err,
    $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
    exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;


$body = array();
$body['aps'] = array();
$body['aps']['alert'] = 'Nueva Notificación';

$tipo_notificacion = $_REQUEST['tipo_notificacion'];
$body['tipo_notificacion'] = $tipo_notificacion;

switch ($tipo_notificacion) {
    case '1':
        $mensaje = $_REQUEST["mensaje"];
        $body['mensaje'] = $mensaje;
        $body['aps']['alert'] = $mensaje;
        break;
    case '2':
        $imagen = $_REQUEST["imagen"];
        $link = $_REQUEST["link"];
        $body['imagen'] = $imagen;
        $body['link'] = $link;
        break;
    case '3':
        $mensaje = $_REQUEST["mensaje"];
        $body['mensaje'] = $mensaje;
        break;
    case '4';
        $mensaje = $_REQUEST["mensaje"];
        $body['mensaje'] = $mensaje;
        break;
}


$body['aps']['sound'] = 'default';
$body['aps']['badge'] = 1;

$payload = json_encode($body);

$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
    echo 'Message not delivered' . PHP_EOL;
else
    echo 'Message successfully delivered' . PHP_EOL;

fclose($fp);
