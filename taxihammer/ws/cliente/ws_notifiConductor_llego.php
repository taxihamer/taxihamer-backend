<?php
header("Content-Type: application/json; charset=utf-8");

require_once'../../DAL/clientesDAO.php';
require_once'../../DAL/constantes.php';
$ClientesDAO = new ClientesDAO();

$idCliente = $_REQUEST['cliente_id'];
$tipo = $_REQUEST['tipo'];

if($tipo != 10){
	$alert = ($tipo == 0)?'Tu Conductor ha llegado':(($tipo == 1)? NAME_EMPRESA_MOVIL.' en proceso':(($tipo == 2)? NAME_EMPRESA_MOVIL.' ha llegado a su destino':'Reserva Iniciada'));
	$sonido = ($tipo == 0)?'conductor_llego.mp3':(($tipo == 1)?'default':(($tipo == 2)?'servicio_finalizado_gracias.mp3':'default'));
}else{
	$alert = 'El Conductor ha cancelado el servicio';
	$sonido = 'default';
}
$json = array();

$resp = $ClientesDAO->listaClienteId($idCliente);

if(!empty($resp)){

	$token_ios = $resp[0]['cliente_token_ios'];

	if(!empty($token_ios)){

		//$pem = '../../notificacion/CertDevPush.pem';
		$pem = '../../notificacion/CertProdPush.pem';
		$passphrase = '123456';

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// $fp = stream_socket_client(
	    // 'ssl://gateway.sandbox.push.apple.com:2195', $err,
	    // $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
		$fp = stream_socket_client(
	    'ssl://gateway.push.apple.com:2195', $err,
	    $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);


		if($fp){
			$body['aps'] = array(
				'alert' => $alert,
				'sound' => $sonido,
				'badge' => 0,
				'status' => $tipo
			);
			
			$payload = json_encode($body);
			$msg = chr(0) . pack('n', 32) . pack('H*', $token_ios) . pack('n', strlen($payload)) . $payload;
			$result = fwrite($fp, $msg, strlen($msg));
			fclose($fp);
			$json['success'] = true;
			$json['message'] = 'Notificacion enviada';
		}
	}
}
else{
	$json['success'] = false;
	$json['message'] = 'Cliente no encontrado';
}

echo json_encode($json);