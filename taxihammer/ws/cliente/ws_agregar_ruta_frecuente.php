<?php
header('Content-Type: application/json');

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once'../../DAL/rutasFrecuentesDAO.php';
require_once'../../DAL/constantes.php';
//INSERTAR RUTAS FRECUENTES
$data['cliente_id'] = $_REQUEST['cliente_id'];
$data['rutas_nombre'] = DelCharacter($_REQUEST['nombre']);
$data['origen_latitud'] = $_REQUEST['latitud_origen'];
$data['origen_longitud'] = $_REQUEST['longitud_origen'];
$data['destino_latitud'] = $_REQUEST['latitud_destino'];
$data['destino_longitud'] = $_REQUEST['longitud_destino'];
$data['id_origen'] = $_REQUEST['id_origen'];
$data['id_destino'] = $_REQUEST['id_destino'];
$data['tiposervicio_id'] = $_REQUEST['tiposervicio_id'];
$data['direccion_origen'] = DelCharacter($_REQUEST['direccion_origen']);
$data['direccion_destino'] = DelCharacter($_REQUEST['direccion_destino']);
$data['referencia_origen'] = DelCharacter($_REQUEST['referencia_origen']);
$data['referencia_destino'] = DelCharacter($_REQUEST['referencia_destino']);
$data['nombre_origen'] = DelCharacter($_REQUEST['nombre_origen']);
$data['nombre_destino'] = DelCharacter($_REQUEST['nombre_destino']);


$rutasFrecuentesDAO = new rutasFrecuentesDAO();

$res = $rutasFrecuentesDAO->insertarRutasFrecuentes($data);

if($res == true){
	$json['status'] = true;
	$json['mensaje'] = "Ruta Frecuente agregada con éxito";
}else{
	$json['status'] = false;
	$json['mensaje'] = "Error al agregar ruta frecuente";
}
echo json_encode($json);