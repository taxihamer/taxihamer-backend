<?php
header('Content-Type: application/json');
$user = $_REQUEST['correo'];
$pass = $_REQUEST['password'];
$tipo = $_REQUEST['tipo'];
$token = $_REQUEST['token'];

error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once'../../DAL/clientesDAO.php';
require_once'../../DAL/generalDAO.php';

$clienteDAO = new clientesDAO();
$generalDAO = new generalDAO();
//$Res = $clienteDAO->lista();
$Res = $clienteDAO->listaBylogin($user,$pass);
$listkey = $generalDAO->listKeyMoviles();

$json['status'] = false;
$json['mensaje'] = "Usuario o password incorrectos";
$contador = 0;

if(!empty($Res)){


	$json['nombre'] = $Res[0]['cliente_nombre'];
	$json['apellido'] = $Res[0]['cliente_apellido'];
	$json['calificacion'] = $Res[0]['cliente_calificacion'];
	$json['cliente_id'] = $Res[0]['cliente_id'];
	$json['empresa_id'] = $Res[0]['empresa_id'];
	$json['puntos'] = $Res[0]['cliente_puntos'];
	$json['celular'] = $Res[0]['cliente_celular'];

	$json['foursquare_key'] = $listkey[0]['api_foursquare_key'];
	$json['foursquare_secret'] = $listkey[0]['api_foursquare_secret'];
	$json['here_key'] = $listkey[0]['api_here_key'];
	$json['here_secret'] = $listkey[0]['api_here_secret'];
	$json['google_ios'] = $listkey[0]['api_google_ios'];
	$json['google_android'] = $listkey[0]['api_google_android'];
	$json['google_web'] = $listkey[0]['api_google_web'];

	$json['status'] = true;
	$json['mensaje'] = "Inicio de sesión exitoso";

	$validtoken = $clienteDAO->validarClienteToken($token,$tipo);

	if($validtoken[0]['TOTAL'] != 0){
		$tokencliente = $clienteDAO->updateClienteToken($tipo,$token,1,$Res[0]['cliente_id']);
	}		
	$tokencliente = $clienteDAO->updateClienteToken($tipo,$token,2,$Res[0]['cliente_id']);

	if($Res[0]['cliente_estados'] == 0){
		$contador = $contador +1;
	}
}

if($contador != 0){
	unset($json);
	$json['status'] = false;
	$json['mensaje'] = "Usuario Empresarial aún no activado";
}
echo json_encode($json);