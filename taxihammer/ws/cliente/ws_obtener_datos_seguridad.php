<?php
header('Content-Type: application/json');

error_reporting(E_ALL);
ini_set('display_errors', 1);


$data['dseg_id'] = $_REQUEST['id'];

require_once'../../DAL/datosSeguridadDAO.php';
$seguridadDAO = new datosSeguridadDAO();
$Res = $seguridadDAO->lista();
$contador = 0;
foreach($Res AS $res){
	if($data['dseg_id'] == $res['dseg_id']){
		$json['status'] = true;
		$json['mensaje'] = "Datos Encontrados";
		$json['id'] = $res['dseg_id'];
		$json['nombre'] = $res['dseg_nombre'];
		$json['correo'] = $res['dseg_correo'];
		$json['estado'] = $res['dseg_status'];
		$contador++;
	}
}
if($contador == 0){
	$json['status'] = false;
	$json['mensaje'] = "Datos no encontrados en la Base de Datos";
}
echo json_encode($json);