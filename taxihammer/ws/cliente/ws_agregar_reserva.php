<?php
require_once'../../DAL/constantes.php';

header('Content-Type: application/json');

$f1 = strtotime($_REQUEST['fecha']);
$fechaselec = $_REQUEST['fecha'];
$explode1 = explode(" ", $fechaselec);
$hora1 = explode(":", $explode1[1]);

$f2 = strtotime(date('Y/m/d H:i'));
$fechas = date('Y/m/d H:i');
$explode2 = explode(" ", $fechas);
$hora2 = explode(":", $explode2[1]);

$horareserv = HORA_RESERVA;
$difHoras = $hora1[0] - $hora2[0];
$contador = false;


//TODO por cambiar, esta comparando las horas ignorando la fecha
//JAVIER
/*
  	if((int)$f1 >= (int)$f2){
      	if((int)$difHoras >= (int)$horareserv){
      		$contador = true;
      	}
  	}
*/
$diffDates = $f1 - $f2;
$diffHours = $diffDates / ( 60 * 60 );
if((int)$f1 >= (int)$f2){
    if((int)$diffHours >= (int)$horareserv){
        $contador = true;
    }
}


if($contador){

	$data['cliente_id'] = $_REQUEST['cliente_id'];
	$data['fecha'] = $_REQUEST['fecha'];
	$data['id_origen'] = (!empty($_REQUEST['id_origen']))?$_REQUEST['id_origen']:0;
	$data['origen'] = $_REQUEST['origen'];
	$data['origen_latitud'] = $_REQUEST['latitud_origen'];
	$data['origen_longitud'] = $_REQUEST['longitud_origen'];
	$data['direccion_origen'] = $direccion_origen = DelCharacter($_REQUEST['direccion_origen']);
	$data['ref_origen'] = $referencia_origen = DelCharacter($_REQUEST['referencia_origen']);
	$data['id_destino'] = (!empty($_REQUEST['id_destino']))?$_REQUEST['id_destino']:0;
	$data['destino'] = $_REQUEST['destino'];
	$data['destino_latitud'] = $_REQUEST['latitud_destino'];
	$data['destino_longitud'] = $_REQUEST['longitud_destino'];
	$data['direccion_destino'] = $direccion_destino = DelCharacter($_REQUEST['direccion_destino']);
	$data['ref_destino'] = $referencia_destino = DelCharacter($_REQUEST['referencia_destino']);
	$data['tiposervicio_id'] = $_REQUEST['tiposervicio_id'];
	$data['tipo_pago'] = $_REQUEST['tipopago_id'];
	$data['tarifa'] = $_REQUEST['tarifa'];
	$data['reserva_estado'] = 0;

	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	require_once'../../DAL/reservaDAO.php';
	require_once'../../DAL/constantes.php';

	$reservaDAO = new reservaDAO();
	$res = $reservaDAO->insertarWS($data);

	if(isset($res['status']) && $res['status'] == true){

	//if($res[0]['p_return_code'] != '-1' || $res[0]['p_return_code'] != '-2'){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, IP_SOCKET.'w_agregar_reserva?reserva_id='.$res['lastid'].'&cliente_id='.$_REQUEST['cliente_id'] );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);

		$json['status'] = true;
		$json['mensaje'] = "Reserva guardada con éxito ".$data['fecha'];

	}else{

		$json['status'] = false;
		$json['mensaje'] = "Ups, hubo un error ".$data['fecha'];

	}

}else{

	$json['status'] = false;
	$json['mensaje'] = "La reserva es con un minimo de ".$horareserv." hora de anticipacion";
}

echo json_encode($json);
