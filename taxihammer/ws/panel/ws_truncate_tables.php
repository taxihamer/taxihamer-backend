<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once'../../DAL/generalDAO.php';

$generalDAO = new generalDAO();
$tables = $generalDAO->showtables(1,null);

if(!empty($tables)){
	$tb = array("tb_api_key", "tb_bancos", "tb_configuracion", "tb_lugares", "tb_menu","tb_menu_rol","tb_rol","tb_submenu","tb_tarifario","tb_tipocliente","tb_tipotarifa", "tb_usuario","tb_tiposervicio");

	$newtb = array();
	foreach ($tables as $rowtables) {
		if (!in_array($rowtables['Tables_in_tutaxi'], $tb)) {
			array_push($newtb, $rowtables['Tables_in_tutaxi']);
		}
	}	

	foreach ($newtb as $rowtr){
		$generalDAO->showtables(2,$rowtr);
	}
}
