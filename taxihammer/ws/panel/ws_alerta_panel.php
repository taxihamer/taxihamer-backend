<?php 

header('Content-type: application/json; charset=utf-8');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once'../../DAL/clientesDAO.php';
require_once'../../DAL/conductorDAO.php';
require_once'../../DAL/vehiculoDAO.php';
require_once'../../DAL/alertaDAO.php';
require_once'../../DAL/servicioDAO.php';
require_once'../../DAL/lugaresDAO.php';
require_once'../../DAL/constantes.php';

$tipo = $_REQUEST['tipo'];
$id = $_REQUEST['codigo'];
$latitud = $_REQUEST['latitud'];
$longitud = $_REQUEST['longitud'];
$flag = $_REQUEST['flag'];

$clientesDAO = new clientesDAO();
$conductorDAO = new conductorDAO();
$vehiculoDAO = new vehiculoDAO();
$alertaDAO = new alertaDAO();
$servicioDAO = new servicioDAO();
$lugaresDAO = new lugaresDAO();

$jsondata = array();

if($tipo == 1){

	$res = $clientesDAO->listaClienteId($id);

	$data['alerta_tipo'] = $tipo;
	$data['alerta_latitud'] = $latitud;
	$data['alerta_longitud'] = $longitud;
	$data['alerta_personaid'] = $id;
	$data['alerta_flag'] = 0;

	foreach ($res as $key => $value) {
		$jsondata['clienteNombre'] = $value['cliente_nombre'].' '.$value['cliente_apellido'];
		$jsondata['clienteCorreo'] = $value['cliente_correo'];
		$jsondata['clienteCelular'] = $value['cliente_celular'];
	}

}elseif($tipo == 2){

	$res = $conductorDAO->listaConductorId($id);
	$unidad = $vehiculoDAO->listaBy($res[0]['unidad_id']);

	$data['alerta_tipo'] = $tipo;
	$data['alerta_latitud'] = $latitud;
	$data['alerta_longitud'] = $longitud;
	$data['alerta_personaid'] = $id;
	$data['alerta_unidadid'] = $unidad[0]['unidad_id'];
	$data['alerta_flag'] = 0;

	foreach ($res as $key => $value) {

		$jsondata['conductorNombre'] = $value['conductor_nombre'].' '.$value['conductor_apellido'];
		$jsondata['conductorCorreo'] = $value['conductor_correo'];
		$jsondata['conductorDni'] = $value['conductor_dni'];
		$jsondata['conductorFoto'] = $value['conductor_foto'];
		$jsondata['unidadtiposervi'] = $unidad[0]['tiposervicio_nombre'];
		$jsondata['unidadAlias'] = $unidad[0]['unidad_alias'];
		$jsondata['unidadPlaca'] = $unidad[0]['unidad_placa'];
		$jsondata['unidadModelo'] = $unidad[0]['unidad_modelo'];
		$jsondata['unidadMarca'] = $unidad[0]['unidad_marca'];
		$jsondata['unidadColor'] = $unidad[0]['unidad_color'];
		$jsondata['unidadFabrica'] = $unidad[0]['unidad_fabricacion'];
		$jsondata['unidadFoto'] = $unidad[0]['unidad_foto'];
	}

}else{

	$service = $servicioDAO->listaServicio($id);
	$cliente = $clientesDAO->listaClienteId($service[0]['cliente_id']);
	$conduct = $conductorDAO->listaConductorId($service[0]['conductor_id']);
	$unidad = $vehiculoDAO->listaBy($conduct[0]['unidad_id']);


	$data['alerta_tipo'] = $tipo;
	$data['alerta_latitud'] = $latitud;
	$data['alerta_longitud'] = $longitud;
	$data['alerta_personaid'] = $id;
	$data['alerta_unidadid'] = $unidad[0]['unidad_id'];
	$data['alerta_flag'] = $flag;

	foreach ($service as $key => $value) {

		$jsondata['servicio_id'] = $value['servicio_id'];
		$jsondata['origen_nombre'] = DelCharacter($value['origen_direccion']);
		$jsondata['destino_nombre'] = DelCharacter($value['destino_direccion']);
		/*$lugarorigen = $lugaresDAO->listabyin($value['origen_id']);
		$lugardestino = $lugaresDAO->listabyin($value['destino_id']);
		foreach ($lugarorigen as $row) {
			$jsondata['origen_nombre'] = $row['lugar_descripcion'];
		}
		foreach ($lugardestino as $row) {
			$jsondata['destino_nombre'] = $row['lugar_descripcion'];
		}*/
		$jsondata['clienteNombre'] = $cliente[0]['cliente_nombre'].' '.$cliente[0]['cliente_apellido'];
		$jsondata['clienteCorreo'] = $cliente[0]['cliente_correo'];
		$jsondata['clienteCelular'] = $cliente[0]['cliente_celular'];
		$jsondata['conductorNombre'] = $conduct[0]['conductor_nombre'].' '.$conduct[0]['conductor_apellido'];
		$jsondata['conductorCorreo'] = $conduct[0]['conductor_correo'];
		$jsondata['conductorDni'] = $conduct[0]['conductor_dni'];
		$jsondata['unidadtiposervi'] = $unidad[0]['tiposervicio_nombre'];
		$jsondata['unidadAlias'] = $unidad[0]['unidad_alias'];
		$jsondata['unidadPlaca'] = $unidad[0]['unidad_placa'];
		$jsondata['unidadModelo'] = $unidad[0]['unidad_modelo'];
		$jsondata['unidadMarca'] = $unidad[0]['unidad_marca'];
		$jsondata['unidadColor'] = $unidad[0]['unidad_color'];
		$jsondata['unidadFabrica'] = $unidad[0]['unidad_fabricacion'];
		$jsondata['unidadFoto'] = $unidad[0]['unidad_foto'];
	}
}

	$insertAlert = $alertaDAO->insertAlerta($data);

	if($insertAlert[0]['p_return_code'] != '-1' || $insertAlert[0]['p_return_code'] != '-2'){

		$jsondata['alerta_id'] = $insertAlert[0]['p_return_code'];
		$jsondata['status'] = true;
		
	}else{

		$jsondata['status'] = false;

	}

echo json_encode($jsondata);
